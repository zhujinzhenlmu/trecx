import os
import sys

dirpath = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0,dirpath+'/../../scripts')

from compare import *

inputDir='tutorial'

def test_00(): test(inputDir,'00HarmonicOsc1',compare_eigenvalues)
def test_01(): test(inputDir,'01HarmonicOsc2',compare_eigenvalues)
def test_02(): test(inputDir,'02HarmonicOscPolar',compare_eigenvalues)
def test_03(): test(inputDir,'03HydrogenPolar',compare_eigenvalues)
def test_04(): test(inputDir,'04Hyd1d',compare_eigenvalues)
def test_05(): test(inputDir,'05irECS',compare_eigenvalues)
def test_06(): test(inputDir,'06TimeProp',compare_outf,[3, 4, 5, 6])
def test_07(): test(inputDir,'07HighHarmonicGeneration',compare_outf)
def test_08(): testSpectrum(inputDir,'08Hyd1dSpectrum',100,6,"total")
def test_09(): testSpectrum(inputDir,'09HydrogenSpectrum',100,6)
def test_10(): testSpectrum(inputDir,'10IRSpectrum',100)
def test_11(): testSpectrum(inputDir,'11IRshortPulse',100)
def test_12(): testSpectrum(inputDir,'12IRlongPulse',100)
def test_17(): testSpectrum(inputDir,'17MixedGauge',100)
def test_22(): test(inputDir,'22Helium6d',compare_eigenvalues)

developDir='develop'
def test_120(): test(developDir,'120Helium2d',compare_outf,[3, 4])
def test_23(): test2DSpectrum(developDir,'23HeliumSpec',100)
def test_100(): testSpectrum(developDir,'00Circular400nm',100,4,"total")
def test_210(): test(developDir,'210OffCenter',compare_eigenvalues)
