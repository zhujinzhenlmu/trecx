#!/usr/bin/env python
import os
import nose
import sys
import argparse
dirpath = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0,dirpath+'/scripts')
import run_tests

run_tests.run(dirpath+'/master','tutorial')
