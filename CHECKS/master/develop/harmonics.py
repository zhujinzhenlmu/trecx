# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENCE file included with the source distribution
# 
# Copyright (c) 2015 by Armin Scrinzi
# End of license

#! /usr/bin/env python

import argparse
import logging
import math
import numpy as np
import os
import time



E_PHOTON_800NM = 3.951908121E-20  # 2.483057104E-19  # in Joule
HARTREE = 4.35974418E-18  # in Joule

# Multiplication factor to convert frequency from atomic units to units of 800nm photon energy
# e.g. E[800nm photon energy] = AU_TO_800NM * Frequency[a.u.]
AU_TO_800NM = HARTREE / E_PHOTON_800NM


def cols_to_arrays(file, usecols=None):
    """
    Parse given file and return columns as arrays.

    Takes a filename or a file object as parameter.
    """
    return np.loadtxt(file, usecols=usecols, unpack=True)


class TdseException(Exception):
    pass


class PulseLengthMismatchError(TdseException):
    pass


class Pulse(object):

    def __init__(self, data, nInterpolationPoints=10000, valuesForAverage=5, wavelength_nm=None):
        '''
        Pulse should be initialized with a data array consisting of two numpy arrays with time and pulse values'''
        if len(data[0]) != len(data[1]):
            raise ValueError('Different number of time- and pulsevalues in Pulse')

        self._spectrum = None
        self._fftfreqs = None
        self._averaged_spectrum = None
        self._raw_times = data[0]
        self._raw_values = data[1]
        self._num_values_for_average = valuesForAverage
        self._frequency_multiplier = \
            2 * math.pi if wavelength_nm is None else AU_TO_800NM / 800.0 * wavelength_nm

        self.nInterpolationPoints = nInterpolationPoints

        self._interpolate()

    @property
    def spectrum(self):
        if self._spectrum is None:
            self._calculate_spectrum()
        return self._spectrum

    @property
    def fftfreqs(self):
        if self._fftfreqs is None:
            self._calculate_spectrum()
        return self._fftfreqs

    def _calculate_spectrum(self):
        time_begin = time.time()
        self._spectrum = np.abs(np.fft.fft(self.interpolated_values))
        normalization = sum(self._spectrum)
        self._spectrum = [value / normalization for value in self._spectrum]
        self._fftfreqs = [
            f * self._frequency_multiplier
            for f in np.fft.fftfreq(len(self.interpolated_values), 1. / self.get_sampling_rate())
        ]
        self._spectrum = self._spectrum[:int(len(self._spectrum) / 2)]  # Drop negative frequencies
        self._fftfreqs = self._fftfreqs[:int(len(self._fftfreqs) / 2)]
        logging.info('Calculated FFT in ' + str(time.time() - time_begin) + ' seconds')

    @property
    def averaged_spectrum(self):
        if self._num_values_for_average == 1:
            return self.spectrum
        if self._averaged_spectrum is None:
            self._calculate_averaged_spectrum(int(self._num_values_for_average / 2))  # implicitly symmetrize average area
        return self._averaged_spectrum

    def _calculate_averaged_spectrum(self, n_neighbors_each_direction):
        self._averaged_spectrum = []
        for i in range(len(self.spectrum)):
            left_bound = max(0, i - n_neighbors_each_direction)
            right_bound = min(len(self.spectrum), i + n_neighbors_each_direction) + 1
            neighbors_of_current_element = self.spectrum[left_bound: right_bound]
            self._averaged_spectrum.append(sum(neighbors_of_current_element) / len(neighbors_of_current_element))

    def _interpolate(self):
        self.interpolated_times = np.linspace(round(self._raw_times[0]), round(self._raw_times[-1]),
                                              self.nInterpolationPoints)
        self.interpolated_values = np.interp(self.interpolated_times, self._raw_times, self._raw_values)

    def get_multiplied_spectrum(self, frequency_power):
        """For comparison of HHG spectra calculated from length, velocity and acceleration data."""
        if frequency_power is 0:
            return self.spectrum

        return [math.pow(frequency * 2 * math.pi, frequency_power) * intensity
                for frequency, intensity in zip(self.fftfreqs, self.spectrum)]

    def get_nr_values(self):
        return len(self.interpolated_times)

    def get_start_time(self):
        return self.interpolated_times[0]

    def get_end_time(self):
        return self.interpolated_times[-1]

    def get_duration(self):
        return self.get_end_time() - self.get_start_time()

    def get_sampling_rate(self):
        return (self.nInterpolationPoints - 1) / self.get_duration()

    def get_relative_spectral_deviation(self, reference):
        if not self.nInterpolationPoints == reference.nInterpolationPoints:
            raise Exception('Number of interpolation points in pulses does not match (' + str(self.nInterpolationPoints)
                            + ' vs. ' + str(reference.nInterpolationPoints) + ')')
        if abs((self.get_duration() - reference.get_duration())) > 0.1:
            print('Duration 1: ' + str(self.get_duration()) + '\nDuration 2: ' + str(reference.get_duration()))
            raise PulseLengthMismatchError('Duration of pulses differs')
        return [2 * abs(own - reference) / (averaged_own + averaged_ref)
                for own, reference, averaged_own, averaged_ref
                in zip(self.spectrum, reference.spectrum, self.averaged_spectrum, reference.averaged_spectrum)]


def run():
    arg_parser = argparse.ArgumentParser(description='Calculate HHG spectra from the given file, output is stored in' +
                                         ' file \'harmonics\'\nFrequencies are calculated in atomic units.')

    arg_parser.add_argument('file', type=str, help='Input file')
    arg_parser.add_argument('-dp', '--discretizationpoints', type=int,
                            help='Use the given number of interpolated points for FFT calculation', default=10000)
    arg_parser.add_argument('-l', '--length', dest='length', type=int, help='Column containing length data', default=5)
    arg_parser.add_argument('-v', '--velocity', dest='velocity', type=int, help='Column containing velocity data',
                            default=6)
    arg_parser.add_argument('-a', '--acceleration', dest='acceleration', type=int,
                            help='Column containing acceleration data', default=7)

    command_line_args = arg_parser.parse_args()

    file = command_line_args.file

    output_header = '#Frequency'
    fft_data = [[]]

    if command_line_args.length:
        print('Reading data for length from column ' + str(command_line_args.length))
        data = cols_to_arrays(file, (0, command_line_args.length - 1))
        length_pulse = Pulse(data, nInterpolationPoints=command_line_args.discretizationpoints)
        fft_data.append(length_pulse.get_multiplied_spectrum(2))
        fft_data[0] = length_pulse.fftfreqs
        output_header += '\tLength'

    if command_line_args.velocity:
        print('Reading data for velocity from column ' + str(command_line_args.velocity))
        data = cols_to_arrays(file, (0, command_line_args.velocity - 1))
        velocity_pulse = Pulse(data, nInterpolationPoints=command_line_args.discretizationpoints)
        fft_data.append(velocity_pulse.get_multiplied_spectrum(1))
        fft_data[0] = velocity_pulse.fftfreqs
        output_header += '\tVelocity'

    if command_line_args.acceleration:
        print('Reading data for acceleration from column ' + str(command_line_args.acceleration))
        data = cols_to_arrays(file, (0, command_line_args.acceleration - 1))
        acceleration_pulse = Pulse(data, nInterpolationPoints=command_line_args.discretizationpoints)
        fft_data.append(acceleration_pulse.get_multiplied_spectrum(0))
        fft_data[0] = acceleration_pulse.fftfreqs
        output_header += '\tAcceleration'

    output_header += '\n'

    output_filename = os.path.dirname(file)
    output_filename = os.path.join(output_filename, 'harmonics')

    with open(output_filename, mode='w') as outfile:
        outfile.write(output_header)
        for i in range(len(fft_data[0])):
            currentline = ''
            for array in fft_data:
                currentline += str(array[i]) + '\t'
            currentline = currentline[:-1] + '\n'
            outfile.write(currentline)
        print('Output written to file ' + output_filename)


if __name__ == '__main__':
    run()

