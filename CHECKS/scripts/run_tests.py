#!/usr/bin/env python
import os
import nose
import sys
import argparse

helptext = ('This script is a wrapper for running the TDSEsolver tutorial nosetests. The tests can also be run via '
            ' the standard \'nosetests\' command  with the optional --nocapture option.')

def run(dataDir,kind):

    info='specify test numbers as in "-t 0 3 11 12"'
    arg_parser = argparse.ArgumentParser(
        description=helptext)
    arg_parser.add_argument('-t', '--test', type=str, nargs='*',help=info)
    command_line_args = arg_parser.parse_args()

    tests = []

    if command_line_args.test:
        for test in command_line_args.test:
            try:
                test_number = int(test)
                if test_number < 10:
                    tests.append('CHECKS/testList.py:test_0' + str(test_number))
                else:
                    tests.append('CHECKS/testList.py:test_' + str(test_number))

            except ValueError:
                sys.exit('Value not recognized: '+test+' (should be integer)')
    else: sys.exit("\nNo tests given, "+info);

    noseargv = ['dummyfilename', '--nocapture', '--with-xunit']
    if len(tests) > 0:
        noseargv.append('--tests')
        noseargv.append(','.join(tests))

    nose.main(argv=noseargv)
