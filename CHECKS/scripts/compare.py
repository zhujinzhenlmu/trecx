import subprocess
import os
import shutil
from numpy import loadtxt
import numpy as np
from numpy.testing import assert_array_almost_equal
from nose.tools import assert_in
import filecmp
from _io import StringIO

DEFAULT_DIGITS_ACCURACY = 10

def setPath(which):
    global dirpath,base_path,master_path,inputs
    dirpath = os.path.dirname(os.path.realpath(__file__))
    base_path = os.path.abspath(os.path.join(dirpath, os.pardir, os.pardir))
    master_path = dirpath + '/../master/'+which+'/'
    inputs = base_path + '/'+which+'/'

def test(which,inputfile,comparison,parameters=""):
    setPath(which)
    run_tutorial(inputfile)
    if parameters=="": comparison(inputfile)
    else:              comparison(inputfile,parameters)

def testSpectrum(which,inputfile,parameters="",decimals=6,kind="partial"):
    test(which,inputfile,compare_outf)
    if  kind!="partial": run_spectrum(inputfile,"100",kind)
    elif parameters!="": run_spectrum(inputfile,parameters,kind)
    else:                run_spectrum(inputfile,"100",kind)
    compare_spectrum(inputfile,decimals)

def test2DSpectrum(which,inputfile,parameters="",decimals=6):
	setPath(which)
	para_tutorial(inputfile)
        subprocess.call(['cp', '%s/%s/0000/inpc-0' % (master_path, inputfile), '%s/%s/0000/inpc' % (which, inputfile)])
	run_tutorial('%s/0000/inpc' % inputfile)
        subprocess.call(['cp', '%s/%s/0000/inpc-1' % (master_path, inputfile), '%s/%s/0000/inpc' % (which, inputfile)])
	para_tutorial('%s/0000/inpc' % inputfile)
	run_spectrum(inputfile,100,"partial")
	compare_spectrum(inputfile,decimals)

def para_tutorial(inputfile):
    """Run the specified inputfile with the tRecX executable."""
    print '\nRunning inputfile ' + inputfile
    filepath = inputs + inputfile
    if not (len(filepath) > 4 and filepath[-4:] == 'inpc'):
        filepath = inputs + inputfile + '.inp'
    if filepath[-1] != 'c':
        if not files_equal(filepath, master_path + inputfile + '/0000/inpc'):
            warn('Inputfile ' + inputfile + ' appears to have changed.')
        remove_directory(inputs + inputfile)
	print 'mpirun -np 2 ' + base_path + '/tRecX'
    p = subprocess.Popen('mpirun -np 2 ' + base_path + '/tRecX ' + filepath, stdout=subprocess.PIPE, shell=True)

    stdout, stderr = p.communicate()


def warn(message):
    print('WARNING: ' + message)


def remove_directory(path):
    if os.path.isdir(path+"/0000"):
        shutil.rmtree(path+"/0000")


def files_equal(file1, file2):
    return filecmp.cmp(file1, file2, shallow=False)


def run_tutorial(inputfile):
    """Run the specified inputfile with the tRecX executable."""
    print '\nRunning inputfile ' + inputfile
    filepath = inputs + inputfile
    if not (len(filepath) > 4 and filepath[-4:] == 'inpc'):
    	filepath = inputs + inputfile + '.inp'
    if filepath[-1] != 'c':
    	if not files_equal(filepath, master_path + inputfile + '/0000/inpc'):
        	warn('Inputfile ' + inputfile + ' appears to have changed.')
        remove_directory(inputs + inputfile)
    subprocess.call([base_path + '/tRecX', filepath], stdout=open(os.devnull, 'wb'))


def run_spectrum(inputfile,count,kind):
    """Run the Spectrum executable on the specified outputfile.
    -nR=100 is specified to produce meaningful results."""
    outputfolder = inputs + inputfile + '/0000'
    print('Running Spectrum on folder ' + outputfolder+' with '+str(count)+' points and plot='+kind)
    subprocess.call([base_path + '/tRecX', outputfolder + '/inpc', '-nR='+str(count), '-plot='+kind], stdout=open(os.devnull, 'wb'))
    subprocess.call([base_path + '/tRecX', outputfolder + '/inpc', '-nR='+str(count), '-plot='+kind], stdout=open(os.devnull, 'wb'))

def compare_eigenvalues(inputfile, decimals=6):
    """Compare the calculated eigenvalues with the master values."""
    output_values = loadtxt(inputs + inputfile + '/0000/eig', unpack=True, delimiter=', ')
    master_values = loadtxt(master_path + inputfile + '/0000/eig', unpack=True, delimiter=', ')
    assert_array_almost_equal(output_values[0], master_values[0], decimals, 'Real parts of eig do not match.')
    assert_array_almost_equal(output_values[1], master_values[1], decimals, 'Imaginary parts of eig do not match.')


def compare_column(column, values, master, error_message, decimals=4):
    """Helper function, compares comlumns in values and master."""
    assert_array_almost_equal(values[column], master[column], decimals, error_message)


def compare_outf(inputfile, columns=[3, 4, 5], decimals=DEFAULT_DIGITS_ACCURACY):
    """Compare the specified columns on the default outputfiles with the master values.
    Columns are 0-based. The first column is the time column which is always checked."""
    output_filename = inputs + inputfile + '/0000/outf'
    master_filename = master_path + inputfile + '/0000/outf'

    propagation_header = '=  TIME PROPAGATION'

    def parse_outf(filename):
        with open(filename, 'r') as file:
            filecontent = file.read()

        assert_in(propagation_header, filecontent, '\nHeader before time propagation seems to be missing in ' + filename +
               '\nPlease check if outf contains header ' + propagation_header)
        splitfile = filecontent.split(propagation_header)

        # advance to actual beginning of time-propagation, signature: line containing 'CPU' and followed by line containing '----'
        lines= splitfile[-1].splitlines()
        for k in range(len(lines)-1):
            if lines[k+1].find("----")!=-1 and lines[k].find("CPU")!=-1:break;
        lines=lines[k+2:]

        relevant_part = ''
        for line in lines:
            if line.strip() == '':
                break
            else:
                relevant_part += line

        try:  # Python 2 needs unicode for StringIO
            file_mock = StringIO(unicode(relevant_part, "utf-8"))
        except NameError:
            file_mock = StringIO(relevant_part)
        values = loadtxt(file_mock, unpack=True)
        return values

    output_values = parse_outf(output_filename)
    master_values = parse_outf(master_filename)

    compare_column(2, output_values, master_values,
                   'WARNING: Propagation timesteps seem to have changed. Please check expectation values by hand.')
    for column in columns:
        compare_column(column, output_values, master_values, 'Outf values in column ' + str(column + 1) +
                       ' do not match.\n Files:\n' + output_filename + '\n' + master_filename)


def compare_expec(inputfile, columns=[2, 3, 4], decimals=DEFAULT_DIGITS_ACCURACY):
    """Compare the specified columns on the expec outputfiles with the master values.
    Columns are 0-based. The first column is the time column which is always checked."""
    output_filename = inputs + inputfile + '/0000/expec'
    master_filename = master_path + inputfile + '/0000/expec'
    output_values = loadtxt(output_filename, unpack=True)
    master_values = loadtxt(master_filename, unpack=True)

    compare_column(0, output_values, master_values,
                   'WARNING: Propagation timesteps seem to have changed. Please check expectation values by hand.')
    for column in columns:
        compare_column(column, output_values, master_values, 'Expec values in column ' + str(column + 1) +
                       ' do not match.\n Files:\n' + output_filename + '\n' + master_filename)


def compare_spectrum(inputfile, decimals=6):
    """Compare the calculated spectrum for the last run with the master values."""
    # The trailing comma causes loadtxt to fail if comlumns are not specified
    with open(inputs + inputfile + '/0000/spec', 'r') as file:
        for line in file:
            if not line.startswith('#'):
                number_of_columns = line.count(',')
                if not line.strip().endswith(','): number_of_columns+=1
                break

    output_values = loadtxt(inputs + inputfile + '/0000/spec', unpack=True, delimiter=', ',
                             usecols=range(number_of_columns))
    master_values = loadtxt(master_path + inputfile + '/0000/spec', unpack=True, delimiter=', ',
                            usecols=range(number_of_columns))

    assert_array_almost_equal(output_values[0], master_values[0], decimals, 'Real parts of spec do not match.')
    maxsize=0
    for i in range(1, number_of_columns): maxsize=max(maxsize,max(master_values[i]))
    dec=int(min(-np.log(maxsize)/np.log(10.)+decimals,12))
    for i in range(1, number_of_columns):
        assert_array_almost_equal(output_values[i], master_values[i],dec, 'Spectral values in column ' +
                                  str(i + 1) + ' do not match.')


