#!/usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
# -*- coding: utf-8 -*-

from math import sqrt
from math import log

speedOfLight=2.997924e2
t0FWHM_fs=40.

xi=6.
Dw=100.

t0_fs=t0FWHM_fs/sqrt(2*log(2))

xi0=-0.5*t0_fs*t0_fs/Dw

rXi=xi/xi0

tp_fs=t0_fs*sqrt(1+rXi*rXi)


tpFWHM_fs=tp_fs*(sqrt(2*log(2)))

print tpFWHM_fs

chirp=-rXi/(t0_fs*t0_fs*(rXi*rXi+1))

wavelength_nm=395

w=2*3.1415*speedOfLight/wavelength_nm

wIhalf=w*(1+chirp*tpFWHM_fs)

print tpFWHM_fs,w,wIhalf,chirp*tpFWHM_fs/2.
