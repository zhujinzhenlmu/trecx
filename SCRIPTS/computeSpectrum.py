# #!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
#from scipy.interpolate import interp1d
from scipy.special import sph_harm
#from matplotlib import gridspec
import sys
import os
import re
import struct
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 20}
import matplotlib
matplotlib.rc('font', **font)

def computeSpectrum( folder ):
    WRITE_SPECTRUM = True
    PLOT_SI_SPECTRUM = True
    PLOT_DI_JAD = True
    PLOT_STRIPE_JAD = False
    if 'stripe_JAD' in options.keys() and options['stripe_JAD']:
        PLOT_STRIPE_JAD = True
    absMomenta,lmax,appearingM,DIspecFiles,SIspecFiles = loadBasicData(folder)
    print 'mmax:', -appearingM[0],' lmax:',lmax
    
    numberOfAngles = 64
    JADangles = rangeDouble(0, np.pi, np.pi/(numberOfAngles-1))
    try:
        E_select1 = float(options['E1']) / energy_factor
        E_select2 = float(options['E2']) / energy_factor
    except:
        print 'no need to plot JAD with input energy'
    if PLOT_DI_JAD:
        if os.path.isfile(getJADSpectrumFilename(folder,0)):
            JADspectrum = np.loadtxt(getJADSpectrumFilename(folder,0))
            thetagrid = np.loadtxt(getJADthetaGridFilename(folder,0))
            E1 = []; E1.append(np.loadtxt(getJADenergiesFilename(folder,0,1)))
            E2 = []; E2.append(np.loadtxt(getJADenergiesFilename(folder,0,2)))
            # JADspectrumBase = np.copy(JADspectrum)
            if 'compare_folder' in options.keys():
                contents = get_input_contents('./DI/' + str(options['compare_folder']))
                label = '$R_c$=' + str(contents['scaling rad']) + ',$I$=' + str(contents['intensity']) + ',$\lambda$=' + str(contents['wavelength'])
                plotJAD(thetagrid, JADspectrum, E1[0], E2[0], single_angle=True, label=label)
            else:
                if 'fix_one_angle' in options.keys():
                    plotJAD(thetagrid, JADspectrum, E1[0], E2[0], single_angle=False, fix_one_angle=float(options['fix_one_angle']))
                else:
                    plotJAD(thetagrid, JADspectrum, E1[0], E2[0], single_angle=False)
            PLOT_DI_JAD = False
            if 'compare_folder' in options.keys() and options['compare_folder'] and 'compare' in options.keys() and \
                    options['compare']:
                compares = options['compare'].split('+')
                for compare in compares:
                    print folder + '/' + compare
                    JADspectrumCompare = np.loadtxt(getJADSpectrumFilename(folder + '/' + compare + '/', 0))
                    JADmax = np.max(JADspectrumCompare)
                    angleCap = JADmax * 0.01
                    JADspectrum[JADspectrumCompare < angleCap] = angleCap
                    anglenormalize = 1.0 / JADmax
                    JADspectrumCompare *= anglenormalize;
                    if len(compares) == 1:
                        JADspectrumCompare = abs(JADspectrumCompare - JADspectrum)
                    E1_compare = []
                    E1_compare.append(np.loadtxt(getJADenergiesFilename(folder + compare + '/', 0, 1)))
                    E2_compare = []
                    print E1_compare[0]
                    E2_compare.append(np.loadtxt(getJADenergiesFilename(folder + compare + '/', 0, 2)))
                    print E2_compare[0]
                    # if len(compares) == 1:
                    #     plotJAD(thetagrid, JADspectrumCompare, E1_compare[0], E2_compare[0], use_compare='comparision', figure_index=3, get_norm=False)
                    # else:
                    contents = get_input_contents('./' + str(folder))
                    label = '$R_c$=' + str(contents['scaling rad']) + ',$I$=' + str(
                        contents['intensity']) + ',$\lambda$=' + str(contents['wavelength'])
                    single_angle = False
                    get_norm = False
                    if 'single_angle' in options.keys():
                        single_angle = True
                        get_norm = True
                    plotJAD(thetagrid, JADspectrumCompare, E1_compare[0], E2_compare[0], use_compare='comparision', get_norm=get_norm, figure_index=3, single_angle=single_angle, label=label)
        else:
            JADspectrum = np.zeros([len(JADangles) * 2 - 1, len(JADangles) * 2 - 1], dtype=np.double)
            E1 = [E_select1];
            E2 = [E_select2]  # from inputl
            sphericalharmonics1plus , sphericalharmonics2plus , sphericalharmonics1minus , sphericalharmonics2minus = initializeSphericalHarmonics(lmax, JADangles, appearingM);
        thetaPicIndecies, E1, E2 = transformThetaPicEnergiesToIndecies(absMomenta**2*0.5, E1, E2);
    if PLOT_STRIPE_JAD:
        if os.path.isfile(getStripeJADSpectrumFilename(folder, 0)):
            stripeJADspectrum = np.loadtxt(getStripeJADSpectrumFilename(folder, 0))
            thetagrid = np.loadtxt(getStripeJADthetaGridFilename(folder, 0))
            E1 = [];
            E1.append(np.loadtxt(getStripeJADenergiesFilename(folder, 0, 1)))
            E2 = [];
            E2.append(np.loadtxt(getStripeJADenergiesFilename(folder, 0, 2)))
            # JADspectrumBase = np.copy(JADspectrum)
            plotJAD(thetagrid, stripeJADspectrum, E1[0], E2[0], figure_index=8, stripe=True)
            PLOT_STRIPE_JAD = False
        else:
            sphericalharmonics1plus , sphericalharmonics2plus , sphericalharmonics1minus , sphericalharmonics2minus = initializeSphericalHarmonics(lmax, JADangles, appearingM);
    mymax = len(absMomenta)
    #mymax = 32     
            
    if os.path.isfile(getTotalDISpectrumFilename(folder)):
        totalSpectrum = np.loadtxt(getTotalDISpectrumFilename(folder))
        if 'compare_DI' in options.keys():
            totalSpectrum_compare = np.loadtxt(getTotalDISpectrumFilename('/'.join(folder.split('/')[:-2]) + '/' + str(options['compare_DI']) + '/'))
            totalSpectrum = abs(totalSpectrum - totalSpectrum_compare) / max(totalSpectrum.max(), totalSpectrum_compare.max())
    else:
        totalSpectrum = np.zeros([mymax,mymax], dtype=np.double)
    if 'stripe_JAD' in options.keys() and options['stripe_JAD'] and options['range_stripe'] and PLOT_STRIPE_JAD:
        low_energy = float(options['stripe_JAD']) - float(options['range_stripe'])
        high_energy = float(options['stripe_JAD']) + float(options['range_stripe'])
        E = absMomenta ** 2
        low_index = len(np.where(E < low_energy)[0])
        high_index = len(np.where(E < high_energy)[0])
        stripeScatteringAmplitudeThetaGrid = np.zeros([len(JADangles) * 2 - 1, len(JADangles) * 2 - 1],
                                                      dtype=np.complex)
        for k1index in range(mymax):
            for k2index in range(k1index, mymax):
                if k1index + k2index >= low_index and k1index + k2index <= high_index:
                    for mindex in range(len(appearingM)):
                        # phi momentum m
                        m = appearingM[mindex]
                        lmaxLocal = lmax - abs(m)
                        scatteringAmplitudeFixedMFixedKs = readInDIdata(lmaxLocal, DIspecFiles[mindex],
                                                                        DIspecFiles[len(appearingM) - 1 - mindex],
                                                                        len(absMomenta), k1index, k2index)
                        stripeScatteringAmplitudeThetaGrid += getThetaScatteringAmplitude(JADangles,
                                                                                        scatteringAmplitudeFixedMFixedKs,
                                                                                        sphericalharmonics1plus,
                                                                                        sphericalharmonics2plus,
                                                                                        sphericalharmonics1minus,
                                                                                        sphericalharmonics2minus,
                                                                                        mindex)
        stripeJADspectrum = abs(stripeScatteringAmplitudeThetaGrid) ** 2
        thetagrid = getThetaGrid(JADangles)
        saveStripeJAD(folder, thetagrid, stripeJADspectrum, [low_energy], [high_energy])
        plotJAD(thetagrid, stripeJADspectrum, low_energy, high_energy, figure_index=8, stripe=True)

    if somethingToDo(folder,PLOT_DI_JAD):
        if PLOT_DI_JAD:
            scatteringAmplitudeThetaGrid = np.zeros([len(JADangles) * 2 - 1, len(JADangles) * 2 - 1], dtype=np.complex)
        for k1index in range(mymax):
            for k2index in range(k1index, mymax):
                # kgrid of momentum
                # if PLOT_DI_JAD and (thetaPicIndecies[k1index, k2index] or thetaPicIndecies[k2index, k1index]):
                #     scatteringAmplitudeThetaGrid = np.zeros([len(JADangles)*2-1,len(JADangles)*2-1], dtype=np.complex)

                for mindex in range(len(appearingM)):
                # for mindex in [1, 2, 3]:

                    # phi momentum m
                    m = appearingM[mindex]
                    lmaxLocal = lmax-abs(m)
                    if not os.path.isfile(getTotalDISpectrumFilename(folder)) or (PLOT_DI_JAD and (thetaPicIndecies[k1index,k2index] or thetaPicIndecies[k2index,k1index])):
                        scatteringAmplitudeFixedMFixedKs = readInDIdata(lmaxLocal, DIspecFiles[mindex],DIspecFiles[len(appearingM)-1-mindex], len(absMomenta), k1index, k2index)
                    
                        setTotalSpectrum(totalSpectrum, k1index, k2index,scatteringAmplitudeFixedMFixedKs)
                    if PLOT_DI_JAD and (thetaPicIndecies[k1index,k2index] or thetaPicIndecies[k2index,k1index]):
                        print "the position is" + str(k1index) + ", and " + str(k2index)
                        scatteringAmplitudeThetaGrid += getThetaScatteringAmplitude(JADangles, scatteringAmplitudeFixedMFixedKs, sphericalharmonics1plus,
                                                                                     sphericalharmonics2plus, sphericalharmonics1minus, sphericalharmonics2minus, mindex)
            if len(absMomenta) > 8 and k1index % 8 == 0: print k1index

                        # if PLOT_DI_JAD and (thetaPicIndecies[k1index, k2index] or thetaPicIndecies[k2index, k1index]):
        JADspectrum += abs(scatteringAmplitudeThetaGrid)**2
        if 'new_harm' not in options.keys():
            thetagrid = getThetaGrid(JADangles)
            saveJAD(folder, thetagrid, JADspectrum, E1, E2)
            plotJAD(thetagrid, JADspectrum, E1[0], E2[0])

        if not os.path.isfile(getTotalDISpectrumFilename(folder)):
            saveTotalSpectrum(folder, totalSpectrum)
    
    plotTotalSpectrum(absMomenta[0:mymax], totalSpectrum)
    
    if PLOT_SI_SPECTRUM:
        plotSIspectrum(absMomenta, SIspecFiles, lmax, folder, WRITE_SPECTRUM)
        
def somethingToDo(folder,PLOT_DI_JAD):
    if not os.path.isfile(getTotalDISpectrumFilename(folder)): return True
    if not os.path.isfile(getJADSpectrumFilename(folder,0)): return True
    return False
    
def saveJAD(folder,thetagrid,JADspectrum,E1,E2):
    np.savetxt(getJADthetaGridFilename(folder,0), thetagrid)
    np.savetxt(getJADSpectrumFilename(folder,0), JADspectrum)
    np.savetxt(getJADenergiesFilename(folder,0,1), E1)
    np.savetxt(getJADenergiesFilename(folder,0,2), E2)

def saveStripeJAD(folder,thetagrid,JADspectrum,E1,E2):
    np.savetxt(getStripeJADthetaGridFilename(folder,0), thetagrid)
    np.savetxt(getStripeJADSpectrumFilename(folder,0), JADspectrum)
    np.savetxt(getStripeJADenergiesFilename(folder,0,1), E1)
    np.savetxt(getStripeJADenergiesFilename(folder,0,2), E2)

def plotJAD(thetagrid,JADspectrum,E1,E2, use_compare='', figure_index=1, get_norm=True, stripe=False, single_angle=False, label=False, fix_one_angle=False):
    JADmax = np.max(JADspectrum)
    print 'JAD max is %.2e'% JADmax
    print 'JAD min is %.2e'% np.min(JADspectrum)
    if get_norm or 'write_fix_one_angle' in options.keys():
        angleCap = JADmax * 0.01
        newJADspectrum = JADspectrum.copy()
        if not fix_one_angle:

#        if not fix_one_angle or 'write_fix_one_angle' in options.keys():
            JADspectrum[JADspectrum < angleCap] = angleCap
            anglenormalize = 1.0/JADmax
        else:
            anglenormalize = 1
        JADspectrum *= anglenormalize
        newJADspectrum *= anglenormalize
    else:
        min_difference = 0.00001
        JADspectrum[JADspectrum < min_difference] = min_difference
        JADspectrum[JADspectrum > 1] = 1
    if not fix_one_angle:
        if not single_angle:
            plt.figure(figure_index)
            values_of_spectrum = []
            if get_norm:
                fig = plt.contourf(thetagrid*180/np.pi, thetagrid*180/np.pi, np.log10(JADspectrum), 40)
            else:
                print 'This is the time when we do not use logscale'
                fig = plt.contourf(thetagrid * 180 / np.pi, thetagrid * 180 / np.pi, JADspectrum, 40)
                print "The maxmum value of the comparision is: " + str(JADmax)
            plt.rc('text', usetex=True)
            plt.xlabel(r"$\theta_1$ (deg)")
            plt.ylabel(r"$\theta_2$ (deg)")

            if not stripe:
                title = r"$(E_1,E_2)=($" + "%.2f" % (E1 * energy_factor) + r"$,$" + "%.2f" % (E2 * energy_factor) + r"$)$" + use_compare
                if 'energy_range_ev' in options.keys():
                    title += '$\Delta E=$' + options['energy_range_ev'] + 'ev'
                plt.title(title)
            else:
                title = r"$E_1+E_2$from" + "%.2f" % (E1 * energy_factor) + r" to " + "%.2f" % (E2 * energy_factor) + use_compare
                if 'energy_range_ev' in options.keys():
                    title += '$\Delta E=$' + options['energy_range_ev'] + 'ev'
                plt.title(title)
            plt.axis('equal')
            plt.xlim(0, 360)
            plt.ylim(0, 360)
            plt.colorbar(fig)
        if single_angle:
            plt.figure(4)
            sum_value = {}
            for i in range(len(thetagrid)):
                for j in range(i, len(thetagrid)):
                    summation_angle = str(thetagrid[i] + thetagrid[j])
                    if summation_angle not in sum_value:
                        sum_value[summation_angle] = 0
                    if get_norm:
                        sum_value[summation_angle] += newJADspectrum[i][j] * 2
                    else:
                        sum_value[summation_angle] += JADspectrum[i][j] * 2
            sum_theta = sorted([float(key) for key in sum_value.keys()])
            sum_spectrum = [float(sum_value[str(key)]) for key in sum_theta]
            plt.xlabel(r"summation of $\theta$")
            plt.ylabel(r"yeild")
            title = r"$(E_1,E_2)=($"+ "%.2f" % (E1 * energy_factor) + r"$,$"+ "%.2f" % (E2 * energy_factor) + r"$)$" + use_compare
            if 'energy_range_ev' in options.keys():
                title += '$\Delta E=$' + options['energy_range_ev'] + 'ev'
            plt.title(title)
            plt.plot(np.array(sum_theta) / np.pi * 180, sum_spectrum, label=label)
            plt.legend()
            plt.show(block=False)
    else:

        plt.figure(figure_index)
        if 'write_fix_one_angle' not in options.keys():
            angles = [15, 45, 75]
        else:
            angles = [0, 30, 60, 90, 135, 150]
        from matplotlib.ticker import FormatStrFormatter
        errors = []
        print 'The theta girds are'
        for angle in angles:
            # angle1 = float(fix_one_angle)
            if 355 > angle > 5:
                index1 = np.where(np.array(thetagrid) < (angle - 5) / 180. * np.pi)[0][-1]
                index2 = np.where(np.array(thetagrid) < (angle + 5) / 180. * np.pi)[0][-1]
                other_JAD = sum(JADspectrum[index1: index2 + 1, :] / (index2 - index1))
            elif 0 <= angle <= 5:
                index1 = np.where(np.array(thetagrid) <= angle / 180. * np.pi)[0][-1]
                index2 = np.where(np.array(thetagrid) < (350 + angle) / 180. * np.pi)[0][-1]
                other_JAD = sum(JADspectrum[:index1, :] / (index2 - index1)) + \
                                sum(JADspectrum[index2:, :] / (index2 - index1))
            else:
                index1 = np.where(np.array(thetagrid) < angle / 180. * np.pi)[0][-1]
                index2 = np.where(np.array(thetagrid) < (angle - 350) / 180. * np.pi)[0][-1]
                other_JAD = sum(JADspectrum[index1:, :] / (index2 - index1)) + \
                                sum(JADspectrum[:index2, :] / (index2 - index1))
            axis = plt.subplot(int('1' + str(len(angles)) + '1') + angles.index(angle), projection='polar')
            axis.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))
            theta_e1 = np.ones(len(thetagrid)) * angle * np.pi / 180
            if other_JAD.max() != other_JAD.min():
                JAD_e1 = np.array(range(len(thetagrid))) * (other_JAD.max() - other_JAD.min()) / len(other_JAD) + np.ones(len(thetagrid)) * other_JAD.min()
            else:
                JAD_e1 = np.array(range(len(thetagrid))) * other_JAD.max() / len(other_JAD)
            axis.plot(theta_e1, JAD_e1, label='e1')
            axis.plot(thetagrid, other_JAD, 'o', label='e2')
            if 'write_fix_one_angle' in options.keys():
                angle_file = open(sys.argv[1] + '/angleDistribution_%i.txt'%angle, 'w+')
                angle_file.write('# angle, yield \n')
                for m in range(len(thetagrid)):
                    angle_file.write('%5f,%4e \n' % (thetagrid[m], other_JAD[m]))
                angle_file.close()
            if other_JAD.max() != other_JAD.min():
                axis.set_rmin(other_JAD.min())
            axis.set_rticks([0, other_JAD.max()])
            # axis.set_rticks([other_JAD.min(), other_JAD.max()])
            axis.set_theta_direction(-1)
            axis.set_theta_offset(np.pi/2.0)
            first_half = other_JAD[1:len(other_JAD) / 2 + 1]
            second_half = other_JAD[len(other_JAD) / 2 + 1:]
            second_half = second_half[::-1]
            errors.append([thetagrid[1:len(other_JAD) / 2 + 1], first_half, thetagrid[len(other_JAD) / 2 + 1:] - np.pi, second_half])
        plt.subplots_adjust(wspace=.5)

        axis.set_rlabel_position(22.5)
        from constants import Ry2, e
        title = 'JAD at $E_1=%.2feV,E_2=%.2feV,|E_1-E_2|=%.2feV$'%(Ry2 * E1 / e, Ry2 * E2 / e, Ry2 * abs(E1 - E2) / e)
        if 'energy_range_ev' in options.keys():
            title += '$\Delta E=$' + options['energy_range_ev'] + 'ev'
        plt.title(title, y=1.08, x=-1.08)
        axis.grid(False)
        plt.legend(loc=9)

        plt.figure(10)
        for i in range(len(errors)):
            item = errors[i]
            plt.plot(item[0] * 180.0 / np.pi, item[1], label='$\\theta_1=' + str(angles[i]) + '^{\\circ}, \\theta=\\theta_2$')
            plt.plot(item[2] * 180.0 / np.pi, item[3], label='$\\theta_1=' + str(angles[i]) + '^{\\circ}, \\theta=360^{\\circ}-\\theta_2$')
        plt.xlabel('$\\theta(^{\\circ})$')
        plt.ylabel('$JAD(\\theta_1=\\Theta, \\theta)$')
        plt.title('$E_1=%.2f eV,E_2=%.2f eV$'%(E1 * energy_factor, E2 * energy_factor))
        plt.legend()


def get_input_contents(path):
    keys = ['intensity', 'n_cycles', 'wavelength', 'pulseshape', 'scaling rad', 'tsurff rad', 'n_leg_interv',
            'order_legend', 'order_laguer', 'lag_expon', 'scale', 'angle', 'lmax', 'mmax', 'propag time',
            'average', 'pot cur start', 'pot cut end', 'RK tolerance', 'charge', 'lobattomax',
            'lambdamax', 'bin', 'hydro', 'folderNum', 'lmaxV', 'P1', 'P2', 'HighEV', 'Up', 'Nk', 'kNum']
    values = open(path + '/input_arguments.txt').readlines()[0].split()
    return dict(zip(keys, values))


def getThetaScatteringAmplitude(JADangles, scatteringAmplitudeFixedMFixedKs, sphericalharmonics1plus,
                                sphericalharmonics2plus, sphericalharmonics1minus, sphericalharmonics2minus, mindex):

    scatteringAmplitudeThetaFixedk = np.zeros([len(JADangles)*2-1, len(JADangles)*2-1], dtype=np.complex)
    if 'same_m' not in options.keys():
        temp = np.dot(scatteringAmplitudeFixedMFixedKs, sphericalharmonics1minus[mindex])
        scatteringAmplitudeThetaFixedk[0:len(JADangles), 0:len(JADangles)] = np.dot(temp.transpose(), sphericalharmonics1plus[mindex])
        if 'add_ham' in options.keys():
            temp1 = np.dot(scatteringAmplitudeFixedMFixedKs, sphericalharmonics8minus[mindex])
            scatteringAmplitudeThetaFixedk[0:len(JADangles), 0:len(JADangles)] += np.dot(temp1.transpose(),
                                                                                        sphericalharmonics8plus[mindex])
        temp2 = np.dot(temp.transpose(), sphericalharmonics2plus[mindex])
        scatteringAmplitudeThetaFixedk[len(JADangles):, 0:len(JADangles)] = temp2[-1:0:-1, :]
        temp = np.dot(scatteringAmplitudeFixedMFixedKs, sphericalharmonics2minus[mindex])
        temp2 = np.dot(temp.transpose(), sphericalharmonics1plus[mindex])
        scatteringAmplitudeThetaFixedk[0:len(JADangles), len(JADangles):] = temp2[:, -1:0:-1]

        temp2 = np.dot(temp.transpose(), sphericalharmonics2plus[mindex])
        scatteringAmplitudeThetaFixedk[len(JADangles):, len(JADangles):] = temp2[-1:0:-1, -1:0:-1]
    else:
        temp = np.dot(scatteringAmplitudeFixedMFixedKs, sphericalharmonics1plus[mindex])
        scatteringAmplitudeThetaFixedk[0:len(JADangles), 0:len(JADangles)] = np.dot(temp.transpose(),
                                                                                    sphericalharmonics1plus[mindex])
        temp2 = np.dot(temp.transpose(), sphericalharmonics2plus[mindex])
        scatteringAmplitudeThetaFixedk[len(JADangles):, 0:len(JADangles)] = temp2[-1:0:-1, :]

        temp = np.dot(scatteringAmplitudeFixedMFixedKs, sphericalharmonics2plus[mindex])
        temp2 = np.dot(temp.transpose(), sphericalharmonics1plus[mindex])
        scatteringAmplitudeThetaFixedk[0:len(JADangles), len(JADangles):] = temp2[:, -1:0:-1]

        temp2 = np.dot(temp.transpose(), sphericalharmonics2plus[mindex])
        scatteringAmplitudeThetaFixedk[len(JADangles):, len(JADangles):] = temp2[-1:0:-1, -1:0:-1]
    return scatteringAmplitudeThetaFixedk

def setTotalSpectrum(totalSpectrum,k1index,k2index,scatteringAmplitudeFixedMFixedKs):
    spectrumFixedMFixedKs = abs(scatteringAmplitudeFixedMFixedKs)**2
    totalSpectrum[k1index,k2index] += np.sum(spectrumFixedMFixedKs)
    totalSpectrum[k2index,k1index] = totalSpectrum[k1index,k2index]
    
def getThetaGrid(JADangles):
    thetagrid = np.zeros(len(JADangles)*2-1)
    thetagrid[0:len(JADangles)] = JADangles
    thetagrid[len(JADangles):] = JADangles[1:]+np.pi
    return thetagrid

    
def initializeSphericalHarmonics(lmax,JADangles,appearingM):
    sphericalharmonics1plus = [] # exp(+m) phi=0
    sphericalharmonics2plus = [] # exp(+m) phi=pi
    sphericalharmonics1minus = [] # exp(-m) phi=0
    sphericalharmonics2minus = [] # exp(-m) phi=pi

    for mindex in range(len(appearingM)):
        m = appearingM[mindex]
        sphericalharmonics1plus.append(np.zeros([lmax-abs(m)+1, len(JADangles)], dtype=np.double))
        sphericalharmonics2plus.append(np.zeros([lmax-abs(m)+1, len(JADangles)], dtype=np.double))
        sphericalharmonics1minus.append(np.zeros([lmax-abs(m)+1, len(JADangles)], dtype=np.double))
        sphericalharmonics2minus.append(np.zeros([lmax-abs(m)+1, len(JADangles)], dtype=np.double))
        if 'add_ham' in options.keys():
            sphericalharmonics8plus.append(np.zeros([lmax-abs(m)+1, len(JADangles)], dtype=np.double))
            sphericalharmonics8minus.append(np.zeros([lmax-abs(m)+1, len(JADangles)], dtype=np.double))
        for i in range(len(JADangles)):
            for varl in range(lmax-abs(m)+1):
                sphericalharmonics1plus[-1][varl, i] = np.real(sph_harm(m, varl + abs(m), 0, JADangles[i]))
                sphericalharmonics2plus[-1][varl, i] = np.real(sph_harm(m, varl + abs(m), np.pi, JADangles[i]))
                sphericalharmonics1minus[-1][varl, i] = np.real(sph_harm(-m, varl + abs(m), 0, JADangles[i]))
                sphericalharmonics2minus[-1][varl, i] = np.real(sph_harm(-m, varl + abs(m), np.pi, JADangles[i]))
                if 'add_ham' in options.keys():
                    sphericalharmonics8plus[-1][varl, i] = np.real(sph_harm(m, varl + abs(m), 0.5 * np.pi, JADangles[i]))
                    sphericalharmonics8minus[-1][varl, i] = np.real(sph_harm(-m, varl + abs(m), 0.5 * np.pi, JADangles[i]))
    return sphericalharmonics1plus, sphericalharmonics2plus, sphericalharmonics1minus, sphericalharmonics2minus
    
def transformThetaPicEnergiesToIndecies(Eofk,E1,E2):
    thetaPicIndecies = np.zeros([len(Eofk), len(Eofk)], dtype=np.int);
    E1temp = E1;
    E2temp = E2;
    for angleindex in range(len(E1)):
        if 'energy_range_ev' in options.keys():
            energy_range = float(options['energy_range_ev']) / energy_factor
            i1_min = getIndecies(Eofk, E1[angleindex] - energy_range)
            i1_max = getIndecies(Eofk, E1[angleindex] + energy_range)
            i2_min = getIndecies(Eofk, E2[angleindex] - energy_range)
            i2_max = getIndecies(Eofk, E2[angleindex] + energy_range)
            thetaPicIndecies[i1_min: i1_max + 1, i2_min: i2_max + 1] = 1
            print 'i1_min:%.1f ,i1_max: %.1f, i2_min:%.1f, i2_max%.1f' % (i1_min, i1_max, i2_min, i2_max)
        i1 = getIndecies(Eofk, E1[angleindex]);
        i2 = getIndecies(Eofk, E2[angleindex]);
        thetaPicIndecies[i1,i2] = 1;
        E1temp[angleindex] = Eofk[i1];
        E2temp[angleindex] = Eofk[i2];
    E1 = E1temp;
    E2 = E2temp;
    return thetaPicIndecies, E1, E2
    
def getIndecies(Eofk,E):
    return np.argmin(abs(Eofk-E));

    
def loadBasicData(folder):
    appearingM = getMs(folder)
    filelist = os.listdir(folder)
    #print filelist
    computedMomenta = getComputedMomenta(filelist)
    DIspecFiles = getDISpecFiles(filelist,folder,appearingM)
    lmax = getLmax(DIspecFiles,appearingM)
    absMomenta = getMomentumGrid(computedMomenta,DIspecFiles)
    saveMomenta(folder,absMomenta)
    SIspecFiles = getSingleIonizationFiles(filelist,folder)
    return absMomenta,lmax,appearingM,DIspecFiles,SIspecFiles

    
def plotSIspectrum(absMomenta, SIspecFiles, lmax, folder, WRITE_SPECTRUM):
    SIdata = np.zeros([len(absMomenta),lmax+1], dtype=np.double)
    basis_name = SIspecFiles[0].split('testspec_')[0] + 'testspec_'
    for i in range(len(absMomenta)):
        try:
            temp = np.loadtxt(basis_name + str(i) + '.txt')
            SIdata[i,:] = temp[1:]
        except:
            SIdata[i, :] = SIdata[i - 1, :]
    #lmax+1 equals all the angular momentums
    totalSI = np.sum(SIdata, 1)
    Energies = 0.5*absMomenta**2
    plt.figure(3)
    if WRITE_SPECTRUM:
        out_file = open(folder + './totalSI.txt', 'w+')
        out_file.write('#Store data for total SI\n')
        out_file.write('#$E$ (au); SI yield\n')
        for i in range(len(totalSI)):
            out_file.write(str(Energies[i]) + ',' + str(totalSI[i]) + '\n')
        out_file.close()
    if options['write_spec_total']:
        spec_out_file = open(folder + '/spec_total.txt', 'w+')
        spec_out_file.write('#Store data for total SI\n')
        spec_out_file.write('#$k$ (au); SI yield\n')
        for i in range(len(totalSI)):
            spec_out_file.write(str(absMomenta[i]) + ',' + str(totalSI[i]) + '\n')
        spec_out_file.close()
    plt.plot(Energies, np.log10(totalSI))
    from constants import Ry2, e
    Lambda = float(sys.argv[2])
    Intensity = float(sys.argv[3]) * 10 ** 14
    # print 'Omega is ' + str()
    from static_methods import LaserProperty
    from constants import helium_eigen_states
    Up = LaserProperty(Intensity, Lambda)['Up']
    omeg = LaserProperty(Intensity, Lambda)['Omeg']
    # theory_energy_final = (np.array(shifted_exact) + Up - helium_eigen_states['E0'][0])/ omeg
    experiment_data = open(folder + '/single_spectrum.txt').readlines()
    energies = [float(k.split()[0]) for k in experiment_data]
    yields = [float(k.split()[1]) for k in experiment_data]
    maps = dict(zip([str(energy) for energy in energies], [str(yield_single) for yield_single in yields]))
    sorted_energy = sorted([float(energy) for energy in energies])
    sorted_yields = [float(maps[str(energy)]) for energy in sorted_energy]
    max_experiment_yield = max(sorted_yields)
    plt.figure(6)
    plt.plot(sorted_energy, sorted_yields, label="experiment data")
    if len(sys.argv) > 4:
        base_folder = '/home/jinzhen/projects/develop/TDSEsolver/HeModel/'
        from JobsInformation import ReadParameters
        readParameters = ReadParameters(base_folder, sys.argv[4], sys.argv[5:])
        result = readParameters.get_list_content()
        numbers = result['numbers']
        folders = result['folders']
        for folder_number in folders:
            another_spectrum = folder_number
            datas = [m.split(',') for m in open(base_folder + str(another_spectrum) + '/spec_total').readlines() if m[0] != '#']
            theory_energy = [float(data[0]) **2 / 2 for data in datas if float(data[0]) **2 / 2 * Ry2 / e < 10]
            theory_yield = [float(data[1]) for data in datas if float(data[0]) **2 / 2 * Ry2 / e < 10]
            shifted_exact = np.array(theory_energy)
            theory_energy_final = shifted_exact * Ry2 / e
            max_theory_yield = max(list(theory_yield[5:]))
            theory_yield = np.array(theory_yield) * max_experiment_yield / max_theory_yield
            plt.plot(theory_energy_final, theory_yield, label=sys.argv[4] + '=' + str(numbers[folders.index(folder_number)]))
    else:
        exact_energy = np.array(Energies) + Up - helium_eigen_states['E0']
        Up_experi_fit = LaserProperty(3.3e14, 400)['Up']
        shifted_exact = exact_energy * Lambda / 400 + helium_eigen_states['E0'] - Up_experi_fit
    # print "The summation of single electron spectrum is: " + str(sum(totalSI))
    try:
        # # Lambda = float(sys.argv[2])
        # # Intensity = float(sys.argv[3]) * 10 ** 14
        # # from static_methods import LaserProperty
        # # Up = LaserProperty(Intensity, Lambda)['Up']
        # # omeg = LaserProperty(Intensity, Lambda)['Omeg']
        # # for i in range(8, 16):
        # #     i_omega = -0.90314 - Up + i * omeg
        # #     plt.plot(list(np.ones(len(Energies)) * i_omega), np.log10(totalSI), 'r')
        # # for j in range(18, 25):
        # #     j_omega = -2 - Up + j * omeg
        # #     plt.plot(list(np.ones(len(Energies)) * j_omega), np.log10(totalSI), 'g--')
        # if 'spec_total' in os.listdir(sys.argv[1]):
        #     os.system('rm ' + sys.argv[1] + '/spec_total')
        # k = open(sys.argv[1] + '/spec_total', 'a')
        # k.write('# tMax,time-resolution\n')
        # k.write('# \n')
        # k.write('#\n')
        # # m = [sum(list(n)) for n in list(SIdata)]
        # k.write('#  sum[Phi,Eta]: (specRn) = (0)\n')
        # for i in range(len(totalSI)):
        #     a = '%.10f,%.10f\n'%(absMomenta[i], totalSI[i])
        #     k.write(a)
        # # print "the total yield of single spectrum is"
        # # print sum([abs(totalSI[i] * (absMomenta[i] - absMomenta[i-1])) for i in range(len(totalSI) - 1)]) / (max(absMomenta) - min(absMomenta))
        # k.close()
        pass
    except:
        print "Fail for the operation of single electron spectrum"
    plt.rc('text', usetex=True)
    plt.xlabel(r"$E$ (eV)")
    plt.ylabel(r"SI yield (Signal)")
    plt.legend()
    plt.show()
        
def rangeDouble(start,stop,step):
    temp = np.zeros(int(np.floor((stop-start)/step)+1),dtype=np.double)
    for i in range(len(temp)):
        temp[i] = start + i*step
    return temp

  
    
def saveMomenta(folder,absMomenta):
    np.savetxt(getMomentaFileName(folder), absMomenta)

    
def saveTotalSpectrum(folder,totalSpectrum):
    np.savetxt(getTotalDISpectrumFilename(folder), totalSpectrum)
    
def getMomentaFileName(folder):
    return folder+'momenta.txt'
    
def getTotalDISpectrumFilename(folder):
    return folder+'total_DI_spectrum.txt'
    
def getJADSpectrumFilename(folder,n):
    return folder+'JAD_spectrum'+str(n)+'.txt'
    
def getJADthetaGridFilename(folder,n):
    return folder+'JAD_thetaGrid'+str(n)+'.txt'
    
def getJADenergiesFilename(folder,n,i):
    return folder+'JAD_energies'+str(n)+'_'+str(i)+'.txt'

def getStripeJADSpectrumFilename(folder, n):
    return folder + 'stripe_JAD_spectrum' + str(n) + '.txt'

def getStripeJADthetaGridFilename(folder, n):
    return folder + 'stripe_JAD_thetaGrid' + str(n) + '.txt'

def getStripeJADenergiesFilename(folder,n,i):
    return folder+'stripe_JAD_energies'+str(n)+'_'+str(i)+'.txt'

def plotTotalSpectrum(absMomenta,totalSpectrum):
    if 'boxSize' in options.keys() and 'propapate_cycle' in options.keys():
        from static_methods import get_k_cutoff
        k_cut_off = get_k_cutoff(float(options['boxSize']), float(options['propapate_cycle']), float(sys.argv[2])) * 2
        index = np.where(absMomenta > k_cut_off * 2)[0][0]
        absMomenta = absMomenta[index:]
        totalSpectrum = totalSpectrum[index:, index:]
    Energies = 0.5*absMomenta**2
    numberOfColors = 50
    plt.figure(2)
    plt.rc('text', usetex=True)
    plt.xlabel(r"$E_1$ (au)")
    plt.ylabel(r"$E_2$ (au)")
    plt.axis('equal')
    factor = 1
    print totalSpectrum.max()

    if 'use_ev' in options.keys():
        from constants import e, Ry2
        factor = Ry2 / e
        plt.xlabel(r"$E_1$ (ev)")
        plt.ylabel(r"$E_2$ (ev)")
    fig = plt.contourf(Energies * factor, Energies * factor, np.log10(totalSpectrum), numberOfColors)
    plt.xlim(0, np.array(Energies[len(Energies)-1]) * factor)
    plt.ylim(0, np.array(Energies[len(Energies)-1]) * factor)
    plt.colorbar(fig)
    # plt.show(block=False)
    # try:
    if 'includek' in options.keys() and options['includek']:
        multi_factor = np.array([np.array(absMomenta) * momenta for momenta in absMomenta])
        totalSpectrum = np.array(totalSpectrum) * multi_factor
    Lambda = float(sys.argv[2])
    Intensity = float(sys.argv[3]) * 10 ** 14
    from static_methods import LaserProperty
    Up = LaserProperty(Intensity, Lambda)['Up']
    omeg = LaserProperty(Intensity, Lambda)['Omeg']

    if 'spec_total' in os.listdir(sys.argv[1]) and not options['write_spec_total']:
        print 'rm the spec_total file \n'
        os.system('rm ' + sys.argv[1] + '/spec_total')
    totalYield = []

    for i in range(len(totalSpectrum)):
        data = 0; ks = []; yiels = []
        for j in range(i):
            yiels.append(totalSpectrum[i - j][j])
            ks.append([absMomenta[i - j], absMomenta[j]])
            data += totalSpectrum[i - j][j]
        if 'integrate' in options.keys() and options['integrate']:
            if len(ks) > 0:
                import copy
                m = copy.deepcopy(ks)
                ksback = m[: -1]
                ksback.insert(0, ks[0])
                dks = [np.sqrt((ks[m][0] - ksback[m][0]) ** 2 + (ks[m][1] - ksback[m][1]) ** 2) for m in range(len(ks))]
                totalYield.append(np.array(yiels).dot(np.array(dks)))
            else:
                totalYield.append(1e-10)
        else:
            totalYield.append(data)
    plt.figure(5)
    if 'logscale' in options.keys() and options['logscale']:
        plt.semilogy(Energies * energy_factor, totalYield, label="$\\sigma_{s}(E_{1} + E_{2})=\\int d(E_1-E2) \\sigma(E_1,E_2)$")
    else:
        plt.plot(Energies * energy_factor, totalYield)
    lower_boundary_photon = int(min(list((np.array(Energies) + 2 * Up + 2.903) / omeg)))
    up_boundary_photon = int(max(list((np.array(Energies) + 2 * Up + 2.903) / omeg)) + 1)
    Sn= []
    total_x = []
    total_y = []
    start_photon = 0
    for i in range(lower_boundary_photon, up_boundary_photon):
        x = np.ones(1000) * (i * omeg - 2 * Up - 2.903) * energy_factor
        Sn.append(x[0])
        y = [i * (max(totalYield) - min(totalYield)) / 1000.0 + min(totalYield) for i in range(1000)]
        total_x += list(x)
        total_y += list(y)
        # if 'logscale' in options.keys() and options['logscale']:
        #
        #     plt.semilogy(list(x), y, 'r', label="$m\\omega - 2U_{p} - 2.903$")
        # else:
        #     plt.plot(list(x), y, 'r', label="$m\\omega - 2U_{p} - 2.903$")
    if 'logscale' in options.keys() and options['logscale']:
        plt.semilogy(list(total_x), total_y, '.', label="$E_1+E_2=m\\omega - 2U_{p} - 2.903(a.u)$")
    else:
        plt.plot(list(total_x), total_y, '.', label='$E_1+E_2m=\\omega - 2U_{p} - 2.903(a.u)$,$m\in['+str(lower_boundary_photon) + ',' + str(up_boundary_photon) + ']$,m integal' )
    plt.xlabel("$E_1 + E_2$")
    if energy_factor!=1:
        plt.xlabel("$E_1 + E_2$(eV)")
    plt.ylabel("$\\sigma_{s}(E_{1} + E_{2})=\\int d(E_1-E2) \\sigma(E_1,E_2)$")
    content = get_input_contents('./' + sys.argv[1])
    title_content = '$I=' + sys.argv[3] + '*10^{14}W/cm^{2}$,$\lambda=' + sys.argv[2] + 'nm$,FWHM=' + content['n_cycles']
    if int(content['pulseshape']) == 15:
        title_content += ', flat-Top[1 optCycle]'
    elif int(content['pulseshape']) == 21:
        title_content += ', cos8'
    plt.legend()
    plt.title(title_content, fontsize=20)
    plt.figure(7)
    if 'logscale' in options.keys() and options['logscale']:
        sum_values = []
        momenta_back = list(absMomenta)[: -1].insert(0, 0)
        dk = np.array(absMomenta) - np.array(momenta_back)
        for item in totalSpectrum:
            sum_values.append(np.array(item).dot(dk))
    else:
        sum_values = sum(totalSpectrum)
    if 'logscale' in options.keys() and options['logscale']:
        plt.semilogy(Energies, sum_values)
    else:
        plt.plot(Energies, sum_values)
    for i in Sn:
        x = np.ones(100) * i / 2.0
        y = [i * (max(sum_values) - min(sum_values)) / 100.0 + min(sum_values) for i in
             range(100)]
        if 'logscale' in options.keys() and options['logscale']:
            plt.semilogy(list(x), y, 'g')
        else:
            plt.plot(list(x), y, 'g')

    plt.xlabel("$S_n / 2$")
    plt.ylabel("$\\sigma_1(E_{1})=\\int dE_2\\sigma(E_1,E2)$")
    title_content = '$I=' + sys.argv[3] + '*10^{14}W/cm^{2}$,$\lambda=' + sys.argv[2] + 'nm$,FWHM=4, flat-Top[1 optCycle]$'
    plt.title(title_content, fontsize=20)
    if not options['write_spec_total']:
        print 'write my own spec_total file'
        k = open(sys.argv[1] + '/spec_total', 'a')
        k.write('# tMax,time-resolution\n')
        k.write('# \n')
        k.write('#\n')
        k.write('#  sum[Phi,Eta]: (specRn) = (0)\n')
        for i in range(len(absMomenta)):
            a = '%.10f,%.10f\n'%(absMomenta[i] * np.sqrt(2), totalSpectrum[i][i])
            k.write(a)
        k.close()
    # except:
    #     print "Fail for the user-defined parameter for total spectrum"
    
    


def getMs(folder):
	for m in range(-100,0,1):
		if os.path.isfile(folder + 'spectrum_test_' + str(m) + '_2_0.bin'):
			appearingM=range(m,-m+1,1)
			return appearingM
		
def getComputedMomenta(filelist):
	computedMomenta = []
	for i in filelist[0:]:
		namelength = 18
		if len(i)>namelength and i[0:namelength] == 'spectrum_test_0_2_':
			computedMomenta.append(int(i[namelength:re.search('.bin',i).start()]))
	return sorted(computedMomenta)
	
def getSingleIonizationFiles(filelist,folder):
	singleIonizationFiles = []
	mysorting = []
	for i in filelist[0:]:
		if len(i)>10 and i[0:9] == 'testspec_':
			singleIonizationFiles.append(folder+i)
			mysorting.append(int(i[9:re.search('.txt',i).start()]))
	sortingArray = [i[0] for i in sorted(enumerate(mysorting), key=lambda x:x[1])]
	return [singleIonizationFiles[i] for i in sortingArray]
		
def getDISpecFiles(filelist,folder,m):
    # return all files with 'spectrum_test_-1_2_146'
	specFiles = []
	for mym in m:
		namelength = 18
		if mym<0: namelength = 19
		specFilesM = []
		mysorting = []
		for i in filelist[0:]:
			if len(i)>namelength and i[0:namelength] == 'spectrum_test_' + str(mym) + '_2_':
				specFilesM.append(folder+i)
				mysorting.append(int(i[namelength:re.search('.bin',i).start()]))
		sortingArray = [i[0] for i in sorted(enumerate(mysorting), key=lambda x:x[1])]
		specFiles.append([specFilesM[i] for i in sortingArray])
	return specFiles
			
def getLmax(specFiles,m):
	myfile = specFiles[(len(m)-1)/2][0]
	with open(myfile, mode='rb') as file:
		fileContent = file.read(16)
		fileContent = file.read(4)
		return struct.unpack("i",fileContent)[0]

def getNumberOfMomenta(specFiles,m):
	myfile = specFiles[(len(m)-1)/2][0]
	with open(myfile, mode='rb') as file:
		fileContent = file.read(12)
		fileContent = file.read(4)
		return struct.unpack("i",fileContent)[0]		
  
def getMomentumGrid(computedMomenta,specFiles):
    absMomenta = []
    for i in specFiles[0][0:]:
        with open(i) as file:
            fileContent = file.read(8)
            try:
                absMomenta.append(struct.unpack("d",fileContent)[0])
            except:
                pass
    mymomenta = np.array(absMomenta)
    print "The grid of mymomenta is"
    return mymomenta
		

def readInDIdata(lmaxLocal,specFilesFixedM1,specFilesFixedM2,numberOfMomenta,k1index,k2index):
    scatteringAmplitudeFixedMFixedKs = np.zeros([lmaxLocal+1,lmaxLocal+1], dtype=np.complex)
    try:
        with open(specFilesFixedM1[k1index]) as file:
            file.seek(20)
            for l in range(lmaxLocal+1):
                lengthToSkip = k2index * 2 * (lmaxLocal + 1) * 8 # 2 doubles of size 8 per complex number
                file.seek(lengthToSkip,1)
                fileContent = file.read(2*(lmaxLocal+1)*8)
                dataInDoubles = np.asarray(struct.unpack("d"*2*(lmaxLocal+1),fileContent))
                dataInComplexes = np.vectorize(np.complex)(dataInDoubles[0:len(dataInDoubles):2],dataInDoubles[1:len(dataInDoubles):2])
                scatteringAmplitudeFixedMFixedKs[l,:] += dataInComplexes
                lengthToSkip = (numberOfMomenta-1 - k2index)*2*(lmaxLocal+1)*8
                file.seek(lengthToSkip,1)
        with open(specFilesFixedM2[k2index]) as file:
            file.seek(20)
            for l in range(lmaxLocal+1):
                lengthToSkip = k1index*2*(lmaxLocal+1)*8
                file.seek(lengthToSkip,1)
                fileContent = file.read(2*(lmaxLocal+1)*8)
                dataInDoubles = np.asarray(struct.unpack("d"*2*(lmaxLocal+1),fileContent))
                dataInComplexes = np.vectorize(np.complex)(dataInDoubles[0:len(dataInDoubles):2],dataInDoubles[1:len(dataInDoubles):2])
                scatteringAmplitudeFixedMFixedKs[:,l] += dataInComplexes
                lengthToSkip = (numberOfMomenta-1 - k1index)*2*(lmaxLocal+1)*8
                file.seek(lengthToSkip,1)
    except:
        print str(k1index) + ' and ' + str(k2index) + ' has something wrong'
    return scatteringAmplitudeFixedMFixedKs

def main():   
    folder = sys.argv[1]
    if folder[-1] != '/' : folder += '/';
    print 'computing DI spectra for ' + folder
    os.system('cp /home/jinzhen/Documents/research_data/experimentData/single_spectrum.txt ' + folder + '/.')
    print 'copying the experiment single electron spectrum data to the folder'
    if 'compare_folder' in options.keys() and options['compare_folder'] and 'compare' in options.keys() and options['compare']:
        os.system('cp /home/jinzhen/projects/HeliumDI/TDSEsolver/DI/' + options['compare_folder'] + '/' + options['compare'] + '/* ' + folder + '/.')
    computeSpectrum(folder)

if __name__ == '__main__':
    options = {'includek': 1}
    simple_arguments = []
    for argument in sys.argv:
        if '--' not in argument:
            simple_arguments.append(argument)
        else:
            options[argument[2:].split('=')[0]] = argument[2:].split('=')[1]
    energy_factor = 1
    if 'use_ev' in options.keys():
        from constants import Ry2, e
        energy_factor = Ry2 / e
    if 'add_ham' in options.keys():
        sphericalharmonics8plus = []
        sphericalharmonics8minus = []

    sys.argv = simple_arguments
    main()
