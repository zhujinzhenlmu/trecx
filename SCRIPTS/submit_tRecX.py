#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
import os
import sys
import shutil

# ALL localization is on gamess_local.py
#from gamess_local import VERNO,NCPU,TARGET,SCRATCH,EXEDIR

def resume(base,runFlags):
    
    # name for the queueing system
    name=base.split('/')[-2]
    numb=base.split('/')[-1]
    name=name[0:min(5,len(name))]+numb[-min(3,len(numb)):]
    print name

    # CAPP (SLURM)
    if 'capp1'==os.environ['HOSTNAME'] or 'sulamith'==os.environ['HOSTNAME']:
        if useMem=="":
            if queue=="capp_medium": mem=8000*int(nproc)
            else              : mem=6000*int(nproc)
        elif useMem=="all":
            mem=128000
        else:
            mem=useMem

        print mem
        commands=\
            "#!/bin/bash                                    \n"+\
            "#SBATCH -p "+queue+"                           \n"+\
            "#SBATCH -o "+base+"/out                        \n"+\
            "#SBATCH -e "+base+"/err                        \n"+\
            "#SBATCH -D "+exedir+"                          \n"+\
            "#SBATCH -J "+name+"                            \n"+\
            "#SBATCH --get-user-env                         \n"+\
            "#SBATCH --mail-type=end                        \n"+\
            "#SBATCH --mail-user=armin.scrinzi@lmu.de       \n"+\
            "#SBATCH --export=NONE                          \n"+\
            "#SBATCH --time=40:00:00                        \n"+\
            "#SBATCH --ntasks="+nproc+"                     \n"

        # designate compute nodes
        if useNodes!="": commands=commands+"#SBATCH --nodelist="+useNodes+"                   \n"

        if  int(nproc)<33: commands+="#SBATCH --mem="+str(mem)+"\n"

        # SLURM is broken with mpirun - the following is a workaround
        if nproc!="1": commands+="mpirun "+exedir+"/tRecX "+base+"/inpc -noScreen "+runFlags+"\n"
        else:          commands+=exedir+"/tRecX "+base+"/inpc -noScreen "+runFlags+"\n"

        open(base+'/script','w').write(commands)
        os.system('sbatch '+base+'/script')

    # LRZ linux cluster (SGE)
    elif 'LRZlinux'==os.environ['HOST']:
        commands=\
            "#!/bin/bash                 \n"+\
            "#$ -M armin.scrinzi@lmu.de  \n"+\
            "#$ -S /bin/bash             \n"+\
            "#$ -N "+name+"              \n"+\
            "#$ -o "+base+"out           \n"+\
            "#$ -e "+base+"err           \n"+\
            "#$ -l mf=1000M,h_rt=10:00:0 \n"+\
            "#$ -l march=x86_64          \n"+\
            ". /etc/profile              \n"+\
            "cd "+os.getcwd()+"          \n"+\
            exedir+"/tsurff "+base+"/inpc -noScreen \n"
        open(base+'/script','w').write(commands)
        os.system('qsub '+base+'/script')
        
    else:
        exit('undefined system: '+os.environ['HOST'])+' or '+os.environ['HOSTNAME']

def argsAndFlags():
    args=[]
    flags=[]
    for item in sys.argv[1:]:
        if item[0]!="-": args.append(item)
        else: flags.append(item)
    return args,flags

# separate args from possible flags
inargs,inflags=argsAndFlags()

if len(inargs)<1:
    print "\n   usage: submit_tRecX input nproc queue   [defaults: nproc=1, queue=medium]"
    print "            -nodes=capp[28-30,32]  to select given nodes "
    print "            -mem=all | 1234          to set memory (default = 8000/proc, all=128GB) "
    exit()

# input file
infile=inargs[0]

# number of tasks
nproc=str(1)
if len(inargs)>1:
    nproc=inargs[1]

# queue
queue="capp_medium"
if len(inargs)>2:
    queue="capp_"+inargs[2]

useNodes=""
for f in inflags:
    if f.find("-nodes=")!=-1:
        useNodes=f.split('=')[1]

useMem=""
for f in inflags:
    if f.find("-mem=")!=-1:
        useMem=f.split('=')[1]

runFlags=""
for f in inflags:
    if f.find("-mem=")==-1 and f.find("-nodes=")==-1:
        runFlags+=" "+f

# exedir
exedir=os.getcwd()
if infile.rfind('/inpc')!=len(infile)-5:
    # strip '.inp' from the end of the file name
    if infile.rfind('.inp')==len(infile)-4: infile=infile[:infile.rfind('.inp')]

    # create directory if needed
    if not os.path.exists(infile):
        os.mkdir(infile)
        os.system('touch '+infile+'/.MARKER_FILE')

    # make a job directory
    for i in range(10000):
        jobdir=exedir+'/'+infile+'/'+str(i).zfill(4)
        if not os.path.exists(jobdir): break
    os.mkdir(jobdir)

    shutil.copy(infile+'.inp',jobdir+'/inpc')
else:
    jobdir=infile[0:len(infile)-5]

# "resume", i.e. run from job directory
print jobdir
resume(jobdir,runFlags)

