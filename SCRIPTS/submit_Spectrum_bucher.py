#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply.
#
# See terms of use in the LICENSE file included with the source distribution
#
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license

import os
import sys


def argsAndFlags():
    args = []
    flags = []
    for item in sys.argv[1:]:
        if item[0] != "-":
            args.append(item)
        else:
            flags.append(item)
    return args, flags


arglist, flags = argsAndFlags()

args = ''
for arg in arglist:
    args += ' '+arg

for flag in flags:
    args += ' '+flag

base = os.getcwd()+'/'+arglist[0].rstrip('/')+'/'
name = 'ana_'+base.rstrip('/').split('/')[-1]
exedir = os.getcwd()
queue = "capp_medium"

commands = \
    "#!/bin/bash                                    \n" + \
    "#SBATCH -p "+queue+"                           \n" + \
    "#SBATCH -o "+base+"/out_a                      \n" + \
    "#SBATCH -e "+base+"/err_a                      \n" + \
    "#SBATCH -D "+exedir+"                          \n" + \
    "#SBATCH -J "+name+"                            \n" + \
    "#SBATCH --get-user-env                         \n" + \
    "#SBATCH --mail-type=end                        \n" + \
    "#SBATCH --mail-user=j.bucher.mn@gmail.com      \n" + \
    "#SBATCH --export=NONE                          \n" + \
    "#SBATCH --time=40:00:00                        \n" + \
    "#SBATCH --ntasks=1                             \n" + \
    "cd "+os.getcwd()+"                             \n" + \
    exedir+"/Spectrum "+args+" -noScreen            \n"

open(base+'anascript', 'w').write(commands)
os.system('sbatch '+base+'/anascript')

