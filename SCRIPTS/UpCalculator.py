#!/usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
# -*- coding: utf-8 -*-


from tools import *
from units import *

from plotTools import *

(args,flags) = argsAndFlags();

if len(args)<2:
    print "Usage: "
    print "   UpCalculator.py lambda(nm) Intensity(W/cm2)"
    sys.exit(0)

Units(au())

lambdaNM=float(args[0])
intenWcm2=float(args[1])


print 'U_p (eV):', au(eV()).energy((SI().intensity(intenWcm2*1e4)/nm_inv().energy(1/lambdaNM)**2)/4.)
