#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
import sys
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.interpolate
from scipy.interpolate import griddata
import copy  as cp
from tools import *
from plotTools import *

normalize=False
smoothRange=0.
tolerance=1.e-4

errRange=1.e-5  # plot in [maxErr*errRange,maxErr]
plotRange=1.e-7 # plot in [max*plotRange,max]

(args,flags) = argsAndFlags();

if len(args)<1:
    print "Usage: "
    print "   compare.py file1[{xcol:}colrange] file2[{xcol:}colrange] ... {flags}"
    print "Example:"
    print "   compare.py -b=Argon/ -a=/spec[2] 0015 0014 0123 "
    print "     will plot relative differences of column 2 in files Argon/0123/spec and Argon/0014/spec against Argon/0015/spec"
    print "     vs. column 0 of the respective files, data will be interpolated if needed"
    print"Command line flags:"
    print"  -b=DIR......... prefix input file by DIR"
    print"  -a=WHICH..... postfix input file by WHICH"
    print"  -label=PAR1,PAR2... use parameter value(s) for PAR1,PAR2,... in plot legend, PARx as in the run's linp-files"
    sys.exit(0)

# get python file name (as run from the command line)
name=sys.argv[0].split(".py")[0]

pre,post=prePost(flags)

files=[]
colum=[]
for arg in args:
    file=pre+arg+post
    colum.append(1)
    if file.find('[')!=-1:
        colum[-1]=int(file[file.find('[')+1:file.find(']')])
        file     =file[:file.find('[')]
    files.append(file)

# semi-log?
logY=True
if "-linY" in flags: logY=False

# use first directory for reference
refe=files[0]

# Two panels
fax,(ax1,ax2) = plt.subplots(nrows=2,sharex=True)
gs = gridspec.GridSpec(2,1,height_ratios=[3,3])

# upper and lower panel
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])

# plot colors for easier comparison
colr=['red','green','blue','cyan','magenta','brown']

# reference calculation
delim=DataFile(refe).colSep
if delim=="": delim=None

dat=np.loadtxt(refe,delimiter=delim,unpack=True)

x=dat[0]
f1=dat[colum[0]]
norm0=f1[len(f1)/2]


xplot=np.array(x)
#if kind=="spec": xplot=xplot*xplot/2

# plot ranges
xmin=xplot[0]
xmax=xplot[-1]

lab=refe
if logY: ax1.semilogy(xplot,f1,label=lab,color='black')
else:    ax1.plot(xplot,f1,label=lab,color='black')

lg = ax1.legend()
lg.draw_frame(False)

dir,which=prePost(flags)

# try find linp-extract file
linpFile=refe[:refe.rfind('/')]
if os.path.exists(linpFile[:linpFile.rfind('/')]+'/linp-extract'):
    linpFile=linpFile[:linpFile.rfind('/')]
# put parameter names into legend
leg=Legend(ax2,dir,which,flags,RunParameters(linpFile))

ic=0
errmax=-1
errmin=1
fmax=np.max(f1)
fmin=np.min(f1)
for i in range(1,len(files)):
    comp=files[i]
    ic=(ic+1)%len(colr)

    dataFile=DataFile(comp)
    delim=DataFile(comp).colSep
    if delim=="": delim=None

    # get columns from file
    dat2=np.loadtxt(comp,delimiter=delim,unpack=True)
    x2=dat2[0]
    f2=dat2[colum[i]]

    fx=griddata(x2,f2,(x,),method='linear')
    norm=fx[len(fx)/2]
    if normalize: fx*=norm0/norm

    plt.suptitle(dir+"*"+which,fontsize=20)
    pars=RunParameters(comp[:comp.rfind('/')])
    lab=leg.label(ax2,colum[i],dataFile,args[i],pars)
    if lab=="": lab=comp

    if logY:
        ax1.semilogy(xplot,fx,label=lab,color=colr[ic])
    else:
        ax1.plot(xplot,fx,label=lab,color=colr[ic])

    fmin=min(fmin,np.min(fx))
    fmax=max(fmax,np.max(fx))

    if np.max(abs(fx-f1)) !=0:
        ferr=[]
        xerr=[]
        for n in range(len(fx)):
            if fx[n]!=0 and f1[n]!=0:
                ferr.append(abs(fx[n]-f1[n])/abs(0.5*(fx[n]+f1[n])))
                xerr.append(xplot[n])
        ax2.semilogy(xerr,ferr,label=lab,color=colr[ic])
        errmin=min(errmin,np.min(ferr))
        errmax=max(errmax,np.max(ferr))

    # rms deviations
    relErr=(fx-f1)/(f1+tolerance*np.max(f1))
    errRMS=np.sqrt( np.sum(relErr*relErr) * (x2[1]-x2[0]) / (np.max(x2)-np.min(x2)) )
    print "errRMS",errRMS,tolerance,np.max(f1),comp



# axis labels
ax1.set_ylabel(post,fontsize=14)
ax2.set_xlabel("x (a.u.)",fontsize=14)
ax2.set_ylabel("Relative error",fontsize=14)


# set plot limits
xmin=np.min(xplot)
xmax=np.max(xplot)

print "error range",errmin,errmax,errRange
errmin=max(errmax*errRange,errmin)
ax1.set_xlim([xmin,xmax])
if logY: fmin=max(fmin,fmax*plotRange)
ax1.set_ylim([fmin,fmax])
ax2.set_xlim([xmin,xmax])
ax2.set_ylim([errmin,errmax])

# Print legend without the box
lg = plt.legend(bbox_to_anchor=(1, 0.5))
lg.draw_frame(False)

# Merge the x axis with lower panel
fax.subplots_adjust(hspace=0.1)
plt.setp([a.get_xticklabels() for a in fax.axes[:-1]], visible=False)

plt.savefig(name+".png")

plt.show(block=False)
answer = raw_input('<return> to finish')

