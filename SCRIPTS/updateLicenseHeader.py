#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
from os import listdir, walk
from os.path import isdir, join

LICENSE = '/home/scrinzi/projects/TDSEsolver/headertext'
LICENSE_SEPARATOR = 'End of '+'license' # split this string to hide from the code
DIRS = ['SRCS', 'TOOLS','SCRIPTS']
CPPSUFFIXES = ['.h', '.cpp']
PYTHONSUFFIXES = ['.py', 'Makefile']


def end_license(text):
	"""Checks whether the text already has a header terminated by the LICENSE_SEPARATOR."""
        for n in range(len(text)):
            if LICENSE_SEPARATOR in text[n]:
                return n+1
        return 0

def process_file(filename, license_text, comment_string):
	"""Opens the specified file, removes an already existing header and adds the new header."""
	license_with_comments = comment_string + comment_string.join(license_text.splitlines(True))

	with open(filename, 'r') as current_file:
                content = current_file.readlines()
                loc=end_license(content)
                for start in range(loc,len(content)):
                    if content[start].strip()!="": break

        with open(file, 'w') as current_file:
                if content[0][:2]=="#!": current_file.write(content[0]+'\n')

                current_file.write(license_with_comments)
                current_file.write(comment_string + LICENSE_SEPARATOR +"\n \n")

                if content[start][:2]=="#!": start=start+1
                for line in content[start:]: current_file.write(line)


def get_filenames():
	"""Recursively walks the folders in DIRS and searches for files ending with one of the strings specified in SUFFIXES."""
	cpp_files = []
	python_files = []

        # get pythons in current directory
        for file in listdir("./"):
            if file.endswith(".py"): python_files.append(file)
            if file.endswith(".cpp"):   cpp_files.append(file)

        for folder in DIRS:
		if not isdir(folder):
			print('Warning: Folder ' + str(folder) + ' not found.')
		else:
			for result in walk(folder):
				for filename in result[2]:
					if filename.endswith(tuple(CPPSUFFIXES)):
						cpp_files.append(join(result[0], filename)) 
					if filename.endswith(tuple(PYTHONSUFFIXES)):
						python_files.append(join(result[0], filename)) 
	return cpp_files, python_files

license = open(LICENSE, 'r').read()

cpp_files, python_files = get_filenames()
for file in cpp_files:
	process_file(file, license, '// ')

for file in python_files:
	process_file(file, license, '# ')

