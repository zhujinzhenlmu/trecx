import os
import sys

def print_ouf_grep(folder_number):
	print("START print the infomation of folder %s " % folder_number)
	for i in range(18, 36):
		if len(os.popen('ls /scratches/capp%i/all/jinzhen/%s' % (i, folder_number)).readlines()) > 1:
			print(os.popen('more /scratches/capp%i/all/jinzhen/%s/S_Rn1/outf | grep "checkpoint"' % (i, folder_number)).readlines())
			print(os.popen('more /scratches/capp%i/all/jinzhen/%s/S_Rn2/outf | grep "checkpoint"' % (i, folder_number)).readlines())
	print("DONE the infomation of folder %s " % folder_number)

def get_capp_by_folder(folder,capp=0):
	capps = []
        for i in range(18, 36):
                try:
                        if os.path.isdir('/scratches/capp%i/all/jinzhen/%s' % (i, folder)) and capp != i:
                                capps.append(i)
                except:
                        continue
	return capps

if len(sys.argv) == 2:
	capps = get_capp_by_folder(sys.argv[1])
	print(capps)
elif len(sys.argv) == 3:
	capps = get_capp_by_folder(sys.argv[1], int(sys.argv[2]))
	if len(capps) == 0:
		print('only exists in capp' + str(sys.argv[2]))
		os.system('mv /scratches/capp%i/all/jinzhen/%s /naslx/projects/pr47cu/ru37quz/.' % (int(sys.argv[2]), sys.argv[1]))
elif sys.argv[-1] == "outf":
	folders = list(set(sys.argv[1:-1]))
	for folder_number in folders:
		print_ouf_grep(folder_number)
else:
	os.system('mkdir /naslx/projects/pr47cu/ru37quz/runs/TDSEsolver_runs_1/%s/old' % sys.argv[1])
	os.system('cp /naslx/projects/pr47cu/ru37quz/runs/TDSEsolver_runs_1/%s/* /naslx/projects/pr47cu/ru37quz/runs/TDSEsolver_runs_1/%s/old' % (sys.argv[1], sys.argv[1]))
	os.system('mkdir /naslx/projects/pr47cu/ru37quz/runs/TDSEsolver_runs_1/%s/S_Rn1' % sys.argv[1])
	os.system('mkdir /naslx/projects/pr47cu/ru37quz/runs/TDSEsolver_runs_1/%s/S_Rn2' % sys.argv[1])
	os.system('cp /scratches/capp%s/all/jinzhen/%s/S_Rn1/linp /naslx/projects/pr47cu/ru37quz/runs/TDSEsolver_runs_1/%s/S_Rn1/.' % (sys.argv[2],sys.argv[1],sys.argv[1]))
	os.system('cp /scratches/capp%s/all/jinzhen/%s/S_Rn2/linp /naslx/projects/pr47cu/ru37quz/runs/TDSEsolver_runs_1/%s/S_Rn2/.' % (sys.argv[2],sys.argv[1],sys.argv[1]))
