import sys
import os
import numpy as np
from scipy.sparse.linalg import svds

from coefficients_loader import (load_folder,
                                 load_folder_gen,
                                 Coefficients,
                                 CoefficientsDescriptor)


def to_energy(coeff, k_axis, E_axis):
    desc = coeff.descriptor
    hierarchy = list(desc.hierarchy)
    hierarchy[hierarchy.index(k_axis)] = E_axis

    descE = CoefficientsDescriptor(hierarchy)
    descE.indices = np.array(coeff.descriptor.indices)
    descE.indices[:, descE.hierarchy.index(E_axis)] = \
        descE.indices[:, descE.hierarchy.index(E_axis)]**2 \
        / 2. * 27.211

    specE = Coefficients(descE)

    k = desc.indices[:, desc.hierarchy.index(k_axis)]
    specE.values = coeff.values * k / 27.211

    return specE


class DataSet:
    def __init__(self, comment=""):
        self.comment = comment
        self.rows = []
        self.cols = []
        self.data = np.zeros(shape=(10, 10))

    def set(self, row, col, val):
        r = None
        for i in range(len(self.rows)):
            if abs(row - self.rows[i]) < 1.e-4:
                r = i
                break
        else:
            r = len(self.rows)
            self.rows += [row]

        c = None
        if col in self.cols:
            c = self.cols.index(col)
        else:
            c = len(self.cols)
            self.cols += [col]

        if r >= self.data.shape[0]:
            tmp = np.zeros(shape=(self.data.shape[0] + 10, self.data.shape[1]))
            tmp[:-10, :] = self.data
            self.data = tmp
        if c >= self.data.shape[1]:
            tmp = np.zeros(shape=(self.data.shape[0], self.data.shape[1] + 10))
            tmp[:, :-10] = self.data
            self.data = tmp

        self.data[r, c] = val

    def write_csv(self, filename):
        print("Writing to %s..." % filename)
        with open(filename, 'w') as csvfile:
            csvfile.write("# %s\n" % self.comment)
            csvfile.write('# x = ')
            for c in self.cols:
                csvfile.write(' %s' % c)

            for i in range(len(self.rows)):
                csvfile.write('\n%.12e' % self.rows[i])
                for j in range(len(self.cols)):
                    csvfile.write(',%.12e' % self.data[i, j])


class InterpolatingIntegrator:
    """
    order == 1: Rectangles. First value does not matter.
    order == 2: Trapezoidal
    and so on
    """

    def __init__(self, order):
        self.order = order
        self.integral = None
        self.values = []
        self.times = []
        self.coeffs = None

    def calculate_coeffs(self):
        """
        Interpolation(t) = ... + coeffs[2]*(t-times[0])**2 +
            coeffs[1]*(t-times[0]) + coeffs[0]
        """
        t_matrix = np.zeros(shape=(self.order, self.order),
                            dtype=np.float64)
        for i in range(self.order):
            for j in range(self.order):
                t_matrix[i, j] = (self.times[i]-self.times[0])**j

        t_matrix = np.linalg.inv(t_matrix)
        self.coeffs = np.dot(t_matrix, np.array(self.values))

    def add_integral(self, t_start, t_end):
        for e in range(self.order):
            self.integral += self.coeffs[e] / (e+1) * \
                ((t_end - self.times[0])**(e+1) -
                 (t_start - self.times[0])**(e+1))

    def next(self, time, value):
        self.values += [value]
        self.times += [time]

        if self.order == 1:
            if len(self.times) >= 2:
                if self.integral is None:
                    self.integral = np.zeros_like(self.values[0])

                self.integral += self.values[-1] * (self.times[-1] -
                                                    self.times[-2])

                self.values = self.values[-1:]
                self.times = self.times[-1:]
        else:
            if len(self.times) >= self.order:
                self.values = self.values[-self.order:]
                self.times = self.times[-self.order:]
                self.calculate_coeffs()

                if self.integral is None:
                    self.integral = np.zeros_like(self.values[0])
                    self.add_integral(self.times[0], self.times[-1])
                else:
                    self.add_integral(self.times[-2], self.times[-1])

        return self.integral


class Resolution:
    def __init__(self, axes):
        self.axes = axes

    def colname(self, index):
        return str(index)


class MomentumResolution(Resolution):
    def __init__(self, k_axis):
        super().__init__([k_axis])
        self.k_axis = k_axis

    def colname(self, index):
        return "k:%f" % index[0]


class EnergyResolution(Resolution):
    def __init__(self, E_axis):
        super().__init__([E_axis])
        self.E_axis = E_axis

    def colname(self, index):
        return "E:%f" % index[0]


class AngularResolution(Resolution):
    def __init__(self, phi_axis, eta_axis):
        super().__init__([phi_axis, eta_axis])
        self.phi_axis = phi_axis
        self.eta_axis = eta_axis

    def colname(self, index):
        return "%dm%d" % (index[0], index[1])

    def transform_to_grid(self, spectrum, phi_pts, eta_pts):
        ampl = spectrum.ampl.transform_ylm_grid(self.eta_axis,
                                                self.phi_axis,
                                                eta_pts, phi_pts)
        spec = Spectrum(spectrum.folder, ampl)
        grid = None
        for r in spectrum.resolutions:
            if isinstance(r, MomentumResolution) or \
                    isinstance(r, EnergyResolution):
                continue
            if r == self:
                grid = (GridResolution(self.phi_axis+'_grid', 'phi'),
                        GridResolution(self.eta_axis+'_grid', 'eta'))
                spec.add_resolution(grid[0])
                spec.add_resolution(grid[1])
            else:
                spec.add_resolution(r)

        spec.clean()
        return spec, grid


class GridResolution(Resolution):
    def __init__(self, axis, name):
        super().__init__([axis])
        self.axis = axis
        self.name = name

    def colname(self, index):
        return "%s:%f" % (self.name, index[0])


class ChannelsResolution(Resolution):
    def __init__(self, channel_axes):
        super().__init__(channel_axes)

    def colname(self, index):
        angular_l = None
        angular_m = None
        n = None
        for i in range(len(self.axes)):
            if self.axes[i].startswith("l"):
                angular_l = index[i]
            elif self.axes[i].startswith("m"):
                angular_m = index[i]
            elif self.axes[i] == "Eigen":
                n = index[i]

        n = n + angular_l + 1

        if n > 3:
            return "other"

        if angular_l == 0:
            angular_l = "s"
        elif angular_l == 1 and angular_m == 0:
            angular_l = "p_z"
        else:
            return "other"

        return "%d%s" % (n, angular_l)


class Spectrum:
    def __init__(self, folder, ampl):
        self.folder = folder
        self.comment = ""
        self.resolutions = []

        """
        As output by tRecX, plusminus averaging
        """
        self.ampl = ampl
        self.descriptor = self.ampl.descriptor

        """
        Normsquared of ampl, including Jacobians, or whatever...
        """
        self.spec = None

        for ax in self.descriptor.hierarchy:
            if ax.startswith('k'):
                self.k_axis = ax

        if self.k_axis is None:
            raise Exception("Could not find k axis")
        self.E_axis = self.k_axis.replace("k", "E")

        self.k_resolution = MomentumResolution(self.k_axis)
        self.E_resolution = EnergyResolution(self.E_axis)

        self.resolutions += [
            self.k_resolution,
            self.E_resolution
        ]

        self.spec = Coefficients(self.descriptor)
        self.spec.values = np.real(self.ampl.values * self.ampl.values.conj())

        """
        Store energy resolved spectrum
        """
        self.specE = to_energy(self.spec, self.k_axis, self.E_axis)

    def add_resolution(self, resolution):
        self.resolutions += [resolution]

    def restrict_k(self, start, end):
        ks = self.descriptor.indices[:, self.descriptor.hierarchy.index(
            self.k_axis)]

        idx = np.argwhere((ks <= end) & (ks >= start)).flatten()

        self.ampl.values = self.ampl.values[idx]
        self.spec.values = self.spec.values[idx]

        desc = CoefficientsDescriptor(self.descriptor.hierarchy)
        desc.indices = self.descriptor.indices[idx]
        self.descriptor = desc

        self.ampl.descriptor = self.descriptor
        self.spec.descriptor = self.descriptor

    def clean(self):
        axes = list(self.descriptor.hierarchy)
        for a in self.descriptor.hierarchy:
            if a == self.k_axis:
                axes.remove(a)
                continue
            for r in self.resolutions:
                if a in r.axes:
                    axes.remove(a)
                    break

        if len(axes) > 0:
            tmp_ampl = self.ampl.reduce_over(axes, np.sum)

            if len(tmp_ampl.values) != len(self.ampl.values):
                raise Exception("Not fully resolved!")

            self.ampl = tmp_ampl
            self.descriptor = self.ampl.descriptor
            self.spec = self.spec.reduce_over(axes, np.sum)
            self.specE = self.specE.reduce_over(axes, np.sum)

    def reduce(self, resolutions, spec=None, x_axis=None):
        if spec is None:
            spec = self.spec
        if x_axis is None:
            x_axis = self.k_axis

        axes = []
        for r in self.resolutions:
            if r not in resolutions:
                axes += [a for a in r.axes if a != x_axis]
        return spec.reduce_over(axes, np.sum)

    def write(self, resolutions, name, spec=None,
              x_axis=None, E_resolved=False):
        if spec is None:
            spec = self.specE if E_resolved else self.spec
        if x_axis is None:
            x_axis = self.E_axis if E_resolved else self.k_axis

        spec = self.reduce(resolutions, spec, x_axis)

        ds = DataSet(self.comment)
        for i in range(len(spec.values)):
            row = spec.descriptor.indices[i, spec.descriptor.hierarchy.index(
                x_axis)]
            col = "("
            for r in resolutions:
                skip = False
                for c in r.axes:
                    if c not in spec.descriptor.hierarchy:
                        skip = True
                        break
                if skip:
                    continue

                ax_idx = [spec.descriptor.hierarchy.index(c) for c in r.axes]
                col += r.colname(spec.descriptor.indices[i][ax_idx])
                col += ","

            col = col[:-1]
            if len(col) > 0:
                col += ")"
            ds.set(row, col, spec.values[i])

        ds.write_csv(os.path.join(self.folder, 'spec_'+name))

    def total_yield(self, jacobian=lambda k: k**2, spec=None, axis=None):
        if spec is None:
            spec = self.spec
        if axis is None:
            axis = self.k_axis
        axes = [h for h in self.descriptor.hierarchy
                if h != axis]
        coeff = spec.reduce_over(axes, np.sum)

        k = coeff.descriptor.indices[:, coeff.descriptor.hierarchy.index(
            axis)]
        coeff.values *= jacobian(k)

        result = 0.
        for i in range(1, len(coeff.values)):
            result += (coeff.values[i-1] + coeff.values[i]) * \
                (coeff.descriptor.indices[i, 0] -
                 coeff.descriptor.indices[i-1, 0]) / 2.

        return result


class SpectrumLoader:
    """
    Handles averaging of amplitudes
    """

    def __init__(self, folder):
        self.spec_folder = folder
        self.folder = os.path.normpath(os.path.join(folder, os.pardir))

        with open(os.path.join(self.folder, "linp")) as linp:
            for row in linp:
                if "Laser:lambda(nm)" in row:
                    lambda_nm = float(row[row.index("=")+1:])
                    self.opt_cyc = lambda_nm/2.99792458/2.418884

        self.coeffs = []

    def load_full(self):
        descriptor, coeffs = load_folder(self.spec_folder)
        self.coeffs = coeffs
        self.t_min = min([c.time for c in coeffs])
        self.t_max = max([c.time for c in coeffs])
        print("Loaded amplitudes with hierarchy %s for times [%f, %f]"
              % (".".join(descriptor.hierarchy),
                 self.t_min, self.t_max))

    def last(self, t_max=None):
        coeffs = self.coeffs if len(self.coeffs) > 0 \
                    else load_folder_gen(self.spec_folder)
        coeff = None
        for c in coeffs:
            if t_max is None or c.time < t_max:
                coeff = c
        return Spectrum(self.folder, coeff)

    def average(self, tStart=None, tEnd=None, order=1):
        coeffs = self.coeffs if len(self.coeffs) > 0 \
                    else load_folder_gen(self.spec_folder)

        integ = InterpolatingIntegrator(order)

        ampl = None
        t_start = None
        t_end = None
        counter = 0
        for c in coeffs:
            if ampl is None:
                ampl = Coefficients(c.descriptor)

            if tStart is not None and c.time < tStart:
                continue
            if tEnd is not None and c.time > tEnd:
                continue

            if t_start is None:
                t_start = c.time
            t_end = c.time

            ampl.values = integ.next(c.time, c.values)
            counter += 1

        if t_end == t_start:
            raise Exception("Too few coefficients")
        else:
            ampl.values /= (t_end - t_start)

        print("Averaged over %d coefficients in interval [%5.2f, %5.2f]" %
              (counter, t_start, t_end))

        return Spectrum(self.folder, ampl)

    def svd(self, tStart=None, tEnd=None):
        if len(self.coeffs) == 0:
            self.load_full()

        if tStart is None:
            tStart = self.t_min
        if tEnd is None:
            tEnd = self.t_max
        coeffs = [c for c in self.coeffs if tStart <= c.time <= tEnd]
        mat = np.zeros(shape=(coeffs[0].values.shape[0], len(coeffs)),
                       dtype=np.complex128)
        for i, c in enumerate(coeffs):
            mat[:, i] = c.values

        u, s, vt = svds(mat, 2)
        if(s[1] / s[0] > .1):
            print("Warning! High intensity noise, t=[%4.2f, %4.2f]" %
                  (tStart, tEnd))

        ampl = Coefficients(self.coeffs[0].descriptor)
        ampl.values = u[:, 0] * s[0]

        factor = 0.
        ts = [c.time for c in coeffs]
        for i in range(1, len(ts)):
            factor += (ts[i] - ts[i-1])\
                * .5 * (np.abs(vt[0, i-1]) + np.abs(vt[0, i]))
        ampl.values *= factor / (np.max(ts) - np.min(ts))

        return Spectrum(self.folder, ampl)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("folder",
                        help="Folder containing si_spec directory")

    parser.add_argument("--last",
                        help="Only use last wavefunction;" +
                        "no averaging",
                        action="store_true")

    parser.add_argument("--tmin",
                        help="Start averaging after t_min." +
                        " Either in au, or '2oc' for two optical cycles." +
                        " Defaults to zero.",
                        type=str)

    parser.add_argument("--tmax",
                        help="End averaging after t_max." +
                        " Either in au, or '2oc' for two optical cycles." +
                        " Defaults to infinity. ",
                        type=str)

    parser.add_argument("--kmin",
                        help="Restrict range of ks. In au.",
                        type=float)
    parser.add_argument("--kmax",
                        help="Restrict range of ks. In au.",
                        type=float)

    """
    Remark: When tStore != 0, ChannelsSubregion writes a dummy
    zero vector initially to signal pulse end time. Also ChannelsSubregion
    handles averaging of intermediate vectors. Therefore only
    order == 1 gives useful results.

    If tStore == 0. this does not apply, as any vector is written
    """
    parser.add_argument("--order",
                        help="Interpolation order. " +
                        " 1: rectangles," +
                        " 2: trapezoidal," +
                        " 3,... polynomial. If tStore != 0" +
                        " in CoefficientsSubregion, use order == 1" +
                        " (default)",
                        type=int)

    parser.add_argument("--svd",
                        help="Use SVD instead of averaging. " +
                        "tmin and tmax apply.",
                        action="store_true")

    parser.add_argument("--prefix",
                        help="Store to spec_prefix_...",
                        type=str)

    args = parser.parse_args()
    prefix = args.prefix + "_" if args.prefix is not None else ""

    """
    Load spectrum and average
    """
    loader = SpectrumLoader(args.folder)
    spec = None
    if args.last:
        spec = loader.last()
    else:
        tmin = None
        tmax = None
        if args.tmin is not None:
            tmp = args.tmin.strip()
            if tmp.endswith("oc"):
                tmin = float(tmp[:-2]) * loader.opt_cyc * (1. - 1.e-6)
            else:
                tmin = float(tmp) * (1. - 1.e-6)
        if args.tmax is not None:
            tmp = args.tmax.strip()
            if tmp.endswith("oc"):
                tmax = float(tmp[:-2]) * loader.opt_cyc * (1. + 1.e-6)
            else:
                tmax = float(tmp) * (1. + 1.e-6)

        if args.svd:
            spec = loader.svd(tStart=tmin, tEnd=tmax)
        else:
            spec = loader.average(tStart=tmin, tEnd=tmax,
                                  order=1 if args.order is None
                                  else args.order)

    """
    Restrict k range
    """
    if args.kmin is not None or args.kmax is not None:
        kmin = args.kmin
        kmax = args.kmax

        if kmin is None:
            kmin = 0
        if kmax is None:
            kmax = 10**6

        spec.restrict_k(kmin, kmax)

    """
    Detect hierarchy and add resolutions
    """
    if set(['m1', 'l1', 'm2', 'l2', 'Eigen', 'kRn2']) \
            < set(spec.descriptor.hierarchy):
        """
        6D Helium S1
        """
        spec.add_resolution(ChannelsResolution(['m1', 'l1', 'Eigen']))
        spec.add_resolution(AngularResolution('m2', 'l2'))
    elif set(['m1', 'l1', 'm2', 'l2', 'Eigen', 'kRn1']) \
            < set(spec.descriptor.hierarchy):
        """
        6D Helium S0
        """
        spec.add_resolution(ChannelsResolution(['m2', 'l2', 'Eigen']))
        spec.add_resolution(AngularResolution('m1', 'l1'))
    elif set(['m', 'l', 'kRn']) \
            < set(spec.descriptor.hierarchy):
        """
        3D
        """
        spec.add_resolution(AngularResolution('m', 'l'))
    else:
        raise Exception("Unknown hierarchy: %s" %
                        '.'.join(spec.descriptor.hierarchy))

    spec.comment = " ".join(sys.argv[1:])
    spec.clean()
    print("Total yield (k space): %.9e" % spec.total_yield())
    print("Total yield (E space): %.9e" % spec.total_yield(
        spec=spec.specE, axis=spec.E_axis, jacobian=lambda k: np.ones_like(k)))

    """
    Total spectrum
    """
    spec.write([], prefix + "total")
    spec.write([], prefix + "Etotal", E_resolved=True)

    """
    Partial spectrum
    """
    for r in spec.resolutions:
        if isinstance(r, AngularResolution):
            spec.write([r], prefix + "partial")
            spec.write([r], prefix + "Epartial", E_resolved=True)

    """
    Channel spectrum
    """
    for r in spec.resolutions:
        if isinstance(r, ChannelsResolution):
            spec.write([r], prefix + "channels")
            spec.write([r], prefix + "Echannels", E_resolved=True)

    """
    Phi/Eta grid
    """
    angular = None
    for r in spec.resolutions:
        if isinstance(r, AngularResolution):
            angular = r

    if angular is not None:
        spec_grid, grid = angular.transform_to_grid(spec, 100, 9)
        spec_grid.write(grid, prefix + "cutEta")
        spec_grid.write([grid[1], spec_grid.k_resolution],
                        prefix + "cutEta_wrt_phi", x_axis=grid[0].axis)

        spec_grid, grid = angular.transform_to_grid(spec, 9, 100)
        spec_grid.write(grid, prefix + "cutPhi")
