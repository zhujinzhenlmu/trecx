# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
import sys
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.interpolate
from scipy.interpolate import griddata
import copy  as cp
from math import *

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import colors, ticker, cm
from matplotlib.colors import LogNorm

from tools import *

class Plot:
    """ collect parameters and modify data for figure """
    def __init__(self,Flags):
        self.atX=None # lineout at X-coordinate
        self.atY=None # lineout at Y-coordinate
        for f in Flags:
            if f.find("-lineoutX=")!=-1: self.atX=float(f[10:])
            if f.find("-lineoutY=")!=-1: self.atY=float(f[10:])

        self.dim=[[1.e10,-1.e10],[1.e10,-1.e10],[1.e10,-1.e10],]

    def layout(self,Nsub):
        """ layout for subplots in figure (rows/cols"""
        if Nsub<3:   nr=1
        elif Nsub<9: nr=2
        else:        nr=3
        nc=(Nsub-1)/nr+1

        return nr,nc

    def extend(self,fmin,fmax,xmin,xmax,ymin=None,ymax=None):
        """ extend the plot size to present """
        def extend1(xmin,xmax,dim):
            dim[0]=min(dim[0],xmin)
            dim[1]=max(dim[1],xmax)
            return dim

        self.dim[0]=extend1(xmin,xmax,self.dim[0])
        self.dim[2]=extend1(fmin,fmax,self.dim[2])
        if ymin!=None: self.dim[1]=extend1(ymin,ymax,self.dim[1])

    def isLineout(self):
        return self.atX!=None or self.atY!=None

    def lineout(Plot,Xcol,Ycol,Zcol):
        """ exctract line-out according to Plot """
        # axes and matrix of values
        x,y,v=xyzToAxisArray(Xcol,Ycol,Zcol)

        # extract row/col nearest to atX/aty
        if Plot.atX!=None:
            row = (np.abs(x-Plot.atX)).argmin()
            return x[row],y,v[row,:]
        elif Plot.atY!=None:
            col = (np.abs(y-Plot.atY)).argmin()
            return y[col],x,v[:,col]
        else:
            print "specify lineout point -lineoutX=XVAL or -lineoutY=YVAL"
            exit(1)


def getRange(flags,data,command,minRatio=None):

    for flag in flags:
        if flag.find(command)==0:
            return floatRange(flag)

    up= np.max(data)
    if minRatio!=None:
        low=up*minRatio
    else:
        low=np.min(data)
    return low,up


def xyzToAxisArray(Xcol,Ycol,Zcol):
    """ convert from 3-column to 2dim data """
    fastFirst=Xcol[0]!=Xcol[1];

    if not fastFirst:
        for k in range(len(Xcol)):
            if(Xcol[0]!=Xcol[k]):
                yAxis=Ycol[:k]
                break
        xAxis=np.array([Xcol[k] for  k in range(0,len(Xcol),len(yAxis))])
        print "sizes x,y,z",xAxis.shape,yAxis.shape,Zcol.shape

        vArray=Zcol.reshape(len(xAxis),len(yAxis)).transpose() # numpy is row-wise
    else:
        for k in range(len(Ycol)):
            if(Ycol[0]!=Ycol[k]):
                xAxis=Xcol[:k]
                break
        yAxis=np.array([Ycol[k] for  k in range(0,len(Ycol),len(xAxis))])
        vArray=Zcol.reshape(len(yAxis),len(xAxis)) # numpy is row-wise

    return xAxis,yAxis,vArray


def peakPositions(pltsize,inputPars,ax1):
    """
    compute peak postitions
    from wavelength, Ip, and intensity
    add lines to plot
    """
    title=inputPars.run

    # get wave-length, intensity, ip
    name=""
    # check wave-length
    lam=inputPars.allItems("lambda(nm)")
    for l in lam:
        if l!=lam[0]: print "multiple wave-length, using first: ",lam
    wavelength=float(inputPars.item("lambda(nm)",0))
    # add up all intensities
    inte=inputPars.allItems("I(W/cm2)")
    intensity=0
    for i in inte: intensity+=float(i)
    intensity=4e14

    omega=45.5633/wavelength
    up=intensity/(4*omega*omega)/3.50944e16
    ip=0.903

    print "intensity",intensity,inte

    # draw lines at n omega - ip - up
    epos=-ip
    title="om="+str(omega)+", Up="+str(up)+", ip="+str(ip)
    epos=epos-up
    title=title+" Up subtracted"
    plt.title(title)

    nphot=0
    xmax=pltsize.dim[0][1]
    while epos<xmax*xmax*0.5-omega:

        epos=epos+omega
        nphot=nphot+1
        if epos>0:
            ax1.plot([sqrt(2*epos),sqrt(2*epos)],pltsize.dim[2],color='b')
            ax1.text(sqrt(2*epos+0.1*omega),pltsize.dim[2][1]*1.e-3,str(nphot),rotation=90)

    return name

def linLevels(vmin,vmax):
    """
    a set of logarithmically spaced levels in [vmin,vmax]
    ticks are returne at powers of 10
    """
    lmin=int(log10(abs(vmin))+1)
    lmax=int(log10(abs(vmax))+1)
    if vmin<0: lmin=-lmin
    if vmax<0: lmax=-lmax

    tics=[]
    fact=(vmax-vmin)/16
    levs=[vmin+fact,]
    for n in range(1,17): levs.append(levs[n-1]+fact)
    for l in range(lmin,lmax): tics.append(pow(10,l))
    return levs,tics

def logLevels(vmin,vmax):
    """
    a set of logarithmically spaced levels in [vmin,vmax]
    ticks are returne at powers of 10
    """
    lmin=int(log10(vmin)+1)
    lmax=int(log10(vmax)+1)

    tics=[]
    fact=pow(vmax/vmin,float(1./16.))
    levs=[vmin*fact,]
    for n in range(1,17): levs.append(levs[n-1]*fact)
    for l in range(lmin,lmax): tics.append(pow(10,l))
    return levs,tics


class Legend:
    """
    for creating labels and formating the legend
    - use "-label=PARNAME" on the command line to extract values from input (linp-file) to legend
    """
    def __init__(self,Axes,Dir,Postfix,Flags,Pars=None):
        self.previous=""
        self.previousDir=""
        self.dir=Dir
        self.postfix=Postfix
        self.labShort=""
        self.labName=""
        for f in Flags:
            if f.find("-label=")!=-1:
                self.labName=f[7:].strip()
                if self.labName[ 0]=="'" or self.labName[ 0]=='"':self.labName=self.labName[1:]
                if self.labName[-1]=="'" or self.labName[-1]=='"':self.labName=self.labName[:-1]
                if Pars!=None:
                    for n in self.labName.split(","):
                        self.labShort+=","+Pars.short(n,0)
                if(len(self.labShort)!=0): self.labShort=self.labShort[1:]
        print self.labName,self.labShort

    def label(self,ax1,col,datfil,info,pars):
        """
        create a new label for a plot, add header info as appropriate
        - if there is a dir or postfix add legend header
        - if there is a new file or info and multiple columns, add legend header
        - put into legend label the info that is not in header
        """

        file=self.dir+"/"+info+"/"+self.postfix

        parVal=""
        # if input parameters are given, add values to label
        if self.labName!="":
            for l in self.labName.split(","):
                parVal+=pars.item(l,0)+","
        parVal=parVal[:-1]

        # append file info or column number to label
        if len(datfil.datCols)==1:
            # if single column, just use file name
            lab=parVal+" ["
            if self.dir+self.postfix=="": lab+=file
            else                        : lab+=info
            lab+="]"
        else:
            # add column names
            lab=str(datfil.cols[col][0])

        # directory or post-fix are given, write into header
        curDir=self.dir+"*"+self.postfix
        if self.dir+self.postfix!="" and curDir!=self.previousDir:
            labHead=""
            if self.labShort!="": labHead=self.labShort+": "+curDir
            else:                 labHead=curDir
            if labHead!="": ax1.plot([1,],[1,],' ',label=labHead)
            self.previousDir=curDir

        # if no directory or post-fix and file name changed and not in label, force into legend
        if len(datfil.datCols)>1 and self.previous!=info and lab.find(file)==-1:
            self.previous=info
            ax1.plot([1,],[1,],' ',label=parVal+" ["+info.split('[')[0]+"]",alpha=1)

        return lab

