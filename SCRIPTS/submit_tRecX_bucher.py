#! /usr/bin/env python3

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply.
#
# See terms of use in the LICENSE file included with the source distribution
#
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license

import os
import sys
import shutil

EMAIL = "j.bucher.mn@gmail.com"
USERNAME = "ru54hiv"

args = []
flags = []
for item in sys.argv[1:]:
    if item[0] != "-":
        args.append(item.strip())
    else:
        flags.append(item.strip())

if len(args) < 1:
    print("\n   usage: submit_tRecX input nproc queue   [defaults: nproc=1, queue=medium]")  # noqa: E501
    print("            -nodes=capp[28-30,32]  to select given nodes ")
    print("            -mem=all | 1234        to set memory (default = 8000/proc, all=128GB) ")  # noqa: E501
    print("            -scratch               to use scratch folders as output ")  # noqa: E501
    print("            -spectrum              start Spectrum instead of tRecX")  # noqa: E501
    exit()

# input file
infile = args[0]

# number of tasks
nproc = 1
if len(args) > 1:
    nproc = int(args[1])

# queue
queue = "capp_medium"
if len(args) > 2:
    queue = "capp_"+args[2]

# nodes
useNodes = ""
for f in flags:
    if f.find("-nodes=") != -1:
        useNodes = f.split('=')[1]
flags = [f for f in flags if f.find("-nodes=") == -1]
print("Running on nodes '%s' with %d processes" % (useNodes, nproc))

# memory
useMem = ""
for f in flags:
    if f.find("-mem=") != -1:
        useMem = f.split('=')[1]
flags = [f for f in flags if f.find("-mem=") == -1]

if useMem == "":
    if queue == "capp_medium":
        mem = 8000*nproc
    else:
        mem = 6000*nproc

elif useMem == "all":
    mem = 128000
else:
    mem = int(useMem)
print("Using %dMB in total" % mem)

# scratch
useScratch = '-scratch' in flags
flags = [f for f in flags if f.find("-scratch") == -1]
print("Using scratch? %s" % useScratch)


# exedir
exedir = os.getcwd()

if infile.rfind('/inpc') != len(infile)-5:
    # strip '.inp' from the end of the file name
    if infile.rfind('.inp') == len(infile) - 4:
        infile = infile[:infile.rfind('.inp')]

    # create directory if needed
    if not os.path.exists(infile):
        os.mkdir(infile)
        os.system('touch '+infile+'/.MARKER_FILE')

    # make a job directory
    for i in range(10000):
        jobdir = os.path.join(exedir, infile, "%04d" % i)
        if not os.path.exists(jobdir):
            break
    os.mkdir(jobdir)

    shutil.copy(infile+'.inp', jobdir+'/inpc')
else:
    jobdir = infile[0:len(infile)-5]

jobdir = os.path.abspath(jobdir)
print("Job directory: %s" % jobdir)

jobdir_output = jobdir
if useScratch:
    jobdir_rel = jobdir[jobdir.find(USERNAME):]
    jobdir_output = os.path.join("/scratch/all/", jobdir_rel)

print("Job directory on nodes: %s" % jobdir_output)

# Suffix to not override script, out, err
suffix = ""
while os.path.isfile(os.path.join(jobdir, "out"+suffix)) or \
        os.path.isfile(os.path.join(jobdir, "err"+suffix)) or \
        os.path.isfile(os.path.join(jobdir, "script"+suffix)):
    suffix += "1"


# Spectrum
executable = "tRecX"
job = jobdir_output
if '-spectrum' in flags:
    executable = "Spectrum"
else:
    job += "/inpc"
flags = [f for f in flags if f.find("-spectrum") == -1]
print("Running %s/%s" % (exedir, executable))

# remaining flags are run flags
runFlags = " ".join(flags)

# name for the queueing system
name = "/".join(jobdir.split('/')[-2:])
print("Job name: %s" % name)

commands = \
    ("#!/bin/bash              \n") + \
    ("#SBATCH -p %s            \n" % queue) + \
    ("#SBATCH -o %s/out%s      \n" % (jobdir, suffix)) + \
    ("#SBATCH -e %s/err%s      \n" % (jobdir, suffix)) + \
    ("#SBATCH -D %s            \n" % exedir) + \
    ("#SBATCH -J %s            \n" % name) + \
    ("#SBATCH --get-user-env   \n") + \
    ("#SBATCH --mail-type=end  \n") + \
    ("#SBATCH --mail-user=%s   \n" % EMAIL) + \
    ("#SBATCH --time=960:00:00 \n") + \
    ("#SBATCH --ntasks=%d      \n" % nproc) + \
    ("#SBATCH --mem-per-cpu=%d \n" % int(mem/nproc))

# designate compute nodes
if useNodes != "":
    commands += "#SBATCH --nodelist=%s\n" % useNodes

# run orted diagnosis
commands += "mpirun python3.0 %s/diagnose_orted.py\n" % \
    os.path.dirname(os.path.realpath(__file__))

if jobdir_output != jobdir:
    commands += "mpirun mkdir -p %s\n" % jobdir_output
    commands += "cp -f %s/inpc %s/inpc\n" % (jobdir, jobdir_output)

# SLURM is broken with mpirun - the following is a workaround
if nproc != 1:
    commands += "mpirun %s/%s %s -noScreen %s" % (exedir, executable,
                                                  job, runFlags)
else:
    commands += "%s/%s %s -noScreen %s\n" % (exedir, executable,
                                             job, runFlags)

open(jobdir+'/script'+suffix, 'w').write(commands)
os.system('sbatch '+jobdir+'/script'+suffix)
