import os
import numpy as np
prefix = '.'
BASE_RESOLVE_FOLDER = '/home/jinzhen/projects/tRecX/TDSEsolver/Helium/'
if '/home/hpc/' in os.getcwd():
    prefix = 'SCRIPTS'
    BASE_RESOLVE_FOLDER = '/naslx/projects/pr47cu/ru37quz/runs/TDSEsolver_runs_1/'


def get_keys():
    return open(prefix + '/input_keys').readlines()[0].split()


def axis_part(dimension):
    if dimension == 6:
        return ('Surface:    points\n' +
                'Rc\n' +
                '\n' +
                'Absorption:     kind,   axis,   theta,  upper\n' +
                '                ECS,    Rn1,    Theta,  Rc\n' +
                '                ECS,    Rn2,    Theta,  Rc\n' +
                '\n' +
                'Axis:   name,   nCoefficients,  lower end,  upper end,  functions,      order\n' +
                '        Phi1,   AngularM,              0.,         2*pi,       expIm\n' +
                '        Eta1,   AngularL,       -1,         1,          assocLegendre{Phi1}\n' +
                '        Phi2,   AngularM,              0.,         2*pi,       expIm\n' +
                '        Eta2,   AngularL,       -1,         1,          assocLegendre{Phi2}\n' +
                '        Rn1,    Rcoeff,         0.,         Rc,         polynomial,     Rorder\n' +
                '        Rn1,    Recs,           Rc,         Infty,      polExp[1.]\n' +
                '        Rn2,    Rcoeff,         0.,         Rc,         polynomial,     Rorder\n' +
                '        Rn2,    Recs,           Rc,         Infty,      polExp[1.]\n' +
                '\n')
    elif dimension == 3:
        return ('Surface:    points\n' +
                'Rc\n' +
                '\n' +
                'Absorption:     kind,   axis,   theta,  upper\n' +
                '                ECS,    Rn,    Theta,    Rc\n' +
                '\n' +
                'Axis:   name,   nCoefficients,  lower end,  upper end,  functions,      order\n' +
                '        Phi,    AngularM,              0.,         2*pi,       expIm\n' +
                '        Eta,    AngularL,       -1,         1,          assocLegendre{Phi}\n' +
                '        Rn,     Rcoeff,         0.,         Rc,         polynomial,     Rorder\n' +
                '        Rn,     Recs,           Rc,         Infty,      polExp[1.]\n'
                '\n')


def basis_constrain(ls_shape, M, LmM, MLMax):
    if ls_shape == '0' and not M and not LmM and not MLMax:
        return ''
    else:
        constraint = 'BasisConstraint:  kind,          axes\n'
        if ls_shape:
            constraint += '                 Lshape[LShape], Eta1.Eta2\n'
        if M >= 0:
            constraint += '                 M=0,           Phi1.Phi2\n'
        if LmM:
            constraint += '                 L-M<LmM,       Eta1.Phi1\n' + \
                          '                 L-M<LmM,       Eta2.Phi2\n'
        if MLMax:
            constraint += '                 MLmax[MLMax],  Phi1.Eta1\n' + \
                          '                 MLmax[MLMax],  Phi2.Eta2\n'
        constraint += '\n'
    return constraint


def unbound_preparition(kGrid=80, energy_grid=True):
    unbound_string = '#Spectrum:energyGrid, radialPoints\n'
    if energy_grid:
        unbound_string += '#true, %i\n' % kGrid
    else:
        unbound_string += '#false, %i\n' % kGrid
    unbound_string += '\n' + \
        '#Source: UnboundDOF, Region\n' + \
        '#1,Rn1\n\n'
        # '#Plot: what\n' + \
        # '#intK\n\n'
    return unbound_string


def operators(EE, projection=True, dim=6):
    operator_stream = 'Operator: smooth=Rsmooth, truncate=Rc\n'
    if EE:
        operator_stream += "Operator: hamiltonian='1/2<<Laplacian>>-2.<<Coulomb>>+[[eeInt6DHelium]]'\n"
    else:
        operator_stream += "Operator: hamiltonian='1/2<<Laplacian>>-2.<<Coulomb>>'\n"
    operator_stream += "Operator: interaction='iLaserAx[t]<<D/DX>>+iLaserAy[t]<<D/DY>>+iLaserAz[t]<<D/DZ>>'\n"
    if projection:
        if dim == 6:
            operator_stream += "Operator: projection='(1/2<<Laplacian>>-2.<<Coulomb>>):Phi1.Eta1.Rn1 + (1/2<<Laplacian>>-2.<<Coulomb>>):Phi2.Eta2.Rn2'\n"
    operator_stream += "\n"
    if EE:
        operator_stream += 'OperatorFloorEE: lambdaMax\n' + '8\n'
        operator_stream += '\n'
    return operator_stream


def laser(laser_shape, fwhm_second=False):
    laser_steam = 'Laser:  shape,               I(W/cm2),   FWHM,       lambda(nm), phiCEO\n' + \
                  '       ' + laser_shape + ',   Intensity,  Fwhm OptCyc, Lambda., PhiCEO\n'
    if fwhm_second:
        laser_steam = 'Laser:  shape,               I(W/cm2),   FWHM,       lambda(nm), phiCEO\n' + \
            '       ' + laser_shape + ',   Intensity,  Fwhm s, Lambda., PhiCEO\n'
    laser_steam += '\n'
    return laser_steam


def propgate():
    return ('TimePropagation:    end,            print,           store,  accuracy,   cutEnergy\n' +
            '                  TEnd OptCyc,    TPrint OptCyc,   TStore, Accuracy,      CutE\n' +
            '\n')


def coef_writer():
    return ('CoefficientsWriter: name,   averageAxes,    store\n' +
            'coeff,  Rn1.Rn2,        0.0625 OptCyc')


def read_maps(folder):
    contents = open(folder + '/inpc').readlines()
    maps = {}
    for content in contents:
        if '#define ' in content:
            key = content.split()[1]
            value = content.split()[2]
            maps[key] = value
    if len(os.popen('more %s/inpc | grep -a cos8' % folder).readlines()) != 0:
        maps['shape'] = 'cos8'
    elif len(os.popen('more %s/inpc | grep -a flatTop' % folder).readlines()) != 0:
        maps['shape'] = 'flatTop'
    if len(os.popen('more %s/inpc | grep -a "[;]"' % folder).readlines()) != 0:
        shape_string = os.popen(
            'more %s/inpc | grep -a "[;]"' % folder).readlines()[0]
        maps['LShape'] = shape_string.split('[')[1].split(']')[0]
    maps['ee_int'] = len(os.popen(
        'more %s/inpc | grep -a "eeInt6DHelium"' % folder).readlines()) != 0 and '1' or '0'
    if 'PhiCEO' not in maps:
        if len(os.popen('more %s/inpc | grep -a "Lambda.,"' % folder).readlines()) != 0:
            ceo_string = os.popen(
                'more %s/inpc | grep -a "Lambda.,"' % folder).readlines()[0]
            maps['PhiCEO'] = ceo_string.split(
                ',')[-1][:-1].strip().replace(' ', '')
        else:
            maps['PhiCEO'] = '0'
    maps['mTotal'] = len(os.popen('more %s/inpc | grep -a "M=0"' %
                                  folder).readlines()) != 0 and '0' or '-1'
    maps['dim'] = len(os.popen('more %s/inpc | grep -a "Rn2"' %
                               folder).readlines()) != 0 and '6' or '3'
    return maps


def read_linp_line(value_string):
    value_string = value_string.replace(
        ' OptCyc', '').replace(' s', '').replace('--', '')
    values = value_string.split()
    job_id = ''
    for k in values[0]:
        if k in '0123456789':
            job_id += k
    return_values = [job_id, values[1], '6']
    for item in values[6:-1]:
        if 'Laplacian' in item:
            if 'eeInt6DHelium' in item:
                return_values.append('1')
            else:
                return_values.append('0')
        elif 'Lshape' in item:
            return_values.append(item.replace('Lshape[', '').replace(']', ''))
            # insert the ones of LMShape and mTotal
            return_values.append('0')
            return_values.append('0')
        elif 'MLmax' in item:
            return_values.append(item.replace('MLmax[', '').replace(']', ''))
        else:
            return_values.append(item)
    return return_values


def read_maps_linp(folder, keys):
    os.system('cp %s/linp-extract %s/.' % (prefix, BASE_RESOLVE_FOLDER))
    contents = os.popen('python %s/lRuns_resolve.py %s %s' %
                        (prefix, BASE_RESOLVE_FOLDER, folder)).readlines()
    read_keys = contents[2].split()
    values = []
    values_string = ''
    for item in contents[3:]:
        if len(item) < 10 or '  ' not in item:
            continue
        values.append(read_linp_line(item))
        values_string = ' '.join(values[-1]) + '\n'
    return values_string, values


def get_value_string(maps, keys):
    return_string = ''
    values = []
    for key in keys:
        if key in maps:
            return_string += maps[key] + ' '
            values.append(maps[key])
        else:
            values.append('unkown')
            return_string += 'unkown '
    return return_string, values


class ResolveInputs:
    def __init__(self, folders, dim=6):
        self.folders = folders
        self.keys = get_keys()
        self.dim = dim

    def run_inpc(self, print_string=True):
        inpc_values_string = ''
        inpc_values = []
        for folder in self.folders:
            if os.path.isfile(BASE_RESOLVE_FOLDER + folder + '/inpc'):
                maps = read_maps(BASE_RESOLVE_FOLDER + folder)
                return_string, values = get_value_string(maps, self.keys)
                inpc_values_string += folder + ' ' + return_string + '\n'
                inpc_values.append([folder] + values)
        if print_string:
            print(inpc_values_string)
        return inpc_values

    def run_linp(self, print_string=True):
        linp_string = ''
        linp_values = []
        for folder in self.folders:
            item_string, values_item = read_maps_linp(folder, self.keys)
            linp_string += item_string
            linp_values = linp_values + values_item
        if print_string:
            print(linp_string)
        return linp_values

    def check_linp_inpc(self):
        inpc_values = self.run_inpc(print_string=False)
        linp_values = self.run_linp(print_string=False)
        if len(inpc_values) != len(linp_values):
            print(inpc_values)
            print(linp_values)
            sys.exit(
                'The lengthes of data from inpc and linp are different, check it')
        is_diff = False
        for i in range(len(inpc_values)):
            for j in range(1, len(inpc_values[i])):
                # the linpc item has the information of capp number at index 1, skip it
                if linp_values[i][j + 1] != inpc_values[i][j] and inpc_values[i][j] != 'unkown':
                    if float(linp_values[i][j + 1]) == float(inpc_values[i][j]):
                        continue
                    is_diff = True
                    print('it is different at %s, %s' %
                          (linp_values[i][j + 1], inpc_values[i][j]))
                    print(linp_values[i])
                    print(inpc_values[i])
                    break
        if not is_diff:
            print('folder %s has the same data extracted from inpc and linp' %
                  inpc_values[0])


class Generator:
    def __init__(self, parameters, title='Helium', e_grid=True, k_number=80):
        keys = get_keys()
        self.map = dict(zip(keys, parameters))
        self.dim = int(parameters[0])
        self.title = title
        self.e_grid = bool(e_grid)
        self.k_number = int(k_number)

    def definition_part(self):
        def_stream = '#### Predefined parameters ######\n'
        for key in self.map:
            if not key[0].islower():
                def_stream += '#define ' + key + '  ' + \
                    self.map[key].replace('E', 'e') + '\n'
        def_stream += '\n'
        return def_stream

    def run(self):
        print('create the inputs')
        total_stream = 'Title:\n' + self.title + '\n'
        total_stream += self.definition_part() + axis_part(int(self.map['dim'])) + \
            basis_constrain(self.map['LShape'], int(self.map['mTotal']), int(self.map['LmM']), int(self.map['MLMax'])) + unbound_preparition(kGrid=self.k_number, energy_grid=self.e_grid) + \
            operators(int(self.map['ee_int']), dim=int(self.dim)) + laser(self.map['shape'].replace('~',
                                                                                 ' '), fwhm_second=float(self.map['Fwhm']) < 1) + propgate()
        if int(self.dim) == 6:
            total_stream += coef_writer()
        return total_stream


if __name__ == '__main__':
    import sys
    value_lists = open('./options.txt').readlines()
    if len(sys.argv) < 2:
        help_message = "This script is used to generate inputs and resolve the parameters from input files" +\
            "including inpc and linp file, for the generate and submit use, type:\n" +\
            "python me.py Helium(the name of the input file) 16(number of processes for run) -total=1(create the full submitscript) -e_grid=[nothing, 1](use energy grid) -k_number=(the number of k grids)\n " +\
            "for resolve using inpc file: python mepoy 209499 resolve\n" +\
            "for resolve using linp file: python me.py 209499 resolveLinp\n" +\
            "for compare the results from linp and inpc: python me.py 209499 compare\n"
        sys.exit(help_message)
    if 'resolve' in sys.argv[1:]:
        resolveInputs = ResolveInputs(sys.argv[1:-1])
        resolveInputs.run_inpc()
        sys.exit('resolve done')
    elif 'resolveLinp' in sys.argv[1:]:
        resolveInputs = ResolveInputs(sys.argv[1:-1])
        resolveInputs.run_linp()
        sys.exit('resolveLinp done')
    elif 'compare' in sys.argv[1:]:
        resolveInputs = ResolveInputs(sys.argv[1:-1])
        resolveInputs.check_linp_inpc()
        sys.exit('compare done')
    for value_list in value_lists:
        write = open(sys.argv[1] + '.inp', 'w')
        create_params = {'title': sys.argv[1]}
        create_keys = ['e_grid', 'k_number']
        params = ''
        if len(sys.argv) > 2:
            n_process = sys.argv[2]
            if len(sys.argv) > 3:
                for item in sys.argv[2:]:
                    if '=' in item:
                        key = item[1:].split('=')[0]
                        if key in create_keys:
                            create_params[item[1:].split('=')[0]] = item.split('=')[1]
                        else:
                            params += ' ' + item

        generator = Generator(value_list.split(), **create_params)
        write.write(generator.run())
        n_process = ''
        if len(sys.argv) > 2:
            n_process = sys.argv[2]
        write.close()
        # params = ''
        print(params)
        # if len(sys.argv) > 3:
        #     params = ' '.join(sys.argv[3:])
        if prefix == 'SCRIPTS':
            contents = os.popen('python ' + prefix + '/submit_tRecX_jinzhen_bound.py ' +
                                sys.argv[1] + '.inp ' + n_process + ' -mem=all ' + params).readlines()
            for content in contents:
                print(content)
    # remove the boring output exceptation
    try:
        sys.stdout.close()
    except:
        pass
    try:
        sys.stderr.close()
    except:
        pass
