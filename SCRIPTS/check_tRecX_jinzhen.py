#! /usr/bin/env python3

# for submitting jobs of tRecX

import os
import sys
import shutil
from shutil import copytree, ignore_patterns, copyfile
import time
from lrz_tools import get_capp_by_folder

user_name = 'jinzhen'
user_email = 'jinzhen.zhu@physik.uni-muenchen.de'
USER_NAME = os.path.abspath('.').split('/')[4]
BASE_PROJECT_FOLDER = '/naslx/projects/pr47cu/' + \
    USER_NAME + '/runs/TDSEsolver_runs_1/'
BASE_SCRATCH_FOLDER = '/scratch/all/' + user_name + '/$SLURM_JOBID/'
BASE_RUN_SCRATCH_FOLDER = '/scratch/all/' + user_name + '/'
LRZ_ID = 'ru37quz'


def get_jobs_info(last_jobs):
    contents = os.popen('squeue').readlines()
    current_jobs = {}
    for i in range(1, len(contents)):
        content = contents[i]
        items = content.split()
        if items[3] == LRZ_ID:
            # items[-1] capp number in form capp26
            current_jobs[items[0]] = items
    operate_jobs = {}
    for last_job_id in last_jobs:
        if last_job_id not in current_jobs:
            operate_jobs[last_job_id] = last_jobs[last_job_id]
    return current_jobs, operate_jobs


def check_and_operate_jobs_complete(last_jobs={}):
    last_jobs, operate_jobs = get_jobs_info(last_jobs)
    SD_jobs, SD_capps = {}, {}
    for job_id in operate_jobs:
        SD_jobs, SD_capps = operator_single_job(operate_jobs[job_id], SD_jobs, SD_capps)
    for SD_id in SD_jobs:
        if len(SD_jobs[SD_id]) > 1:
            # case0 two UB all finished
            submit_S2D(SD_id, SD_capps[SD_id][0], SD_capps[SD_id][1])
        elif len(SD_jobs[SD_id]) == 1:
            # case1 only one is finished
            capps = get_capp_by_folder(SD_id)
            other_capps = []
            for capp in capps:
                if 'capp' + str(capp) != SD_capps[0]:
                    other_capps.append('capp' + str(capp))
            if not os.path.isdir('/scratches/%s/all/%s/%s/S_Rn2' % (SD_capps[0], user_name, SD_id)):
                os.system('cp -r /scratches/%s/all/%s/%s/S_Rn1/* /scratches/%s/all/%s/%s/S_Rn1/.' % (SD_capps[0], user_name, SD_id, other_capps[0], user_name, SD_id))
            else:
                if os.path.isfile('/scratches/%s/all/%s/%s/S_Rn1/outf' % (SD_capps[0], user_name, SD_id)) and len(os.popen('more /scratches/%s/all/%s/%s/S_Rn1/outf | grep "===  done"' % (SD_capps[0], user_name, SD_id)).readlines()) != 0:
                    commands = 'python SCRIPTS/submit_tRecX_jinzhen_bound.py %s 16 -nodes=%s \n' % (SD_id, SD_jobs[SD_id][0][4:])
                    # case 1.1 the finished one is the second one
                    os.system(commands)
                else:
                    # case1.0 the finished one is the first one
                    os.system('cp -r /scratches/%s/all/%s/%s/S_Rn2 /scratches/%s/all/%s/%s/.' % (SD_capps[0], user_name, SD_id, other_capps[0], user_name, SD_id))
    return get_jobs_info(last_jobs)[0]


def operator_single_job_complete(items):
    if 'UB' in items[2]:
        if 'S_Rn2' in os.listdir('/scratches/%s/all/%s/%s/' % (items[-1], user_name, items[2][:-2])):
            return len(os.popen('tail /scratches/%s/all/%s/%s/S_Rn2/outf | grep "===  done - "' % (items[-1], user_name, items[2][:-2])).readlines()) > 0 and 'SDmain' or False
        else:
            return len(os.popen('tail /scratches/%s/all/%s/%s/S_Rn1/outf | grep "===  done - "' % (items[-1], user_name, items[2][:-2])).readlines()) > 0 and 'SD' or False
    elif 'SP' in items[2]:
        return len(os.popen('tail /scratches/%s/all/%s/%s/anaout | grep "===  done - "' % (items[-1], user_name, items[2][:-2])).readlines()) > 0 and 'DONE' or False
    else:
        return len(os.popen('tail /scratches/%s/all/%s/%s/outf | grep "===  done - "' % (items[-1], user_name, items[0])).readlines()) > 0 and 'UB' or False


def check_and_operate_jobs():
    contents = os.popen('squeue').readlines()
    SD_jobs = {}
    SD_capps = {}
    for i in range(len(contents)):
        content = contents[i]
        items = content.split()
        if LRZ_ID == items[3]:
            SD_jobs, SD_capps = operator_single_job(items, SD_jobs, SD_capps)
    for SD_id in SD_jobs:
        if len(SD_jobs[SD_id]) > 1:
            kill_jobs(SD_jobs[SD_id])
            submit_S2D(SD_id, SD_capps[SD_id][0], SD_capps[SD_id][1])


def operator_single_job(items, SD_jobs, SD_capps):
    item_type = check_type(items)
    if not item_type:
        return SD_jobs, SD_capps
    if item_type == 'UB':
        submit_B2S(items[0], items[-1])
    elif 'SD' in item_type:
        if items[2][:-2] in SD_jobs:
            if item_type == 'SDmain':
                SD_capps[items[2][:-2]].insert(0, items[-1])
            else:
                SD_capps[items[2][:-2]].append(items[-1])
            SD_jobs[items[2][:-2]].append(items[0])
        else:
            SD_capps[items[2][:-2]] = [items[-1]]
            SD_jobs[items[2][:-2]] = [items[0]]
    elif item_type == 'DONE':
        kill_jobs([items[0]])
    return SD_jobs, SD_capps


def submit_S2D(job_id, capp_num1, capp_num2):  # capp_num in capp26 form
    print('submit_S2D ' + job_id + capp_num1 + capp_num2)
    commands = 'cp -r /scratches/%s/all/%s/%s/S_Rn1/* /scratches/%s/all/%s/%s/S_Rn1/. \n' % (
        capp_num2, user_name, job_id, capp_num1, user_name, job_id)
    commands += 'python SCRIPTS/submit_tRecX_jinzhen_bound.py %s 16 -nodes=%s \n' % (
        job_id, capp_num1[4:])
    print(commands)
    os.system(commands)
    return commands


def submit_B2S(job_id, capp_num):  # capp_num in capp26 form
    print('submit_B2S ' + job_id + capp_num)
    nodes = get_avaliable_nodes()
    if len(nodes) < 1:
        return
    base_num = int(capp_num[4:])
    node_num = nodes[0]
    for i in range(1, len(nodes)):
        if nodes[i - 1] < base_num and nodes[i] > base_num:
            node_num = nodes[i - 1]
            break
    command = 'mkdir /scratches/capp%i/all/%s/%s/ \n' % (
        node_num, user_name, job_id)
    command += 'mkdir /scratches/capp%i/all/%s/%s/S_Rn1 \n' % (
        base_num, user_name, job_id)
    command += 'rm /scratches/%s/all/%s/%s/wfCheck* \n' % (
        capp_num, user_name, job_id)
    command += 'cp /scratches/capp%i/all/%s/%s/surface* /scratches/capp%i/all/%s/%s/. \n' % (
        base_num, user_name, job_id, node_num, user_name, job_id)
    command += 'cp /scratches/capp%i/all/%s/%s/inpc /scratches/capp%i/all/%s/%s/. \n' % (
        base_num, user_name, job_id, node_num, user_name, job_id)
    command += 'python  SCRIPTS/submit_tRecX_jinzhen_bound.py %s 16 -nodes=%i \n' % (
        job_id, base_num)
    command += 'python  SCRIPTS/submit_tRecX_jinzhen_bound.py %s 16 -nodes=%i \n' % (
        job_id, node_num)
    kill_jobs([job_id])
    print(command)
    os.system(command)
    return


def get_avaliable_nodes():
    avali_stream = ''
    for content in os.popen('sinfo ').readlines():
        if 'idle' in content:
            avali_stream = content.split(
            )[-1].replace('capp[', '').replace(']', '')
            break
    aveliable_nodes = []
    for item in avali_stream.split(','):
        if '-' in item:
            aveliable_nodes += range(int(item.split('-')
                                         [0]), int(item.split('-')[1]))
        else:
            aveliable_nodes.append(int(item))
    return_nodes = []
    for node in aveliable_nodes:
        disk_usage = int(os.popen('df /scratches/capp%i' %
                                  node).readlines()[1].split()[-2][:-1])
        if disk_usage <= 95:
            return_nodes.append(node)
    return return_nodes


def check_type(items):
    if 'capp' not in items[-1]:
        return False
    if 'UB' in items[2]:
        if 'S_Rn2' in os.listdir('/scratches/%s/all/%s/%s/' % (items[-1], user_name, items[2][:-2])):
            return len(os.popen('tail /scratches/%s/all/%s/%s/S_Rn2/outf | grep "===  done - "' % (items[-1], user_name, items[2][:-2])).readlines()) > 0 and 'SDmain' or False
        else:
            return len(os.popen('tail /scratches/%s/all/%s/%s/S_Rn1/outf | grep "===  done - "' % (items[-1], user_name, items[2][:-2])).readlines()) > 0 and 'SD' or False
    elif 'SP' in items[2]:
        return len(os.popen('tail /scratches/%s/all/%s/%s/anaout | grep "===  done - "' % (items[-1], user_name, items[2][:-2])).readlines()) > 0 and 'DONE' or False
    else:
        return len(os.popen('tail /scratches/%s/all/%s/%s/outf | grep "===  done - "' % (items[-1], user_name, items[0])).readlines()) > 0 and 'UB' or False


def kill_jobs(job_ids):
    print('qdel ' + ' '.join(job_ids))
    os.system('qdel ' + ' '.join(job_ids))


if __name__ == '__main__':
    SLEEP_TIME = 300
    # while True:
    #     check_and_operate_jobs()
    #     time.sleep(SLEEP_TIME)
    last_jobs = {}
    while True:
        last_jobs = check_and_operate_jobs_complete(last_jobs)
        time.sleep(SLEEP_TIME)
