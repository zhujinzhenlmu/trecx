#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
"""
prints a summary of input for jobs from the same input file
output is controlled by a file that is generated at first run
"""
import os
import sys
import shutil
from tools import *

# generate extraction list from linp-extract
def inputExtraction(Root):
    """
    get entry names from "linp-extract" file
    get column header names where these are defined by an extra =colName
    """
    entr=[]
    head=[]
    rang=[]
    outf=[]
    if os.path.isfile(Root+"/linp-extract"):
        lines=open(Root+"/linp-extract",'r').readlines()
        for l in lines:

            if l.find("DO NOT EDIT")!=-1:
                print "remove leftover line from file:\n",Root+"/linp-extract"
                print '"'+l+'"'
                exit(1)

            if l[0]=='#':
                # substrings from outf
                if len(l)>6 and l[:6]=="#outf:":
                    outf.append(l[6:])
                continue

            info=splitOutsideBrackets(l,"=",["{<"],["}>"])
            entr.append(info[0])
            rang.append([0,-1])
            if len(info)==3:
                rang[-1][0],rang[-1][1]=rangeInSquareBrackets(info[2])
                head.append(info[2][:info[2].find('[')].strip())
            else:
                head.append('... ')
    return entr,head,rang,outf


# extract according to linp-extract (generate linp-extract if not present)
def extractInput(root,job,extractOutf):

    # create initial extraction file (needs editing)
    if not os.path.isfile(root+"/linp-extract"):
        os.system('cp '+root+'/'+job+'/linp '+root+'/linp-extract')
        print "\n*************************************************************"
        print "Instructions for extraction list:"
        print "- edit "+infile+"/linp-extract: leave only entries you want displayed"
        print "- append =colHeader to the selected lines to define column headers"
        print "- # in first column marks comments"
        print "*************************************************************"
        exit(0)

    # extract from input file
    inp=open(root+'/'+job+"/linp",'r').readlines()
    extInp,extExtra=extractOutput(root,job,extractOutf)

    k=0
    for name in extract:
        for val in inp:
            if name==val.split('=')[0]:
                vals=splitOutsideBrackets(val,"=",["{<"],["}>"])
                val1="=".join(vals[1:])
                if val1.strip().find("e+308")==-1: valExt=val1.strip()
                else: valExt="Infty"

                # we can define a sub-range of the input string
                if extractRange[k][1]==-1: extInp+="\t"+valExt[extractRange[k][0]:]
                else: extInp+="\t"+valExt[extractRange[k][0]:extractRange[k][-1]]
                break;
        else:
            extInp+="\tn/a"
        k+=1
    return extInp,extExtra

def isRunLine(l):
    # last line criteria: all numbers, at least 4
    if len(l.split())<4: return False
    for s in l.split():
        try: float(s)
        except ValueError: return False
    return True

def splitOutsideBrackets(str,sep,left,right):
    if len(left)!=len(right):
        print "number of left brackets does not match number of right brackets"
        print " left:",left
        print "right:",right

    # replace separators between brackets by PLACEHOLDER
    ph="PLACEHOLDER"
    ph=ph.replace(sep,"") # modify to avoid accidental agreement

    loc=str.find(sep)
    while loc!=-1:
        for k in range(len(left)):
            if str[loc:].count(left[k])!=str[loc:].count(right[k]):
                str=str[:loc]+str[loc:].replace(sep,ph)
                break;
        loc=str.find(sep,loc+1)

    # split and re-insert separators
    sp=str.split(sep)
    for k in range(len(sp)): sp[k]=sp[k].replace(ph,sep)

    return sp


def extractOutput(root,job,extractOutf):

    if not os.path.isfile(root+'/'+job+"/outf"): return " "+job+":       --- no output file ---"
    outlines=open(root+'/'+job+"/outf",'r').readlines()

    # check for regular termination
    term=" "
    for lin in outlines[-5:]:
        if lin.find(" done ")!=-1: term='*'

    if os.path.isfile(root+'/'+job+"/err"):
        errlines=open(root+'/'+job+"/err",'r').readlines()
        if len(errlines)>0: term="E"    

    if os.path.isfile(root+'/'+job+"/out"):
        errlines=open(root+'/'+job+"/out",'r').readlines()
        for lin in errlines:
            if lin.find("Killed")!=-1: term="E"

    # mark as "+" if analysis was done, as "a" if analysis is in progress
    if term=='*' and os.path.isfile(root+'/'+job+"/anaout"):
        term="a"
        analines=open(root+'/'+job+"/anaout",'r').readlines()
        for lin in analines[-5:]:
            if lin.find(" done ")!=-1: term='+'


    # find master host name
    host="(no info)"
    for k in range(len(outlines)):
        if outlines[k].find("Master host = ")!=-1: host=outlines[k].strip().split()[4];
    host=(10*" "+host.strip()+" ")[-10:]


    # find time-propagation header
    proc="?"
    for k in range(len(outlines)):
        if outlines[k].find("running parallel with")!=-1: proc=outlines[k].strip().split()[4];
        if outlines[k].find("CPU")!=-1 and outlines[k].find("(%)")!=-1: break
    else:
        return term+job+": "+host+"            --- no propagation ---                 ",""

    # find progress printout
    last=" --- --- --- --- ---"
    kLast=len(outlines)-1
    for l in range(k+1,len(outlines)):
        # accept/reject - finished
        if outlines[l].find("accept/reject")!=-1: kLast=l;break
    while not isRunLine(outlines[kLast]): kLast-=1
    else: last=outlines[kLast]

    # compose info into single line:
    s=term+job+": "+host                  # status (' '/'*') + job number + host
    s+=(last.split()[0]+" "*10)[:9]       # current running time
    s+=proc.rjust(5)                      # number of processe, right justified
    s+=' '+(last.split()[2].strip()+" "*20)[:7]  # time-propagation time
    s+=' '+(last.split()[3].strip()+" "*20)[:10] # standard norm
    if len(last.split())>5:
        s+=' '+(last.split()[5].strip()+" "*20)[:14] # 0th expectation value (may not always be there?)
    else:
        s+="     ----     "

    extra=''
    for codeOut in extractOutf:
        for out in outlines:
            beg=out.find(codeOut.split('=')[0])
            end=beg+out[beg:].find(codeOut.split('=')[1])
            if beg!=-1 and end!=-1:
                extra+='\t'+out[beg+len(codeOut.split('=')[0]):end]
                break
        else:
            extra+='\t??'

    return s,extra

if __name__ == "__main__":

    args,flags=argsAndFlags();

    if len(args)<1:
        print "\n Input overview for runs in a base directory"
        print   " -------------------------------------------"
        print " usage: lRuns base_dir select"
        print "        base_dir....  w/o the /0000,/0001, etc."
        print "        select......  format 3,5,8,10-13,8 (w/o blanks)"
        print "                      42- all >=42, default: 0-9999"
        print " flags: -A...print all, -C...print completed (default: print started)"
        print ""
        print " Example for linp-extract:"
        print "     #outf:'Rg='=>>=Rg"
        print "     Axis:functions[2]=assocLegendre{Phi.L-M<5}=AssLeg[17:23]"
        print "     Axis:lower end[4]=20.=Box"
        print " plots three columns with headers 'AssLeg' 'Box' 'Rg'"
        print " values characters 17-23 of 'assocLegendre{Phi...}', here '.L-M<5', of Axis:functions[2]"
        print " and all after first '=' and before second '=', i.e. 20. of Axis:lower end[4] "
        print " the run's outf will be scanned for \"Rg\" and \">>\", string in between will be printed"
        exit()

    # input file
    infile=args[0]

    # what to display
    display="started"
    if "-A"   in flags: display="all"
    elif "-C" in flags: display="completed"

    # full path to base directory
    root=os.getcwd()+"/"+infile

    # which values to extract form input and column headers
    extract,extractName,extractRange,extractOutf=inputExtraction(root);

    #header line
    head=head=" Run:"
    head+="    Master  Running  #Proc  Time    NormTotal <Expectation0>"
    for n in extractName: head+="\t"+n
    for o in extractOutf:
        head+="\t"
        if o.split("=")>2: head+=o.split("=")[-1].rstrip()

    # output header line
    print " --- data selected according to "+root+"/linp-extract ---"
    headCnt=0

    # generate list of jobs for display
    jobs=range(0,9999)
    if len(args)>1:
        jobs=expandRange(args[1])

    noProp=[]
    for i in jobs:
        job=str(i).zfill(4)
        print(root+'/'+job+'/inpc')
        if os.path.isfile(root+'/'+job+'/inpc'):
            if headCnt%30==0: print "\033[1m"+head+"\033[0m" # the escapes make bold-face
            print(headCnt)
            headCnt+=1
            if not os.path.isfile(root+'/'+job+"/linp"):
                print "linp-file missing - generate dummy in "+job
                os.system('touch '+root+"/"+job+"/linp")
            line,extra=extractInput(root,job,extractOutf)

            if display=="all" or line.find("no propagation")==-1:
                if not display=="completed" or line[0]=="*": print line+extra
            else:
                noProp.append(i)
    if len(noProp)>0: print "W/o propagation",len(noProp)," last few:",noProp[-min(len(noProp),20):]
