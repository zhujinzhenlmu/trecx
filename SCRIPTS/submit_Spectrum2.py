#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
import os
import sys

def argsAndFlags():
    args=[]
    flags=[]
    for item in sys.argv[1:]:
        if item[0]!="-": args.append(item)
        else: flags.append(item)
    return args,flags

arglist,flags=argsAndFlags()

args=''
for arg in arglist: args+=' '+arg
for flag in flags: args+=' '+flag

if len(args)!=1:
    print "usage: >submit_Spectrum2.py Dir/xxxx (only specify run directory, no flags)"
    #exit()

if len(flags)!=0:
    print "no flags allowed for this script, found: ",flags
    exit()


base=os.getcwd()+'/'+arglist[0].rstrip('/')+'/'
name=base.rstrip('/').split('/')[-1]
exedir=os.getcwd()
queue="capp_medium"

    # CAPP (SLURM)
if 'capp1'==os.environ['HOSTNAME'] or 'sulamith'==os.environ['HOSTNAME']:
    for sub in ['0','1']:
        commands=\
            "#!/bin/bash                                    \n"+\
            "#SBATCH -p "+queue+"                           \n"+\
            "#SBATCH -o "+base+"/anaout"+sub+"              \n"+\
            "#SBATCH -D "+exedir+"                          \n"+\
            "#SBATCH -J an"+sub+"_"+name+"                  \n"+\
            "#SBATCH --get-user-env                         \n"+\
            "#SBATCH --mail-type=end                        \n"+\
            "#SBATCH --mail-user=armin.scrinzi@lmu.de       \n"+\
            "#SBATCH --export=NONE                          \n"+\
            "#SBATCH --time=40:00:00                        \n"+\
            "#SBATCH --ntasks=1                             \n"+\
            "cd "+os.getcwd()+"                             \n"+\
            exedir+"/tRecX "+args+"/inpc -unbound=1 -subregion="+sub+"\n"
        open(base+'anascript'+sub,'w').write(commands)
        os.system('sbatch '+base+'/anascript'+sub)


# LRZ linux cluster (SGE)
elif 'LRZlinux'==os.environ['HOST']:
    commands=\
               "#!/bin/bash                \n"+\
               "#$ -M armin.scrinzi@lmu.de \n"+\
               "#$ -S /bin/bash            \n"+\
               "#$ -N "+name+"             \n"+\
               "#$ -o "+base+"anaout       \n"+\
               "#$ -e "+base+"anaerr       \n"+\
               "#$ -l mf=1000M,h_rt=10:00:0 \n"+\
               "#$ -l march=x86_64         \n"+\
               ". /etc/profile             \n"+\
               "cd "+os.getcwd()+"         \n"+\
               "./Spectrum "+args+"         \n"
    open(base+'anascript','w').write(commands)
    os.system('qsub '+base+'anascript')

else:
    exit('undefined system: '+os.environ['HOST'])
    
