from computeSpectrum_static_functions import *
import pickle
import sys
from scipy import interpolate
on_lrz = False
if '/home/hpc/' in os.getcwd():
    on_lrz = True
if not on_lrz:
    from matplotlib import pyplot as plt


def run(cls_instance, i):
    return cls_instance.generate_kz_grid(i)

def plot_new_kz_spectrum(folder):
    kz_spectrum = np.loadtxt(folder + '/kzSpectrum.txt')
    kz_momenta = np.loadtxt(folder + '/kz.txt')
    if on_lrz:
        return
    fig = plt.contourf(kz_momenta, kz_momenta, np.log10(kz_spectrum), 50)
    plt.xlabel('$pz_1(a.u)$')
    plt.ylabel('$pz_2(a.u)$')
    plt.axis('equal')
    # print kz_matrix
    plt.colorbar(fig)
    plt.show()


class KzSpectrumInterpolate:
    def __init__(self, folder, number_of_angles=64, kz_number=64, proc_rank=0, proc_num=1):
        if folder[-1] != '/':
            folder += '/'
        self.folder = folder
        self.inputs = get_input_contents(self.folder)
        self.kz_number, self.number_of_angles, self.proc_rank, self.proc_num = kz_number, number_of_angles, proc_rank, proc_num 
        self.JADangles = range_double(0, np.pi, np.pi / (self.number_of_angles - 1))
        self.cosThetas = np.cos(self.JADangles)
        self.abs_momenta, self.lmax, self.appearing_m, self.DI_spec_files, self.SI_spec_files = load_basic_data(folder)
        abs_momenta_back = list(self.abs_momenta[:-1])
        abs_momenta_back = [0.] + abs_momenta_back
        self.d_k = np.array(self.abs_momenta) - np.array(abs_momenta_back)
        self.spherical_harmonics_1plus, self.spherical_harmonics_2plus, self.spherical_harmonics_1minus, \
        self.spherical_harmonics_2minus = initialize_spherical_harmonics(self.lmax, self.JADangles, self.appearing_m)
        self.kz_momenta = np.array(range(- self.kz_number + 1, self.kz_number)) * self.abs_momenta[-1] / self.kz_number
        self.d_kz, self.d_theta = self.abs_momenta[-1] / (self.kz_number - 1), np.pi / (self.number_of_angles - 1)
        if not os.path.isdir(self.folder + '/' + str(self.proc_rank)):
            os.mkdir(self.folder + str(self.proc_rank))
        self.k_range = self.calculate_k_range()
        self.average_num = 5
        self.add_JADs = False

    def sum_JAD(self):
        thetas = [0.]
        total_JAD = []
        for k1Idx in range(len(self.abs_momenta)):
            for k2Idx in range(len(self.abs_momenta)):
                JAD_file = self.folder + '/JADFromOld_%i_%i' % (k1Idx, k2Idx)
                contents = open(self.folder + '/JADFromOld').readlines()
                JAD_yields = [[]]
                for content in contents:
                    if '#' in content:
                        continue
                    if content.strip() == '':
                        JAD_yields.append([])
                        if k1Idx ==0 and k2Idx == 0:
                            thetas.append(0.)
                        continue
                    if k1Idx == 0 and k2Idx == 0:
                        thetas[-1] = np.arccos(float(content.split(',')[0])) * 180 / np.pi
                    JAD_yields[-1].append(float(content.split(',')[-1]))
                if k1Idx == 0 and k2Idx == 0:
                    total_JAD = np.array(JAD_yields) * self.abs_momenta[k1Idx] ** 2 * self.abs_momenta[k2Idx] ** 2 * self.d_k[k1Idx] * self.d_k[k2Idx]
                else:
                    total_JAD += np.array(JAD_yields) * self.abs_momenta[k1Idx] ** 2 * self.abs_momenta[k2Idx] ** 2 * self.d_k[k1Idx] * self.d_k[k2Idx]
            if on_lrz:
                return
            fig = plt.contourf(thetas, thetas, np.log10(total_JAD), 50)
            plt.xlabel('$\theta_1$')
            plt.ylabel('$\theta_2$')
            plt.axis('equal')
            # print kz_matrix
            plt.colorbar(fig)
            plt.show()            

    def calculate_k_range(self):
        numbers = np.ones(self.proc_num) * len(self.abs_momenta) / self.proc_num
        start_points, pointer = [], 0
        for i in range(self.proc_num):
            start_points.append(pointer)
            pointer += int(numbers[i])
        return start_points

    def find_kzIdx(self, kz):
        return int(np.sign(kz) * round(abs(kz) / self.d_kz)) + self.kz_number - 1

    def generate_kz_grid(self, proc_rank=0):
        scattering_amplitude_theta_grid = np.zeros([len(self.JADangles), len(self.JADangles)],
                                                       dtype=np.complex)
        k1_select = 16
        k2_select = 16
        # scattering_amplitude_theta_grid = np.zeros([2 * len(self.JADangles) - 1, 2 * len(self.JADangles) - 1],
        #                                                dtype=np.complex)
        k1_index_start = self.k_range[proc_rank]
        k1_index_end = 0
        kz_matrix = np.zeros([2 * self.kz_number - 1, 2 * self.kz_number - 1])
        if self.kz_number == self.number_of_angles and self.add_JADs:
            kz_matrix = np.zeros([2 * self.kz_number - 3, 2 * self.kz_number - 3])
        if proc_rank == self.proc_num - 1:
            k1_index_end = len(self.abs_momenta)
        else:
            k1_index_end = self.k_range[proc_rank + 1]
        allowed_pairs = [(6, 11)]
        scattering_amplitude_theta_grid_test = np.zeros([2 * len(self.JADangles) - 1, 2 * len(self.JADangles) - 1], dtype=np.complex)
        for k1Idx in range(k1_index_start, k1_index_end):
            for k2Idx in range(0, len(self.abs_momenta)):
                if (k1Idx, k2Idx) not in allowed_pairs and (k2Idx, k1Idx) not in allowed_pairs:
                    continue
                
                scattering_amplitude_theta_grid = np.zeros([len(self.JADangles), len(self.JADangles)], dtype=np.complex)
                if self.kz_number == self.number_of_angles and self.add_JADs:
                    scattering_amplitude_theta_grid_test = np.zeros([2 * len(self.JADangles) - 1, 2 * len(self.JADangles) - 1], dtype=np.complex)
                for mIndex in range(len(self.appearing_m)):
                    m = self.appearing_m[mIndex]
                    lmax_local = self.lmax - abs(m)
                    scattering_amplitude_fixedm_fixedks = self.read_in_DI_data(k1Idx, k2Idx, lmax_local, mIndex)
                    scattering_amplitude_theta_grid += \
                            get_kz_theta_scattering_amplitude(self.JADangles, scattering_amplitude_fixedm_fixedks, self.spherical_harmonics_1minus,
                            self.spherical_harmonics_1minus, mIndex)
                    #  if k1Idx == k1_select and k2Idx == k2_select:
                    scattering_amplitude_theta_grid_test += self.get_theta_scattering_amplitude(scattering_amplitude_fixedm_fixedks, mIndex)
                scattering_crossing_theta_grid = abs(scattering_amplitude_theta_grid) ** 2
                average_crossing_theta_grid = np.zeros([len(scattering_amplitude_theta_grid), len(scattering_amplitude_theta_grid)])
                average_crossing_theta_grid = scattering_crossing_theta_grid
                if self.kz_number != self.number_of_angles or not self.add_JADs:
                    for i in range(len(scattering_amplitude_theta_grid)):
                        kz1Idx = self.find_kzIdx(self.abs_momenta[k1Idx] * np.cos(self.d_theta * i))
                        for j in range(len(scattering_amplitude_theta_grid)):
                            kz2Idx = self.find_kzIdx(self.abs_momenta[k2Idx] * np.cos(self.d_theta * j))
                            value = self.integration_theta_k_test(k1Idx, k2Idx, self.d_theta * i, self.d_theta * j, average_crossing_theta_grid[i][j])
                            # if kz1Idx >= kz2Idx:
                            kz_matrix[kz1Idx][kz2Idx] += value
                else:
                    kz_matrix = kz_matrix + abs(scattering_amplitude_theta_grid_test) ** 2 * self.abs_momenta[k1Idx] ** 2 * self.abs_momenta[k2Idx] ** 2 * self.d_k[k1Idx] * self.d_k[k2Idx]
                if k2Idx % 8 == 0:
                    print ('%i,%i' % (k1Idx, k2Idx))
        return kz_matrix
    
    def get_theta_scattering_amplitude(self, scattering_amplitude_fixed_m_fixed_k, mindex):
        return get_theta_scattering_amplitude(self.JADangles, scattering_amplitude_fixed_m_fixed_k,
                                              self.spherical_harmonics_1plus, self.spherical_harmonics_2plus,
                                              self.spherical_harmonics_1minus, self.spherical_harmonics_2minus, mindex)

    def run(self):
        total_yields = np.zeros([self.kz_number, self.kz_number])
        ratio = 0.
        if not os.path.isfile(self.folder + '/kzSpectrum.txt'):
            import multiprocessing
            pool = multiprocessing.Pool(processes=self.proc_num)
            results = []
            for i in range(self.proc_num):
                results.append(pool.apply_async(run, (self, i)))
            # kz_matrix = np.zeros([2 * self.kz_number - 1, 2 * self.kz_number - 1])
            pool.close()
            pool.join()
            total_yields = np.zeros([2 * self.kz_number - 1, 2 * self.kz_number - 1])
            if self.kz_number == self.number_of_angles and self.add_JADs:
                total_yields = np.zeros([2 * self.kz_number - 3, 2 * self.kz_number - 3])
            for result in results:
                total_yields += (result.get())            
            total_yields = total_yields / (self.d_kz ** 2)
            ratio = (np.sum(total_yields[:self.kz_number, :self.kz_number]) + np.sum(total_yields[self.kz_number:, self.kz_number:])) \
                    / (np.sum(total_yields[:self.kz_number, self.kz_number:]) + np.sum(total_yields[self.kz_number:, :self.kz_number]))
            total_yields /= np.max(total_yields)
            total_yields[total_yields < 0.01] = 0.01
            np.savetxt(self.folder + '/kzSpectrum.txt', total_yields)
        else:
            total_yields = np.loadtxt(self.folder + '/kzSpectrum.txt')
            ratio = (np.sum(total_yields[:self.kz_number, :self.kz_number]) + np.sum(total_yields[self.kz_number:, self.kz_number:])) \
                    / (np.sum(total_yields[:self.kz_number, self.kz_number:]) + np.sum(total_yields[self.kz_number:, :self.kz_number]))
        print('ratio is %.4f' % ratio)
        if on_lrz:
            return
        fig = plt.contourf(self.kz_momenta[:len(total_yields)], self.kz_momenta[:len(total_yields)], np.log10(total_yields), 50)
        plt.xlabel('$p_x(a.u)$')
        plt.ylabel('$p_y(a.u)$')
        plt.axis('equal')
        # print kz_matrix
        plt.colorbar(fig)
        plt.show()

    def write_files(self, mIndex, k1Idx, k2Idx, theta1, theta2, ampl):
        kz1Idx, kz2Idx = self.find_kzIdx(self.abs_momenta[k1Idx] * np.cos(theta1)), self.find_kzIdx(self.abs_momenta[k2Idx] * np.cos(theta2))
        file_name = '%s/%i/interpolate_%i_%i_%i.bin' % (self.folder, self.proc_rank,self.appearing_m[mIndex], kz1Idx, kz2Idx)
        write_file = open(file_name, 'a')
        np.save(write_file, struct.pack('d', self.integration_theta_k(k1Idx, k2Idx, theta1, theta2, ampl)))
        write_file.close()

    def integration_theta_k(self, k1Idx, k2Idx, theta1, theta2, ampl):
        k1, k2 = self.abs_momenta[k1Idx], self.abs_momenta[k2Idx]
        dk1, dk2 = self.d_k[k1Idx], self.d_k[k2Idx]
        return abs(ampl) ** 2 * np.sin(theta1) * np.sin(theta2) * k1 ** 2 * k2 ** 2 * dk1 * dk2 * self.d_kz ** 2 * abs(np.cos(theta1)) ** 2 * abs(np.cos(theta2)) ** 2
    
    def integration_theta_k_test(self, k1Idx, k2Idx, theta1, theta2, cross):
        k1, k2 = self.abs_momenta[k1Idx], self.abs_momenta[k2Idx]
        dk1, dk2 = self.d_k[k1Idx], self.d_k[k2Idx]
        # return cross * np.sin(theta1) * np.sin(theta2) * k1 ** 2 * k2 ** 2 * dk1 * dk2 * self.d_kz ** 2 * abs(np.cos(theta1)) ** 2 * abs(np.cos(theta2)) ** 2
        return cross * np.sin(theta1) * np.sin(theta2) * k1 ** 2 * k2 ** 2 * dk1 * dk2 * self.d_kz ** 2


    def read_in_DI_data(self, k1_index, k2_index, lmax_local, m_index):
        return read_in_DI_data(lmax_local, self.DI_spec_files[m_index], self.DI_spec_files[
            len(self.appearing_m) - 1 - m_index], len(self.abs_momenta), k1_index, k2_index)

    def concat_binary(self):
        for i in range(self.proc_num):
            if not os.path.isdir(self.folder + '/' + str(i)):
                sys.exit('the folder %i does not exist' % i)
            for item in os.listdir(self.folder + '/' + str(i)):
                target_name = '%s/%s' % (self.folder, item)
                source_name = '%s/%i/%s' % (self.folder, i, item)
                if os.path.isfile(target_name):
                    concat_binary(target_name, source_name)
                else:
                    os.system('cp ' + source_name + ' ' + target_name)
            os.system('rm -r ' + self.folder + '/' + str(i))
            print ('%i done' % i)

 


if __name__ == "__main__":
    import sys
    proc_num = 1
    proc_rank = 0
    kz_number = 64
    kz_matrix = np.zeros([kz_number * 2 - 1, kz_number * 2 - 1])

    if len(sys.argv) == 4:
        proc_num = int(sys.argv[2])
        proc_rank = int(sys.argv[3])
    if len(sys.argv) > 2 and sys.argv[2] == 'new':
        plot_new_kz_spectrum(sys.argv[1])
        sys.exit('done of new')
    kzSpectrumInterpolate = KzSpectrumInterpolate(sys.argv[1], proc_num=proc_num, proc_rank=proc_rank)
    kzSpectrumInterpolate.run()
    # kzSpectrumInterpolate.write_kz_grid()
    # kzSpectrumInterpolate.concat_binary()
    # kzSpectrumInterpolate.write_kz_grid()