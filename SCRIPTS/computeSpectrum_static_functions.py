import numpy as np
import os
import re
import struct
from scipy.special import sph_harm

def initialize_spherical_harmonics(lmax, JADangles, appearing_m, add_harm=False):
    spherical_harmonics_1plus = []  # exp(+m) phi=0
    spherical_harmonics_2plus = []  # exp(+m) phi=pi
    spherical_harmonics_1minus = []  # exp(-m) phi=0
    spherical_harmonics_2minus = []  # exp(-m) phi=pi
    if add_harm:
        sphericalharmonics8plus, sphericalharmonics8minus = [], []
    for mindex in range(len(appearing_m)):
        m = appearing_m[mindex]
        spherical_harmonics_1plus.append(np.zeros([lmax - abs(m) + 1, len(JADangles)], dtype=np.double))
        spherical_harmonics_2plus.append(np.zeros([lmax - abs(m) + 1, len(JADangles)], dtype=np.double))
        spherical_harmonics_1minus.append(np.zeros([lmax - abs(m) + 1, len(JADangles)], dtype=np.double))
        spherical_harmonics_2minus.append(np.zeros([lmax - abs(m) + 1, len(JADangles)], dtype=np.double))
        if add_harm:
            sphericalharmonics8plus.append(np.zeros([lmax - abs(m) + 1, len(JADangles)], dtype=np.double))
            sphericalharmonics8minus.append(np.zeros([lmax - abs(m) + 1, len(JADangles)], dtype=np.double))
        for i in range(len(JADangles)):
            for varl in range(lmax - abs(m) + 1):
                spherical_harmonics_1plus[-1][varl, i] = np.real(sph_harm(m, varl + abs(m), 0, JADangles[i]))
                spherical_harmonics_2plus[-1][varl, i] = np.real(sph_harm(m, varl + abs(m), np.pi, JADangles[i]))
                spherical_harmonics_1minus[-1][varl, i] = np.real(sph_harm(-m, varl + abs(m), 0, JADangles[i]))
                spherical_harmonics_2minus[-1][varl, i] = np.real(sph_harm(-m, varl + abs(m), np.pi, JADangles[i]))
                if add_harm:
                    sphericalharmonics8plus[-1][varl, i] = np.real(
                        sph_harm(m, varl + abs(m), 0.5 * np.pi, JADangles[i]))
                    sphericalharmonics8minus[-1][varl, i] = np.real(
                        sph_harm(-m, varl + abs(m), 0.5 * np.pi, JADangles[i]))
    return spherical_harmonics_1plus, spherical_harmonics_2plus, spherical_harmonics_1minus, spherical_harmonics_2minus


def initialize_harm_kz(lmax, abs_momenta, appearing_m):
    kz_momentum = np.array(list(-abs_momenta[::-1]) + [0.] + list(abs_momenta))
    JADangles = [np.arccos(kz_momentum[i] / kz_momentum[-1]) for i in range(len(kz_momentum))]
    spherical_harmonics_1plus = []  # exp(+m) phi=0
    spherical_harmonics_2plus = []  # exp(+m) phi=pi
    spherical_harmonics_1minus = []  # exp(-m) phi=0
    spherical_harmonics_2minus = []  # exp(-m) phi=pi
    for mindex in range(len(appearing_m)):
        m = appearing_m[mindex]
        spherical_harmonics_1plus.append(np.zeros([lmax - abs(m) + 1, len(JADangles)], dtype=np.double))
        spherical_harmonics_2plus.append(np.zeros([lmax - abs(m) + 1, len(JADangles)], dtype=np.double))
        for i in range(len(JADangles)):
            for varl in range(lmax - abs(m) + 1):
                spherical_harmonics_1plus[-1][varl, i] = np.real(sph_harm(m, varl + abs(m), 0, JADangles[i]))
                spherical_harmonics_2plus[-1][varl, i] = np.real(sph_harm(m, varl + abs(m), np.pi, JADangles[i]))
    return spherical_harmonics_1plus, spherical_harmonics_2plus


def crop_harm_mat(k_index, harm_matrix):
    return_mat = np.zeros(harm_matrix.shape)
    return_mat[:, (harm_matrix.shape[1] - 1) / 2 - k_index:(harm_matrix.shape[1] - 1) / 2 + k_index] = harm_matrix[:, (harm_matrix.shape[1] - 1) / 2 - k_index:(harm_matrix.shape[1] - 1) / 2 + k_index]
    return return_mat


def get_theta_scattering_amplitude(JADangles, scattering_amplitude_fixedm_fixedks, spherical_harmonics_1plus,
                                   spherical_harmonics_2plus, spherical_harmonics_1minus, spherical_harmonics_2minus,
                                   mindex):
    scatteringAmplitudeThetaFixedk = np.zeros([len(JADangles) * 2 - 1, len(JADangles) * 2 - 1], dtype=np.complex)
    temp = np.dot(scattering_amplitude_fixedm_fixedks, spherical_harmonics_1minus[mindex])
    scatteringAmplitudeThetaFixedk[0:len(JADangles), 0:len(JADangles)] = np.dot(temp.transpose(),
                                                                                spherical_harmonics_1plus[mindex])
    temp2 = np.dot(temp.transpose(), spherical_harmonics_2plus[mindex])
    scatteringAmplitudeThetaFixedk[len(JADangles):, 0:len(JADangles)] = temp2[-1:0:-1, :]
    temp = np.dot(scattering_amplitude_fixedm_fixedks, spherical_harmonics_2minus[mindex])
    temp2 = np.dot(temp.transpose(), spherical_harmonics_1plus[mindex])
    scatteringAmplitudeThetaFixedk[0:len(JADangles), len(JADangles):] = temp2[:, -1:0:-1]

    temp2 = np.dot(temp.transpose(), spherical_harmonics_2plus[mindex])
    scatteringAmplitudeThetaFixedk[len(JADangles):, len(JADangles):] = temp2[-1:0:-1, -1:0:-1]
    return scatteringAmplitudeThetaFixedk

def get_kz_theta_scattering_cross(JADangles, scattering_amplitude_fixedm_fixedks, spherical_harmonics_1plus_kz, spherical_harmonics_2minus_kz, mindex, k1Index, k2Index):
    temp = np.dot(abs(scattering_amplitude_fixedm_fixedks) ** 2, crop_harm_mat(k1Index, spherical_harmonics_1plus_kz[mindex]))
    return (np.dot(temp.transpose(), crop_harm_mat(k2Index, spherical_harmonics_2minus_kz[mindex])[:,::-1]))

def get_kz_theta_amplitude(JADangles, scattering_amplitude_fixedm_fixedks, spherical_harmonics_1plus, mindex):
    temp = np.dot(scattering_amplitude_fixedm_fixedks, spherical_harmonics_1plus[mindex])
    return np.dot(temp.transpose(), spherical_harmonics_1plus[mindex])

def get_theta_grid(JAD_angles):
    theta_grid = np.zeros(len(JAD_angles) * 2 - 1)
    theta_grid[0:len(JAD_angles)] = JAD_angles
    theta_grid[len(JAD_angles):] = JAD_angles[1:] + np.pi
    return theta_grid



def saveJAD(folder, theta_grid, JAD_spectrum, E1, E2, modify=''):
    np.savetxt(get_JAD_theta_grid_filename(folder, 0, modify=modify), theta_grid)
    np.savetxt(get_JAD_spectrum_filename(folder, 0, modify=modify), JAD_spectrum)
    np.savetxt(get_JAD_energies_filename(folder, 0, 1, modify=modify), E1)
    np.savetxt(get_JAD_energies_filename(folder, 0, 2, modify=modify), E2)

def save_kz_projection_spectrum(kz_projection_spectrum, folder):
    np.savetxt(get_kz_projection_spectrum_filename(folder), kz_projection_spectrum)

def get_input_contents(path):
    keys = ['intensity', 'n_cycles', 'wavelength', 'pulseshape', 'scaling rad', 'tsurff rad', 'n_leg_interv',
            'order_legend', 'order_laguer', 'lag_expon', 'scale', 'angle', 'lmax', 'mmax', 'propag time',
            'average', 'pot cur start', 'pot cut end', 'RK tolerance', 'charge', 'lobattomax',
            'lambdamax', 'bin', 'hydro', 'folderNum', 'lmaxV', 'P1', 'P2', 'HighEV', 'Up', 'Nk', 'kNum']
    values = open(path + '/input_arguments.txt').readlines()[0].split()
    return dict(zip(keys, values))


def set_total_spectrum(total_spectrum, k1index, k2index, scattering_amplitude_fixed_m_fixed_ks):
    spectrum_fixed_m_fixed_ks = abs(scattering_amplitude_fixed_m_fixed_ks) ** 2
    total_spectrum[k1index, k2index] += np.sum(spectrum_fixed_m_fixed_ks)
    total_spectrum[k2index, k1index] = total_spectrum[k1index, k2index]
    return total_spectrum

def get_theta_indexes_by_yields(total_spectrum, ratio=0.01):
    theta_pic_indecies = np.ones([len(total_spectrum), len(total_spectrum)], dtype=np.int)
    theta_pic_indecies[total_spectrum > ratio * np.max(total_spectrum)] = 1
    return theta_pic_indecies

def transform_theta_pic_energies_to_indexes(E_of_k, E1, E2, energy_range_ev=0):
    theta_pic_indecies = np.zeros([len(E_of_k), len(E_of_k)], dtype=np.int)
    E1temp = E1
    E2temp = E2
    for angle_index in range(len(E1)):
        if energy_range_ev:
            energy_range = float(energy_range_ev) / (Ry2 / e)
            i1_min = get_indecies(E_of_k, E1[angle_index] - energy_range)
            i1_max = get_indecies(E_of_k, E1[angle_index] + energy_range)
            i2_min = get_indecies(E_of_k, E2[angle_index] - energy_range)
            i2_max = get_indecies(E_of_k, E2[angle_index] + energy_range)
            theta_pic_indecies[i1_min: i1_max + 1, i2_min: i2_max + 1] = 1
            print ('i1_min:%d ,i1_max: %d, i2_min:%d, i2_max%d' % (i1_min, i1_max, i2_min, i2_max))
        i1 = get_indecies(E_of_k, E1[angle_index])
        i2 = get_indecies(E_of_k, E2[angle_index])
        theta_pic_indecies[i1, i2] = 1
        theta_pic_indecies[i2, i1] = 1
        E1temp[angle_index] = E_of_k[i1]
        E2temp[angle_index] = E_of_k[i2]
    E1 = E1temp
    E2 = E2temp
    return theta_pic_indecies, E1, E2


def get_indecies(E_of_k, E):
    return int(np.argmin(abs(E_of_k - E)))


def load_basic_data(folder):
    appearing_m = get_ms(folder)
    file_list = os.listdir(folder)
    # print filelist
    # computed_momenta = get_computed_momenta(file_list)
    DI_spec_files = getDISpecFiles(file_list, folder, appearing_m)
    lmax = get_lmax(DI_spec_files, appearing_m)
    abs_momenta = get_momentum_grid(DI_spec_files)
    save_momenta(folder, abs_momenta)
    SI_spec_files = get_single_ionization_files(file_list, folder)
    return abs_momenta, lmax, appearing_m, DI_spec_files, SI_spec_files


def range_double(start, stop, step):
    vector_size = int(np.floor((stop - start) / step) + 1)
    return np.array(range(vector_size)) * step + np.ones(vector_size) * start


def save_momenta(folder, abs_momenta):
    np.savetxt(get_momenta_filename(folder), abs_momenta)


def save_total_spectrum(folder, total_spectrum):
    np.savetxt(get_total_DI_spectrum_filename(folder), total_spectrum)


def save_kz_projection_spectrum(folder, kz_projection_spectrum):
    np.savetxt(get_kz_projection_spectrum_filename(folder), kz_projection_spectrum)


def get_momenta_filename(folder):
    return folder + 'momenta.txt'


def get_total_DI_spectrum_filename(folder):
    return folder + 'total_DI_spectrum.txt'

def get_kz_projection_spectrum_filename(folder):
    return folder + 'kz_projection_spectrum.txt'

def get_JAD_spectrum_filename(folder, n, modify=''):
    return folder + modify + 'JAD_spectrum' + str(n) + '.txt'


def get_JAD_theta_grid_filename(folder, n, modify=''):
    return folder + modify + 'JAD_thetaGrid' + str(n) + '.txt'


def get_JAD_energies_filename(folder, n, i, modify=''):
    return folder + modify + 'JAD_energies' + str(n) + '_' + str(i) + '.txt'


def get_ms(folder):
    for m in range(-100, 1, 1):
        if os.path.isfile(folder + '/spectrum_test_' + str(m) + '_2_0.bin'):
            appearing_m = range(m, -m + 1, 1)
            return appearing_m


def get_computed_momenta(file_list):
    computed_momenta = []
    for i in file_list[0:]:
        name_length = 18
        if len(i) > name_length and i[0:name_length] == 'spectrum_test_0_2_':
            computed_momenta.append(int(i[name_length:re.search('.bin', i).start()]))
    return sorted(computed_momenta)


def get_single_ionization_files(file_list, folder):
    singleIonizationFiles = []
    my_sorting = []
    for i in file_list[0:]:
        if len(i) > 10 and i[0: 9] == 'testspec_':
            singleIonizationFiles.append(folder + i)
            my_sorting.append(int(i[9:re.search('.txt', i).start()]))
    sorting_array = [i[0] for i in sorted(enumerate(my_sorting), key=lambda x: x[1])]
    return [singleIonizationFiles[i] for i in sorting_array]


def getDISpecFiles(file_list, folder, m):
    # return all files with 'spectrum_test_-1_2_146'
    spec_files = []
    for mym in m:
        name_length = 18
        if mym < 0:
            name_length = 19
        spec_files_m = []
        my_sorting = []
        for i in file_list[0:]:
            if len(i) > name_length and i[0:name_length] == 'spectrum_test_' + str(mym) + '_2_':
                spec_files_m.append(folder + i)
                my_sorting.append(int(i[name_length:re.search('.bin', i).start()]))
        sorting_array = [i[0] for i in sorted(enumerate(my_sorting), key=lambda x: x[1])]
        spec_files.append([spec_files_m[i] for i in sorting_array])
    return spec_files


def get_lmax(spec_files, m):
    my_file = spec_files[(len(m) - 1) / 2][0]
    with open(my_file, mode='rb') as file:
        file_content = file.read(16)
        file_content = file.read(4)
        return struct.unpack("i", file_content)[0]


def get_number_of_momenta(spec_files, m):
    my_file = spec_files[(len(m) - 1) / 2][0]
    with open(my_file, mode='rb') as file:
        file_content = file.read(12)
        file_content = file.read(4)
        return struct.unpack("i", file_content)[0]


def get_momentum_grid(spec_files):
    abs_momenta = []
    for i in spec_files[0][0:]:
        with open(i) as file:
            fileContent = file.read(8)
            try:
                abs_momenta.append(struct.unpack("d", fileContent)[0])
            except:
                pass
    my_momenta = np.array(abs_momenta)
    return my_momenta

def read_in_DI_data(lmax_local, spec_filesFixedM1, spec_filesFixedM2, number_of_momenta, k1index, k2index):
    scattering_amplitude_fixedm_fixedks = np.zeros([lmax_local + 1, lmax_local + 1], dtype=np.complex)
    try:
        with open(spec_filesFixedM1[k1index]) as file:
            file.seek(20)
            for l in range(lmax_local + 1):
                length_to_skip = k2index * 2 * (lmax_local + 1) * 8
                # 2 doubles of size 8 per complex number
                file.seek(length_to_skip, 1)
                fileContent = file.read(2 * (lmax_local + 1) * 8)
                try:
                    dataInDoubles = np.asarray(struct.unpack("d" * 2 * (lmax_local + 1), fileContent))
                    dataInComplexes = np.vectorize(np.complex)(dataInDoubles[0:len(dataInDoubles):2],
                                                            dataInDoubles[1:len(dataInDoubles):2])
                    scattering_amplitude_fixedm_fixedks[l, :] += dataInComplexes
                    length_to_skip = (number_of_momenta - 1 - k2index) * 2 * (lmax_local + 1) * 8
                    file.seek(length_to_skip, 1)
                except:
                    print ('read error k1Index %i' % (k1index))
                    break
        with open(spec_filesFixedM2[k2index]) as file:
            file.seek(20)
            for l in range(lmax_local + 1):
                length_to_skip = k1index * 2 * (lmax_local + 1) * 8
                file.seek(length_to_skip, 1)
                fileContent = file.read(2 * (lmax_local + 1) * 8)
                try:
                    dataInDoubles = np.asarray(struct.unpack("d" * 2 * (lmax_local + 1), fileContent))
                    dataInComplexes = np.vectorize(np.complex)(dataInDoubles[0:len(dataInDoubles):2],
                                                            dataInDoubles[1:len(dataInDoubles):2])
                    scattering_amplitude_fixedm_fixedks[:, l] += dataInComplexes
                    length_to_skip = (number_of_momenta - 1 - k1index) * 2 * (lmax_local + 1) * 8
                    file.seek(length_to_skip, 1)
                except:
                    print ('read error k2Index %i' % (k2index))
                    break
    except IOError:
        print (str(k1index) + ' and ' + str(k2index) + ' has something wrong')
    return scattering_amplitude_fixedm_fixedks

def read_in_DI_data_test(lmax_local, spec_filesFixedM1, spec_filesFixedM2, number_of_momenta, k1index, k2index):
    scattering_amplitude_fixedm_fixedks = np.zeros([lmax_local + 1, lmax_local + 1], dtype=np.complex)
    try:
        with open(spec_filesFixedM1[k1index]) as file:
            file.seek(20)
            for l in range(lmax_local + 1):
                length_to_skip = k2index * 2 * (lmax_local + 1) * 8
                # 2 doubles of size 8 per complex number
                file.seek(length_to_skip, 1)
                fileContent = file.read(2 * (lmax_local + 1) * 8)
                try:
                    dataInDoubles = np.asarray(struct.unpack("d" * 2 * (lmax_local + 1), fileContent))
                    dataInComplexes = np.vectorize(np.complex)(dataInDoubles[0:len(dataInDoubles):2],
                                                            dataInDoubles[1:len(dataInDoubles):2])
                    scattering_amplitude_fixedm_fixedks[l, :] += dataInComplexes
                    length_to_skip = (number_of_momenta - 1 - k2index) * 2 * (lmax_local + 1) * 8
                    file.seek(length_to_skip, 1)
                except:
                    print ('read error k1Index %i' % (k1index))
                    break
    except IOError:
        print (str(k1index) + ' and ' + str(k2index) + ' has something wrong')
    return scattering_amplitude_fixedm_fixedks

def concat_binary(traget, source):
    with open(traget, "ab") as myfile:
        with open(source, "rb") as file2:
            myfile.write(file2.read())