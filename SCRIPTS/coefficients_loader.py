import sys
import os
import csv
import numpy as np
import scipy.special

"""
Python counterpart to CoefficientsWriter class
"""


def factorial(up, low):
    if up == low:
        return 1.
    else:
        return up * factorial(up-1, low)


class YlmGrid:
    def __init__(self, l_pts, m_pts):
        if isinstance(l_pts, list):
            self.l_pts = len(l_pts)
            self._l_grid = l_pts
        else:
            self.l_pts = l_pts
            self._l_grid = None
            self._l_grid = np.array([self.l_grid(i)
                                     for i in range(self.l_pts)])

        if isinstance(m_pts, list):
            self.m_pts = len(m_pts)
            self._m_grid = m_pts
        else:
            self.m_pts = m_pts
            self._m_grid = None
            self._m_grid = np.array([self.m_grid(i)
                                     for i in range(self.m_pts)])

        self.plm = [[]] * self.l_pts

    def l_grid(self, idx):
        if self._l_grid is not None:
            return self._l_grid[idx]
        return -1. + 2.*idx/(self.l_pts - 1)

    def m_grid(self, idx):
        if self._m_grid is not None:
            return self._m_grid[idx]
        if self.m_pts == 1:
            return 0
        return 0. + 2.*np.pi*idx/(self.m_pts - 1)

    def compute(self, lmax, mmax):
        self.plm = [[]] * self.l_pts
        for i in range(self.l_pts):
            val, der = scipy.special.lpmn(mmax, lmax, self.l_grid(i))

            for m in range(val.shape[0]):
                for l in range(m, val.shape[1]):
                    val[m][l] *= np.sqrt((2*l+1)/factorial(l+m, l-m)/(4*np.pi))
            self.plm[i] = val

        self.plm = np.array(self.plm)

    def y_lm(self, l, m):
        if len(self.plm[0]) <= abs(m):
            self.compute(l, m)
        if len(self.plm[0][abs(m)]) <= l:
            self.compute(l, m)

        f = 1.
        if m < 0 and (-m) % 2 == 1:
            f = -1.

        l_grid = f * self.plm[:, abs(m), l]
        m_grid = np.exp(1j * m * self._m_grid)

        return np.outer(l_grid, m_grid).flatten()

    def trafo(self, lm_indices):
        result = np.zeros(shape=(self.l_pts*self.m_pts, len(lm_indices)),
                          dtype=np.complex128)
        for i, lm in enumerate(lm_indices):
            result[:, i] = self.y_lm(int(lm[0]), int(lm[1]))

        return result


translate_dict = {
    'Phi': 'm',
    'Eta': 'l',
    'Phi1': 'm1',
    'Phi2': 'm2',
    'Eta1': 'l1',
    'Eta2': 'l2',
    'CoupledL': 'L',
    'CoupledM': 'M'
}


def translate(n):
    if n in translate_dict:
        return translate_dict[n]
    return n


class CoefficientsDescriptor:
    def __init__(self, hierarchy=None, indices=[]):
        self.hierarchy = hierarchy
        self.indices = np.array(indices)

        self.parent = None
        self.parent_associations = None

    def set_parent(self, parent, associations):
        self.parent = parent
        self.parent_associations = np.array(associations)

    def load(self, filename):
        self.hierarchy = None
        self.indices = []

        with open(filename, 'r') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                if self.hierarchy is None:
                    # Header row
                    self.hierarchy = []
                    for r in row:
                        t = translate(r)
                        while t in self.hierarchy:
                            t += "'"
                        self.hierarchy += [t]
                else:
                    idx = [float(r) for r in row]
                    if len(idx) != len(self.hierarchy):
                        raise Exception("Inconsistent")
                    self.indices += [idx]

        """
        DiscretizationSpectral leaves double m axes.
        Second one counts
        """
        if "m1" in self.hierarchy and "m1'" in self.hierarchy:
            self.hierarchy[self.hierarchy.index("m1")] = "dummy1"
            self.hierarchy[self.hierarchy.index("m1'")] = "m1"
        if "m2" in self.hierarchy and "m2'" in self.hierarchy:
            self.hierarchy[self.hierarchy.index("m2")] = "dummy2"
            self.hierarchy[self.hierarchy.index("m2'")] = "m2"

        self.indices = np.array(self.indices)

    def reduce_over(self, axes):
        new_hierarchy = []
        reduce_index = []
        for h in self.hierarchy:
            reduce_index += [h in axes]
            if h not in axes:
                new_hierarchy += [h]

        new_indices = []
        new_indices_dict = dict()
        associations = []
        for n_idx, idx in enumerate(self.indices):
            reduced_index = []
            for i in range(len(idx)):
                if not reduce_index[i]:
                    reduced_index += [idx[i]]

            idx_hash = ""
            for i in reduced_index:
                idx_hash += "%e_" % i

            found_idx = None
            if idx_hash in new_indices_dict:
                found_idx = new_indices_dict[idx_hash]
            else:
                new_indices += [reduced_index]
                associations += [[]]
                found_idx = len(new_indices) - 1
                new_indices_dict[idx_hash] = found_idx

            associations[found_idx] += [n_idx]

        result = CoefficientsDescriptor(new_hierarchy, new_indices)
        result.set_parent(self, associations)
        return result


class Coefficients:
    def __init__(self, descriptor, params={}):
        self.descriptor = descriptor
        self.params = params
        self.values = []

    def load(self, filename):
        values = np.fromfile(filename, dtype=np.float64)
        self.values = values[:-1:2] + 1.j*values[1::2]
        self.time = values[-1]

    def reduce_from(self, coefficients, reducer):
        if self.descriptor.parent != coefficients.descriptor:
            raise Exception("Unsupported")

        self.values = np.zeros(shape=(len(self.descriptor.indices),),
                               dtype=coefficients.values.dtype)
        for i, a in enumerate(self.descriptor.parent_associations):
            self.values[i] = reducer(coefficients.values[a])

    def reduce_over(self, axes, reducer):
        descriptor = self.descriptor.reduce_over(axes)
        result = Coefficients(descriptor, self.params)
        result.reduce_from(self, reducer)
        return result

    def pretty_print(self):
        print("----------------------------------")
        print(self.descriptor.hierarchy, self.params)
        print("----------------------------------")
        for i, v in zip(self.descriptor.indices, self.values):
            print("%s: %s" % (i, v))

    def transform_ylm_grid(self, l_axis, m_axis, l_pts, m_pts):
        desc = self.descriptor.reduce_over([l_axis, m_axis])

        result_hierarchy = desc.hierarchy + [l_axis+"_grid", m_axis+"_grid"]
        result_indices = []
        result_values = []

        grid = YlmGrid(l_pts, m_pts)

        if isinstance(l_pts, list):
            l_pts = len(l_pts)
        if isinstance(m_pts, list):
            m_pts = len(m_pts)

        print("Transforming (%d)..." % (len(desc.parent_associations)/100))
        counter = 0
        for i in range(len(desc.parent_associations)):
            counter += 1
            if counter % 100 == 0:
                print(".", end='', flush=True)
            rhs_indices = self.descriptor.indices[desc.parent_associations[i]]
            rhs_indices = rhs_indices[:, [self.descriptor.hierarchy.index(
                l_axis), self.descriptor.hierarchy.index(m_axis)]]

            rhs_values = self.values[desc.parent_associations[i]]
            lhs_values = np.dot(grid.trafo(rhs_indices), rhs_values)

            for l in range(l_pts):
                for m in range(m_pts):
                    result_indices += [list(desc.indices[i]) +
                                       [grid.l_grid(l), grid.m_grid(m)]]
            result_values += list(lhs_values)
        print("\n...done")

        desc = CoefficientsDescriptor(result_hierarchy,
                                      np.array(result_indices))
        result = Coefficients(desc)
        result.values = np.array(result_values)
        return result


def load_folder(folder):
    descriptor = CoefficientsDescriptor()
    descriptor.load(folder + "/desc.csv")

    coefficients = []

    print("Loading coefficients...")
    for f in os.listdir(folder):
        if not f.endswith("desc.csv"):
            coeff = Coefficients(descriptor, {'name': f})
            coeff.load(folder + "/" + f)
            coefficients += [coeff]
            print(".", end='', flush=True)

    print("\n...done")
    return descriptor, sorted(coefficients, key=lambda c: c.time)


def load_folder_gen(folder):
    descriptor = CoefficientsDescriptor()
    descriptor.load(folder + "/desc.csv")

    print("Loading coefficients...")
    files = []
    for f in os.listdir(folder):
        try:
            t = float(f)
            files += [(t, f)]
        except Exception:
            pass

    files = sorted(files, key=lambda c: c[0])

    for _, f in files:
        if not f.endswith("desc.csv"):
            coeff = Coefficients(descriptor, {'name': f})
            coeff.load(folder + "/" + f)
            yield coeff
            print(".", end='', flush=True)

    print("\n...done")


if __name__ == '__main__':
    descriptor, coeffs = load_folder(sys.argv[1])
