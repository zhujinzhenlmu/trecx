#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
"""
plot one or several data-files with column-wise storage

columns can be specified in square brackets after the file name
        in the form fileName[2,3,7-9] for plotting columns 2,3,7,8,9
        without square bracket, all columns are plotted
column 0 of each file is interpreted as x-axis by default
       for different x-axis use [3:7,8], which plot columns 7,8 against 3
column headers will be recognized and used as legend labels
"""

import sys
import time

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.interpolate
from scipy.interpolate import griddata
import copy  as cp
from math import *

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import colors, ticker, cm
from matplotlib.colors import LogNorm
from matplotlib.widgets import Cursor

from tools import *
from plotTools import *

(files,flags) = argsAndFlags();

if len(files)<1:
    print "Usage: "
    print "   plotColPlot.py file1[{xcol:}colrange] file2[{xcol:}colrange] ... {flags}"
    print "Example:"
    print "   plot -dir=Argon -which=spec[3:22,26,30] 0015 0014 -label='I(W/cm2),lambda(nm)'"
    print "     will plot column 3 vs columns 22,26,30 of files Argon/0015/spec and Argon/0014/spec"
    print "     with legend labels including intensity and wavelength"
    print"Command line flags:"
    print"  -dir=DIR......... prefix input file by DIR/"
    print"  -which=WHICH..... postfix input file by {/}WHICH"
    print"  -label=PARNAME... parameter value for PARNAME will be used in plot legend"
    print"  -linY.... plot on linear y-axis (default is log)"
    print"  -peaks... mark photon peaks at n*omega-Up-Ip"
    print"  -eV...... x -> x^2/2 *Rydberg"
    sys.exit(0)

dir,which,rows=prePost(flags)

# get total number of datasets to be plotted
nSets=0
for f in files:
    file=dir+f+which
    if file.find("[")==-1:
        nSets+=1
    else:
        nSets=nSets+len(expandRange(file[max(file.find('['),file.find(':'))+1:file.find(']')]) )

replot=False
def allPlots():

    plot=Plot(flags) # accumulate info for graph
    previous=""
    previousDir=""
    nPlot=-1
    fig=plt.figure()

    #loop through files
    for info in files:
        file=dir+info+which

        # read parameters (if any)
        pars=RunParameters(file[:file.rfind('/')])

        # get data into selected columns
        datfil=DataFile(file)

        # loop through columns in file
        for col in datfil.datCols:
            nPlot+=1
            axn=plot1dim(flags,dir,which,info,pars,fig,nPlot,nSets,datfil,plot,col)
            axn.set_xlabel("Re(E)",fontsize=14)
            axn.set_ylabel("Im(E)",fontsize=14)
            axn.set_xlim(plot.dim[0])
            axn.set_ylim(plot.dim[2])

    return fig

fax=allPlots()

# direct termination from figure
def press(event):
    """ dug this out from somewhere """
    sys.stdout.flush()
    if event.key=='q': sys.exit(0)
fax.canvas.mpl_connect('key_press_event', press)

plt.show(block=False)
ans=''
answer = {}
answer['key']=''
    # Getch() and mpl_connect somehow compete
    # while the following does not compete...
ans=raw_input("<enter> to quit")
#    ans= Getch()()
print ""
# get python file name (as run from the command line)
name=sys.argv[0].split(".py")[0]
fax.savefig(name+".png")

