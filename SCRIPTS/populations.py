#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
"""
extract one or several items from column wise data file
plot the data of each row separately as a function vs. file
"""

import sys
import time

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.interpolate
from scipy.interpolate import griddata
import copy  as cp
from math import *

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import colors, ticker, cm
from matplotlib.colors import LogNorm
from matplotlib.widgets import Cursor

from tools import *
from plotTools import *

(files,flags) = argsAndFlags();

if len(files)<1:
    print "Usage: "
    print "   plotColPlot.py file1[{xcol:}colrange] file2[{xcol:}colrange] ... {flags}"
    print "Example:"
    print "   plot -dir=Argon -which=[2,3] -rows=2-5 0015/sp1 0014/sp1"
    sys.exit(0)

dir,which,rows=prePost(flags)


#loop through files
vals=[]
lamb=[]
for r in range(len(rows)): vals.append([])

for f in range(len(files)):
    file=dir+files[f]+which

    pars=RunParameters(file[:file.rfind("/")])
    lamb.append(pars.floatItem("Laser:lambda(nm)",0))

    # get data into selected columns
    datfil=DataFile(file)

    for r in range(len(rows)):
        vals[r].append(0)
        # loop through columns in file
        for col in datfil.datCols:
            vals[r][f]+=datfil.column(col)[rows[r]]**2
            #print file,"row",rows[r],"val",datfil.column(col)[rows[r]]

for r in range(len(rows)):
    plt.plot(lamb,vals[r],'.-',label=rows[r])


plt.legend()
print "wave lengths:",lamb
plt.show(block=False)
answer = raw_input('<return> to finish')


# get python file name (as run from the command line)
name=sys.argv[0].split(".py")[0]
plt.savefig(name+".png")

