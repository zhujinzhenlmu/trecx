#!/usr/bin/env python3.0
import os
import time

def execute(capp, command):
    folder = "/home/hpc/pr47cu/ru54hiv"
    os.system("rm -f %s %s %s" % (os.path.join(folder, "tmp_script"),
                                  os.path.join(folder, "tmp_out"),
                                  os.path.join(folder, "tmp_err")));
    with open(os.path.join(folder, "tmp_script"), "w") as script:
       script.write("#!/bin/bash\n")
       script.write("#SBATCH -p capp_medium\n")
       script.write("#SBATCH -o tmp_out\n")
       script.write("#SBATCH -e tmp_err\n")
       script.write("#SBATCH -D %s\n" % folder)
       script.write("#SBATCH --get-user-env\n")
       script.write("#SBATCH --time=0:10:00\n")
       script.write("#SBATCH --ntasks=1\n")
       script.write("#SBATCH --mem-per-cpu=1000\n")
       script.write("#SBATCH --nodelist=%s\n" % capp)
       script.write(command)

    os.system("sbatch %s" % os.path.join(folder, "tmp_script"))
    for i in range(5):
        time.sleep(3)
        if os.path.isfile(os.path.join(folder, "tmp_out")):
            with open(os.path.join(folder, "tmp_out"),"r") as outf:
                print(outf.read())
            break


while True:
    capp = input("Enter node? ")
    if capp == "":
        break
    command = input("Enter command? ")
    confirm = input("Execute '%s' on %s? (Y/n) " % (command, capp))
    if confirm != "n" and confirm != "N":
        if capp=="all":
            for i in range(18,36):
                print("capp%d... " % i)
                execute("capp%d" % i, command)
        else:
            execute(capp, command)
