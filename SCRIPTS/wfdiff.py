#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
import sys
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.interpolate
from scipy.interpolate import griddata

#plt.rcParams.update({'font.size': 14})

# get python file name (as run from the command line)
name=sys.argv[0].split(".py")[0]

if len(sys.argv)<2:
    print "--- supply time argument ---"
    sys.exit(0);

num=sys.argv[1]
dire="./"
data=[
["fd/0203/wf"+num,"lege/lagu(20)"],
["fd/0178/wf"+num,"400,0.5"],
["fd/0213/wf"+num,"140,0.5,exp[0.1]"],
["fd/0212/wf"+num,"140,0.5,exp[0.2]"],
["fd/0217/wf"+num,"120,0.5,exp[0.2]"],
["fd/0218/wf"+num,"140,0.5,exp[0.2]"],
["fd/0219/wf"+num,"lege/lagu(10)"],
["fd/0220/wf"+num,"160,0.5,exp[0.2]"],
]
pltRange=25
xsmooth=0.5 # smoothing range (au)
refer=0                # reference calculation
plots=[6,1,2,3,4,5,7] # list of runs to plot

# Two panels
fax,(ax1,ax2) = plt.subplots(nrows=2,sharex=True)
gs = gridspec.GridSpec(2,1,height_ratios=[1,3])

# upper panel
ax1 = plt.subplot(gs[0])
ax1.set_xlim([-pltRange,pltRange])
ax1.set_ylim([1.e-4,1])

# lower panel
ax2 = plt.subplot(gs[1])
ax2.set_xlim([-pltRange,pltRange])
ax2.set_ylim([1.e-14,10])

# plot colors for easier comparison
colr=['red','green','blue','cyan','magenta','brown']

# reference calculation
f = open(dire+data[refer][0])
v1 = f.readlines()
f.close()
x =  [float(v1[i].split(",")[0]) for i in range(1,len(v1))]
f1 = [float(v1[i].split(",")[1]) for i in range(1,len(v1))]
dat=data[refer]
lab=dat[1]
if len(dat)>2: lab+="|"+str(dat[2])+","+str(dat[3])
ax1.semilogy(x,f1,label=lab,color='black')
lg = ax1.legend()
lg.draw_frame(False)
d1=x[len(x)/2]-x[len(x)/2-1]


# smoothed density over x+-xsmooth for error estimates 
smooth=np.zeros((len(f1),))
for i in range(len(f1)):
    count=0
    for j in range(i,len(f1)):
        if x[j]>x[i]+xsmooth: break
        smooth[i]+=f1[j]
        count+=1
    for j in range(i,0,-1):
        if x[j]<x[i]-xsmooth: break
        smooth[i]+=f1[j]
        count+=1
    smooth[i]/=count

# ax1.semilogy(x,smooth)

ic=0
for k in plots:
    dat=data[k]
    ic=(ic+1)%len(colr)


    # get first and second column from file
    f = open(dire+dat[0])
    v2 = f.readlines()
    f.close()
    x2 = np.array([float(v2[i].split(",")[0]) for i in range(1,len(v2))])
    f2 = np.array([float(v2[i].split(",")[1]) for i in range(1,len(v2))])
    d2=x2[len(x2)/2]-x2[len(x2)/2-1]

    print "spacings: ",d1,d2,d1/d2

    # transform: stretch coordinates, multiply by factors
    if len(dat)>2:
        for i in range(len(x2)):
            if x2[i]> dat[2]:
                x2[i]=dat[2]+(x2[i]-dat[2])*dat[3]
                f2[i]=f2[i]/dat[3]
            if x2[i]<-dat[2]:
                x2[i]=-dat[2]+(x2[i]+dat[2])*dat[3]
                f2[i]= f2[i]/dat[3]

    fx=griddata(x2,f2,(x,),method='linear')*d1/d2

    ax1.semilogy(x,fx,color=colr[ic])
    lab=dat[1]
    if len(dat)>2: lab+="|"+str(dat[2])+","+str(dat[3])
    ax2.semilogy(x,abs((fx-f1))/smooth,label=lab,color=colr[ic])

ax1.set_ylabel("Electron density $n_0(x)$",fontsize=14)

#plt.axhline(y=1e-3)

ax2.set_xlabel("x (a.u.)",fontsize=14)
ax2.set_ylabel("Relative error",fontsize=14)

# Print legend without the box
lg = plt.legend(bbox_to_anchor=(1, 1))
lg.draw_frame(False)

# Merge the x axis with lower panel
fax.subplots_adjust(hspace=0.1)
plt.setp([a.get_xticklabels() for a in fax.axes[:-1]], visible=False)

print "Plot parameters:\n smoothing Dx=",xsmooth
plt.savefig(name+".png")

plt.show()

