import os

top = os.popen("top -n1 -b | grep orted").read()
ps_aux_raw = os.popen("ps aux | grep orted").read()

# ps aux contains this script, grep, ...
ps_aux = ""
for l in ps_aux_raw.split("\n"):
    if "diagnose_orted.py" in l:
        continue
    if "grep" in l:
        continue
    ps_aux += l  + "\n"

top = top.strip()
ps_aux = ps_aux.strip()

print("***********************************************")
print("ORTED DIAGNOSTICS:")
print(top)
print("-----------------------------------------------")
print(ps_aux)
print("***********************************************")
