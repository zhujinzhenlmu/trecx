#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
from constants import Ry2, e
from matplotlib.ticker import FormatStrFormatter
from static_methods import LaserProperty
import sys
import os
import matplotlib
from calculate_integration import IntegrationProbability
from computeSpectrum_static_functions import *

font = {
    'weight': 'bold',
    'size': 24}
matplotlib.rc('font', **font)
TRECX_FOLDER = '/home/jinzhen/projects/develop/TDSEsolver/HeModel/'
up_limit = 0.


class DITool:
    def __init__(self, folder, number_of_angles=64, compare_folder='', compare='',
                 use_ev='', compare_DI='', E1='', E2='', stripe_JAD='', range_stripe='', energy_range_ev='',
                 write_spec_total='', cut_off='', log_scale=1, include_k=1, fix_one_angle='', write_fix_one_angle='',
                 boxSize='', propapate_cycle='', sum_angle='', normalize=1, compare_SI='', kz_number='0'):
        self.folder = folder
        self.use_ev, self.compare_DI, self.E1, self.E2, self.stripe_JAD, self.range_stripe, self.energy_range_ev = \
            use_ev, compare_DI, E1, E2, stripe_JAD, range_stripe, energy_range_ev
        self.write_spec_total, self.cut_off, self.log_scale, self.include_k, self.write_fix_one_angle = \
            write_spec_total, cut_off, log_scale, include_k, write_fix_one_angle
        self.boxSize, self.propapate_cycle, self.compare_folder, self.compare, self.sum_angle, self.fix_one_angle,\
            self.compare_SI = boxSize, propapate_cycle, compare_folder, compare, sum_angle, fix_one_angle, compare_SI
        self.normalize = float(normalize)
        self.inputs = get_input_contents(self.folder)
        self.wavelength = float(self.inputs['wavelength'])
        self.intensity = float(self.inputs['intensity'])
        self.abs_momenta, self.lmax, self.appearing_m, self.DI_spec_files, self.SI_spec_files = \
            load_basic_data(folder)
        self.number_of_momentas = len(self.abs_momenta)
        self.number_of_angles = number_of_angles
        self.JADangles = range_double(
            0, np.pi, np.pi / (self.number_of_angles - 1))
        self.theta_grid = get_theta_grid(self.JADangles)
        self.spherical_harmonics_1plus, self.spherical_harmonics_2plus, self.spherical_harmonics_1minus, \
            self.spherical_harmonics_2minus = initialize_spherical_harmonics(
                self.lmax, self.JADangles, self.appearing_m)
        self.kz_number = int(kz_number)
        self.d_abs_momenta = [self.abs_momenta[0]] + [self.abs_momenta[i] -
                                                      self.abs_momenta[i-1] for i in range(1, len(self.abs_momenta))]
        if os.path.isfile(get_kz_projection_spectrum_filename(folder)):
            self.kz_projection_spectrum = np.loadtxt(
                get_kz_projection_spectrum_filename(folder))
            self.kz_number = (len(self.kz_projection_spectrum) + 1) / 2
            self.d_kz = self.abs_momenta[-1] / (self.kz_number - 1)
            self.kz_momentum = np.array(
                list(-self.abs_momenta[::-1]) + [0.] + list(self.abs_momenta))
            self.kz_back = np.array(
                [self.kz_momentum[0]] + list(self.kz_momentum[:-1]))
            self.d_kz = np.array(self.kz_momentum) - np.array(self.kz_back)
            self.kz_number = 0
        if self.kz_number:
            self.spherical_harmonics_1plus_kz, self.spherical_harmonics_2plus_kz = initialize_harm_kz(
                self.lmax, self.abs_momenta, self.appearing_m)
            self.kz_momentum = np.array(
                list(-self.abs_momenta[::-1]) + [0.] + list(self.abs_momenta))
            self.kz_back = np.array(
                [self.kz_momentum[0]] + list(self.kz_momentum[:-1]))
            self.d_kz = np.array(self.kz_momentum) - np.array(self.kz_back)
            self.kz_projection_spectrum = np.zeros(
                [len(self.kz_momentum), len(self.kz_momentum)])
            self.kz_ampl = np.zeros(
                [len(self.kz_momentum), len(self.kz_momentum)], dtype=np.complex)
        self.figure_index = 0
        self.energy_factor = 1
        if self.use_ev:
            self.energy_factor = Ry2 / e
        energy_momenta_back = list(self.abs_momenta[:-1] ** 2)
        energy_momenta_back = [0] + energy_momenta_back
        # check if we need to create total DI spectrum and initialize it
        if os.path.isfile(get_total_DI_spectrum_filename(folder)):
            self.get_total_DI = False
            self.total_spectrum = np.loadtxt(
                get_total_DI_spectrum_filename(folder))
            if self.compare_DI:
                total_spectrum_compare = np.loadtxt(
                    get_total_DI_spectrum_filename('/'.join(folder.split('/')[:-2]) + '/' +
                                                   self.compare_DI + '/'))
                self.total_spectrum = abs(self.total_spectrum - total_spectrum_compare) / \
                    max(self.total_spectrum.max(), total_spectrum_compare.max())
        else:
            self.get_total_DI = True
            self.total_spectrum = np.zeros(
                [self.number_of_momentas, self.number_of_momentas], dtype=np.double)
        # check if we need to create total JAD spectrum and initialize it
        if os.path.isfile(get_JAD_spectrum_filename(folder, 0)):
            self.get_JAD = False
            self.JAD_spectrum = np.loadtxt(
                get_JAD_spectrum_filename(folder, 0))
            self.theta_grid = np.loadtxt(
                get_JAD_theta_grid_filename(folder, 0))
            self.E1_select = np.loadtxt(
                get_JAD_energies_filename(folder, 0, 1))
            self.E2_select = (np.loadtxt(
                get_JAD_energies_filename(folder, 0, 2)))
        else:
            self.get_JAD = True
            self.E1_select, self.E2_select = 1, 1
            if self.E1:
                self.E1_select = float(self.E1) / self.energy_factor
            if self.E2:
                self.E2_select = float(self.E2) / self.energy_factor
            self.theta_pic_indecies, E1_select, E2_select = \
                transform_theta_pic_energies_to_indexes(self.abs_momenta ** 2 * 0.5,
                                                        [self.E1_select], [self.E2_select])
            if self.E1 and float(self.E1) < 0:
                self.theta_pic_indecies = get_theta_indexes_by_yields(
                    self.total_spectrum, float(self.E2))
            self.E1_select, self.E2_select = E1_select[0], E2_select[0]
            self.JAD_spectrum = np.zeros([self.number_of_angles * 2 - 1, self.number_of_angles * 2 - 1],
                                         dtype=np.double)
            print('selected ')
            print(E1_select)
            print(E2_select)
        # check if we need to create total stripe JAD spectrum and initialize it
        self.get_stripe_JAD, self.plot_stripe_JAD = False, False
        if self.stripe_JAD:
            self.plot_stripe_JAD, self.plot_stripe_JAD = True, True
            if os.path.isfile(get_JAD_spectrum_filename(folder, 0, modify='stripe_')):
                self.get_stripe_JAD = False
                self.stripe_JAD_spectrum = np.loadtxt(
                    get_JAD_spectrum_filename(folder, 0, modify='stripe_'))
                self.theta_grid = np.loadtxt(
                    get_JAD_theta_grid_filename(folder, 0, modify='stripe_'))
                self.low_energy = np.loadtxt(
                    get_JAD_energies_filename(folder, 0, 1, modify='stripe_'))[0]
                self.high_energy = np.loadtxt(
                    get_JAD_energies_filename(folder, 0, 2, modify='stripe_'))[0]
            else:
                self.low_energy = float(
                    self.stripe_JAD) - float(self.range_stripe)
                self.high_energy = float(
                    self.stripe_JAD) + float(self.range_stripe)
                self.stripe_theta_pic_indecies, self.low_energy, self.high_energy = \
                    transform_theta_pic_energies_to_indexes(
                        self.abs_momenta ** 2 * 0.5, self.low_energy, self.high_energy,
                        energy_range_ev=self.high_energy-self.low_energy)
                self.stripe_JAD_spectrum = np.zeros([self.number_of_angles * 2 - 1, self.number_of_angles * 2 - 1],
                                                    dtype=np.double)

    def get_DI_JAD(self):
        if not (self.get_total_DI or self.get_JAD or self.get_stripe_JAD):
            return
        if self.get_stripe_JAD:
            E = self.abs_momenta ** 2
            low_index = len(np.where(E <= self.low_energy)[0])
            high_index = len(np.where(E <= self.high_energy)[0])
            stripe_scattering_amplitude_theta_grid = np.zeros(
                [len(self.JADangles) * 2 - 1, len(self.JADangles) * 2 - 1],
                dtype=np.complex)
        if self.get_JAD:
            scattering_amplitude_theta_grid = np.zeros([len(self.JADangles) * 2 - 1, len(self.JADangles) * 2 - 1],
                                                       dtype=np.complex)
        for k1index in range(self.number_of_momentas):
            for k2index in range(k1index, self.number_of_momentas):
                for mindex in range(len(self.appearing_m)):
                    m = self.appearing_m[mindex]
                    lmax_local = self.lmax - abs(m)
                    if self.get_total_DI or (self.get_JAD and (self.theta_pic_indecies[k1index, k2index] or
                                                               self.theta_pic_indecies[k1index, k2index])) or \
                            (self.get_stripe_JAD and low_index <= k1index + k2index <= high_index) or self.kz_number:
                        scattering_amplitude_fixedm_fixedks = self.read_in_DI_data(
                            k1index, k2index, lmax_local, mindex)
                        if self.kz_number:
                            print('get kz at %i,%i' % (k1index, k2index))
                            self.get_kz_scattering_amplitude(
                                scattering_amplitude_fixedm_fixedks, mindex, k1index, k2index)
                    if self.get_total_DI:
                        self.total_spectrum = set_total_spectrum(self.total_spectrum, k1index, k2index,
                                                                 scattering_amplitude_fixedm_fixedks)
                    if self.get_JAD and (self.theta_pic_indecies[k1index, k2index] or
                                         self.theta_pic_indecies[k2index, k1index]):
                        print('The point we select is %d,%d' %
                              (k1index, k2index))
                        print(self.E1_select)
                        print(self.E2_select)
                        int_element = self.abs_momenta[k1index] ** 2 * self.abs_momenta[k2index] ** 2 * self.d_abs_momenta[k1index] * self.d_abs_momenta[k2index] \
                            + self.abs_momenta[k2index] ** 2 * self.abs_momenta[k1index] ** 2 * \
                            self.d_abs_momenta[k2index] * \
                            self.d_abs_momenta[k1index]
                        scattering_amplitude_theta_grid += \
                            abs(self.get_theta_scattering_amplitude(
                                scattering_amplitude_fixedm_fixedks, mindex)) ** 2 * int_element
                        # print('The total yield at this point is %.4e ' % (np.sum(abs(scattering_amplitude_fixedm_fixedks) ** 2  * self.abs_momenta[k1index] ** 2 * self.abs_momenta[k2index] ** 2 * self.d_abs_momenta[k1index] * self.d_abs_momenta[k2index])))
                        # JAD_int_method = abs(np.tensordot(np.sin(self.theta_grid), np.sin(self.theta_grid), 0)) * (self.theta_grid[1] - self.theta_grid[0]) ** 2
                        # print('The yield by theta is %.4e' % (np.sum(scattering_amplitude_theta_grid * JAD_int_method) * 4 * np.pi ** 2 / 4))
                    if self.get_stripe_JAD and low_index <= k1index + k2index <= high_index:
                        # phi momentum m
                        stripe_scattering_amplitude_theta_grid += \
                            self.get_theta_scattering_amplitude(
                                scattering_amplitude_fixedm_fixedks, mindex) * self.abs_momenta[k1index] * self.abs_momenta[k2index] * self.d_abs_momenta[k1index] * self.d_abs_momenta[k2index]
                #     print 'mindex'
            if k1index % 8 == 0:
                print(k1index)
        if self.get_stripe_JAD:
            self.stripe_JAD_spectrum = abs(
                stripe_scattering_amplitude_theta_grid) ** 2
            saveJAD(self.folder, self.theta_grid, self.stripe_JAD_spectrum, [self.low_energy], [self.high_energy],
                    modify='stripe_')
        if self.get_JAD:
            self.JAD_spectrum = abs(scattering_amplitude_theta_grid)
            # self.JAD_spectrum = abs(scattering_amplitude_theta_grid) ** 2
            saveJAD(self.folder, self.theta_grid, self.JAD_spectrum,
                    [self.E1_select], [self.E2_select])
        if self.get_total_DI:
            print('start saving %.6e' % self.total_spectrum[0][0])
            save_total_spectrum(self.folder, self.total_spectrum)
        if self.kz_number:
            self.kz_projection_spectrum += self.kz_projection_spectrum.transpose()
            save_kz_projection_spectrum(
                self.folder, self.kz_projection_spectrum)

    def plot_JAD_normal(self, JAD_spectrum_in, E1, E2, get_norm, stripe, use_compare="", log_scale=True):
        # JAD_spectrum = abs(JAD_spectrum)
        JAD_spectrum = np.zeros(JAD_spectrum_in.shape) + JAD_spectrum_in
        JAD_max = np.max(JAD_spectrum)
        angleCap = JAD_max * 0.01
        JAD_spectrum[JAD_spectrum < angleCap] = angleCap
        if get_norm or (not bool(use_compare)):
            angle_normalize = 1.0 / JAD_max
        else:
            angle_normalize = 1
        JAD_spectrum *= angle_normalize
        plt.figure(self.figure_index)
        d_JAD = np.abs(np.tensordot(
            np.sin(self.theta_grid), np.sin(self.theta_grid), 0))
        total_yield = np.sum(JAD_spectrum_in * d_JAD *
                             (self.theta_grid[1] - self.theta_grid[0]) ** 2) / 4 * 4 * np.pi ** 2
        print('The total yield by JAD is %.4e. if not total, just ignore it' % total_yield)
        JAD_spectrum = JAD_spectrum * d_JAD
        JAD_for_ratio = JAD_spectrum[:len(self.theta_grid) / 2, :len(
            self.theta_grid) / 2]
        ratio = (np.sum(JAD_for_ratio[:len(JAD_for_ratio) / 2, :len(
            JAD_for_ratio) / 2]) + np.sum(JAD_for_ratio[len(JAD_for_ratio) / 2:, len(JAD_for_ratio) / 2:])) / (np.sum(JAD_for_ratio[:len(JAD_for_ratio) / 2, len(
                JAD_for_ratio) / 2:]) + np.sum(JAD_for_ratio[len(JAD_for_ratio) / 2:, :len(JAD_for_ratio) / 2]))
        print('The JAD ratio with same m is %.4f' % ratio)
        JAD_for_ratio2 = JAD_spectrum[:len(self.theta_grid) / 2, len(
            self.theta_grid) / 2:]
        ratio2 = (np.sum(JAD_for_ratio2[:len(JAD_for_ratio2) / 2, :len(
            JAD_for_ratio2) / 2]) + np.sum(JAD_for_ratio2[len(JAD_for_ratio2) / 2:, len(JAD_for_ratio2) / 2:])) / (np.sum(JAD_for_ratio2[:len(JAD_for_ratio2) / 2, len(
                JAD_for_ratio2) / 2:]) + np.sum(JAD_for_ratio2[len(JAD_for_ratio2) / 2:, :len(JAD_for_ratio2) / 2]))
        print('The JAD ratio with different is %.4f' % (1 / ratio2))
        JAD_for_ratio_total = JAD_spectrum[:len(self.theta_grid) / 2, :]
        half_theta_num = len(self.theta_grid) / 4
        same_eta_yield = np.sum(JAD_for_ratio_total[:half_theta_num, :half_theta_num]) + np.sum(JAD_for_ratio_total[half_theta_num:, half_theta_num:-half_theta_num]) \
            + np.sum(JAD_for_ratio_total[:half_theta_num, -half_theta_num:])
        diff_eta_yield = np.sum(JAD_for_ratio_total[:half_theta_num, half_theta_num:-half_theta_num]) + np.sum(JAD_for_ratio_total[half_theta_num:, :half_theta_num]) \
            + np.sum(JAD_for_ratio_total[half_theta_num:, -half_theta_num:])
        print('total ratio is %.4f' % (same_eta_yield / diff_eta_yield))
        if log_scale:
            fig = plt.contourf(self.theta_grid * 180 / np.pi, self.theta_grid * 180 / np.pi,
                               np.log10(JAD_spectrum), 40)
        else:
            print('This is the time when we do not use logscale')
            fig = plt.contourf(self.theta_grid * 180 / np.pi,
                               self.theta_grid * 180 / np.pi, JAD_spectrum, 40)
            print("The maxmum value of the comparision is: " +
                  str(np.max(JAD_spectrum)))
        plt.rc('text', usetex=True)
        plt.xlabel(r"$\theta_1$ (deg)")
        plt.ylabel(r"$\theta_2$ (deg)")
        if not stripe:
            print(E1 * float(self.energy_factor))
            title = r"$(E_1,E_2)=($" + "%.2f" % (E1 * self.energy_factor) + r"$,$" + \
                    "%.2f" % (E2 * self.energy_factor) + r"$)$" + use_compare
            if self.energy_factor != 1:
                title += '$\Delta E=$%.2fev' % (abs(E1 - E2)
                                                * self.energy_factor)
            plt.title(title)
        else:
            title = r"$E_1+E_2$from" + "%.2f" % (E1 * self.energy_factor) + r" to " + "%.2f" % (
                E2 * self.energy_factor) + use_compare
            if self.energy_range_ev:
                title += '$\Delta E=$' + self.energy_range_ev + 'ev'
            plt.title(title)
        plt.axis('equal')
        plt.xlim(0, 360)
        plt.ylim(0, 360)
        plt.colorbar(fig)
        self.figure_index += 1

    def plot_JAD_sum_angle(self, JAD_spectrum, E1, E2, use_compare, label):
        plt.figure(self.figure_index)
        sum_value = {}
        for i in range(self.number_of_angles):
            for j in range(i, self.number_of_angles):
                summation_angle = str(self.theta_grid[i] + self.theta_grid[j])
                if summation_angle not in sum_value:
                    sum_value[summation_angle] = 0
                sum_value[summation_angle] += JAD_spectrum[i][j] * 2
        sum_theta = sorted([float(key) for key in sum_value.keys()])
        sum_spectrum = [float(sum_value[str(key)]) for key in sum_theta]
        plt.xlabel(r"summation of $\theta$")
        plt.ylabel(r"yeild")
        title = r"$(E_1,E_2)=($" + "%.2f" % (E1 * self.energy_factor) + r"$,$" + "%.2f" % (
            E2 * self.energy_factor) + r"$)$" + use_compare
        if self.energy_range_ev:
            title += '$\Delta E=$' + self.energy_range_ev + 'ev'
        plt.title(title)
        plt.plot(np.array(sum_theta) / np.pi * 180, sum_spectrum, label=label)
        plt.legend()
        self.figure_index += 1

    def plot_JAD_fix_angle(self, JAD_spectrum, E1, E2, calculate_error=False):
        plt.figure(self.figure_index)
        if not self.write_fix_one_angle:
            angles = [0, 90, 180]
        else:
            angles = [0, 45, 90, 135, 180]
        errors = []
        self.figure_index += 1
        theta_range = 5
        for angle in angles:
            # angle1 = float(fix_one_angle)
            # do the average over 10 deg
            index1 = np.where(np.array(self.theta_grid) < np.mod(
                angle - theta_range, 360) / 180. * np.pi)[0][-1]
            index2 = np.where(np.array(self.theta_grid) < np.mod(
                angle + theta_range, 360) / 180. * np.pi)[0][-1]

            if int(self.fix_one_angle) == 1:
                if index1 < index2:
                    other_JAD = sum(
                        JAD_spectrum[index1: index2 + 1, :] / (index2 - index1))
                else:
                    other_JAD = (sum(JAD_spectrum[index1:, :]) + sum(
                        JAD_spectrum[:index2 + 1, :])) / (len(JAD_spectrum) - index1 + index2)
            else:
                if index1 < index2:
                    other_JAD = sum(
                        JAD_spectrum[index1: index2 + 1, :, ].transpose() / (index2 - index1))
                else:
                    other_JAD = (sum(JAD_spectrum[index1:, :].transpose(
                    )) + sum(JAD_spectrum[:index2 + 1, :].transpose())) / (len(JAD_spectrum) - index1 + index2)
                # print JAD_spectrum[0]
            print('%i index with size %i and the angle is %.4f' %
                  (index1, len(self.theta_grid), angle))
            other_JAD = JAD_spectrum[:, index1]
            axis = plt.subplot(int('1' + str(len(angles)) + '1') +
                               angles.index(angle), projection='polar')
            axis.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))
            theta_e1 = np.ones(len(self.theta_grid)) * angle * np.pi / 180
            if other_JAD.max() != other_JAD.min():
                JAD_e1 = np.array(range(len(self.theta_grid))) * (other_JAD.max() - other_JAD.min()) / len(
                    other_JAD) + np.ones(len(self.theta_grid)) * other_JAD.min()
            else:
                JAD_e1 = np.array(range(len(self.theta_grid))) * \
                    other_JAD.max() / len(other_JAD)
            axis.plot(theta_e1, JAD_e1, label='e1')
            axis.plot(self.theta_grid, other_JAD, 'o', label='e2')
            if self.write_fix_one_angle:
                angle_file = open(self.folder + '/%.2f-%.2fangleDistribution_%i.txt' % (self.E1_select, self.E2_select,
                                                                                        angle), 'w+')
                angle_file.write('# angle, yield \n')
                for m in range(len(self.theta_grid)):
                    angle_file.write('%5f,%4e \n' %
                                     (self.theta_grid[m], other_JAD[m]))
                angle_file.close()
            if other_JAD.max() != other_JAD.min():
                axis.set_rmin(other_JAD.min())
            axis.set_rticks([0, other_JAD.max()])
            axis.set_theta_direction(-1)
            axis.set_theta_offset(np.pi / 2.0)
            first_half = other_JAD[1:len(other_JAD) / 2 + 1]
            second_half = other_JAD[len(other_JAD) / 2 + 1:]
            second_half = second_half[::-1]
            errors.append(
                [self.theta_grid[1:len(other_JAD) / 2 + 1], first_half, self.theta_grid[len(other_JAD) / 2 + 1:]
                 - np.pi, second_half])
            axis.set_rlabel_position(22.5)
            axis.grid(False)
        plt.subplots_adjust(wspace=.5)
        title = 'JAD at $E_1=%.2feV,E_2=%.2feV,|E_1-E_2|=%.2feV$' % (
            Ry2 * E1 / e, Ry2 * E2 / e, Ry2 * abs(E1 - E2) / e)
        if not self.use_ev:
            title = 'JAD at $E_1=%.2fa.u,E_2=%.2fa.u,|E_1-E_2|=%.2fa.u$' % (
                E1, E2, abs(E1 - E2))
        if self.energy_range_ev:
            title += '$\Delta E=$' + self.energy_range_ev + 'ev'
        if abs(E1) < 1e-2 or abs(E2) < 1e-2:
            title = 'total integration'
        plt.title(title, y=1.08, x=-1.08)
        plt.legend(loc=9)
        self.figure_index += 1
        if calculate_error:
            self.plot_JAD_fix_angle_error(E1, E2, angles, errors)

    def plot_JAD_fix_angle_error(self, E1, E2, angles, errors):
        plt.figure(self.figure_index)
        for i in range(len(errors)):
            item = errors[i]
            plt.plot(item[0] * 180.0 / np.pi, item[1],
                     label='$\\theta_1=' + str(angles[i]) + '^{\\circ}, \\theta=\\theta_2$')
            plt.plot(item[2] * 180.0 / np.pi, item[3],
                     label='$\\theta_1=' + str(angles[i]) + '^{\\circ}, \\theta=360^{\\circ}-\\theta_2$')
        plt.xlabel('$\\theta(^{\\circ})$')
        plt.ylabel('$JAD(\\theta_1=\\Theta, \\theta)$')
        plt.title('$E_1=%.2f eV,E_2=%.2f eV$' %
                  (E1 * self.energy_factor, E2 * self.energy_factor))
        plt.legend()
        self.figure_index += 1

    def plot_JAD(self, JAD_spectrum, E1, E2, use_compare='', stripe=False, label='', fix_one_angle=False):
        JAD_max = np.max(JAD_spectrum)
        print('JAD max is %.2e' % JAD_max)
        print('JAD min is %.2e' % np.min(JAD_spectrum))
        if not fix_one_angle:
            if not self.sum_angle:
                self.plot_JAD_normal(JAD_spectrum, E1, E2,
                                     self.sum_angle, stripe, use_compare)
                if os.path.isfile(self.folder + "/JAD_from_old.txt"):
                    # created from C++ code with the same method as this script
                    JAD_from_old = np.loadtxt(self.folder + "JAD_from_old.txt")
                    # max_JAD = np.max(JAD_from_old)
                    # JAD_from_old /= max_JAD
                    # JAD_from_old[JAD_from_old < 0.01] = 0.01
                    self.plot_JAD_normal(
                        JAD_from_old, E1, E2, self.sum_angle, stripe, use_compare)
                # if os.path.isfile(self.folder + "/JADFromOld"):
                #     contents = open(self.folder + '/JADFromOld').readlines()
                #     thetas = [0.]
                #     JAD_yields = [[]]
                #     for content in contents:
                #         if '#' in content:
                #             continue
                #         if content == ' \n':
                #             JAD_yields.append([])
                #             thetas.append(0.)
                #             continue
                #         thetas[-1] = np.arccos(float(content.split(',')[0])) * 180 / np.pi
                #         JAD_yields[-1].append(float(content.split(',')[-1]))
                #     JAD_yields = np.array(JAD_yields)
                #     JAD_yields /= np.max(JAD_yields)
                #     JAD_yields[JAD_yields < 0.01] = 0.01
                #     plt.figure(self.figure_index)
                #     fig = plt.contourf(thetas, thetas,
                #                np.log10(JAD_yields), 40)
                #     plt.contourf(fig)
                #     plt.title("From theta Grid")
                #     plt.axis("equal")
                #     plt.legend()
                #     self.figure_index += 1
                if os.path.isfile(self.folder + '/totalJADFromOld.txt'):
                    totalJAD = np.loadtxt(self.folder + '/totalJADFromOld.txt')
                    thetas = np.loadtxt(self.folder + '/thetaFromOld.txt')
                    totalJAD /= np.max(totalJAD)
                    totalJAD[totalJAD < 0.01] = 0.01
                    plt.figure(self.figure_index)
                    fig = plt.contourf(thetas, thetas, np.log10(totalJAD), 50)
                    plt.xlabel('$\\theta_1$')
                    plt.ylabel('$\\theta_2$')
                    plt.axis('equal')
                    # print kz_matrix
                    plt.colorbar(fig)
                    self.figure_index += 1
            else:
                self.plot_JAD_sum_angle(
                    JAD_spectrum, E1, E2, use_compare, label)
        else:
            self.plot_JAD_fix_angle(
                JAD_spectrum, E1, E2, calculate_error=False)

    def plot_SI_spectrum(self, write_spectrum, add_experiment_data=False):
        SIdata = np.zeros(
            [self.number_of_momentas, self.lmax + 1], dtype=np.double)
        basis_name = self.SI_spec_files[0].split('testspec_')[0] + 'testspec_'
        for i in range(self.number_of_momentas):
            try:
                temp = np.loadtxt(basis_name + str(i) + '.txt')
                SIdata[i, :] = temp[1:]
            except:
                SIdata[i, :] = SIdata[i - 1, :]
        # lmax+1 equals all the angular momentums
        totalSI = np.sum(SIdata, 1)
        totalSI = np.array(totalSI) * np.array(self.abs_momenta)
        Energies = 0.5 * self.abs_momenta ** 2
        plt.figure(self.figure_index)
        if write_spectrum:
            out_file = open(self.folder + './totalSI.txt', 'w+')
            out_file.write('#Store data for total SI\n')
            out_file.write('#$E$ (au); SI yield\n')
            for i in range(len(totalSI)):
                out_file.write(str(Energies[i]) + ',' + str(totalSI[i]) + '\n')
            out_file.close()
        if self.write_spec_total:
            spec_out_file = open(self.folder + '/spec_total_SI', 'w+')
            spec_out_file.write('#Store data for total SI\n')
            spec_out_file.write('#$k$ (au); SI yield\n')
            for i in range(len(totalSI)):
                spec_out_file.write(
                    str(self.abs_momenta[i]) + ',' + str(totalSI[i]) + '\n')
            spec_out_file.close()
        if self.compare_SI:
            spec_compare_contents = [item.split(',') for item in
                                     open('/'.join(self.folder[:-1].split('/')[:-1])
                                          + '/' + self.compare_SI + '/totalSI.txt').readlines() if '#' not in item]
            energies = [float(k[0]) for k in spec_compare_contents]
            yields = [float(k[1]) for k in spec_compare_contents]
            plt.semilogy(energies * self.energy_factor, yields,
                         label='compare from ' + self.compare_SI)
        if add_experiment_data:
            experiment_data = open(
                self.folder + '/single_spectrum.txt').readlines()
            energies = [float(k.split()[0]) for k in experiment_data]
            yields = [float(k.split()[1]) for k in experiment_data]
            maps = dict(zip([str(energy) for energy in energies], [
                        str(yield_single) for yield_single in yields]))
            sorted_energy = sorted([float(energy) for energy in energies])
            sorted_yields = [float(maps[str(energy)])
                             for energy in sorted_energy]
            max_experiment_yield = max(sorted_yields)
            totalSI = totalSI * max_experiment_yield / np.max(totalSI)
            plt.plot(sorted_energy, sorted_yields, label="experiment data")
        plt.semilogy(Energies * self.energy_factor, totalSI, label='SI')
        plt.title('single spectrum')
        plt.rc('text', usetex=True)
        if self.energy_factor != 1:
            plt.xlabel(r"$E$ (eV)")
        else:
            plt.xlabel(r'$E$(a.u)')
        plt.ylabel(r"SI yield (Signal)")
        plt.legend()
        self.figure_index += 1

    def cut_off_energy(self, box_size, propagate_cycle):
        from static_methods import get_k_cutoff
        k_cut_off = get_k_cutoff(
            box_size, propagate_cycle, self.wavelength) * 2
        index = np.where(self.abs_momenta > k_cut_off * 2)[0][0]
        self.abs_momenta = self.abs_momenta[index:]
        self.total_spectrum = self.total_spectrum[index:, index:]

    def plot_total_normal(self, energies, number_of_colours):
        plt.figure(self.figure_index)
        plt.rc('text', usetex=True)
        plt.xlabel(r"$E_1$ (au)")
        plt.ylabel(r"$E_2$ (au)")
        plt.axis('equal')
        if self.use_ev:
            plt.xlabel(r"$E_1$ (ev)")
            plt.ylabel(r"$E_2$ (ev)")
        momenta_element = self.abs_momenta ** 2 * self.d_abs_momenta
        total_yield_DI = np.sum(np.array(
            self.total_spectrum) * np.tensordot(momenta_element, momenta_element, 0))
        print('total yield of DI %.4e' % total_yield_DI)
        self.total_spectrum = np.array(self.total_spectrum) * np.array(np.mat(self.abs_momenta).transpose() *
                                                                       np.mat(self.abs_momenta))
        fig = plt.contourf(energies * self.energy_factor, energies * self.energy_factor,
                           np.log10(self.total_spectrum), number_of_colours)
        axis_up_limit = np.array(
            energies[len(energies) - 1]) * self.energy_factor
        if up_limit != 0:
            axis_up_limit = up_limit
        print('axis_up_limit %.4f' % axis_up_limit)
        plt.xlim(0, axis_up_limit)
        plt.ylim(0, axis_up_limit)
        plt.colorbar(fig)
        self.figure_index += 1

    def plot_total_compare(self, compare_spectrum, energies, number_of_colours):
        plt.figure(self.figure_index)
        plt.rc('text', usetex=True)
        plt.xlabel(r"$E_1$ (au)")
        plt.ylabel(r"$E_2$ (au)")
        plt.axis('equal')
        if self.use_ev:
            plt.xlabel(r"$E_1$ (ev)")
            plt.ylabel(r"$E_2$ (ev)")
        compare_spectrum = abs(np.array(compare_spectrum)) * np.array(np.mat(self.abs_momenta).transpose() *
                                                                      np.mat(self.abs_momenta))
        fig = plt.contourf(energies * self.energy_factor, energies * self.energy_factor,
                           np.log10(compare_spectrum), number_of_colours)
        axis_up_limit = np.array(
            energies[len(energies) - 1]) * self.energy_factor
        if up_limit != 0:
            axis_up_limit = up_limit
        print('axis_up_limit %.4f' % axis_up_limit)
        plt.xlim(0, axis_up_limit)
        plt.ylim(0, axis_up_limit)
        plt.title('for compare total Spectrum')
        plt.colorbar(fig)
        self.figure_index += 1

    def plot_total_sum_energy_yield(self, energies):
        plt.figure(self.figure_index)
        if 'spec_total' in os.listdir(self.folder):
            print('rm the spec_total file \n')
            os.system('rm ' + self.folder + '/spec_total')
        total_yield = []
        for i in range(len(self.total_spectrum)):
            data = 0
            ks = []
            yiels = []
            for j in range(i):
                yiels.append(self.total_spectrum[i - j][j])
                ks.append([self.abs_momenta[i - j], self.abs_momenta[j]])
                data += self.total_spectrum[i - j][j]
            if len(ks) > 0:
                import copy
                m = copy.deepcopy(ks)
                ksback = m[: -1]
                ksback.insert(0, ks[0])
                dks = [np.sqrt((ks[m][0] - ksback[m][0]) ** 2 + (ks[m][1] - ksback[m][1]) ** 2) for m in
                       range(len(ks))]
                total_yield.append(np.array(yiels).dot(np.array(dks)))
            else:
                total_yield.append(1e-10)
        if self.log_scale:
            plt.semilogy(energies * self.energy_factor, total_yield,
                         label="$\\sigma_{s}(E_{1} + E_{2})=\\int d(E_1-E2) \\sigma(E_1,E_2)$")
        else:
            plt.plot(energies * self.energy_factor, total_yield)
        return total_yield

    def plot_total_sum_energy_interval(self, energies, total_yield):
        plt.figure(self.figure_index)
        Up = LaserProperty(self.intensity, self.wavelength)['Up']
        omeg = LaserProperty(self.intensity, self.wavelength)['Omeg']
        lower_boundary_photon = int(
            min(list((np.array(energies) + 2 * Up + 2.903) / omeg)))
        up_boundary_photon = int(
            max(list((np.array(energies) + 2 * Up + 2.903) / omeg)) + 1)
        Sn, total_x, total_y = [], [], []
        print(max(total_yield))
        print(min(total_yield))
        for i in range(lower_boundary_photon, up_boundary_photon):
            x = np.ones(1000) * (i * omeg - 2 * Up - 2.903) * \
                self.energy_factor
            Sn.append(x[0])
            y = [i * (max(total_yield) - min(total_yield)) /
                 1000.0 + min(total_yield) for i in range(1000)]
            total_x += list(x)
            total_y += list(y)
        if self.log_scale:
            plt.semilogy(list(total_x), total_y, '.',
                         label="$E_1+E_2=m\\omega - 2U_{p} - 2.903(a.u)$")
        else:
            plt.plot(list(total_x), total_y, '.',
                     label='$E_1+E_2m=\\omega - 2U_{p} - 2.903(a.u)$,$m\in[' + str(lower_boundary_photon) + ',' + str(
                         up_boundary_photon) + ']$,m integal')
        plt.xlabel("$E_1 + E_2$")
        if self.energy_factor != 1:
            plt.xlabel("$E_1 + E_2$(eV)")
        plt.ylabel(
            "$\\sigma_{s}(E_{1} + E_{2})=\\int d(E_1-E2) \\sigma(E_1,E_2)$")
        title_content = '$I=' + str(self.intensity) + 'W/cm^{2}$,$\lambda=' + str(self.wavelength) \
                        + 'nm$,FWHM=' + self.inputs['n_cycles']
        if int(self.inputs['pulseshape']) == 15:
            title_content += ', flat-Top[1 optCycle]'
        elif int(self.inputs['pulseshape']) == 21:
            title_content += ', cos8'
        plt.legend()
        plt.title(title_content, fontsize=20)
        return Sn

    def plot_total_sum_energy_total(self, energies):
        plt.figure(self.figure_index)
        if self.include_k:
            multi_factor = np.array(
                np.mat(self.abs_momenta).transpose() * np.mat(self.abs_momenta))
            self.total_spectrum = np.array(self.total_spectrum) * multi_factor
        total_yield = self.plot_total_sum_energy_yield(energies)
        Sn = self.plot_total_sum_energy_interval(energies, total_yield)
        self.figure_index += 1
        return Sn

    def plot_total_spectrum_Sn(self, energies, Sn):
        plt.figure(self.figure_index)
        sum_values = []
        momenta_back = list(self.abs_momenta)[: -1]
        momenta_back.insert(0, 0)
        dk = np.array(self.abs_momenta) - np.array(momenta_back)
        for item in self.total_spectrum:
            sum_values.append(np.array(item).dot(dk))
        if self.log_scale:
            plt.semilogy(energies, sum_values)
        else:
            plt.plot(energies, sum_values)
        for i in Sn:
            x = np.ones(100) * i / 2.0
            y = [i * (max(sum_values) - min(sum_values)) / 100.0 + min(sum_values) for i in
                 range(100)]
            if self.log_scale:
                plt.semilogy(list(x), y, 'g')
            else:
                plt.plot(list(x), y, 'g')
        plt.xlabel("$S_n / 2$")
        plt.ylabel("$\\sigma_1(E_{1})=\\int dE_2\\sigma(E_1,E2)$")
        title_content = '$I=%.2e*10^{14}W/cm^{2}$,$\lambda=%.2fnm$,FWHM=4, flat-Top[1 optCycle]$' %\
                        (self.intensity, self.wavelength)
        plt.title(title_content, fontsize=20)
        self.figure_index += 1

    def plot_total_spectrum(self):
        if self.cut_off:
            if not (self.boxSize and self.propapate_cycle):
                sys.exit(
                    'when cut off energy, you should specify --boxSize=... and propapate_cycle=...')
            self.cut_off_energy(self.boxSize, self.propapate_cycle)
        energies = 0.5 * self.abs_momenta ** 2
        number_of_colors_in = 50
        self.plot_total_normal(energies, number_of_colors_in)
        if os.path.isfile(self.folder + '/spec_from_old'):
            plt.figure(self.figure_index)
            compare_spectrum = np.loadtxt(self.folder + '/spec_from_old')
            compare_spectrum = np.array(compare_spectrum) * np.array(np.mat(self.abs_momenta).transpose() *
                                                                     np.mat(self.abs_momenta))
            fig = plt.contourf(energies * self.energy_factor, energies * self.energy_factor,
                               np.log10(abs(compare_spectrum - self.total_spectrum) / compare_spectrum), number_of_colors_in)
            axis_up_limit = np.array(
                energies[len(energies) - 1]) * self.energy_factor
            if up_limit != 0:
                axis_up_limit = up_limit
            print('axis_up_limit %.4f' % axis_up_limit)
            plt.xlim(0, axis_up_limit)
            plt.ylim(0, axis_up_limit)
            plt.colorbar(fig)
            self.figure_index += 1
        if os.path.isfile(self.folder + '/specFromOld'):
            contents = open(self.folder + '/specFromOld').readlines()
            momentas = [0.]
            yields = [[]]
            for content in contents:
                if '#' in content:
                    continue
                if content == ' \n':
                    yields.append([])
                    momentas.append(0.)
                    continue
                momentas[-1] = float(content.split(',')[0])
                yields[-1].append(float(content.split(',')[-1]))
            yields = np.array(yields) * np.array(np.mat(self.abs_momenta).transpose() *
                                                 np.mat(self.abs_momenta))
            self.plot_total_compare(abs(
                yields - self.total_spectrum) / self.total_spectrum, momentas, number_of_colors_in)
        Sn = self.plot_total_sum_energy_total(energies)
        self.plot_total_spectrum_Sn(energies, Sn)
        if self.write_spec_total:
            print('write my own spec_total file')
            k = open(self.folder + '/spec_total_DI_diagonal', 'a')
            k.write('# tMax,time-resolution\n')
            k.write('# \n')
            k.write('#\n')
            k.write('#  sum[Phi,Eta]: (specRn) = (0)\n')
            for i in range(len(self.abs_momenta)):
                a = '%.10f,%.10f\n' % (
                    self.abs_momenta[i] * np.sqrt(2), self.total_spectrum[i][i])
                k.write(a)
            k.close()

    def compare_JAD(self):
        compares = self.compare.split('+')
        for compare in compares:
            print('We are going to compare' + self.folder + '/' + compare)
            compare_folder = '/'.join(self.folder.split('/')[:-2]) + '/' + \
                             self.compare_folder + '/' + self.compare + '/'
            JAD_spectrumCompare = np.loadtxt(
                get_JAD_spectrum_filename(compare_folder, 0))
            JAD_max = np.max(JAD_spectrumCompare)
            angleCap = JAD_max * 0.01
            JAD_spectrumCompare[JAD_spectrumCompare < angleCap] = angleCap
            angle_normalize = 1.0 / JAD_max
            JAD_spectrumCompare *= angle_normalize
            if len(compares) == 1:
                JAD_spectrumCompare = abs(
                    JAD_spectrumCompare - self.JAD_spectrum)
            # print 'compare folder ' + compare_folder
            # print np.loadtxt(get_JAD_energies_filename(compare_folder, 0, 1))
            E1_select = np.loadtxt(
                get_JAD_energies_filename(compare_folder, 0, 1))
            E2_select = np.loadtxt(
                get_JAD_energies_filename(compare_folder, 0, 2))
            # print(E1_select)
            # if abs(E1_select - self.E1_select) > 0.01 or abs(E2_select - self.E2_select) > 0.01:
            #     sys.exit('The compare JAD is not in the same energy as the original folder')
            contents = get_input_contents(
                '/'.join(self.folder.split('/')[:-2]) + '/' + self.compare_folder + '/')
            label = '$R_c$=' + str(contents['scaling rad']) + ',$I$=' + str(
                contents['intensity']) + ',$\lambda$=' + str(contents['wavelength'])
            self.plot_JAD_normal(JAD_spectrumCompare, E1_select,
                                 E2_select, False, False, use_compare='comparision')

    def plot_kz_projection_spectrum(self, kz_projection_spectrum):
        int_elem = np.tensordot(self.d_kz, self.d_kz, 0)
        print('total yield kz %.4e' % np.sum(kz_projection_spectrum))
        plt.figure(self.figure_index)
        plt.xlabel('$pz_1(a.u)$')
        plt.ylabel('$pz_2(a.u)$')
        fig = plt.contourf(self.kz_momentum, self.kz_momentum,
                           np.log10(abs(kz_projection_spectrum)), 40)
        plt.colorbar(fig)
        plt.title('The pz spectrum')
        plt.legend()
        plt.axis('equal')
        self.figure_index += 1

    def compute_spectrum(self, write_spectrum=True, plot_SI_spectrum=True):
        print('mmax:', - self.appearing_m[0], ' lmax:', self.lmax)
        self.get_DI_JAD()
        self.plot_JAD(self.JAD_spectrum, self.E1_select, self.E2_select)
        print('start entering %.6e' % self.total_spectrum[0][0])
        self.plot_total_spectrum()
        if hasattr(self, 'kz_projection_spectrum'):
            self.plot_kz_projection_spectrum(self.kz_projection_spectrum)
        if self.fix_one_angle:
            self.plot_JAD(self.JAD_spectrum, self.E1_select,
                          self.E2_select, fix_one_angle=True)
        print('see the compare folder ' + self.compare_folder)
        print('compare ' + self.compare)
        if self.compare_folder and self.compare:
            self.compare_JAD()
        if self.plot_stripe_JAD:
            self.plot_JAD(self.theta_grid, self.stripe_JAD_spectrum,
                          self.low_energy, self.high_energy, stripe=True)
        if plot_SI_spectrum:
            self.plot_SI_spectrum(write_spectrum)
        integration_object = IntegrationProbability(self.folder, False, False)
        ratio = integration_object.calculate_ratio()
        print("The ratio is %4f, SI norm is %4f, DI norm is %4f" %
              (ratio, integration_object.SI_norm, integration_object.DI_norm))
        plt.show()

    def get_theta_scattering_amplitude(self, scattering_amplitude_fixed_m_fixed_k, mindex):
        return get_theta_scattering_amplitude(self.JADangles, scattering_amplitude_fixed_m_fixed_k,
                                              self.spherical_harmonics_1plus, self.spherical_harmonics_2plus,
                                              self.spherical_harmonics_1minus, self.spherical_harmonics_2minus, mindex)

    def get_kz_scattering_amplitude(self, scattering_amplitude_fixed_m_fixed_k, mindex, k1Index, k2Index):
        # kz_theta_amplitute = get_kz_theta_scattering_amplitude()
        # calculate k1,k2
        kz_theta_cross_back = get_kz_theta_scattering_cross(self.JADangles, scattering_amplitude_fixed_m_fixed_k, self.spherical_harmonics_1plus_kz,
                                                            self.spherical_harmonics_2plus_kz, mindex, k1Index, k2Index)
        kz_theta_cross_same = get_kz_theta_scattering_cross(self.JADangles, scattering_amplitude_fixed_m_fixed_k, self.spherical_harmonics_1plus_kz,
                                                            self.spherical_harmonics_1plus_kz, mindex, k1Index, k2Index)
        self.kz_projection_spectrum += np.array(kz_theta_cross_same) * self.abs_momenta[k1Index] * \
            self.abs_momenta[k2Index] * \
            self.d_abs_momenta[k1Index] * self.d_abs_momenta[k2Index]
        self.kz_projection_spectrum += np.array(kz_theta_cross_back) * self.abs_momenta[k1Index] * \
            self.abs_momenta[k2Index] * \
            self.d_abs_momenta[k1Index] * self.d_abs_momenta[k2Index]

    def read_in_DI_data(self, k1_index, k2_index, lmax_local, m_index):
        return read_in_DI_data(lmax_local, self.DI_spec_files[m_index], self.DI_spec_files[
            len(self.appearing_m) - 1 - m_index], len(self.abs_momenta), k1_index, k2_index)


if __name__ == '__main__':
    from static_methods import convert_inputs
    inputs, arguments = convert_inputs(sys.argv[1:])
    folder_in = inputs[0]
    if folder_in == 'help':
        sys.exit('python ~/Documents/research_data/ownScripts/computeSpectrum_jinzhen.py DI/134744 '
                 '--wavelength=394.5 --intensity=1e13')
    if folder_in[-1] != '/':
        folder_in += '/'
    if 'up_limit' in arguments:
        up_limit = float(arguments['up_limit'])
        del arguments['up_limit']
    DI_Tool = DITool(folder_in, **arguments)
    DI_Tool.compute_spectrum()
