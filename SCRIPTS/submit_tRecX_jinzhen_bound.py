#! /usr/bin/env python3

# for submitting jobs of tRecX

import os
import sys
import shutil
from shutil import copytree, ignore_patterns, copyfile
import socket

user_name = 'jinzhen'
user_email = 'jinzhen.zhu@physik.uni-muenchen.de'
USER_NAME = os.path.abspath('.').split('/')[4]


def get_scratch_folder(node_number, is_running=False):
    if socket.gethostname() != 'capp1':
        return '$SCRATCH/'
    elif is_running:
        return '/scratch/all/' + user_name + '/'
    else:
        return '/scratches/capp' + str(node_number) + '/all/' + user_name + '/'


BASE_PROJECT_FOLDER = '/naslx/projects/pr47cu/' + \
    USER_NAME + '/runs/TDSEsolver_runs_1/'
BASE_SCRATCH_FOLDER = get_scratch_folder('1', True) + '/$SLURM_JOBID/'
BASE_RUN_SCRATCH_FOLDER = get_scratch_folder('1', True)


def numeric(data):
    for item in data:
        if item not in '0123456789.':
            return False
    return True


def argsAndFlags():
    args = []
    flags = {}
    for item in sys.argv[1:]:

        if item[0] != "-":
            args.append(item)
        else:
            flags[item.split('=')[0][1:]] = item.split('=')[1]
    return args, flags


def change_k_points(file_name, process_number, k_per_process=4):
    file_contents = open(file_name).readlines()
    change_file = False
    for i in range(len(file_contents)):
        content = file_contents[i]
        if change_file:
            content = ','.join(content.split(
                ',')[:-1]) + ', ' + str(k_per_process * process_number) + '\n'
            change_file = False
        if 'radialPoints' in content:
            change_file = True
        file_contents[i] = content
    k = open(file_name, 'w+')
    for content in file_contents:
        k.write(content)
    k.close()
    print('changing file done')


def change_inpc_bound(folder, use_Rn2=False):
    os.system('sed -i -e "s/\#Spectrum/Spectrum/g" -e "s/\#true/true/g" -e "s/\#false/false/g" -e "s/\#Source/Source/g" -e "s/\#1/1/g" %s/inpc ' % folder)
    if use_Rn2:
        os.system('sed -i -e "s/\,Rn1/\,Rn2/g" %s/inpc ' % folder)
    else:
        os.system('sed -i -e "s/\,Rn2/\,Rn1/g" %s/inpc ' % folder)


def change_inpc_spec(folder, plot_what=''):
    os.system('sed -i -e "s/\#Plot/Plot/g" %s/inpc ' % folder)
    os.system('sed -i -e "s/\#intK/intK/g" %s/inpc ' % folder)
    if plot_what:
        os.system('sed -i -e "s/intK/' + plot_what + '/g" %s/inpc ' % folder)
        os.system('sed -i -e "s/partial/' +
                  plot_what + '/g" %s/inpc ' % folder)


def prepare_SD_inpc(infile, node=''):
    source_folder = BASE_PROJECT_FOLDER + infile
    capp_info = os.popen('more %s/outf | grep -a capp' %
                         source_folder).readlines()
    if len(capp_info) == 0 and not node:
        print('there is no capp info in ' + source_folder +
              '/outf, please specify the capp info for the run')
    capp_num = ''
    if node:
        capp_num = node
    if not capp_num:
        import re
        matches = re.finditer(r"\d{2}", capp_info[0], re.MULTILINE)
        for matchNum, match in enumerate(matches):
            capp_num = match.group()
    tem_scratch_folder = get_scratch_folder(capp_num, False) + infile + '/'
    if os.path.isdir(os.path.expandvars('%s/%s/S_Rn1' % (get_scratch_folder(capp_num, False), infile))):
        change_inpc_bound(tem_scratch_folder, use_Rn2=True)
    else:
        change_inpc_bound(tem_scratch_folder, use_Rn2=False)
    return capp_num


# the name variable has a "SP" end
def submit_big_spectrum(script_spectrum_file, name, capp_num):
    os.system('sed -i -e "s/JOBIDTOREPLACE/%s/g" %s ' %
              (name, script_spectrum_file))
    os.system('sed -i -e "s/NODETOREPLACE/nodelist=capp%s/g" %s ' %
              (capp_num, script_spectrum_file))
    os.system('sbatch ' + script_spectrum_file)
    os.system('cp ' + script_spectrum_file + ' .')
    os.system('sed -i -e "s/nodelist=capp%s/NODETOREPLACE/g" %s ' %
              (capp_num, script_spectrum_file))
    os.system('sed -i -e "s/%s/JOBIDTOREPLACE/g" %s ' %
              (name, script_spectrum_file))


def run_6D_total(exedir, infile, runFlags):
    commands = 'mkdir -p ' + BASE_SCRATCH_FOLDER + '\n' + \
        'mkdir ' + BASE_PROJECT_FOLDER + '/$SLURM_JOBID\n' +\
        'scp %s/inpc %s/inpc\n' % (jobdir, BASE_SCRATCH_FOLDER) + \
        'mpirun %s/tRecX %s/inpc %s -noScreen\n\n' % (
            exedir, BASE_SCRATCH_FOLDER, runFlags)
    # change with Bound1
    commands += 'sed -i -e "s/\#Spectrum/Spectrum/g" -e "s/\#true/true/g" -e "s/\#false/false/g" -e "s/\#Source/Source/g" -e "s/\#1/1/g" %s/inpc \n' % BASE_SCRATCH_FOLDER
    # run UNBOUND propagation
    commands += 'mpirun %s/tRecX %s/inpc  %s -noScreen\n\n' % (
        exedir, BASE_SCRATCH_FOLDER, runFlags)
    # change with Bound2
    commands += 'sed -i -e "s/\,Rn1/\,Rn2/g" %s/inpc \n' % BASE_SCRATCH_FOLDER
    commands += 'mpirun %s/tRecX %s/inpc  %s -noScreen\n\n' % (
        exedir, BASE_SCRATCH_FOLDER, runFlags)
    commands += 'mpirun %s/Spectrum6D %s  %s -noScreen\n' % (
                exedir, BASE_SCRATCH_FOLDER, runFlags)
    commands += ''
    return commands


def run_6D_by_step(exedir, infile, runFlags):
    commands = ''
    if numeric(infile):
        if os.path.isdir(os.path.expandvars('%s/%s/S_Rn1' % (get_scratch_folder(capp_num, False), infile))) and os.path.isdir(os.path.expandvars('%s/%s/S_Rn2' % (get_scratch_folder(capp_num, False), infile))):
            plot_what = 'plot' in inflags and inflags['plot'] or ''
            change_inpc_spec(
                '%s/%s/' % (get_scratch_folder(capp_num, False), infile), plot_what=plot_what)
            if nproc == 1:
                commands += '%s/Spectrum6D %s  %s -noScreen\n' % (
                    exedir, BASE_RUN_SCRATCH_FOLDER + infile, runFlags)
            else:
                commands += 'mpirun %s/Spectrum6D %s  %s -noScreen\n' % (
                    exedir, BASE_RUN_SCRATCH_FOLDER + infile, runFlags)
        else:
            commands += 'mpirun %s/tRecX %s/inpc  %s -noScreen\n' % (
                exedir, BASE_RUN_SCRATCH_FOLDER + infile, runFlags)
    else:
        commands += 'mkdir -p ' + BASE_SCRATCH_FOLDER + '\n'
        commands += 'mkdir ' + BASE_PROJECT_FOLDER + '/$SLURM_JOBID\n' +\
                    'scp %s/inpc %s/inpc\n' % (jobdir, BASE_SCRATCH_FOLDER) + \
                    'mpirun %s/tRecX %s/inpc %s -noScreen\n' % (
                        exedir, BASE_SCRATCH_FOLDER, runFlags)
    return commands


def move_file(folder_number, capp_number):
    os.system("shopt -s extglob \n")
    os.system("rm %s/%s/wfCheck* \n" %
              (get_scratch_folder(capp_number, False), folder_number))
    for item in os.listdir('%s/%s/' % (get_scratch_folder(capp_number, False), folder_number)):
        if 'surface' not in item:
            os.system("cp %s/%s/%s %s/%s/ \n" %
                      (get_scratch_folder(capp_number, False), folder_number, item, BASE_PROJECT_FOLDER, folder_number))


# separate args from possible flags
inargs, inflags = argsAndFlags()

if len(inargs) < 1:
    print("\n   usage: submit_tRecX input nproc queue   [defaults: nproc=1, queue=medium]")  # noqa: E501
    print("            -nodes=capp[28-30,32]  to select given nodes ")
    print("            -mem=all | 1234          to set memory (default = 8000/proc, all=128GB) ")  # noqa: E501
    exit()

# input file
infile = inargs[0]


# number of tasks
nproc = 1
if len(inargs) > 1:
    nproc = int(inargs[1])

# queue
queue = "capp_medium"
if len(inargs) > 2:
    queue = "capp_"+inargs[2]

if 'mpp3' in socket.gethostname():
    queue = 'teramem_inter'
elif 'mpp2' in socket.gethostname():
    queue = 'mpp2_inter'

useNodes = ""
if 'nodes' in inflags:
    useNodes = inflags['nodes']
else:
    if numeric(infile):
        from lrz_tools import get_result_capp_by_folder
        nodes = get_result_capp_by_folder(infile)
        print(nodes)
        if len(nodes) == 1:
            useNodes = str(nodes[0])
        else:
            sys.exit('there are two folders that has ampl file')
print('The useNodes ' + str(useNodes))

useMem = ""
if 'mem' in inflags:
    useMem = inflags['mem']

default_k_per_process = -1
if 'kpoints' in inflags:
    default_k_per_process = int(inflags['kpoints'])

runFlags = ""
for key in inflags:
    if key not in ['mem', 'nodes', 'kpoints', 'total']:
        runFlags += ' -' + key + '=' + inflags[key]

print(inflags)
# exedir
exedir = os.getcwd()
name = ''
if infile.rfind('/inpc') != len(infile)-5 and not numeric(infile):
    # strip '.inp' from the end of the file name
    if infile.rfind('.inp') == len(infile) - 4:
        infile = infile[:infile.rfind('.inp')]
    # create directory if needed
    if not os.path.exists(infile):
        os.mkdir(infile)
        os.system('touch '+infile+'/.MARKER_FILE')
    # make a job directory
    for i in range(10000):
        jobdir = exedir+'/'+infile+'/'+str(i).zfill(4)
        if not os.path.exists(jobdir):
            break
    os.mkdir(jobdir)
    shutil.copy(infile+'.inp', jobdir+'/inpc')
elif numeric(infile):
    name = infile + 'UB'
    jobdir = BASE_PROJECT_FOLDER + infile + '/'
else:
    jobdir = infile[0:len(infile)-5]

# "resume", i.e. run from job directory
print('The diretory', jobdir)

# for move file


# name for the queueing system
if name == '':
    name = jobdir.split('/')[-2]
if not numeric(infile):
    numb = jobdir.split('/')[-1]
    name = name[0:min(5, len(name))]+numb[-min(3, len(numb)):]
else:
    name = infile + 'UB'

if useMem == "":
    if queue == "capp_medium":
        mem = 8000*nproc
    else:
        mem = 6000*nproc

elif useMem == "all":
    mem = 8000 * 16
else:
    mem = useMem

print('use memory %i' % mem)

capp_num = ''
if numeric(infile):
    capp_num = prepare_SD_inpc(infile, node=useNodes)
if capp_num:
    if os.path.isdir(os.path.expandvars('%s/%s/S_Rn1' % (get_scratch_folder(capp_num, False), infile))) and os.path.isdir(os.path.expandvars('%s/%s/S_Rn2' % (get_scratch_folder(capp_num, False), infile))):
        name = infile + 'SP'
print('job name %s' % name)

commands = \
    "#!/bin/bash                                    \n" + \
    ("" if 'mpp2' in socket.gethostname() else "#SBATCH -p "+queue+"                           \n") + \
    "#SBATCH -o "+jobdir+"/out                      \n" + \
    "#SBATCH -e "+jobdir+"/err                      \n" + \
    "#SBATCH -D "+exedir+"                          \n" + \
    "#SBATCH -J "+name+"                            \n" + \
    "#SBATCH --get-user-env                         \n" + \
    "#SBATCH --mail-type=end                        \n" + \
    "#SBATCH --mail-user=%s      \n" % user_email + \
    ("" if 'mpp2' in socket.gethostname() else "#SBATCH --ntasks="+str(nproc)+"                \n") + \
    "#SBATCH --time=40:00:00                        \n" + \
    "#SBATCH --mem-per-cpu="+str(int(mem/nproc))+"  \n"
if capp_num and socket.gethostname() == "capp1":
    commands += "#SBATCH --nodelist=capp"+capp_num+"  \n"
elif socket.gethostname() != "capp1":
    if 'mpp2' in socket.gethostname():
        commands += '#SBATCH --clusters=serial\n'
    os.system('module unload boost')
    os.system('module load boost/1.61_icc')

if 'move' in inflags:
    move_file(infile, capp_num)
    sys.exit('move done')

# designate compute nodes
if useNodes != "" and not capp_num:
    commands += "#SBATCH --nodelist="+useNodes+"\n"
if os.getcwd().find('/home/hpc/') == 0:
    commands += "mpirun python "+exedir+"/SCRIPTS/diagnose_orted.py \n"

if 'total' in inflags:
    commands += run_6D_total(exedir, infile, runFlags)
else:
    commands += run_6D_by_step(exedir, infile, runFlags)


if default_k_per_process != -1:
    change_k_points(infile, nproc, default_k_per_process)
open(jobdir+'/script', 'w').write(commands)
os.system('sbatch '+jobdir+'/script')
if 'SP' in name and ('plot' not in inflags or inflags['plot'] == 'partial') or (socket.gethostname() != "capp1" and useNodes == "mpp"):
    script_spectrum_file = os.getcwd() + '/SCRIPTS/script_spectrum'
    submit_big_spectrum(script_spectrum_file, name[:-2], capp_num)
