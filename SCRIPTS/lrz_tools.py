import os
import sys

def get_capp_by_folder(folder_num):
    capps = []
    for i in range(18, 36):
        if os.path.isdir('/scratches/capp%i/all/jinzhen/%s' % (i, folder_num)):
            capps.append(i)
    return capps

def get_result_capp_by_folder(folder_num):
    capps = get_capp_by_folder(folder_num)
    result_capps = []
    for capp in capps:
        if os.path.isfile('/scratches/capp%i/all/jinzhen/%s/ampl' % (capp, folder_num)):
            result_capps.append(capp)
    return result_capps

def create_folder_project(folder_number, node_number):
    os.system('mkdir /naslx/projects/pr47cu/ru37quz/runs/TDSEsolver_runs_1/%s/S_Rn1' % folder_number)
    os.system('mkdir /naslx/projects/pr47cu/ru37quz/runs/TDSEsolver_runs_1/%s/S_Rn2' % folder_number)
    os.system('cp /scratches/capp%s/all/jinzhen/%s/S_Rn1/linp /naslx/projects/pr47cu/ru37quz/runs/TDSEsolver_runs_1/%s/S_Rn1/.' % (node_number, folder_number, folder_number))
    os.system('cp /scratches/capp%s/all/jinzhen/%s/S_Rn2/linp /naslx/projects/pr47cu/ru37quz/runs/TDSEsolver_runs_1/%s/S_Rn2/.' % (node_number, folder_number, folder_number))
    os.system('sed -i -e "s/TOREPLACE/%s/g" script_test ' % folder_number)
    os.system('sbatch script_test')
    os.system('sed -i -e "s/%s/TOREPLACE/g" script_test ' % folder_number)


if __name__ == '__main__':
    create_folder_project(sys.argv[1], sys.argv[2])
