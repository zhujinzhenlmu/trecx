#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
import sys
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.interpolate
from scipy.interpolate import griddata
import copy  as cp

if len(sys.argv)<7:
    print "reads surface points from grad-file and plots, usage:"
    print "\ngrad.y which cut=n1,n2,n3,... base-dir run1,run2,... time1,time2,... nphi"
    print "\nwhich...val|dphi|deta|dr, cut..eta|phi"
    print "NOTE: for now the number of nphi=points must be given as last input argument"
    sys.exit(0)

# get python file name (as run from the command line)
name=sys.argv[0].split(".py")[0]

# which value to show
which=sys.argv[1]

# which cut to use
cut=sys.argv[2]
cutCoor=cut.split("=")[0]
cuts=cut.split("=")[1].split(",")

# base directory to use
dire=sys.argv[3]

if dire.find('/')!=len(dire)-1: dire+="/"

# list of runs to show
runs=sys.argv[4].split(',')

# list of times to show
times=sys.argv[5].split(',')

# for now, we need to supply the number of phi-points
nPhi=int(sys.argv[6])
nEta=0

if (len(times)>1 and len(runs)>1) or (len(runs)>1 and len(cuts)>1) or (len(times)>1 and len(cuts)>1):
    print "specify only one list of either cuts,files, or times, is: ",sys.argv[2]," files:",sys.argv[4],"times:",sys.argv[5]
    sys.exit(0)

#======================================================================
# end of input
#======================================================================

#===========================================================
# set up plot panels
#========================================================
# Two panels
fax,(ax1,ax2) = plt.subplots(nrows=2,sharex=True)
gs = gridspec.GridSpec(2,1,height_ratios=[3,3])

# upper and lower panel
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])

# plot colors for easier comparison
colr=['red','green','blue','cyan','magenta','brown']

if cutCoor=="eta":
    coor0=0
    coor1=2*3.1415927
elif cutCoor=="phi":
    coor0=-1
    coor1=1
else:
    print "coordinates must be eta or phi, is: "+cutCoor
    sys.exit(0)


#===========================================================
# add a graph to plot
#===========================================================
def addPlot(run,which,cutN,nCoor,vals,lab):

    if   which=="val":  iStart=1
    elif which=="dphi": iStart=3
    elif which=="deta": iStart=5
    elif which=="dr":   iStart=7
    else: print "cannot plot which="+which

    if cutCoor=="eta":
        iStart+=8*int(cutN-1)
        incr=8*nEta
    else:
        iStart+=8*(int(cutN)-1)*nEta
        incr=8
    iStop=iStart+nCoor*incr

    x=np.array([float(coor0)+i*(float(coor1)-float(coor0))/(nCoor-1) for i in range(nCoor)])
    y=np.array([float(vals[i]) for i in range(iStart,iStop,incr)])
    ax1.plot(x,y,'-o',label=vals[0]+"."+run,color=colr[ic])
    y=np.array([float(vals[i]) for i in range(iStart+1,iStop+1,incr)])
    ax2.plot(x,y,'-o',label=lab,color=colr[ic])


#===========================================================
#loops through directories, times, or cuts
#===========================================================
ic=0 # color count
tit=dire
for run in runs:
    if len(runs)>1: lab="run="+run;
    else:          tit+=run

    # read lines for run
    f = open(dire+run+"/grad_Phi.Eta.RnAscii")
    lines = f.readlines()
    f.close()

    for time in times:
        time_previous=-1.e24
        for l in lines:
            vals=l.split(",")
            nEta=(len(vals)-1)/(8*nPhi)

            if   cutCoor=="eta": nCoor=int(nPhi)
            elif cutCoor=="phi": nCoor=int(nEta)

            if time_previous<float(time) and float(time)<=float(vals[0]):
                if len(times)>1: lab=vals[0]
                elif run==runs[0]:  tit+=" time="+vals[0]
                for cutN in cuts:
                    if len(cuts)>1: lab="cut="+cutN
                    elif run==runs[0] and time==times[0]: tit+=" "+cut

                    # add plot for given time
                    addPlot(run,which,int(cutN),nCoor,vals,lab)
                    ic=ic+1 # increment color count

                break
            time_previous=float(vals[0])
        else:
            print "time=",time," not found in file= "+dire+"/"+run+"/grad_Phi.Eta.RnAscii"

#===========================================================
# finalize the plots
#===========================================================
plt.text(0.5, 1.08,tit,horizontalalignment='center',fontsize=20,transform = ax1.transAxes)

# axis labels
ax1.set_ylabel("abs:"+which,fontsize=14)
ax2.set_ylabel("phas:"+which,fontsize=14)
if cutCoor=="phi":
    ax2.set_xlabel("Eta",fontsize=14)
else:
    ax2.set_xlabel("Phi",fontsize=14)

# set plot limits
ax1.set_xlim([coor0,coor1])
ax2.set_xlim([coor0,coor1])

# Print legend without the box
lg = plt.legend(bbox_to_anchor=(1, 1))
lg.draw_frame(False)

# Merge the x axis with lower panel
fax.subplots_adjust(hspace=0.1)
plt.setp([a.get_xticklabels() for a in fax.axes[:-1]], visible=False)


plt.savefig(name+".png")

plt.show(block=False)
answer = raw_input("plot on "+name+".png\n<return> to finish")

