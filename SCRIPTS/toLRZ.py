#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
import os
import sys
import shutil

# ALL localization is on gamess_local.py
#from gamess_local import VERNO,NCPU,TARGET,SCRATCH,EXEDIR

if len(sys.argv)<2:
    print "send single file to TDSEsolver/ on LRZ\nusage:\n>LRZfrom.py fileName"
    sys.exit(0)

os.system('rsync -rav --exclude="*surf*" --exclude="*grad*" '+sys.argv[1]+' ri32bif@lxlogin6.lrz.de:TDSEsolver/')

