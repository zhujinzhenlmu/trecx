#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
import sys
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.interpolate
from scipy.interpolate import griddata
import copy  as cp
from tools import DataFile

normalize=False
smoothRange=0.

errRange=1.e-5  # plot in [maxErr*errRange,maxErr]
plotRange=1.e-7 # plot in [max*plotRange,max]

if len(sys.argv)<4:
    print "compares two-column data\nusage: compare what(eig,spec,wf+time,expec:5,...) directory_base refer comp1 comp2 ..."
    sys.exit(0)

# get python file name (as run from the command line)
name=sys.argv[0].split(".py")[0]

# what to compare
what=sys.argv[1]
kind=what.split("[")[0]
if len(what.split("["))==1: col=1
else:                       col=int(what.split("[")[1].split("]")[0])

# semi-log?
logY=True
if kind=="expec": logY=False

# base directory to use
dire=sys.argv[2]

if dire.find('/')!=len(dire)-1: dire+="/"

# use first directory for reference
refe=sys.argv[3]

# Two panels
fax,(ax1,ax2) = plt.subplots(nrows=2,sharex=True)
gs = gridspec.GridSpec(2,1,height_ratios=[3,3])

# upper and lower panel
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])

# plot colors for easier comparison
colr=['red','green','blue','cyan','magenta','brown']

# reference calculation
delim=DataFile(dire+refe+"/"+kind).colSep
if delim=="": delim=None

dat=np.loadtxt(dire+refe+"/"+kind,delimiter=delim,unpack=True)

x=dat[0]
f1=dat[col]
norm0=f1[len(f1)/2]


xplot=np.array(x)
#if kind=="spec": xplot=xplot*xplot/2

# plot ranges
xmin=xplot[0]
xmax=xplot[-1]

lab=refe
if logY:
    ax1.semilogy(xplot,f1,label=lab,color='black')
else:
    ax1.plot(xplot,f1,label=lab,color='black')

lg = ax1.legend()
lg.draw_frame(False)

ic=0
errmax=-1
errmin=1
fmax=np.max(f1)
fmin=np.min(f1)
for comp in sys.argv[4:]:
    ic=(ic+1)%len(colr)

    # get columns from file
    dat2=np.loadtxt(dire+comp+"/"+kind,delimiter=delim,unpack=True)
    x2=dat2[0]
    f2=dat2[col]

    fx=griddata(x2,f2,(x,),method='linear')
    norm=fx[len(fx)/2]
    print "norms",norm,norm0
    if normalize: fx*=norm0/norm

    if logY:
        ax1.semilogy(xplot,fx,color=colr[ic])
    else:
        ax1.plot(xplot,fx,color=colr[ic])
    fmin=min(fmin,np.min(fx))
    fmax=max(fmax,np.max(fx))

    lab=comp+" "+str(norm0/norm)
    ax2.semilogy(xplot,abs((fx-f1))/(fx+f1),label=lab,color=colr[ic])
    errmin=min(errmin,np.min(abs((fx-f1))/(fx+f1)))
    errmax=max(errmax,np.max(abs((fx-f1))/(fx+f1)))


# axis labels
ax1.set_ylabel(what,fontsize=14)
ax2.set_xlabel("x (a.u.)",fontsize=14)
ax2.set_ylabel("Relative error",fontsize=14)


# set plot limits
xmin=np.min(xplot)
xmax=np.max(xplot)

errmin=max(errmax*errRange,errmin)
ax1.set_xlim([xmin,xmax])
if kind!="expec": fmin=max(fmin,fmax*plotRange)
ax1.set_ylim([fmin,fmax])
ax2.set_xlim([xmin,xmax])
ax2.set_ylim([errmin,errmax])

# Print legend without the box
lg = plt.legend(bbox_to_anchor=(1, 1))
lg.draw_frame(False)

# Merge the x axis with lower panel
fax.subplots_adjust(hspace=0.1)
plt.setp([a.get_xticklabels() for a in fax.axes[:-1]], visible=False)

plt.savefig(name+".png")

plt.show(block=False)
answer = raw_input('<return> to finish')

