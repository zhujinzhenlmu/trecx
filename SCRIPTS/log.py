#!/usr/bin/env python3
import os
import time
import csv
try:
    import argparse
except Exception as err:
    import sys
    sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), "../LIBRARIES"))
    import argparse

rows, columns = os.popen('stty size', 'r').read().split()
terminal_cols = int(columns)


def bold(text, do_print_bold=True):
    if do_print_bold:
        return "\033[1m%s\033[0m" % text
    else:
        return text


class Legend:
    def __init__(self):
        self.data = []

    def add(self, name):
        self.data += [name]
        return len(self.data)

    def get(self, d):
        for i, d_ in enumerate(self.data):
            if d == d_:
                return i + 1

        return -1


class Buffer:
    def __init__(self):
        self.buffer = []

    def set(self, row, col_begin, col_end, idx):
        while len(self.buffer) < row + 1:
            self.buffer += [""]

        while len(self.buffer[row]) < col_end:
            self.buffer[row] += " "

        output = "%d" % idx

        if len(output) < col_end - col_begin:
            alt = 0
            while len(output) < col_end - col_begin - 2:
                if alt % 2 == 0:
                    output += "="
                else:
                    output = "=" + output
                alt += 1

            output = "|" + output + "|"

        output = output[0:col_end - col_begin]
        tmp = self.buffer[row][:col_begin] \
            + output \
            + self.buffer[row][col_end:]
        self.buffer[row] = tmp

    def plot(self):
        for c in self.buffer:
            print(c)


class Renderer:
    def __init__(self, legend):
        self.legend = legend
        self.buffer = Buffer()

    def render(self, root, begin_space=-1, end_space=-1):
        if begin_space == -1:
            begin_space = 0
        if end_space == -1:
            end_space = terminal_cols

        if end_space - begin_space < 4:
            return

        idx = self.legend.add(root)
        self.buffer.set(root.depth(), begin_space, end_space, idx)

        children_spaces = []
        for c in root.children:
            begin = (end_space - begin_space) * (c.begin - root.begin) / \
                (root.end - root.begin) + begin_space
            end = (end_space - begin_space) * (c.end - root.begin) / \
                (root.end - root.begin) + begin_space

            children_spaces += [[int(begin), int(end)]]

        for c, s in zip(root.children, children_spaces):
            self.render(c, *s)

    def plot(self):
        self.buffer.plot()


class Epoch:
    def __init__(self, name, begin, parent):
        self.name = name
        self.begin = begin
        self.end = None
        self.parent = parent
        self.children = []
        self.begin_rss_peak = None
        self.end_rss_peak = None

        if self.parent is not None:
            self.parent.children += [self]

    def depth(self):
        if self.parent is None:
            return 0
        return self.parent.depth() + 1

    def leaves(self):
        if len(self.children) == 0:
            return 1

        result = 0
        for c in self.children:
            result += c.leaves()

        return result

    def print_full(self, legend, indent=0):
        name = self.name.split("-")[-1]
        idx = legend.get(self)
        if idx != -1:
            name += " (%d)" % idx

        rss = ""
        print_bold = False
        if self.begin_rss_peak is not None and self.end_rss_peak is not None:
            rss = ", %5.2fGiB" % (self.end_rss_peak - self.begin_rss_peak)
            print_bold = (self.end_rss_peak - self.begin_rss_peak) > 1.

        print((indent*"  "), end='')
        print(bold("|%8.2fs%s: %s" % (float(self.end - self.begin)/1000,
                                      rss, name), print_bold))
        for c in self.children:
            c.print_full(legend, indent + 1)

    def purge(self, duration):
        new_children = []
        for c in self.children:
            if c.end - c.begin >= duration or c.end_rss_peak != c.begin_rss_peak:
                new_children += [c]

        self.children = new_children
        for c in self.children:
            c.purge(duration)

    def find_child(self, name):
        if self.name == name:
            return self

        for c in self.children:
            res = c.find_child(name)
            if res is not None:
                return res

        return None

    def plot(self, renderer, legend, depth):
        if self.depth() < depth:
            for c in self.children:
                c.plot(renderer, legend, depth)
        elif self.depth() == depth:
            renderer.draw(self.begin,
                          legend.add(self.name),
                          self.end)


def parse_rss(rep):
    rss_gib = rep.find("RSS(GiB)")
    if rss_gib == -1:
        return -1, -1

    slash = rep.find("/", rss_gib)
    if slash == -1:
        return -1, -1

    space = rep.find(" ", slash)
    if space == -1:
        return -1, -1

    return float(rep[rss_gib + 9:slash]), \
        float(rep[slash + 1:space])


class MemInfo:
    def __init__(self):
        self.times = []
        self.rss = []
        self.rss_peak = []

    def add(self, time, rep):
        rss, rss_peak = parse_rss(rep)
        if rss == -1 or rss_peak == -1:
            return

        self.times += [time]
        self.rss += [rss]
        self.rss_peak += [rss_peak]

    def plot(self, begin, end):
        times = []
        rss = []
        rss_peak = []

        for i in range(len(self.times)):
            if self.times[i] < begin or self.times[i] > end:
                continue
            times += [self.times[i]]
            rss += [self.rss[i]]
            rss_peak += [self.rss_peak[i]]

        max_rss = max(rss_peak)
        min_rss = min(rss)

        if max_rss == min_rss:
            max_rss += 1

        xs = []
        y1s = []
        y2s = []
        if len(times) > 0:
            for i in range(len(times) + 1):
                t = end if i == len(times) else times[i]
                r = rss[i-1] if i == len(times) else rss[i]
                r_peak = rss_peak[i-1] if i == len(times) \
                    else rss_peak[i]

                x = terminal_cols * (t - begin) / (end - begin)
                y1 = 10 * (r - min_rss) / (max_rss - min_rss)
                y2 = 10 * (r_peak - min_rss) / (max_rss - min_rss)

                if x >= terminal_cols:
                    x -= 1

                xs += [int(x)]
                y1s += [y1]
                y2s += [y2]

        vals = [terminal_cols * [0] for i in range(11)]
        for i in range(1, len(xs)):
            for x in range(xs[i-1], xs[i]):
                y1 = int(y1s[i-1] + (y1s[i] - y1s[i-1]) * (x - xs[i-1]) /
                         (xs[i] - xs[i-1]))
                y2 = int(y2s[i-1] + (y2s[i] - y2s[i-1]) * (x - xs[i-1]) /
                         (xs[i] - xs[i-1]))

                # Draw y1 on top
                vals[y2][x] = 2
                vals[y1][x] = 1

        vals[int(y2s[-1])][xs[-1]] = 2
        vals[int(y1s[-1])][xs[-1]] = 1

        left_padding = int((terminal_cols - 11)/2)
        right_padding = left_padding
        if right_padding + left_padding + 11 < terminal_cols:
            left_padding += 1

        print(left_padding * "#", end='')
        print(" %5.2f GiB " % max_rss, end='')
        print(right_padding * "#")
        for i in range(len(vals) - 1, -1, -1):
            for j in range(len(vals[i])):
                if vals[i][j] == 1:
                    print('+', end='')
                elif vals[i][j] == 2:
                    print('-', end='')
                else:
                    print(' ', end='')

        print(left_padding * "#", end='')
        print(" %5.2f GiB " % min_rss, end='')
        print(right_padding * "#")


epochs = dict()
root = Epoch("root", 0, None)
mem_info = MemInfo()

parser = argparse.ArgumentParser(
    description='Pretty print log files generated by TOOLS/log.h')

parser.add_argument('logfile',
                    metavar='file',
                    type=str,
                    help='Path to logfile or folder containing logfile, ' +
                    'i. e. Helium/0000/log_0.csv or Helium/0000')

parser.add_argument('-c',
                    dest='currenttime',
                    action='store_true',
                    help='Use current time at right boundary as opposed ' +
                    'to last logged time plus one second')

parser.add_argument('-r',
                    dest='root',
                    type=str,
                    help='Select certain stack entry as root, e. g. "setup"')

parser.add_argument('-p',
                    dest='purge',
                    action='store_true',
                    help='Do not show stack entries that are shorter than a second')

parser.set_defaults(currenttime=False)
parser.set_defaults(root="")
args = parser.parse_args()

logfile = args.logfile

if os.path.isfile(logfile) or \
        os.path.isfile(os.path.join(logfile, 'log_0.csv')):
    if os.path.isdir(logfile):
        logfile = os.path.join(logfile, 'log_0.csv')

    with open(logfile, 'r') as csvfile:
        reader = csv.reader(csvfile)
        last_name = None
        last_push_name = None
        last_pop_name = None
        for row in reader:
            if len(row) < 7:
                continue

            if row[2] == "MEM_INFO":
                mem_info.add(int(row[1]), row[6])

                rss, rss_peak = parse_rss(row[6])
                if last_push_name is not None and rss_peak != -1:
                    epochs[last_push_name].begin_rss_peak = rss_peak
                elif last_pop_name is not None and rss_peak != -1:
                    epochs[last_pop_name].end_rss_peak = rss_peak

            name = row[3]
            if row[6] == "PUSH":
                begin = int(row[1])
                parent_name = None
                if "-" in name:
                    parent_name = "-".join(name.split("-")[:-1])

                parent = root
                if parent_name is not None and parent_name in epochs:
                    parent = epochs[parent_name]
                epochs[name] = Epoch(name, begin, parent)
                last_push_name = name
            else:
                last_push_name = None

            if row[6] == "POP":
                if last_name in epochs:
                    epochs[last_name].end = int(row[1])
                    last_pop_name = last_name
            else:
                last_pop_name = None

            last_name = name

    """
    Set end time if log is not complete
    and begin time of root
    """
    artificial_end = max([epochs[n].end for n in epochs
                          if epochs[n].end is not None])
    artificial_end += 1000

    if args.currenttime:
        artificial_end = int(round(time.time() * 1000))

    begin = min([epochs[n].begin for n in epochs])

    def set_end(e):
        if e.end is None:
            e.end = artificial_end
        for c in e.children:
            set_end(c)

    set_end(root)

    root.begin = begin
    root.end = artificial_end

    if args.root != "":
        root = root.find_child(args.root)
        if root is None:
            print("Could not find '%s'" % args.root)

    """
    Delete epochs shorter than one second
    """
    if args.purge:
        root.purge(1000)

    """
    Plot
    """
    mem_info.plot(root.begin, root.end)
    print()
    legend = Legend()
    renderer = Renderer(legend)
    renderer.render(root)
    renderer.plot()
    root.print_full(legend)

else:
    subdirs = next(os.walk(logfile))[1]
    subdirs = [s for s in subdirs if
               os.path.isfile(os.path.join(logfile, s, 'log_0.csv'))]

    def parse_mon(path):
        with open(path, 'r') as mon:
            data = None
            for r in mon:
                if "Date" in r:
                    data = "NEXT"
                elif data == "NEXT" and r.strip() != "":
                    data = r
                    break

            if data is None:
                return ""

            return data.strip()

    def keyfunc(d):
        try:
            return int(d)
        except Exception:
            return 100000 + hash(d)

    subdirs = sorted(subdirs, key=keyfunc)
    print(bold("Run       \tTime\tRSS        \tRSS (Peak)\t" +
               "Current                       \tmon"))
    for d in subdirs:
        if not os.path.isfile(os.path.join(logfile, d, 'log_0.csv')):
            continue

        mon = ""
        if os.path.isfile(os.path.join(logfile, d, 'S1_mon')):
            mon = "S1: " + parse_mon(os.path.join(logfile, d, 'S1_mon'))
        elif os.path.isfile(os.path.join(logfile, d, 'mon')):
            mon = parse_mon(os.path.join(logfile, d, 'mon'))

        with open(os.path.join(logfile, d, 'log_0.csv')) as log:
            reader = csv.reader(log)
            first_ts = None
            last_epoch = None
            last_rss = None

            next(reader)
            for row in reader:
                if first_ts is None:
                    first_ts = int(row[1])
                last_epoch = row[3]

                if row[2] == "MEM_INFO":
                    last_rss = row[6]

            rss, rss_peak = 0, 0
            if last_rss is not None:
                rss, rss_peak = parse_rss(last_rss)

            deltat = int(round(time.time() * 1000)) - first_ts
            print("%-10s\t%ds\t%.2fGiB  \t%.2fGiB  \t%-30s\t%s" %
                  (d, deltat/1000, rss, rss_peak, last_epoch, mon))



