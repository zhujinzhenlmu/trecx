import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt


class MPLDistribution:
    def __init__(self, descriptor, x_axis, y_axis, cutoff=-12, reducer=np.max):
        self.descriptor = descriptor
        self.x_axis = x_axis
        self.y_axis = y_axis
        self.cutoff = cutoff
        self.reducer = reducer

        self.x_axis = self.descriptor.hierarchy.index(self.x_axis)
        self.y_axis = self.descriptor.hierarchy.index(self.y_axis)

        self.x_min = int(np.min([i[self.x_axis] for i in descriptor.indices]))
        self.x_max = int(np.max([i[self.x_axis] for i in descriptor.indices]))
        self.y_min = int(np.min([i[self.y_axis] for i in descriptor.indices]))
        self.y_max = int(np.max([i[self.y_axis] for i in descriptor.indices]))

        self.fig = None
        self.ax = None
        self.im = None

    def reset(self):
        self.fig = None
        self.ax = None
        self.im = None

    def plot(self, coefficients):
        vals = [self.reducer([c.values[i] for c in coefficients])
                for i in range(len(self.descriptor.indices))]

        X = np.arange(self.x_min, self.x_max + 1)
        Y = np.arange(self.y_min, self.y_max + 1)
        X, Y = np.meshgrid(X, Y)

        Z = (self.cutoff - 1.) * np.ones_like(X)
        for v, i in zip(vals, self.descriptor.indices):
            val = np.log(v)/np.log(10)
            Z[self.y_max - int(i[self.y_axis]),
              int(i[self.x_axis]) - self.x_min] = val \
                if val > self.cutoff else self.cutoff

        Z = np.ma.masked_less(Z, self.cutoff - 0.5)

        if self.fig is None:
            fig, (ax) = plt.subplots(1, 1, figsize=(10, 10))
            self.fig = fig
            self.ax = ax

            self.im = ax.imshow(Z, vmin=self.cutoff, vmax=0.,
                                extent=[self.x_min-.5, self.x_max+.5,
                                        self.y_min-.5, self.y_max+.5])
            ax.set_aspect('equal')
            ax.set_xticks(np.arange(self.x_min, self.x_max+1, dtype=np.int))
            ax.set_yticks(np.arange(self.y_min, self.y_max+1, dtype=np.int))
            ax.set_xlabel(self.x_axis)
            ax.set_ylabel(self.y_axis)

            cax, kw = mpl.colorbar.make_axes([ax])
            plt.colorbar(self.im, cax=cax, **kw)
        else:
            self.im.set_data(Z)

        return self.fig


if __name__ == '__main__':
    import sys
    import time  # noqa: E402
    import numpy as np  # noqa: E402

    from coefficients_loader import load_folder  # noqa: E402

    if len(sys.argv) < 4:
        print("Usage: coefficients_distribution.py " +
              "<folder> <first axis> <second axis>")
        exit(1)

    plt.ion()

    descriptor = None
    while descriptor is None:
        try:
            descriptor, _ = load_folder(sys.argv[1])
        except Exception:
            time.sleep(1)

    print("Hierarchy: %s" % descriptor.hierarchy)

    plot_axes = sys.argv[2:4]
    reduce_axes = []
    for a in descriptor.hierarchy:
        if a not in plot_axes:
            reduce_axes += [a]

    distribution = MPLDistribution(
        descriptor.reduce_over(reduce_axes), *plot_axes, cutoff=-9)
    while True:
        _, coefficients = load_folder(sys.argv[1])
        coefficients = [c.reduce_over(reduce_axes, np.sum)
                        for c in coefficients]
        # for c in coefficients:
        #     s = np.sum(c.values)
        #     if(abs(s - 1.) > 1.e-3):
        #         print("Warning: Values do not sum up to one:" +
        #               " %5.2e" % s)

        distribution.plot(coefficients)
        plt.pause(1.)

