#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
"""
plot one or several data-files with column-wise storage

columns can be specified in square brackets after the file name
        in the form fileName[2,3,7-9] for plotting columns 2,3,7,8,9
        without square bracket, all columns are plotted
column 0 of each file is interpreted as x-axis by default
       for different x-axis use [3:7,8], which plot columns 7,8 against 3
column headers will be recognized and used as legend labels
"""

import sys

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.interpolate
from scipy.interpolate import griddata
import copy  as cp
from math import *

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import colors, ticker, cm
from matplotlib.colors import LogNorm

from tools import *
from plotTools import *

(files,flags) = argsAndFlags();

if len(files)<1:
    print "Usage: "
    print "   plot.py file1[{xcol:}colrange] file2[{xcol:}colrange] ... {flags}"
    print "Example:"
    print "   plot -b=Argon/ -a=/spec[3:22,26,30] 0015 0014 -label='I(W/cm2),lambda(nm)'"
    print "     will plot column 3 vs columns 22,26,30 of files Argon/0015/spec and Argon/0014/spec"
    print "     with legend labels including intensity and wavelength"
    print"Command line flags:"
    print"  -b=DIR......... prefix input file by DIR"
    print"  -a=WHICH..... postfix input file by WHICH"
    print"  -label=PAR1,PAR2... parameter value(s) for PAR1,PAR2,... will be used in plot legend"
    print"  -linY.... plot on linear y-axis (default is log)"
    print"  -peaks... mark photon peaks at n*omega-Up-Ip"
    print"  -eV...... x -> x^2/2 *Rydberg"
    print"  -vrange[vmin,vmax]... function value range for ALL plots "
    print"  -xrange[xmin,xmax]... plot axis range for All plots "
    print"  -yrange[ymin,ymax]... plot axis range for All plots "
    print"  -polar            ... polar plot of 2d data"
    print"  -equalAx          ... 2d with commensurate axes"
    print"  -rmax=r           ... radius in polar plot"
    sys.exit(0)

dir,which=prePost(flags)

# get total number of datasets to be plotted
nSets=0
for f in files:
    file=dir+f+which
    if file.find("[")==-1:
        nSets+=1
    else:
        nSets=nSets+len(expandRange(file[max(file.find('['),file.find(':'))+1:file.find(']')]) )

theta=[] # may need this
replot=False
def allPlots():
    global fax

    # loop through input files
    plot=Plot(flags)
    previous=""
    previousDir=""
    nPlot=-1
    #loop through files
    for info in files:
        file=dir+info+which

        print "file",file
        pars=RunParameters(file[:file.rfind('/')])

        # get data into selected columns
        datfil=DataFile(file)

        if info==files[0]:
            # first file --- set up the figure
            fax=plt.figure()
            if datfil.isMulti and not plot.isLineout():
                """ two axes -- separate panels """
                cmap = plt.cm.get_cmap("gnuplot")
                nr,nc=plot.layout(nSets)
                gs = gridspec.GridSpec(nr,nc,height_ratios=[1,1])
            else:
                """ single axis --- all in one panel """
                gs = gridspec.GridSpec(1,1,height_ratios=[1,])
                colr=['red','green','blue','cyan','magenta','brown']
                axn=fax.add_subplot(gs[0])
                leg=Legend(axn,dir,which,flags)

        # loop through columns in file
        for col in datfil.datCols:
            nPlot+=1

            ### 2d plots in separate panels
            if datfil.isMulti and not plot.isLineout():
                xVal=datfil.xAxis()
                yVal=datfil.yAxis()
                fVal=datfil.column(col)

                if "-eV" in flags:
                    for k in range(len(xVal)): xVal[k]*=xVal[k]*0.5*27.211
                    for k in range(len(yVal)): yVal[k]*=yVal[k]*0.5*27.211

                ratio=1.e-5
                if "-linY" in flags: ratio=None
                vMin,vMax=getRange(flags,fVal,"-vrange",ratio)
                xMin,xMax=getRange(flags,xVal,"-xrange")
                yMin,yMax=getRange(flags,yVal,"-yrange")

                # extend the plot sizes to present
                plot.extend(np.min(fVal),np.max(fVal),np.min(xVal),np.max(xVal),np.min(yVal),np.max(yVal))

                #xyzToAxisArray(xVal,yVal,fVal)
                x,y,v=xyzToAxisArray(xVal,yVal,fVal)

                if file!=info: plt.suptitle(dir+"*"+which,fontsize=20)

                if "-linY" in flags or "-linV" in flags: lev,tic=linLevels(vMin,vMax)
                else:                                    lev,tic=logLevels(vMin,vMax)

                if "-polar" in flags:
                    # guessing coordinate meaning from file name
                    if file.find("/spec")==len(file)-5:
                        sys.exit("need \"Eta\" or \"Phi\" in file name for -polar, file-name is: "+file)
                    theta=y
                    if file.find("Phi")!=-1:
                        theta=np.arccos(y)
                        for j in range(len(y)): v[j,:]*=sqrt(1-min(y[j]*y[j],1)) # correct weight for theta

                    axn=fax.add_subplot(gs[nPlot],projection='polar')
                    v+=vMin
                    CS=axn.contourf(theta,x,np.transpose(v,[1,0]),lev,cmap=cmap,vmin=vMin,vmax=vMax,locator=ticker.LogLocator())
                    axn.set_rmax(float(flagValue(flags,"-rmax",xMax)))
                else:
                    axn=fax.add_subplot(gs[nPlot])
                    if file.find("kXkY")==len(file)-4 or "-equalAx" in flags:
                        axn.set_aspect('equal', 'box')

                    vMin,vMax=getRange(flags,plot.dim[2],"-vrange=")
                    yMin,yMax=getRange(flags,plot.dim[1],"-yrange=")
                    xMin,xMax=getRange(flags,plot.dim[0],"-xrange=")

                    print "values",vMin,vMax,v


                    if "-linV" in flags:
                        CS=axn.contourf(x,y,v,lev,cmap=cmap,vmin=vMin,vmax=vMax)
                    else:
                        if vMin<=0: sys.exit("cannot log-plot, lowest value is "+str(vMin)+", specify -vrange=[vmin,vmax]")
                        CS=axn.contourf(x,y,v,lev,cmap=cmap,vmin=vMin,vmax=vMax,locator=ticker.LogLocator())
                    axn.set_xlim(xMin,xMax)
                    axn.set_ylim(yMin,yMax)

                if pars.hasParameters():
                    title=info+" I="+pars.item("I(W/cm2)",0)+" phi="+pars.item("phiCEO",0)
                    axn.set_title(title)

                cbar=fax.colorbar(CS,ticks=tic,ax=axn, shrink=0.7)
                cbar.ax.set_ylabel('yield')

            ### graphs, all in one panel
            else:
                if plot.isLineout():
                    xVal=datfil.xAxis()
                    yVal=datfil.yAxis()
                    fVal=datfil.column(col)
                    atCoor,xVal,fVal=plot.lineout(xVal,yVal,fVal)
                    print "linout at "+str(atCoor)
                else:
                    xVal=datfil.xAxis()
                    fVal=datfil.column(col)

                if "-eV" in flags:
                    for k in range(len(xVal)):
                        fVal[k]*=pow(xVal[k],1)
                        xVal[k]*=xVal[k]*0.5*27.211

                # extend the plot sizes to present
                plot.extend(np.min(fVal),np.max(fVal),np.min(xVal),np.max(xVal))

                # get label (parameter values can be prepended by "-label=PARNAME")
                lab=str(col)+": "+leg.label(axn,col,datfil,info,pars)

                fNegative=np.min(fVal)<=0.
                if (fNegative and "-logY" not in flags) or "-linY" in flags:
                    axn.plot(xVal,fVal,label=lab,color=colr[(nPlot+1)%len(colr)])
                else:
                    axn.semilogy(xVal,fVal,label=lab,color=colr[(nPlot+1)%len(colr)])


    if not datfil.isMulti or plot.isLineout():

        # single plot, adjust sizes
        xMin,xMax=getRange(flags,plot.dim[0],"-xrange")
        vMin,vMax=getRange(flags,plot.dim[2],"-vrange")

        if "-polar" not in flags:
            axn.set_xlim(xMin,xMax)
            if "-linY" in flags: axn.set_ylim(vMin,vMax)
            else:                axn.set_ylim(vMin,vMax)

        # axis labels
        xlab="x"
        if "-eV" in flags: xlab="eV"
        axn.set_xlabel(xlab,fontsize=14)

        # Print legend
        lg = axn.legend()

        # mark peak positions (if so desired)
        if "-peaks" in flags: peakPositions(plot,pars,axn)

answer="u"
while answer=="u":
    allPlots()
    plt.show(block=False)
    answer = raw_input('"u" to update ')
    plt.close()
    replot=True

# get python file name (as run from the command line)
name=sys.argv[0].split(".py")[0]
fax.savefig(name+".png")

