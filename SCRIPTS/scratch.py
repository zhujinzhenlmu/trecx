import os
import sys

USERNAME = "ru54hiv"

for i in range(18, 36):
    os.system('df -m /scratches/capp%d/all | grep scratch' % i)

for k in range(1, len(sys.argv)):
    folder = sys.argv[k]
    folder = os.path.normpath(os.path.join(os.getcwd(), folder))
    parent = os.path.normpath(os.path.join(folder, os.pardir))


    def scratch_folder(i):
        rel_path = folder[folder.find(USERNAME):]
        return os.path.join('/scratches/capp%d/all' % i, rel_path)


    for i in range(18, 36):
        if os.path.isdir(scratch_folder(i)):
            print("------------------------------------------------")
            print(scratch_folder(i))
            print("------------------------------------------------")
            os.system('ls -l %s' % scratch_folder(i))

            answer = input("Sync? (Y/n) ")
            if answer != "N" and answer != "n":
                os.system(
                    'rsync -rav --exclude="*surf*" --exclude="*grad*" ' +
                    '--exclude="*ampl*" --exclude="wf*" --exclude="S0_wf*" --exclude="S1_wf*" ' +
                    '%s %s' % (scratch_folder(i), parent))

