#! /usr/bin/env python

# The tRecX package is free for personal use.
# Any commercial use of the code or parts of it is excluded.
# Restrictions for academic use apply. 
# 
# See terms of use in the LICENSE file included with the source distribution
# 
# Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
# End of license
 
import sys
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import scipy.interpolate
from scipy.interpolate import griddata
import copy  as cp
from tools import DataFile

if len(sys.argv)<3:
    print "re-stores spectra for Esry benchmark"
    sys.exit(0)

# get python file name (as run from the command line)
name=sys.argv[0].split(".py")[0]

dir=sys.argv[1]
lowN=sys.argv[2]

upFile=dir+"/spec"
loFile=dir+"/specR"+lowN

print loFile

delim=DataFile(loFile).colSep
if delim=="": delim=None

# get columns from file
dat=np.loadtxt(loFile,delimiter=delim,unpack=True)
xlo=dat[0]
flo=dat[1]

dat=np.loadtxt(upFile,delimiter=delim,unpack=True)
xup=dat[0]
fup=dat[2]

outName="benchmark_tRecX"
outFile = open(outName,'w')
outFile.write("# authors: Armin Scrinzi\n")
outFile.write("# method: tRecX\n")
outFile.write("# sequential:\n")
outFile.write("# cos(theta k) low-energy high-energy\n")

pi=3.1415927
fac=8*pi*pi*pi
fac=1
print pi,fac

klo=0.20959*fac
kup=2.0959*fac

for i in range(len(xlo)):
    print >>outFile,xlo[i],flo[i]*klo,fup[i]*kup

print "files:",upFile,loFile
print " --- output on "+outName+" ---"

