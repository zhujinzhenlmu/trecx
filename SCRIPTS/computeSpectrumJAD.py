# #!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
#from scipy.interpolate import interp1d
from scipy.special import sph_harm
#from matplotlib import gridspec
import sys
import os
import re
import struct


def computeSpectrum( folder ):
    WRITE_SPECTRUM = True
    PLOT_SI_SPECTRUM = True
    PLOT_DI_JAD = True
    
    absMomenta,lmax,appearingM,DIspecFiles,SIspecFiles = loadBasicData(folder)
    print 'mmax:', -appearingM[0],' lmax:',lmax   
    
    numberOfAngles = 64
    JADangles = rangeDouble(0,np.pi,np.pi/(numberOfAngles-1))   
    try:
        E_select1 = float(sys.argv[2])
        E_select2 = float(sys.argv[3])
    except:
        print 'no need to plot JAD with input energy'
    if PLOT_DI_JAD:
        if os.path.isfile(getJADSpectrumFilename(folder,0)):
            JADspectrum = np.loadtxt(getJADSpectrumFilename(folder,0))
            thetagrid = np.loadtxt(getJADthetaGridFilename(folder,0))
            E1 = []; E1.append(np.loadtxt(getJADenergiesFilename(folder,0,1)))
            E2 = []; E2.append(np.loadtxt(getJADenergiesFilename(folder,0,2)))
            # JADspectrumBase = np.copy(JADspectrum)
            plotJAD(thetagrid,JADspectrum,E1[0],E2[0])
            PLOT_DI_JAD = False
            for compare in sys.argv[1:]:
                if 'compare' in compare:
                    print folder + '/' + compare.split('compare')[0]
                    JADspectrumCompare = np.loadtxt(getJADSpectrumFilename(folder + compare.split('compare')[0]+ '/' , 0))
                    JADmax = np.max(JADspectrumCompare)
                    angleCap = JADmax * 0.01
                    JADspectrum[JADspectrumCompare < angleCap] = angleCap
                    anglenormalize = 1.0 / JADmax
                    JADspectrumCompare *= anglenormalize;
                    JADspectrumCompare = abs(JADspectrumCompare - JADspectrum)
                    E1_compare = []
                    E1_compare.append(np.loadtxt(getJADenergiesFilename(folder + compare.split('compare')[0]+ '/' , 0, 1)))
                    E2_compare = []
                    E2_compare.append(np.loadtxt(getJADenergiesFilename(folder + compare.split('compare')[0]+ '/' , 0, 2)))
                    plotJAD(thetagrid, JADspectrumCompare, E1_compare[0], E2_compare[0], use_compare=compare, figure_index=2 + sys.argv.index(compare), get_norm=False)
        else:
            JADspectrum = np.zeros([len(JADangles)*2-1,len(JADangles)*2-1], dtype=np.double)
            E1 = [E_select1];E2 = [E_select2] # from input
            sphericalharmonics1plus , sphericalharmonics2plus , sphericalharmonics1minus , sphericalharmonics2minus = initializeSphericalHarmonics(lmax,JADangles,appearingM);
        thetaPicIndecies, E1, E2 = transformThetaPicEnergiesToIndecies(absMomenta**2*0.5,E1,E2);
            
    mymax = len(absMomenta)  
    #mymax = 32     
            
    if os.path.isfile(getTotalDISpectrumFilename(folder)):
        totalSpectrum = np.loadtxt(getTotalDISpectrumFilename(folder))
    else:
        totalSpectrum = np.zeros([mymax,mymax], dtype=np.double)

    if somethingToDo(folder,PLOT_DI_JAD):
        for k1index in range(mymax):
            for k2index in range(k1index,mymax):
                # kgrid of momentum
                if PLOT_DI_JAD and (thetaPicIndecies[k1index,k2index] or thetaPicIndecies[k2index,k1index]):
                    scatteringAmplitudeThetaGrid = np.zeros([len(JADangles)*2-1,len(JADangles)*2-1], dtype=np.complex)
                        
                for mindex in range(len(appearingM)):
                    # phi momentum m
                    m = appearingM[mindex]
                    lmaxLocal = lmax-abs(m)
                    scatteringAmplitudeFixedMFixedKs = readInDIdata(lmaxLocal,DIspecFiles[mindex],DIspecFiles[len(appearingM)-1-mindex],len(absMomenta),k1index,k2index)
                    
                    setTotalSpectrum(totalSpectrum,k1index,k2index,scatteringAmplitudeFixedMFixedKs)

                    
                    if PLOT_DI_JAD and (thetaPicIndecies[k1index,k2index] or thetaPicIndecies[k2index,k1index]):
                        print "the position is" + str(k1index) + ", and " + str(k2index)
                        scatteringAmplitudeThetaGrid += getThetaScatteringAmplitude(JADangles,scatteringAmplitudeFixedMFixedKs,sphericalharmonics1plus ,
                                                                                     sphericalharmonics2plus , sphericalharmonics1minus , sphericalharmonics2minus,mindex)


                if PLOT_DI_JAD and (thetaPicIndecies[k1index,k2index] or thetaPicIndecies[k2index,k1index]): 
                    JADspectrum = abs(scatteringAmplitudeThetaGrid)**2
                    thetagrid = getThetaGrid(JADangles)
                    saveJAD(folder,thetagrid,JADspectrum,E1,E2)
                    plotJAD(thetagrid,JADspectrum,E1[0],E2[0])
                    


            if len(absMomenta)>8 and k1index%8==0: print k1index
        saveTotalSpectrum(folder,totalSpectrum)
    
    plotTotalSpectrum(absMomenta[0:mymax],totalSpectrum)
    
    if PLOT_SI_SPECTRUM:
        plotSIspectrum(absMomenta, SIspecFiles, lmax, folder, WRITE_SPECTRUM)
        
                
        
def somethingToDo(folder,PLOT_DI_JAD):
    if not os.path.isfile(getTotalDISpectrumFilename(folder)): return True
    if not os.path.isfile(getJADSpectrumFilename(folder,0)): return True
    return False
    
def saveJAD(folder,thetagrid,JADspectrum,E1,E2):
    np.savetxt(getJADthetaGridFilename(folder,0), thetagrid)
    np.savetxt(getJADSpectrumFilename(folder,0), JADspectrum)
    np.savetxt(getJADenergiesFilename(folder,0,1), E1)
    np.savetxt(getJADenergiesFilename(folder,0,2), E2)
   
def plotJAD(thetagrid,JADspectrum,E1,E2, use_compare='', figure_index=1, get_norm=True):
    JADmax = np.max(JADspectrum)
    if get_norm:
        angleCap = JADmax * 0.01
        newJADspectrum = JADspectrum.copy()
        JADspectrum[JADspectrum < angleCap] = angleCap
        anglenormalize = 1.0/JADmax
        JADspectrum *= anglenormalize
        newJADspectrum *= anglenormalize
    else:
        min_difference = 0.00001
        JADspectrum[JADspectrum < min_difference] = min_difference
        JADspectrum[JADspectrum > 1] = 1
    plt.figure(figure_index)
    values_of_spectrum = []
    if get_norm:
        fig = plt.contourf(thetagrid*180/np.pi,thetagrid*180/np.pi,np.log10(JADspectrum),40)
    else:
        # JADspectrum[JADspectrum > 0.7] = 0.1
        print 'This is the time when we do not use logscale'

        fig = plt.contourf(thetagrid * 180 / np.pi, thetagrid * 180 / np.pi, JADspectrum, 40)
        print "The maxmum value of the comparision is: " + str(JADmax)
    plt.rc('text', usetex=True)
    plt.xlabel(r"$\theta_1$ (deg)")
    plt.ylabel(r"$\theta_2$ (deg)")
    plt.title(r"$(E_1,E_2)=($"+ "%.2f" % E1 + r"$,$"+ "%.2f" % E2 + r"$)$" + use_compare)
    plt.axis('equal')
    plt.xlim(0,360)
    plt.ylim(0,360)
    plt.colorbar(fig)
    if get_norm:
        plt.figure(4)
        sum_value = {}
        for i in range(len(thetagrid)):
            for j in range(i, len(thetagrid)):
                summation_angle = str(thetagrid[i] + thetagrid[j])
                if summation_angle not in sum_value:
                    sum_value[summation_angle] = 0
                if get_norm:
                    sum_value[summation_angle] += newJADspectrum[i][j] * 2
                else:
                    sum_value[summation_angle] += JADspectrum[i][j] * 2
        sum_theta = sorted([float(key) for key in sum_value.keys()])
        sum_spectrum = [float(sum_value[str(key)]) for key in sum_theta]
        plt.xlabel(r"summation of $\theta$")
        plt.ylabel(r"yeild")
        plt.title(r"$(E_1,E_2)=($"+ "%.2f" % E1 + r"$,$"+ "%.2f" % E2 + r"$)$" + use_compare)
        plt.plot(np.array(sum_theta) / np.pi * 180, sum_spectrum)
        plt.show(block=False)


def getThetaScatteringAmplitude(JADangles,scatteringAmplitudeFixedMFixedKs,sphericalharmonics1plus ,
                                sphericalharmonics2plus , sphericalharmonics1minus , sphericalharmonics2minus,mindex):
    scatteringAmplitudeThetaFixedk = np.zeros([len(JADangles)*2-1,len(JADangles)*2-1], dtype=np.complex)
    
    temp = np.dot(scatteringAmplitudeFixedMFixedKs,sphericalharmonics1minus[mindex])
    scatteringAmplitudeThetaFixedk[0:len(JADangles),0:len(JADangles)] = np.dot(temp.transpose(),sphericalharmonics1plus[mindex])
    
    temp2 = np.dot(temp.transpose(),sphericalharmonics2plus[mindex])
    scatteringAmplitudeThetaFixedk[len(JADangles):,0:len(JADangles)] = temp2[-1:0:-1,:]
    
    temp = np.dot(scatteringAmplitudeFixedMFixedKs,sphericalharmonics2minus[mindex])
    temp2 = np.dot(temp.transpose(),sphericalharmonics1plus[mindex])
    scatteringAmplitudeThetaFixedk[0:len(JADangles),len(JADangles):] = temp2[:,-1:0:-1]
    
    temp2 = np.dot(temp.transpose(),sphericalharmonics2plus[mindex])
    scatteringAmplitudeThetaFixedk[len(JADangles):,len(JADangles):] = temp2[-1:0:-1,-1:0:-1]
    
    return scatteringAmplitudeThetaFixedk
    
    
                                                     
    
def setTotalSpectrum(totalSpectrum,k1index,k2index,scatteringAmplitudeFixedMFixedKs):
    spectrumFixedMFixedKs = abs(scatteringAmplitudeFixedMFixedKs)**2
    totalSpectrum[k1index,k2index] += np.sum(spectrumFixedMFixedKs)
    totalSpectrum[k2index,k1index] = totalSpectrum[k1index,k2index]
    
def getThetaGrid(JADangles):
    thetagrid = np.zeros(len(JADangles)*2-1)
    thetagrid[0:len(JADangles)] = JADangles
    thetagrid[len(JADangles):] = JADangles[1:]+np.pi
    return thetagrid

    
def initializeSphericalHarmonics(lmax,JADangles,appearingM):
    sphericalharmonics1plus = [] # exp(+m) phi=0
    sphericalharmonics2plus = [] # exp(+m) phi=pi
    sphericalharmonics1minus = [] # exp(-m) phi=0
    sphericalharmonics2minus = [] # exp(-m) phi=pi

    for mindex in  range(len(appearingM)):
        m = appearingM[mindex]
        sphericalharmonics1plus.append(np.zeros([lmax-abs(m)+1,len(JADangles)],dtype=np.double))
        sphericalharmonics2plus.append(np.zeros([lmax-abs(m)+1,len(JADangles)],dtype=np.double))
        sphericalharmonics1minus.append(np.zeros([lmax-abs(m)+1,len(JADangles)],dtype=np.double))
        sphericalharmonics2minus.append(np.zeros([lmax-abs(m)+1,len(JADangles)],dtype=np.double))

        for i in range(len(JADangles)):
            for varl in range(lmax-abs(m)+1):
                sphericalharmonics1plus[-1][varl,i] = np.real(sph_harm(m,varl+abs(m),0.0,JADangles[i]))
                sphericalharmonics2plus[-1][varl,i] = np.real(sph_harm(m,varl+abs(m),np.pi,JADangles[i]))
                sphericalharmonics1minus[-1][varl,i] = np.real(sph_harm(-m,varl+abs(m),0.0,JADangles[i]))
                sphericalharmonics2minus[-1][varl,i] = np.real(sph_harm(-m,varl+abs(m),np.pi,JADangles[i]))

    return sphericalharmonics1plus , sphericalharmonics2plus , sphericalharmonics1minus , sphericalharmonics2minus
    
def transformThetaPicEnergiesToIndecies(Eofk,E1,E2):
    thetaPicIndecies = np.zeros([len(Eofk),len(Eofk)],dtype=np.int);
    E1temp = E1;
    E2temp = E2;
    for angleindex in range(len(E1)):
        i1 = getIndecies(Eofk,E1[angleindex]);
        i2 = getIndecies(Eofk,E2[angleindex]);
        thetaPicIndecies[i1,i2] = 1;
        E1temp[angleindex] = Eofk[i1];
        E2temp[angleindex] = Eofk[i2];
    E1 = E1temp;
    E2 = E2temp;
    return thetaPicIndecies, E1, E2
    
def getIndecies(Eofk,E):
    return np.argmin(abs(Eofk-E));

    
def loadBasicData(folder):
    appearingM = getMs(folder)
    filelist = os.listdir(folder)
    #print filelist
    computedMomenta = getComputedMomenta(filelist)
    DIspecFiles = getDISpecFiles(filelist,folder,appearingM)
    lmax = getLmax(DIspecFiles,appearingM)
    absMomenta = getMomentumGrid(computedMomenta,DIspecFiles)
    #saveMomenta(folder,absMomenta)    
    SIspecFiles = getSingleIonizationFiles(filelist,folder)
    return absMomenta,lmax,appearingM,DIspecFiles,SIspecFiles

    
def plotSIspectrum(absMomenta, SIspecFiles, lmax, folder, WRITE_SPECTRUM):
    SIdata = np.zeros([len(absMomenta),lmax+1], dtype=np.double)
    basis_name = SIspecFiles[0].split('testspec_')[0] + 'testspec_'
    for i in range(len(absMomenta)):
        try:
            temp = np.loadtxt(basis_name + str(i) + '.txt')
            SIdata[i,:] = temp[1:]
        except:
            SIdata[i, :] = SIdata[i - 1, :]
    #lmax+1 equals all the angular momentums
    totalSI = np.sum(SIdata, 1)
    Energies = 0.5*absMomenta**2
    plt.figure(3)
    if WRITE_SPECTRUM:
        out_file = open(folder + './totalSI.txt', 'w+')
        out_file.write('#Store data for total SI\n')
        out_file.write('#$E$ (au); SI yield\n')
        for i in range(len(totalSI)):
            out_file.write(str(Energies[i]) + ',' + str(totalSI[i]) + '\n')
        out_file.close()
    plt.plot(Energies, np.log10(totalSI))
        
def rangeDouble(start,stop,step):
    temp = np.zeros(np.floor((stop-start)/step)+1,dtype=np.double)
    for i in range(len(temp)):
        temp[i] = start + i*step
    return temp

  
    
def saveMomenta(folder,absMomenta):
    np.savetxt(getMomentaFileName(folder), absMomenta)

    
def saveTotalSpectrum(folder,totalSpectrum):
    np.savetxt(getTotalDISpectrumFilename(folder), totalSpectrum)
    
def getMomentaFileName(folder):
    return folder+'momenta.txt'
    
def getTotalDISpectrumFilename(folder):
    return folder+'total_DI_spectrum.txt'
    
def getJADSpectrumFilename(folder,n):
    return folder+'JAD_spectrum'+str(n)+'.txt'
    
def getJADthetaGridFilename(folder,n):
    return folder+'JAD_thetaGrid'+str(n)+'.txt'
    
def getJADenergiesFilename(folder,n,i):
    return folder+'JAD_energies'+str(n)+'_'+str(i)+'.txt'
    
def plotTotalSpectrum(absMomenta,totalSpectrum):
    print 'The total yield is : ' + str(sum(sum(totalSpectrum)))
    print 'the max moemtum is ' + str(max(absMomenta)) + ', the minimum is ' + str(min(absMomenta))
    Energies = 0.5*absMomenta**2
    numberOfColors = 50
    plt.figure(2)
    fig = plt.contourf(Energies,Energies,np.log10(totalSpectrum),numberOfColors)

    plt.rc('text', usetex=True)
    plt.xlabel(r"$E_1$ (au)")
    plt.ylabel(r"$E_2$ (au)")
    plt.axis('equal')
    plt.xlim(0,Energies[len(Energies)-1])
    plt.ylim(0,Energies[len(Energies)-1])
    plt.colorbar(fig)
    # plt.show(block=False)
    try:
        Lambda = float(sys.argv[2])
        Intensity = float(sys.argv[3]) * 10 ** 14
        from static_methods import LaserProperty
        Up = LaserProperty(Intensity, Lambda)['Up']
        omeg = LaserProperty(Intensity, Lambda)['Omeg']

        if 'spec_total' in os.listdir(sys.argv[1]):
            os.system('rm ' + sys.argv[1] + '/spec_total')
        totalEnergy = []
        for i in range(len(totalSpectrum)):
            data = 0
            for j in range(i):
                data += totalSpectrum[i - j][j]
            totalEnergy.append(data)
        plt.figure(5)
        plt.plot(Energies, np.log(totalEnergy))
        plt.xlabel("total Energy")
        plt.ylabel("yield")
        print "start to write the spectrum file"
        k = open(sys.argv[1] + '/spec_total', 'a')
        k.write('# tMax,time-resolution\n')
        k.write('# \n')
        k.write('#\n')
        # cut_index = 0
        # # select the line when E1 + E2 = constance
        # # for i in range(len(Energies)):
        # #     if i < len(Energies) - 1 and i > 0:
        # #         if Energies[i] < float(sys.argv[4]) and Energies[i + 1] > float(sys.argv[4]):
        # #             cut_index = i
        # #             continue
        # # m = []
        # # for n in range(len(list(totalSpectrum))):
        # #     if n < cut_index:
        # #         m.append(totalSpectrum[n][cut_index - n])
        # #     else:
        # #         m.append(1e-9)
        #
        # # k.write('#  sum[Phi,Eta]: (specRn) = (0)\n')
        # # for i in range(len(m)):
        # #     a = '%.10f,%.10f\n'%(absMomenta[i], m[i])
        # #     k.write(a)
        # # k.close()
        k.write('#  sum[Phi,Eta]: (specRn) = (0)\n')
        for i in range(len(absMomenta)):
            a = '%.10f,%.10f\n'%(absMomenta[i] * np.sqrt(2), totalSpectrum[i][i])
            k.write(a)
        k.close()
        # plt.figure(4)
        # # for n in range(len(list(totalSpectrum))):
        # #     if n < cut_index:
        # #         m.append(totalSpectrum[n][cut_index - n])
        # #     else:
        # #         m.append(1e-9)
        # m = [n[cut_index] for n in totalSpectrum]
        # plt.plot(list(np.array(Energies) + float(sys.argv[4])), np.log10(np.array(m)))
        # # for i in range(8, 16):
        # #     i_omega = -0.90314 - Up + i * omeg
        # #     plt.plot(list(np.ones(len(Energies)) * i_omega), np.log10(np.array(m)), 'r')
        # for j in range(15, 45):
        #     j_omega = -2.903 - 2 * Up + j * omeg
        #     plt.plot(list(np.ones(len(Energies)) * j_omega), np.log10(np.array(m)), 'g--')
        # plt.show()
        # plt.figure(5)
        # m1 = [sum(n) for n in totalSpectrum]
        # plt.plot(list(np.array(Energies)), np.log10(np.array(m1)))
        # for i in range(8, 16):
        #     i_omega = -0.90314 - Up + i * omeg
        #     plt.plot(list(np.ones(len(Energies)) * i_omega), np.log10(np.array(m1)), 'r')
        # plt.show()
    except:
        print "Fail for the user-defined parameter for total spectrum"
    
    


def getMs(folder):
	for m in range(-100,0,1):
		if os.path.isfile(folder + 'spectrum_test_' + str(m) + '_2_0.bin'):
			appearingM=range(m,-m+1,1)
			return appearingM
		
def getComputedMomenta(filelist):
	computedMomenta = []
	for i in filelist[0:]:
		namelength = 18
		if len(i)>namelength and i[0:namelength] == 'spectrum_test_0_2_':
			computedMomenta.append(int(i[namelength:re.search('.bin',i).start()]))
	return sorted(computedMomenta)
	
def getSingleIonizationFiles(filelist,folder):
	singleIonizationFiles = []
	mysorting = []
	for i in filelist[0:]:
		if len(i)>10 and i[0:9] == 'testspec_':
			singleIonizationFiles.append(folder+i)
			mysorting.append(int(i[9:re.search('.txt',i).start()]))
	sortingArray = [i[0] for i in sorted(enumerate(mysorting), key=lambda x:x[1])]
	return [singleIonizationFiles[i] for i in sortingArray]
		
def getDISpecFiles(filelist,folder,m):
    # return all files with 'spectrum_test_-1_2_146'
	specFiles = []
	for mym in m:
		namelength = 18
		if mym<0: namelength = 19
		specFilesM = []
		mysorting = []
		for i in filelist[0:]:
			if len(i)>namelength and i[0:namelength] == 'spectrum_test_' + str(mym) + '_2_':
				specFilesM.append(folder+i)
				mysorting.append(int(i[namelength:re.search('.bin',i).start()]))
		sortingArray = [i[0] for i in sorted(enumerate(mysorting), key=lambda x:x[1])]
		specFiles.append([specFilesM[i] for i in sortingArray])
	return specFiles
			
def getLmax(specFiles,m):
	myfile = specFiles[(len(m)-1)/2][0]
	with open(myfile, mode='rb') as file:
		fileContent = file.read(16)
		fileContent = file.read(4)
		return struct.unpack("i",fileContent)[0]

def getNumberOfMomenta(specFiles,m):
	myfile = specFiles[(len(m)-1)/2][0]
	with open(myfile, mode='rb') as file:
		fileContent = file.read(12)
		fileContent = file.read(4)
		return struct.unpack("i",fileContent)[0]		
  
def getMomentumGrid(computedMomenta,specFiles):
    absMomenta = []
    for i in specFiles[0][0:]:
        with open(i) as file:
            fileContent = file.read(8)
            try:
                absMomenta.append(struct.unpack("d",fileContent)[0])
            except:
                pass
    mymomenta = np.array(absMomenta)
    print "The grid of mymomenta is"
    print len(mymomenta)
    return mymomenta
		

def readInDIdata(lmaxLocal,specFilesFixedM1,specFilesFixedM2,numberOfMomenta,k1index,k2index):
    scatteringAmplitudeFixedMFixedKs = np.zeros([lmaxLocal+1,lmaxLocal+1], dtype=np.complex)
    try:
        with open(specFilesFixedM1[k1index]) as file:
            file.seek(20)
            for l in range(lmaxLocal+1):
                lengthToSkip = k2index*2*(lmaxLocal+1)*8 # 2 doubles of size 8 per complex number
                file.seek(lengthToSkip,1)
                fileContent = file.read(2*(lmaxLocal+1)*8)
                dataInDoubles = np.asarray(struct.unpack("d"*2*(lmaxLocal+1),fileContent))
                dataInComplexes = np.vectorize(np.complex)(dataInDoubles[0:len(dataInDoubles):2],dataInDoubles[1:len(dataInDoubles):2])
                scatteringAmplitudeFixedMFixedKs[l,:] += dataInComplexes
                lengthToSkip = (numberOfMomenta-1 - k2index)*2*(lmaxLocal+1)*8
                file.seek(lengthToSkip,1)
        with open(specFilesFixedM2[k2index]) as file:
            file.seek(20)
            for l in range(lmaxLocal+1):
                lengthToSkip = k1index*2*(lmaxLocal+1)*8
                file.seek(lengthToSkip,1)
                fileContent = file.read(2*(lmaxLocal+1)*8)
                dataInDoubles = np.asarray(struct.unpack("d"*2*(lmaxLocal+1),fileContent))
                dataInComplexes = np.vectorize(np.complex)(dataInDoubles[0:len(dataInDoubles):2],dataInDoubles[1:len(dataInDoubles):2])
                scatteringAmplitudeFixedMFixedKs[:,l] += dataInComplexes
                lengthToSkip = (numberOfMomenta-1 - k1index)*2*(lmaxLocal+1)*8
                file.seek(lengthToSkip,1)
    except:
        print str(k1index) + ' and ' + str(k2index) + ' has something wrong'
    return scatteringAmplitudeFixedMFixedKs

def main():   
    folder = sys.argv[1]
    if folder[-1]!='/': folder += '/';
    print 'computing DI spectra for ' + folder
    os.system('cp /home/jinzhen/Documents/research_data/experimentData/single_spectrum.txt ' + folder + '/.')
    print 'copying the experiment single electron spectrum data to the folder'
    computeSpectrum(folder)



if __name__ == '__main__':
    main()
