# tRecX - time-dependent Recursive indeXing
This code is developed by Jinzhen Zhu see [here](http://www.zhujinzhen.com/) who was a Ph.D student in Armin Scrinzi's Group, which was developed from on an old version of tRecX of from the group of Armin Scrinzi at the LMU Munich, see [here](http://www.theorie.physik.uni-muenchen.de/lsruhl/index.html).
This code mainly focuses on the double ionization of Helium atoms (by J.Zhu and A.Scrinzi) and 6D dissociative ionization of H_2^+ (by J.Zhu). The relavent papers can be found [here](https://arxiv.org/search/?query=Jinzhen+Zhu&searchtype=all&source=header)


The following contents are from the old tRecX version. 
Great thanks to Tobias Koelling, who did all the porting to Windows and Mac OS
and who initiated this documentation.

LICENSE:
By using the code you accept the conditions layed out in the file LICENSE see [there](http://homepages.physik.uni-muenchen.de/~armin.scrinzi/tRecX/home.html)

# Directories
 
    tRecX/SRCS        specific sources for the code
    tRecX/TOOLS       sources for wider use
    LIBRARIES         default libraries (replace by system libraries, where available)

# Compilation

1. Supply libraries, compile where needed (see details below)

2. Create your system specific Makefile, using the CMAKE utility

   prompt> cmake .

3. Compile and build by

   prompt> make

   For parallel build try 

   prompt> make -j 4 (on 4 threads)


### Trouble shooting


1. Make sure you have the correct version of Eigen 3.3.4 (or higher)

2. prompt> make clean

3. Start over


### Needed libraries

The following libraries are needed:

* Eigen (version 3.3.4 or up) 
* alglib
* ARPACK 
* LAPACK
* LAPACKE
* BLAS
* FFTW
* boost-system

All libraries used are public domain and can be downloaded from the web.

Except for alglib, all other libraries are available in standard Linux distributions.

For convenience, all sources except fftw are included with tRecX.

Warning: lapacke/lapack is broken in some distributions, compile manually.


### Required tools

* C compiler, e.g. gcc (reasonably new)
* C++ compiler, e.g. g++
* Fortran77 and Fortran90 compilers, e.g. gfortran
* make
* possibly git (for retrieval of the code from the git repository)

# Documentation

Overview of classes and code structure by Doxygen

## Build the Doxygen documentation

The documentation can be built using

    prompt> doxygen

This requires the following programs to be installed:

* ``doxygen`` (>= 1.8.3 works best) see [here](http://www.doxygen.org)
* ``dot`` for inheritance graphs see [here](http://www.graphviz.org/)

# Ports

The following ports may not be working any more (not maintained)

## Mac OS X

To get a basic build system, you need to install [XCode](https://developer.apple.com/xcode/) first.

If you need additional software, e.g. *gfortran* or some libraries, you can get them via [MacPorts](http://www.macports.org/).

### Basic MacPorts usage

You might want to so some of the following commands:

    sudo port selfupdate          #update the MacPorts system
    port search fortran           #search for something (e.g. fortran)
    sudo port install g95         #install the GNU fortran95 compiler
    sudo port upgrade outdated    #update outdated packages
    sudo port uninstall inactive  #remove old packages

Hereby ``sudo`` is needed to grant you administrator rights.

You can find more information in the official [guide](http://guide.macports.org/).

## Windows

### git

Install "Git for Windows" from [here](http://msysgit.github.io/). Maybe [Tortoise Git](https://code.google.com/p/tortoisegit/) is a helpful addition, it has a better context-menu integration for Windows Explorer.

### MinGW

It is possible to get all this running under Windows using [MinGW](http://www.mingw.org/wiki/Getting_Started). I did the following selection, however it may be possible to replace *Developer Toolkit* by *MSYS*:

* MinGW Compiler Suite
    * C Compiler
    * C++ Compiler
    * Fortran Compiler
* MinGW Developer Toolkit ?!

After the installation, ``wget`` has to be installed as well (on the command line, e.g. ``cmd``):

    mingw-get install msys-wget-bin

### MinGW-w64

There is a 64bit implementation of MinGW, which I expect to work as well. However, I did not do any tests. MinGW-w64 can be found [here](http://mingw-w64.sourceforge.net/).

### Compile

Do the preparations as mentioned above.

MinGW supplies two versions of make: ``mingw32-make`` in the MinGW package and ``make`` in MSYS. If you run from ``cmd``, use ``mingw32-make``, as ``make`` needs the MSYS-shell.



