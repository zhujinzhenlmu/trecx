\section{Stiffness control}

When using explicit ODE solvers for
\beq
i\ddt \vc= \mS\inv1 \mH(t) \vc
\eeq
 the admissible time step is inversely proportional
to the largest in magnitude eigenvalue of $\mS\inv1\mH(t)$. If the eigenproblem
at any time $t$
\[
\mH(t) \vg_E = \mS \vg_E E
\]
has very large eigenvalues $E$, the step size becomes very small at that time: we say the 
system of equations is ``stiff''. 

The traditional way out is the use of implicit solvers, that are immune to stiffness.
 However, implicit solvers are often expensive to implement and use, as they 
require the solution of a set of implict equations at every time step. Typically,
implict solving would be done iteratively, a process which itself suffers from stiffness
related problems and require intelligent and case specific ``preconditioning''.

\subsection{Sources of stiffness}

The main source of high eigenvalues are the kinetic energy terms in $\mH(t)$: the fundamental 
reason is that kinetic energies represented in the matrix grow as $\propto(\Delta x)\inv2$,
i.e.\ when we try to improve spatial discretization, we pay a price not only in system size, but
also by a slow-down of time-propagation. 

In the FE-DVR method there is a second, more technical source of high energy eigenvalues.
It is well known that solutions of the Schr\"odinger equations near $r=0$ behave as
$r^L$, $L$ being the angular momentum. Ideally, our radial basis set would be $L$-dependent 
correct behavior near $r=0$. 

For the presently used FE-DVR method to be efficient, the basis set should be {\em independent}
of $L$. As we need to represent the case $L=0$, this means in all $L$ there will be functions
that become constant as $r\to0$. Such functions have very large matrix elements $\propto L(L+1)$ 
resulting from the centrifugal term of the kinetic energy:
\beq
\int_0^{r_1} dr r^2 \frac{L(L+1)}{2r^2}\propto  L(L+1).
\eeq
As we increase $L$, time-propagation by an explict solver will slow down quadratically in $L$.

\subsection{Removing stiffness}

Having identified the sources of stiffness, we can easily remove it. For that we
observe that our operators typically have the shape
\beq
\mH(t) = \mH_0 + \mH_I(t) = \mT + \mV + \mH_I(t). 
\eeq
We now remove vectors with the largest eigenvalues $E>E_{\rm cut}$ from field-free $\mH_0$ or from kinetic energy $\mT$.

Let $\mP$ denote the projector onto the subspace of these highest eigenvectors for either $\mK=\mH_0$ or $\mK=\mT$.
We remove these from our solution space by looking only for solutions $\vc_Q(t) = (1-\mP)\vc(t) =: \mQ\vc(t)$:
\beq
i\ddt \vc_Q(t) = \mQ \mS\inv1 \mH(t) \vc_Q.
\eeq
Note that the projector commutes with $\mS$ and with $\mS\inv1$.
Usually, the high eigenvalues will not be too many: essentially there will be one or a few for each 
angular momentum, and also some high eigenvalues that may result from over-ambitious $\Delta x$.
Note also, that $E_{\rm cut}$ will need to be comparatively hight (discussion below). That means that
the projector $\mP$ will be low-dimensional and therefor cheap to apply. If $\mK$ has further symmetry
or if it is separable, e.g. 
\beq
\mK = \mK_1\otimes \one + \one \otimes \mK_2
\eeq
the projector is sparse and/or it has tensor product form, which all allow highly efficient 
implementation. tRecX can handle manifest symmetries and also exploit separability.


\subsubsection{Choice of $E_{\rm cut}$}
The spectral cutoff needs to be chosen much higher up than what one would na\"ively expect: in our typical 
system, $E_{\rm cut}\gtrsim 50 a.u.$, even if the actual ``physics'' happens at energies of a few $a.u.$
The reason is that in the dynamics short range modulations of the wave function can appear that require
for their representation high fourier components (high momenta). If we forbid these by a spectral cut, 
we compromise the solution.  In more tradtions quantum mechanics lingo, these are ``virtual states'' or
``transient populations'' of high energies.

\subsection{Special case}

There is some flexibility in the choice of $\mK$, other than $=\mH_0$ or $=\mT$.
One important case arised in ``mixed gauge'' for use mostly with haCC: here, our mixed-gauge
operator introduces a non-differentiability into the operator at a ``gauge radius'' $r=R_g$.
While this is not a problem for the FE-DVR basis, that admits non-differential behavior
at element boundaries, eigenfunctions of energy tend to impose smoothness at all $x$. 

Not very surprisingly, the (smooth) spectral projectors $\mP$ were found to compromise this 
type of interaction operator $\mD_M(t)$. There is a ``kink'' in the operator (and as a result
of the solution) that would require large (local) fourier components and these we are not
allowed to cut in that place.

A viable solution to that problem, it turns out, is to impose a zero-condtion at $r=R_g$ on $\mK$:
Mathematically, we constrain the domains to 
\beq
\mK = \mK_0 \oplus \mK_1,
\eeq
where terms are on $[0,R_g]$ and $[R_g,\infty)$ with Dirichlet boundary conditions on the 
respective intervals. Technically it amounts to zero-in the matrix row and column corresponditing
the $n$'th element boundary where $r_n=R_g$.

\subsubsection{Kinks in first deriviatives}
We want to admit kinks in the first derivatives as well. I.e. we only  want to remove spectral
eigenfunctions that are differntiably smooth at $R_g$. For that we project onto a basis that 
has values and derivatives = 0 at $R_g$.

Denote by $b_v$ the basis function $b_v(R_g)\neq0$ and, for definiteness, choose one $b_1$ as 
a function where $b_1'(R_g)\neq0$. We denote the new basis as
\beq
\vc = \vb^T \mW: c_0(R_g),c_1(R_g)\neq0,\quad c_0'(R_g),c_1'(R_g)\neq0, \text{ else } c_i(R_g)=c_i'(R_g)=0
\eeq
We denote $b_i'(R_g)=:d_i$ and find
\beq
\vc^T=\vb^T\bma
1&0&0&\ldots\\
-\frac{d_0}{d_1}&1&-\frac{d_2}{d_1}&\ldots\\
0&0&1&\ldots\\
&\vdots& & \ddots\\
\ema
=\left(
b_0-b_1\frac{d_0}{d_1},b_1,b_2-b_1\frac{d_2}{d_1},b_3-b_1\frac{d_3}{d_1},\ldots
\right)
\eeq
which we can write as
\beq
\mW = 1-\vd^T\otimes \ve_1
\eeq
Unfortunately, this $\mW$ is not unitary, which we would need for correct implmentation of a projector.
We can Schmidt-orthonormalize the basis, starting from the top: we obtain a set of orthonormal
functions $\va$, where only $a_0$ and $a_1$ are have non-zero values and/or derivatives at $R_g$.
Let $\mU$ denote the transformation
\beq
\va^T=\vb^T\mU,\qquad \l \va^T|\va^T\r = \one = \mU\adj\l\vb^T|\vb^T\r\mU = \mU\adj \mS \mU 
\eeq
then the projection onto functions that have zero values and derivatives at $R_g$ is
\beq
\vb^T\vbe=\va^T\val = \vb^T\mU \val
\eeq
or
\beq
\vbe\up0 = \mU \mQ \mU\adj \vbe = \one -  \mU \mP \mU\adj = (\one - \vu_0 \vu_0\adj- \vu_1 \vu_1\adj)\vbe
\eeq
where $\mP$ is the projector onto coefficients $\al_0$ and $\al_1$.
As $\mP$ has rank 2, this also means that we only need to use the first two rows in the transform.

\subsection{Spectral bases}
The problem, seemingly, is abesent when one works in an eigenbasis of $\mH_0$. However, to avoid artefacts
in the time-dependent problem, very large eigenenergies need to be included resulting in a 
large global basis set and full matrices. 
One looses the advantages of locality of the typical TDSE. 


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "Stiffness"
%%% End: 
