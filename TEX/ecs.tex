\section{Spectra of ECS operators}
The purpose of these notes it to discuss the ideal, mathematical spectrum of an ECS operator.
I will provide some mathematical discussion and contrast it with numerical findings, trying to 
point to the key difference between the mathematics and most numerical implementations
(including the one that I use here), beyond possible questions of convergence.

I should mention that this is a purely mathematical point about spectral properties. 
Computationally, we very easily get full machine precision control over all measurable quantities.
For here, I am only interested in the {\em continuous} spectrum, not in bound states and neither 
in resonant states. Therefore I just discuss the empty 2nd derivative operator.

\subsection{Two alternative definitions of the spectrum}
We first need to define, what we mean by spectrum. We can define the spectrum
in the sense of the theory of unbounded operators, where the spectrum are those complex numbers $z\in\si(H_\Rth)$,
where the resolvent does not exist as a bounded operator. To put it differently, for $c\not\in\si(H_\Rth)$
\beq\label{eq:operatorspectrum}
||(H_{R_0,\th}-c)\inv1||<\infty.
\eeq
This is the operator spectrum in the sense of functional analysis.
It makes mathematically precise the standard notion of an eigenvalue
\beq
(H_{R_0,\th}-z)\Psi_z=0
\eeq
and in particular circumvents the problem that $\Psi_z$ is not in the Hilbert space.
Alternatively, we may can discuss the spectrum in the sense that
\beq\label{eq:formspectrum}
\l \Psi_z|H-z|\Psi_z\r=0,
\eeq
where however we face the problem that this requires solutions where
$\Psi_z\not\in\H$. (I use $\H$ to denote the Hilbert space).
The latter form is what is often implied in numerical approximations, 
as we build our discrete approximations such that the matrix elements (\ref{eq:formspectrum})
exist, but $H\Psi_z\not\in\H$ is not defined as a function in the Hilbert space.

Note that all eigenfunctions of the Laplacian are outside the Hilbert space, as they just are plane waves. 
With ECS there is one further problem: $-\De_{\Rth}$ is not defined on functions that are 
continuously differentiable at $R_0$. Applying $-\De_\Rth$ to smooth functions invariably 
requires $\delta$-like singularities, which are not part of our Hilbert space. 
While increasing the basis size reduces the error due to the
infinite extension of the eigenfunctions, it does not help with the lack of continuity. 
If we use  (\ref{eq:formspectrum}) we can obtain a different continuous spectrum from 
(\ref{eq:operatorspectrum}).


\subsection{Operator spectrum of $-\De_{R_0,\th}$}

We consider an operator that has the form
\beq
-\De_{R_0,\th}=\left\{\bar 
-\ddx^2 &{\rm for }|x|<R0\\
-e^{-2i\th}\ddx^2&{\rm for }|x|>R0.
\ear\right.
\eeq
At the moment, we do not define what happens at $|x|=R_0$.

Whatever our eigenfunctions for a given eigenvalue $z=k^2e^{-2\al}$, they must fulfill the
differential equations in the inner and in the outer region. For $-\ddx^2$ these are
\beq
-\ddx^2 \exp(\pm i e^{-i\al} kx) = e^{-2i\al}k^2 \exp(\pm i e^{-i\al} kx).
\eeq
Therefore, for the inner domain $[-R_0,R_0]$ eigenfunctions of the operator $\ddx^2$ are
\beq
a_+\exp(+ i e^{-i\al} kx)+a_-\exp(- i e^{-i\al} kx).
\eeq
In the outer region, eigenfunction of $e^{-2i\th}\ddx^2$  to the {\em same eigenvalue} $e^{-2i\al}k^2 $
are
\beq\label{eq:outer}
b_+\exp(+ i e^{i(\th-\al)} kx)+b_-\exp(- i e^{i(\th-\al)} kx).
\eeq
Note that here the constants $b_\pm$ are arbitrary and also includes the form
\beq
\exp[- i kR_0 + e^{i(\th-\al)} k(x-R_0)]=\exp(- i kR_0- ie^{i(\th-\al)}kR_0)\exp(- i e^{i(\th-\al)} kx).
\eeq

For reasons of normalizibility, we must require that the outer region solution (\ref{eq:outer}) 
does not grow exponentially
which can be achieved by one of the conditions (assuming now that $0\leq \al,\th$):
\begin{itemize}
\item[(a)] $\al=\th$
\item[(b)] $\th>\al$ and $b_-=0$
\item[(c)] $\th<\al$ and $b_+=0$
\end{itemize}
Condition (a) leads to the same continuous spectral values that are found in global scaling.
To investigate (b) we need further input, namely about the functions on which $-\De_\Rth$ is defined. 
For defining $-\De_\Rth$ as an {\em operator}, we must avoid $\delta$-like singularities at 
the scaling radius $R_0$,
This can be achieved by subjecting values and derivatives at $R_0$ to the conditions
\bea
\phi(R_0+0)&=&e^{-i\th/2}\phi(R_0-0)\label{eq:discont1}\\
\phi'(R_0+0)&=&e^{i\th/2}\phi'(R_0-0). \label{eq:discont2}
\eea 
This is indeed the correct operator domain for  $-\De_\Rth$.
A plausibility discussion using similarity transformations 
is given (for the 3d rather than 1d case) in Scrinzi, Elander
J. Chem. Phys. {\bf 98},3866 (1993). This works fine and is near trivial for 
{\em real} exterior scaling, where the similarity transformations are unitary. 
For {\em complex} exterior scaling, 
similarity transformations themselves become unbounded operators and raise a whole host
of new domain questions. This is why the discussion above remains formal. However, 
a profound prove is given in 
J. M. Combes, P. Duclos, M. Klein, and R. Seiler, Commun. Math.
Phys. {\bf 110}, 215 (1987), with he bottom line for our purposes given in their Eq. (2.7).

For $b_-=0$ this implies
\bea\label{eq:conderiv}
 b_+\exp( i e^{i(\th-\al)} kR_0)                 &=&e^{-i\th/2}            [a_+\exp( i e^{-i\al} kR_0)+a_-\exp(- i e^{-i\al} kR_0)] \\
 i e^{i(\th-\al)}k b_+\exp( i e^{i(\th-\al)} kR_0)&=&e^{i\th/2} i e^{-i\al}k [a_+\exp( i e^{-i\al} kR_0)-a_-\exp(- i e^{-i\al} kR_0)]
\eea
Dividing the second equation by $ie^{i(\th-\al)}k$ and subtracting this leads to
\beq
0=2e^{-i\th/2}a_-\exp(- i e^{-i\al} kR_0).
\eeq
or 
\beq
a_-=0.
\eeq
This reduces the condition to
\beq
 b_+\exp( i e^{i(\th-\al)} kR_0)=e^{-i\th/2}a_+\exp(i e^{-i\al} kR_0). 
\eeq
We see that $|b_+|=|a_+|$ or
\beq
\exp( i e^{i(\th-\al)} kR_0)=e^{i\ga}\exp(- i e^{-i\al} kR_0)
\eeq
for some real $\ga$. Taking the logarithm we find
\beq
e^{i(\th-\al)}kR_0=\ga-e^{-i\al}kR_0
\eeq
or
\beq
e^{i(\th-\al)}+e^{-i\al}=\frac{\ga}{kR_0}.
\eeq
This has no solution for $\th,\al,k,\ga,R_0$ all real and $\th\neq0$.
The same reasoning works for case (c). We conclude that the spectrum is 
$\si(-\Delta_{R_0,\th})=e^{-2i\th}\RR_+$. 

Adding ``well behaved'' potentials does not change the continuous spectrum.
Well behaved could mean, ``relatively compact'' (i.e. $(-\De_\Rth-c)\inv1 V$ is a compact operator), which can at most add a
finite number of discrete values to the spectrum. Non-singular potentials that decay at infinity stronger than
$1/r$ will be relatively compact. Coulomb is ``relatively bounded'' ($(-\De_\Rth-c)\inv1 V$ is bounded), 
where infinitely many, but only discrete spectral values may appear (e.g. hydrogen atom). The new discrete eigenvalues, of course,
may be bound or resonant states. 

\subsection{Numerical situation}

In our numerical description we do consistently find deviation from the strictly rotated spectrum. 
One would expect that by improving the discretization one can get closer to the expected operator spectrum.
In our simulation we operate with 
matrix elements only (\ref{eq:formspectrum}): we do impose condition (\ref{eq:discont1}), but we do {\em not} impose (\ref{eq:discont2}).
However, this is not the origin of the deviation: spectra with condition (\ref{eq:discont2}) agree largely
with the ones shown here.

Typical spectra with increasingly good discretization are shown in Fig.~\ref{fig:spectra}.
There is large part of the Eigenvalues that fall right on the complex rotated continuum.
Then one observes a set of solutions that branch away from the rotated spectrum (near real spectral value 17
for the the scaling radius 5 used here).

Finally the branch splits into two. When we improve discretization at fixed $R_0$, the branch extends further, but does not move. 
We improve the discretization by using denser elements, the box size seems to be saturated with respect to these observations.
However, when we move $R_0$, the angle of the branch changes: for small $R_0$ it is lies closer to the rotated spectrum (Fig.~\ref{fig:spectra2}).
This shows that also the extra branch is due to discretization, although at this point it is not clear, which aspect of 
discretization exactly. Note that due to unitarity of real global scaling, the spectrum of any operator $H_\Rth$ must be independent of $R_0$:
\beq
H_{\al\Rth} = U_\al H_\Rth U_\al^*,
\eeq
with the real scaling unitary map
\beq
(U_\al\Psi)(x):=\al^{1/2} \Psi(\al x).
\eeq
This, by the way, almost proves that ECS and global scaling must have the same spectrum: we can take any ECS radius arbitrarily close
to 0 without changing the spectrum. Of course, it is not guaranteed that the limit of this procedure is actually global scaling.

\begin{figure}
\includegraphics[height=12cm,angle=-90]{spectra}
\caption{\label{fig:spectra}
Three spectra using high finite elements discretizations with matrix sizes ranging from 400 (green) to 1200 (blue). 
$R_0=5$, $\th=0.3$. The magenta line is $\tan(-0.6)E$. 
}
\end{figure}

\begin{figure}
\includegraphics[height=12cm,angle=-90]{spectra2}
\caption{\label{fig:spectra2}
Three spectra varying $R_0$ and the discretization. Red: $R_0=6$, green $R_0=3$ and magenta $R_0=3$ with finer discretization
for $x>R_0$. As $R_0$ can be varied by unitary real global scaling, the operator spectrum of $\si(-\De_\Rth)$ is independent of
$R_0$. This proves that the extra solutions off the rotated spectrum are effects of the finite discretization. 
}
\end{figure}

\section{Wave equation}
\subsection{In one dimension}
The original equation is
\beq
i\ddt \Psi = \bma 0 & i\ddx \\ i\ddx & 0 \ema \Psi
\eeq
This can be transformed to diagonal form
\beq
i\ddt \Psi = \bma - i\ddx & 0 \\ 0 & i\ddx \ema \Psi
\eeq
In the diagonal form, we can easily apply a PML that moves the complete spectrum into the lower half plane
\beq
i\ddt \Psi = \bma - i\ddx-\eta  & 0\\ 0 & i\ddx -\eta \ema \Psi ,
\eeq
with $\Im{\eta}>0$. This leads to stable equations for the time-propagation. 
It also derives from the unitary gauge transformation
\beq
U(\la,t)\bma\psi_0\\\psi_1\ema=\bma e^{-i\la\Theta_F(x)}& 0 \\ 0 & e^{i\la\Theta_F (x)}  \ema \bma\psi_0\\\psi_1\ema
\eeq
with
\beq
\Theta_F(x) = \int^x dx' \theta(x) \qquad \theta(x)\geq0\text{ and } \theta(x)=0\text{ for }x\in F. 
\eeq
The analytical continuation $\la\to\eta_0$ of the equation leads to 
\beq
\eta(x):=\eta_0 \theta(x).
\eeq
Note that the {\em continous spectrum} depends only on the asymptotic behavior of $\eta$. In particular, if
$\eta$ becomes constant asymptitically, the spectrum will be shifted by that constant value. 
As to the spatial dependence of the solutions, in the respective components only the left- or right-going 
waves are actually exponentially decaying. By a discretization, which is necessarily $\L^2$, we exclude the 
exponentially growing parts in the respective components. 
In terms of physics, this means that we accept only solutions what leave the area $F$ asymptotically.
Note that local, finite behavior is unaffected by this reasoning. Rather then providing proof of these
statements, we produce numerical evidence.

We can back-transform this to the non-diagonal form
\beq
i\ddt \Psi = \bma 0 & i\ddx-\eta \\  i\ddx+\eta &0  \ema \Psi
\eeq
(give or take a sign).
This should be tried in practice. It is interesting, because it can be generalized to higher dimensions.
This trivally has the same solutions as the untransformed equation above.

Preparing for the 2d case, one can play with this by using different scalings. For example, one can use a ``gauge transform''
in position and momentum, i.e. 
\beq
U_\la = \exp [\la ( x\ddx + \ddx x)],
\eeq
all understood as operators. Note that this is just the scaling operator! It will fail, as momenta will have the wrong sign.
Alternatively, one can use
\beq
U_\la = \exp [\la( x \ddx^2+ \ddx^2 x)],
\eeq
which will work but give poor performance in damping the long wave-length.

\subsubsection{1d wave equation with materials}
Let the equation be
\beq
i\ddt \Psi = \bma 0 & b(x)i\ddx \\ a(x)i\ddx & 0 \ema \Psi
\eeq
with $a(x)b(x)=c(x)$ the local speed of light. Note that here the conserved energy is
\beq
E=\int dx \psi_0^*(x)b\inv1(x)\psi_0^*(x) +  \psi_1^*(x)a\inv1(x)\psi_1^*(x).
\eeq
For obtaining evidently hermitian operators, it is convenient to change to
\beq
\Phi=\bma \phi_0 \\ \phi_1 \ema=\bma b\inv{1/2}\psi_0 \\ a\inv{1/2}\psi_1 \ema.
\eeq
where the scalar product is $\int dx |\phi_0(x)|^2 +  |\phi_1(x)|^2$ and the wave equation is
\beq
i\ddt \Phi = \bma 0 & b^{1/2}(x)i\ddx a^{1/2} \\ a^{1/2}(x)i\ddx b^{1/2} & 0 \ema \Phi
=\bma 0 & A^\dagger \\ A & 0\ema \Phi
\eeq
with $A:=a^{1/2}i\ddx b^{1/2}$. Note that in general $A^\dagger A \neq A A^\dagger$ for $a\neq b$.
Let us for simplicity assume that $A$ commutes with its adjoint, e.g. if $a=b$. Then 
the block-diagonal form is
\beq
i\ddt \bma \chi_+ \\ \chi_-\ema = \bma \sqrt{A^\dagger A} & 0 \\ 0 &-\sqrt{A^\dagger A} \ema \bma \chi_+\\ \chi_-\ema
\eeq
with $\chi_\pm  :=\phi_0\pm \phi_1$ (or so).


\subsection{In two dimensions}

First, let us note that any matrix of the wave-equation type can be diagonalized quite easily.
Let use the block-operator notation
\beq
i\ddt\bma w_0\\w_1\\w_2 \ema = \bma 0 & A^\dagger & B^\dagger \\ A & 0 & 0 \\ B & 0 & 0 \ema \bma w_0\\w_1\\w_2 \ema,
\eeq
where the $w_i$ can be understood in analogy to the $\phi_i$ above.
We assume again $a=b$ for our space-dependent parts, from which follows hermiticity of the
individual blocks $A^\dagger= A$ and $B^\dagger=B$. 
We define the generalized Laplacian $\Sigma=A^\dagger A + B^\dagger B$ and the commutator $C=AB-BA$.
Then one easily sees the eigenspaces spanned by 
\beq
\bma Z_0 \\ Z_1 \\ Z_2 \ema\frac{1}{\sqrt{2\Sigma}}, \bma \sqrt{\Sigma } \\ \pm A \\ \pm  B  \ema\frac{1}{\sqrt{2\Sigma}}.
\eeq
We denote the ``Laplacian'' as $\Sigma:=A^\dagger A +B^\dagger B$.
The eigenvalues of the second an third ``eigenvector'' are $\pm \sqrt{\Sigma}$. Note that $\Sigma$ non-negative (positive
on any finite representation).
The subspace spanned by the first ``eigenvector'' is a subspace with eigenvalues =0. 
These won't evolve in time and do not need to be computed explicitly. For the case $A^\dagger B=B^\dagger A$
it has the simple explicit form
\beq
\bma Z_0 \\ Z_1 \\ Z_2 \ema=\bma 0 \\ -B \\ A \ema.
\eeq

This can be inferred from 
the fact, that the matrix has rank $2N$ (assuming a block-size of $N\times N$) and that we have allready found 
$2N$ eigenvectors with non-zero eigenvalues.
Note that the ``eigenvectors'' with the block-matrices as entries are mutually orthogonal, e.g.
\beq
\bma \sqrt{\Sigma} \\ -A^\dagger \\ -B^\dagger \ema^\dagger\cdot 
\bma \sqrt{\Sigma } \\ A \\ B  \ema = \Sigma -A^\dagger A - B^\dagger B = 0.
\eeq
We have chosen them normalized in the sense
\beq
\frac{1}{\sqrt{2\Sigma}}\bma \sqrt{\Sigma } \\ A \\  B  \ema^\dagger\cdot 
\bma \sqrt{\Sigma } \\  A \\   B  \ema\frac{1}{\sqrt{2\Sigma}} = \one.
\eeq
In this basis, the block-diagonal matrix is
\beq
\mD = \bma 0 & 0 &0 \\ 0 & \sqrt{\Sigma } & 0 \\ 0 & 0 & -\sqrt{\Sigma }  \ema.
\eeq
Doing mathematics by analogy, we find for the 2d wave equation
\beq
D = \bma 0 & 0 &0 \\ 0 & \sqrt{\Delta} & 0 \\ 0 & 0 & -\sqrt{\Delta}  \ema.
\eeq
Because of the integral operator $\sqrt{\Delta}$, 
this is not useful for numerics. However, we can use it to design an absorption scheme.
Here, the components have spectra on the positive and negative half-axis, respectively.
This broadens our options in that we can, for example, complex scaling with opposite imaginary parts
on the two components, such that the spectrum rotates into the negative half-plane for both (ECS style).
Alternatively, one may shift (PML style) both components into the lower half plain.

As a first example, let us use PML style:
\beq
D_\eta = \bma 0 & 0 &0 \\ 0 & \sqrt{\Delta}-\eta & 0 \\ 0 & 0 & -\sqrt{\Delta}-\eta  \ema.
\eeq
Both components will decay in time, but one of the components may have exponentially exploding spatial 
dependence and will therfore be excluded by choosing a discretization.

Back-transformation to the numerically useful form without the square-root of the Laplacian is not 
obvious. The key point probably is that $\eta(\vr)$ does not commute with all differential operators. 
There will be extra terms arising from the commutations.

We can use our orthonormal ``eigenvectors'' to back-transform the absorbing equations to a computationally
useful form. We define the block-matrix with the eigenvectors as columns
\beq
\mE=
\bma Z_0 &  \sqrt{\Sigma } &  \sqrt{\Sigma }\\ Z_1 & A &  -A \\   Z_2 &  B & -B  \ema(2\Sigma)\inv{1/2}.
\eeq
After back-transformation we have
\beq
\mD_\eta= \bma 0 & A^\dagger & B^\dagger \\ A & 0 & 0 \\ B & 0 & 0 \ema - \mE \bma 0&0&0\\0&\eta&0\\0&0&\eta\ema\mE^\dagger
\eeq
with the explict form of the absorbing part 
\beq
\mC_\eta=\mE\eta \mE^\dagger = 
\bma 
1 & 0& 0\\
0 &A \Sigma\inv{1/2}\eta \Sigma\inv{1/2} A^\dagger&A \Sigma\inv{1/2}\eta \Sigma\inv{1/2} B^\dagger \\
 0& B \Sigma\inv{1/2}\eta \Sigma\inv{1/2} A^\dagger & B \Sigma\inv{1/2}\eta \Sigma\inv{1/2} B^\dagger \ema .
\eeq
As we have chosen not to modify the equation for the zero-eigenvectors, their components do not appear in 
the absorber.

If $\Sigma$ commutes with $\eta$, e.g. if $\eta=\eta(|\vr|)$, we can contract the square roots into
\beq
\mC_\eta=
\bma 
1 & 0& 0\\
0 &A \Sigma\inv1\eta  A^\dagger&A \Sigma\inv1\eta B^\dagger \\
 0& B \Sigma\inv1\eta A^\dagger & B \Sigma\inv1\eta B^\dagger 
\ema.
\eeq
If also all block-matrices commute, we can further simplify
\beq
\mC_\eta=
\Sigma\inv1\bma 
\Sigma & 0& 0\\
0 &A \eta  A^\dagger&A \eta B^\dagger \\
 0& B \eta A^\dagger & B \eta B^\dagger 
\ema.
\eeq
Let us assume that $A$ and $B$ are the discrete representatives of $\partial_x$ and $\partial_y$.
Then our discretized version of the absorbing equations is
\beq
i\ddt \Psi = \mD-\mC_\eta\Psi.
\eeq

The $\Sigma$ and $A,B$ will not commute except for (possibly) rotationally symmetric media.
Also, using $\eta$ that commutes with $\Sigma$ will often create numerical difficulties. 
Applying the inverse of the Laplacian is a feasible, but not a desirable operation. We can avoid it, 
if we perform the absorption by subracting $\Sigma^{1/2}\eta\Sigma^{1/2}$ instead of only $\eta$. 
This amounts to absorption that depends on radial momentum with the alternative damping factor
\beq
\mF_\eta=
\bma 
1 & 0& 0\\
0 &A \eta  A^\dagger&A \eta B^\dagger \\
 0& B \eta A^\dagger & B \eta B^\dagger 
\ema.
\eeq

One can also use the fully symmetrized operator that has the same qualitative behavior as $\mC_\eta$
with the spectral modfication $\Sigma^{1/2}\eta^{1/2}\Sigma\inv1\eta^{1/2}\sigma^{1/2}$, leading to
\beq
\mC'=
\bma 
\Sigma & 0& 0\\
0 &A  \eta^{1/2}\Sigma\inv1\eta^{1/2}  A^\dagger&A \eta^{1/2}\Sigma\inv1\eta^{1/2} B^\dagger \\
 0& B \eta^{1/2}\Sigma\inv1\eta^{1/2}   A^\dagger & B \eta^{1/2}\Sigma\inv1\eta^{1/2}  B^\dagger 
\ema.
\eeq
This form has the advantage of being manifestly local and not requiring spherical symmetry
of $\eta$. We {\em do} need the inverse of the Laplacian, though.

Complex scaling takes an intermedate role: in order to damp both components, we need to 
real scale with opposite signs $e^{-\la}$ and $e^{\la}$ on the respective components. The damping term then
is analogous to a term where we replace $\eta\to\eta\sqrt{\Sigma}$: we see that this is not practical here,
as the $\sqrt{\Delta}$ remains in the final damping expression. If we take scaling in the strict sense, 
the scalings would also figure in the transformation vectors. It might be that this removes the unpleasant terms 
(not likely).

Other options are to ``engineer'' damping by including terms like $(AB)\inv1 \Sigma$ (properly symmetrized),
the inverses $A\inv1$ and $B\inv1$ are easy to apply. Such terms should be very efficient in damping low 
frequency components. Higher frequencies could be taken care of by terms $A\inv1\Sigma$, $B\inv1\Sigma$ or just
$\Sigma$.  

\subsubsection{Perfect absorption}
We need to investigate, whether the proposed manipulations will leave the equations unaffected in the 
inner region $F$. A safe heuristics for this is to derive the absorption as the analytic contuation of
a unitary transformation $U_\la$ that leaves the functions on $F$ unchanged. Functions of the Laplacian
other than polynomials are not very suitable objects for constructing such transformations, as they are 
in general non-local. A local manipulation in the basis the contains the square-root of the Laplacian 
does not necessarily translate into a local manipulation of the original system of equations. 

This symmetrized absorption is manifestly local. 

As to the construction of the unitary transformation, let us first observe that
\beq
[B,\exp(A)]=[B,A]\exp(A)\text{  for   }[[B,A],A]=0,
\eeq
as, for example for the pair $A,B=\ddx,x$. Let us assume that there exists a self-adjoint operator
\beq
K: [\sqrt{\Delta},K]= iY,
\eeq
where $Y$ is the operator part of any absorber method, e.g. $\eta(\vr)=\eta_0 \theta_F(\vr)$.
The exponential of the self-adjoint operator is unitary
\beq
U_\la =\exp(i \la K).
\eeq
Also, it ``generates'' our absorption method in the form
\beq
\mU_\la = \bma \exp(-i \la K) & 0 \\ 0 &  \exp(i \la K) \ema.
\eeq
It is sufficient to prove the existence of $K$, its exact shape is irrelevant.

Side remark: I conjecture that proving the {\em non-existence} would also prove the absence of
absorbing layers consisting in the addition of a term to the block-diagonal form 
of the quations. The argument could go roughly as follows: 
assuming that indeed the dependence of the solution on $\eta_0$ is analytic, we can 
continue to the real axis. This would define a family of unitarily related operators
modified wave operators. $K$ should be the generator of this unitary transformation.
Clearly, this requires, for example that $\sqrt(\Sigma)+\eta$ be self-adjoint.

From that we know that also the operator expressed in the old coordinates is generated from 
a unitary transform and, as we can ensure it is local, it leaves the inner region unaffected.

\section{Maxwell's equation}
By the substitution $\vE,\vH\to \vE\pm i\vH$ one readly brings the equations to block-diagonal form
with the operators $\pm \vna \times$ on the diagonal. 

\subsection{Block-diagonal form $\vna \times$}

The rotation operator has the form
\beq
R=\bma 0 & -Z & Y \\ Z & 0 & -X \\ -Y & X & 0 \ema.
\eeq
We recognize the structure of the generator of a rotation around the axis $(X,Y,Z)$ with the
``angle'' of rotation $\sqrt{X^2+Y^2+Z^2}$. The rotation axis also provides an invariant eigenvector.
Together with two more eigenvector we obtain the three mutually orthognal eigenvectors
\beq
(\va,\vb,\vc)=\bma X &0& Y^2+Z^2 \\ Y &XZ& -XY \\ Z &-XY& -XZ \ema
\eeq 
One also sees
\beq
R\vb = \bma 0 & -Z & Y \\ Z & 0 & -X \\ -Y & X & 0 \ema \bma 0 \\ XZ \\ -XY \ema =
\bma -X(Z^2+Y^2) \\  -X(-XY) \\ -X(-XZ) \ema = -X \vc.
\eeq
and similarly
\beq
R\vc = \bma 0 & -Z & Y \\ Z & 0 & -X \\ -Y & X & 0 \ema \bma Y^2+Z^2  \\ -XY \\ -XZ \ema =
\bma 0 \\ Z(X^2+Y^2+Z^2) \\ -Y(X^2+Y^2+Z^2) \ema = \frac{X^2+Y^2+Z^2}{X} \vb.
\eeq
Let us denote the norms as $n_b,n_c$ and $b=\vb/n_b,c=\vc/n_c$.
We then have
\beq
R b = -X cn_c/n_b , \quad R c =  \frac{X^2+Y^2+Z^2}{X}b n_b/n_c.
\eeq
In matrix form this is 
\beq
\bma 0 & U \\ V & 0 \ema
\eeq
with
\beq
U=\frac{\Delta}{X} \frac{n_b}{n_c},\qquad V = -X \frac{n_c}{n_b}.
\eeq
with $\Delta=X^2+Y^2+Z^2$.
Eigenvalues are
\beq
\pm \sqrt{UV}=\sqrt{\Delta}
\eeq
Normalized eigenvectors are
\beq
\frac{1}{\sqrt{2\Delta}}\bma \sqrt{V} \\\sqrt{U} \ema \qquad \frac{1}{\sqrt{2\Delta}}\bma \sqrt{U} \\-\sqrt{V} \ema
\eeq
Note that $2\Delta = U^2+V^2$:
\beq
\left(\frac{n_b}{n_c}\right)^2=\frac{X^2(Y^2+Z^2)}{(X^2+Y^2+Z^2)(Y^2+Z^2)}=\frac{X^2}{\Delta}
\eeq
\beq
U^2+V^2=X^2\frac{\Delta}{X^2}+\frac{\Delta^2}{X^2}\frac{X^2}{\Delta}=2\Delta.
\eeq
Finally, the first eigenvector expressed in the orignal basis $X,Y,Z$ is
\beq
b\sqrt{\frac{V}{2\Delta}}+c\sqrt{\frac{U}{2\Delta}}
\eeq
The first term is (up to the factor $\sqrt{2\Delta}\inv1$):
\beq
 \bma 0 \\ XZ \\ -XY \ema \frac{1}{n_b} \sqrt{-X \frac{n_c}{n_b}}=
 \bma 0 \\ XZ \\ -XY \ema \frac{1}{X\sqrt{Y^2+Z^2}} \sqrt{-X \frac{\sqrt{\Delta}}{X}}=
 \bma 0 \\ Z \\ -Y \ema \frac{\sqrt{-X\sqrt{\Delta}}}{\sqrt{Y^2+Z^2}}
\eeq
and the second term
\beq
 \bma Y^2+Z^2  \\ -XY \\ -XZ \ema\frac{1}{n_c}\sqrt{\frac{\Delta}{X}\frac{n_b}{n_c}}=
 \bma Y^2+Z^2  \\ -XY \\ -XZ \ema\frac{1}{\sqrt{Y^2+Z^2}\sqrt{\Delta}}\sqrt{\frac{\Delta}{X}\frac{X}{\sqrt{\Delta}}}=
 \bma Y^2+Z^2  \\ -XY \\ -XZ \ema\frac{1}{\sqrt{Y^2+Z^2}}\sqrt{\frac{1}{\sqrt{\Delta}}}
\eeq
and their sum up to a common denominator $\sqrt{Y^2+Z^2}\Delta^{1/4}$
\beq
 \bma 0 \\ Z \\ -Y \ema \sqrt{-X\Delta}+ 
\bma Y^2+Z^2  \\ -XY \\ -XZ \ema
\eeq

\section{2d Vectorial Wave equation in Polar Coordinates}

In Carthesian coordinates
\beq
\int dx \int dy 
\bma \chi_x \\ \chi_y \\ \chi_0 \ema \cdot 
\bma
0&0&\ddx\\
0&0&\ddy\\
-\ddx&-\ddy&0
\ema
\bma \psi_x \\ \psi_y \\ \psi_0 \ema 
\eeq
For finite element basis, need to use explicitly symmetrized form of any (hermitian) operator
\beq
\l \mD \chi | \psi \r + \l \chi | \mD \psi\r 
\eeq
We re-write the operator in the form
\beq
 \l \chi | \mD \psi\r =\int dr^2 \bma \vchi \\ \chi_0 \ema \cdot \bma 0&\vna\\ -\vna & 0 \ema \bma \vpsi \\ \psi_0 \ema 
\eeq
For cylinder coordinates and omitting $z$, we use (Wikipedia)
\beq
\vna  = \hat{\rho}\partial_\rho  + \frac{1}{\rho}\hat{\phi}\partial_\phi 
\eeq
from which we arrive at
\beq
\int dr^2 \chi^*_\rho \partial_\rho \psi_0 + \frac{1}{\rho}\chi^*_\phi \partial_\phi \psi_0 
         -\chi^*_0 \partial_\rho \psi_\rho - \frac{1}{\rho}\chi^*_0 \partial_\phi \psi_\phi 
\eeq
The hermitian operator $\mD$ in planar polar coordinates is therefor more explicitly
\beq
\mD = \bma 0 & 0 & \partial_\rho\\  0 & 0 & \frac{1}{\rho}\partial_\phi\\-\partial_\rho  & -\frac{1}{\rho}\partial_\phi&0\ema=\mD^\dagger 
\eeq
The explicitly symmetrized form of the matrix elements 
\beq\frac12
\int dr^2  
[\chi^*_\rho \partial_\rho \psi_0- (\partial_\rho \chi^*_\rho) \psi_0]
+ [\frac{1}{\rho}\chi^*_\phi \partial_\phi \psi_0 -\frac{1}{\rho}(\partial_\phi\chi^*_\phi) \psi_0] 
-[\chi^*_0 \partial_\rho \psi_\rho - (\partial_\rho\chi^*_0) \psi_\rho] 
-[\frac{1}{\rho}\chi^*_0 \partial_\phi \psi_\phi - \frac{1}{\rho}(\partial_\phi\chi^*_0) \psi_\phi] 
\eeq
This is consistent with the present definition of the operater strings
where
\beq
\verb"<Jd>":=\frac12[\l f_i | \partial_q f_j\r - \l \partial_q f_i | f_j\r]
\eeq
The full definition for $\mD$ in planar polar coordinates defined in the sequence  \verb"vec,rho,phi"
and with interpretation of the vector components as $\psi_0,\psi_\rho,\psi_\phi$ should be
\begin{verbatim}
  '-<0,1><Jd><J> -<0,2><Jr><Jd> +<1,0><Jd><J> +<2,0><Jr><Jd>'
\end{verbatim}


\section{Maxwell's Equation in Cylindrical Coordinats}
\beq
\ddt \bma \vE \\ \vH \ema = \bma 0 & \vna \times \\ -\vna \times & 0 \ema  \bma \vE \\ \vH \ema
\eeq
Both, the vector components and the derivatives will be represented in cylindrical coordinates.
According to Wikipedia (I even could cut/paste this from the html!)
\beq
\vna\times \vA =
\hat{\rho}\left( \frac{1}{\rho} \frac{\partial A_z}{\partial \phi} - \frac{\partial A_\phi}{\partial z} \right)
 +\hat{\phi}   \left( \frac{\partial A_\rho}{\partial z} - \frac{\partial A_z}{\partial \rho} \right) 
+\hat{z}  \frac{1}{\rho} \left( \frac{\partial \left(\rho A_\phi\right)}{\partial \rho} - \frac{\partial A_\rho}{\partial \phi} \right) 
\eeq
We assume for simplicitly the $z$-translational symmetric case, i.e. all $\ddz$ give zero, and likewise $H_\phi=H_\rho=E_z=0$.
Our energy is then with $\vF=(H_z,E_\phi,E_\rho)$:
\bea
\l \vF | \mD \vF\r&=\int d\phi d\rho \,\rho&
E_\rho\left( \frac{1}{\rho} \partial_\phi H_z \right)
 + E_\phi   \left( - \partial_\rho H_z \right) 
+H_z  \frac{1}{\rho} \left(\partial_\rho \left(\rho E_\phi\right) - \partial_\phi E_\rho \right)= \\
&\int d\phi d\rho&
 E_\rho\left( \partial_\phi H_z \right) - \rho E_\phi   \left( \partial_\rho H_z \right) 
+H_z \partial_\rho \left(\rho E_\phi\right) - H_z\partial_\phi E_\rho = 
\\ &=\frac12\int d\phi d\rho&
\quad\left[E_\rho\left(\partial_\phi H_z \right)-\left(\partial_\phi E_\rho \right)H_z\right]
\\ &&
-\left[\rho E_\phi \left(\partial_\rho H_z \right)- \left(\partial_\rho \rho E_\phi \right)  H_z\right]
 \\ &&           
 + \left[H_z \partial_\rho \left(\rho E_\phi\right)-\rho\left(\partial_\rho H_z\right)E_\phi\right]
 \\ &&
 - \left[ H_z\partial_\phi E_\rho - \partial_\phi H_z E_\rho \right]
\\ &=\frac12\int d\phi d\rho\,\rho&
\quad\frac{1}{\rho}\left[E_\rho\left(\partial_\phi H_z \right)-\left(\partial_\phi E_\rho \right)H_z\right]
\\ &&
-\left[ E_\phi \left(\partial_\rho H_z \right)- \left(\partial_\rho E_\phi \right)  H_z\right]+\frac{1}{\rho}E_\phi H_z
 \\ &&
 + \left[H_z \left( \partial_\rho E_\phi\right)-\left(\partial_\rho H_z\right)E_\phi\right]+\frac{1}{\rho} H_z E_\phi
 \\ &&
 - \frac{1}{\rho}\left[ H_z(\partial_\phi E_\rho) - (\partial_\phi H_z) E_\rho \right]
\eea
From this we can read off the properly symmetrized Maxwell derivative in cylinder coordinates
\beq
\mD=\bma 
0&\partial_\rho+\frac{1}{2\rho}&-\frac{1}{\rho}\partial_\phi
\\-\partial_\rho+\frac{1}{2\rho}&0&0
\\\frac{1}{\rho}\partial_\phi&0&0
\ema
\eeq
Here all derivatives are understood in their symmetrized form $\partial_q:=\frac12[\partial\up{r}_q-\partial\up{l}_q]$ and
integration is {\em after} formation of the inner product \wrt  componens of $\vF$:
\beq
\frac12\int d\phi d\rho\,\rho\left[\vF\cdot\mD\vF'\right]
\eeq
The tRecX operator string for this with a coordinate hierarchy \verb"vec,rho,phi" and \verb"vec" sorted as 
above:
\begin{verbatim}
         +<0,1><Jr+Jd><J> -<0,2><Jr><Jd> +<1,0><Jr-Jd><J> +<2,0><Jr><Jd>
\end{verbatim}
as the symbols \verb"<Jd>" indicate
\beq
\verb"<Jd>":=\frac12\int dq \, [f_i(q)  J(q) f'_j(q)- f'_i(q)  J(q) f_j(q)]
\eeq

\subsection{Absorption of the Jacobian into the functions}
Redefine the components as 
\beq
(H_z,E_\phi,E_\rho)\to (\tH_z,\tE_\phi,\tE_\rho):= (\rho^{1/2}H_z,\rho^{1/2}E_\phi,\rho^{1/2}E_\rho)
\eeq
Re-write
\bea
\lefteqn{\int d\phi d\rho\,
 E_\rho\left( \partial_\phi H_z \right) - \rho E_\phi   \left( \partial_\rho H_z \right) 
+H_z \partial_\rho \left(\rho E_\phi\right) - H_z\partial_\phi E_\rho}
\\ &=\int d\phi d\rho&
 \rho\inv1\tE_\rho( \partial_\phi \tH_z) - \rho^{1/2} \tE_\phi( \partial_\rho \rho^{-1/2}\tH_z) 
+\rho\inv{1/2}\tH_z \partial_\rho(\rho^{1/2} E_\phi) - \rho\inv1\tH_z\partial_\phi \tE_\rho 
\\ &=\int d\phi d\rho&
 \frac{1}{\rho}\tE_\rho( \partial_\phi \tH_z) - \tE_\phi( \partial_\rho\tH_z)  +\frac{1}{2 \rho} \tE_\phi(\tH_z) 
+\tH_z(\partial_\rho \tE_\phi)+\frac{1}{2\rho}\tH_z \tE_\phi - \frac{1}{\rho}\tH_z\partial_\phi \tE_\rho 
\eea

\subsection{PML - rotationally symmetric case}
Let us assume all $\partial_\phi$ give 0, we then have the $E_\rho$-componentn decoupled and are left with 
\beq
\mD=\bma 
0&\partial_\rho+\frac{1}{2\rho}
\\-\partial_\rho+\frac{1}{2\rho}&0
\ema
\eeq
This can be written as
\beq
\mD=\bma 0 &\mB\\\mB^\dagger & 0\ema
\eeq
Unfortunately, the $\mB$ are very ugly operators: they are not ``normal'', i.e. $[\mB,\mB^\dagger]\neq0$.
As a consequence, $\mB$ cannot be diagonalized in a symmetric form, as in diagonal representation they would commute.
This prevents the majority of our formal manipulations from being valid. If I remember right, even the formal 
block-diagonalization assumed normality at one point (I thought it was an innocent assumption).

Alternatively we can write
\beq
-i\si_2\partial_\rho +\si_1\frac{1}{2\rho}
\eeq
and, changing indices of the Pauli matrices, bring it to the equivalent form
\beq
\mD=\bma 
-i\partial_\rho&\frac{1}{2\rho}
\\\frac{1}{2\rho}&i\partial_\rho
\ema
\eeq
Now we can apply our spectral shift. This may be working, it definitly should be reflection-free in 
the unitary case. However, the second important property of the PML may be in danger: that the outgoing 
spectral eigenfunctions of the complete operator are exponentially damped. Clearly, due to the coupling by $1/\rho$, 
the eigenfunctions of the complete operator will always mix in- and out-going waves of the system without $1/\rho$.
Now, with complex $\la$, the ingoing uncoupled eigenfunctions grow exponentially in space.

Also, it is no obvious that the {\em wave packet} composed of complete outgoing waves necessarily is exponentially 
growing. This would somehow contradict a finite speed of light. Outside the light cone, the solution should be
zero, and, if analyticity arguments are any good, also be zero in the complex scaled case. I.e. the
damping by time-dependence has a chance to win over the a possible growth due to progression in space.

\subsection{PML - 2d cartesian case}
We apply the same principle as in the rotational symmetric case to define the PML for 
the 2d cartesian case with $\vF=(H_z,E_x,E_y)$
\beq
\mD = 
\bma
0 & \ddy & -\ddx \\
-\ddy & 0 & 0\\
\ddx & 0 & 0
\ema
\eeq
should be equivalent to 
\beq
\mD=\bma
-i\ddx+i\ddy & 0 & 0 \\
0 & i\ddx 0 \\
0 & 0 & -i\ddy
\ema 
\eeq
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "tsurff"
%%% End: 
