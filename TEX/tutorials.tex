\printindex
\section{General remarks}

\subsection{Structure of the tutorials}
For each tutorial a few comments and selected examples are given. 
Typically, some features are new compared to what has been discussed previously and 
 these  are discussed in short paragraphs. An index of 
all such discussions is provided for quick reference.

\subsection{Discretization and accuracies}
Discretizations and parameters in all tutorials are ``reasonable'', but convergence to
high accuracy is not guaranteed. Whether results of any such calculation are accurate to 
a desired level needs to be established by systematic convergence studies, as usual 
in computational physics.

Neither are the parameters optimized for some upper bound of accuracies: for production
in a well-defined range of parameters, it will almost certainly pay off to determine the smallest bases required
for a certain purpose, gains in compute times can be very large.

\subsection{Useful python scripts}
\index{Scripts}

\subsubsection*{Comparing results}
\index{Scripts!compare.py}
For comparing relative deviations in spectra and other observables, there is a convenient python script
\verb"compare.py" in the distribution subdirectory \verb"SCRIPTS" that can be used as follows: 
\begin{verbatim}
>SCRIPTS/compare.py  spec tutorial/12IRlongPulse 0008 0009 0010 0011
\end{verbatim}
where the \verb"spec"-files obtained in runs 0009-0011 of subdirectory \verb"tutorial/12IRlongPulse" are compared to 0008.
The spectrum and relative deviations will be displayed.
Some help is displayed when you run the script w/o arguments.

\subsubsection*{List parameters of several runs}
\index{Scripts!lRuns.py}
Another useful python script is \verb"lRuns.py", which will selected parameters as they apear in the 
actual input files of a series of calculations. The command
\begin{verbatim}
SCRIPTS/lRuns.py tutorial/12IRlongPulse 4-7,11-
\end{verbatim}
will display the parameters of the specified range of runs (or all runs, if no ranges are given) int he following form:
{\small
\begin{verbatim}
--- data selected according to /home/scrinzi/projects/tRecX/tutorial/12IRlongPulse/linp-extract ---
 Run:    Master  Running  #Proc  Time    NormTotal <Expectation0>       #L      #Rn     #Abs
*0004:  sulamith 0.5456       1 2758    0.79659716 0.796597168194       20      60      20
*0006:  sulamith 0.5461       1 2758    0.79659716 0.796597168194       20      60      20
*0007:  sulamith 0.5596       1 2758    0.79659716 0.796597168194       20      60      20
*0011:  sulamith 338.2        1 3309.6  0.47104979 0.471049796705       20      48      20
*0012:  sulamith 373.2        1 3309.6  0.47105111 0.471051118114       20      40      15
*0013:  sulamith 549.8        1 3309.6  0.47065925 0.470659256066       30      40      15
*0014:  sulamith 425.3        1 3309.6  0.47105137 0.471051373348       20      40      20
 0015:  sulamith 893.2        1 1199.64 0.47065799 0.470657992877       40      60      20
\end{verbatim}
}
The selection of input parameters that will be displayed and headers for the columns are defined in a
file \verb"linp-extract" (``list of inputs - extract'') that must resided in the respective directory, here \verb"tutorial/12IRlongPulse",
with the format
\begin{verbatim}
Axis:order[2]=20=#L
Axis:nCoefficients[3]=80 = #Rn
Axis:nCoefficients[4]=80 = #Abs
#Absorption:theta[1]=0.3 = thet
#Axis:order[3]=20=OrdRn
Operator:interaction[1]=<<MixedGaugeDipole:Rg=20>>=rGauge[-7:-2]
\end{verbatim}
The \verb"#" act as comment tokens, disabling the lines, the string after the second \verb"=" is used as a header.
The range in \verb"rGauge[-7:-2]" will show the 7th last through the 3rd last charcters of the input string (useful
for long strings, where only sections are of interest).
Every run directory, say \verb"tutorial/12IRlongPulse/0007" contains a file \verb"linp" with all 
actual inputs in the above format. This can be copied into \verb"tutorial/12IRlongPulse/linp-extract" and edited
to only display the relevant information.

\subsection{Pulses in dipole-approximation}
\index{Pulses}
Pulses are defined through their vector potentials $\vA(t)$ in the form
\beq
\vA(t) = \sum_p\vep_p\frac{E_p(t)}{\om_k}\sin[\om (t-\tau_p)+\varphi_p\up{ceo}]
\eeq
A range of pulse envelopes are defined, such as $\cos^2$,  $\cos^8$, Gaussian, flat-top.
For pulse componente $p$ duration is specified through its FWHM of the intensitity envelope, i.e.
of $[E_p(t)]^2$. The peak intensity for each component is defined as $\max_t |E_p(t)|^2/2$.
The field is then defined as
\beq
\vEf(t) = \ddt \vA(t),
\eeq
note the sign convention.

\section{Tutorials 00-09}
The basic usage \verb"tRecX" and \verb"Spectrum" is described by comments in the \verb"tutorial/0.....inp" files,
where also small exercises are suggested.

\section{10IRSpectrum - H-atom with 2-cycle 800nm pulse}

Computes the photo-electron spectra for the Hamiltonian
\beq\label{eq:hamiltionianH}
H(\vr,t) = -\frac12\Delta - \frac{c(r)}{r} + iA_z(t)\ddz
\eeq
specified as
\begin{verbatim}
Operator: hamiltonian='1/2<<Laplacian>>-<1><1><trunc[15,20]/Q>'
Operator: interaction='iLaserA0[t]<<D/DZ>>'
\end{verbatim}


\subsubsection*{Potential cutoff}
\index{Potential cutoff}
The cutoff function 
\beq\label{eq:cutoff}
c(r)=\left\{\bar 
1 & \text{for }  r< R_a\\
0 &  \text{for } r> R_b > R_a
\ear\right.
\eeq
On $[R_a,R_b]$ there as a smooth 3rd order polynomial connecting from 1 to 0. The values $R_a,R_b=15,20$
are specified in \verb"trunc[15,20]" in the present example. It is advisable, but not enforced by the
code, to let $R_a$ and $R_b$ coincide with element boundaries and to keep $R_b\leq R_0$, the complex scaling angle.

The rational of the potential cutoff is that in Volkov-based tSurff any effect of the long-range Coulomb
potential outside the surface will not be captured correctly, even when the surface values and derivatives are obtained
for the exact Coulomb problem without any truncation. The truncation does not  
change the scale of these errors. Having free motion beyond $R_b$ ensures that spectra should be 
independent of any choice of the tSurff-radius $R_c\ge R_b$, a useful sanity check for the computation.
\subsubsection*{tSurff-radius $R_c$}
\index{tSurff!radius}
The surface radius can be picked anywhere below or equal to the complex scaling radius: $R_c\leq R_0$.
Beyond that point, surface values do not have their physical meaning and cannot be used for extracting
spectra. The most reasonable choice is $R_c=\max(R_b,R_0)$. For consistency checks one may want to use
shorter radii: all radii $R_c\geq R_b$, i.e. outside the range of the potential, spectra must agree exactly 
up to possible discretization errors. The value
of $R_c$ is set by \verb"Surface: points=20.", i.e. $R_c=20$ in the present tutorial.

\subsubsection*{Time-propagation parameters}
\index{Time propagation}
\begin{verbatim}
TimePropagation: begin,end,print,store,cutEnergy,accuracy
-2 OptCyc, 6 OptCyc, 0.05 OptCyc,0.05 au,100,1.e-9
\end{verbatim}
\begin{itemize}
\item
\verb"begin": The begin of time-propagation is chosen at -2 optical cycles, the exact beginning of the $\cos^2$-pulse
with FWHM of 2 opt.cyc. Actually, this can also be omitted, as begin of time-propagation defaults to 
the beginning of the pulse (for finite pulses).
\index{Time propagation!begin}
\item
\verb"end": Propagation continues until 6 opt.cyc. This turns out to be sufficiently long after the end of the 
pulse for all relevant amplitude to have passed through the surface. At IR, 1 or 2 cycles typically
suffice, but case by case checking is recommended.
\index{Time propagation!end}
\item
\verb"print": progress will be printed to terminal or file output at these intervals.
\index{Time propagation!printout}
\item
\verb"store": the surface values will be stored at time intervals that are never smaller than this value.
Roughly, \verb"store"$\lesssim 2\pi/Emax$, where $Emax$ is the maximal spectral energy one may be interested in.
In our case, $Emax$ is chosen generously with a value of 120 au $\approx 3keV$. Note, this bears on storage and 
also somewhat on propagation time; for large problems, this should be chosen with some consideration.
\index{Time propagation!data saving}
\item
\verb"cutEnergy": a spectral cut is applied to the Hilbert space by removing eigenvectors of some projection operator
with eigenvalues larger then \verb"cutEnergy" from the calculations. The operator can be chosen, but defaults
to the field free Hamiltonian specified in \verb"Operator: hamiltonian". This controls the stiffeness problem for 
the explict time-propagators used in \tRecX. Values of 100 au, as in the present example, were found to be safe, but 
again, case by case checks are needed. Note that CPU time typically grows linearly with increasing cutoff.
\index{Time propagation!stiffness}
\item
\verb"accuracy": $\infty$-norm based control of the time-integration step size. Values in the 
range \verb"1e-7" to  \verb"1e-9" were found to work reasonably well. Time-penalties for high accuracy are moderate. 
Values beyond $\lesssim$\verb"1e-11" are not useful and may even lead to breakdown due to numerical noise.
\index{Time propagation!accuracy}
\end{itemize}

\section{11IRshortPulse - H-atom with 2-cycle 800nm pulse}

Very similar to \verb"10IRSpectrum", except for higher intensity and a different pulse shape.

\subsubsection*{$\cos^8$ pulse}
\index{Pulses!cos8}
\begin{verbatim}
Laser: shape, I(W/cm2), FWHM, lambda(nm)
cos8, 2.e14, 3. OptCyc, 800.
\end{verbatim}
The popular $\cos^2$-pulse have severe side-bands and an usually produce artefacts, see discussion in \cite{Zielinsiki}.
A very good way out is to use $\cos^8$ where we found no artifacts. Note that this approaches a Gaussian if the proper 
limit $\lim_{n\to\infty}\cos^n$ is taken.

\subsubsection*{Threshold for operator application}
\index{Time propagation!small operators}
\verb"TimePropagation: operatorThreshold=5.e-8"\\
The code can detect when the application of the parts of the operator would have little effect, i.e. when the norm of
the resulting derivative vector is below some threshold. The threshold defaults to 0, but can be raised, as in this 
example. Speedups by factors 2-3 are achieved, but must be used with caution, in particular in relation to the 
desired accuracy. Warnings will be issued when the threshold seems to be too high. Note, that the procedure effectively 
produces non-hermitian operators and may lead to violation of norm-conservation.

\section{12IRlongPulse - H-atom with 27 fs, 800nm pulse}
More of the same, just now with a seriously long pulse. Run time on my laptop (Intel I7), single CPU,
compiled with gcc flags \verb"-O3 -D_USE_FFTW_ -D_NODEVELOP_" (see make.inc\_EXAMPLES): about 7 min for generating
the surfaces, about 6 min for generating spectrum with 400 momentum points equidistant in $|\vk|$ and all partial waves.
Analysis time scales linearly with the number of $|\vk|$ values.


\section{13Circular400nm - H-atom with short circular pulse}

The Hamiltonian (\ref{eq:hamiltionianH}) is used, but the vector potential of the two-cycle pulse 
is in the $xz$-plane:
\begin{verbatim}
Laser: shape, I(W/cm2), FWHM, lambda(nm), polarAngle, azimuthAngle, phiCEO
cos2, 1.e14, 2. OptCyc, 400., 90., 0.
cos2, 1.e14, 2. OptCyc, 400., 90.,90.,pi/2
\end{verbatim} 
The $y$-component is phase-shifted as in a circularly polarized pulse. Note, however, that
the meaning of circular polarization is limited for a short pulse, see figure.
\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{img/13field}
\end{center}
\caption{Field of the above pulse in the $xy$-plane. Plot columns 7 vs. 10 of ``Laser'' file in the run directory:  
\quoteInput{SCRIPTS/plot.py -dir=tutorial/13Circular400nm -which=Laser[7:10] 0017}.
}
\end{figure}

\subsubsection*{Fully 3d basis}
\begin{verbatim}
Axis: name,nCoefficients,lower end, upper end,functions,order
 Phi,33
 Eta,17,-1,1, assocLegendre{Phi}
 Rn,40, 0.,Rc,polynomial,10
 Rn,20, Rc,Infty,polExp[1.]
\end{verbatim}
with 17 angular momenta $l=0,1,\ldots,17-1=16$, i.e. $l_{\max}=16$ and all corresponding $m$'s.
\subsubsection*{Operator threshold}
\index{Time propagation!small operators}
\index{Speedups|see{Time propagation}}
The complete basis will be set up, but, in fact, quite a few of the components will never be populated much.
The \verb"TimePropagation: operatorThreshold=1.e-6" detects this and does not waste time
on applying to near-zero components.

\subsubsection*{Spectra}
\begin{verbatim}
>Spectrum tutorial/13Circular400nm/0002 -nR=200 
\end{verbatim}
produces the partial wave spectra with 200 points for the $|\vk|$. Reasonable defaults for 
all relevant spectra parameters are provided, e.g. for maximal energy, resolution, integration time.
Enter \verb"Spectrum" w/o arguments to see input options. If other representations are desired, e.g.
cuts at a range of values of $\cos\theta=:\eta$, this is obtained by
\begin{verbatim}
>Spectrum tutorial/13Circular400nm/0002 -plot=cutEta
\end{verbatim}
which will use previously defined amplitudes or compute afresh, if not available.
Any change of the input flags will solicitate a complete re-compute, which can be requested by
the flag \verb"-compute".

\subsubsection*{Plots}
There are plot scripts located in \verb"SCRIPTS" that recognize many output files of \tRecX.
Just try \verb"plot.py".\index{Scripts!plot.py}
 For example:
\begin{verbatim}
>SCRIPTS/plot.py -dir=tutorial/13Circular400nm -which=spec_cutEta[5] 0001 0002
\end{verbatim}
which will plot the 5'th colum of the \verb"spec_cutEta" files (i.e. two-dimensional spectral cuts) found 
in the the subdirectories 0001 and 0002 of \verb"tutorial/13Circular400nm", resulting plot below, also putting
a PNG-copy into ``plot.png''. If no inputs are specfied, some help will be displayed.
\begin{figure}
\begin{center}
\includegraphics[width=0.8\textwidth]{img/13plot}
\end{center}
\caption{PNG plot obtained by
\quoteInput{plot.py -dir=tutorial/13Circular400nm -which=spec\_cutEta[5] 0001 0002}.
\index{Scripts!plot.py}
}

\end{figure}

\section{14Circular400nmLong - H-atom with long circular pulse}
The only difference to tutorial/13Circular400nm is the FWHM pulse duration of 10 OptCyc.
This is now a bonafide cicrular polarization, producing nice cricular multi-photon peaks, as shown 
in the figure below. Run times 
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.32\textwidth]{img/14field}
\includegraphics[width=0.4\textwidth]{img/14cutEta}
\end{center}
\caption{Field  and $\eta=0$ cut of the photo-emission spectrum for a long circular pulse (14Circular400nmLong).  
(plot.py parameters:  \quoteInput{tutorial/14Circular400nmLong/0000/Laser[7:10] -linY}
and \quoteInput{tutorial/14Circular400nmLong/0000/spec\_cutEta[5]}).
}
\end{figure}

\subsubsection*{Angular selection}
\index{Speedups!preponderance rule}
There is a preponderance rule for circular pulses such that
\beq
L\pm M\quad \text{small},
\eeq
where the sign depends on the field's helicity. In case of a cw field one would have the strict
conservation $L\pm M=0$. Although this will be detected by the code and near-zeros will not 
be acted on, both, setup and propagation speed up significantly if the constraint is imposed on
the basis directly. This can be done by specifying the \verb"Phi" and \verb"Eta" axes as
\begin{verbatim}
 Phi,33,,,expIm
 Eta,17,-1,1, assocLegendre{Phi.L-M<5}
\end{verbatim}
The explicit specification of \verb"expIm" as the basis on \verb"Phi" is needed as the default 
is \verb"cosSin", combining to reals spherical harmonics. \verb"cosSin" this is default, as it can be useful to 
have purely real basis functions, in particular in combination with complex scaling.
In the present example, this reduces the propagation time from about 28 to 9 min, without compromizing the
accurcay of the photo-electron spectra, see figure. Clearly, for larger angular momentum ranges, the gains by constraints
will grow further. Computation of spectra by \verb"Spectrum" does not equally profit from the constraint as it
uses a full discretization on the $\vk$-grid.
\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.5\textwidth]{img/14constraint}
\end{center}
\caption{Relative difference of the total photo-electron spectrum computed with  and without the constraint $L-M<5$.
(Eta basis function definition as ``assocLegendre\{Phi.L-M$<$5\}'').
Note that the calculation with constraint is about a factor 3 faster. Figure obtained by 
\quoteInput{SCRIPTS/compare.py spec\_total tutorial/14Circular400nmLong 0000 0001}
}
\end{figure}

\section{15TayloredField - H-atom in a two-color field}
Here the field is composed of 2 two-cycle (2.7 fs FWHM) 400 nm pulse and a 2.7 fs 200 nm pulse with 
perpendicular polarization (polarization plane xy). Preponderance, although some rules may be there, is hard to guess.
It is advisible to leave the suppression of possible unwanted components to the autmatic detection. Nevertheless, with a 
total of 280 partial waves, run time remains $\sim 9$ min (significant part of it setup). Pretty images result.
\begin{figure}[h!]
\includegraphics[width=0.3\textwidth]{img/15field}
\includegraphics[width=0.65\textwidth]{img/15cutEta}
\caption{Field and $\eta=0$ cut of the photo-emission spectrum for two-color field (15TayloredField).
The \rhs set of figures was obtained by \quoteInput{plot.py tutorial/15TayloredField/0000/spec\_cutEta[2-7]}.  
}
\end{figure}

\section{16RotatingFrame - align coordinates with the rotating field}
\index{Speedups!rotating frame}
The same physical problem as \verb"13Circular400nm", but now we 
choose the $zx$-plane as the polarization plane
and operate in a rotating frame
where $z$-axis remains aligned with the vector potential.

The rational of this is that if 
 the laser frequency is low compared to any dynamics,
i.e. when we are approaching adiabatic motion,
the wavefunction expansion requires only low $|m|$-values.
Contrary to the preponderance of small $L-M$ in circular polarization discussed above,
the rotating frame profits from approximate adiabaticity \wrt to changes in the 
field direction and  can be used with arbitrary polarization, including non-planar.

In the present example, using 9 instead of the full 33 Phi-functions in lab frame
reduces computation times by a factor 2, ionization yields agree on 5 digits, see figure.
Compared to the \verb"13Circular400nm" calculation in the $xy$-plane without use of preponderance, 
the gain is a factor 3. Gains will increase at longer wave length. 

The rotating frame is an alterative to the (dynamical) adjustment of the
basis. It comes at the cost of an extra operator. Of course, also with a rotating frame dynamical basis 
adjustent can be used (as in the present example), but gains are small.

{\bf Caution:}
as of now, the surfaces are written in the instantaneous frame
and cannot be used with the Spectrum code
(back-rotating the surface is cheap, but awaits implementation).
We therefore only compare the expectation value for the ball \verb"Rn=[0,20]", written into the \verb"expec"-file.
\begin{figure}[h!]
\includegraphics[width=0.45\textwidth]{img/16timeGain}
\includegraphics[width=0.45\textwidth]{img/16error}
\caption{Left: CPU time vs. wave-function time with full angular basis of \quoteInput{13Circular400nm} compared to
\quoteInput{16RotationgFrame} with all, 9 and 7 functions on \quoteInput{Phi},i.e. constraints $|m|\leq4$ and $\leq3$. 
In the full computation, effects of the dynamical adjustment of the basis are visible. With field in the $xy$-plane,
\quoteInput{13Circular400nm} is slowest.
Right: relative error by calculating in rotating frame with constraint $|m|\leq4$ and $\leq3$. 
{\footnotesize Plot commands:
 \quoteInput{plot.py -dir=tutorial -which=expec[1] 13Circular400nm/0003 16RotatingFrame/0006 16RotatingFrame/0005 16RotatingFrame/0007 -linY} and
 \quoteInput{compare.py expec[4] tutorial/16RotatingFrame 0005 0006 0007 }}
}
\end{figure}

\section{17MixedGauge - locally length, asymptotic velocity gauge}
\index{Gauge!mixed}
When field-free states are used as part of the basis, they retain their intended physical meaning
only when length gauge is used. On the other hand, for computational efficiency we want velocity 
gauge where the electron is essentially moving freely. How to combine the two is described in 
Ref.~\cite{majety15:mixed}. In the transition region rather ugly, quadrupole-type operators
appear. These are pre-defined for polar coordinates as \verb"<<MixedGaugeDipole:Rg=20>>".
In this example length gauge will be used up to the ``gauge radius'' $R_g=20$. The radius must coincide
with an element boundary. This will be checked and the code terminates, if it is violated.

Surfaces will be transformed to velocity gauge before saving, such that spectral analysis 
works exactly as in velocity gauge. At present, this transformation is only implemented from
length to velocity gauge, therefore we need the surface radius $R_c\le R_g$ (not a deep limitation, can 
and will be removed).

Mixed gauge is computationally slightly less efficient than velocity gauge, see figure.

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.6\textwidth]{img/17error}
\end{center}
\caption{Comparison of velocity gauge \quoteInput{tutorial/17MixedGauge} with $R_g=0$ and mixed gauge $R_g=20$
to a fully converged spectrum. Mixed requires 25 angular momenta for the same scale error as velocity with 19 angular momenta (parameters
of \quoteInput{10IRSpectrum})
}
\end{figure}




\section{20Helium2d - 2 x 1d Helium (basic)}

Computes double-emission spectra for the simple 2x1d Helium model at high intensity, short pulse in the UV.
Demonstrates the use of tSurff for computing double emission spectra, see Ref.~\cite{scrinzi12}.

\beq
H_0(x,y) = -\frac12\ddxx -\frac12\ddyy - \frac{2c(x)}{\sqrt{x^2+0.5}} - \frac{2c(y)}{\sqrt{x^2+0.5}} + \frac{c(x)x(y)}{\sqrt{(x-y)^2+\al}}
\eeq
The electron repulsion parameter is chosen as $\al=0.3$, which results in the system's ground state energy of -2.88, a fair approximation
to the 6d He ground state of -2.903. Note that the ionic ground state for this Hamiltonian is -2 {\em exactly}. The cutoff functions
$c(x),c(y)$ are defined in Eq.~(\ref{eq:cutoff}).
\beq
H_I(x,y,t)=iA_z(t)(\ddx+\ddy)
\eeq
{\bf Pulse parameters:} typically $I=4\times10^{14}$, $\la=200\,nm$, FWHM 5 optical cycles, but check \verb"tutorial/20Helium2d.inp" for the exact current values.

\subsubsection*{Running the tutorial}

\begin{enumerate}
\item
\verb">tRecX tutorial/02Helium2d":\\
This solves the two-electron problem for $|x|,|y|<R_c$ for obtaining surface an derivative values at the 
boundaries of that region. Amounts to solving the homogeous TDSE with irECS absorption starting at $R_0=R_c$.
\item
\verb">tRecX tutorial/20Helium2d/"xxxx\verb"/inpc -unbound=1 -subregion="$s$ for subregions $s=0,1$.\\
Solves the {\em inhomogeneous} Schr\"odinger equation on the respective sub-regions $[0,R_c]\times [R_c,\infty)$, 
 $[R_c,\infty)\times[0,R_c]$,  etc. to obtain values and derivatives at the surfaces $x=[R_c,\infty),y=R_c$ and 
 $x=R_c, y=[R_c,\infty)$. Note that surfaces indeed extend to infinity as one uses the analytical 
Volkov solutions. Amounts to solving several inhomogeous TDSEs with the flux through the surfaces
$y=R_c$ and $x=R_c$, respectively, as source terms and irECS absorption beyond the $R_c$ for the other
coordinate.
\item 
\verb">Spectrum tutorial/20Helium2d/"xxxx:\\
From values and derivatives at the boundaries of the sub-regions, compute the spectral amplitudes 
for the region $[R_c,\infty)\times [R_c,\infty)$. These are given as the amplitudes of an expansion
of $\Psi(x,y)$ in terms of products single-particle Volkov solutions. 
\end{enumerate}
Note: in the present public version of \tRecX only ionization into the quadrant $k_x,k_y>0$ can be produced.
Implementation of the other quadrants is analogous and will be provided in due time.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "Tutorials"
%%% End: 
