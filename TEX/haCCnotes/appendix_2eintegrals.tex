\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}

\section{Two electron integrals} \label{app:2e}

The two electron integrals are the computationally most intensive parts in the setting up of the Hamiltonian.
In the current implementation of these integrals, they are evaluated using a multi-pole expansion, where the 
electron-electron interaction operator is expanded as:

\begin{equation}
\frac{1}{| \vec{r}_1 - \vec{r}_2 |} = \sum\limits_{L=0}^\infty \frac{4 \pi}{2L+1} \frac{r_<^L}{r_>^{L+1}} \sum\limits_{M=-L}^L Y_{LM}(\Omega_1) Y^*_{LM}(\Omega_2)
\end{equation}
where $r_< = \min(r_1,r_2)$ and $r_> = \max(r_1,r_2)$. The infinite multi-pole expansion is truncated by the angular momentum
content in the basis and the limits are denoted using symbols $L_{min},L_{max},M_{min}$ and $M_{max}$. 

The radial part of the operator, $\frac{r_<^L}{r_>^{L+1}}$, is evaluated with a polynomial basis and
the matrix elements obtained are transformed to a corresponding quadrature grid representation. The quadrature grid has the same order as the polynomial basis.
This matrix is denoted as $V^L_{qq'}$. The molecular orbitals, $\phi_k$ are expressed in a single centered expansion 
which can be written as:
\begin{equation}
 \phi_i = \sum\limits_{ql_im_i} c^i_{ql_im_i} Y_{l_im_i}.
\end{equation}
Since a multi-centered atomic orbital needs an infinite angular momenta, the above expansion
is truncated by defining a tolerance and this tolerance is a convergence parameter in the calculations. 
The resulting limits for the angular momentum expansion for the molecular orbitals are denoted as $l^g_{max}$
and $m^g_{max}$.
The single-electron numerical basis can also be expressed on a quadrature grid as:
\begin{equation}
 \alpha(\vec{r}) = \sum_q \alpha_q Y_{l_\alpha m_\alpha}.
\end{equation}
The largest angular momenta in this one-electron expansion is denoted using symbols $l_{max}$ and $m_{max}$.

There are four types of two-electron integrals that appear
in the haCC scheme:
$\la \alpha \phi_i | V^{\{2\}} |  \beta \phi_j \ra \rho^{IJ}_{ij}$,
$\la \alpha \phi_i | V^{\{2\}} |  \phi_j \beta \ra \rho^{IJ}_{ij}$,
$\la \alpha \phi_m | V^{\{2\}} |  \phi_l \phi_n \ra \rho^{IJ}_{klmn}$ and
$\la \alpha \phi_l | V^{\{2\}} |  \phi_m \phi_k \ra \eta^{IN}_{klm}$ (See section (\ref{sec:2e_der})). Outline of the
steps following to compute them in the current implementation is presented in the following subsections.
There exists a trade-off between the storage requirements and the operations count while designing
a suitable algorithm.

\subsection{Hartree term}

The Hartree term: $\la \alpha \phi_i | V^{\{2\}} | \beta \phi_j \ra \rho^{IJ}_{ij}$, is the easiest of the all the
four varieties as the integral over the molecular orbitals yields an effective potential for the second
electron coordinate. Denoting the matrix element as $M^{IJ}_{\alpha \beta}$:
\begin{equation}
     \begin{aligned}
      M^{IJ}_{\alpha \beta} &= \sum_{ij} \la \alpha \phi_i | V^{\{2\}} | \beta \phi_j \ra \rho^{IJ}_{ij} \\
      &= \sum_{ij} \rho^{IJ}_{ij} \sum_{qq'} \sum_{LM} \frac{4 \pi}{2L+1} \alpha_q^* \beta_q V^L_{qq'}  
%       \\ & \qquad \qquad 
      \sum_{l_im_i} c^{i*}_{q'l_im_i} \sum_{l_jm_j} c^j_{q'l_jm_j} \la Y_{l_\alpha m_\alpha} Y_{LM} |  Y_{l_\beta m_\beta} \ra \la Y_{l_im_i} | Y_{LM} Y_{l_j m_j} \ra  
     \end{aligned}
\end{equation}
the integral evaluation is performed using the following steps:
\begin{enumerate}

\item First, an effective potential object $R^{IJ}_{LMq}$ is defined as follows:
\begin{equation} \label{eq:ht_g1} 
R^{IJ}_{LMq} = \frac{4 \pi}{2L+1} \sum_{ij} \rho^{IJ}_{ij} \sum_{q'} V^L_{qq'} \sum_{l_im_i} c^{i*}_{q'l_im_i} \sum_{\l_jm_j} c^j_{q'l_jm_j} \la Y_{l_im_i} | Y_{LM} Y_{l_j m_j} \ra
\end{equation}
which is computed at the start and stored. The next steps are done on the fly.

\item  For each $I,J,\alpha,\beta$ the vector $T_q$ is computed as:
\begin{equation} \label{eq:ht_g2}
 T_q = \sum_{LM} R^{IJ}_{LMq} \la Y_{l_\alpha m_\alpha} Y_{LM} | Y_{l_\beta m_\beta} \ra
\end{equation}

\item This yields the required integral
\begin{equation}
M^{IJ}_{\alpha \beta} = \sum_q \alpha_q^* T_q \beta_q  
\end{equation}
\end{enumerate}

The limits of $LM$ expansion define the required storage and this can be obtained by examining
equations \ref{eq:ht_g1} and \ref{eq:ht_g2}. The needed $LM$ are:

\begin{eqnarray*}
 L_{min} &=& 0 \\
 L_{max} &=& 2\min(l_{max},l^g_{max}) \\
 M_{min}(L) &=& \max(\max(-2m^g_{max},-2m_{max}),-L) \\
 M_{max}(L) &=& \min(\min(2m^g_{max},2m_{max}),L) 
\end{eqnarray*}



\subsection{Standard exchange term}

The standard exchange term which is the most expensive of all can be expressed as:

\begin{equation}
\begin{aligned}
M_{\alpha\beta}^{IJ} & =  \sum_{ij}\langle\alpha\phi_{i}|V^{\{2\}}|\phi_{j}\beta\rangle\rho_{ij}^{IJ}\\
 & =  \sum_{ij}\rho_{ij}^{IJ}\sum_{qq'}\sum_{LM}\frac{4\pi}{2L+1}\alpha_{q}^{*}\beta_{q'}V_{qq'}^{L}
%  &  & \qquad
 \sum_{l_{i}m_{i}}c_{q'l_{i}m_{i}}^{i*}\langle Y_{l_{i}m_{i}}Y_{LM}|Y_{l_{\beta}m_{\beta}}\rangle\sum_{l_{j}m_{j}}c_{ql_{j}m_{j}}^{j}\langle
 Y_{l_{\alpha}m_{\alpha}}|Y_{LM}Y_{l_{j}m_{j}}\rangle 
 \end{aligned}
\end{equation}
which can be recast as:
\beq
M_{\alpha\beta}^{IJ}=\sum_{qq'}\alpha_{q}^{*}\beta_{q}\sum_{LM}\frac{4\pi}{2L+1}V_{qq'}^{L}\sum_{ij}R_{\alpha jq}^{LM}\rho_{ij}^{IJ}R_{\beta iq'}^{\dagger LM}
\eeq
where 
\beq
R_{\alpha jq}^{LM}=\sum_{l_{j}m_{j}}c_{ql_{j}m_{j}}^{j}\langle Y_{l_{\alpha}m_{\alpha}}|Y_{LM}Y_{l_{j}m_{j}}\rangle
\eeq

The expression within the ij summation can be simplified
by performing a singular value decomposition or in other words by transforming
from the molecular orbital basis to natural orbital basis. In the natural orbital
basis $\rho_{ij}^{IJ}$ is a diagonal matrix.
Using the singular value decomposition $\rho_{ji}^{IJ}=U_{jx}^{IJ}V_{xi}^{IJ\dagger}$,

\beq
\sum_{ij}R_{\alpha jq}^{LM}\rho_{ij}^{IJ}R_{\beta iq'}^{\dagger LM}=\sum_{ij}R_{\alpha jq}^{LM}U_{jx}^{IJ}V_{xi}^{IJ\dagger}R_{\beta iq'}^{\dagger LM}=\sum_{x}T_{\alpha xq}^{IJLM}T_{\beta xq'}^{\dagger IJLM}
\eeq

This reduces the number of required floating point operations when
the number of natural orbitals is smaller than the number of Hartree-Fock
orbitals. The reduction in the double summation ij to single summation is compensated by the fact that T is also a function of IJ unlike R.

The original expression for the exchange integral
can be re-written as:

\beq
M_{\alpha\beta}^{IJ}=\sum_{qq'}\alpha_{q}^{*}\beta_{q}\sum_{LM}\frac{4\pi}{2L+1}V_{qq'}^{L}\sum_{x}T_{\alpha xq}^{IJLM}T_{\beta xq'}^{\dagger IJLM}
\eeq
The integrals are computed using the following steps:
\begin{enumerate}
 \item  The objects T are computed and stored. This is possible for atomic
case. In the molecular case due to the large angular momentum requirements,
storing these may not be possible.

\item The next steps are done on the fly. An object $S_{qq'\al\be}^{IJ}$ is evaluated as:
\beq
S_{qq'\al\be}^{IJ}=\sum_{LM}\frac{4\pi}{2L+1}V_{qq'}^{L}\sum_{x}T_{\alpha xq}^{IJLM}T_{\beta xq'}^{\dagger IJLM}
\eeq

\item This leads to the required integral:
\beq
M_{\alpha\beta}^{IJ}=\sum_{qq'}\alpha_{q}^{*}S_{qq'\al\be}^{IJ}\beta_{q}
\eeq
\end{enumerate}
The multipole expansion truncations can be obtained by examining the integrals between
the spherical harmonics. In the current case the $LM$ truncation limits are:
\begin{eqnarray*}
L_{min} &=& \max(0,l_a-l^g_{max}) \\
L_{max} &=& l_a+l^g_{max}\\
M_{min} &=& \min(\max(-L,m_a-m^g_{max}),L)\\
M_{max} &=& \max(\min(L,m_a+m^g_{max}),-L)\\
\end{eqnarray*}

\subsection{Non-standard two-electron integral: $\la \alpha \phi_b | V^{\{2\}} |\phi_c \phi_d \ra \rho^{IJ}_{abcd}$}

There are other non-standard exchange terms that appear in the two-particle operator due to non-orthogonality of the 
molecular orbitals from quantum chemistry with respect to the single electron finite element basis.
The integral 
\begin{equation}
     \begin{aligned}
      M^{IJ}_{\alpha a} &= \sum_{bcd} \la \alpha \phi_b | V^{\{2\}} |\phi_c \phi_d \ra \rho^{IJ}_{abcd} \\
      &= \sum_{bdc} \rho^{IJ}_{abcd} \sum_{qq'} \sum_{LM} \frac{4 \pi}{2L+1} \alpha^*_q V^L_{qq'} \sum_{l_bm_b} c^{b*}_{q'l_bm_b} 
%       \\ & \qquad \qquad 
      \sum_{l_cm_c} c^c_{ql_cm_c} \sum_{l_dm_d} c^{d}_{q'l_dm_d} \la Y_{l_b m_b} | Y_{LM} Y_{l_d m_d} \ra \la Y_{l_\alpha m_\alpha} Y_{LM} | Y_{l_cm_c} \ra  
     \end{aligned}
\end{equation}
is evaluated using the following steps:

\begin{enumerate}
 \item A direct potential like object $P^{bd}_{LMq}$ is first constructed:
\begin{equation}
P^{bd}_{LMq} = \frac{4 \pi}{2L+1} \sum_{q'} V^L_{qq'} \sum_{l_bm_b} c^{b*}_{q'l_bm_b} \sum_{l_dm_d} c^{d}_{q'l_dm_d} \la Y_{l_b m_b} | Y_{LM} Y_{l_d m_d} \ra
\end{equation}

\item For each $l_\alpha,m_\alpha$ an intermediate object $Q^{cq}_{bd}$
\begin{equation}
Q^{cq}_{bd} = \sum_{l_cm_c} c^c_{ql_cm_c} \sum_{LM} \la Y_{l_\alpha m_\alpha} Y_{LM} | Y_{l_cm_c} \ra P^{bd}_{LMq}
\end{equation}

\item Using the object Q, an object $T^{IJ}_{qa}$ is constructed as:
\begin{equation}
T^{IJ}_{qa} = \sum_{bcd} Q^{cq}_{bd} \rho^{IJ}_{abcd}
\end{equation}
which is computed and stored.

\item This leads to the required result:
\begin{equation}
M^{IJ}_{\alpha a} = \sum_{q} T^{IJ}_{qa} \alpha^*_q
\end{equation}
\end{enumerate}

The multipole expansions are truncated as:\\
\begin{eqnarray*}
L_{min} &=& 0 \\
L_{max} &=& \min(2l^g_{max},l_{max}+l^g_{max}) \\
M_{min} &=& \max(-L,-2m^g_{max},-m_{max}-m^g_{max}) \\
M_{max} &=& \min(L,2m^g_{max},m_{max}+m^g_{max}) \\
\end{eqnarray*}

\subsection{Non-standard two-electron integral: $\langle\phi_{a}\phi_{b}|V^{\{2\}}|\phi_{d}\beta\rangle\eta_{abd}^{\mathcal{N}J}$}

The steps followed for this integral is very similar to the previous one.
The integral can be expanded using the multi-pole expansion as:

\begin{equation}
\begin{aligned}
M_{\beta}^{\mathcal{N}J} & = \sum_{abd}\langle\phi_{a}\phi_{b}|V^{\{2\}}|\phi_{d}\beta\rangle\eta_{abd}^{\mathcal{N}J} \\
 & = \sum_{abd}\eta_{abd}^{\mathcal{N}J}\sum_{qq'}\sum_{LM}\frac{4\pi}{2L+1}\beta_{q'}V_{qq'}^{L}\sum_{l_{a}m_{a}}c_{ql_{a}m_{a}}^{a*}
%  &  & \qquad
 \sum_{l_{b}m_{b}}c_{q'l_{b}m_{b}}^{b*}\sum_{l_{d}m_{d}}c_{ql_{d}m_{d}}^{d}\langle Y_{l_{a}m_{a}}|Y_{LM}Y_{l_{d}m_{d}}\rangle\langle Y_{l_{b}m_{b}}Y_{LM}|Y_{l_{\beta}m_{\beta}}\rangle
\end{aligned}
\end{equation}

which is evaluated using the following steps.

\begin{enumerate}
 \item  Construct the potential $P_{LMq'}^{ad}$
\beq
P_{LMq'}^{ad}=\frac{4\pi}{2L+1}\sum_{q}V_{qq'}^{L}\sum_{l_{a}m_{a}}c_{ql_{a}m_{a}}^{a*}\sum_{l_{d}m_{d}}c_{ql_{d}m_{d}}^{d}\langle Y_{l_{a}m_{a}}|Y_{LM}Y_{l_{d}m_{d}}\rangle
\eeq

\item For each $l_{\beta}$ and $m_{\beta}$ construct an intermediate
object $Q_{ab}^{bq'}$as:
\beq
Q_{ab}^{bq'}=\sum_{l_{b}m_{b}}c_{q'l_{b}m_{b}}^{b*}\sum_{LM}\langle Y_{l_{b}m_{b}}Y_{LM}|Y_{l_{\beta}m_{\beta}}\rangle P_{lmq'}^{ad}
\eeq

\item Construct object $T_{q'}$ as:
\beq
T_{q'}^{\mathcal{N}J}=\sum_{abd}Q_{ad}^{bq'}\eta_{abd}^{\mathcal{N}J}
\eeq
This object is computed initially for each $l_{\beta}$ and $m_{\beta}$ and stored.

\item Finally, the required matrix element is computed on the fly using the stored object T as:
\beq
M_{\beta}^{\mathcal{N}J}=\sum_{q'}T_{q'}^{\mathcal{N}J}\beta_{q'}
\eeq
\end{enumerate}

The multi-pole expansions are truncated as:\\
\begin{eqnarray*}
L_{min} &=& 0 \\
L_{max} &=& \min(2l^g_{max},l_{max}+l^g_{max})\\
M_{min} &=& \max(-L,-2m^g_{max},-m_{max}-m^g_{max})\\
M_{max} &=& \min(L,2m^g_{max},m_{max}+m^g_{max})\\
\end{eqnarray*}

\noindent {\bf Note:} As finite elements are used, the angular momentum limits can also be made a function of the 
finite element number.