% \renewcommand\thesection{\Alph{section}}
\allowdisplaybreaks

% \chapter{Technical appendices}

\section{Derivation of matrix elements} \label{app:matelem}

The haCC basis with the neutral bound states ($|\mathcal{N}\rangle$) and the single ionic channel functions
($\mathcal{A}[|I\rangle|\alpha\rangle]$) is expressed as:
\begin{equation}
|\Psi\rangle= \frac{1}{\sqrt{N}} \sum_{I,\alpha}\mathcal{A}[|I,\alpha\rangle]C_{I\alpha}+\sum_{\mathcal{N}}|\mathcal{N}\rangle C_{\mathcal{N}}
\end{equation}
To avoid confusion, the single electron basis is represented using Greek letters: $|\alpha\rangle$ and $|\beta\rangle$
and the normalization factor is explicitly written.
For the sake of simplicity, the ionic and neutral configuration interaction functions composed of Hartree Fock orbitals,
$|\phi_{k}\rangle$ are denoted as:
\begin{equation}
|I\rangle= \frac{1}{\sqrt{(N-1)!}} \sum_{i_{1}i_{2}\cdots i_{N-1}}|\phi_{i_{1}}\phi_{i_{2}}\cdots\phi_{i_{N-1}}\rangle a_{i_{1}\cdots,i_{N-1}}
           := \frac{1}{\sqrt{(N-1)!}} \sum_{i_{1}\cdots i_{N-1}}|i_{1}\cdots i_{N-1}\rangle a_{i_{1}\cdots,i_{N-1}}
\end{equation}
\begin{equation}
|\mathcal{N}\rangle= \frac{1}{\sqrt{N!}} \sum_{n_{1}\cdots n_{N}}|\phi_{n_{1}}\phi_{n_{2}}\cdots\phi_{n_{N}}\rangle d_{n_{1}\cdots n_{N}}
                  := \frac{1}{\sqrt{N!}} \sum_{n_{1}\cdots n_{N}}|n_{1}\cdots n_{N}\rangle d_{n_{1}\cdots n_{N}}
\end{equation}
with the coefficients $a_{i_{1}\cdots i_{N-1}}$ and $d_{i_{1}\cdots i_{N}}$ satisfying the required anti-symmetry property. 
The ionic channels functions
\begin{equation}
\mathcal{A}|I,\alpha\rangle=\sum_{i_{1}\cdots i_{N-1}}\mathcal{A}|i_{1}\cdots i_{N-1}\alpha\rangle a_{i_{1}\cdots i_{N-1}}
\end{equation}
can be explicitly expanded as:
\begin{equation}
\mathcal{A}|i_{1}\cdots i_{N-1}\alpha\rangle=|i_{1}\cdots i_{N-1}\alpha\rangle+\cdots+(-1)^{N-k}|i_{1}\cdots i_{k-1}\alpha i_{k}\cdots i_{N-1}\rangle+\cdots
\end{equation}
Introducing a convenient notation:
\begin{equation}
A_{k}=\sum_{i_{1}\cdots i_{N-1}}(-1)^{N-k}|i_{1}\cdots i_{k-1}\alpha i_{k} \cdots i_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}
\end{equation}
the channel function can be written as:
\begin{equation}
\mathcal{A}|I,\alpha\rangle=\sum_{k=1}^{N}A_{k}
\end{equation}
where $A_{k}$ denotes the term with $\alpha$ at the kth position. 

The ionic and neutral states are orthonormal to each other, that is $\langle I|J\rangle=\delta_{IJ}$ and
$\langle\mathcal{N}|\mathcal{N}'\rangle=\delta_{\mathcal{NN}'}$. The Hartree-Fock orbitals are also
orthonormal $\langle\phi_{i}|\phi_{j}\rangle=\delta_{ij}$. The anti-symmetrization satisfies the property
$\mathcal{AA}=N\mathcal{A}$.

Finally, the generalized reduced density matrices between two N-particle wavefunctions are defined as:
\begin{equation}
\rho_{i_{1}\cdots i_{k}j_{1}\cdots j_{k}}^{IJ}=\frac{N\,!}{(N-k)\,!}\,\sum_{i_{k+1}\cdots i_{N}}\sum_{j_{k+1}\cdots j_{N}}a_{i_{1}\cdots i_{k}i_{k+1}\cdots i_{N}}^{*}b_{j_{1}\cdots j_{k}j_{k+1}\cdots j_{N}}
\end{equation}
and the non-standard generalized reduced density matrices between an N-particle function and an N-1 particle function as:
\begin{equation}
\eta_{i_{N-k}\cdots i_{N}j_{N-k}\cdots j_{N-1}}^{\mathcal{N}J}= \frac{\sqrt{N}(N-1)\,!}{(N-k)\,!}\, \sum_{i_{1}\cdots i_{N-k}\cdots i_{N}}\sum_{j_{1}\cdots j_{N-k}\cdots j_{N-1}}d_{i_{1}\cdots i_{N}}^{*}b_{j_{1}\cdots j_{N-1}}
\end{equation}

\subsection{Overlap} \label{sec:ov_der}

\subsubsection*{Between neutrals}
The overlap matrix between neutral states satisfy the orthonormality
condition:
\begin{equation}
\boxed{\langle\mathcal{N}|\mathcal{N}'\rangle=\delta_{\mathcal{NN}'}}
\end{equation}

\subsubsection*{Between neutral and channel function}
The overlap between neutral and a channel functions can be evaluated as:
\begin{eqnarray*}
\frac{1}{\sqrt{N}} \langle\mathcal{N}|\mathcal{A}|J\beta\rangle & = & \sqrt{N} \cdot\langle\mathcal{N}|J\beta\rangle \qquad [\mathcal{N} \text{ is anti-symmetric}]\\
 & = & \sqrt{N} \cdot\sum_{n_{1}\cdots n_{N}}\sum_{j_{1}\cdots j_{N-1}}\langle n_{1}n_{2}\cdots n_{N}|j_{1}\cdots j_{N-1}\beta\rangle d_{n_{1}\cdots n_{N}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sqrt{N} \cdot\sum_{n_{1}\cdots n_{N}}\langle n_{N}|\beta\rangle d_{n_{1}\cdots n_{N}}^{*}b_{n_{1}\cdots n_{N-1}}\\
\text{[Renaming indices]} & = & \sum_{i}\langle i|\beta\rangle\eta_{i}^{\mathcal{N}J}\\
\\
\end{eqnarray*}

\[
\boxed{\frac{1}{\sqrt{N}} \langle\mathcal{N}|\mathcal{A}|J\beta\rangle=\langle i|\beta\rangle\eta_{i}^{\mathcal{N}J}}
\]

\subsubsection*{Between channel functions}
The overlap matrices between channel functions can be computed as:
\begin{eqnarray*}
\frac{1}{N} \langle I\alpha|\mathcal{AA}|J\beta\rangle  =  \langle I\alpha|\mathcal{A}|J\beta\rangle
 & = & \sum_{k=1}^{N}\langle A_{N}|B_{k}\rangle\\
 & = & \langle A_{N}|B_{N}\rangle+\sum_{k=1}^{N-1}\langle A_{N}|B_{k}\rangle\\
 \text{[Identical N-1 terms]} & = & \langle A_{N}|B_{N}\rangle+(N-1)\langle A_{N}|B_{1}\rangle
\end{eqnarray*}
The equivalence of the terms in each of the summations can be proved
through a simple interchange and renaming of indices. 

\[
\langle A_{N}|B_{N}\rangle=\langle I|J\rangle\langle\alpha|\beta\rangle
\]

\begin{eqnarray*}
\langle A_{N}|B_{1}\rangle & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}\cdots j_{N-1}}(-1)^{N-1}\langle i_{1}\cdots i_{N-1}\alpha|\beta j_{1}\cdots j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{N-1}}(-1)^{N-1}\langle i_{1}|\beta\rangle\langle\alpha|j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{i_{2}\cdots i_{N-1}j_{N-1}}\\
{}[\text{Reordering indices}] & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{N-1}}-\langle i_{1}|\beta\rangle\langle\alpha|j_{N-1}\rangle a_{i_{2}\cdots i_{N-1}i_{1}}^{*}b_{i_{2}\cdots i_{N-1}j_{N-1}}\\
{}[\text{Renaming indices}] & = & \sum_{i_{2}\cdots i_{N-1}ij}-\langle i|\beta\rangle\langle\alpha|j\rangle a_{i_{2}\cdots i_{N-1}i}^{*}b_{i_{2}\cdots i_{N-1}j}\\
\end{eqnarray*}

\begin{equation*}
 (N-1)\langle A_{N}|B_{1}\rangle = \sum_{ij}-\langle\alpha|j\rangle\rho_{ij}^{IJ}\langle i|\beta\rangle
\end{equation*}

Hence,
\[
\boxed{\frac{1}{N} \langle I\alpha|\mathcal{AA}|J\beta\rangle=\langle I|J\rangle\langle\alpha|\beta\rangle-\langle\alpha|j\rangle\rho_{ij}^{IJ}\langle i|\beta\rangle}
\]

\subsection{Single particle operators} \label{sec:1e_der}

The single particle operator for an N-particle system is defined as:
\beq
\hat{S}=\sum_{l=1}^{N}s_{l}
\eeq
where $s_l$ is the single particle operator that acts on the $l^{th}$ coordinate. Note, in the following treatment the subscript is
dropped when not essential.
As the operators are symmetric with respect to coordinate exchange $\frac{1}{N} \mathcal{A}\hat{S}\mathcal{A}=\hat{S}\mathcal{A}$

\subsubsection*{Between neutrals}
The single particle matrix elements between two neutral states can be computed as:
\begin{equation}
 \boxed{
\langle\mathcal{N}|\hat{S}|\mathcal{N}'\rangle=\langle i|s|j\rangle\rho_{ij}^{\mathcal{NN}'}
}
\end{equation}

\subsubsection*{Between neutral and channel function}
The single particle matrix elements between a neutral and an ionic channel functions are evaluated as:

\begin{eqnarray*}
\frac{1}{\sqrt{N}} \langle\mathcal{N}|\hat{S}\mathcal{A}|J\beta\rangle = \sqrt{N} \cdot \langle\mathcal{N}|\hat{S}|J\beta\rangle
 & = & \sqrt{N} \cdot \sum_{l=1}^{N}\langle\mathcal{N}|s_{l}|B_{N}\rangle\\
 & = & \sqrt{N} \cdot \langle\mathcal{N}|s_{N}|B_{N}\rangle+ \sqrt{N} \cdot \sum_{l=1}^{N-1}\langle\mathcal{N}|s_{l}|B_{N}\rangle\\
 & = & \sqrt{N} \cdot \langle\mathcal{N}|s_{N}|B_{N}\rangle+\sqrt{N} \cdot(N-1)\langle\mathcal{N}|s_{1}|B_{N}\rangle .\\
\end{eqnarray*}
Evaluating term-wise:
\\

\noindent \underline{Term 1:}

\begin{eqnarray*}
\langle\mathcal{N}|s_{N}|B_{N}\rangle & = & \sum_{n_{1}\cdots n_{N}}\sum_{j_{1}\cdots j_{N-1}}\langle n_{1}\cdots n_{N}|s_{N}|j_{1}\cdots j_{N-1}\beta\rangle d_{n_{1}\cdots n_{N}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{n_{1}\cdots n_{N}}\langle n_{N}|s_{N}|\beta\rangle d_{n_{1}\cdots n_{N}}^{*}b_{n_{1}\cdots n_{N-1}}\\
 & = & \sum_{i}\langle i|s_{N}|\beta\rangle\eta_{i}^{\mathcal{N}J} / \sqrt{N}
\end{eqnarray*}


\noindent \underline{Term 2:}

\begin{eqnarray*}
\langle\mathcal{N}|s_{1}|B_{N}\rangle & = & \sum_{n_{1}\cdots n_{N}}\sum_{j_{1}\cdots j_{N-1}}\langle n_{1}\cdots n_{N}|s_{1}|j_{1}\cdots j_{N-1}\beta\rangle d_{n_{1}\cdots n_{N}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{n_{1}\cdots n_{N}}\sum_{j_{1}}\langle n_{1}|s_{1}|j_{1}\rangle\langle n_{N}|\beta\rangle d_{n_{1}\cdots n_{N}}^{*}b_{j_{1}n_{2}\cdots n_{N-1}}\\
\text{[Reordering indices]} & = & \sum_{n_{1}\cdots n_{N}}\sum_{j_{1}}\langle n_{1}|s_{1}|j_{1}\rangle\langle n_{N}|\beta\rangle d_{n_{2}\cdots n_{N-1}n_{1}n_{N}}^{*}b_{n_{2}\cdots n_{N-1}j_{1}}\\
\text{[Renaming indices]} & = & \sum_{n_{1}n_{2}j_{1}}\langle n_{1}|s_{1}|j_{1}\rangle\langle n_{2}|\beta\rangle\eta_{n_{1}n_{2}j_{1}}^{\mathcal{N}J} / \left[ \sqrt{N} (N-1) \right]
\end{eqnarray*}


The total single particle operator between the neutral and the ionic
channel functions is:

\begin{equation}
\boxed{\frac{1}{\sqrt{N}} \langle\mathcal{N}|\hat{S}\mathcal{A}|J\beta\rangle=\langle i|s|\beta\rangle\eta_{i}^{\mathcal{N}J}+\langle n_{1}|s|j_{1}\rangle\langle n_{2}|\beta\rangle\eta_{n_{1}n_{2}j_{1}}^{\mathcal{N}I}}
\end{equation}

\subsubsection*{Between channel functions}
The single particle matrix elements between two ionic channel functions are evaluated as:

\begin{eqnarray*}
 \frac{1}{N} \langle I\alpha|\mathcal{A}\hat{S}\mathcal{A}|J\beta\rangle &=&\langle I\alpha|\hat{S}\mathcal{A}|J\beta\rangle \\
& = & \sum_{k=1}^{N}\sum_{l=1}^{N}\langle A_{N}|s_{l}|B_{k}\rangle\\
 & = & \langle A_{N}|s_{N}|B_{N}\rangle+\sum_{l=1}^{N-1}\langle A_{N}|s_{l}|B_{N}\rangle+\sum_{k=1}^{N-1}\langle A_{N}|s_{N}|B_{k}\rangle\\
 &  & +\sum_{k=1}^{N-1}\langle A_{N}|s_{k}|B_{k}\rangle+\sum_{k=1}^{N-1}\sum_{l=1,l\neq k}^{N-1}\langle A_{N}|s_{l}|B_{k}\rangle\\
\text{[Equivalent terms]} & = & \langle A_{N}|s_{N}|B_{N}\rangle+(N-1)\langle A_{N}|s_{1}|B_{N}\rangle+(N-1)\langle A_{N}|s_{N}|B_{1}\rangle\\
 &  & +(N-1)\langle A_{N}|s_{1}|B_{1}\rangle+(N-1)(N-2)\langle A_{N}|s_{2}|B_{1}\rangle
\end{eqnarray*}
Evaluating term-wise:
\\

\noindent \underline{Term 1:}

\[
\langle A_{N}|s_{N}|B_{N}\rangle=\langle I\alpha|s_{N}|J\beta\rangle=\langle I|J\rangle\langle\alpha|s_{N}|\beta\rangle
\]


\noindent \underline{Term 2:}

\begin{eqnarray*}
\langle A_{N}|s_{1}|B_{N}\rangle & = & \langle I\alpha|s_{1}|J\beta\rangle\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}\cdots j_{N-1}}\langle i_{1}\cdots i_{N-1}\alpha|s_{1}|j_{1}\cdots j_{N-1}\beta\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{i_{1}j_{1}}\langle i_{1}|s_{1}|j_{1}\rangle\rho_{i_{1}j_{1}}^{IJ}\langle\alpha|\beta\rangle / (N-1)
\end{eqnarray*}


\noindent \underline{Term 3:}

\begin{eqnarray*}
\langle A_{N}|s_{N}|B_{1}\rangle & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}\cdots j_{N-1}}(-1)^{N-1}\langle i_{1}\cdots i_{N-1}\alpha|s_{N}|\beta j_{1}\cdots j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{N-1}}(-1)^{N-1}\langle i_{1}|\beta\rangle\langle\alpha|s_{N}|j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{i_{2}\cdots i_{N-2}j_{N-1}}\\
\text{[Reordering indices]} & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{N-1}}-\langle i_{1}|\beta\rangle\langle\alpha|s_{N}|j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{N-1}i_{2}\cdots i_{N-2}}\\
 & = & \sum_{ij}-\langle i|\beta\rangle\langle\alpha|s_{N}|j\rangle\rho_{ij}^{IJ} / (N-1)
\end{eqnarray*}


\noindent \underline{Term 4:}

\begin{eqnarray*}
\langle A_{N}|s_{1}|B_{1}\rangle & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}\cdots j_{N-1}}(-1)^{N-1}\langle i_{1}\cdots i_{N-1}\alpha|s_{1}|\beta j_{1}\cdots j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{N-1}}(-1)^{N-1}\langle i_{1}|s_{1}|\beta\rangle\langle\alpha|j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{i_{2}\cdots i_{N-1}j_{N-1}}\\
\text{[Reordering indices]} & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{N-1}}-\langle i_{1}|s_{1}|\beta\rangle\langle\alpha|j_{N-1}\rangle a_{i_{1}\cdots j_{N-1}}^{*}b_{j_{N-1}i_{2}\cdots i_{N-1}}\\
 & = & \sum_{ij}-\langle i|s_{1}|\beta\rangle\langle\alpha|j\rangle\rho_{ij}^{IJ} / (N-1)
\end{eqnarray*}


\noindent \underline{Term 5:}

\begin{eqnarray*}
\langle A_{N}|s_{2}|B_{1}\rangle & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}\cdots j_{N-1}}(-1)^{N-1}\langle i_{1}\cdots i_{N-1}\alpha|s_{2}|\beta j_{1}\cdots j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{2}j_{N-1}}(-1)^{N-1}\langle i_{1}|\beta\rangle\langle i_{2}|s_{2}|j_{1}\rangle\langle\alpha|j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}i_{3}\cdots i_{N-1}j_{N-1}}\\
\text{[Reordering indices]} & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{2}j_{N-1}}-\langle i_{1}|\beta\rangle\langle i_{2}|s_{2}|j_{1}\rangle\langle\alpha|j_{N-1}\rangle a_{i_{1}i_{2}i_{3}\cdots i_{N-1}}^{*}b_{j_{N-1}j_{1}i_{3}\cdots i_{N-1}}\\
\text{[Renaming indices]} & = & \sum_{i_{3}\cdots i_{N-1}}\sum_{i_{1}i_{2}j_{1}j_{2}}-\langle i_{1}|\beta\rangle\langle i_{2}|s_{2}|j_{2}\rangle\langle\alpha|j_{1}\rangle a_{i_{1}i_{2}i_{3}\cdots i_{N-1}}^{*}b_{j_{1}j_{2}i_{3}\cdots i_{N-1}}\\
 & = & \sum_{i_{1}i_{2}j_{1}j_{2}}-\langle i_{1}|\beta\rangle\langle i_{2}|s_{2}|j_{2}\rangle\langle\alpha|j_{1}\rangle\rho_{i_{1}i_{2}j_{1}j_{2}}^{IJ}
 / \left[ (N-1)(N-2) \right]
\end{eqnarray*}


Hence, the overall one particle operator between the ionic channel
functions takes the form:

\newcommand*\widefbox[1]{\fbox{\hspace{2em}#1\hspace{2em}}}

\begin{empheq}[box=\widefbox]{align}
 \frac{1}{N} \langle I\alpha|\mathcal{A}\hat{S}\mathcal{A}|J\beta\rangle & =\langle I|J\rangle\langle\alpha|s|\beta\rangle+\langle\alpha|\beta\rangle\langle i|s|j\rangle\rho_{ij}^{IJ} \nonumber \\
 & - \langle\alpha|s|j\rangle\rho_{ij}^{IJ}\langle i|\beta\rangle-\langle\alpha|j\rangle\rho_{ij}^{IJ}\langle j|s|\beta\rangle \\
 & - \langle\alpha|j_{1}\rangle\langle i_{1}|\beta\rangle\langle i_{2}|s|j_{2}\rangle\rho_{i_{1}i_{2}j_{1}j_{2}}^{IJ} \nonumber
\end{empheq}

\subsection{Two particle operators} \label{sec:2e_der}

A general two particle operator can be written as
\begin{equation}
 \hat{T}=\sum_{m=1}^{N}\sum_{n=m+1}^{N}t_{mn}
\end{equation}
where $t_{mn}$ acts on the $m,n$ coordinates. Note, that the subscripts are dropped in the following treatment when not essential.
Again, as the operators are symmetric with respect to coordinate exchange $\frac{1}{N} \mathcal{A}\hat{T}\mathcal{A}=\hat{T}\mathcal{A}$

\subsubsection*{Between neutrals}
The two particle operators between two neutrals can be evaluated as:
\begin{equation}
\boxed{
\langle\mathcal{N}|\hat{T}|\mathcal{N}'\rangle=\frac{1}{2}\langle n_{1}n_{2}|t|n_{1}'n_{2}'\rangle\rho_{n_{1}n_{2}n_{1}'n_{2}'}^{\mathcal{NN}'}
 }
\end{equation}

\subsubsection*{Between neutral and channel function}
The two particle operator between a neutral and a channel function can be computed as:
\begin{eqnarray*}
\frac{1}{\sqrt{N}} \langle\mathcal{N}|\hat{T}\mathcal{A}|J\beta\rangle & = &  \sqrt{N} \langle\mathcal{N}|\hat{T}|J\beta\rangle\\
 & = & \sqrt{N} \cdot \sum_{p=1}^{N}\sum_{q=p+1}^{N}\langle\mathcal{N}|t_{pq}|B_{N}\rangle \\ 
 & = & \sqrt{N} \cdot \left[ \sum_{p=1}^{N-1} \langle\mathcal{N}|t_{pN}|B_{N}\rangle+\sum_{p=1}^{N-1}\sum_{q=p+1}^{N-1}\langle\mathcal{N}|t_{pq}|B_{N}\rangle \right]\\
 & = & \sqrt{N}(N-1)\langle\mathcal{N}|t_{1N}|B_{N}\rangle+\frac{\sqrt{N}(N-1)(N-2)}{2}\langle\mathcal{N}|t_{12}|B_{N}\rangle
\end{eqnarray*}
Evaluating term-wise:
\\

\noindent \underline{Term 1:}

\begin{eqnarray*}
\langle\mathcal{N}|t_{1N}|B_{n}\rangle & = & \sum_{n_{1}\cdots n_{N}}\sum_{j_{1}\cdots j_{N-1}}\langle n_{1}\cdots n_{N}|t_{1N}|j_{1}\cdots j_{N-1} \beta \rangle d_{n_{1}\cdots n_{N}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{n_{1}\cdots n_{N}}\sum_{j_{1}}\langle n_{1}n_{N}|t_{12}|j_{1} \beta \rangle d_{n_{1}\cdots n_{N}}^{*}b_{j_{1}n_{2}\cdots n_{N-1}}\\
\text{[Reordering indices]} & = & \sum_{n_{1}\cdots n_{N}}\sum_{j_{1}}\langle n_{1}n_{2}|t_{12}|j_{1} \beta\rangle d_{n_{2}\cdots n_{N-1}n_{1}n_{N}}^{*}b_{n_{2}\cdots n_{N-1}j_{1}}\\
\text{[Renaming indices]} & = & \sum_{n_{1}\cdots n_{N}}\sum_{j_{1}}\langle n_{2}n_{1}|t_{12}|j_{1}\beta\rangle d_{n_{3}\cdots n_{N}n_{1}n_{2}}^{*}b_{n_{3}\cdots n_{N}j_{1}}\\
 & = & \sum_{n_{1}n_{2}j_{1}}\langle n_{1}n_{2}|t|j_{1}\beta\rangle\eta_{n_{1}n_{2}j_{1}}^{\mathcal{N}J}
 / \left[ \sqrt{N} (N-1) \right]
\end{eqnarray*}


\noindent \underline{Term 2:}

\begin{eqnarray*}
\langle\mathcal{N}|t_{12}|B_{N}\rangle & = & \sum_{n_{1}\cdots n_{N}}\sum_{j_{1}\cdots j_{N-1}}\langle n_{1}\cdots n_{N}|t_{12}|j_{1}\cdots j_{N-1}\beta\rangle d_{n_{1}\cdots n_{N}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{n_{1}\cdots n_{N}}\sum_{j_{1}j_{2}}\langle n_{N}|\beta\rangle\langle n_{1}n_{2}|t_{12}|j_{1}j_{2}\rangle d_{n_{1}\cdots n_{N}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{n_{1}n_{2}n_{3}j_{1}j_{2}}\langle n_{1}n_{2}|t|j_{1}j_{2}\rangle\langle n_{3}|\beta\rangle\eta_{n_{1}n_{2}n_{3}j_{1}j_{2}}^{\mathcal{N}J}
 / \left[ \sqrt{N} (N-1) (N-2) \right]
\end{eqnarray*}
The overall matrix element looks as:
\beq
\boxed{
\frac{1}{\sqrt{N}} \langle\mathcal{N}|\hat{T}\mathcal{A}|J\beta\rangle=\langle n_{1}n_{2}|t|j_{1}\beta\rangle\eta_{n_{1}n_{2}j_{1}}^{\mathcal{N}J}+\frac{1}{2}\langle n_{1}n_{2}|t|j_{1}j_{2}\rangle\langle n_{3}|\beta\rangle\eta_{n_{1}n_{2}n_{3}j_{1}j_{2}}^{\mathcal{N}J}
}
\eeq

\subsubsection*{Between channel functions}

The two particle matrix elements between two ionic channel functions can be evaluated as:
\begin{eqnarray*}
& & \frac{1}{N} \langle I\alpha|\mathcal{A}\hat{T}\mathcal{A}|J\beta\rangle = \langle I\alpha|\hat{T}\mathcal{A}|J\beta\rangle \\
& = & \sum_{k=1}^{N}\sum_{m=1}^{N}\sum_{n=m+1}^{N}\langle A_{N}|t_{mn}|B_{k}\rangle\\
 & = & \sum_{m=1}^{N}\sum_{n=m+1}^{N}\langle A_{N}|t_{mn}|B_{N}\rangle+\sum_{k=1}^{N-1}\sum_{m=1}^{N}\sum_{n=m+1}^{N}\langle A_{N}|t_{mn}|B_{k}\rangle\\
 & = & \sum_{m=1}^{N-1}\sum_{n=m+1}^{N-1}\langle A_{N}|t_{mn}|B_{N}\rangle+\sum_{m=1}^{N-1}\langle A_{N}|t_{mN}|B_{N}\rangle+\sum_{k=1}^{N-1}\sum_{m=1}^{N-1}\langle A_{N}|t_{mN}|B_{k}\rangle+\sum_{k=1}^{N-1}\sum_{m=1}^{N-1}\sum_{n=m+1}^{N-1}\langle A_{N}|t_{mn}|B_{k}\rangle\\
 & = & \sum_{m=1}^{N-1}\sum_{n=m+1}^{N-1}\langle A_{N}|t_{mn}|B_{N}\rangle+\sum_{m=1}^{N-1}\langle A_{N}|t_{mN}|B_{N}\rangle+\sum_{k=1}^{N-1}\langle A_{N}|t_{kN}|B_{k}\rangle+\sum_{k=1}^{N-1}\sum_{m=1,m\neq k}^{N-1}\langle A_{N}|t_{mN}|B_{k}\rangle\\
 &  & +\sum_{m=1}^{N-1}\sum_{n=m+1}^{N-1}\langle A_{N}|t_{mn}|B_{n}\rangle+\sum_{m=1}^{N-1}\sum_{n=m+1}^{N-1}\langle A_{N}|t_{mn}|B_{m}\rangle+\sum_{m=1}^{N-1}\sum_{n=m+1}^{N-1}\sum_{k=1,k\neq m,n}^{N-1}\langle A_{N}|t_{mn}|B_{k}\rangle\\
 & = & \frac{(N-1)(N-2)}{2}\langle A_{N}|t_{12}|B_{N}\rangle+(N-1)\langle A_{N}|t_{1N}|B_{N}\rangle+(N-1)\langle A_{N}|t_{1N}|B_{1}\rangle \\
 &  & +(N-1)(N-2)\langle A_{N}|t_{2N}|B_{1}\rangle +\frac{(N-1)(N-2)}{2}\langle A_{N}|t_{12}|B_{2}\rangle \\
 & &+\frac{(N-1)(N-2)}{2}\langle A_{N}|t_{12}|B_{1}\rangle+\frac{(N-1)(N-2)(N-3)}{2}\langle A_{N}|t_{23}|B_{1}\rangle\\
 & = & \frac{(N-1)(N-2)}{2}\langle A_{N}|t_{12}|B_{N}\rangle+(N-1)\langle A_{N}|t_{1N}|B_{N}\rangle+(N-1)\langle A_{N}|t_{1N}|B_{1}\rangle \\
 & & +(N-1)(N-2)\langle A_{N}|t_{2N}|B_{1}\rangle +(N-1)(N-2)\langle A_{N}|t_{12}|B_{1}\rangle \\
 & & +\frac{(N-1)(N-2)(N-3)}{2}\langle A_{N}|t_{23}|B_{1}\rangle
\end{eqnarray*}
There are 6 terms in the above expression which can be evaluated as:
\\

\noindent \underline{Term 1:}

\[
\frac{(N-1)(N-2)}{2} \langle A_{N}|t_{12}|B_{N}\rangle=\frac{1}{2}\langle i_{1}i_{2}|t|j_{1}j_{2}\rangle\rho_{i_{1}i_{2}j_{1}j_{2}}^{IJ}\langle\alpha|\beta\rangle
\]


\noindent \underline{Term 2:}

\begin{eqnarray*}
\langle A_{N}|t_{1N}|B_{N}\rangle & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}\cdots j_{N-1}}\langle i_{1}\cdots i_{N-1}\alpha|t_{1N}|j_{1}\cdots j_{N-1}\beta\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}}\langle i_{1}\alpha|t_{1N}|j_{1}\beta\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}i_{2}\cdots i_{N-1}}\\
 & = & \sum_{ij}\langle i\alpha|t|j\beta\rangle\rho_{ij}^{IJ} / (N-1)
\end{eqnarray*}


\noindent \underline{Term 3:}

\begin{eqnarray*}
\langle A_{N}|t_{1N}|B_{1}\rangle & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}\cdots j_{N-1}}(-1)^{N-1}\langle i_{1}\cdots i_{N-1}\alpha|t_{1N}|\beta j_{1}\cdots j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{N-1}}(-1)^{N-1}\langle i_{1}\alpha|t_{1N}|\beta j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{i_{2}\cdots i_{N-1}j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{N-1}}-\langle i_{1}\alpha|t_{1N}|\beta j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{N-1}i_{2}\cdots i_{N-1}}\\
 & = & \sum_{ij}-\langle i\alpha|t|\beta j\rangle\rho_{ij}^{IJ} / (N-1)
\end{eqnarray*}


\noindent \underline{Term 4:}

\begin{eqnarray*}
\langle A_{N}|t_{2N}|B_{1}\rangle & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}\cdots j_{N-1}}(-1)^{N-1}\langle i_{1}\cdots i_{N-1}\alpha|t_{2N}|\beta j_{1}\cdots j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}j_{N-1}}(-1)^{N-1}\langle i_{1}|\beta\rangle\langle i_{2}\alpha|t_{2N}|j_{1}j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}i_{3}\cdots i_{N-1}j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}j_{N-1}}-\langle i_{1}|\beta\rangle\langle i_{2}\alpha|t_{2N}|j_{1}j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{N-1}j_{1}i_{3}\cdots i_{N-1}}\\
\text{[Renaming indices]} & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}j_{2}}-\langle i_{1}|\beta\rangle\langle i_{2}\alpha|t_{2N}|j_{2}j_{1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}j_{2}i_{3}\cdots i_{N-1}}\\
 & = & \sum_{i_{1}i_{2}j_{1}j_{2}}-\langle i_{1}|\beta\rangle\langle i_{2}\alpha|t_{2N}|j_{2}j_{1}\rangle\rho_{i_{1}i_{2}j_{1}j_{2}}^{IJ}\\
 & = & \sum_{i_{1}i_{2}j_{1}j_{2}}-\langle i_{1}|\beta\rangle\langle\alpha i_{2}|t|j_{1}j_{2}\rangle\rho_{i_{1}i_{2}j_{1}j_{2}}^{IJ}
 / \left[ (N-1)(N-2) \right]
\end{eqnarray*}


\noindent \underline{Term 5:}

\begin{eqnarray*}
\langle A_{N}|t_{12}|B_{1}\rangle & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}\cdots j_{N-1}}(-1)^{N-1}\langle i_{1}\cdots i_{N-1}\alpha|t_{12}|\beta j_{1}\cdots j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}j_{N-1}}(-1)^{N-1}\langle i_{1}i_{2}|t_{12}|\beta j_{1}\rangle\langle\alpha|j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}i_{3}\cdots i_{N-1}j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}j_{N-1}}-\langle i_{1}i_{2}|t_{12}|\beta j_{1}\rangle\langle\alpha|j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{N-1}j_{1}i_{3}\cdots i_{N-1}}\\
\text{[Renaming indices]} & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}j_{2}}-\langle i_{1}i_{2}|t_{12}|\beta j_{2}\rangle\langle\alpha|j_{1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}j_{2}i_{3}\cdots i_{N-1}}\\
 & = & \sum_{i_{1}i_{2}j_{1}j_{3}}-\langle i_{1}i_{2}|t|\beta j_{2}\rangle\langle\alpha|j_{1}\rangle\rho_{i_{1}i_{2}j_{1}j_{2}}^{IJ}
 / \left[ (N-1)(N-2) \right]
\end{eqnarray*}


\noindent \underline{Term 6:}

\begin{eqnarray*}
\langle A_{N}|t_{23}|B_{1}\rangle & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}\cdots j_{N-1}}(-1)^{N-1}\langle i_{1}\cdots i_{N-1}\alpha|t_{23}|\beta j_{1}\cdots j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}\cdots j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}j_{2}j_{N-1}}(-1)^{N-1}\langle i_{1}|\beta\rangle\langle i_{2}i_{3}|t_{23}|j_{1}j_{2}\rangle\langle\alpha|j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}j_{2}i_{4}\cdots i_{N-1}j_{N-1}}\\
 & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}j_{2}j_{N-1}}-\langle i_{1}|\beta\rangle\langle i_{2}i_{3}|t_{23}|j_{1}j_{2}\rangle\langle\alpha|j_{N-1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{N-1}j_{1}j_{2}i_{4}\cdots i_{N-1}}\\
\text{[Renaming indices]} & = & \sum_{i_{1}\cdots i_{N-1}}\sum_{j_{1}j_{2}j_{3}}-\langle i_{1}|\beta\rangle\langle i_{2}i_{3}|t|j_{2}j_{3}\rangle\langle\alpha|j_{1}\rangle a_{i_{1}\cdots i_{N-1}}^{*}b_{j_{1}j_{2}j_{3}i_{4}\cdots j_{N-1}}\\
 & = & \sum_{i_{1}i_{2}i_{3}j_{1}j_{2}j_{3}}-\langle i_{1}|\beta\rangle\langle i_{2}i_{3}|t|j_{2}j_{3}\rangle\langle\alpha|j_{1}\rangle\rho_{i_{1}i_{2}i_{3}j_{1}j_{2}j_{3}}^{IJ}
    \frac{(N-4)!}{(N-1)!}
 \end{eqnarray*}


Finally, the two particle matrix elements between two ionic channel functions
look as:
\begin{empheq}[box=\widefbox]{align}
& \frac{1}{N} \langle I\alpha|\mathcal{A}\hat{T}\mathcal{A}|J\beta\rangle \nonumber \\ 
& = \frac{1}{2}\langle i_{1}i_{2}|t|j_{1}j_{2}\rangle\rho_{i_{1}i_{2}j_{1}j_{2}}^{IJ}\langle\alpha|\beta\rangle+\langle i\alpha|t|j\beta\rangle\rho_{ij}^{IJ}-\langle i\alpha|t|\beta j\rangle\rho_{ij}^{IJ} \nonumber \\
 &   -\langle i_{1}|\beta\rangle\langle\alpha i_{2}|t|j_{1}j_{2}\rangle\rho_{i_{1}i_{2}j_{1}j_{2}}^{IJ}-\langle i_{1}i_{2}|t|\beta j_{2}\rangle\langle\alpha|j_{1}\rangle\rho_{i_{1}i_{2}j_{1}j_{2}}^{IJ} \\
 &  - \frac{1}{2} \langle i_{1}|\beta\rangle\langle i_{2}i_{3}|t|j_{2}j_{3}\rangle\langle\alpha|j_{1}\rangle\rho_{i_{1}i_{2}i_{3}j_{1}j_{2}j_{3}}^{IJ}. \nonumber
\end{empheq}

\noindent {\bf Note:}
The current treatment needs upto three particle reduced density matrices. The non-orthogonality of the single electron basis, $\alpha$, with respect to the 
Hartree-Fock (HF) basis, $\phi_k$, leads to a significant complexity in the matrix elements. Explicit orthogonalization of the single electron basis with 
respect to HF orbitals is not a solution because: the ionic and neutral states are treated on the configuration interaction level and each ionic state
is composed of several Slater determinants.
