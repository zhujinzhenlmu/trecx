\section{Eigenvalue solver}

\subsection{Near-perturbative problem}

Basic idea is that we have 
\beq
\mH=\mH_0+\mV,
\eeq
where $\mH_0$ is easily invertible and dominates the spectral characterisitics,
while $\mV$ is relatively bounded, permitting perturbative expansion. Given an initial 
guess subspace $\mP\up0$ for eigenvectors of $\mH$ we want to iteratively improve on this 
by adding the perturbative corrections $\tilde{P}\up1$ and selecting a new $\mP\up1$ by some
criteria.

With the definition $\mQ\up0:=\one-\mP\up0$, $\mP\up0\mQ\up0=0$ we decompose
\beq
\mH = \underbrace{\mP\up0 \mH \mO\up0 + \mQ\up0 \mH_0 \mQ\up0}_{=:\mA}  
+ \underbrace{\mQ\up0 \mV \mQ\up0 + \mQ\up0 \mH \mP\up0  + \mP\up0 \mH \mQ\up0}_{=:\mB} 
\eeq
The perturbative correction to the $\mP\up0$ is
\begin{align}
\tilde{P}\up1=&-(\mA-\mE)\inv1 \mB \mP\up0=
-\left\{[\mP\up0(\mH-\mE)\mP\up0]\inv1 \oplus [\mQ\up0(\mH_0-\mE)\mQ\up0]\inv1\right\} \mQ\up0 H \mP\up0
=\\&- [\mQ\up0(\mH_0-\mE)\mQ\up0]\inv1 \mQ\up0 H \mP\up0
\end{align}
Clearly, $\tilde{P}\up0\mP\up0=0$.

A simple choice is $\mE=E\up0\one$, where $E\up0$ would be the eigenvalue of $\mP\up0 H \mP\up0$ closests to the target
value, the eigenvalue corresponding to the eigenvector with maximal overlap with the target state etc.
We should be able to use a diagonal matrix for $\mE$ with diagonal values $E_i$ corresponding to several 
states most similar to target states etc. 

Using the projection formula for the inverse with $P\up0=\mC_0\mC_0^\dag$, we can write
\beq
\tilde{C}\up1=\left\{
(\mH_0-\mE)\inv1-(\mH_0-\mE)\inv1 \mC_0 \left[\mC_0^\dag (\mH_0-\mE)\inv1 \mC_0\right]\inv1 \mC_0^\dag (\mH_0-\mE)\inv1 
\right\}(1-\mC_0\mC_0^\dag) \mH \mC_0
\eeq
A new subspace $\mC\up1$ is selected from $\mC\up0\oplus \tilde{C}\up1$ and the procedure is
iterated, hopefully to convergence.  

For complex symmetric (instead of hermitian) problems this generalizes by the replacement $\dag\to T$.

\subsubsection{Formulation for general overlap $\mS$}

Formaly, for non-identiy overlap we expect the following expression, keeping the original projectors
$\mP=CC^\dag$:
\beq
\tilde{C}\up1 = \left[(H_0-SE)\inv1-(H_0-SE)\inv1 S C[C^\dag S (H_0-SE_0)\inv1 S C]\inv1 C^\dag S (H_0-SE)\inv1 \right](1-SCC^\dag) H C
\eeq
where the $C$ fulfill 
\beq
SC C^\dag H C = S C E.
\eeq
If the $C$ are exact eigenvectors of $H: HC=SCE$, the $(1-SCC^\dag) H C=0$.

\subsubsection{Implementation}
A single update $\mC\up0\to\mC\up1$ involves the following steps:
\begin{enumerate}
\item Initialize: get $C$, compute $K=C^\dag H C$.
\item Compute $D=(H_0-SE_0)\inv1 S C$
\item Compute $M=C^\dag S (H_0-SE_0)\inv1 S C$ and its inverse $M\inv1$
\item Get $E = HC-SC\,C^\dag H C = HC-SC K$
\item Solve $F= (H_0-SE_0)\inv1 (HC-SC C^\dag H C) =  (H_0-SE_0)\inv1 E$
\item Compute $G=[C^\dag S (H_0-SE_0)\inv1 S C]\inv1 C^\dag S F = M\inv1 C^\dag S F$
\item Add $F-=D G$. 
\item Diagonalize $H$ \wrt $F\oplus C$, select $C$ from suitable subspace and iterate.
\end{enumerate}

\subsubsection{Simplified version}
Pragmatically, it may well be that a simplified version of the approach,
where the projection for the field-free inverse is omitted, may produce
very similar results at much lower cost (we keep the projecting subspace fixed
say at some decent subset of the eigenstates of $H_0$:
\beq
\tilde{C}\up1 = (H_0-SE_0)\inv1(1-SCC^\dag) H C
\eeq
We refomulate by using
\beq
(H_0-SE_0)\inv1 (H_0+V) C = 1+(H_0-E_0)\inv1(E_0+V) C
\eeq
as
\begin{align}
\tilde{C}\up1 
%= (H_0-SE_0)\inv1(1-SCC^\dag) (H_0+V) C
=& C + (H_0-SE_0)\inv1[(SE_0+V)C -SCC^\dag (H_0+V) C]
\\=& C + (H_0-SE_0)\inv1[VC +SCE_0-SCC^\dag (H_0+V) C)]
\end{align}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "tsurff"
%%% End: 
