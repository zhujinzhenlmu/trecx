\section{Streaking formula}

Streaking is based on the idea that one can measure ``emission times'' of electrons from matter
by imparting a boost to the emitted electron by a field that gets a hold of the electron
{\em at the instance of release}. In tratditional streaking, that ``instance of release'' is 
associated with the presence of an XUV pulse. In its original use, the purpose of streaking 
was to characterize the XUV pulse. It was always understood, that there is an ``atomic phase''
present, but it was assumed that can be computed or otherwise factored out of the measurement.

With the ever better understanding of pulses, the situation was turned around and the 
``atomic phase'' is what we want to measure. More precisely, possible energy- and 
channel dependencies of that phase, which allows to assign ``delays'' to the emission
process. 

Such an interpretation requires a few assumption to be admissible:
\begin{enumerate}
\item 
The system and its dynamics are not significantly perturbed by the presence of a laser field
\item 
The motion of the electron from the moment where it is driven by the IR field, is free, i.e. 
Coulomb scattering etc. does not matter
\end{enumerate}

Assuming the condtions for extracting atomic phases from streaking are fulfilled, it is possible
to reconstruct all delays from a singel set of tSurff surface values gained without the presence 
of an IR field. Here, we want to work this out.


\subsection{tSurff for streaking experiments}

We distinguish two radii: the radius $R_f$, beyond which we would consider the electron as free
and the tSurff radius $R_f\leq R_c$, where we actually pick up the surface flux.

The free radius $R_f$ may turn out to be a somewhat delicate object: even assuming an electron 
wave packet with energy $E_f$ to appear at some time $t_f$ in the vicinity of the center,
it will, on its way to asymptotics, experience a phase shift/delay in the atomic potential.

An example: at an energy of 6 eV (0.2 a.u), we have a momentum of 0.6 au, i.e. it will take
the electron a time of 50 au to move to about 30 au (720 as) from its postion of release. Now, from 
tSurff calculations we know that 30 au is a typical radius (somewhat depending on electron
energies) where we can consider the motion as {\em really} free. The motion from 
a (somewhat arbitrarily) chosen $R_f$ to $R_c$ will invariably mix the dynamics in the 
atomic potential with the dynamics in streak field. The intermediate potential
is not accessible to analytic treatment for molecules, and only barely so for atoms.

It is, at present, not very obvious what this means in terms of the streaking traces and 
for concluding anything about the $t_f$ from streaking traces.

We will try to test the validity of the streaking hypothesis using the tSurff technique.
For that, we will perform calculations for different $R_c$ {\em without} a streaking field.
A transformation will be applied to obtain the hypothetical surface flux at some $R_f$ 
(which may include $R_f=0$) assuming free motion from $R_f$ to $R_c$. Spectra will be 
computed from this hypothetical $R_f$, including the effect of streaking.

We may compute the ``Wigner-delays'', i.e. a hypothetical delay that would be seen if the 
unbound electrons were moving in absence of the long-range Coulomb tail: we diagonalize the
field-free hamiltonian and take the solution from the surface back in time to let it pass
trhough a closer surface at $R_f$. We may consider taking the surface all the way to 0
in each partial wave. These are then propagated forward in presence of a streak field to 
accumulate into spectra, as usual. 

A brief look into literature shows that the streaking idea, as such, is barely meanigful
on the level of subtle time-delays:
there is an unavoidable intertwining of the long-range part of the potential and the laser
that manifests itself in the time-delays. We may, in some way split a more ``fundamental''
part of the multi-electron molecular Hamiltonian from a simple part, say, a single-electron
Hamiltonian, that would contribute a ``universal'' part to the time delays. To the extent
that that universal part can be considered perturbative, a separation of contributions is 
possible. In particular, from emission and delay in presence of the field we may be able to 
conclude about emission delays in absence of a field (or delays in presence of some other field).


We can justifiably assume that the XUV interaction is perturbative. We seek a partitioning
of the Hamiltonian into part where we can neglect the effect of the IR and the rest,
where the coupling occurs. Obvious canditates for the bound part are the lowest few
bound states of the investigated system, denote the projector onto that (small) subspace by $P$:
\begin{align*}
H=H_0+H_{IR}+H_{XUV}=PHP+QHQ+PHQ+QHP\approx H_0+PH_{XUV}Q+QH_{XUV}P+QH_{IR}Q.
\end{align*}
The approximation embodies the basic idea underlying any meaningful concept of streaking, which
would reveal relevant information about the XUV photo-ionization process:
there is a process that puts electrons into the ``continuum'' (subspace $Q$), but that process is 
not affected by the IR. In that continuum, further motion is governed by 
free motion in $H_0$ and by the (dipole) interation with the IR.

In fact, this is still not sufficient, IR driving in the complex hamiltionan $H_0$ will 
induce rather uncontrollable delays. In order to make the technique useful, we need to 
be able to split $H_0$ further into a $H_0=H_s+H_u$, the ``specific'' and the ``universal''
parts of the binding. For experimental use, the ``universal'' part must be a simple, well-known
interaction and it must be possible to disentangle its impact on delays in a perturbative manner.
For numerical applications, we need a $H_u$ that greatly simplifies the solution of the numerical problem
in presence of the IR.

Assuming $P$ to be an eigen-subspace of $H_0$, we have
\beq
H= P(H_s+H_u)P +Q(H_s+H_u)Q+PH_{XUV}Q+QH_{XUV}P+QH_{IR}Q.
\eeq
The idea is to treat the transition into $Q$ space perturbatively (this appears fully 
justfied), and treat the further dynamics neglecting $QH_sQ$. 
Include the initial state in $P$. In the above, we have suppressed any impact of the IR
on $P$ (this may be possible to improve). 

In the spirit of the strong field approximation, we split the dynamics into a ``source'' part
governed by $H_0+H_{XUV}$ and a ``free'' part, governed by the approximation 
$H_0+H_{IR}\approx H_u+H_{IR}$. The ``free'' part may include some channel-dynamics induced by $H_{IR}$.
\newpage
We introduce the dynamics
\begin{align}
U(t)&\text{ for }H\\
U_0(t)&\text{ for }H_0\\
U_{XUV}(t)&\text{ for }H_0+H_{XUV}\\
U_{IR}(t)&\text{ for }H_0+H_{IR}\\
U_{source}&\text{ for }H_0+PH_{IR}P\\
U_{free}(t)&\text{ for }H_u+H_{IR}\\
\end{align}
Here is the sequence of approximations, starting from the Dyson equatin \wrt the XUV interaction:
\begin{align}
\Psi(t)
&=\int d\tau  U(t-\tau)H_{XUV}(\tau)U_{IR}(\tau)\Psi(0)\\
&\approx\int d\tau  U(t-\tau)H_{XUV}(\tau)U_{source}(\tau)\Psi(0)\\
&\approx\int d\tau  U_{free}(t-\tau)H_{XUV}(\tau)U_{source}(\tau)\Psi(0)\\
\end{align}
As $P$ is rather low-dimensional, we write
\beq
U_{source}(t)\Psi(0)=\sum_n \Phi_n c_n(t)
\eeq
and compute 
\beq
\Psi_n(t)=\int_0^t d\tau\,  U_{free}(t-\tau)H_{XUV}(\tau)\Phi_n
\eeq
with 
\beq
\Psi(t)=\sum_n \Psi_n(t) c_n(t)
\eeq
The $\Psi_n$ evolve by the inhomogeneous equation
\beq\label{eq:streak}
i\ddt \Psi_n(t) = (H_u+H_{IR})\Psi_n(t) + H_{XUV}(t)\Phi_n
\eeq
Assuming that $U_{free}$ can be solved comparatively easily, on the scale of single-electron 
problems, computations can be drastically simplified.

In order for the idea to be useful at all, we must be able to neglect in $H_u$ the exchange 
terms, i.e. the ``dynamical exchange''. Exhange interaction in the initial state in in the ionic states
is kept. For $H_u$ we may be able to use some hamiltonian on the Hartree level, e.g.
\beq
(H_u)_{mn}=
\left[
-\frac12\Delta + V_{mn},
\right]
\eeq
where $V_{n}$ would be the Hartree potential of the current ionic channel. We may 
need to use the time-dependent channels, i.e.
\beq
V_{mn}(t,\vr) = \l \Phi_m(t,\vr')|\frac{1}{\vr-\vr'}| \Phi_n(t,\vr')\r,
\eeq
which, of course, is related to $t=0$ by a small $N\times N$ 
unitary transform $\mV(t)=U^\dagger(t) \mV(r,0) U(t)$.

\subsection{A scheme}

The SFA-type formula above will suffer from the problem that the XUV ionization process
remains insensitve to the possibly complex structure of the continuum. I.e.~even in absence
of the IR, the sprectra will not be necessarily correct. However, we may reconstruct the 
streaked spectra under the hypothesis, hoping that the intertwining of IR with scattering
in the potential is reasonably well captured by Eq.~(\ref{eq:streak}).

The idea would be to compute XUV spectra completely, then back-propagate without IR and forward
propagate {\em with} IR. The question that arises is just how long to back-propagate: in absence 
of the XUV there is no sink for the amplitude, i.e.\ the electrons will continue their path past
their ``place and time of birth'', thus undergoing more scattering than in the actual process. 

\newpage
\subsection{Perturbative IR}

The original RABBITT scheme was conceived as perturbative. If this holds indeed,
the solution can be written as
\begin{align}
\Psi_n(t) 
&= \int_0^t d\tau U_0 (t, \tau) H_{IR}(\tau) U(\tau,0)\Phi_n 
\\&\approx \int_0^t d\tau \exp[-it (t-\tau)H_0] H_{IR}(\tau) \int_0^\tau d\tau'U_0(\tau,\tau') H_{XUV}(\tau')\Phi_n 
\\&\approx \int_0^t d\tau \exp[-it (t-\tau)H_0] \vd\cdot\vEf_{IR}(\tau) \int_0^\tau d\tau'\exp[-it (\tau-\tau')H_0]  H_{XUV}(\tau')\Phi_n 
\end{align}
In a spectral representation of $H_0$ we write
\begin{align}
\Psi_n(t)
&=
\int dE d\al \int dE' d\al' |E,\al\r\l E',\al'| \int_{-\infty}^t d\tau \int_{-\infty}^\tau d\tau' 
e^{-it(t-\tau)E}\vd(E,E',\al,\al') \vEf_{IR}(\tau) e^{-it(\tau-\tau')E'}|\chi_n(\tau')\r
\\&=
\int dE d\al d\al' |E,\al\r\l E,\al'| \int_{-\infty}^t d\tau \int_{-\infty}^\tau d\tau' 
e^{-it(t-\tau')E}\vd(E,\al,\al') \vEf_{IR}(\tau)\chi_n(\tau')
\\&=
\int dE d\al d\al' |E,\al\r\l E,\al| \int_{-\infty}^t d\tau \int_{-\infty}^\tau d\tau' 
e^{-it(t-\tau')E}\vd(E,\al,\al') \vEf_{IR} (\tau)\chi_n(\tau',E,\al')\r.
\end{align}
Considering that 
\beq
\chi_n(\tau',E,\al')=\l E,\al'|\vd|\Phi_n \r \cdot \vEf_{XUV}(\tau')
\eeq
we can compute the time time-integrals and take the limit $t\to\infty$ (with fields in $z$-direction:
\beq
Q(E,\al,\al')\lim_{t\to\infty}\int_{-\infty}^t d\tau \int_{-\infty}^\tau d\tau' 
e^{-it(t-\tau')E} \vEf_{IR}(\tau)\vEf_{XUV}(\tau').
\eeq
That is
\begin{align}
\Psi_n(\infty)
&=
\int dE d\al d\al' |E,\al\r Q(E,\al,\al')d_z(E,\al,\al') \l E,\al'|d_z|\Phi_n\r
\end{align}
In fact, we are not interested in $\Psi_n(\infty)$ but rather in $\l E,\al|\Psi_n(\infty)\r$,
i.e.
\beq
f(E,\al)=\int d\al' Q(E,\al,\al')d_z(E,\al,\al')\l E,\al'|d_z|\Phi_n\r.
\eeq


\subsection{Computing spectral amplitudes}

The tSurff idea may be used to compute spectral amplitudes. For that we observe that,
given the exsistence of scattering theory, energy and asymptotic momentum of a state
$|E,\al\r$ translates into the properties of plane waves:
\beq
\l \vr | E,\al\r \sim \exp(i\vk\vr)\otimes\chi_\al(\vr)
\eeq
Given the time-evolution of emission from some source $\phi\r$ ($=d_z|\Phi(n)\r$ in our application)
we consider
\beq
\Phi(t)=\int dE d\al |E,\al\r e^{-iEt}\l E, \al|\phi\r = \exp(-itH_0)|\phi\r.
\eeq
For sufficiently large times, the amplitude beyond a radius $R_c$ will dominate the 
the total amplitude such that
\begin{align}
\l E,\al,T|\Phi(T)\r
&\approx\l E,\al,T|\th(R_c)|\Phi(T)\r =
\\ &=\int_0^T dt  \ddt \l E,\al|e^{itH_0}\th(R_c)e^{-itH_0}|\phi\r
\\ &=\int_0^T dt  \l E,\al,t|[iH_0,\th(R_c)]|\Phi(t)\r,
\end{align}
i.e. exactly the tSurff expression. However, there is a problem: we do not know the phase shifts
of $|E,\al\r$ as a function of $E$. While for spectra this does not matter, it is or may be
important for further integration over $E$.

We may compute $d_z(E,\al,\al')$ by the same scheme, but that exposes the 
necessity to know the relative phase shifts for $\al\neq\al'$. One way or another,
for connecting the IR streaking we need sufficient scattering information of the 
field-free problem, i.e. solve the single-electron field free scattering for our system.
This may not be an easy task.


\subsection{What are the dominant scattering contributions}

In order to determine the degree of intertwining of IR scattering with field-free emission,
we should perform some simple studies in 1d or 3d spherical symmetry for various potentials.
In this case, we try to disentangle IR scattering from XUV ionization by using different
$H_u$ in the above scheme, including the full Hamiltonian.

This can be separated into two questions:
\begin{enumerate}
\item Can we approximate the propagation after (perturbative) XUV ionization by some $H_u$, Eq.~(\ref{eq:streak})?
\item What are the limits for treating IR as perturbative?
\end{enumerate}
The first question is the more important one.

We can proceed as follows:
\begin{itemize}
\item Choose $H_0$ and get the initial bound state $\Phi_n$.
\item Get streaking spectra for various choices of $H_u$
\item Study absolute and relative delays. Where do they agree.
\end{itemize}

In tRecX, this amounts to implementing a new source term, then produce lots of spectra.
The source term is defined by an eigenvalue problem for a given ``source'' Hamiltonian.
\begin{verbatim}
Source: hInitial, nInitial, coupling
\end{verbatim}
The XUV pulse can be defined by some misuse of notation as \verb"iLaserA0[t]" or so.

\subsection{RABBITT}

If the basic assumption of RABBITT is fulfilled, i.e. that the interaction $H_{IR}(t)=\cos(\om_{IR}t+\Delta)\vEf\cdot\vr$
can be treated perturbatively, the emitted wave packet can be reconstructed from only two calcuations.
Observing
\beq
\cos(\om_{IR}t+\Delta)=\cos{\Delta}\cos{\om_{IR}t}-\sin{\Delta}\sin{\om_{IR}t}
\eeq
We see that the amplitude for any delay will be the linear combination of the amplitudes for $\sin$ and $\cos$.
Perturbative is required as only then the amplidudes simply add up, e.g. in time-dependent perturbation theory,
first order:
\begin{align}
\Psi(t) 
&= 
\int_{-\infty}^t d\tau U_0(t-\tau) 
\left[\cos{\Delta}\cos{\om_{IR}\tau}-\sin{\Delta}\sin{\om_{IR}\tau}\right](\vEf\cdot\vr) U_0(\tau) \Phi_{XUV}(\tau)
\\ &=
\cos{\Delta}\int_{-\infty}^t d\tau U_0(t-\tau)\cos{\om_{IR}\tau}(\vEf\cdot\vr) U_0(\tau) \Phi_{XUV}(\tau)
\\&-\sin{\Delta}\int_{-\infty}^t d\tau U_0(t-\tau)\sin{\om_{IR}\tau}(\vEf\cdot\vr) U_0(\tau) \Phi_{XUV}(\tau).
\end{align}
We need 1st order perturbation theory, as 
the splitting would not be possible, if instead of the last $U_0(\tau)$ we had the full propagation
by the pulse-shape dependent $U(\tau)$.

By similar arguments, we only need two orientations, e.g. parallel and perpendicular, assuming that
IR and XUV are parallel (if not parallel at most two more calculations are needed). Of course, we
need that XUV is perturbative, but that is clear. We put the corresponding spectral amplitudes to 
disk and combine them at will for any beautiful figure.

For arbitrary orientation we need to take into consideration that both, the XUV and IR need 
to be available at all orientiations, i.e. parallel and perpendicular component of the source needs
to be streaked parallel and perpendicular, giving a total of 4 calculations.
We assume linear polarization and denote the interactions
as $\vEf_{IR}\cdot\vr=\Ef_{IR}(\cos\al z + \sin\al x)$ and $\Phi_{XUV}(\tau)=\cos\be\Phi\up{z}_{XUV}(\tau)+\sin\be\Phi\up{x}_{XUV}(\tau)$,
and
\beq
\Psi\up{ab}(t)=\int_{-\infty}^t d\tau U_0(t-\tau)\cos{\om_{IR}\tau}\Ef a U_0(\tau) \Phi\up{b}_{XUV}(\tau),\qquad a,b=x,z
\eeq
then for arbitrary relative polarizations of the fields
\beq
\Psi(t)=\cos\al\cos\be \Psi\up{zz}(t)+\sin\al\cos\be \Psi\up{xz}(t)+\cos\al\sin\be \Psi\up{zx}(t)+\sin\al\sin\be \Psi\up{xx}(t).
\eeq

\subsection{Numerical evidence}

H-atom with laser
\begin{verbatim}
Laser: shape, I(W/cm2), FWHM, lambda(nm),peak,phiCEO
cos2, 1.e10, 12 OptCyc, 800.,0.,pi/4
cos2, 1.e12, 4 OptCyc, 800./9,0.,pi/2
cos2, 1.e12, 4 OptCyc, 800./11,0.,pi/2
cos2, 1.e12, 4 OptCyc, 800./13,0.,pi/2
cos2, 1.e12, 4 OptCyc, 800./15,0.,pi/2
cos2, 1.e12, 4 OptCyc, 800./17,0.,pi/2
cos2, 1.e12, 4 OptCyc, 800./19,0.,pi/2
cos2, 1.e12, 4 OptCyc, 800./21,0.,pi/2
\end{verbatim}
and varying intensities from IR/XUV=1e11/1e13 to 1e9/1e11, using 3 and 6 angular momenta on each: 
\begin{figure}[h]
\includegraphics[width=12cm]{rabitt_l6vs3}
\caption{3 vs.\ 6 angular momenta at three different intensities: all but the highest intensity spectra
agree, maximum angular momentum 2 as required by
the perturbative photon picture suffices.}
\end{figure} 
Also, using 4 or 12 IR cycles at IR=1e12 W/cm2:
\begin{figure}[h]
\includegraphics[width=12cm]{rabitt_ir4vs12cyc}
\caption{IR pulse duration 12 vs 4 cycles: effects are negligable}
\end{figure} 

\subsubsection{Reconstruction of side-bands}
\begin{figure}[h]
\includegraphics[width=12cm]{rabitt_reconstruct}
\caption{Perturbative reconstruction of phase shift $\pi/4$ from two calculations 
at shifts 0 and $\pi/2$: errors at side-band peaks are negligible. Reconstruction is not applicable for main peaks.}
\end{figure} 



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "Streak"
%%% End: 
