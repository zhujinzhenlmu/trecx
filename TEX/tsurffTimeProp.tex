
\section{Lanczos algorithm}
{\bf 2012 legacy notes}

Starting from an arbitrary vector $w_0$ it generates an orthonormal
basis in the Krylov-space ${\rm span}\{w_0,Aw_0,A^2w_0,\ldots\}$.
\\
$w_0$\ldots arbitrary, $w_{=1}=0$, $\beta_0=1$
\begin{eqnarray*}
\alpha_{j+1}&=&\l w_j | A w_j \r \\
\beta_{j+1}&=&||  A w_j - \beta_j w_{j-1} - \alpha_{j+1} w_j || \\
w_{j+1}&=& \frac{1}{b_{j+1}} (A w_j - \beta_j w_{j-1} - \alpha_{j+1} w_j)
\end{eqnarray*}
The basis has the properties
\begin{eqnarray*}
(a)\qquad &&\l w_j | w_j \r =1 \\
(b)\qquad &&\l w_j | w_{j+1} \r =0 \\
(c)\qquad &&Aw_j = \beta_j w_{j-1} + \alpha_{j+1} w_j + \beta_{j+1} w_{j+1}
\end{eqnarray*}
The main numerical problem is that the orthogonality between $w_j$'s
is lost.  An (expensive, brute force) way of fixing this is to
orthogonalize each new $w_j$ to all its predecessors.

\subsection{Exponentiation by Lanczos}

We approximate the exponential of a hermitian linear operator $A$ on
vector $b$
\begin{equation}
c(t) = \exp( i t A)b
\end{equation}
by writing
\begin{equation}
c(t) \approx c_m(t)= W_m exp[i t T_m ] W^*_m b 
\end{equation}
where $T_m$ is the Lanczos matrix of dimension $m$ and the
corresponding vectors are $W_m=(w_0,w_1,\ldots,w_{m-1})$.  $T_m$ is
diagonalized by the matrix of its eigenvectors $Q_m$
\begin{equation}
T_m = Q_m l_m Q^*_m,
\end{equation}
where $l_m$ is the diagonal matrix of the eigenvalues of $T_m$.  We
consider the expansion as converged when
\begin{equation}
||c_m(t)-c_{m-1}(t)||<{\rm tolerance}
\end{equation}
Since, ideally, the Lanczos vectors are orthogonal $W_m W_m^*=1$, we
do not need to calculate the full $c_m$ and $c_{m-1}$, but can
calculate the norm of its coefficients with respect to the Lanczos
vectors. This makes the convergence estimate very cheap, since the
number of Lanczos vectors will be much smaller than their dimension.
\\
{\bf Note:} the loss of orthogonality of the Lanczos vectors $W_m$
does not seem to affect the procedure, as was verified using random
matrices and random vectors of dimension up to 1000 and up to 50
Lanczos vectors.

\subsection{Combination of Lanczos and Runge-Kutta}

Suppose we have an ordinary differential equation (ODE)
\begin{equation}
\frac{d}{dt} \psi = g(\psi,t)
\end{equation}
and there is a linear ODE that approximates it on an interval
$[t_0,t_1]$
\begin{equation}
\frac{d}{dt} \phi = -i H_0 \phi
\end{equation}
with hermitian (actually: self-adjoint) $H_0$. The solution is given
by the unitary operator
\begin{equation}
\phi(t) = U(t-t_0) \phi(t_0),\qquad
U(t-t_0):=\exp[-i(t-t_0)H_0],\quad
\frac{d}{dt}U(t-t_0)=-iH_0U(t-t_0) 
\end{equation}
We can now write
\begin{equation}
\psi(t)=U(t-t_0)\chi(t), \qquad \chi(t_0)=\psi(t_0).
\end{equation}
The better the full evolution of $\psi$ is approximated by its
linearized version, the smaller the changes of $\chi(t)$. The
differential equation for $\chi(t)$ is
\begin{equation}
\frac{d}{dt}\chi(t)=
U(t_0-t)\left\{g[U(t-t_0)\chi(t)]+iH_0U(t-t_0)\chi(t)\right\}
\end{equation}
Hoping that $\chi$ varies little and slowly, one can integrate its
differential equation by standard techniques.
\\
\\
{\bf Choice of $H_0$:} For a simple case of the time-dependent
Schr\"odinger equation $g(\psi,t)=H(t)\psi$, an obvious choice is
\begin{equation}
H_0=H[(t_0+t_1)/2].
\end{equation} 
For the MCTDHF one could write
\begin{equation}
H_0:=H[\psi(t_0),(t_0+t_1)/2]
\end{equation}

\noindent
{\bf Performance:} Depends strongly on the relation between the
time-derivative of the derivative function $g$ and its constant part.
For the TDSE of a two-electron system in a laser field, the number of
Runge-Kutta-Lanczos vs. Runge-Kutta was reduced by a factor between 3
and 1 for our typical parameter settings.  This is quite
disappointing, since of course each step is much more expensive with
the Lanczos. It does not seem to be a sensible method for TDSE. That
may be different for MCTDHF, where operator updates take all the time.

\subsection{The Magnus integrator}

According to Lubich: Integrators for Quantum Dynamics, a 4th order
approximation to the TDSE integral is given by the exponential
\begin{equation}
\Psi(t_n+h)\approx 
\exp{-i(\tilde{B}_0 - [\tilde{B}_0,\tilde{B}_1])}\Psi(t_n),
\end{equation}
where the $\tilde{B}_k$ are defined by
\begin{equation}
\tilde{B}_k=h\int_{-1/2}^{1/2} (-iy)^{k} H[t_n+h(\frac{1}{2}+y)] dy
\end{equation}
Comparing to Lubich's original $B_k$, we have redefined
\begin{equation}
\tilde{B}_k=(-i)^{k-1}hB_k.
\end{equation}
By that re-definition we obtain for the exponent
\begin{equation}
-i(\tilde{B}_0 - [\tilde{B}_0,\tilde{B}_1])=hB_0-h^2[B_0,B_1]
\end{equation}
The lhs operator $(\ldots)$ is Hermitian and its prefactor is $-i$,
which is both assumed by our Lanczos exponentiator.
\\
\\
{\bf Some results of numerical experiments:}\\
Using a random full matrix of dimension 100 x 100 and a random initial
vector, a comparison between Runge-Kutta and the Magnus procedure was
performed. The time-dependence of the matrix was trivially $H(t)=A + t
B$ where the matrix elements of A and B have random values in the
range [-0.5,0.5].  Time integration was made for t=[0,10]. Step size
was controlled by a two single/one double step comparison.
\begin{itemize}
\item Given a fixed tolerance parameter for the two-step procedure,
  Magnus gives the more accurate results. The reason may be, that
  Magnus makes fewer steps and error accumulation is less.
\item At tolerance(magnus)=20 tolerance(runge-kutta) final accuracies
  are comparable.
\item Accuracy for the exponentiation can be chosen the same as for
  the two-step control without further loss of total accuracy.  with
  fewer Magnus steps.
\end{itemize}


\subsection{Error estimates for time-dependent Hamiltonians}
Assuming that we have an efficient exponentiator for {\em time-constant} linear operators,
we estimate the error by comparing three steps that are arranged symmetrically around 
the center of the time step $t_c=t+h/2$, with the time-step sequence
\beq
[t,t_c-h_c],[t_c-h_c,t_c+h_c],[t_c+h_c,t+h]
\eeq
Exponentiate $\mH(t_i), i=-,0,+$ for the tree times at the center of the respective intervals and 
denote the respective unitary operators as $U_i$.
Choose the center interval size small $2h_c<h/3$. 
Error estimate by computing
\beq
\vc(t+h)=\mU_1\mU_0\mU_{-1}\vc(t)
\eeq
and comparing forward propagation to $t\to t_0$ with the backward propagation $t+h\to t_0$:
\beq
\vb_-=\exp[-i\frac{h}{2}\mH(t_{-})]\vc(t)\text{ with }\vb_+=\exp[i\frac{h}{2}\mH(t_{+})]\vc(t+h)
\eeq
For the exponentiations, no extra Krylov vectors may be required, if $h_c$ is chosen sufficiently 
small. This error should be quadratic, an this is what we observe numerically. This by itself can be
used as a control for the error.

An explicit error estimate for the purpose of step-size control can be obtained as follows: write the
asymmetric step around any time $t_i$ as
\beq
\mU(t,k,j)\vc(t-k/2)=\vc(t+k/2+j/2)+\ve k^3 +\vd j^2,
\eeq
i.e. we assume that the asymmetry introduces a quadratic error. The quadratic error is assumed to change
sign when the asymmetry changes sign upon back-propagation
\beq
\mU^*(t,k,j)\vc(t+k/2+j/2)=\vc(t-k/2)+\ve k^3 -\vd j^2,
\eeq

We need to include a linear dependence of $\vd$ on $t$ in the form
\beq
\vd(t+h/2+x)=\vd_0+x\vd_1.
\eeq 
For estimating our error, we need to determine $\ve,\vd_0,\vd_1$ from three calculations.

We define our basic step as composed of three symmetric steps $h=k+j+k$
\beq
\vc_1=\mU(t+j+k/2,k,0)\mU(t+h/2,j,0)\mU(t+k/2,k,0)\vc(t)
\eeq
The error is
\bea
\vc_1&=&\mU(t+j+k/2,k,0)\mU(t+h/2,j,0)[\vc(t+k)+\ve k^3]=\ldots 
\\&=&
\vc(t+k+j+k)+\ve (2k^3+j^3)
\eea
We compute approximation at $t+h/2$ in three ways. First as
\bea
\vc_m&=&\mU(t+h/2,j,-j)\mU(t+k/2,k,0)\vc(t)=\mU(t+h/2,j,-j)[\vc(t+k)+\ve k^3]
\\&=&
\vc(t+h/2)+\ve (k^3+j^3) - \vd_0 j^2. 
\eea
Here, the $\vd_1$ term does not appear as we are at $t+h/2$, i.e. $x=0$.
Second as
\bea
\vc_r&=&\mU(t+k/2,k,j)\vc(t)
=
\vc(t+h/2)+\ve k^3 + \vd(t+k/2) j^2 
\\&=&
\vc(t+h/2)+\ve k^3 + \vd_0 j^2 - \vd_1 (k/2+j/2)j^2 
\eea
and third 
\bea
\vc_l&=&\mU(t+k+j+k/2,0,-j)\mU(t+h/2,j,0)\mU(t+k/2,k,0)\vc(t)
\\&=&
\mU(t+k+j+k/2,0,-j)[\vc(t+h/2)+\ve (k^3+j^3)]
\\&=&
\vc(t+h/2)+\ve (k^3+j^3)+\vd j^2 
\\&=&
\vc(t+h/2)+\ve (k^3+j^3)+\vd_0 j^2 + \vd_1(k/2+j/2)j^2
\eea

We form the differences
\bea
\va_{lr}&=&\ve j^3+\vd_1(k+j)j^2
\\
\va_{lm}&=&2\vd_0 j^2+\vd_1(k/2+j/2)j^2
\\
\va_{rm}&=&-\ve j^3\ldots
\eea
From which we can compute the error vectors and obtain the error estimates.

The advantage is that in Krylov methods all calculations can be done using the 
three Krylov subspaces generated from the middle points of the intervals. Here it
is assumed that the exponentiations are ``exact'', which is easily achievable 
if the center $j$-interval is small at virtually no extra cost for extrapolating the 
side $k$-intervals.

\subsubsection{Practical experience}
\begin{itemize}
\item 
For norm-conservation, truncation of the operator must be used with extreme caution.
Very easily, the resulting Arnoldi matrix violates hermiticity (in the unscaled case) and 
norm is lost, if that matters
\item
It is important to obtain Krylov vectors that are orthogonal in the sense of the main 
Hilbert space's scalar product, i.e. use the proper scalar product
\item
When starting from an eigenstate, finding an invariant subspace is highly likely and 
must be handled properly.
\item 
At naive application to time-dependen complex scaled problems, the SIA is instable in spite of
the fact that the Krylov exponentiation seems to converge decently. This remains to be 
investigated.
\item 
Low consistency order in the field is a problem for realistic fields as the step-size becomes field-driven
rather than stiffness dominated.
\end{itemize}

\subsection{The Arnoldi-RK4 approach}
In an attempt to take the best of two worlds and (hopefully) avoid the instability issues, we revive 
the old idea of combining exponentiation with RK4, using the matrix exponential as a preconditioner
for RK4 over small time intervals.

We write our solution as
\beq
\Psi(t)=\mU_c(t)\Phi(t),
\eeq
where
\beq
i\ddt \mU_c(t) = \mH(t_c)\mU_c(t),\quad \mU_c(t_0)=\one
\eeq
In the TDSE we have
\bea
i\ddt [\mU_c(t)\Phi(t)]&=& \mU_c(t)[\mH(t_c)+i\ddt\Phi(t)]
=\mH(t)\mU_c(t)\Phi(t)
\\&=& [\mH(t_c)+\mH(t)-\mH(t_c)][\mU_c(t)\Phi(t)]
\eea
and therefore
\beq
i\ddt\Phi(t)=\mU_c(t)\inv1[\mH(t)-\mH(t_c)]\mU_c(t)\Phi(t)=\mW(t;t_c)\Phi(t)
\eeq

We time-step $\Phi(t_0)$ in $[t_0,t_0+h]$ using RK4 and $t_c=t_0+h/2$ to obtain
\beq
\Psi(t_0+h)\approx\Psi_h = U_c(t_0+h)\Phi_h.
\eeq

\newpage
As RK4 has two of its support vectors at $t_i=t_c$ where $\mW(t_c,t_c)=0$.
We abbreviate 
\bea
\mU_h&:=&\mU_c(t_0+h)\\
\Psi_0&:=&\Psi(t_0)\\
\Phi_0&:=&\Phi(t_0)=\Psi_0
\eea
as well as  $\mH(t)=\mH_0$, $\mH(t_c)=\mH_c$, and $\mH(t_0+h)=\mH_h$.

\bea
k_0&=&\Phi_0= \Psi_0\\
k_1&=&f(t,\Phi_0)=-i[\mH_0-\mH_c]\Psi_0\\
k_2&=&f(t+\frac{h}{2},\Phi_0+\frac{h}{2}k_1)=0\\
k_3&=&f(t+\frac{h}{2},\Phi_0+\frac{h}{2}k_2)=0\\
k_4&=&f(t+h,\Phi_0)=-i\mU_h\inv1[\mH_h-\mH_c]\mU_h\Psi_0
\eea
and
\bea
\Phi_h&=&\Phi_0+\frac{h}{6} (k_1 + k_4)
\\&=&
\Phi_0-i\frac{h}{6}\left[
(\mH_0-\mH_c)\Psi_0+\mU_h\inv1[\mH_h-\mH_c]\mU_h \Psi_0
\right]
\eea
\bea
\Psi_h&=& \mU_h\Phi_h
\\&=&
\mU_h\left[
[1-i\frac{h}{6}[\mH_0-\mH_c]-i\frac{h}{6}\mU_h\inv1[\mH_h-\mH_c]\mU_h
\right]\Psi_0
\\&=&\left[
\mU_h-i\frac{h}{6}[\mU_h\mH_0-\mU_h\mH_c]-i\frac{h}{6}[\mH_h-\mH_c]\mU_h
\right]\Psi_0
\\&=&[(1+i\frac{h}{3}\mH_c)\mU_h-i\frac{h}{6}(\mU_h\mH_0+\mH_h\mU_h)]\Psi_0
\eea

\subsubsection{Scheme}
\bea
\Psi_h&=&-i\frac{h}{6}\mH_0\Psi_0\\
\Psi_h&=&\mU_h\Psi_h\\
\Psi_c&=&\mU_h\Psi_0\\
\Psi_h&+=&\Psi_c\\
\Psi_h&+=&i\frac{h}{3}\mH_c\Psi_c\\
\Psi_h&+=&-i\frac{h}{6}\mH_h\Psi_c\\
\eea
We may gain by calculating $\Psi_h\pm\Psi_c$, as the difference may be smaller than the individual terms.

\subsection{Highly oscillatory coefficients}
Based on the observation that neither length nor velocity gauge is sensitive to spectral cuts
we assume that it is a rather small subset of coefficients that exhibits the highly oscillatory behavior
that reduces the step size and forecloses the use of spectral constraints. If the transition were strictly 
from length to velocity, this should be a single coefficient. Unfortunately, with our continuous transition
a whole region after the switchover from length gauge may be affected.

In the DVR-version of the code, where no inverse overlap needs computation, a time-dependent sudden switchover
may be reconsidered. 

Here, we show how a few (if it is a few) coefficients can be isolated and be treated on their own time scale.
First, we separate the Hamiltonian matrix into 4 blocks $\mH_{aa}$, $\mH_{ab}=\mH_{ba}^\dagger$, $\mH_{bb}$.
For the moment we assume $\mS=\one$ for simplicity.

Then 
\bea
i\ddt \va(t)&=&\mH_{aa}(t)\va(t) + \mH_{ab}(t)\vb(t)\\
i\ddt \vb(t)&=&\mH_{ba}(t)\va(t) + \mH_{bb}(t)\vb(t)
\eea 
Let $h$ be the ``large'' step size for the complete system, i.e. the step size if it were not for the $\va$ part.
We assume that during sufficientely small intervals the evolution of 
the $\va(t)$ can be computed by keeping keeping $\vb(t_c)$ constant at some suitable intermediate time $t_c$
\beq
i\ddt \va(t)=\mH_{aa}(t)\va(t) + \mH_{ab}(t)\vb(t_c)
\eeq

The RK4 scheme for the $\vb_h$ reads
\bea
k_1&=&\mH_{ba}(t)\vc_{a}(t) + \mH_{bb}(t)\vb(t)\\
k_2&=&\mH_{ba}(t+\frac12 h)\va(t+\frac12 h) + \mH_{bb}(t+\frac12 h)k_1\\
k_3&=&\mH_{ba}(t+\frac12 h)\va(t+\frac12 h) + \mH_{bb}(t+\frac12 h)k_2\\
k_4&=&\mH_{ba}(t+ h)\va(t+h) + \mH_{bb}(t+ h)k_3\\
\eea

The values $\va(t_i)$ can be obtained by small step propagation of the inhomogeneous equation, starting
from $\va(t_{i-1})$ by  
\beq
i\ddt \va(t)=\mH_{aa}(t)\va(t) + \mH_{ab}(t)\vb((t_{i-1}+t_i)/2).
\eeq
For the source term we may use the obvious approximation
\beq
\vb((t_{i-1}+t_i)/2)\approx \vb(t),
\eeq
or, maybe a bit better
\beq
\vb((t_{i-1}+t_i)/2)\approx \vb(t_{i-1})+\frac{t_i-t}{2}[\mH_{ba}(t_i)\va(t_i)+\mH_{bb}(t_i)\vb(t)].
\eeq
For evaluating the time-dependence of the source, we can take advantage of the structure of the time-depdendence 
\beq
\mH_{ab}(t)\vb=\sum_s f_s(t) \mH_{ab}\up{s}\vb = \sum_s  f_s(t) \vh\up{s}
\eeq

If the scheme proves to be useful, there is room for improvement on the approximation of $\vb(t_i)$ by using 
the $\mH_{bb}(t+\frac12 h)]k_1$ etc. as they are being calculated. The Hamiltonians may even be evaluated at other times than at the $t_i$, 
which however involves extra operators applies on the $\vb$-component.

\subsubsection{Small fast subspace --- perturbative approximation}

Here we assume that we can identify the fast components as the eigenvectors of some operator.
We denote the corresponding projector as $\mP$ and define the operator blocks as
\bea
\mA(t)&=& \mP \mH(t) \mP\\
\mB(t)&=& (\one-\mP) \mH(t) (\one-\mP)\\
\mD(t)&=& \mP \mH(t) (\one-\mP)\\
\eea
and similar as before $\vc=\va+\vb=\mP\vc + (1-\mP)\vc$.
Time-evolution is 
\beq
i\ddt\bma \va \\ \vb \ema=
\bma \mA(t) & \mD(t) \\ \mD^\dagger(t) &\mB(t)\ema \bma \va \\ \vb \ema.
\eeq
Based on the assumption of fast and slow time scales we approximate the operator as
\beq
\mA(t)\approx \mA_0,\qquad \mD(t)=\mD(t_0).
\eeq
This is in the spirit of a perturbative approach where the time-evolution of the $\va$ is dominated by its
energies and the time-dependent coupling between the vectors of $\mP$ can be neglected.

For short time intervals we write
\beq
\mH(t)\approx \mB(t) + \mA_0 + \mD(t) + \mD(t)^\dagger
\eeq
i.e. we neglect all time-dependent interactions in the high energy block $\mA_0$.
With the ansatz $\Phi(t)=[\exp(it\mA_0)\oplus \one]\Psi(t)$ obtain the equation for the approximate 
Hamiltonian as
\beq
i\ddt \Phi(t) = [e^{it\mA_0}\mD(t)+\mD(t)^\dagger  e^{-it\mA_0} + \mB(t)]\Phi(t)
\eeq
For obtaining the derivative
\begin{enumerate}
\item $\chi_0=\mP\Phi(t)$, $\chi_1=e^{-it\mA_0}\chi_0=e^{-it\mA_0}\mP\Phi(t)$
\item $\Phi_1:=\chi_1-\chi_0+\Phi(t) = [e^{-it\mA_0}\mP+(\one-\mP)]\Phi(t)$
\item $\Phi_2:=-i\mH(t)\Phi_1+i\mA_0\chi_1=-i[(A(t)-\mA_0) e^{-it\mA_0}+\mD(t)+\mD^\dagger(t) e^{-it\mA_0}  + \mB(t)]\Phi(t)$
\item $\Phi_2\approx -i[\mD(t)+\mD^\dagger(t) e^{-it\mA_0}  + \mB(t)]\Phi(t) $
\item $\ddt\Phi(t):=[e^{it\mA_0}-1]\mP\Phi_2+\Phi_2\approx  -i[ e^{it\mA_0}\mD(t)+\mD^\dagger(t) e^{-it\mA_0}  + \mB(t)]\Phi(t)$
\end{enumerate}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "tsurff"
%%% End: 
