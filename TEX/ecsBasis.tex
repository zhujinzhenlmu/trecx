\section{Exterior Complex Scaling - Basis}

\subsection{Polar coordinates - radial functions}

Here we consider the case where we formulate our problem in polar coordinates with 
a radial basis, i.e. with the Hilbert space
\beq
\cH = L^2(dr d\cos\th d\phi, [0,\infty)\times[-1,1]\times[0,2\pi]) 
\eeq
In these coordinates the differential operators have the form using the shorthand $\cos\th=\eta$ and
writing manifestly symmetrically
\beq
-\Delta =-\ddr^2 - \frac{1}{r^2}\left[\partial_\eta (1-\eta^2)\partial_\eta-\frac{1}{1-\eta^2}\partial_\phi^2\right]
\eeq
and 
\beq
\partial_z =\ddr+\frac{1}{2r} \left[\partial_\eta(1-\eta^2)+(1-\eta^2)\partial_\eta\right]
\eeq
As here there are no complications with multi-dimensionality, the complex scaled operators on $r>R_0$ 
are simply
\beq
-\Delta_{R_0,\th}=-e^{-2i\th}\ddr^2 - \frac{1}{R(r)^2}\left[\partial_\eta (1-\eta^2)\partial_\eta-\frac{1}{1-\eta^2}\partial_\phi^2\right]
\eeq
and 
\beq
\partial_z =e^{-i\th}\ddr+\frac{1}{2R(r)}\left[(1-\eta^2)\partial_\eta+\partial_\eta(1-\eta^2)\right]
\eeq
In this representation of the Hilbert space the radial functions from the domain of the operators have a
discontinuity at $r=R_0$ of the form
\beq
u(R+0) = e^{i\th/2}u(R-0).
\eeq
A particularly straight forward way for constructing a basis $\{|i\r_{R_0,\th}\}$ that obeys this continuity is to 
take any basis  $\{|i\r\}$ that would be suitable for the non-scaled case and multiply it by $e^{i\th/2}$ for $r>R_0$.
In a finite element basis, this amounts letting an element boundary coincide with $R_0$ and 
multplying all finite element basis functions outside that  radius by $e^{i\th/2}$. With FE-DVR being 
nothing but a finite element basis with an approximate quadrature, exactly the same holds true.
As the scaling factor does not get complex conjugated, matrix elements for the complex scaled basis 
$|i\r_{R_0,\th}$ outside $R_0$ are calculated by the simple rule
\beq
\l i | O_{R_0,\th} |j\r_{R_0,\th} =  e^{i\th} \l i | O_{R_0,\th} |j\r,
\eeq
where $O_{R_0,\th}$ is any of the complex scaled operators above. In particular, this also holds
for the overlap matrix, i.e. the matrix
\beq
\l i | O_{R_0,\th} |j\r_{R_0,\th}= e^{i\th}  \l i |j\r
\eeq
will not be hermitian, rather all elemements outside $R_0$ will be complex. For the diagonal (but approximate)
FE-DVR overlap matrix, this means that all diagonal elements outside $R_0$ are complex. In fact, all elements
execept for the one that corresponds to the ``bridge function'' with collocation point at $R_0$, the factor will 
simply be $e^{i\th}$, which can be easily transforme away. Or putting it differently, we can choose to multiply
only one single function, namely the one right outside $R_0$ by $e^{i\th}$. That one single function however contains
essential information that cannot be easily transformed away. This is essential comtent of the abrupt ECS that we are using.

\subsection{Polar coordinates - non-radial functions}
By this I mean that we work in 
\beq
\cH^n = L^2(r^2dr d\cos\th d\phi, [0,\infty)\times[-1,1]\times[0,2\pi]),
\eeq
i.e. with functions
\beq
r\varphi(r)=u(r)
\eeq
The necessary formula can be derived from the above by substitution $O^n_{R_0,\th}=R(r)\inv1 O_{R_0,\th} R(r)$, trivially
acting on $\varphi(r)$ as
\beq\label{eq:nonradialOp}
R(r)\inv1 O_{R_0,\th} R(r)\varphi(r)=R(r)\inv1 O_{R_0,\th} u(r).
\eeq
In particular, we see that there will be no change in the actual matrix elements, if we expand in a basis that is
related to the previous one as
\beq
R(r)|i\r^n_{R_0,\th}=|i\r_{R_0,\th}.
\eeq

\subsection{Correct form of complex scaled operators}

The central point is that we analytically continue the operators, which, for real scaling are readily defined using
the unitary transforms. It is important to keep in mind that also the complex scaled operators are operators in the 
Hilber space, be it $\cH$ or $\cH\up{n}$. Coming back to Eq.~(\ref{eq:ddr-complex}), where the somewhat misleading 
notation for $d/dR(r)$ was introduced. We agree, I believe that in the case of radial functions $r\phi(r)\in\cH$, 
the correct form of the complex scale derivative is
\beq\label{eq:ddr-correct}
(\ddr)_{R_0\th}=\left\{\bar 
\ddr & \text{ for } r<R_0\\
(\la e^{i\th})\inv1 \ddr & \text{ for } r>R_0\ear \right.,
\eeq
which, if you like, you can formally write that as $U_{R_0\th}\ddr U_{R_0\th}\inv1$. Again: the $U_{R_0\th}$ are
unbounded operators about who little is known. You can imagine them to probably map out of the Hilbert space, 
then we apply the derivative, than hopefully we go back into Hilbert space. The exact meaning of that procedure is 
full of pitfalls. That is why I say ``formally''. In constrast, Eq.~(\ref{eq:ddr-correct}) is perfectly well defined.
For it to be the correct analytic continuation of the real scaled operator, we need to restrict its application to 
functions with the correct discontinuity at $R_0$.

From this operator, we can easily deduce the required form on the non-radial Hilbert space $\cH\up{n}$. 
We pick any (differentiable) $\phi\in\cH\up{n}$ and multiply it by $r$. The multiplication maintains the
kind of discontinuities required by ECS. Then we know that the complex scaled 
operator for this radial function is applied as 
\beq
 (\ddr)_{R_0\th} [r \phi(r)]. 
\eeq


\subsection{(Anti-)hermitian differential operators}

It is important to implement all operators in hermitian form (if scaling is not complex). 
Clearly, for functions $u(r)=r\phi(r)\in\cH$
the operator $i\ddr$ is hermitian:
\beq
\int_0^\infty dr v(r) \ddr u(r) = -\int_0^\infty dr [\ddr v(r)]  u(r) +\underbrace{ u(r)v(r)|_0^\infty}_{=0}.
\eeq 
Now, let us substitute $u(r)=r\phi(r)$ and $v(r)=r\chi(r)$ and work in $\cH\up{n}$ space, i.e. non-radial functions:
\beq
\int_0^\infty dr r\chi(r) \ddr r \phi(r) = \int_0^\infty dr r^2 \chi(r) \frac{1}{r}\ddr r \phi(r).
\eeq 
We see that the correct hermitian form of the operator in $\cH\up{n}$ is
\beq
\frac{1}{r}\ddr r = \ddr + \frac{1}{r},
\eeq
in accordance with Eq.~(\ref{eq:nonradialOp}). Note that this differs from your original form with the factors
$rR(r)=r^2$ (unscaled)
\beq
\frac{1}{r^2}\ddr r^2 = \ddr + \frac{2}{r},
\eeq
which is not hermitian on $\cH\up{n}$, i.e. with integration $r^2 dr$.
For radial functions, $u,v\in \cH$, we agree that the scaling would be
\beq\label{eq:ddr-complex}
\frac{d}{dr}\to \frac{1}{\lambda e^{i\th}}\frac{d}{dr}=:\frac{d}{dR(r)}=(\ddr)_{R_0,\th}
\eeq
and consequently on $\cH\up{n}$:
\beq
\frac{1}{r}\frac{d}{dr}r\to \frac{1}{R(r)}\frac{d}{dR(r)}R(r)
\eeq


\subsection{Orthogonality}

In the present form of the paper, I have difficulties to understand the orthogonality. It is not completely clear,
how the bridge functions are treated. For being consistent with section III.A, the functions should be discontinuous 
at $R_0$. We have described in detail in the Weinmueller paper, how this can be done.

But even without any scaling, I do not quite understand.
Let us first consider that unscaled case with a radial function
\beq
\Phi(r) = \sum_i ry_i(r) c_i,
\eeq
where the $c_i$ are the actual numbers appearing on the computer. Usually, on presumes that these are the
values at the quadrature grid points $\Phi(r_i)=c_i$. 
As the some functions share boundary points $r_b$, the interpretation of $c_i$ as function values is only valid if
\beq
r_b y_i(r_b)= r_b y_j(r_b)=1\quad\forall i,j.
\eeq
The condition $=1$ can be dropped, if one never really uses $c_i$ for plotting the function. However, the 
equality of the functions at $r_b$ is needed for continuity, if the same $c_b$ is used for both.
 
However, this seems to be violated if you normalize by dividing by $\sqrt{w_i}$ unless you have the same 
Gauss-Lobatto quadrature on all elements and a;; elements of equal size, such that the boundary weights $w_b$ are all equal.
This will not be usually the case if you mix gauss-lobatto and gauss-radau-laguerre.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "EcsBasis"
%%% End: 
