\section{Transformation between coordinate systems}

\subsection{Mixed coordinate systems}

To accomodate different coordinates on the same space, a \verb"class BasisNdim" is
introduced that refers to several coordinates. It is assumed that basis sets have,
in general, fewer functions than the main discretization with its efficient standard
tree-product form. A \verb"BasisNdim" always 
relates to the {\em original coordinate system} used for the tree-product basis. 
A \verb"BasisNdim" carries a set of quadrature points 
\verb"_quadGrid", where for each point \verb"pt", the components \wrt the original
coordinates are given as the vector \verb"_quadGrid[pt]". A quadrature weight is also available
for each \verb"pt".
The member \verb"_valDer[pt][ibas][0]" contains the value of the
\verb"ibas"'th basis function at the point \verb"_quadGrid[pt]". Partial derivatives \wrt
to the {\em original coordinates} are in  \verb"_valDer[pt][ibas][k]", for $k=1,\ldots$dim.
Typically, this quadrature grid will be well-adjusted to the \verb"BasisNdim", assuming that
the tree-product basis is comparatively smooth across the support of \verb"BasisNdim".

An standard tree-product basis can be converted to \verb"BasisNdim"-form using the derived 
\verb"class BasisProd", specfying a desired  \verb"_quadGrid" in the constructor.

For an off-center basis in polar coordinates, one uses the derived class \verb"class BasisPolarOff" which 
consists of an offset \verb"_origin" and a simple expansion into spherical harmonics and polynomials
at a (typically small) radius around it.  

\subsection{Operators}
Operator matrix elements between two \verb"BasisNdim" (provided they are compatible) can be computed
by specifying any standard \verb"class OperatorDefinition" and the two basis sets. An operator 
consists of multiple terms in the general form
\begin{align}
\l A | \mO | B \r = \int d\vu 
&\sum_k  A(\vu) V\up{k}(\vu) B(\vu)
+\sum_{l,i} (\nabla_i A(\vu)) D_i\up{l}(\vu) B(\vu)\\
+&\sum_{l',j} A(\vu) D_j\up{l'}(\vu)\nabla_j B(\vu)
+\sum_{m,ij} (\nabla_i A(\vu)) Q_{ij}\up{l'}(\vu)\nabla_j B(\vu)
\end{align}
The partial derivatives refer to the \verb"_quadGrid" coordinate system.
The necessary functions and operators are generated automatically for standard \verb"OperatorDefinition"'s
using \verb"class OperatorNdim".

The \verb"OperatorTree" constructor handles the \verb"BasisAbstract" found in \verb"Index". 
This can be either a
(traditional) \verb"BasisSet" or a new \verb"BasisNdim". 

\subsection{Off-center polar coordinates}

Off-center polar coordinats $(\tphi,\teta,\trad)$ with the definitions
\beq\label{eq:polarShift}
\vu=\vr_0+
\bma 
\cos\tphi\sqrt{1-\teta^2}\trad\\
\sin\tphi\sqrt{1-\teta^2}\trad\\
\teta\trad
\ema
\eeq
where vectors always refer to the standard origin.

Our off-center basis returns partial derivatives wrt to the tilde coordinates
\beq
\bma \partial_\tphi \\ \partial_\teta \\ \partial_\trad \ema f(\vu) =: 
\bma f_\tphi \\ f_\teta \\ f_\trad \ema.
\eeq
We transform to partial derivatives \wrt to the standard coordinates
by going through cartesian coordinates $(x,y,z)$:
\beq
\bma \partial_\phi \\ \partial_\eta \\ \partial_r \ema f(\vu) = 
\bma
\frac{\partial_\tphi}{\partial_\phi}&
\frac{\partial_\teta}{\partial_\phi}&
\frac{\partial_\trad}{\partial_\phi}\\
\frac{\partial_\tphi}{\partial_\eta}&
\frac{\partial_\teta}{\partial_\eta}&
\frac{\partial_\trad}{\partial_\eta}\\
\frac{\partial_\tphi}{\partial_r}&
\frac{\partial_\teta}{\partial_r}&
\frac{\partial_\trad}{\partial_r}\\
\ema
\bma f_\tphi \\ f_\teta \\ f_\trad \ema  
=
\bma
\frac{\partial_x}{\partial_\phi}&
\frac{\partial_y}{\partial_\phi}&
\frac{\partial_z}{\partial_\phi}\\
\frac{\partial_x}{\partial_\eta}&
\frac{\partial_y}{\partial_\eta}&
\frac{\partial_z}{\partial_\eta}\\
\frac{\partial_x}{\partial_r}&
\frac{\partial_y}{\partial_r}&
\frac{\partial_z}{\partial_r}\\
\ema
\bma
\frac{\partial_\tphi}{\partial_x}&
\frac{\partial_\teta}{\partial_x}&
\frac{\partial_\trad}{\partial_x}\\
\frac{\partial_\tphi}{\partial_y}&
\frac{\partial_\teta}{\partial_y}&
\frac{\partial_\trad}{\partial_y}\\
\frac{\partial_\tphi}{\partial_z}&
\frac{\partial_\teta}{\partial_z}&
\frac{\partial_\trad}{\partial_z}\\
\ema
\bma f_\tphi \\ f_\teta \\ f_\trad \ema  
\eeq
The Jacobians for polar coordinates are
\beq
\frac{\partial(x,y,z)}{\partial(\phi,\eta,r)}=
\bma
\frac{\partial_x}{\partial_\phi}&
\frac{\partial_y}{\partial_\phi}&
\frac{\partial_z}{\partial_\phi}\\
\frac{\partial_x}{\partial_\eta}&
\frac{\partial_y}{\partial_\eta}&
\frac{\partial_z}{\partial_\eta}\\
\frac{\partial_x}{\partial_r}&
\frac{\partial_y}{\partial_r}&
\frac{\partial_z}{\partial_r}\\
\ema
=
\bma
-\sin\phi\sqrt{1-\eta^2} r& \cos\phi\sqrt{1-\eta^2} r&0\\
-\cos\phi\frac{\eta}{\sqrt{1-\eta^2}} r&-\sin\phi\frac{\eta}{\sqrt{1-\eta^2}} r& r\\
\cos\phi\sqrt{1-\eta^2}&\sin\phi\sqrt{1-\eta^2}&\eta\\
\ema
=:\mJ(\phi,\eta,r).
\eeq
With this we write
\beq
\partial f(\vu) = 
\partial f(\tphi,\teta,\trad) = 
\mJ(\phi,\eta,r)\mJ(\tphi,\teta,\trad)\inv1 \tilde{\partial} f(\tphi,\teta,\trad)  
\eeq

\subsection{Radial basis functions}

One important complication arises, as we are mostly using radial functions, i.e.
our 3d quadrature is 
\beq
\int d\tphi d\teta d\trad |f(\tphi,\teta,\trad)|^2 =\sum_i \tilde{w}_i  |f(\tphi_i,\teta_i,\trad_i)|^2
\eeq
Our standard operators are all defined with respect to coordiantes $\vu$, so we should 
re-express functions and quadratures for radial functions \wrt $\vu$. At the same time we
want to sample the off-center functions at a quadrature points referring to $\tphi,\teta,\trad$.

Internally, we re-define the functions $f$ as radial functions for $\vu$ and adjust the quadrature 
weights accordingly:
\beq
\sum_i \tilde{w}_i  |f(\tphi_i,\teta_i,\trad_i)|^2
=\sum_i w_i  |\frac{u_i}{\trad_i}f(\tphi_i,\teta_i,\trad_i)|^2, \quad  w_i:= \tilde{w}_i \frac{\trad_i^2}{u_i^2}.
\eeq

We need to evaluate the gradients $\partial (uf/\trad)$ from $\tilde{\partial}f$ by
\beq\label{eq:opOff})
\partial \left(\frac{u}{\trad}f\right) 
=  u\partial \frac{f}{\trad} + \bma 0\\0\\1\ema\frac{f}{\trad}
= u\mJ_{u}\tilde{\partial}\left(\frac{f}{\trad}\right) + \bma 0\\0\\1\ema\frac{f}{\trad}
= \left(\frac{u}{\trad}\mJ_{u}\right)\left[\tilde{\partial}f- \bma 0\\0\\1\ema\frac{f}{\trad}\right] 
  + \bma 0\\0\\1\ema\frac{f}{\trad}
\eeq 
where the last form corresponds to the implementation in \verb"class BasisPolarOff". These re-defined
functions and gradients can be combined with radial functions $\phi(\vu)$, e.g. for the 
matrix element of $u$-radial second derivative $-\partial_u^2$:
\beq
\l \partial_u \phi | \partial_u \frac{u}{\trad} f \r = \sum_i w_i (\partial_u\phi)(\vu_i) (\partial_u \frac{u}{\trad} f)(\vu_i),
\eeq
where the $\vu_i$ are the properly transformed points $\tphi_i,\teta_i,\trad_i$, Eq.~(\ref{eq:polarShift}).

\subsection{General coordinates}
The above scheme readily generalizes to arbitrary mixed coordinate systems. Let $\vq$ denote the basic reference coordinates
in which all operators are defined ($\phi,\eta,r$ of the specific example above) and $\vu$ the additional coordinates (above $\tphi,\teta,\trad$).
Let further $\vc$ be the intermediate coordinate system for easy construction of the jacobians (above $x,y,z$).
We assume that the coordinate systems are compatible, i.e. of equal dimension and the maps are (almost everywhere) invertible.

We define the Jacobians
\beq
\left.\frac{\partial{\vc}}{\partial{\vq}}\right|_{\vc(\vq)}=:\mJ_q(\vq),\quad
\left.\frac{\partial{\vc}}{\partial{\vu}}\right|_{\vc(\vu)}=:\mJ_u(\vu);
\eeq
The gradient \wrt to $\vq$ is
\beq
\vna_q f(\vu) = \mJ_q(\vq) [\mJ_u(\vu)]\inv1 \vna_u f(\vu):=\mJ_{qu}\vna_u f(\vu)
\eeq

As in the case of polar coordinates, we need to take into consideration that our bases may not 
be defined \wrt to the standard cartesian integral. Let the cartesian measure of coordinate system 
$\vq$ be 
\beq
 dx^n = \omega(\vq) dq^n,
\eeq
and assume that some factor $j(\vq)$ is absorbed into the definition of main basis $B_n(\vq)$, such that 
integrals are computed as
\beq
\int dq^n \omega(\vq) [\frac{1}{j(\vq)}B_m(\vq)] [\frac{1}{j(\vq)}B_n(\vq)].
\eeq
For example, in case of radial polar functions, $j(r,\eta,\phi)=r$. The definition of operators is in 
coordinates $\vq$. Second derivative operators are given as
\beq\label{eq:operQ}
\l B_m|\oQ| B_n\r = \int dq^n \frac{\om(\vq)}{j_q^2(\vq)} [\vna_q B^*_m]\cdot \mQ(\vq) [\vna_q B_n]. 
\eeq 
A more general form includes first derivatives and multiplicative operators, using a 4-vector notation $\tB_m:=(B_m,\vna_q B_m)$:
\beq\label{eq:operQ}
\l B_m|\oQ| B_n\r = \int dq^n \frac{\om(\vq)}{j_q^2(\vq)} \tB_m \cdot \tQ(\vq) \tB_n. 
\eeq 

Usually, the extra basis $C_m(\vu)$ used for coordinates $\vu$ will be understood \wrt to a different $j_u(\vu)$.
For example, the off-center polar coordinates, this are the respective radial bases, $j_u=\tilde{r}\neq j_q=r$.
We assume, however, that additional basis $C_m$ varies more strongly on its domain, while the main basis
is locally smooth there. For that reason, the quadrature rule is adjusted to the $C_m(\vu)$-basis.  
Let the quadrature scheme for $\vu$ be
\beq
\int du^n \frac{\om(\vu)}{j_u^2(\vu)} C_m(\vu)C_n(\vu)=\sum_i w_i C_m(\vu_i)C_n(\vu_i). 
\eeq
As all operators are defined defined \wrt $\vq$ and $j_q(\vq)$, Eq.~(\ref{eq:operQ}), 
we work with functions that refer to the $\vq$-basis and include the $j_q$'s into it.
For that we transform the extra basis 
\beq
C_m(\vu)\to C_m^q(\vu)=\frac{j_q(\vq(\vu))}{j_u(\vu)}C_m(\vu)=:G^0_m(\vu)
\eeq
We define transformed points and new quadrature weights
\beq
\vq_i:=\vq(\vq_i),\qquad w^q_i:=w_i\frac{j^2_u(\vu_i)}{j^2_q(\vq_i)}
\eeq
then
\beq
\int du^n \frac{\om(\vu)}{j_u^2(\vu)} C_m(\vu)C_n(\vu)
=\int du^n \frac{\om(\vu)}{j_q^2(\vu)} C^q_m(\vu)C^q_n(\vu)
=\sum_i w_i^q G^0_m(\vu_i)G^0_n(\vu_i). 
\eeq
For using the operators in the form (\ref{eq:operQ}) on the $C^q_m$ we observe
\beq
\vna_q \left[\frac{j_q}{j_u} C_m\right](\vu)
=\left[j_q \vna_q + (\vna_q j_q)\right]\frac{C_m}{j_u} 
=\frac{j_q}{j_u}\left[ \mJ_{qu}\left(\vna_u-\frac{(\vna_u j_u)}{j_u}\right) + \frac{(\vna_q j_q)}{j_q}\right]C_m(\vu)=:\vG_m(\vu).
\eeq 
In the case of shifted polar coordinates, $j_q=r$ and $j_u=\trad$, i.e.\ we recover Eq.~(\ref{eq:opOff})
\beq
\vna_r \left[\frac{r}{\trad}C_m\right] =   \frac{r}{\trad}\mJ_{qu}\left[\vna_\trad-\frac{1}{\trad}\bma 0\\0\\1\ema \right]C_m + \frac{1}{\trad}\bma 0\\0\\1\ema C_m
\eeq
(Note some notational inconsistency in the use of $u$).
We compute the matrix elements of the operator $\oQ$ as
\beq
\l C_m|\oQ| C_n\r 
= \int du^n \frac{\om(\vu)}{j_q^2} [\vna_q C^q_m]\cdot \mQ(\vq(\vu)) [\vna_q C^q_n] 
= \sum_i w^q_i \vG_m(\vu_i)\cdot \mQ(\vq_i) \vG_n(\vu_i) 
\eeq
Using 4-vectors, general operators are
\beq
\l C_m|\oQ| C_n\r = \sum_i w^q_i \tilde{G}_m(\vu_i)\cdot \tQ(\vq_i) \tilde{G}_n(\vu_i) 
\eeq

\subsubsection{Implementation}
Given a basis $C_m(\vu)$ a suitable $I$-point multidimensional quadrature grid $\vu_i,i=\range{I}$ is set up.
The $\vu_i,\vq_i$ and $w^q_i$ are held by the \verb"BasisNdim". 
All the 4-vectors of values and derivatives are made available through a function for $ \tilde{G}_n(\vu_i), \forall n,i$.
For the purpose of the integration, the main basis functions and their gradients are evaluated on the 
$\vu_i$-grid, re-expressed in the $\vq$-coordinates: $\tB_m(\vq_i)$. The basis functions
for a single node in the index tree have product form and will be evaluated by \verb"BasisProd" as needed. 
Note, however, that the $\vq_i$ are not on a product grid in the $\vq$-coordinates and therefore 
the matrices do {\em not} factor into a tensor product.



\subsection{Two-dimensional example}

(Not sure whether this makes  sense\...)

Assume we have an expansion in terms of one pair of coordinates $x,y$
\beq
\Psi(x,y)=\sum_{i,j=0}^{I-1,J-1} f_i(x)g_j(y) C_{ij}=:\sum_{i,j=0}^{I-1,J-1} |i\r\otimes|j\r C_{ij}
\eeq
we want to re-express this function in a new basis
\beq
\Phi(r,c)=\sum_{n,l=0}^{N-1,L-1} a_n(r)b_l(c) D_{nl}=\sum_{n,l=0}^{N-1,L-1} |n\r\otimes|l\r D_{nl}.
\eeq
By re-express we mean we project the original function onto the subspace spanned by the $|n\r,|l\r$
\beq
\Phi(r,s)=Q\up{rc}|\Psi\r=|n\r\otimes|l\r\left(S\up{r}\otimes S\up{c}\right)\inv1 [\l l'|\otimes\l n'|]|i\r\otimes|j\r C_{ij}
\eeq
The operations count looks like order 4: $I\times J\times N\times L$.

However, we can reduce this to lower order by transforming to a quadrature grid. Let $c_\la,r_\nu$ denote
the quadrature grid for the $c,r$ coordinates, then we need $\Phi(c_\la,r_\nu)$.
We perform the transformation in two steps: first we transform to
\beq
\chi(x_\al,r_\nu)=\Psi(x_\al,y(x_\al,r_\nu))
\eeq
Here it is important to select the $x_\al,\al=\range{A}$ and --- if not unique --- the $y(x_\al,r_\nu)$ such that the interpolation
for $c_\la$ can be performed in a robust form (see below). The number of $x_\al$ should be $A\sim I$.
Then we transform to 
\beq
\Phi(c_\la,r_\nu)=\chi(x(c_\la,r_\nu),r_\nu)
\eeq
where the value at $x(c_\la,r_\nu)$ is obtained by interpolation from the $\chi(x_\al,r_\nu)$. For interpolation
we use the original expansion functions $f_i(x)$.

Here the procedure in detail
\bea
\chi(x_\al,r_\nu)&=&\sum_{ij} g_j(y(x_\al,r_\nu)) f_i(x_\al)C_{ij}\\
&=&\sum_j G\up{\al}_{\nu j}\sum_i\underbrace{ F\up\al_i C_{ij}}_{\order{AIJ}}\\
&=&\sum_j \underbrace{G\up{\al}_{\nu j} C\up\al_{j}}_{\order{ANJ}}
\eea
The interpolation to the $c_\la$ grid goes by first transforming to the expansion 
coefficients $d_{i\nu}$ \wrt the $f_i$:
\beq
d_{i\nu}=\sum_\al \underbrace{\overline{F}_{i\al}\chi(x_\al,r_\nu)}_{\order{IAN}}
\eeq
with 
\beq
\overline{F}_{i\al}=[S\up{x}]\inv1_{ij} f_j(x_\be)w_{\be}I_{\be\al},
\eeq
where $x_\be$ and $w_\be$ is a sutiable quadrature and $I_{\be\al}$ is an interpolation 
from the $x_\al$ to the quadrature points.
Finally we evaluate at $c_\la$:
\beq
\Psi(c_\la,r_\nu)=\sum_i\underbrace{ f_i(x(c_\la,r_\nu))d_{i\nu}}_{\order{ILN}}.
\eeq
This procedure requires 6 $\order{M^3}$ operations, while a direct transformation 
from the $C_{ij}$ to the new expansion coefficients $D_{lm}$ requires a single $O(M^4)$ operation.
Clearly, it is advantegous only for rather large orders of the expansions. However, with large angular
momenta sizes on the scale $L_{\rm max}=30$, it can gain significantly. 

\begin{itemize}
\item Get the expansions
\beq
\tilde{f}_i(r,c)=Q\up{rc}f_i(x),\quad\tilde{g}_j(r,c)=Q\up{rc}g_j(y)
\eeq
in fll precision during setup.
\item Evaluate at setup $\tilde{f}_i(r_\nu,c_\la)$, and  $\tilde{g}_j(r_\nu,c_\la)$, where $r_\nu,c_\la$ are quadrature points
 These will be matrices of size $NL\times I$ and $NL\times J$, respectively.
\item We have
\beq
\Phi(r,c)=\sum_{ij}\tilde{f}_i(r,c)\tilde{g}_j(r,c)C_{ij}
\eeq
\item The $\tilde{D}_{nl}:=(S\up{r}\otimes S\up{c})D_{nl}$ are given by
\beq
[\l n| \otimes \l l|]| \Phi\r= \sum_{\nu,\la} w_\nu v_\la a_n(r_\nu)b_l(c_\la)\Phi(r_\nu,c_\la)
=:\sum_{\nu,\la} A_{n\nu}B_{l\la} \sum_{ij}F_{i,\nu\la}G_{j,\nu\la}C_{ij}
\eeq
\item
We can rewrite
\beq
[\l n| \otimes \l l|] \Phi\r=\sum_{\nu,\la} \sum_{ij}\tilde{F}_{i,n\la}\tilde{G}_{j,\nu l}C_{ij}
\eeq
\item In matrix notation
\beq
\tilde{F}^TC\tilde{G}
\eeq
\end{itemize}

\subsection{Combination with DVR method}

It appears that combining exact integration on the additional (e.g. off-center) basis with 
DVR integration renders the system poorly defined. We will try to remedy that problem, by 
representing the coupling between the additional to the DVR basis in the original DVR basis rather
than computing the extact integrals. Let $|n\r$ represent a basis function added to the system and
let $|\al\r$ be a (product) DVR basis. The exact matrix elements coupling the two are
\beq
\l n|\mB |\al\r=\int dq^d \chi^*_n(\vq) \mB \psi_\al(\vQ(\vq)).
\eeq 
At present, these integrals are (essentially) computed exactly, while matrix elements
$\l \al |\mB|\be\r$ are computed only approximately, using DVR. As a result, artifacts appear
(eigenvalues far below the variational limit, even with comparatively innocent operators
such as $q^2$. The exact origin of the problem (other that DVR is not exact) remains unclear 
a present. However, when DVR is replaced with accurate quadrature, the problem disappears.

In practice, we cannot sacrifice DVR of the large, fundamental basis. We may, however, try to 
remedy the problem by masking the basis $|n\r$ from the DVR by projecting it onto the
DVR basis before applying any operator $\mB$:
\beq
\l n|\mB|\al\r\approx \l n|\mP\mB|\al\r = \sum_j \l n |\be\r w_\be\inv1 \l \be |\mB|\al\r,
\eeq
where $w_\be$ is the (diagonal) DVR overlap elements.

\subsubsection{Current solution}
The above strategy did not solve the problem. We found (for off-center polar coordinates) that 
one can keep the overlap matrix in DVR approximation for the basic discretization and 
only compute all operators by a more accurate quadrature. Although this introduces some
poorly understood inconsistency of the \verb"Index" definition with its \verb"s0" and \verb"ovr" matrices, 
we keep this solutions for now. 


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "tsurff"
%%% End: 
