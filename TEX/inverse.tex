\section{Inverse overlap}

In the single particle version: let $P_i$ be a set of mutually orthogonal projectors 
and $Q=\prod_i (1-P_i) = 1-\sum_i P_i$. The second equality holds because of mutual orthogonality
$P_iP_j=\delta_{ij} P_i$. The $P_i$ have the specific shape 
\beq
P_i = | i \r \l i | \qquad \l i | := (\ldots0,-\frac{1}{\sqrt{2}},0,\ldots, 0,\frac{1}{\sqrt{2}},0 \ldots)
\eeq
Then the inverse understood on the $Q$-space is
\beq
(QS_1Q)\inv1 = \left[1 - S_1\inv1 R_1 (R_1^T S_1\inv1R_1)\inv1 R_1^T\right] S_1\inv1=[1-T_1]S_1\inv1
\eeq
When 
\beq
QSQ=Q_1S_1Q_1\otimes  Q_2S_2Q_2
\eeq
the projectors may not be mutually orthogonal. We need to apply
\bea
\lefteqn{[1-T_1]S_1\inv1\otimes[1-T_2]S_2\inv1}
\\&=& 
S_1\inv1\otimes S_2\inv1 - T_1S_1\inv1 \otimes S_2\inv1 - S_1\inv1 \otimes T_2S_2\inv1 + T_1S_1\inv1 \otimes  T_2S_2\inv1
\\&=&
\left[\one\otimes\one -L_1 M_1\inv1 R_1^T \otimes \one_2  -\one_1\otimes L_2 M_2\inv1 R_2^T +L_1 M_1\inv1 R_1^T \otimes  L_2 M_2\inv1 R_2^T\right]
S_1\inv1\otimes S_2\inv1 
\eea
with 
\beq
L_i= S_i\inv1 R_i,\quad{\rm and }\quad M_i=R_i^TS_i\inv1R_i
\eeq

The algorithm for solving is then
\begin{itemize}
\item $ \vec{c} \leftarrow (S_1\inv1\otimes S_2\inv1) \vec{c}$
\item $\vec{e}_1=(R_1^T\otimes \one_2)\vec{d}$, $\vec{e}_2=(\one_1\otimes R_2^T)\vec{c}$ 
\item $\vec{e}_1\leftarrow (M_1\inv1\otimes \one_2)\vec{e}_1$, $\vec{e}_2\leftarrow (\one_1\otimes M_2\inv1)\vec{e}_2$
\item $\vec{f}=(R_1^T\otimes \one_2)\vec{e}_2$ 
\item $\vec{f}\leftarrow (M_1\inv1\otimes \one)\vec{f}$
\item $\vec{c}\leftarrow \vec{c}- (L_1\otimes \one_2)\vec{e}_1- (\one_1\otimes L_2)\vec{e}_2 + (L_1\otimes L_2)\vec{f}$
\end{itemize}
In a straight-forward implementation, two new arrays of size $\sim N/p$ each and one array $\sim N/p^2$ are required.
In the parallel version, these need to be re-distributed in an all-to-all type of communication.
Probably, building and redistributing can be intertwined as to never build the full vectors on any single node.
Application of each factor $M_i\inv1$ can be done in parallel with respect to the second particle index.

\subsection{Simplified version}

We write
\beq
(QS_1Q)\inv1\otimes (QS_2Q)\inv1=\left[(QS_1Q)\inv1\otimes \one\right] \left[(\one\otimes (QS_2Q)\inv1\right]
\eeq
which greatly simplifies procedures at the expense of two sweeps  through
the whole function instead of one.


\subsection{Iteration of inverse overlap}
Assume that $S_0$ cannot be factored into tensor products. The projector is the
product of projectors in the respective spatial dimensions. Note that individual
projectors are not mutally orthogonal and do not commute.
When the discontuity vectors in two different directions share one coefficient
at the corner of one patch we have:
\beq
|i\r\l j| = \bma 1 \\ -1 \\ 0 \ema \bma 0 & 1 & -1 \ema =
\bma 0 & 1 & -1 \\ 0 & -1 & 1 \\ 0 & 0 &0 \ema \neq |j\r\l i|
\eeq
However, in the cartesian geometry to which we restrict the present discussion, 
projectors at a corner always come in pairs
\beq
P_1=|a\r\l a| +|b\r\l b|\text{ and }P_2=|c\r\l c| +|d\r\l d|,
\eeq
with 
\beq
|a\r=\bma 1 \\ -1 \\ 0 \\ 0 \ema,\quad
|b\r=\bma 0\\ 0\\ 1 \\ -1  \ema,\quad
|c\r=\bma 1\\ 0\\ -1 \\ 0  \ema,\quad
|d\r=\bma 0\\ 1\\ 0\\ -1  \ema
\eeq
In this case it is easy to see that 
\beq
P_1P_2=P_2P_1.
\eeq
In this case, we can iterate the inverse correction for the individual directions.
\beq
S= Q_2Q_1 S_0 Q_1Q_2.
\eeq
We want the pseudo-inverse
\beq
S\inv1 S = Q_1Q_2 = Q_2Q_1.
\eeq
Let us define
\beq
S_1:=Q_1S_0Q_1,\quad S_1\inv1 S_1 = S_1S_1\inv1=Q_1.
\eeq
with which we obtain the pseudo-inverse:
\beq
S\inv1 = S_1\inv1-S_1\inv1 P_2 (P_2S_1\inv1P_2)\inv1 P_2 S_1\inv1
\eeq
Verify:
\bea
\lefteqn{[S_1\inv1-S_1\inv1 P_2 (P_2S_1\inv1P_2)\inv1 P_2 S_1\inv1] Q_2Q_1SQ_1Q_2}\\
&=&[S_1\inv1-S_1\inv1 P_2 (P_2S_1\inv1P_2)\inv1 P_2 S_1\inv1] (1-P_2)S_1(1-P_2)\\
&=&Q_1-S_1\inv1P_2S_1-Q_1P_2+S_1\inv1P_2S_1P_2\\
&&-(S_1\inv1 P_2 (P_2S_1\inv1P_2)\inv1 P_2)(Q_1-S_1\inv1P_2S_1-Q_1P_2+S_1\inv1P_2S_1P_2)\\
&=&1-P_1-S_1\inv1P_2S_1-P_2+P_1P_2+S_1\inv1P_2S_1P_2\\
&&-(S_1\inv1 P_2 (P_2S_1\inv1P_2)\inv1 P_2)(1-P_1-S_1\inv1P_2S_1-P_2+P_1P_2+S_1\inv1P_2S_1P_2)\\
&=&1-P_1-[S_1\inv1P_2S_1]_a-P_2+P_1P_2+[S_1\inv1P_2S_1P_2]_b\\
&&-[(S_1\inv1 P_2 (P_2S_1\inv1P_2)\inv1 P_2)]_c\\
&&+[(S_1\inv1 P_2 (P_2S_1\inv1P_2)\inv1 P_2)P_1]_d\\
&&+[(S_1\inv1 P_2 (P_2S_1\inv1P_2)\inv1 P_2)S_1\inv1P_2S_1]_a\\
&&+[(S_1\inv1 P_2 (P_2S_1\inv1P_2)\inv1 P_2)P_2]_c\\
&&-[(S_1\inv1 P_2 (P_2S_1\inv1P_2)\inv1 P_2)P_1P_2]_d\\
&&-[(S_1\inv1 P_2 (P_2S_1\inv1P_2)\inv1 P_2)S_1\inv1P_2S_1P_2]_b\\
&=&1-P_1-P_2+P_1P_2=Q_1Q_2.
\eea
(Terms with equal subscripts cancel).

We have further
\bea
P_2S_1\inv1P_2&=&P_2(S_0\inv1-S_0\inv1P_1(P_1S_0\inv1P_1)\inv1P_1S_0\inv1)P_2.
\eea
If $S_0=T_1\otimes T_2$, then
\beq
P_1 S_0\inv1 P_2 = (P_1\otimes \one)(T_1\inv1\otimes T_2\inv1)(\one\otimes P_2)
\eeq
and
\bea
P_2S_1\inv1P_2
&=&T_1\inv1\otimes P_2T_2\inv1P_2-T_1\inv1 P_1\otimes P_2T_2\inv1 
(P_1T_1\inv1P_1\otimes T_2\inv1)\inv1(P_1T_1\inv1\otimes T_2\inv1P_2)\\
&=&T_1\inv1\otimes P_2T_2\inv1P_2
-T_1\inv1 P_1
(P_1T_1\inv1P_1)\inv1(P_1T_1\inv1)\otimes P_2T_2\inv1 P_2\\
&=&[Q_1T_1Q_1]\inv1\otimes P_2[T_2\inv1]P_2.
\eea
In particular, there are no terms mixing the two factors. However, when 
$S_0$ is not just a tensor product, the factorization does not work and 
$P_2S_1\inv1P_2$ should be calculated explicitly.

\subsection{Parallelization of inverse overlap}
Taking into account the fact that the matrix $PS_0\inv1P$ for a single direction 
is tri-diagonal and allows a Cholesky-decomposition or pseudo-Cholesky for our typical 
complex symmetric matrices, communication can be reduced to a sequence of nearest neighbor 
communications.

Here the algorithm for a single \verb"Strand", i.e. a sequence of projectors that are linked by 
the inverse overlap.
\begin{enumerate}
\item denote by $d_n$ the discontinuity vector, i.e. $P_n=\frac12 d_nd^T_n$. We choose smaller index coefficient
of $d_n$ to be negative $-1$, i.e. the uper boundary of the lower neighbor sees the negativ
coefficient.
\item let $R_{mn}=d^T_m S_0\inv1 d_n$ be our tri-diagonal matrix with a decomposition
\beq
R = C^TC
\eeq
and diagonal and super-diagonal elements
\beq
c,s: c_n:=C_{nn},\,n=0,\ldots,N,\quad, s_n=C_{n,n+1}=C^T_{n+1,n},n=0,\ldots,N-1 
\eeq
\item Compute $z: C^TC z = x, z=C\inv1 y, y=C\inv{1T}x$ by up-sweep for $y$ and down-sweep for $z$.
\item {\bf up-sweep: } Compute $y=C\inv{1T}x$, $y$ is a vector of length $N-1$. 
\beq
C^T y = x:\quad C_{nn}y_n + C^T_{nn-1}y_{n-1}=x_n\rightarrow x_n=(y_n-s_{n-1}y_{n-1})/c_n
\eeq
We have 
$x\up0_n$\ldots lower boundary value on element $n$, $x\up1_{n-1}$ upper boundary value on element $n-1$.
Inner product with discontinuity vector $d_n:\, x_n=x\up0_n-x\up1_{n-1}$, 
note that the summands are on different elements and need to be communicated between elements.
During the up-sweep, \verb"z" is used as auxiliary storage. 
\begin{enumerate}
\item get $z_{n-1}:=(-x\up1_{n-1}-s_{n-1}y_{n-1})$ from lower neighbor (=0 for n=0, no lower neighbor)
\item compute $y_n=(x\up0_{n}+z_{n-1})/c_n=((x\up0_n-x\up1_{n-1})-y_{n-1})/c_n$
\item compute $z_n=(-x\up1_n-s_ny_n)$ for use in next higher element 
\end{enumerate}
\item {\bf down-sweep: } Compute $y'=C\inv{1}z'$, here we use again \verb"y" as auxiliary storage.
\beq
Cz=y:\quad C_{nn}z_n + C_{n,n+1}z_{n+1}=y_n\rightarrow z_n=(y_n-s_{n}z_{n+1})/c_n
\eeq
 The vectors $\vl_n,\vu_n$ are the first/last column
of the local inverse matrix $s_0\inv1$  
\begin{enumerate}
\item get $z_n=(y_n-s_{n}z_{n+1})/c_n$
\item correction from upper an lower boundaries of element
$B-=(\vl_nz_n-\vu_n y_n)=(\vl_nz_n-\vu_n z_{n+1})$
\end{enumerate}
\end{enumerate}

\subsection{Contracting vectors and matrices}

With the properly normalized (dis-)contininuity vectors $|c_i\r$ the projectors
are 
\beq
|i\r\l i| = \frac12 \bma \vdots\\ 1 \\ -1 \\ \vdots \ema  \bma \cdots & 1 & -1 & \cdots \ema
\eeq
Imposing continuity at boundary $i$ (for all boundaries: \verb"Coefficients::makeContinous()" at the time of writing) is 
to apply the projector
\beq
\mQ_i = 1-|i\r\l i|.
\eeq
In actual application of the operator, we do
\beq
\mH \vc= \mQ \mH_0 \mQ \vc.
\eeq

Let us now consider a contraction of $\mH$ into reduced dimensional matrix $\mK$,
which is obtaind by {\em adding} the rows and columns of the matrix that correspond to the 
indices to the left and right of an element boundary. The complications arise because this
operation does not correspond to the above projection, as the sum of matrix elements is taken,
not their average.

We illustrate using the most elementary example, where $\mH_0$ is 4$\times$4. Here the projector can be 
written in full as
\beq
\mQ = \one - \frac12 \bma 0\\ 1 \\ -1 \\ 0\ema \bma 0& -1 & 1 & 0\ema 
= 
\bma
1 & 0 & 0 & 0 \\ 
0 &\frac12 & \frac12 & 0 \\
0 &\frac12 & \frac12 & 0 \\
0 & 0 & 0 & 1
\ema
= 
 \bma
1 & 0 & 0  \\ 
0 &\sqrt{\frac12}  & 0 \\
0 & \sqrt{\frac12} & 0 \\
0 & 0  & 1
\ema
\bma
1 & 0 & 0 & 0 \\ 
0 &\sqrt{\frac12} & \sqrt{\frac12} & 0 \\
0 & 0 & 0 & 1
\ema
=\mR \mR^T
\eeq

The matrix used effectively in standard appliction is
\beq
\mH = \mR \mR^T \mH_0 \mR \mR^T.
\eeq
The contracted matrix obtained by just adding the individual pieces into the total matrix 
is 
\beq
\mH_c = \mT^T \mH_0 \mT,\qquad \mT:= 
\bma
1 & 0 & 0  \\ 
0 & 1 & 0 \\
0 & 1 & 0 \\
0 & 0  & 1
\ema=
\mR
\bma
1 & 0 &  0 \\ 
0 & \sqrt{2} & 0 \\
0 & 0 &  1
\ema=:\mR\md
\eeq
We can now write the standard application in terms of the contracted matrix as
\beq
\mH = \mR \md\inv1 \mH_c \md\inv1 \mR^T.
\eeq
It is obvious that before applying $\mH_c$ we need to apply $\mR^T$,
but also multiply the boundary coefficients by $\sqrt{\frac12}$, $\md\inv1\mR^T$.
The effect of \verb"makeContinous()" is to take the average of the two margin 
coefficients   
\beq
a_0,a_1,a_2,a_3 \to (a_0,(a_1+a_2)/2,(a_1+a_2)/2,a_3).
\eeq
In fact, this is exactly the vector we need: $(c_0,c_1,c_2)=\vc=\mD\inv1\mR\va= (a_0,(a_1+a_2)/2,a_3)$,

For the expansion step from the contracted vector
\beq
\vf = \mH_c \md\inv1 \mR^T \va
\eeq
we need to remember that we simply take that same coefficient to two different places. 
\beq
\mR \md\inv1 \vf = 
 \bma
1 & 0 & 0  \\ 
0 &\sqrt{\frac12}  & 0 \\
0 & \sqrt{\frac12} & 0 \\
0 & 0  & 1
\ema
\bma
1 & 0 &  0 \\ 
0 & \sqrt{\frac12} & 0 \\
0 & 0 &  1
\ema
\bma f_0 \\ f_1 \\ f_2 \ema
=
\bma f_0 \\ f_1/2 \\ f_1/2 \\ f_2 \ema
\eeq
By just copying the contracted values $f_1$ into two positions, we are missing a factor of 1/2.
These factors are put into the right place by  \verb"makeContinous(0.5)".

However, for compatibility with other parts of the code, the matrix in \verb"BlockView::sparseMatrix"
is not simply contracted, but rather the matrix
\beq
\mH_s: = \md\inv1 \mH_c \md\inv1 
\eeq
is computed. For correct application of that matrix we must insert a $\md$ before applying $\mH_s$.
The 1/2 factors after matrix application now are generated by applying $\md\inv1$ after applying $\mH_s$. 

Note that the contraction to $\mH_s$ should be reassesed. In view of the above discussion it appears much 
clearer to use $\mH_c$ instead. However, as such a change may affect several places in the code we postpone this
correction.

\subsection{Recursive FE inverse overlap}
This algorithm does the FE inverse recursively, using the Woodbury-style formula.

At a given FE level, split the FE axis into two pieces that are connected only by the projection.
\beq\label{eq:recursiveInverse}
\mS_0 = \bma \mT_0 & 0\\0&\mT_1 \ema
\eeq
and 
\beq
\mS = \mQ \mS_0 \mQ,\qquad \mS\inv1 = \mS_0\inv1 - \mS_0\inv1 \mR (\mR^T \mS_0\inv1 \mR)\inv1 \mR^T \mS_0\inv1
\eeq

Algorithm for a single FE axis:
\begin{enumerate}
\item Get $\vd=\vd_0\oplus\vd_1=(\mT_0\oplus\mT_1)\inv1\vc=S_0\inv1\vc$
\item Get the corresponding $\va=\mR^T\vd=\mR^T  \mS_0\inv1 \vc$ 
\item Muliply: $\vb=\mZ\inv\va= (\mR^T \mS_0\inv1 \mR)\inv1\va$: this matrix is a scalar in the 1d case, and diagonal in higher dimensions
\item Subtract $\mS\inv1\vc=\vd-\mS_0\inv1\mR\vb$: not local!\label{item:flaw}.
\item Do this recursively for the FE axis until it is fully joined.
\end{enumerate}
\paragraph{Flaw:} 
Unfortunately, step \ref{item:flaw}.\  is NOT local, as $\mS_0\inv1$ here is not the 
original local inverse overlap, but rather (\ref{eq:recursiveInverse}). This means that we need to have $\vb$ available
on all relevant threads introducing a rather complicated communication pattern including at least one broadcast essentially
to all.

\subsubsection{Implementation details}

\paragraph{Indices}
We assume that $\mS_0\inv1=\mT_0\inv1\oplus \mT_1\inv1$ is in $d$-direction with the multi-indices
 $I_i=(i_0\ldots i_{d-1}i i_{d+1}\ldots i_{D-1})$. We assume that the split is between $i_d=s$ and $i_d=s+1$.  
Note that $i_s$ may be dependent on the remaining indices $i_k, k\neq s$, if the index grid is not
a strictly a product grid.

\paragraph{Computation of $\mZ$}
We create a ``margin vector'' to either side of the split, 
\beq
\vr_{I_{s+\al}}=(-1)^\al \text{ for } \al=0,1 ,\qquad =0 \text{ for other indices }
\eeq
This functionality is in the method \verb"setBoundary".
We compute
\beq
\vz=\mS_0\inv1\vr.
\eeq
In general, $\vz$ can be a full vector.
The $\mZ$ matrix is diagonal in the indices $I_d=(i_0\ldots i_{d-1}i_d i_{d+1}\ldots)$ with the matrix element
\beq
\mZ_{I_{s},I_{s}}=\mZ_{I_{s+1},I_{s+1}}= \frac12 [\vz_{I_{s}}+\vz_{I_{s+1}}]
\eeq

\paragraph{Computation of the correction map}

The complet (pseudo-)inverse, with $\mP=\mR\mR^T$
\beq
\mS\inv1 \vc = (\one - \mC \mP) \mS_0\inv1\vc.
\eeq
The map 
\beq
\mC = \mS_0\inv1 \mR \mZ\inv1
\eeq
can be constructed from $\vz$ and $\vr$ as the direct sum of maps from one-dimensional subspaces 
We write $\vz$ as a direct sum of pieces with fixed $i_d$-index
\beq
\vr=\bigoplus_i \vr_{I_i},
\qquad
\vz=\bigoplus_i \vz_{I_i}.
\eeq
Then the map is
\beq
\mC=\frac12 \bigoplus_i \vz_{I_i} \mZ_{I_i,I_i}\inv1 \vm_{I_i}
\eeq
Note that this map has many zeros that will be passed over in setup.

\paragraph{Diagonal $\mS_0$}
In this case, the correction reduces to the application of the continuity condition at the given boundary.
It may be advantageous to implement the application of continuity within the same context: we compute
$\vd=\mS_0\inv1\vc$ and compute
\beq
\mS\inv1\vc = \vd - \mC\mP\vd,
\eeq
where $\mC$ does not need to be applied (and consequently not constructed) in the diagonal case.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "Inverse.tex"
%%% End: 
