\section{Time-dependent Born-Oppenheimer (tBO)}

Without a driving field, this idea has been formulated essentially by Cederbaum.

The central idea for us is that there is a massive correlation between electronic and
nuclear motion. However, that correlation is rather simple in being strictly in space.
Therefore, it is useful to build the dynamics on time-dependent electronic solutions 
for space-fixed nuclei. 

We write
\beq
\Psi(R,r,t)=\chi(R,t)\Phi_R(r,t).
\eeq
Without fixing any of the functions, this ansatz is trivially exact, 
although not unique.
Now write the Hamiltonian
\beq
\mH(t) = \mT(t) + \mH_e(t).
\eeq
Assuming that it is useful to have the much faster 
dynamics of electrons treated separately, we now make a choice for
the electronic functions by demanding
\beq
i\ddt \Phi_R(r,t)=\mH_e(t) \Phi_R(r,t).
\eeq
As initial state we can choose the exact initial state of the full system 
$\Phi(R,r,0)=\Psi(R,r,0)$. The TDSE becomes
\bea
i\ddt \Psi
&=&[i\ddt \chi(R)]\Phi_R + \chi(R) \mH_e \Phi_R\\
&=& [\mT\chi(R)]\Phi_R - \frac12[\partial_R\chi(R)]\partial_R\Phi_R + \chi(R)\mT\Phi_R +  \chi(R) \mH_e \Phi_R
\eea

Formally, we can close the equations the equations by $\Phi_R(r,t)$ to obtain (integral over $R$ implied):
\bea
\langle\Phi_{S}|\Phi_R\rangle i\ddt \chi(R)
&=&\langle\Phi_{S}|\Phi_R\rangle \mT\chi(R) - \frac12\langle\Phi_S|\partial_R\Phi_R\rangle \partial_R\chi(R) + 
\langle\Phi_S|\mT\Phi_R\rangle\chi(R).
\eea
Note that this includes a severe approximation: the functions $\Phi_R(r,t)$ are far from complete
at any moment in time. Improvements over this approximation will be discussed later. 
The overlap matrix will be non-singular, in a finite disretization one would choose discretization points for $R$
sufficiently different.

Then the equation for $\chi(R,t)$ can be written as
\beq
 i\ddt \chi
=\mT\chi - \mO\inv1 \mD \partial_R\chi +\mO\inv1 \mQ\chi.
\eeq
with the obvious definitions
\beq
\mO_{SR}:=\langle\Phi_{S}|\Phi_R\rangle,\quad\mD_{SR}:=\langle\Phi_{S}|\partial_R\Phi_R\rangle,\quad\mQ_{SR}:=\langle\Phi_{S}|\mT\Phi_R\rangle
\eeq

\subsection{Non-trivial systems}

The approach above can play out its advantages only if across the grid of the nuclear coordinates the electronic factors
can made to be changing slowly. If this holds, it may be useful to exploit this similarity also in the time-propagation
of the $\Phi_\vR(\vr,t)$ at neighboring nuclear cooridinates. Let us denote $\Phi_0(\vr,t)$ and $\Phi_1(\vr,t)$ as two such
solutions and let us assume we have already obtained a solution $\Phi_0$ at point $\vR_0$. The question arises, which 
advantage we can draw from the knowledge of $\Phi_0$ for dertermining $\Phi_1$ at a neighboring point $\vR_1$. The Hamiltonian
for the two solutions differ $H_1(t)=H_0(t)+V$, where $V$ is time-independent. Similarity of the solutions could be expressed 
in a variety of ways:
\bea
\Phi_1 &=& \xi\Phi_0\\
       &=& \phi+\Phi_0\\
       &=& \eta\arg(\Phi_0) + \Phi_0
\eea
The purpose of these splittings is to find the unknown to be slowly varying in time. $\xi$ very likely is not 
a good candidate, as nodes of the solution will be depending on $\vR$ may cause (near-)singularities to move in time.
The plain difference $\phi$ will have propagation equations very similar to $\Phi_0$, inheriting all numerical 
stiffness from it. Local kinetic energy may be reduced to the minimum possible by using any phase oscillation in $\Phi_0$
to precondition the time-evolution of $\eta$.

The considerations above appear rather hopeless. Instead, we would rather try to use the ``gauge'' freedom to keep
the $\chi(R,t)$ as smooth as possible at all times such that it can be sampled on a coarse grid. First and, possibly,
second derivatives wrt.\ $R$ may be obtained the solving the corresponding dynamical equations. The initial condition
for the propagations may be best obtained by numerical differentiation, where eigenvalues at different $R$ can
be obtained by root tracing.

The ``gauge'' determines just how oscillatory the nuclear wave packet becomes, thus determining the 
the number of sample points needed. It would not arise as a numerical problem, if we can define reasonable equations for
modulus and phase separately. Alternatively, we may ``fix'' the gauge by the constraint that 
the nuclear kinetic energy be minimal at any point in time.

\subsubsection{Exact factorization}

A suitable choice could be to minimize the phase oscillations of $\chi(R,t)$ and throw all these
oscillations onto $\Phi_R(r,t)$. 
A solution where the phase of $\chi(R,t)$ reflects the
nuclear current is given byt he ``exact factorization'' [Abedi2010]. There is a relevant price to pay 
for this, namely that the actual time-evolution acts back on the factorization, introducing a 
non-linearity into the equations. The non-linearity is mediated by scalar and vector potential terms
$\ep(R,t)$ and $\vA(R,t)$ that appear in the equations for $\Phi_R(\vr,t)$ which themselves are
functionals of $\Phi_R(\vr,t)$.  Assuming this non-linear effect to be small, we can use 
a piece-wise constant approximation and, possibly, use a slightly different factorization that
remains close to the exact one.

The gauge-invariant nuclear currents in this scheme are
\beq
\Im \l \chi | \vna \chi \r + |\chi^2|\vA
\eeq
In a numerical scheme, assuming that the current is the only relevant source of phase oscillations, 
we want to have the vector potential $\vA=0$. This would amount to a constraint
\beq
0=\l \Phi_R(t)|-i\partial_R\Phi_R(t)\r
\eeq

 A second consideration would be that the physical current may
not always be the least oscillating, as, for example, in length vs.\ velocity gauge. However, 
this may be of minor concern, as we plan to ingnore, for the time being, the direct intercation of
the field with the nuclei.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "tsurff"
%%% End: 
