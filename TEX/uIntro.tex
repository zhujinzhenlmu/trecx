\section{Basic definitions}
tRecX is a general environment for solving homogeneous and inhomogeneous initial value problems of the form
\beq
\ddt \Psi = D[t,\Psi]+ \phi(t)
\eeq
where $D$ is general map from a Hilbert space onto itself. The primary design
is for $D$ being local and linear, but non-local and non-linear operators can be implemented.

\subsection{Discretization}
The package handles discretizations in the form of an $n$-dimensional index space by a hierarchy of discretization functions in the form
\beq
\Psi(\rangesub{q}{n})=\sum_{i_{0}=0}^{I_{0}}\sum_{i_1=0}^{I_1(i_0)}\ldots\sum_{i_{n-1}}^{I_{n-1}(i_0\ldots i_{n-2})}
f\up0_{i_0}(q_0)f\up{i_0,i1}_{i_1}(q_1)\ldots f_{i_{n-1}}\up{i_0\ldots i_{n-1}}(q_n)C_{i_0\ldots i_{n-1}}
\eeq
The coordinates $q_i$ can denote continuous coordinates (radii, angles, cartesian, etc.) als well as discrete coordinates
(angular momentum, spin, ionic state, etc.). 

\subsubsection{Example: spherical harmonics}
Note that all functions lower in hierarchy may be dependent on all indices higher in hierarchy.
By that, for example, one can implement spherical harmonics $Y_{lm}(\th,\phi)$ by identifying $i_0=2|m|+(1-sign(m))/2$, $i_1=l-|m|$ and the functions
$f\up0_m=\exp(im\phi)$ and $f\up{m}_l=P\up{|m|}_l(\eta)$ with $\eta:=\cos\th$

\subsubsection{Example: finite elements}
The design specifically allows for finite elements discretization by identifying one index $k>l$ with the label of 
an interval for a coordinate $q_{l}\in [q\up{k},q\up{k+1}]$. The union over all $k$ gives the whole range of coordinate $q_l$ covered by the
the discretization. Continuity at the element boundaries is handled by the code and thus allows use of up to second order differential equations.


\subsection{Index hierarchy and floor}

The indices are grouped into two parts $i_0\ldots i_{h-1}$ and $i_h\ldots i_{n-1}$, the index hierarchy and the index ``floor''
In a typical application, the operators will be sparse \wrt the indices in the hierarchy and full on the floor level.
 This choice of grouping is for efficiency consideration, but the code
will work allso for full operators on the hiearchy index level or sparse operators on the floor level.

\subsubsection{Example: expansion in spherical harmonics and radial coodinate}
The top hierarchy level can be chosen as the $m$-quantum number, the next level als $l$,
then follows $n$, the finite element interval $[r\up{n},r\up{n+1}]$ with $r\up0=0$ and $r\up{N}=R$
defining the overall box size. The hierarchy depth is $h=3$.
The floor level numbers the discretization functions on a given interval $i_3=k$ is one-dimensional and $f\up{lm3}_k(r)$
is some suitable local basis set.
Rotationally symmetric operators are block-diagonal, dipole operators are block-tridaonal \wrt to the indices $m$ and $l$.

The overall expansion is
\beq
\Psi(\varphi,\cos\th,r)=\sum_{i_0=0}^{2M}\sum_{i_1=0}^{L-|m|}\sum_{n=0}^{N-1}\sum_{k=0}^{K}
e^{im\varphi}P\up{m}_{l}(\eta) f\up{n}_k(r)C_{i_0i_1nk},\qquad i_0=2|m|+(1-\sign(m))/2, l=|m|+i_1.
\eeq
We denote $\eta=\cos\theta$.
Note that hierarchy indices are non-negative and usually start from 0. Therefore we re-label the conventional
quantum numbers $m=-M,\ldots,M$ and $l\geq |m|$ by $i_0,i_1$.

\subsubsection{Example: 2-particle spherical harmonics}
The angles are discretized by spherical harmonics. Here we may choose a the top level the finite element interval $n_1$ for the
$r_1$-coordinate, then $n_2$. Next we can choose $M=m_1-m_2$, further $m_1$, further $l_1$ and $l_2$. The hierarchy
depth is $h=6$. The floor level is 2-dimensional with the areas $[r_1\up{n_1},r_1\up{n_1+1}]\times[r_2\up{n_2},r_2\up{n_2+1}]$.
The overal expansion is 
\bea
\Psi(\varphi_1,\eta_1,r_1,\varphi_2,\eta_2,r_2)&=&\sum_{n_1n_2Mm_1l_1l_2k_1k_2}
e^{im_1\varphi_1}e^{i(M-m_1)\varphi_2}P\up{m_1}_{l_1}(\eta_1)P\up{m_2}_{l_2}(\eta_2)\\ 
&&\qquad\qquad f\up{n_1}_{k_1}(r_1) f\up{n_2}_{k_2}(r_2)C_{n_1n_2Mm_1l_1l_2k_1k_2}.
\eea
Here, again, in the actual implementation, indices should all be re-labelled to start from 0.


\subsection{Form of the operators}
It is assumed that operator matrices can be written as a finite sum of tensor products
\beq
\sum_{n_0} O\up{n_0}_{i_0,j_0}\otimes \sum_{n_1} O\up{n_0,n_1}_{i_1,j_1}\otimes\ldots\otimes
\sum_{n_{h}}O\up{n_0,\ldots,n_{h}}_{i_h\ldots i_n,j_h\ldots j_n}
\eeq
Explict correlation will be needed, e.g., if a potential correlates different coordinates, 
like in the case of the coulomb potential when written in cartesian 
coordinates. 

\subsubsection{Example: $-\Delta$ in $xy$-coordinates}
Assuming again finite elements with hierachy $n_x,n_y$, the operator can be written as
\beq
\one\otimes\one\otimes\sum_{n_x,n_y} O\up{n_xn_y,n_3}_{i_xi_y,j_xj_y}
\eeq
All upper hierarchy operates can be set to identity. Note that the floor level operator itself can be written as a short
sum of tensor products
\beq
 O\up{n_xn_y,n_3}_{i_xi_y,j_xj_y}=D\up{n_x}_{i_x,j_x}\otimes S\up{n_y}_{i_y,j_y}+S\up{n_x}_{i_x,j_x}\otimes D\up{n_y}_{i_y,j_y}
\eeq
with the tensor factors
\beq
S\up{n}_{i,j}=\int_{x_n}^{x_{n+1}} dx' f\up{x}_i(x')f\up{x}_j(x')\text{ and } 
D\up{n}_{i,j}=\int_{x_n}^{x_{n+1}} dx' \left[\ddx f\up{x}_i(x')\right]\left[\ddx f\up{x}_j(x')\right]
\eeq
Tensor structure for the floor levels is implemented and used in the code, but the general structure is admissible as well as 
specific mutliplication operators.

\subsection{Definition of a discretization}
A few discretizations are available: 1d and 2d Cartesian, polar coordinates, prolate-spheroidal coordinates, as well as discretizations combining a basis consisting
of quantum chemical states given in terms of determinants molecular orbitals anti-symmetrized with a finite-element continuum basis. Also, a more flexible generic discretizationn
is available, where the user unly needs to define the meaning of axes and operators.

For adding a discretization that is not coverd by the above cases, the user must define the following
\begin{itemize}
\item The depth $h$ of the index hierarchy and memnotic names for the individual levels of the hierarchy
\item The total number of inidices $n$, which also defines the dimension of the floor $n-h$
\item The number (and, optionally, kind) of functions $f\up{i_0\ldots i_{k-1}}_{i_k}(q_k)$ for each level of indices
\item The operators $O\up{n_0,\ldots,n_{h}}_{i_h\ldots i_n,j_h\ldots j_n}$ for the respective hierarchy levels. 
For a set of standard basis sets, the code provides automatic, error-free integrations (well, if the integrals exist)
\item If finite element functions are used, the functions for each interval $[q\up{i_k},q\up{i_{k+1}}]$ 
must all be on the floor level. The user must indicate which hierchy indices $i_{k}$ correspond to floor indices $i_{h+k}$.
\end{itemize} 

Technically, the user derives a new discretization class from an abstract base class \verb"Discretization".

\section{Usage}
Documentation of the code is in-line (largely accessible through the Doxygen-generated documenation). Here we only 
illustrate usage on a few examples.
More usage examples are collected in the subfolder ``examples''. For more complete lists of input options and 
variables, consult (in this sequence) the \verb"*.doc_input" files, the Doxygen documentation and the code itself.
Many classes have a \verb"test" or \verb"usage" (static) member function which illustrates usage options.

\subsection{Basic usage}
The most elementary usage of the code is to control it purely by input text files.

\subsubsection{Axis input} 
 This allows the definition 
of a coordinate ``axis'', like X, Y, Z, R (for polar radial), Phi (for polar azimuthal), L (for discrete angular momentum) etc.
On each coordinate, a range of discretization functions can be used.
\\
\\
{\bf Example} Polar coordinate axes, discretized with spherical harmonics $Y_{lm},m=-1,0,1,l=0,1,2,3,4$
and radial discretization with approximately 120 discretization coefficients on the interval $r\in[0,60]$, with 20 Legendre polynomials (degrees 0 to 19)
on 120/20=6 equal size finite elements: 
\begin{verbatim}
Axis: coordinate,nCoefficients,lower end, upper end,functions,order
 Phi,3
 Eta,5
 R,120,0.,60.,legendre,20
\end{verbatim}

\noindent
{\bf Example} 2-d Cartesian coordinates for a 3-component vector
\begin{verbatim}
Axis: coordinate,nCoefficients,lower end, upper end,functions,order
 Vec, 3
 X,64,-10,10,legendre,8
 Y,64,-10,10,legendre,8
\end{verbatim}

\subsubsection{Operator input}
Operator definitions can be read in a strings. A plausible scripting implements a range of multiplicative an 
derivative operators. 
\\
\\
{\bf Example} Polar coordinates as above, the Hamiltonian of the hydrogen atom can be input as
\begin{verbatim}
Operator: hamiltonian
1/2<d_1_d><1><1>+<1/(Q*Q)><J><d_(1-Q*Q)_d>+<1/(Q*Q)><d_1_d><1/(1-Q*Q)>-<1/Q><1><1>
\end{verbatim}
where \verb"<...>" indicates integration over the coordinate axis. \verb"_d" indicates a first derivative
applied to the rhs function, \verb"d_" the first derivative applied to the lhs function, and \verb"d_1_d" is the explicitly symmetric, discrete 
representation of minus the laplacian $-\Delta$. $Q$ generically indicates the coordinate at the given tensor factor.
\\
{\bf Example} Two-dimensional wave equation operator 
\beq
D=\bma 0 & i\partial_x & i\partial_y \\ -i\partial_x & 0 & 0 \\ -i\partial_y & 0 & 0 \ema
\eeq
can be input as follows (matching the axis definition above)
\begin{verbatim}
Operator: derivative
i(<0|1><1_d><J>+<0|2><1><1_d>+<1|0><d_1><1>+<2|0><1><d_1> 
\end{verbatim}
Note that the construction is explicitly hermitian. This is required, as the underlying finite element discretization is 
on the 1st Sobolev space. Using, e.g. \verb"-i<1|0><1_d><1>" instead of  \verb"+i<1|0><d_1><1>" (partial integration) 
produces a non-hermitian discretization which violates energy conservation.

\subsubsection{Time propagtion}
Start and end times, printout-intervals and output intervals can be input as follows
\begin{verbatim}
TimePropagation: begin,end,print,store
0,10,1,0.1
\end{verbatim}

\subsubsection{Plotting}
Wave functions can be evaluated on a user-defined grid and output to a (gnuplot-style) text file
The standard form of defining the output grid is, e.g.
\begin{verbatim}
Plot: coordinate,points,lowerBound, upperBound
       X,100,-5,7
       Y,50,-7,3
     Vec,2,1,2
\end{verbatim}
which writes a text file with 4 columns, $x$-coordinate, $y$-coordinate, $|\phi_1(x,y)|^2$, $|\phi_2(x,y)|^2$

\subsection{Advanced usage}
For that you can take advantage of all available classes. Only very few of them are at the core. Apart from various 
I/O, these are the abstract base class \verb"Discretization", which in turn largely depends on \verb"Index",
\verb"Coefficients" and classed used in these two. These should not be altered by the user in any way. Doing this is
surgery on the open heart. Rather, the user should just write his or her instance of the abstract base class \verb"Discretization". 

For that you need to provide the code with the ``hierarchy'' of the indices
you want to use in your discretization. The depth of the hierarchy is defined \verb"vector<string> hierarchy".

Say, you want discretize 3-dimensional space in polar coordinates with a finite element discretization of the radius,
you might choose, say \verb" Nradius, Mqn, Lqn, relement "  
element discretization of the radius. 

You may need to define a member function \verb"getBasis" that returns the desired basis set corresponding to a given index, 
say $\exp[im\varphi]$ when the second hierarchy level index has value $m$.

You are free to define your \verb"operatorData" member, which will return the block-matrices the make up the complete operator. 

There is almost complete flexibility within the general mathematical framwork defined above.
See \verb"class Polar" for a rather elaborate usage example. 

\section{Methods}
At present, the code in particular implements two key methods: irECS (infinite range exterior complex scaling, \cite{scrinzi10}) for 
highly efficient absorption with parabolic $D$-operators, typically quantum-mechanical Hamiltonians and
tSURFF (the time-dependent surface-flux method, \cite{tao12,scrinzi12}) for computing photo-electron spectra from atoms and molecules.
For usage, see the example section. A description of the specific finite element implementation used can be found in \cite{scrinzi10}.

Trivia: you may recognize the (photonetic) origin of tRecX=tSURFF+irECS (special thanks to Mattia Luppetti for the catchy acronym!).

\section{Parallelization}
The code is MPI parallelized and was tested with up to 30000 CPUs. In typical usage, band-width requirements remain high and
a careful layout can be advisable. For example, in the 2-electron system, matrices are full \wrt angular indices. Therefore, these
should share memory. The radial finite element discretization is largely local with only continuity conditions and application 
of the inverse overlap requiring communication. Non-local parts of the inverse overlap are implemented through low-rank updates
with minimal communication.  

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "tRecX"
%%% End: 
