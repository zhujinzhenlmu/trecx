\section{Molecular breakup}

Different from double-electron emission, there can be attractive forces between the 
fragments of molecular breakup. While with repulsive forces the configuration space 
where the fragments are close can be safely ignored, with attractive forces this part
of the space must be treated explicitly. From a fundamental point of view, we must
define the asymptotic regions such that one can safely exclude that fragments can
rearrange from one asymptotic region into another one. Fundamentally, if the concept
of emission into various channels is meaningful at all for a system, then there is
also such an asymptotic region. The profound mathematical question, whether the complete
Hilbert space can be mapped into such asymptotic regions is not addressed here.

For simplicity, assume strictly finite range pairwise interactions
\beq
V(\vr_{ij})=0\quad \text{ for }|\vr_{ij}|>R_v
\eeq 
Also, for simplifying the discussion, assume all length scales to be equal for now.
We conduct the argument for three-particle break up, as it would happen in 
\beq
H_2 \to \left\{
\bar H_2^+ + e\\ H^- + p \\ H + H 
\ear
\right\}\to H + p + e 
\eeq
In this example, the asymptotic regions must be far enough such that recombination
can be excluded once the system has entered any of the other asymptotic regions:
\[
H+p+e\not\to H+H.
\]

\begin{figure}
  \begin{center}
    \setlength{\unitlength}{2.mm}
    \begin{picture}(45,45) 
\linethickness{0.05mm}
      \put(0,0){\line(0,1){35}} 
     \put(0,0){\line(1,0){35}}
      \put(25,0){\line(0,1){25}} 
      \put(0,25){\line(1,0){25}} 

% alterante region
\put(10,25){\line(1,-1){15}}
 
% diagonal channel
\linethickness{0.2mm}
\put(0,7){\line(1,1){30}}
\put(7,0){\line(1,1){30}}

%horizontal channel
\put(0,10){\line(1,0){35}}

%vertical channel
\put(10,0){\line(0,1){35}}

%  flows
\put(32,5){\vector(0,1){10}}
\put(35,20){\vector(-1,1){7}}
\put(26,26){\vector(1,-1){7}}
%annotation
      \put(-6,9){$R_W$}
      \put(9,-3){$R_U$}
      \put(-6,24){$R_c$}
      \put(24,-3){$R_c$}

      \put(-6,33){$|\vr|$} 
      \put(35,-3){$|\vR|$}

      \put(12,12){\Large Full} 
      \put(28,30){III: H+H} 
      \put(2,30){II: $H_2^+$+e}
      \put(30,3){I: $H^-$+p} 
      \put(33,16){IV: H+e+p} 
      \put(13,33){IV: H+e+p} 

      \put(24,14){Small} 
      \put(29,11){OK} 
      \put(17,19){\rotatebox{-45}{Alt}} 
      \put(30,27){\rotatebox{-45}{Forbidden}} 
      \put(29,20){\rotatebox{-45}{OK}} 

      \put(-3,-3){0}
%      \put(33,20){\rotatebox{30}{$l_1,l_2,m_1$}}


    \end{picture}\vspace*{2ex}
\end{center}
\caption{\label{fig:breakup} Partitioning of configuration space for breakup. Remark: the path through I is the 
unlikely one, used here only for illustration. }
\end{figure}

For tSurff, and any method that endeavors to represent the asymptotics by product functions,
there is a more stringent condition: not only the possibility for recombination, but all interactions
between the fragments must be negligible once the system has reached the asymptotic channel region.
This applies only in presence of a laser field, as good approximations to fragment scattering 
may be obtained in absence of a laser field.

Let us now choose the specific Hamiltonian of a simplified $H_2$ consisting of an inseparable $H$,
a proton $p$ and $e$. Also, we only let the electron interact with the laser. This bypasses several
important issues that arise in real $H_2$, but for the purpose of fragmentation it is sufficient. We can further fix 
the $H$ at the origin with the remaining coordinates $\vr$ and $\vR$:
\beq
H(\vR,\vr)=-\frac{1}{2M}\Delta_R-\frac12\Delta_r+i\vA\cdot\vna_r+U(R)+W(r)+V(|\vr-\vR|)
\eeq

Outside the reach of the potentials, we have the channel Hamiltonians (labeled by region)
\bea
H_I&=&-\frac{1}{2M}\Delta_R-\frac12\Delta_r+i\vA\cdot\vna_r+U(R)\text{ for }r>R_W,|\vr-\vR|>R_V
\\
H_{II}&=&-\frac{1}{2M}\Delta_R-\frac12\Delta_r+i\vA\cdot\vna_r+W(r)\text{ for }R>R_U,|\vr-\vR|>R_V
\\
H_{III}&=&-\frac{1}{2M}\Delta_R-\frac12\Delta_r+i\vA\cdot\vna_r+V(|\vr-\vR|)\text{ for }R>R_U, r>R_W
\eea

In tSurff, to the extent that flux through the forbidden surfaces and rearrangement between asymptotic 
regions is negligible, one can apply standard procedures. Clearly, for the channel III, inter-fragment
coordinates would be advantageous to use.

The tSurff radius must be chosen large enough, such that the surface regions towards the individual 
channels remain well separated, such that flux can unambiguously assigned to exactly one of the 
channels. This implies that already in the inner region there are domains in configurations space
where probability is near negligible: the wedge-shape areas labeled ``Small'' in Fig.~\ref{fig:breakup}.

With attractive forces between the fragments, the line $\vr=\vR$ will be a region of high probability density 
and it will exhibit a high degree of correlation between the coordinates $\vr$ and $\vR$. On a grid, this
does not pose a major problem. With an angular momentum expansion or similar, such correlations will
require very expensive, large expansions. 
It may help to absorb along a anti-diagonal line in order remove parts of the configuration space with 
high correlation, line ``Alt'' in Fig.~\ref{fig:breakup}.

Assuming the problem can be solved with sufficient accuracy in the ``Full'' region, tSurff allows for a
comparatively cheap transition to inter-particle coordinates in III, as only the surface values and derivatives
need to be evaluated along the anti-diagonal line and this evaluation needs to be done only at the time-intervals
needed to resolve the energies going into III.

It is not obvious at the moment, whether irECS absorption would be easy to implement across the anti-diagonal.
Certainly there will be numerical issues, as the change in the wave-function again would ``correlate''
the coordinates $\vr$ and $\vR$.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "breakup"
%%% End: 
