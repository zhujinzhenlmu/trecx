Title:
Tutorial example  - IR photo-electron spectrum of hydrogen

First, run 'tRecX' with this input file.
Then compute the photo-electron spectra with 'Spectrum' and the output directory as input

The purpose of this exercise is to demonstrate scaling with propagation time

# TIMING
Run time for tRecX with given parameters about 7 min on a decent CPU for tRecX
Run time for Spectrum grows linearly with momentum resolution, e.g. 120 sec at -nR=200

[TASK] run Spectrum, inspect the spectra obtained by Rc=20 and 30
[TASK] perform convergence studies
the initial parameters in this file are adjusted to yield ~ 1% accuracy in much of the spectrum
# Verify by varying:
# Eta-axis  (i.e. angular momenta)
# Rn-axis   radial basis
# TimePropagation: accuracy
# TimePropagation: cutEnergy

#define BOX 20

# Rc - where to pick up the surface flux
Surface: points=BOX

# basis for about 1% good results (some optimization tried)
Axis: name,nCoefficients,lower end, upper end,functions,order
 Phi,1
 Eta,30,-1,1, assocLegendre{Phi}
 Rn,40, 0.,BOX,polynomial,20
 Rn,20, BOX,Infty,polExp[0.5]

Absorption: kind, axis, theta, upper
ECS,Rn,0.3,BOX

# potential in the Hamiltonian - as in 1d, we clip the Coulomb tail off

Operator: hamiltonian='1/2<<Laplacian>>-<1><1><trunc[15,20]/Q>'
Operator: interaction='iLaserA0[t]<<D/DZ>>'
Operator: expectationValue='<1><1><chi[0,20]>'

Laser: shape, I(W/cm2), FWHM, lambda(nm)
cos8, 2.e14, 10. OptCyc, 800.

TimePropagation: end,print,store,cutEnergy,accuracy,operatorThreshold
30 OptCyc, 0.25 OptCyc,0.5 au,50,5.e-9,5.e-8
