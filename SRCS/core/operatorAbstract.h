// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#ifndef OPERATORABSTRACT_H
#define OPERATORABSTRACT_H
#include "abort.h"
#include <vector>
#include <complex>
#include <cfloat>
#include "linSpaceMap.h"
#include "labelled.h"

#include "qtEigenDense.h"


class Coefficients;
class Index;
class UseMatrix;

/** @defgroup Operators
 * \brief definitions, structures, evaluation
*/


/** @defgroup Structures
 * \ingroup Operators
 * \brief maps between Coefficients: recursive, spectral projections, diagonal operators, etc.
 *  @{
*/

/// Abstract base class for any map between recursively Index'd spaced
class OperatorAbstract: public LinSpaceMap<Coefficients>, public Labelled<OperatorAbstract>{
    Coefficients * _tempRHS;
    Coefficients * _tempLHS;

protected:
    double time;
    /// add contract overall matrix and add into GMat (optionally times Factor)
    virtual void matrixContract(const UseMatrix &Mat, UseMatrix &GMat, std::complex<double> Factor=1.,std::vector<int>ISort=std::vector<int>(0),std::vector<int>JSort=std::vector<int>(0)) const;
    std::string definition;

public:
    std::string name;
    const Index *iIndex, *jIndex;     ///< left and right indices of operator

    std::string def() const {return definition;}

    virtual ~OperatorAbstract();
    OperatorAbstract():name("undefined"),definition("n/a"),iIndex(nullptr),jIndex(nullptr),_tempLHS(nullptr),_tempRHS(nullptr){}
    OperatorAbstract(std::string Name,const Index* IIndex,const Index* JIndex)
        :name(Name),definition("n/a"),iIndex(IIndex),jIndex(JIndex),time(0.),_tempLHS(nullptr),_tempRHS(nullptr){}

    /// required for abstract base class LineSpaceMap
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const=0;
    virtual void update(double Time, const Coefficients* CurrentVec=nullptr){} ///< required for LineSpaceMap
    const Coefficients & lhsVector() const {return *tempLHS();}///< required for LineSpaceMap
    const Coefficients & rhsVector() const {return *tempRHS();}///< required for LineSpaceMap

    virtual void axpy(std::complex<double> Alfa,const Coefficients& X,std::complex<double> Beta, Coefficients& Y, const double Time);

    virtual void axpy(std::complex<double> A, const Coefficients &X, std::complex<double> B, Coefficients &Y){apply(A,X,B,Y);}
    virtual void axpy(const Coefficients &X,Coefficients &Y){apply(1.,X,1.,Y);}

    Coefficients* tempLHS() const; ///< pointer to storage of lhs vector
    Coefficients* tempRHS() const; ///< pointer to storage of rhs vector

    /// print minimal info
    virtual std::string str(std::string Info="") const;// {return name+" (no structure information defined)";}

    /// convert to full matrix, column-wise storage in Mat (slow)
    virtual Eigen::MatrixXcd matrix() const; ///< convert to full matrix
    virtual void matrix(std::vector<std::complex<double> > & Mat) const;
    virtual void matrix(UseMatrix & Mat) const;///< convert to full matrix, column-wise storage in Mat (slow)

    virtual double norm() const;

    UseMatrix matrix(std::vector<Coefficients*> Left, std::vector<Coefficients*> Right) const;

    /// convert to sub matrix, column-wise storage in Mat (slow)
    virtual void subMatrix(std::vector<std::complex<double> > & Mat,const Index* ISub, const Index* JSub) const;

    /// add operator to matrix, create new if empty matrix
    virtual void matrixAdd(std::complex<double> factor, UseMatrix & Mat) const;

    /// (pseudo-)orthonormalos wrt to Op: 1=<C|Op C> (<C^*|Op C> for pseudoScalar)
    void orthoNormalize(std::vector<Coefficients *> &C, bool pseudo=false) const;

    /// (pseudo-)orthonormalize near-degenerate subspaces wrt. Op
    void orthoNormalizeDegenerate(std::vector<std::complex<double> > Eval, std::vector<Coefficients *> Evec, bool Pseudo) const;

    /// <wf1|Op|wf2>, if pseudoScalar <wf1*|Op|wf2>
    virtual std::complex<double> matrixElement(const Coefficients &Ci, const Coefficients &Cj, bool pseudoScalar = false) const;

    std::complex<double> matrixElementUnscaled(const Coefficients &Ci, const Coefficients &Cj) const;///< <wf1*|Op|wf2>
    
    double applicationCost() const;
    virtual long applyCount() const{ ABORT("applyCount not implemented"); }

    virtual bool isBlockDiagonal() const;///< only Tree-type operators can be block-diagonal

    virtual std::string getDefininition() const {return definition;}

    bool isHuge(std::string Message="") const;
    virtual bool isIdentity(double Eps=1.e-12, bool Stochastic=true) const;
    virtual bool isZero(double Eps=0.,bool Stochastic=true) const;
    virtual bool isDiagonal() const;
};
/** @} */ // end group Operators


#endif // OPERATORABSTRACT_H
