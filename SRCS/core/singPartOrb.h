// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef SINGPARTFUNC_H
#define SINGPARTFUNC_H

#include <vector>
#include <string>

class UseMatrix;
class ReadInput;
class Gaussian;

class SingPartOrb
{
public:
    SingPartOrb();
    virtual ~SingPartOrb(){}

    /// values at a set of cartesian points
    virtual void val(std::vector<std::vector<double> > XYZ, UseMatrix & Val) const;
    virtual unsigned int size() const {return trans.cols();}
    virtual void readTrans(ReadInput & Inp, const std::string Type);

private:
    const Gaussian gauss;
    UseMatrix trans;
};

#endif // SINGPARTFUNC_H
