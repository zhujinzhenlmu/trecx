// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef PARALLELOPERATOR_H
#define PARALLELOPERATOR_H

#include "blockView.h"

class ParallelProcess;

/// parallel operations on OperatorTree
class ParallelOperator
{
    OperatorAbstract * _op;
    BlockView _view;

    static std::map<std::string,int> _host;
public:
    static bool emptyLeafOrDummy(const OperatorTree* Op);
    static void sync(OperatorTree* Op);
    static void bcast(OperatorTree* Op);

    static constexpr int undefined=-1;
    static constexpr int none=-2;
    static constexpr int all=-3;
    static constexpr int distributed=-4;
    static int getHost(const OperatorAbstract* Op);
    static std::string strHost(const OperatorAbstract* Op);
    static void unsetHost(const OperatorTree *Op);

    ParallelOperator(const OperatorAbstract *Op);

    /// host for a given OperatorTree
    class Host{
    public:
        virtual ~Host(){}

        /// return host number for Op
        virtual int operator() (const OperatorTree* Op) const =0;
    };

    /// actual hosting of OperatorTree leaf (all/none/distributed)
    class PresentHost:public Host{
    public:
        PresentHost(){}
        int operator() (const OperatorTree *Op) const;
    };
    class SingleHost:public Host{
        int _host;
    public:
        SingleHost(int Host):_host(Host){}
        int operator() (const OperatorTree *Op) const{return _host;}
    };

    /// hosting according to parallel on parallel processes
    class ProcessHost:public Host{
        std::map<const OperatorTree*,int> _proc;
    public:
        ProcessHost(const std::vector<ParallelProcess *> &Proc);

        /// assign Op to one single Host
        ProcessHost(const OperatorTree* Op, int Host);

        int operator()(const OperatorTree *Op) const;
    };

    /// distribute diagonal blocks of operator (include overlap diagonality)
    class DiagonalBlockHost:public Host{
        const OperatorTree * _root;
    public:
        /// node count relative to Root
        DiagonalBlockHost(const OperatorTree *Root):_root(Root){}
        int operator()(const OperatorTree *Op) const;
    };

    /// record distributon (for later recovery)
    class SavedHost:public Host{
        std::map<const OperatorAbstract*,int> savedHost;
    public:
        /// node count relative to Root
        SavedHost(const ParallelOperator* Par);
        int operator()(const OperatorTree *Op) const;
    };

    static void setDistribution(const OperatorAbstract* Op, const Host& Hosts=PresentHost());
    void reDistribute(const Host& NewHosts); ///< move floor data to hosts according to NewHosts
    void setDistribution(){reDistribute(PresentHost());} ///< determine and register distribution

    void bcast();                        ///< distribute floor data to all processes
    void purge();                        ///< total purge: emtpy branches and zero-branches
    void fuse();                         /// fuse, but make sure parallel is OK
    void syncNormCost();                 ///< broadcast norm and application cost

    std::string str() const;
};

#endif // PARALLELOPERATOR_H
