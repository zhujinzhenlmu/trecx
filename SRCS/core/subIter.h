// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef SUBITER_H
#define SUBITER_H
#include "tools.h"

class Coefficients;

/// @brief subspace iteration class for Eigenvalues
///
/// get eigenvectors and values of and Operator in the vicinity of a complex value E0
/// use an invertible Op0 to generate new vectors: newVectors = (Op0-E0)^-1 Operator oldVectors
class SubIter
{
public:
    SubIter();

    void eigen(unsigned int Nvec, std::vector<Coefficients*> Rvec, std::complex<double> Eval, double Eps=1.e-12);
};

#endif // SUBITER_H
