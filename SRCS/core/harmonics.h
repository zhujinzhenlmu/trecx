// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef HARMONICS_H
#define HARMONICS_H

#include <string>
#include <vector>

class ReadInput;
class Plot;
class OperatorAbstract;

/// \ingroup Plot
/// \brief compute high harmonic respose from dipole data (uses FFTW)
class Harmonics
{
    int nPoints;
public:
    Harmonics(ReadInput & Inp, std::string Exten, std::string Sep, bool RowWise, double UnitT);
    static std::string dipoleDefinitions(ReadInput & Inp, const std::string &Coor, const std::string & Hamiltonian,
                                         std::vector<std::string> &Names);

    static void compute(int argc, char *argv[]);

    /// find dipole operators in OpList and add to density plots
    static void addDipolesToPlot(std::string DipoleDefinitions, Plot & PlotDef, const std::vector<OperatorAbstract *> &OpList);
};

#endif // HARMONICS_H
