// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASISMATMULTI_H
#define BASISMATMULTI_H
#include <string>
#include "qtEigenDense.h"
#include "index.h"
#include "basisMatAbstract.h"

class BasisAbstract;
class UseMatrix;
/// multiplicative operator (only in DVR, diagonal)
class BasisMatMulti: public BasisMatAbstract
{
    void construct(std::string Op, const Index * IIndex, const Index* JIndex,std::vector<std::complex<double> > &Diag);
protected:
    void _construct(std::string Op, const Index *IIndex, const Index *JIndex);
public:
    BasisMatMulti(){}
    BasisMatMulti(std::string Op, const Index * IIndex, const Index* JIndex){_construct(Op,IIndex,JIndex);}
};

#endif // BASISMATMULTI_H
