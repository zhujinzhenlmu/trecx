#ifndef PROJECTSUBSPACE_H
#define PROJECTSUBSPACE_H

#include <memory>
#include "operatorAbstract.h"
#include "index.h"

class OperatorTree;
/** \ingroup Structures */
///@brief Project onto a subspace
///
/// sequence of maps from full to expansion in terms of orthonormal subspace basis and back
class ProjectSubspace : public OperatorAbstract
{
    std::vector<int> _sorting;
    std::unique_ptr<const Index> _subspaceIndex;
    std::unique_ptr<OperatorTree> _mapFrom,_mapTo;
    std::unique_ptr<Coefficients> _subspaceC;
    void _construct(std::vector<const Coefficients*> Vectors, std::vector<const Coefficients*> Duals);
public:
    ProjectSubspace(std::vector<Coefficients*> Vectors, std::vector<Coefficients*> Duals);
    ProjectSubspace(std::vector<const Coefficients*> Vectors, std::vector<const Coefficients*> Duals){_construct(Vectors,Duals);}
    const OperatorTree* mapFrom() const { return _mapFrom.get();} ///< map from the reference discretization to vector
    const OperatorTree* mapTo() const { return _mapTo.get();}     ///< map to the reference discretization from vector
    const Index* subspaceIndex() const {return _subspaceIndex.get();}
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
    int dim() const {return _subspaceIndex->size();}

    bool verify() const;
    const std::vector<int> & sorting() const {return _sorting;}

    /// return the orbitals corresponding to projector
    std::vector<Coefficients> orbitals(std::vector<int> Select={} /** list of coefficients, defaults to all */);
    virtual ~ProjectSubspace();
};

#endif // PROJECTSUBSPACE_H
