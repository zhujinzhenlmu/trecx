// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef INVERSE_H
#define INVERSE_H

#include "operatorAbstract.h"
#include "coefficientsLocal.h"

class IIndex;
class JIndex;
/// Inverse composed of direct inverse S0^-1 and correction: S^-1(v) = correction( S0^-1(v) )
class Inverse: public Tree<Inverse>, public OperatorAbstract
{
protected:
    const OperatorAbstract* s0inv;
public:
    static const Inverse *factory(const Index *Idx);
    Inverse(const OperatorAbstract* S0inv):OperatorAbstract(S0inv->name,S0inv->iIndex,S0inv->jIndex),s0inv(S0inv){}
    Inverse(std::string Name, const Index* IIndex, const Index* JIndex):OperatorAbstract(Name,IIndex,JIndex),s0inv(0){}

    virtual void applyCorrection(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const=0;
    virtual void apply0(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const=0;
    virtual void project(Coefficients & Vec) const {} ///< project onto vector where inverse is defined

    /// for parallel application: use CoefficientsLocal
    virtual void parallelSetup() const {ABORT("no parallel setup implmented");}
    virtual void applyCorrection(std::complex<double> A, const CoefficientsLocal &Vec, std::complex<double> B, CoefficientsLocal &Y) const=0;
    virtual void apply0(std::complex<double> A, const CoefficientsLocal &Vec, std::complex<double> B, CoefficientsLocal &Y) const=0;

    virtual void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const
    {apply0(A,Vec,B,Y);applyCorrection(A,Vec,B,Y);}

    virtual void apply(std::complex<double> A, const CoefficientsLocal &Vec, std::complex<double> B, CoefficientsLocal &Y) const
    {apply0(A,Vec,B,Y); applyCorrection(A,Vec,B,Y);}

    void verify(const OperatorAbstract * Ovr) const; ///< print matrices S Sinv and Sinv S
    void assignToIndex(Index* Idx) const;
};

#endif // INVERSE_H
