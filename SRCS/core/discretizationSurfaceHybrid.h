// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef DISCRETIZATIONSURFACEHYBRID_H
#define DISCRETIZATIONSURFACEHYBRID_H

#include "operatorAbstract.h"
#include "discretizationSurface.h"

class DiscretizationHybrid;

/// \ingroup Discretizations
/// \brief surface for Neutral (+) Channels discretization
///
/// surface is strictly computed from the Channels part,
/// the neutral does not reach the surface
/// for now only for Neut+Chan type hybrids
class DiscretizationSurfaceHybrid : public DiscretizationSurface
{
public:
    class Map: public OperatorAbstract{
        const OperatorAbstract * surfMap;
    public:
        Map(const OperatorAbstract * SurfMap,const Index* ParentIndex):OperatorAbstract(SurfMap->name,SurfMap->iIndex,ParentIndex),surfMap(SurfMap){}
        void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const
        {surfMap->apply(A,*Vec.child(1),B,Y);}
        /// dummy update function for surface
        void update(double Time, const Coefficients* CurrentVec=0){}
    };

    DiscretizationSurfaceHybrid(const DiscretizationHybrid *Parent, const std::vector<double> &Rad, unsigned int NSurf);
};

#endif // DISCRETIZATIONSURFACEHYBRID_H
