// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef ROTATEBASIS_H
#define ROTATEBASIS_H

#include <vector>
#include <complex>
#include <map>
#include <index.h>


class VectorComplex;
class Index;
class Coefficients;
class BasisSet;


/// \ingroup Basissets
/// \brief rotatation matrix U[l][m,m'](theta)
///
/// for eigenbasis Q[l][m](phi,eta) of L^2
class RotateBasis
{
    static std::map<std::string,std::vector<std::vector<VectorComplex> > >tabLrot; ///< table of l-wise rotation matrices for given Index

    double angle;
    std::vector<std::vector<std::vector<std::complex<double> > > > uRot; ///< l-wise transformations to given rotation axis

    class IndexR: public Index{
    public:
        IndexR(const BasisSet* Phi, unsigned int LOrder, double M=0.);
    };

    void extract(Operator &Op, std::vector<VectorComplex> &LRot); // extract from standard operator to l-blocked storage
    std::vector<VectorComplex> transAx; // transformation to eigenstates of rotation wrt to rotation axis
    Coefficients *angC,*origView; // coefficients with Eta,Phi contiguously at floor, and view on them with original sorting
public:
    RotateBasis(const Index* Idx, std::vector<double> RotAx);
    void rotate(double Angle, const Coefficients *C, Coefficients *RotC);
};

#endif // ROTATEBASIS_H
