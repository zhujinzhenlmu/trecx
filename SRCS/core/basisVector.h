#ifndef BASISVECTOR_H
#define BASISVECTOR_H

#include "basisAbstract.h"
#include <map>
#include <memory>
#include "stringTools.h"

/** \ingroup Basissets */

///@brief Vector of complex numbers, replaces BasisSet(function useIndex)
class BasisVector : public BasisAbstract
{
    static std::map<std::string,std::unique_ptr<const BasisVector> >_list;
    unsigned int _size;
public:
    BasisVector(int Size):BasisAbstract("Vector"),_size(Size){}
    unsigned int size() const{return _size;}
    static const BasisVector* factory(std::string Def){
        auto pBas=_list.find(Def);
        if(pBas==_list.end()){
            if(Def.find("Vector:")!=0)ABORT("need format Vector:size, got: "+Def);
            _list[Def]=std::unique_ptr<const BasisVector>(new BasisVector(tools::string_to_int(Def.substr(Def.find(":")+1))));
            pBas=_list.find(Def);
        }
        return pBas->second.get();
    }
    std::string strDefinition() const {return "Vector:"+tools::str(size());}
};
std::map<std::string,std::unique_ptr<const BasisVector> > BasisVector::_list;

#endif // BASISVECTOR_H
