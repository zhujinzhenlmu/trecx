#ifndef CONVERTFROMGRID_H
#define CONVERTFROMGRID_H

#include <vector>

class Index;
class OperatorMap;

class ConvertFromGrid
{
    const Index *gridIdx, *basIdx;
    const OperatorMap* toBas;
public:
    ConvertFromGrid(const Index * GridIdx, std::vector<int> Deflate={} /** reduce basis size at level k by Deflate[k] compared to grid size (if specified) */);
};

#endif // CONVERTFROMGRID_H
