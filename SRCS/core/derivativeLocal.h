// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef DERIVATIVELOCAL_H
#define DERIVATIVELOCAL_H

#include "linSpaceMap.h"
#include "coefficientsLocal.h"
#include "coefficientsGlobal.h"
#include "derivativeFlat.h"
#include "mpiWrapper.h"

class DerivativeLocal : public LinSpaceMap<CoefficientsLocal>
{
    DerivativeFlat * der;
    CoefficientsLocal * model;
    CoefficientsGlobal * globX;
    CoefficientsGlobal * globY;
public:
    DerivativeLocal(){}
    DerivativeLocal(DerivativeFlat* Der):LinSpaceMap<CoefficientsLocal>(der->applyAlias()),der(Der){
        model=new CoefficientsLocal(Der->idx());
        globX=new CoefficientsGlobal(Der->idx());
        globY=new CoefficientsGlobal(Der->idx());
    }
    void apply(std::complex<double> A, const CoefficientsLocal &X, std::complex<double> B, CoefficientsLocal &Y) const{
           der->apply(A,const_cast<CoefficientsLocal*>(&X),B,Y);
    }
    const CoefficientsLocal & lhsVector() const {return *model;}
    const CoefficientsLocal & rhsVector() const {return *model;}

    void update(double Time, const CoefficientsLocal* CurrentVec=0){ der->update(Time,CurrentVec);}
};

#endif // DERIVATIVELOCAL_H
