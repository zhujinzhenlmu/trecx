// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#ifndef PLOT_H
#define PLOT_H

#include <memory>

#include "tools.h"
#include "useMatrix.h"
#include "plotCoefficients.h"
#include "plotKind.h"
#include "discretization.h"

class DiscretizationGrid;
class ReadInput;
class Coefficients;
class Index;
class OperatorAbstract;
class OperatorTree;


/** @defgroup Plot Plotting and analysis
 *  \brief plots (1d, 2d), spectral analysis, harmonics, Floquet etc.
 *  @{
 */

/// \brief evaluate densities  @f$\Psi^*@f$(args)@f$\Psi@f$(args) and optionally @f$\Psi^*@f$(args) (Op[k] @f$\Psi@f$)(args) to (ascii) file, gnuplot style
///
/// argument ("axis") ranges can be selected, arguments can be summed/integrated over, separate plots for some arguments fixed can be made
class Plot : public PlotCoefficients {
    //    const Discretization * disc;    //! starting discretization

    const Index * discIndex;                   //! alternate access to disc->Idx (disc is going to be eliminated eventually)
    std::shared_ptr<const DiscretizationGrid> dGrid;          //!< an intermediate grid discretization
    std::vector<const OperatorAbstract*> _ops; //! list of operators (=idenity if not specified)
    std::shared_ptr<const OperatorTree> _densityOp; ///< quadrature for axes that are integrated over

    std::string columnHeader; ///< column titles
    std::vector<std::vector<double> > axCols; ///< coordinate axes (in full length)
    mutable std::vector<std::vector<double> > _cols; ///< current columns, can be written to file
    unsigned int nDigits;     //!< number of digits in storage
    bool _append;             ///< append to exisiting _cols

    std::shared_ptr<Index> iFull; ///< indices for fullView and plotView
    std::shared_ptr<Index> iPlot; ///< indices for fullView and plotView
    std::shared_ptr<Coefficients> gvec;       ///< input converted to plot grid
    Coefficients* rightFullView;  ///< view of gvec with floor lower (MUST NOT BE A shared_ptr????)
    std::shared_ptr<Coefficients> rightView;  ///< permutation of fullView in the sequence for plotting
    std::shared_ptr<Coefficients> leftView;   ///< left hand side: = rightView for density, =derivative for current
    std::shared_ptr<Index> iStor;
    Coefficients *plotStore;  ///< modulus squared and summed over sum-coordinates

    unsigned int plotDepth;   ///< levels of plotStore from this onward will to go into single column

    double riemannSumWeight; //HACK to acount for summation over equidistant grids

    /// put into storage, sum over non-printing levels (returns sum of all squares)
    double sumSquares(Coefficients *LVec, Coefficients *RVec, Coefficients & Store) const;
    void generateAxes(const Index* Idx,  std::vector<std::vector<double>  > &Cols, unsigned int IAx=0) const;
    void intoColumns(const Coefficients &Store,  std::vector<std::vector<double>  > &Cols) const;

    /// OBSOLETE (keep until new well tested)
    void constructOld(std::vector<std::string> Axis,
                      std::vector<std::string> Use,
                      std::vector<unsigned int> GridPoints,
                      std::vector<std::vector<double> > GridBounds
                      );
    void construct(std::vector<std::string> Axis, std::vector<std::string> Use,
                   std::vector<std::vector<double>> Grid, std::vector<std::vector<double>> Weig);
    bool plotable(const Index* Idx,std::vector<std::string> Axes, std::vector<unsigned int> Points);
public:
    virtual ~Plot();
    Plot();

    Plot(const Discretization *Disc, ReadInput & inp, bool WarnIfEmpty=false):
        Plot(Disc->idx(),inp,WarnIfEmpty){} //!< OBSOLESCENT: use Plot(const Index ...) instead

    Plot(const Index *Idx, ReadInput & inp, bool WarnIfEmpty=false); //!< get plot definition from input

    Plot(const Index *Idx              /**< Index to plot */,
         std::vector<std::string> Axis /**< Axis to plot (all other will be summed over) */,
         std::vector<std::string> Use  /**< how to use axis "g"...grid,"p"...separate plots, "s"...sum*/ ,
         std::vector<unsigned int> GridPoints={} /**< number of points in equidistant grid (default: 0 = do not transform)*/,
         std::vector<std::vector<double> > GridBounds={} /**< lower [0] and upper [1] grid boundaries (default=all)*/
            );

    Plot(const Index *Idx, const PlotKind *Kind /** plot definition, contains Axis, Use, GridPoints, GridBounds, see description of main constructor */);

    void plot(const Coefficients & C, const std::string & File,
              const std::vector<std::string> & Head=std::vector<std::string>(0),
              const std::string Tag="",
              bool OverWrite=false) const; //!< transform to plot and write to file

    const Plot & generate(const Coefficients & C, const OperatorAbstract *Op=0) const;

    /// convert to plot, scale and add to columns (create new, if no columns exist)
    std::vector<std::vector<double> > sum(const Coefficients & C, std::string &File, double Scale=1., const std::string &Tag="") const;
    /// write current columns to file
    void write(const std::string & File,
               const std::vector<std::string> &Head={},
               bool OverWrite=true) const;
    void clear(){_cols.clear();} ///< clear any previous plot
    double integral() const; /// integrate density

    std::string briefName() const {return "wf";}

    void addOperator(const OperatorAbstract * Op); ///< plot density conj(Psi(coor)) [Op Psi](coor)
    static void plotJAD(const Coefficients * C, double E1, double E2, std::string fileName, unsigned int thetaNum=100);
    unsigned int dimension() const {return axCols.size();} //!< number of plot axes
    void digits(unsigned int NDigits){nDigits=NDigits;} //!< number of digits for ASCII output
    std::string str() const; ///< plot info
    void print() const; ///< neatly print plot definition
    bool isEmpty() const {return plotStore==0;}

    const Coefficients & plot() const {return *plotStore;}
    void append(bool Append);///< {_append=Append;} ///< tagged data will be written into 2d plot
    bool isAppend() const {return _append;} ///< true if plot in append-mode
};

/** @} */
#endif // PLOT_H
