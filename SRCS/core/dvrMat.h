// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef DVRMAT_H
#define DVRMAT_H

#include "basisAbstract.h"
#include <map>

class BasisSet;
class DvrBasis: public BasisAbstract
{
    // a lot of time goes into re-evaluation - keep table instead (until DvrBasis is properly incorporated)
    static std::map<int,const DvrBasis*> _tab;

    const BasisSet * orig;
    std::vector<double> _points;
    std::vector<double> _weights;
    std::vector<std::complex<double> > _pointsComplex;

    std::vector<std::complex<double> > _vals;
    std::vector<std::vector<std::complex<double> > > _ders;

public:
    static const DvrBasis* get(const BasisSet* Bas);
    DvrBasis(const BasisSet & Bas);

    const std::vector<double> & dvrPoints() const {return _points;}
    const std::vector<double> & dvrWeights() const {return _weights;}
    const std::vector<std::complex<double> > & dvrPointsComplex() const {return _pointsComplex;}

    const std::complex<double> & val(int K) const {return _vals[K];} ///<returns DVR value at K'th point
    const std::vector<std::complex<double> > & ders(int K) const {return _ders[K];} ///<returns all DVR derivatives at K'th point
    unsigned int size() const{return _ders[0].size();}
    std::complex<double> eta() const;
};

class DvrMat
{
    int _rows,_cols;
    std::vector<std::complex<double> > _data;
public:
    DvrMat(std::string Def, const DvrBasis & IBas, const DvrBasis & JBas);
    void useMatrix(UseMatrix &Mat) const; /// return in suitable UseMatrix format
};

#endif // DVRMAT_H
