// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef __SOD__
#define __SOD__

#include "singleparticleorbitals.h"
#include <vector>
#include <complex>
#include "ostream"

///
/// \brief The sod class: single determinant class.
/// Integrals done using Slater-Condon rules
///

class sod{
  // contains list of spatial(spac) part and spin of spin orbitals
 public:
    int nElectrons() const {return spin.size();} ///< number of electrons
  std::vector<int > spac;
  std::vector<int > spin;            //1=up, 0=down
  singleparticleorbitals *Orb;
  int sign;                     // change of sign after sorting
  int spinup;                   // assigned in sort function, stores number of spin ups- useful for matrix element evaluation

  sod(std::vector<int > spac, singleparticleorbitals& Orb);
  void sort();
  void diff_det(const sod& dj, int ndiff, int& count, std::vector<int >& diff_ind, int& sign);
  std::complex<double > sod_matel(std::string op, sod& dj);
  void sod_dyson(sod& dj, bool& res, int& pos, int& sign);

  void sod_dens1(sod& dj, bool& res, std::vector<int > &pos1, std::vector<int > &pos2, int &sign);
  void sod_dens2(sod& dj, bool& res, std::vector<Eigen::Vector4i> &pos, std::vector<int> &sign);
  void sod_dens3(sod& dj, bool& res, std::vector<Eigen::Matrix<int,6,1> > &pos, std::vector<int> &sign);
  void sod_dens_n(int n, sod& dj, bool& res, std::vector<Eigen::VectorXi> &pos, std::vector<int> &sign);

  void sod_matel_vector(Eigen::VectorXcd& res, std::string op,sod& dj);
  void sod_dipoles_vector(Eigen::VectorXcd& resx,Eigen::VectorXcd& resy,Eigen::VectorXcd& resz,sod& dj);
  std::complex<double > sod_hamiltonian(sod& dj);
  void sod_extend(int spin);

  friend std::ostream& operator<<(std::ostream& os, const sod& s);

  void convert_to_combined_format(vector<int > spc, vector<int > spn, vector<int >& res);
  void convert_to_seperate_format(vector<int > com, vector<int >& spc, vector<int >& spn);
  void sod_spindens_upto_n(int n, sod &dj, vector<bool> &res1, vector<vector<Eigen::VectorXi> > &pos1, vector<vector<int> > &sign_final);
};


#endif
