// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
/** @defgroup DiscretizationClasses Discretization
 * \brief Kinds, coordinates, basis sets, indexing, coefficients
 */


/** @defgroup Discretizations Discretizaion kinds
 *  \ingroup DiscretizationClasses
 *  \brief Product of coordinate axes, spectral discretizations, grids etc.
 *  @{
*/



#ifndef __DISCRETIZATION__
#define __DISCRETIZATION__
#include "axis.h"
#include <memory>

// forward class delarations

class Wavefunction;
class Coefficients;
class Index;
class IndexFloor;
class BasisSet;
class IndexConstraint;
#include "axisTree.h"


/// \brief base class for all discretizations
class Discretization {
    Index* _idx;
public:
    friend class Coefficients;
    friend class OperatorData;
    friend class InFlux;
    friend class DiscretizationDerived;
    friend class DiscretizationConstrained;

    static bool indexSelect(const Index* I);

    Discretization():parent(0),_idx(0),constraint(0){}
    static Discretization * factory(ReadInput & Inp);

    /// Derived class destructor must delete Idx.
    virtual ~Discretization();
    
    /// return \a mats with operator matrices from \p IFloor to \p JFloor
   virtual void operatorData(std::string def,///< string defining operator
                              const Index * const IFloor, ///< left hand block index
                              const Index * const JFloor, ///< right hand block index
                              std::vector<UseMatrix> & mats ///< one or more (in case of tensor product form) matrices
                              ) const;

    // ======================================================================================
    // virtual functions

    /// return vector of pointers to basis functions for block
    virtual void getBasis(const std::vector<const Index*> &Path,      ///< index path to present block
                          std::vector<const BasisSet *> &Basis ///< pointers to BasisSet's
                          ) const;

    /// number of functions that that are non-zero at element boundary
    virtual int marginSize(const std::vector<const BasisSet *> &Bas, const int ContIdx, const int LowUp) const;


    /// index of margin coefficient, i.e. the actual coefficients that need to be made continuous.
    virtual int marginIndex(const std::vector<const BasisSet*>& Bas, /**< determines the position in the index hierarchy */
                            const int ContIdx, /**< determines the continuous level for which the margin index is asked. This is relevant only when multiple continuous levels are present.*/
                            const int LowUp,   /**< = 0/1 for lower/upper upper margin index*/
                            const int M         /**< REDUNDAND?? relevant only when multiple continuous levels are present. Determines the position of the other continuous level*/
                            ) const;

    /// alternative inverse overlap function for Coefficients
    virtual Coefficients& inverseOverlap(Coefficients& coeffs, Index* index, bool& CoeffsPerformInvOv);

//    /// \p true if level is "continous", i.e. finite-element axis
//    virtual bool isContinuous(int level) const;

//    ///  Setup the local inverse overlap matrix
//    virtual void setupInverse();

    ///////////////////////////////////////////////////////////////
    // data
    std::string name;                   ///< name string (defaults to axis names if axes are given)
    std::vector<std::string> hierarchy; ///< strings naming the hierarchy (to be replaced by more structured info)
    Index*& idx(){return _idx;}              ///< the index system
    const Index * idx() const {return _idx;} ///< the index system
    std::vector<int> continuityLevel;   ///< (OBSOLESCENT - replace using Index::continuity(N)) hierarchy levels where continuity is imposed
    const Discretization * parent;      ///< derived discretization has a parent, top parent==0
    const IndexConstraint* constraint;  ///< Constraints imposed on the discretization
    ///////////////////////////////////////////////////////////////


    void show() const;                       ///< overview of the discretization
    std::string str(unsigned int Brief=0) const;                       ///< overview of the discretization
    void print(std::string File="", std::string Title="") const;   ///< print to file (default to cout)
    const std::vector<Axis> & getAxis() const { return axis;} ///< return the Discretization's axis
    const AxisTree* axisTree() const { return _axisTree.get(); } ///< return the Discretization's axis


    virtual std::string coordinates() const;

protected:
    std::vector<Axis> axis;
    std::shared_ptr<AxisTree> _axisTree;
    void setAxis(ReadInput &In,std::string Subset=""); //!< completely defines a discretization
    void construct();            //!< construct once axes are set up

    const BasisSet* blockBasis(unsigned int LAxis, unsigned int N, const std::vector<const Index *> &Path) const; //!< return updated basis definition for current block

};
/** @}/ */

#endif
