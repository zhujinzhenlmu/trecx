// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORZD_H
#define OPERATORZD_H


#include <string>
#include <vector>
#include <complex>

class UseMatrix;
class Index;

#include "operatorFloor.h"

/** \ingroup OperatorFloors */
/// complex diagonal matrix
class OperatorZD: public OperatorFloor
{
protected:
    void axpy(const std::complex<double> & Alfa, const std::complex<double>*X, unsigned int SizX,
              const std::complex<double> & Beta,       std::complex<double>*Y, unsigned int SizY) const;

    void axpyTranspose(std::complex<double> Alfa, const std::vector<std::complex<double> > & X,
                       std::complex<double> Beta, std::vector<std::complex<double> > & Y) const
    {axpy(Alfa,X.data(),X.size(),Beta,Y.data(),Y.size());}
    void construct(const UseMatrix *Mat, std::string Kind);
    void construct(const Eigen::VectorXcd & Mat, std::string Kind);
public:
    OperatorZD():OperatorFloor(0,0,"ZD"){}
    OperatorZD(const UseMatrix *Mat, std::string Kind);
    OperatorZD(const Eigen::MatrixXcd & Mat, std::string Kind);

    void pack(std::vector<int> &Info, std::vector<std::complex<double> >&Buf) const;

    OperatorZD(const std::vector<int> &Info, const std::vector<std::complex<double> > &Buf):OperatorFloor("ZD"){
        unpackBasic(Info,Buf);
        dat=addComplex(hashString(_rows,_cols),std::vector<std::complex<double> >(Buf.begin(),Buf.begin()+Info[3]));
    }
    
    virtual long applyCount() const;
};


#endif // OPERATORZD_H
