// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASISMAT1D_H
#define BASISMAT1D_H

#include <string>
#define EIGEN_MATRIX_PLUGIN "EigenAddonMatrix.h"
#include "index.h"
#include "qtEigenDense.h"

#include "basisMatMatrix.h"

class BasisAbstract;
class UseMatrix;
/// matrix for 1d BasisIntegrable
class BasisMat1D : public BasisMatMatrix
{
    Eigen::MatrixXcd valDer(const UseMatrix & X, const BasisIntegrable* Bas, int Derivative) const;
public:
    BasisMat1D(){}
    BasisMat1D(std::string Op, int Idim, int Jdim);
    BasisMat1D(std::string Op, const Index * IIndex, const Index* JIndex);
    BasisMat1D(std::string Op, const BasisAbstract* IBas, const BasisAbstract * JBas);
};
#endif // BASISMAT1D_H
