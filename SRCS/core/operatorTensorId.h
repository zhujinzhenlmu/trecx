// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORTENSORID_H
#define OPERATORTENSORID_H

#include "operatorAbstract.h"
#include <vector>

class IndexProd;
class Coefficients;
class CoefficientsPermute;


/// Op = Id (x) OpFactor, permuted to match rhs index JFull
class OperatorTensorId : public OperatorAbstract
{
    const OperatorAbstract* factor;
    std::vector<Coefficients*> iVfac,jVfac; // points to factor level
    CoefficientsPermute *iLoc,*jLoc; // local storage with product structure; provides permuted view matching original structure
    IndexProd *iProd,*jProd; // product indices
    void indexReplace(Coefficients* C, const Index *I);
public:
    OperatorTensorId(const OperatorAbstract * Factor, const Index *JFull, const Index *IFull=0);
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
};

#endif // OPERATORTENSORID_H
