// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef MOLECULARORBITAL_H
#define MOLECULARORBITAL_H
#include <string>
#include <vector>

#include "symmAdapOrb.h"

class UseMatrix;

/// set of molecular orbitals
class MolecularOrbital: public SingPartOrb
{
public:
    MolecularOrbital();
    void val(std::vector< std::vector<double> > XYZ, UseMatrix &Val);
private:
    SymmAdapOrb sao;
    UseMatrix trans;
};

#endif // MOLECULARORBITAL_H
