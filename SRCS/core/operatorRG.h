// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORRG_H
#define OPERATORRG_H

#include <string>
#include <vector>
#include <complex>

#include "operatorFloor.h"

class UseMatrix;
class Operator;

/** \ingroup OperatorFloors */
/// real general matrix with phase
class OperatorRG: public OperatorFloor
{
    std::vector<double> *dat; ///< pointer to the matrix data
    std::vector<double> *transDat; ///< pointer to transposed storage of the matrix data
    std::vector<double> x,y; ///< temporary reals
    std::vector<std::complex<double> >*phas; ///< pointer to column-wise phases
    std::vector<unsigned int> idx;

protected:
    void axpy(const std::complex<double> &Alfa, const std::complex<double>*X,unsigned int SizX, const std::complex<double>&Beta, std::complex<double> *Y,unsigned int SizY) const;
    void axpyTranspose(const std::complex<double> &Alfa, const std::complex<double> *X, unsigned int SizX,const std::complex<double> &Beta, std::complex<double>*Y, unsigned int SizY) const;
public:
    OperatorRG(const std::vector<std::complex<double> > &Phas,const std::vector<double> &Dat,unsigned int Rows, unsigned int Cols, std::string Kind);
    void pack(std::vector<int> & Info, std::vector<std::complex<double> >&Buf) const;

    OperatorRG(const std::vector<int> &Info, const std::vector<std::complex<double> > &Buf):OperatorFloor("RG"){
        unpackBasic(Info,Buf);
        std::vector<double> rBuf;
        for(unsigned int k=0;k<Info[3];k++){rBuf.push_back(Buf[k].real());rBuf.push_back(Buf[k].imag());}
        dat=addReal(hashString(_rows,_cols),rBuf);
    }
};

#endif // OPERATORRG_H
