#ifndef QUADRATURERULE_H
#define QUADRATURERULE_H

#include <vector>
#include <complex>

class Algebra;
class BasisIntegrable;
///@brief quadratur rule for BasisIntegrable
namespace QuadratureRule
{

void pointsAndWeights(const BasisIntegrable * Bas, std::vector<double> & Points, std::vector<double> & Weights);

std::vector<std::complex<double> > integralsBasisAlgebra(const BasisIntegrable * Bas, const Algebra* Alg);
std::vector<std::complex<double> > integralsBasisBasis(const BasisIntegrable * Bas);

void test();
}

#endif // QUADRATURERULE_H
