#include "eigenSolverAbstract.h"
#include "eigenSolver.h"
#include "operatorNonLin.h"
#include "operatorFloorGP.h"
#include "operatorMeanEE.h"
#include "operatorDefinition.h"

#include "operator.h"
#include "basisSet.h"
#include "gaunt.h"
#include "coefficientsFloor.h"
#include "index.h"
#include "qtAlglib.h"
#include "radialmultipole.h"
#include "basisMat.h"
#include "inverseDvr.h"
#include "basisMat1D.h"
#include "eigenNames.h"
#include "indexNew.h"
#include "basisDvr.h"

class EigenSolverNonLin : public EigenSolverAbstract
{
    void _compute();
    bool *_store;
    bool *_iterations;
    bool *_noInteraction;
    EigenSolver _slv;
    std::string _method;
public:
    EigenSolverNonLin(double Emin, double Emax, int Nmax, bool RightVectors, bool DualVectors, bool ExcludeRange, std::string Method);
    std::vector<std::complex<double> > eigenvalues() const {return _slv.eigenvalues();}
    std::vector<Coefficients *> rightVectors() const {return _slv.rightVectors();}
    std::vector<Coefficients *> dualVectors() const {return _slv.dualVectors();}
};
