#ifndef TOOLSPRINT_H
#define TOOLSPRINT_H

#include <string>
// collection of tools for print analysis

class Coefficients;

namespace ToolsPrint
{

   void CoefByFloors(std::string FileName, const Coefficients* C);
}

#endif // TOOLSPRINT_H
