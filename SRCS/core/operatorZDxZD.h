// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORZDXZD_H
#define OPERATORZDXZD_H


#include <string>
#include <vector>
#include <complex>

class UseMatrix;
class Index;
class OpeatorZD;

#include "qtEigenDense.h"
#include "operatorTensorProduct.h"
#include "operatorZD.h"

/** \ingroup OperatorFloors */
/// tensor product of diagonal matrices (returns factors separately)
class OperatorZDxZD: public OperatorZD
{
    std::vector<OperatorZD> facs;
public:
    OperatorZDxZD(std::vector<const UseMatrix *> Mat, std::string Kind);
    Eigen::MatrixXcd matrixFactor(int D) const;
};


#endif // OPERATORZDXZD_H
