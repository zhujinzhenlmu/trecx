// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASISINTEGRABLE_H
#define BASISINTEGRABLE_H

#include <memory>
#include "basisAbstract.h"
#include "jacobian.h"
#include "complexScaling.h"

class UseMatrix;

/// an integrable basis must have an arbitrary point quadrature
class BasisIntegrable: public BasisAbstract
{
protected:
    double _lowBound,_upBound; ///< integration range
    std::shared_ptr<const Jacobian> _jac;
    std::shared_ptr<const ComplexScaling> _comSca;

public:
    static double infty;
    virtual ~BasisIntegrable(){}
    BasisIntegrable(double LowBound, double UpBound,std::string Jacob="1", ComplexScaling ComSca=ComplexScaling())
        :_lowBound(LowBound),_upBound(UpBound),
          _comSca(std::shared_ptr<const ComplexScaling>(new ComplexScaling(ComSca)))
    {
        if(Jacob=="1")_jac=std::shared_ptr<Jacobian1>(new Jacobian1(0.));
        else DEVABORT("awaits implementation");
    }

    virtual unsigned int order() const=0; ///< quadrature order for exact evaluation of overlap
    virtual unsigned int quadSizeSafe() const;//{return order()+2;} ///< number of quadrature points so that the integration is precise enough
    virtual void quadRule(int N, UseMatrix & QuadX, UseMatrix & QuadW) const;

    virtual void quadRule(int N, std::vector<double> &QuadX, std::vector<double> &QuadW) const=0;
    std::vector<std::complex<double> > val(double X) const;

    /// values and derivatives at points X, ZeroOutside: set = 0, where X outside range of basis function
    virtual void valDer(const std::vector<std::complex<double> > & X,
                std::vector<std::complex<double> > & Val,
                std::vector<std::complex<double> > & Der, bool ZeroOutside=false) const=0;
    /// alternate call with strictly real coordinate poitns
    void valDerD(const std::vector<double > & X,
                std::vector<std::complex<double> > & Val,
                std::vector<std::complex<double> > & Der, bool ZeroOutside=false) const;


    UseMatrix val(const UseMatrix & Coordinates, bool ZeroOutside=false) const; ///< val(i,k): value f_k(coordinate[i])
    UseMatrix der(const UseMatrix & Coordinates, bool ZeroOutside=false) const; ///< derivatives at a coordinate grid

    /// old style version of valDer
    virtual void valDer(const UseMatrix & X,UseMatrix & Val, UseMatrix & Der, bool ZeroOutside=false) const;

    virtual double upBound() const {return _upBound;}       ///< return upper boundary of interval
    virtual double lowBound() const{return _lowBound;}      ///< return lower boundary of interval
    virtual std::vector<double> intervals() const {return {_lowBound,_upBound};} ///< points where basis is not analytic
    const std::shared_ptr<const Jacobian> jacobian() const {return _jac;}
    const std::shared_ptr<const ComplexScaling> complexScaling() const {return _comSca;}

    std::complex<double> eta() const;// {return _comSca->etaX(0.5*(upBound()+lowBound()));}
    bool isAbsorptive() const {return eta().imag()!=0.;}

    std::vector<std::vector<std::complex<double> > > transZeroValDer(double Q) const; ///< unitary transformation to valDer(Q)=0
};

#endif // BASISINTEGRABLE_H
