#ifndef SPECTRUMLINCOM_H
#define SPECTRUMLINCOM_H

#include <string>
#include <vector>
#include <memory>
#include "readInputList.h"

#include "coefficients.h"

class SpectrumPlot;

/**
 * \ingroup tSurff
 * \brief Read several amplitude files and linearly combine (see tsurff.pdf on RABITT)
 */
class SpectrumLinCom
{
    std::vector<std::string> amplFiles;
    std::vector<Coefficients> _ampl;
    std::vector<double> _phiCEO;
    std::vector<double> _theAlign;
    std::string channel;

    std::vector<double> gridPhiCEO;
    std::vector<double> gridAlign;

    /// value found in Values at position of Which in Names, supply/crosscheck if linp-file exists
    double getValue(std::string Which,std::vector<std::string> Names,std::vector<std::string> Values);

    std::vector<double> transPhi(double Alfa, double Beta, double Gamma) const; ///< for data given at phiCEO=Alfa,Beta, get linear combination for Gamma
    std::vector<double> transPolar(std::vector<double> PolarAngle, double Gamma) const; ///< for data given at polarAngle's get linear combination for Gamma
    static void read(std::string & RangePhiCEO, std::string & RangeAlign, std::string & RangeAver);

    /// test the transformation by inverting it
    void test(const std::vector<Coefficients> & Ampl,const std::vector<double> PhiCeo, const std::vector<double> TheAlign) const;
    /// at location of maximum of first, scale to all phases = 1
    void phaseLockAtMax(std::vector<Coefficients>& Ampl) const;
public:
    static std::string strFractionOfPi(double AngleRadians);
    static bool found();
    /// set up for interpolation from several input amplitudes (PDF docu on "RABITT")
    SpectrumLinCom(std::vector<std::string> Dirs);

    Coefficients amplPhiCEO(const std::vector<Coefficients> &_ampl, const std::vector<double> &_phiCEO, const std::vector<double> &_theAlign, double Phi, double Polar) const; ///< get a single interpolated amplitude

    void plotAverage(SpectrumPlot & Plt); ///< compute linear combinations  for given parameter grid, generat plot files

    std::vector<std::string> info() const; ///< files and parameters
};

#endif // SPECTRUMLINCOM_H
