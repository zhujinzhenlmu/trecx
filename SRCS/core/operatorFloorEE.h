// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORFLOOREE_H
#define OPERATORFLOOREE_H

#include "operatorFloor.h"
#include "basicDisc.h"
#include "readInput.h"
#include "useMatrix.h"

using namespace std;

/** \ingroup OperatorFloors */
/// \brief two-particle Coulomb repulsion, using multipole expansion
class OperatorFloorEE : public OperatorFloor
{
    std::vector<int > Idx,Jdx;
    void construct_index(const Index* I, std::vector<int> &Idx); // order n1,n2,m1,m2,l1,l2
     std::vector<std::complex<double> > qWeights;

    // Helpers //////////////
    class BasicDiscOnlyRadial: public BasicDisc{
    public:
        BasicDiscOnlyRadial(ReadInput &In);
        int lmax;
    };

    class Helpers_e_e{
        static void lobatto_quadrature(BasisSet* B, Eigen::VectorXd & quadraturePoints, Eigen::VectorXd & iWeights);
        static void analyseDVRInternallyUsed(BasisSet* B, UseMatrix& pts, UseMatrix& wgs, std::vector<bool>& wgs_inc);
        static void lobatto_quadratureNew(const BasisAbstract*, Eigen::VectorXd&, Eigen::VectorXd&);
        static void analyseDVR(const BasisAbstract* B, UseMatrix& pts, UseMatrix& wgs, vector<bool>& wgs_inc);
    public:
        static std::vector<std::vector<std::vector<UseMatrix > > > TwoElecOverlap;// TwoElecOverlap[n1][n2][lambda]
        static std::vector<std::vector<std::vector<double> > > TwoElecOverlapMaxValue;
        static void initialize(const Index *Root, int& lambda_upper_limit);
        static void absorbInverse(const Index *Root);
    
    };
    // /////////////////////
    static double epsGaunt;
    static double epsTwoElec;

    static int lambda_upper_limit;
protected:
    void axpy(const std::complex<double> & Alfa, const std::complex<double>* X, unsigned int SizX,
              const std::complex<double> & Beta, std::complex<double>* Y, unsigned int SizY) const;

    /// OBSOLESCENT
    void axpy(std::complex<double> Alfa, const std::vector<std::complex<double> > & X,
                      std::complex<double> Beta, std::vector<std::complex<double> > & Y) const;
public:
    static void read(ReadInput& Inp);
    static void constrain(UseMatrix& Mult, const Index* IIndex, const Index* JIndex);

    OperatorFloorEE(const std::string Name, const std::string Def, const Index *IIndex, const Index *JIndex);
    OperatorFloorEE(const std::vector<int> &Info, const std::vector<std::complex<double> > &Buf);
    ~OperatorFloorEE();

    virtual void apply(std::vector<std::complex<double> > &InOut) const;
    virtual void inverse();
    bool isZero(double Eps=0.) const;

    void pack(std::vector<int> & Info,std::vector<std::complex<double> > &Buf) const;
};

#endif // OPERATORFLOOREE_H
