// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef PARALLELCROSS_H
#define PARALLELCROSS_H

#include <vector>
#include "derivativeBlock.h"
#include <iostream>
#include <string>

class Index;
class ParallelProcess;
/** \ingroup Parallelization */
/// rows and columns to be owned by one process
class ParallelCross
{
    friend class Parallel;
    friend class ParallelProcess;
    friend class DerivativeFlat;
    std::vector<DerivativeBlock*> colBlock,rowBlock;
public:
    ParallelCross(){}

    std::string str() const;
    const Index* index() const;
    double load() const {
        double l=0.;
        for(std::vector<DerivativeBlock*>::const_iterator b=rowBlock.begin();b!=rowBlock.end();b++)l+=(*b)->load();
        for(std::vector<DerivativeBlock*>::const_iterator b=colBlock.begin();b!=colBlock.end();b++)l+=(*b)->load();
        return l;
    }
};

#endif // PARALLELSTRIPE_H
