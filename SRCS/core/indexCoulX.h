#ifndef INDEXCOULX_H
#define INDEXCOULX_H

#include "indexDerived.h"

class IndexCoulX:public IndexDerived
{
public:
    IndexCoulX(const Index* Idx, double RC, double RMax, const std::vector<double> KGrid, bool BandOvr, bool PureBessel=false);
};

#endif // INDEXCOULX_H
