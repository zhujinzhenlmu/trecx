// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASISMAT2D_H
#define BASISMAT2D_H

#include <string>
#include "qtEigenDense.h"
#include "index.h"

class BasisAbstract;
class UseMatrix;
class BasisMat2D
{
    Eigen::MatrixXcd _mat,_mat0,_mat1;
public:
    BasisMat2D(){}
    BasisMat2D(std::string Op, const Index * IIndex, const Index* JIndex);
    bool isEmpty() const {return _mat.size()==0 and (_mat0.size()==0 or _mat1.size()==0);}
//    const UseMatrix useMat() const;
    const std::vector<const Eigen::MatrixXcd*> mats() const;
};
#endif // BASISMAT1D_H
