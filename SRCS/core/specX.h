#ifndef SPECX_H
#define SPECX_H

#include <string>

class DiscretizationGrid;
class Coefficients;

/// corrections to the scattering amplitude
class SpecX
{
public:
    SpecX(std::string Basis, std::string CorrOper, double Start, double TimeStep, const DiscretizationGrid * Disc);
    void step(Coefficients & C);
};

#endif // SPECX_H
