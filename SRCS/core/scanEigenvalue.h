// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef SCANEIGENVALUE_H
#define SCANEIGENVALUE_H

#include "parameterScan.h"
#include "eigenSubspace.h"

class DerivativeFlat;
class Operator;
class Coefficients;
class ScanEigenvalue : public ParameterScan
{
    static EigenSubspace * eigSub;
    static std::vector<std::complex<double> > resolv;     // resolvent values
    static std::vector<std::vector<std::complex<double> > > eval;   // eigenvalues for each resolvent value
    static std::vector<std::vector<Coefficients*> > evec;
public:
    ~ScanEigenvalue();
    static void eigenvaluesAtPar(const  std::vector<std::string>& ParName, const std::vector<double> & ParVal, std::vector<double> & Result);
    ScanEigenvalue(ReadInput & Inp);

    void print() const;
    void setEigenSub(const Discretization * D,const Operator* H,const Operator* H0){eigSub->setup(D,H,H0);}
};

#endif // SCANEIGENVALUE_H
