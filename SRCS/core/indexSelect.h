// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef INDEXSELECT_H
#define INDEXSELECT_H

#include <vector>
#include <string>

class Index;
class Axis;
class ReadInput;

/// \ingroup Index
/// \brief for imposing constraints on Index's
class IndexSelect
{
    static const IndexSelect* cur;
public:
    /// true if Index I is in current selection
    static bool current(const Index * I){if(cur==0)return true; return cur->select(I);}
    static void read(ReadInput & Inp, const Index *Root);
    static void read(ReadInput & Inp, const std::vector<Axis> & Ax);
    static void output();
    /// remove nodes from Index according to current selection
    static void remove(Index * I);

    virtual bool select(const Index * I) const=0;
    virtual std::string str() const=0;
};


class IndexSelectLminusM: public IndexSelect{
    unsigned int diffLM;
    unsigned int Lmin;
    std::vector<unsigned int> lLevel,mLevel;
public:
    IndexSelectLminusM(ReadInput & Inp, const Index *Root);
    IndexSelectLminusM(ReadInput & Inp, const std::vector<Axis> & Ax);
    bool select(const Index * I) const;
    std::string str() const;
};

#endif // INDEXSELECT_H
