#ifndef COULXVOLKOV_H
#define COULXVOLKOV_H

#include <memory>
#include "operatorTree.h"

#include "discretization.h"

class Index;
class VolkovPhase;

/// apply the Volkov phase for the CoulX
/// the hierarchy must be Phi1.Eta1.kRn1.Phi2.Eta2.Rn2
class CoulXVolkov: public OperatorTree
{
    std::shared_ptr<VolkovPhase> _volk;

    // temporary dummy class for using VolkovPhase constructor...
    class _disc:public Discretization{
    public:
       _disc(const Index *_Idx){idx()=const_cast<Index*>(_Idx);}
    };

public:
    CoulXVolkov(const Index *Idx);
    void update(double Time, const Coefficients* CurrentVec=0);
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
};

#endif // COULXVOLKOV_H
