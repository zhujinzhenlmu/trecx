// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef MOL_SAE_SHELF_H
#define MOL_SAE_SHELF_H

#include "qtEigenDense.h"
#include <vector>
//#include <boost/multi_array.hpp>
#include "My4dArray.h"
#include "my4darray_shared.h"
#include "wavefunction.h"

///
/// \brief The mol_sae_shelf class: Left over shelf class, should be eliminated in the long term
///

class mol_sae_shelf
{

public:

  static mol_sae_shelf main;     ///< main static object  

  // Zero vectors to be projected out
  vector<Wavefunction* > zero_vec_proj,s0_zero_vec_proj;
  vector<Wavefunction* > hev_proj,sX_hev_proj;

  //helper to mo::coulomb_gauss_poly(Eigen::MatrixXcd &res, int n, int l, int m)
  vector<vector<vector<complex<double > > > > ylm_0;    // ylm_0[na][L][M]

  // exhange integrals
  vector<vector<Eigen::MatrixXcd* > > xch_data;

  Eigen::MatrixXcd sinv_help;

  void xch_data_clear();
  mol_sae_shelf();
  ~mol_sae_shelf();
  void clear_rho2();
};

template<class T>
class mo_store{

  struct mat{
    string op;
    T right_fn;
    Eigen::MatrixXcd m;
  };

  vector<mat > store;

public:
  Eigen::MatrixXcd* retrieve(string name, T rfn){
    for(unsigned int i=0;i<store.size();i++){
        if(store[i].op==name)
          if(store[i].right_fn==rfn)
            return &(store[i].m);
      }
    return NULL;
  }
  void add(string name, T rfn, Eigen::MatrixXcd a){
    mat temp;
    temp.m = a;
    temp.op = name;
    temp.right_fn = rfn;
    store.push_back(temp);
  }
  void clear(){
    store.clear();
  }
};

class mo_store_v1{
   vector<string > op;
   vector<vector<vector<vector<Eigen::MatrixXcd* > > > > s;

   int ne;
   int mmax;
   int lmax;

public:
   mo_store_v1();
   ~mo_store_v1();
   void add(string name, Eigen::Vector3i rfn, Eigen::MatrixXcd a);
   Eigen::MatrixXcd* retrieve(string name, Eigen::Vector3i rfn);
   void clear();

};

#endif // MOL_SAE_SHELF_H
