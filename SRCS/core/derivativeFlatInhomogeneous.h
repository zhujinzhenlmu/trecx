// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORINHOMOGENEOUSTSURFF_H
#define OPERATORINHOMOGENEOUSTSURFF_H

#include "tools.h"
#include "operator.h"
#include "derivativeFlat.h"
#include "projectSubspace.h"

class TsurffSource;

/** \ingroup Structures */
/// provide the tSurff source term for partially ionized subregion

class DerivativeFlatInhomogeneous: public DerivativeFlat{
  std::vector<TsurffSource* > tS;
  const OperatorTree* _op; // for debug
public:
  DerivativeFlatInhomogeneous(const OperatorTree* Op, double ApplyThreshold, const DiscretizationSpectral *ProjectionDisc=0, std::vector<TsurffSource* > Source=std::vector<TsurffSource* >(0));
  DerivativeFlatInhomogeneous(const OperatorTree* Op, double ApplyThreshold, const ProjectSubspace *Project=0, std::vector<TsurffSource* > Source=std::vector<TsurffSource* >(0));
  ~DerivativeFlatInhomogeneous();

  void update(double Time, const Coefficients* CurrentVec=0);
  void apply(std::complex<double> A, const Coefficients& X, std::complex<double> B, Coefficients &Y) const;
  void apply(std::complex<double> A, CoefficientsLocal *localX, std::complex<double> B, CoefficientsLocal &Y) const;
  const Index* idx() const;
};

#endif // OPERATORINHOMOGENEOUSTSURFF_H
