// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef DISCRETIZATIONGRID_H
#define DISCRETIZATIONGRID_H

#include "discretizationDerived.h"
#include "indexDerived.h"
#include "operatorAbstract.h"
#include "tree.h"
#include <memory>

class OperatorFloor;
class OperatorIdentity;

/** \ingroup Discretizations */
/// Discretization by converting function on selected axes to grids
class DiscretizationGrid:public DiscretizationDerived
{
    const Index* _parentIndex;
    mutable std::shared_ptr<const OperatorAbstract> _mapToDual; // mutable as it will be created upon first use
    void _construct(const Index *Parent, const std::vector<std::string> &Axes, std::vector<std::vector<double>> Grid, std::vector<std::vector<double>> Weig, bool Gradient);
public:
//    Operator* mapFrom;
//    DiscretizationGrid(const DiscretizationGrid & Other);
public:
    class IndexG: public IndexDerived{
        friend class DiscretizationGrid;
    public:
        IndexG(const Index *I, std::vector<unsigned int> Level, std::vector<unsigned int> Point, std::vector<double> Limit);
        IndexG(const Index *I, const std::vector<std::string> &Ax, std::vector<std::vector<double>> Grid, std::vector<std::vector<double>> Weig={});
    };

    ~DiscretizationGrid();

    /// standard construtor
    DiscretizationGrid(const Index *Parent, const std::vector<std::string> &Axes, std::vector<std::vector<double>> Grid, std::vector<std::vector<double>> Weig, bool Gradient);

    /// alternate constructor, use Index for parent (legacy)
    DiscretizationGrid(const Index *Parent /** original discretizaiton */,
                       std::vector<std::string> Axes /** names of axes to be transformed */,
                       std::vector<unsigned int> Point=std::vector<unsigned int>() /** number of points, minimal quadrature grid if not specified */,
                       std::vector<std::vector<double> > Limit=std::vector<std::vector<double> >() /** lower and upper grid boundaries */,
                       bool Gradient=false /** also compute transformations to gradients wrt all grid axes */);

    const OperatorAbstract * mapFromParent() const;
    const OperatorAbstract *mapToParent() const; ///< maps back to one of To = Parent, Dual, Mix

    std::vector<const OperatorAbstract*> mapDerivative;

    /// return Grid's and Weig's for a standard plot
    ///
    /// rules for each k of Axes:
    /// <br> equidistant Points[k] in [a,b]=[Bounds[k][0],Bounds[k][1]]
    /// <br> if a>b, empty Grid[k] and Points[k] zeros in Weig[k] (interpreted as Weig[k].size() quadrature grid in DiscretizationGrid)
    static void gridWeight(std::vector<std::vector<double>> &Grid,std::vector<std::vector<double>> &Weig,
                           std::vector<std::string> Axes,std::vector<unsigned int> Points={},std::vector<std::vector<double>> Bounds={});
};

#endif // DISCRETIZATIONGRID_H
