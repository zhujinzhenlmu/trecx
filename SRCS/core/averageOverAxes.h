#ifndef AVERAGE_OVER_AXES_H
#define AVERAGE_OVER_AXES_H

#include <vector>
#include <string>

#include "index.h"
#include "coefficients.h"

/**
 * Compute
 * \f[
 * \phi_{km} = \sum_{jl}\psi^*_{jklm}\psi_{jklm}
 * \f]
 * where the indices to be summed over are defined by _axNames.
 *
 * Warning! This class is not extensively tested. Worked for Helium 6D when averaging ober radial indices.
 *
 * This yields the diagonal of the reduced density matrix, so we should have
 *
 * \f[
 * \sum_{km}\phi_{km} = 1
 * \f]
 */
class AverageOverAxes{
    std::vector<bool> averageAxLevel;

    // Suppress warning coefficients construct during timeCritical
    Coefficients temp;

    void setupIndex(const Index* root, Index* parent);
    void averageRec(const Coefficients& src1, const Coefficients& src2, Coefficients& target);
public:
    AverageOverAxes(const Index* idx, std::vector<std::string> _axNames);

    const Index* Idx;
    void average(const Coefficients& src, Coefficients& target);
};


#endif
