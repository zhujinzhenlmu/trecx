// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef INFLUX_H
#define INFLUX_H


#include <complex>
#include "operatorAddvector.h"
#include "basisSet.h"
#include "qtAlglib.h"

class Discretization;
class Axis;
class ReadInput;

/// incoming flux
class InFlux
{
    double pWidth; ///< spectral width
    double rIn;    ///< source radius
    double time0;  ///< time for flat phase
    double pCentral; ///< central momentum
    int lPartial;  ///< partial wave
    int mPartial;  ///< partial wave
    std::string shape; ///< spatial shape of pulse at time0
    const Axis* axis;
    unsigned int kEntry; ///< entry element on axis
    const BasisSet * rightFlux; ///< basis for in-flux
    const Discretization* disc;

    std::vector<std::vector<std::complex<double> > > matel; ///< the 4 different matrix elements needed for tFlux
    void vals(const double Time,std::vector<std::complex<double> > & Vals),der(const double Time); ///< incoming values and derivatives at surface
    void add(Discretization * G,Coefficients * Vec); ///< add the inFlux contribution to a grid-wavefunction
    void setVals();

    alglib::spline1dinterpolant valTab,derTab,idotValTab,idotDerTab; ///< interpolation tables for surface flux

public:
    InFlux(){}
    InFlux(ReadInput &Inp, const Discretization* Disc, std::string HamDef);
    std::string inFlux() const; ///< ingoing flux operator string

    static void tFlux(double Time, std::vector<std::complex<double> > & C); ///< update the flux operator to current time
    static InFlux main; ///< make incoming flux globally available

    void printOut() const; ///< output the flux definition
};

#endif // INFLUX_H
