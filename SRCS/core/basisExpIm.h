#ifndef BASISEXPIM_H
#define BASISEXPIM_H
class BasisSub;

#include "basisIntegrable.h"

class BasisTrigon : public BasisIntegrable
{
protected:
    std::vector<int> _mValues;
    BasisTrigon(const BasisSetDef & Def);
    BasisTrigon(std::string StrDefinition);
public:
    /// exp(i m ...) or m>0 sin(m..), else cos(m...)
    int mValue(int N) const {return _mValues[N];}
    double physical(int N) const {return double(_mValues[N]);}

    unsigned int size() const {return _mValues.size();}
    unsigned int order() const;
    std::string str(int Level) const;
    std::string strDefinition() const;

    void valDer(const std::vector<std::complex<double> > & X,
                std::vector<std::complex<double> > & Val,
                std::vector<std::complex<double> > & Der, bool ZeroOutside=false) const=0;
    void quadRule(int N, std::vector<double> & QuadX, std::vector<double> & QuadW) const;
    bool operator==(const BasisAbstract& Other) const;
};

class BasisExpIm: public BasisTrigon {
public:
    BasisExpIm(const BasisSetDef & Def):BasisTrigon(Def){}
    BasisExpIm(std::string StrDefinition):BasisTrigon(StrDefinition){_name="expIm";}
    void valDer(const std::vector<std::complex<double> > & X,
                std::vector<std::complex<double> > & Val,
                std::vector<std::complex<double> > & Der, bool ZeroOutside=false) const;
};

class BasisCosSin: public BasisTrigon {
public:
    BasisCosSin(const BasisSetDef & Def):BasisTrigon(Def){}
    BasisCosSin(std::string StrDefinition):BasisTrigon(StrDefinition){_name="CosSin";}
    void valDer(const std::vector<std::complex<double> > & X,
                std::vector<std::complex<double> > & Val,
                std::vector<std::complex<double> > & Der, bool ZeroOutside=false) const;
};

#endif // BASISEXPIM_H
