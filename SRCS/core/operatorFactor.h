// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORFACTOR_H
#define OPERATORFACTOR_H

#include <map>
#include "useMatrix.h"

class ReadInput;

// special definitions for individual operator factors
class OperatorFactor
{
public:
    static std::map<std::string,UseMatrix> matrix;
    static void readMatrix(ReadInput & Inp);
};

#endif // OPERATORFACTOR_H
