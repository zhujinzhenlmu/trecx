// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASISFUNCTIONCINEUTRAL_H
#define BASISFUNCTIONCINEUTRAL_H

#include "basisFunction.h"
#include <boost/multi_array.hpp>
#include "mol_sae_shelf.h"

/// Forward declarations
class mo;
class ci;
class My4dArray_shared;
class BasisFunctionCIion;

/** \ingroup Basissets */
/// \brief CI neural functions used in haCC
class BasisFunctionCINeutral: public BasisFunction
{
  mo* orbitals;                          /// Molecular orbitals
  ci* CI;                                /// CI states
  mutable std::map<std::string,UseMatrix> onepMat;

public:

  friend class BasisFunctionCIion;

  BasisFunctionCINeutral(int order);
  ~BasisFunctionCINeutral();

  /// Public data members: reduced density matrices
  std::vector<std::vector<Eigen::MatrixXd > > neut_rho1,neut_rho3;
  std::vector<std::vector<My4dArray_shared* > > neut_rho2;
  Eigen::MatrixXd neut_2e;

  UseMatrix matrix_1e(std::string def) const;
};

#endif // BASISFUNCTIONCINEUTRAL_H
