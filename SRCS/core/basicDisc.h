// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef __BASICDISC__
#define __BASICDISC__

#include <vector>
#include "discretization.h"
// forward class declarations
class ReadInput;
class UseMatrix;
class Axis;
class IndexConstraint;

/// \ingroup Discretizations
/// \brief standard discretization (product of Axis's)
class BasicDisc: public Discretization {
public:
    BasicDisc(){}
    BasicDisc(std::vector<Axis> Ax, const IndexConstraint* Constraint=nullptr);
    BasicDisc(ReadInput &In, const std::string & Subset="", const IndexConstraint* Constraint=nullptr); ///< take several Axis from input and construct
    BasicDisc(std::string InputFile); ///< create ReadInput from file and construct

    /// create or append to exisint testX.inp and testXY.inp axis inputs for test purposes
    static std::string generateTestInputs(std::string InpFile="", int Dim=1);
    static std::string generateTestInputs(int argc=0, char *argv[]=nullptr, int Dim=1);

    static void Test(); ///< demonstrate general discretization
};

#endif
