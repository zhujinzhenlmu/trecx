// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASISABSTRACT_H
#define BASISABSTRACT_H

#include "basisFunction.h"
#include <deque>
#include <string>
#include "labelled.h"

//class basisMat;
class ComplexScaling;

class Index;
class Axis;
class UseMatrix;
class BasisNdim;
class BasisSetDef;
class Coordinate;
class BasisSub;

/** @defgroup Basissets Basis sets
 * \ingroup DiscretizationClasses
 * \brief One and multi-dimensional basis functions, grid; operations on basis
* @{
*/

/// @brief Abstract base for all basis sets and grids
///
/// a basis set is a set of functions (0,...,Order-1)
/// defined as a linear combination of shifted and scaled BasisFunction
class BasisAbstract:public Labelled<BasisAbstract>
{
protected:
    std::string _name;
public:
    virtual std::string name() const {return _name;}
    virtual unsigned int size() const=0;

//    virtual std::complex<double> jacobian(const std::complex<double> &X) const {ABORT("Jacobian not defined for basis: "+str());}
    virtual ~BasisAbstract(){}
    BasisAbstract(std::string Name="NONE"):_name(Name){}

    virtual bool operator==(const BasisAbstract &other) const;

    virtual UseMatrix val(const UseMatrix & coordinates, bool ZeroOutside=false) const{ABORT("not implemented");} ///< val(i,k): value f_k(coordinate[i])
    virtual UseMatrix der(const UseMatrix & coordinates, bool ZeroOutside=false) const{ABORT("not implemented");} ///< derivatives at a coordinate grid

    virtual void valDer(const UseMatrix & X,UseMatrix & Val, UseMatrix & Der, bool ZeroOutside=false) const{ABORT("not implemented");} ///< values and derivates on X grid

    /// returns quadrature weight * jacobian as a diagonal matrix
    UseMatrix matrixWeigJacobian(const UseMatrix & x, const UseMatrix & w, bool Eta=false) const{ABORT("not implemented");}

    virtual bool isPeriodic() const {return false;}
    virtual unsigned int lowerMargin() const {return 0;} ///< first function is on left boundary (should go to BasisIntegrable)
    virtual unsigned int upperMargin() const {return size()-1;}///< last function is on right boundary (should go to BasisIntegrable)

    virtual inline unsigned int order()const{ABORT("not implemented for"+name());} ///< order = number of fundamental functions
    virtual bool isIntegrable(const BasisAbstract& other) const{ABORT("not implemented");} ///< true if matrix can be computed
    virtual std::vector<double> parameters() const{ABORT("not implemented");} ///< return the function set parameter vector
    virtual double parameter(int K, std::string Kind) const{ABORT("not implemented");} ///< return K'th basis function parameter of Kind (Kind will we crosschecked)
    virtual bool isAbsorptive() const; ///< true if absorption acts on basis
    virtual std::string str(int Level=0) const; ///< print basis parameters
    virtual std::string strDefinition() const; ///< complete definition of basis - can be used in factory
    virtual std::string strFunc(const int I) const{ABORT("not implemented");} ///< string of parameters of I'th function
    virtual std::string parameterString(int K) const{ABORT("not implemented");} ///< parameter for K'th basis function (if any)
    virtual const BasisAbstract *remove(const std::vector<int> &RemoveK) const; ///< return BasisSub - basis set with subset of functions removed
//    virtual const BasisAbstract* remove(int RemoveK) const {return remove(std::vector<int>(1,RemoveK));} ///< new basis set with k'th basis function removed, add to table, return pointer

    virtual double upBound() const{ABORT("not implemented"+str());}        ///< return upper boundary of interval
    virtual double lowBound() const{ABORT("not implemented for "+str());}        ///< return lower boundary of interval
    virtual double param(int I) const{ABORT("not implemented"+str());}

    virtual UseMatrix Points() const{DEVABORT("trying to use Points in non-BasisSet(grid)");return UseMatrix();}
    virtual UseMatrix Weights() const{DEVABORT("trying to use Weights in non-BasisSet(grid)");return UseMatrix();}

    virtual bool isGrid() const{return false;}
    virtual bool isIndex() const{return false;}
    bool isCIstate() const{ABORT("not implemented");}
    /// true if domains of two basis sets overlap, ShareBoundary=true counts shard point as overlap
    virtual bool hasOverlap(const BasisAbstract & other) const{ABORT("not implemented");}
    virtual bool shareBoundary(const BasisAbstract & other) const{ABORT("not implemented");}
    virtual const BasisFunction* PointerToFunctions() const{ABORT("not implemented");}

    static const BasisAbstract* factory(const BasisSetDef & Def);
    static const BasisAbstract* factory(const std::string & Def);

    virtual std::complex<double> complexScaled(double X) const {ABORT("not implemented");}
    const BasisNdim* basisNdim() const;

    virtual bool isDVR() const {return false;}

    /// Return the physical quantity associated to the Index'th basis function
    /// This might be the magnetic/angular momentum, a grid point or sth similar.
    /// Meant to be used for plotting/visualization/purposes primarliy!
    virtual double physical(int Index) const{ return Index; }
};

/**
 *  @}
*/ // end Basissets

#endif // BASISABSTRACT_H
