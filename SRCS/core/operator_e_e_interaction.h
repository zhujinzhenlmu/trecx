// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATOR_E_E_INTERACTION_H
#define OPERATOR_E_E_INTERACTION_H

#include "operatorSingle.h"
#include "basicDisc.h"
#include "readInput.h"

class OperatorFloor;
/** \ingroup Discretizations */
/// for use in electron Coulumb repulsion
class Operator_e_e_interaction : public OperatorSingle
{
public:
    // Helpers //////////////
    class BasicDisc_remove1particle: public BasicDisc{
    public:
        BasicDisc_remove1particle(ReadInput &In, int remove=2);
        int lmax;
    };

    class Helpers_e_e{
        static void lobatto_quadrature(BasisSet* B, Eigen::VectorXd & quadraturePoints, Eigen::VectorXd & weights);
    public:
        static std::vector<std::vector<std::vector<Eigen::MatrixXcd > > > TwoElecOverlap;// TwoElecOverlap[n1][n2][lambda]
        static void initialize();
    };
    // /////////////////////

    Operator_e_e_interaction(const std::string Name, const std::string Def, const Index *IIndex, const Index *JIndex);
    ~Operator_e_e_interaction();

    virtual void axpy(std::complex<double> Alfa, CoefficientsFloor & X,std::complex<double> Beta, CoefficientsFloor & Y, bool transpose=false) const;
    virtual void apply(std::vector<std::complex<double> > &InOut) const;
    virtual bool isZero(double Eps=0.) const;
    virtual void inverse();

    OperatorFloor* oFloor;
private:
    void construct_index(const Index* I, std::vector<int> &Idx); // order n1,n2,m1,m2,l1,l2

    std::vector<int > Idx,Jdx;
    double epsGaunt;
    int lambda_upper_limit;
};

#endif // OPERATOR_E_E_INTERACTION_H
