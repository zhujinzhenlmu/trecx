#ifndef BASISORBITAL_H
#define BASISORBITAL_H

#include "basisAbstract.h"
#include "coefficients.h"

class OperatorTree;
class OperatorDefinition;
class ReadInput;

/** \ingroup Basissets */

///@brief Several orbitals w.r.t. a reference Index
class BasisOrbital: public BasisAbstract
{
    std::vector<std::string> _plotDef;
    void generate() const;
protected:
    mutable std::vector<Coefficients> _orb;
    void orthonormalize(bool Warn=true); ///< (pseudo)-Schmidt-orthonormalize, warn if not orthogonal
    BasisOrbital(std::string Name, ReadInput* Inp=0);
    std::vector<double> expectationValues(std::string OpDef) const;
public:
    virtual ~BasisOrbital(){}
    /// table of names, as Index may not exist yet upon creation of BasisOrbital (should go protected)
    static std::map<std::string,const Index*> referenceIndex;
    static void addIndex(std::string Name, const Index* Idx);

    virtual void generateOrbitals(const Index* Idx)=0;
    unsigned int size() const {return _orb.size();}
    const Coefficients * orbital(int N) const;
    std::vector<const Coefficients*> orbitals() const;

    ///\brief attach operator tree to Node, where at least IIndex, JIndex or both have BasisOrbital
    ///
    /// i.e. construct <orbBasis| OperDef | referenceBasis> etc.
    static void attachOperator(OperatorTree * Node, std::string Name, const OperatorDefinition & Def, const Index* IIndex, const Index* JIndex);

    std::vector<double> kineticEnergy() const {return expectationValues("0.5<<Laplacian>>");}
    std::vector<double> norms() const  {return expectationValues("<<Overlap>>");}
    bool isIndex() const {return true;}
};

#endif // BASISORBITAL_H
