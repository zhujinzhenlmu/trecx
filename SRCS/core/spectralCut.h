// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef SPECTRALCUT_H
#define SPECTRALCUT_H

#include "operatorAbstract.h"

class ReadInput;
class Discretization;
class BasicDisc;
class DiscretizationSpectral;



/** \ingroup Structures */
/// \brief apply spectral cuts
class SpectralCut : public OperatorAbstract
{
    DiscretizationSpectral * specD;
    BasicDisc * subD;
    std::vector<OperatorAbstract*> from,to;
public:
    ~SpectralCut();
    SpectralCut(const Discretization* Disc, ReadInput & Inp, std::string DefaultOper="");
    // apply is not meant to be used
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
};

#endif // SPECTRALCUT_H
