// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef __SAO__
#define __SAO__

#include "ao.h"

///
/// \brief The sao class: Symmetrized atomic orbitals class
///

class sao{

 public:

  ao A;
  Eigen::MatrixXd coef;
 
  sao(columbus_data *col_data);
  sao(QuantumChemicalInput &ChemInp);
  int number_saos();
  void sao_matrix_1e(Eigen::MatrixXcd &result, std::string op, sao* b= NULL);
  void values(Eigen::MatrixXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz);
  void values(Eigen::VectorXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, int fun_index);
  void derivatives(Eigen::MatrixXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, std::string wrt);
  void derivatives(Eigen::VectorXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, int fun_index, std::string wrt);
};

#endif
