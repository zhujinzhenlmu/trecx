// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef DERIVATIVEBLOCK_H
#define DERIVATIVEBLOCK_H

#include <vector>
#include <complex>


class Coefficients;
class OperatorFloor;
class ParallelFloor;
class OperatorTree;
class DerivativeBlock {
public:
    static bool lessEqual(const DerivativeBlock & A, const DerivativeBlock & B);
    static bool less(const DerivativeBlock & A, const DerivativeBlock & B);
    DerivativeBlock(std::vector<unsigned int> BlockSort,
                    const OperatorTree* OLeaf, Coefficients * ICoef, Coefficients*JCoef, double ONorm, double *XNorm);

    std::vector<int> _info; // floor structural info
    const OperatorTree * oLeaf;
    std::vector<Coefficients*> cInOut;

    //NOTE: factor duplicates oLeaf->floor()->factor - should be removed
    double eps;    // rejection threshold
    double* xNorm; // pointer to norm of pX

    std::vector<unsigned int> blockSort; // multi-index that will be used for sorting
    double load() const;
private:
    DerivativeBlock();
};


#endif // DERIVATIVEBLOCK_H
