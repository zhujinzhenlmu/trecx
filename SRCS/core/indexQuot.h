// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef INDEXQUOT_H
#define INDEXQUOT_H

#include "index.h"

/// \ingroup Index
/// \brief builds a quotient index - the levels NOT contained in Fac
class IndexQuot : public Index
{
public:
    IndexQuot(const Index * Full, const Index * Fac);
};

#endif // INDEXQUOT_H
