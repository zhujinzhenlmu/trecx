// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASISSUB_H
#define BASISSUB_H

#include "basisSet.h"
#include "str.h"
#include "qtEigenDense.h"

/** \ingroup Basissets */
/// \brief subset of the functions from a BasisSet
class BasisSub: public BasisAbstract
{
    friend class BasisAbstract;
    friend class BasisThread;
    std::vector<int> _subset;
    const BasisAbstract* _bas;

    BasisSub(const BasisAbstract *Bas, const std::vector<int> & Subset=std::vector<int>());
    BasisSub(std::string StrDefinition);
public:
    static const BasisAbstract* superBas(const BasisAbstract* Sub); ///< first super-basis that is not class BasisSub
    static std::vector<int> subset(const BasisAbstract* Sub); ///< subset wrt. superBas
    std::vector<int> subset() const { return _subset;}
    static Eigen::MatrixXcd subMatrix(const Eigen::MatrixXcd & Mat, std::vector<int> ISub, std::vector<int> JSub); ///< (to toolsEigen)

    void valDer(const UseMatrix &X, UseMatrix &Val, UseMatrix &Der, bool ZeroOutside) const;

    unsigned int size() const{return _subset.size();}
    unsigned int order() const{return superBas(this)->order();}
    std::string str(int Level) const;//{return Str("subset{","")+_subset.size()+"} of "+_bas->str(Level);}
    std::string strDefinition() const;
    static std::string strDefinition(const BasisAbstract* Bas, std::vector<int> Subset);

    static Eigen::MatrixXcd map(const BasisAbstract * Ibas, const BasisAbstract * Jbas);
    double physical(int Index) const override;
    bool operator==(const BasisAbstract &other) const;

};

#endif // BASISSUB_H
