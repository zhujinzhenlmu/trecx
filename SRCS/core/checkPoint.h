#ifndef CHECKPOINT_H
#define CHECKPOINT_H
#include "mpiWrapper.h"
#include <ctime>

class Wavefunction;
class Discretization;

#ifndef CHECKPOINT_EVERY_S
#define CHECKPOINT_EVERY_S 1200
#endif

class CheckPoint {
    bool initial;
    clock_t last_write;
    CheckPoint(): initial(true) {}
    int iterateCounts = 0;
    int countsPerCheck = INT_MAX;
    std::vector<MPI_Request> sendReq, recvReq;

public:
    static CheckPoint main;
    double startTime;

    /// Returns nullptr if no checkpoint was found
    Wavefunction *read(const Discretization *D) const;

    /// Call on every step, method will decide whther to store or not
    void write(const Wavefunction &Wf);
    bool startWrite();
    void writeTime(double time);
    double readTime();
};

#endif
