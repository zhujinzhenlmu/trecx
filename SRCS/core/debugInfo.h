#ifndef DEBUGINFO_H
#define DEBUGINFO_H

class Coefficients;
namespace debug_trecx{
bool destruct();
void unsetDestruct();
bool hasNan(const Coefficients* C);
extern int rank;
}
#endif // DEBUGINFO_H
