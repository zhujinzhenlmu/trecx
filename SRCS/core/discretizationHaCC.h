// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef DISCRETIZATIONHACC_H
#define DISCRETIZATIONHACC_H

#include "basicDisc.h"
#include "qtEigenDense.h"
#include "mol_sae_shelf.h"
#include "operatorAbstract.h"

/// Forward declarations
class ReadInput;
class Index;
class ci;
class mo;
class BasisFunctionCIion;
class BasisFunctionCINeutral;
class InverseDVR;

/** \ingroup Discretizations */
/// \brief The DiscretizationHaCC class: Handles ionic channel term in the haCC discretization
class HaccInverse;

class DiscretizationHaCC : public BasicDisc
{
    friend class HaccInverse;

    const OperatorAbstract* s0;
    const InverseDVR * s0Inv;

    HaccInverse * haccInv;

    /// Data members
    mo* MO;                                   /// Molecular orbitals
    bool anti_sym;                            /// Anti-symmetrize

    /// Ion and neutral basis functions
    BasisFunctionCIion* ionBf;
    BasisFunctionCINeutral* neutBf;

    /// Various helper functions
    void setup_Rho();      /// Setup Rho that is composed of all \rho^IJ and \eta^NI, linearly combined based on spin symmetry
    std::vector<int> getIndices(const Index* Idx) const;

public:
    static DiscretizationHaCC* current; // HACK
    DiscretizationHaCC(ReadInput &Inp, const std::string & Subset="", const BasisFunction *Neut=0);
    ~DiscretizationHaCC();
    unsigned int mos() const; ///< number of MOs
    unsigned int ions() const;///< number of ionic states
    virtual void operatorData(std::string def, const Index * const IFloor, const Index * const JFloor, std::vector<UseMatrix> &mats) const;
};

#endif // DISCRETIZATIONHACC_H
