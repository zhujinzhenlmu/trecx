// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef RADIALMULTIPOLE_H
#define RADIALMULTIPOLE_H

#include "toolsHeader.h"
#include "basisSet.h"

class UseMatrix;

class RadialMultipole
{
  BasisSet* B1;
  BasisSet* B2;

  int lambdaMax;
  std::vector<UseMatrix> mats;   // mats[lambda]

  void RadialMultipoleMatrix(UseMatrix& res, int lambda);
  void MatrixOnGrid(UseMatrix& res, int lambda, const UseMatrix& points1, const UseMatrix& weig1, const UseMatrix &points2, const UseMatrix &weig2) const;      // Evaluate the matrix on a grid
public:

  // Input basis sets: set1 and set2; new basis sets B1 and B2 will have order give by the order
  // Default order is 2*max(ord1, ord2) + lambdaMax
  RadialMultipole(const BasisSetDef* Set1, const BasisSetDef* Set2, int lambdaMax, std::vector<int> order=std::vector<int>(0));
  ~RadialMultipole();

  // Evaluate the operator on a given set of points & associated weights
  void MatrixOnGrid(std::vector<UseMatrix>& res, const UseMatrix& points1, const UseMatrix& weig1,  const UseMatrix &points2, const UseMatrix &weig2) const;
};

#endif // RADIALMULTIPOLE_H
