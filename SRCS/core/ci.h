// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef __CI__
#define __CI__

#include "sod.h"
#include "mo.h"

///
/// \brief The ci class: Configuration interation class
///  List of determinants, computed generalized reduced density matrices and matrix elements
///

class ci{
  
 public:
    int nElectrons() const {return det[0].nElectrons();}
  std::vector<Eigen::VectorXcd > coef;
  std::vector<sod > det;

  int total_states;
  ci(mo& L_Orb,int state);
  ci(mo &L_Orb);
  ci(columbus_data *ColuData, mo& L_Orb, int no_states, int start_state);
  ci(QuantumChemicalInput & ChemInp, mo& L_Orb, int no_states, int start_state);
  std::complex<double > ci_matel(std::string op, ci* sj=NULL);
  void ci_dyson(ci* sj, int state1=0, int state2 =0);

  void ci_dens1(Eigen::MatrixXcd &res, ci* sj=NULL,int state1=0,int state2=0);
  void ci_dens2(My4dArray &res, ci* sj=NULL, int state1=0, int state2=0);
  void ci_dens3_X_vee(Eigen::MatrixXcd &res,ci* sj=NULL,int state1=0,int state2=0);
  void ci_setup_all_dens(ci* sj=NULL);

  //void ci_matel_vector_multiple_states(Eigen::VectorXcd& res,string op, ci* sj);
  //void ci_matel_vector(Eigen::VectorXcd& res,string op, ci* sj, int state1=0, int state2=0);
  
  // special routines to compute x,y,z dipoles all at a time.
  //void ci_dipoles_vector_multiple_states(Eigen::VectorXcd& resx, Eigen::VectorXcd& resy, Eigen::VectorXcd& resz, string op, ci* sj);
  //void ci_dipoles_vector(Eigen::VectorXcd& resx, Eigen::VectorXcd& resy, Eigen::VectorXcd& resz, ci* sj, int state1=0, int state2=0);

  void ci_add_continuum(int spin);

  friend ostream& operator<<(ostream& os, const ci& s);
  void COMM_WORLD();
  void ci_setup_all_spindens(My4dArray_shared * Vee,vector<vector<Eigen::MatrixXd > > &rho1, vector<vector<My4dArray_shared *> > &rho2, vector<vector<Eigen::MatrixXd > > &rho3_X_vee, vector<double> lc_fac, vector<int> spin, Eigen::MatrixXd* ene_2e=NULL ,ci* sj=NULL);
  void ci_setup_all_spindyson(My4dArray_shared * Vee,vector<vector<Eigen::VectorXd > > &eta1, vector<vector<multiarray3d > > &eta3, vector<vector<Eigen::VectorXd > > &eta5, ci* sj);
  void add_multiplets(int Mult);
};


#endif
