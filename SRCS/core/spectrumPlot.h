#ifndef SPECTRUMPLOT_H
#define SPECTRUMPLOT_H

#include <string>
#include <vector>
#include <memory>

#include "plot.h"

class Coefficients;
class Discretization;
class OperatorMap;
class Index;

/**
 * \ingroup tSurff
 * \brief Plot spectra from differential scattering amplitude
 *
 * Various partial integrals of the spectrum are defined
 *
 * (Replaces Evaluator::plotSpectrum)
 */
class SpectrumPlot
{
    Plot plt; // eventually should turn into "is-a" relation

    std::vector<std::string> specFileHeader;
    bool overwrite;        ///< by default, most recent spectrum overwrites previous
    std::string plotWhat;  ///< what to plot: compute,total,partial,2dim, etc.
    std::string outputDir; //!< directory for output file

    // this is only informational - should be moved elswhere
    double runPropagationTime; ///< original run propagation time
    double integrationTime; ///< how far to integrate

    const Index *gridIdx, *basIdx;
    std::shared_ptr<Index> plotIdx;
    const OperatorMap* toBas;

    Coefficients* coefs; /// internal storage of amplitudes (for historical reasons on grid, better would be on basis)

    double yield() const;
public:
    ~SpectrumPlot();
    SpectrumPlot();
    SpectrumPlot(std::string OutputDir /** write spec-file to here */,
                 ReadInput& Inp);
    SpectrumPlot(std::string OutputDir,std::string PlotWhat,bool Overwrite,std::vector<std::string> InputFlags);
    const Coefficients* amplFromFile(std::string AmplDir); ///< set reference amplitude from ampl-file found in AmplDir
    void basFromGrid(const Coefficients &Coefs, int Deflate=2);///< set up map to basis-representation
    void plot(ReadInput& Inp); /// collect amplitudes from all path's and create spectrum plot
    void plot(std::string ExtName=""){plot(nullptr,ExtName);} ///< plot reference amplitude
    void plot(const Coefficients *Ampl, std::string ExtName=""); ///< Ampl must have same structure as reference amplitude
    std::string what() const {return plotWhat;} ///< (for transition only)
    void setInfo(const std::vector<std::string> Info){specFileHeader=Info;} ///< strings to be written into header of spec-file
    const Coefficients* ampl() const {return coefs;}

    /// convert to spectrum and add into columns
    void addPlot(const Coefficients *Ampl, std::string &SpecFile, double Scale=1., std::string Tag="");
    /// like addPlot, but preceded by subregion integration as in Defintion
    void addIntegratedPlot(std::string Definition, const Coefficients *Ampl, std::string SpecFile, double Scale=1., std::string Tag="");
    ///< convert to spectrum and add into columns
    void write(std::string ExtName=""); ///< write present plot with optional special extension
    std::string strPlotKind() const; ///< string defining plot for use in file-names

    static void read(ReadInput & Inp, std::string & What, bool & Overwrite, std::string &AmplFiles);
    static void allRegions(std::string OutputDir,ReadInput& Inp); /// scan all regions and produce spectra
};

#endif // SPECTRUMPLOT_H
