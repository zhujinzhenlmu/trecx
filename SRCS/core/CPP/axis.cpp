// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "axis.h"
#include "readInput.h"
#include "basisSet.h"
#include "basisNdim.h"
#include "printOutput.h"
#include "str.h"

using namespace std;
using namespace tools;

Axis::Axis(const Axis &Ax1, const Axis &Ax2){
    if(Ax1.name!=Ax2.name)ABORT("cannot merge axes with different coordinates");
    name=Ax1.name; // check useful name later
    coor=Ax1.coor;

    for(unsigned int k=0;k<Ax1.basDef.size();k++){
        const BasisSetDef * b1=&Ax1.basDef[k];
        if(b1->funcs.find("ploynomial")==string::npos or b1->comSca.eta!=1.)continue;

        for(unsigned int l=0;l<Ax2.basDef.size();l++){
            const BasisSetDef * b2=&Ax2.basDef[l];
            if(b2->funcs.find("ploynomial")==string::npos or b1->comSca.eta!=1.)continue;
            BasisSet* s1=BasisSet::get(*b1);
            BasisSet* s2=BasisSet::get(*b2);
            if(s1->hasOverlap(*s2)){
                basDef.push_back(*b1);
                double lo=max(b1->lowBound(),b2->lowBound());
                double up=min(b1->upBound(), b2->upBound());
                basDef.back().shift=lo;
                basDef.back().scale=(up-lo);
                basDef.back().order=max(b1->order,b2->order);
            }
        }
    }

}

Axis::Axis(string Coor, unsigned int Ncoefs, double Qlow, double Qup, string Functions, unsigned int Order, ComplexScaling Comsca)
    : comsca(Comsca) {
    construct(Coor,Ncoefs,Qlow,Qup,Functions,Order);
}

Axis::Axis(const string Name, const ComplexScaling Comsca, const BasisSetDef &Def):
    name(Name),comsca(Comsca)
{
    coor = Def.coor;
    basDef.assign(1,Def);
}

Axis::Axis(const std::string Name,const std::vector<const BasisAbstract*> & Bases)
    :name(Name),comsca(ComplexScaling()),coor(Coordinate::fromString(Name)),bases(Bases)
{}

// separate this for use by other routines;
std::string Axis::readName(ReadInput & Inp, int Line, string Default, std::string InputName){
    string coor; //
    Inp.read(InputName,"name",coor,Default,
             "coordinate string: X,Y,Z,R,Rn,Rho,Eta,Phi,Ndim,Vec,Channel,BLANK,Ion,Neutral,Orbital,Xperiodic,Yperiodic,PXi,PEta",Line);
    return coor;
}

Axis::Axis(ReadInput & in,unsigned int Line,string InputName){
    if(Line==0)DEVABORT("cannot read axis from Line=0");
    string Coor,functions;
    double qlow,qup;
    int ncoef,order;
    bool exact;

    // read axis name backward from current line until non-blank: we need not repeat axis name on each line
    for (int l=Line;l>0;l--){
        if("BLANK"!=(Coor=readName(in,l,"BLANK",InputName)))break;
    }

    if(Line!=0 and functions.find("grid")!=string::npos)
        ABORT("finite difference grid are only supported in 1d with a single Axis input line");

    Coordinate coor(Coor);
    in.read(InputName,"functions",functions,coor.defaultFunction(),"which functions to use",Line);
    const BasisAbstract * basNdim=BasisNdim::factory(functions);
    string scoef="-1";
    if(basNdim!=0)scoef=tools::str(basNdim->size());
    in.read(InputName,"nCoefficients",ncoef,scoef,"(approximate) number of coefficients",Line);
    if(not in.noInputFile() and ncoef==-1)return;

    in.read(InputName,"lower end",qlow,tools::str(coor.qmin),"lower boundary of the axis",Line);
    in.read(InputName,"upper end",qup ,tools::str(coor.qmax), "upper boundary of the axis",Line);
    in.read(InputName,"order",order,tools::str(Coordinate::automaticOrder(Coor,ncoef)),"number of functions on element (default - adjust to 4 elements)",Line);
    string strExact="false";
    if(coor.exactIntegration)strExact="true";
    in.read(InputName,"exactIntegral",exact,strExact,"use exaxt integrations for axis, even if overall femDVR",Line);
    if(functions=="laguerreExp" or functions=="legendre" or functions=="lobatto"  or functions=="radauExp"  )
        ABORT("functions \""+functions+"\" obsolete, use \"polynomial\" or \"polExp[...]\" instead");

    if(functions.find("polExp")==0){
        if(not functions.find("polExp[")==0)ABORT("specify exponent in the form \"polExp[0.123]\"");
        if(order!=-1)ABORT("do not specify order for basis polExp (is automatic)");
        order=ncoef;
    }
    else if(functions.find("besselCoulomb")==0){
        //        if(not functions.find("besselCoulomb[")==0)ABORT("specify exponent in the form \"besselCoulomb[0.123]\"");
        if(order!=-1)ABORT("do not specify order for basis besselCoulomb (is automatic)");
        order=ncoef;
    }

    if(Line!=1 and Coor=="Vec")ABORT("component axis Comp must be first in hierarchy");
    comsca=ComplexScaling(in,Coor);

    if(not in.noInputFile())construct(Coor,ncoef,qlow,qup,functions,order,exact);
}

void Axis::extendBox(std::vector<double> Box){
    if(name!="Rn")DEVABORT("for now only for Rn-axis, is: "+name);
    if(basDef.back().funcs.find("polExp")!=0)DEVABORT("assumes last is polExp, found: "+basDef.back().str());

    BasisSetDef ext=basDef[basDef.size()-2];
    double box=Box[1],len=ext.upBound()-ext.lowBound();
    if(abs(box-boxsize())<1.e-7*len)return; // do not extend, same size

    if(box-boxsize()<0.11*len)DEVABORT("extend size differst too little from present");

    int addElem=int((box-boxsize())/len+0.9);
    ext.scale=ext.scale*(box-boxsize())/(addElem*len);
    for(int k=0;k<addElem;k++){
        ext.shift+=len;
        basDef.insert(basDef.end()-1,1,ext);
    }
    basDef.back().shift=ext.upBound();
    // comsca info is duplicated - NOT NICE!
    comsca._r0upper=ext.upBound();

    for(auto &b: basDef){
        b.comSca=comsca;
    }
}

void Axis::truncateBox(std::vector<double> Box){

    double eps=(Box[1]-Box[0])*1.e-10;
    auto ele=basDef.begin();
    while(ele!=basDef.end()){
        if(ele->upBound()<Box[0]+eps or ele->lowBound()>Box[1]-eps)
            ele++; // keep element
        else if(ele->lowBound()>Box[0]-eps and ele->upBound()<Box[1]+eps)
            basDef.erase(ele,ele+1);
        else
            DEVABORT(Str("truncation boundary does not coincide with element boundary: [")+Box+"] vs ["+ele->lowBound()+ele->upBound()+"]");
    }
}

bool Axis::isElementBoundary(double Val) const{
    double eps=1.e-12;
    for(unsigned int k=0;k<basDef.size();k++){
        if(((basDef[k].lowBound()+eps<Val and Val<basDef[k].upBound()-eps) or
            (basDef[k].lowBound()+eps<Val and Val<basDef[k].upBound()-eps)))return false;
    }
    return true;
}

void Axis::fromFile(ReadInput &In, std::vector<Axis> & Ax, string Subset, string ReadCategory){
    Ax.clear();
    int l=0;
    string sub="";
    vector<Axis> allAx;
    while(not In.endCategory(ReadCategory,++l) ){
        Axis ax=Axis(In,l,ReadCategory);
        if(ax.basDef.size()==0)break; // empty axis

        // keep track of all axes on input
        allAx.push_back(ax);

        // check for subset
        if(Subset!=""){
            //Note: using previous sub as default - need to specify only for first axis of block
            In.read("Axis","subset",sub,sub,"axis will be read into named subset",l);
            if(sub!=Subset)continue; // ignore axes from non-matching subset
        }
        // compose subsequent sections of the same axis
        if(Ax.size()>0 and ax.name==Ax.back().name){
            Ax.back()=Ax.back().append(ax);
        } else {
            Ax.push_back(ax);
        }
    }
    for(unsigned int i=0;i<Ax.size();i++)Ax[i].check();
    for(unsigned int i=0;i<ComplexScaling::names.size();i++)
        if(not In.noInputFile())isNameOfAxis(ComplexScaling::names[i],allAx,"Absorption: axis");

    //HACK easy error for special case of spherical harmonics input
    //(could be moved to some separate auxiliary checking class)
    for(unsigned int k=0;k<Ax.size();k++){
        if(Ax[k].name.find("Phi")==0){
            for(unsigned int l=0;l<Ax.size();l++){
                if(Ax[l].name.find("Eta")==0){
                    if(Ax[l].basString().find(Ax[k].name)!=string::npos){
                        // related axes
                        if(l<k or Ax[l].maxOrder()*2<Ax[k].maxOrder()+1){
                            Axis::print(Ax);
                            ABORT("polar coordinates: Phi must be above Eta and must have (Phi order) < 2*(Eta order)");
                        }
                    }

                }
            }
        }
    }

}

bool Axis::isNameOfAxis(const string Name, const std::vector<Axis> Ax, const string Mess){
    string s;
    for(unsigned int l=0;l<Ax.size();l++){
        s+=" "+Ax[l].name;
        if(Name==Ax[l].name)return true;
    }
    if(Mess!="")ABORT(Mess+" \""+Name+"\"  does not name an axis:"+s);
    return false;
}

void Axis::construct(string Coor, int Ncoefs, double Qlow, double Qup, string Functions, int Order, bool ExactIntegral){
    if(Coor=="BLANK")return; // do not try to construct blank axis
    // many more consistency checks and default supplementation should go here
    name=Coor;
    coor=Coordinate(Coor);
    if (Ncoefs<1)error("need at least one coefficient");
    if (Order<1)error("order must be > 0");
    if (Order>Ncoefs)error("fewer coefficients than orders");
    if (Qlow >= Qup and Ncoefs>1)ABORT(Str("lower axis boundary must be below upper: ")+name+Qlow+Qup+Ncoefs);
    if (Functions=="automatic")Functions = coor.defaultFunction();

    // correct for tiny floating differences
    if(abs(Qlow-coor.qmin)<=1.e-10*abs(coor.qmin))Qlow=coor.qmin;
    if(abs(Qup -coor.qmax)<=1.e-10*abs(coor.qmax))Qup =coor.qmax;

    vector<int> marg(2);
    marg[0] = 0;       // left margin is first function of element
    marg[1] = Order - 1; // right margin is last function of element
    basDef.clear();
    double x0=Qlow,scal;

    string func=Functions.substr(0,Functions.find('['));
    if(func=="grid"){
        // for grids, make one single element, communicate fd order separately
        if(Ncoefs<1)ABORT("grid axis must have at least two points, found "+tools::str(Ncoefs));
        if(Order<3 or Order%2==0)ABORT("grid axis must have odd order >=3, found "+tools::str(Order));

        vector<double> parms;
        parms.push_back(double(Order));

        // exponential grid scaling damping factor
        parms.push_back(0.);
        if(Functions.find('[')!=string::npos)
            parms.back()=tools::string_to_double(tools::stringInBetween(Functions,"[","]"));
        basDef.push_back(BasisSetDef(Ncoefs-1,x0,Qup-Qlow,func,ExactIntegral,true,true,coor,marg,comsca,false,parms));
    }
    else {
        int nElem = Ncoefs / Order;
        for (int n = 0; n < nElem; n++){
            scal=(Qup-Qlow)/nElem;
            if((Functions.find("polExp[")==0)){
                //scaling is by inverse of exponent!
                scal=0.5/abs(tools::string_to_double(tools::stringInBetween(Functions,"[","]")));
                if(Qlow<-DBL_MAX/2){
                    scal=-scal; // polyExp on first element is reverted
                    x0=Qup;
                }
                else if(Qup<DBL_MAX/2)ABORT(Functions+" must have lower or upper boundary \"Infty\", is: ["+tools::str(Qlow,4)+","+tools::str(Qup,4)+"]");
            }
            string functions=Functions;
            if(Functions.find("Rl*")==0 and x0!=0.)functions=Functions.substr(3,Functions.find("{")-3);
            if(Functions.find("sqrt*")==0 and x0!=0.)functions=Functions.substr(5,Functions.find("{")-3);
            basDef.push_back(BasisSetDef(Order,x0,scal,functions,ExactIntegral,n==0,n==nElem-1,coor,marg,comsca));
            x0 +=(Qup-Qlow)/nElem;
        }
    }
}

void Axis::check(){
    if(basDef.size()==0)ABORT("no basis defined");
    if(coor.cString=="Ndim")return; // no checks on Ndim coordinate
    //    if(coor.jaco!=Coordinate::J_one)ABORT("coordinates with non-trivial Jacobian temporarily disabled");

    // check for suspicioius boundaries
    if (lowerEnd()<coor.qmin)error("axis boundary below minimum by: "+tools::str(lowerEnd()-coor.qmin)+" min: "+tools::str(coor.qmin));
    if (upperEnd()>coor.qmax)error("axis boundary above maximum by: "+tools::str(upperEnd()-coor.qmax)+" max: "+tools::str(coor.qmax));
    if (abs(coor.qmin) != DBL_MAX and lowerEnd()!= coor.qmin)
        cout << "\n!!!!!!!!! WARNING: lower boundary " + coor.cString + "=" <<lowerEnd()<< " above minimum=" << coor.qmin << "\n\n";
    if (abs(coor.qmax) != DBL_MAX and upperEnd()!= coor.qmax)
        cout << "\n!!!!!!!!! WARNING: upper boundary " + coor.cString + "=" <<upperEnd()<< " below maximum=" << coor.qmax << "\n\n";
    string forbid=".R.Rho.";
    if(forbid.find(coor.name())!=string::npos and comsca.eta!=1.)ABORT("complex scaling on R, Rho temporarily not admitted");
}

/// re-do the axis setup, using present data
void Axis::remake(bool Deriv){
    /// recalculate basis, possibly with different Deriv
    vector<int> marg(2);
    for (unsigned int i = 0; i < basDef.size(); i++){
        basDef[i].margin[0] = 0;					// left margin is first function of element
        basDef[i].margin[1] = basDef[i].order - 1;	// right margin is last function of element
        basDef[i].first=i==0;
        basDef[i].last=i=basDef.size()-1;
        basDef[i].deriv=Deriv;
    }

}

/// restrict range an order of an axis
void Axis::constrain(double Lower, double Upper, unsigned int Order){
    double lower=max(Lower,coor.qmin),upper=min(Upper,coor.qmax);
    vector<BasisSetDef> con;
    for (unsigned int i = 0; i < basDef.size(); i++){
        if(basDef[i].upBound()-lower<1e-12*abs(lower) or basDef[i].lowBound()-upper>-1.e-12*abs(upper))continue; // outside range
        con.push_back(basDef[i]);
        con.back().order=min(basDef[i].order,Order);
        con.back().margin[1]=con.back().order-1;
    }
    if(con.size()==0)ABORT("constrained axis has zero size: "+tools::str(Lower)+", "+tools::str(Upper)+" ("+tools::str(Order)+") ");
    con[0].first=true;
    con.back().last=true;
    basDef=con;
    if(lowerEnd()!=lower)PrintOutput::warning("constraint of axis boundary moved from "+tools::str(lower)+" to "+tools::str(lowerEnd()));
    if(upperEnd()!=upper)PrintOutput::warning("constraint of axis boundary moved from "+tools::str(upper)+" to "+tools::str(upperEnd()));
}

/// re-do the axis setup, using present data for overlap matrices for prolate spheroidal coordinates
void Axis::setupXiBasis(std::complex<double> s_k, std::complex<double> q_k){
    /// recalculate basis, possibly with some given overlap matrices
    vector<int> marg(2);
    for (unsigned int i = 0; i < basDef.size(); i++){
        basDef[i].margin[0] = 0;					// left margin is first function of element
        basDef[i].margin[1] = basDef[i].order - 1;	// right margin is last function of element
        basDef[i].first=i==0;
        basDef[i].last=i=basDef.size()-1;
        basDef[i].deriv=false;
    }
}

/// append an axis to present
Axis Axis::append(const Axis & app) const {
    if(basDef.size()==0)return app;
    if(app.basDef.size()==0)return Axis(*this);
    if(coor.name()!=app.coor.name())
        ABORT("cannot append axis "+app.coor.name()+" to "+coor.name());
    if(basDef.size()>1 and basDef.back().upBound()==DBL_MAX){
        show("axis");
        ABORT("cannot append to infinitely long axis");
    }

    // re-create axis with present comsca
    Axis sum((*this));
    sum.basDef.clear();
    for (unsigned int k=0;k<basDef.size();k++){
        sum.basDef.push_back(basDef[k]);
        sum.basDef.back().comSca=sum.comsca;
        sum.basDef.back().last=false;
    }

    // check whether we can append
    if(!(sum.comsca==app.comsca))ABORT("cannot append: complex scaling parameters do not match");
    if(abs(sum.upperEnd()-app.lowerEnd())>1.e-12*abs(basDef.back().scale)){
        sum.show("sum");
        app.show("app");
        ABORT("ends of axes must match: "+tools::str(sum.upperEnd())+" != "+tools::str(app.lowerEnd()));
    }

    // append further basis functions
    for (unsigned int k=0;k<app.basDef.size();k++){
        sum.basDef.push_back(app.basDef[k]);
        sum.basDef.back().comSca=sum.comsca;
        sum.basDef.back().first=false;
    }
    return sum;
}

void Axis::appendInPlace(const Axis & app){
    if(app.basDef.size()==0)return;
    if(coor.name()!=app.coor.name())
        error("cannot append axis "+app.coor.name()+" to "+coor.name());
    if(basDef.size()>1 and basDef.back().upBound()==DBL_MAX){
        show("axis");
        ABORT("cannot append to infinitely long axis");
    }

    // re-create axis with present comsca

    // check whether we can append
    if(!(comsca==app.comsca))ABORT("cannot append: complex scaling parameters do not match");
    if(abs(upperEnd()-app.lowerEnd())>1.e-12*abs(basDef.back().scale)){
        show("sum");
        app.show("app");
        ABORT("ends of axes must match: "+tools::str(upperEnd())+" != "+tools::str(app.lowerEnd()));
    }

    // append further basis functions
    basDef.back().last=false;
    for (unsigned int k=0;k<app.basDef.size();k++){
        basDef.push_back(app.basDef[k]);
        basDef.back().comSca=comsca;
        basDef.back().first=false;
    }
}

unsigned int Axis::maxSize() const {
    if(BasisNdim::factory(basDef[0].funcs)!=0)return basDef[0].order; //HACK case hacking...
    unsigned int sum=0;
    for(unsigned int k=0;k<basDef.size();k++)sum+=basDef[k].order-1;
    if(not BasisFunction::asympZero(basDef[0].funcs) and basDef[0].coor.zeroLow)sum--; // subtract if first coefficient is discarded
    if(BasisFunction::asympZero(basDef.back().funcs) or not basDef.back().coor.zeroUp)sum++; // add if last coefficient is not discarded
    return sum;
}
unsigned int Axis::maxOrder() const {
    unsigned int maxorder=0;
    for(unsigned int k=0;k<basDef.size();k++)maxorder=max(maxorder,basDef[k].order);
    return maxorder;
}
unsigned int Axis::minOrder() const {
    unsigned int minorder=INT_MAX;
    for(unsigned int k=0;k<basDef.size();k++)minorder=min(minorder,basDef[k].order);
    return minorder;
}


BasisSet * Axis::bas(BasisSetDef Def) const {
    return BasisSet::get(Def);
}

string Axis::basString(string Kind) const {
    string s=basDef[0].funcs;
    for(unsigned int n=0;n<basDef.size();n++)
        if(basDef[n].funcs!=s and s.find(basDef[n].funcs)==string::npos)
            s+="/"+basDef[n].funcs;
    if(s=="grid")s+="["+tools::str(basDef[0].par[1])+"]";
    if(Kind=="brief" and s.length()>20)s=s.substr(0,17)+"...";
    return s;
}

void Axis::show(string Text) const {
    if(Text!="")cout<<Text<<endl;
    cout<<"Name: "+str();
}

string Axis::str(int Brief) const {
    string s,b;
    s=name;
    int ncoef=0,maxOrder=0,minOrder=INT_MAX;
    for(unsigned int n=0;n<basDef.size();n++){
        ncoef+=basDef[n].order-1;
        if(n==0)ncoef++;
        maxOrder=max(maxOrder,(int) basDef[n].order);
        minOrder=min(minOrder,(int) basDef[n].order);
    }
    b+=tools::str(ncoef);
    if(maxOrder==minOrder){
        s+=" Approx. size (elements, order) "+tools::str(ncoef);
        s+=" ("+tools::str(basDef.size())+", "+tools::str(minOrder)+")";
        if(ncoef>maxOrder)b+="("+tools::str(maxOrder)+")";
    } else {
        s+=" Approx. size (elements, min/max order) "+tools::str(ncoef);
        s+=" ("+tools::str(basDef.size())+", "+tools::str(minOrder)+"/"+tools::str(maxOrder)+")";
        b+="("+tools::str(minOrder)+"/"+tools::str(maxOrder)+")";
    }
    // add interval, unless full coordinate axis
    if(lowerEnd()!=coor.qmin or upperEnd()!=coor.qmax or arg(comsca.eta)!=0.){
        b+="["+tools::str(lowerEnd(),3,DBL_MAX/2)+",";
        if(arg(comsca.eta)!=0.)b+=tools::str(int(comsca.r0up()))+"/"+tools::str(upperEnd(),3,DBL_MAX/2)+"@"+tools::str(int(arg(comsca.eta)/(math::pi)*180.))+"]";
        else b+=tools::str(upperEnd(),3,DBL_MAX/2)+"]";
    }
    for(unsigned int n=0;n<basDef.size();n++)s+="  "+basDef[n].str();
    if(Brief>0)return b;
    return s;
}

void Axis::print(const vector<Axis> & Ax,string File) {
    PrintOutput::end();
    PrintOutput::set(File);
    PrintOutput::paragraph();
    PrintOutput::newRow();
    PrintOutput::rowItem("Axis");
    PrintOutput::rowItem("maxCoeff");
    PrintOutput::rowItem("from");
    PrintOutput::rowItem("  to");
    PrintOutput::rowItem("order");
    PrintOutput::rowItem("basis");
    PrintOutput::rowItem("absorption: eta,inner");
    vector<string>basisDetails;
    for(unsigned int n=0;n<Ax.size();n++){
        PrintOutput::newRow();
        PrintOutput::rowItem(Ax[n].name);
        PrintOutput::rowItem(Ax[n].maxSize());
        if(BasisNdim::factory(Ax[n].basDef[0].funcs)!=0)continue; //HACK case hacking...
        if(Ax[n].lowerEnd()>-DBL_MAX/2)PrintOutput::rowItem(Ax[n].lowerEnd());
        else  PrintOutput::rowItem("-Infty|"+tools::str(Ax[n].basDef[0].upBound(),3));
        if(Ax[n].upperEnd()< DBL_MAX/2)PrintOutput::rowItem(Ax[n].upperEnd());
        else  PrintOutput::rowItem(tools::str(Ax[n].basDef[Ax[n].basDef.size()-1].lowBound(),3)+"|Infty");
        if(Ax[n].minOrder()+1>=Ax[n].maxOrder())PrintOutput::rowItem(Ax[n].maxOrder());
        else     PrintOutput::rowItem(tools::str(Ax[n].minOrder())+"-"+tools::str(Ax[n].maxOrder()));
        PrintOutput::rowItem(Ax[n].basString());
        PrintOutput::rowItem(Ax[n].comsca.str());

        if(Ax[n].basString()!=Ax[n].basDef[0].funcs)
            basisDetails.push_back(Ax[n].basString("full"));

    }
    PrintOutput::end();

    if(basisDetails.size()>0){
        PrintOutput::paragraph();
        PrintOutput::lineItem("Full basis name(s)",basisDetails[0]);
        for(int k=1;k<basisDetails.size();k++)
            PrintOutput::lineItem("",basisDetails[k]);
        PrintOutput::end();
    }
    PrintOutput::paragraph();
}

double Axis::lowerEnd() const {return basDef[0].lowBound();}
double Axis::upperEnd() const {return basDef[basDef.size()-1].upBound();}
double Axis::lowerRange() const {return basDef[0].upBound()-basDef[0].scale;}
double Axis::upperRange() const {return basDef[basDef.size()-1].lowBound()+basDef[basDef.size()-1].scale;}

double Axis::boxsize() const{
    double boxSize=lowerEnd();
    for(unsigned int k=0;k<basDef.size();k++){
        BasisSet* B = BasisSet::get(basDef[k]);
        if(basDef[k].funcs.find("polExp[")==0 or B->isAbsorptive())break;
        boxSize=B->upBound();
    }
    return boxSize;
}

// plot all basis functions on axis
void Axis::plot(string File, int Points, double QLow, double QUp) const {
    double plow=max(QLow,lowerEnd()),pup=min(QUp,upperEnd());

    // maximum number of basis functions on axis
    unsigned int maxcols=0;
    for(unsigned int n=0;n<basDef.size();n++)maxcols=max(maxcols,basDef[n].order);
    ofstream out;
    out.open(File.c_str());

    // loop through elements on axis
    for(double xi=plow;xi<=pup;xi+=(pup-plow)*(1.-1.e-15)/(max(1,Points-1)))
    {
        out<<setprecision(5);
        out<<setw(10)<<xi;
        for (unsigned int n=0;n<basDef.size();n++){
            if(basDef[n].lowBound()>xi or basDef[n].upBound()<xi)continue;
            // get x-points in range
            // evaluate basis functions on this grid
            complex<double>cxi=xi;
            ABORT("implement this");
            UseMatrix vals;//=basDef[n].val(UseMatrix::UseMap(&cxi,1,1),true);
            // write to file
            out<<setprecision(5);
            for(unsigned int k=0;k<vals.cols();k++)out<<" "<<setw(10)<<vals(0,k).real();
            for(unsigned int k=vals.cols();k<maxcols;k++)out<<" "<<setw(10)<<0.;
            out<<endl;
        }
    }

    out.close();
}

void Axis::error(string Message){ABORT("Axis "+coor.name()+": "+Message);}

