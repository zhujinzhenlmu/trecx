#include "indexExtract.h"

#include "mpiWrapper.h"
#include "threads.h"
#include "indexConstraint.h"
#include "basisSub.h"
#include "overlapDVR.h"
#include "inverseDvr.h"

static std::vector<std::string> ExtractAxes;

//NOTE: leaf's must always be accepted
static bool selectAxes(const Index* Idx){
    return std::find(ExtractAxes.begin(),ExtractAxes.end(),Idx->axisName())!=ExtractAxes.end();
}
static bool selectComplementOfAxes(const Index* Idx){
    return std::find(ExtractAxes.begin(),ExtractAxes.end(),Idx->axisName())==ExtractAxes.end();
}

Index* IndexExtract::get(const Index *Idx, const std::vector<std::string> Ax, bool Complement, const IndexConstraint *Constraint){
    // count expected levels
    int levels=0;
    std::vector<std::string> hier(tools::splitString(Idx->hierarchy(),'.'));
    for(std::string ax: Ax)levels+=(std::find(hier.begin(),hier.end(),ax)==hier.end())==Complement;
    if(levels<Ax.size())return 0;
    return new IndexExtract(Idx,Ax,Complement,Constraint);
}

// remove all floor markers below highest (there must be only a single floor in any branch)
static void cleanFloors(Index* Idx, bool FloorFound){
    for(int k=0;k<Idx->childSize();k++)cleanFloors(Idx->child(k),FloorFound or Idx->hasFloor());
    if(FloorFound)Idx->unsetFloor();
}

IndexExtract::IndexExtract(const Index *Idx, const std::vector<std::string> Ax, bool Complement, const IndexConstraint *Constraint){

    if(Complement)DEVABORT("Complement==true is broken");

    std::vector<std::string> hier(tools::splitString(Idx->hierarchy(),'.'));
    if(not Complement)
        ExtractAxes=Ax;
    else {
        // all axes NOT in Ax (avoid duplications)
        ExtractAxes.clear();
        for(std::string ax: hier)
            if(std::find(Ax.begin(),Ax.end(),ax)==Ax.end() and
                std::find(ExtractAxes.begin(),ExtractAxes.end(),ax)==ExtractAxes.end())
                ExtractAxes.push_back(ax);
    }

    // count expected levels
    int levels=0;
    for(std::string ax: ExtractAxes)levels+=std::count(hier.begin(),hier.end(),ax);
    if(levels<ExtractAxes.size())return;

    Idx->bottomExpandAll();
    if(Complement)_construct(this,Idx,selectComplementOfAxes);
    else          _construct(this,Idx,selectAxes);
    sizeCompute();

    if(Constraint!=0)Constraint->apply(this,{},{});
    purge(levels-1);
    cleanFloors(this,false);
    Idx->bottomUnexpandAll();
    bottomUnexpandAll();


    // this should go into Index member
    localOverlapAndInverse(0,0);
    Inverse::factory(this);
}

// first node that matches Select starting from Idx
static const Index* firstMatch(const Index *Idx, selectIndex Select){
    const Index* idx=Idx;
    // Note: leaf's cannot be omitted - always match
    while(idx!=0 and not (idx->isLeaf() and idx->axisName()=="NONE") and not Select(idx))idx=idx->nodeNext(Idx);
    return idx;
}

/// true is functions of ABas and BBas at branches ABranch and BBranch agree (mostly for subsets)
static bool sameFunction(const BasisAbstract* ABas, int ABranch, const BasisAbstract* BBas, int BBranch){
    // compare super-bases
    if(not (*BasisSub::superBas(ABas)==*BasisSub::superBas(BBas)))return false;
    // compare function numbers
    return BasisSub::subset(ABas)[ABranch]==BasisSub::subset(BBas)[BBranch];
}

// for use in std::find_if
static const Index* staticCurrentBranch;
static bool sameFunctionAsCurrent(const Index* Idx){
    if(Idx==staticCurrentBranch)return true;
    return sameFunction(Idx->parent()->basisAbstract(),Idx->nSibling(),
                        staticCurrentBranch->parent()->basisAbstract(),staticCurrentBranch->nSibling());
}

void IndexExtract::_construct(Index *Result, const Index *Idx, selectIndex Select){

    const Index* idx=firstMatch(Idx,Select);
    if(idx==0){
        Idx->bottomUnexpandAll();
        return;
    }

    Result->nodeCopy(idx,false); // copy this node
    Result->setBasis(BasisSub::superBas(Result->basisAbstract()));

    // set floor if needed, but avoid creating floors at leaf level
    // may create multiple floors - needs cleanup after construction is complete
    for(const Index* f=idx;f!=0;f=f->parent())
        if(f->hasFloor()){Result->setFloor();break;}

    // first matching level below idx
    std::vector<const Index*> attached;
    for(const Index* nxt=firstMatch(idx->descend(1,Idx),Select);nxt!=0;nxt=nxt->nodeRight(Idx))
    {
        // not all nodes on level may be eligible (e.g. for hybrid axis)
        if(nxt->isLeaf() or Select(nxt)){

            // get current branch of Idx (also for use in static sameFunctionAsCurrent)
            staticCurrentBranch=nxt;
            while(staticCurrentBranch->parent()->depth()!=Idx->depth())
                staticCurrentBranch=staticCurrentBranch->parent();

            if(not (*Result->basisAbstract()==*BasisSub::superBas(staticCurrentBranch->parent()->basisAbstract())))
                DEVABORT(Idx->str()+"\nnodes do not seem to derive from same Basis\n"+idx->strData()
                         +"\n"+staticCurrentBranch->parent()->strData()+"\n"
                         +Result->basisAbstract()->str()+" != "+BasisSub::superBas(staticCurrentBranch->parent()->basisAbstract())->str());

            // attach if on new function
            if(std::find_if(attached.begin(),attached.end(),sameFunctionAsCurrent)==attached.end()){
                int nFun=BasisSub::subset(staticCurrentBranch->parent()->basisAbstract())[staticCurrentBranch->nSibling()];
                attached.push_back(staticCurrentBranch);
                while(Result->childSize()<=nFun)Result->childAdd(new Index());
                _construct(Result->childBack(),nxt,Select);
            }
        }
    }

    if(attached.size()!=Result->childSize()){
        // only subset was attached - erase emtpy branches and adjust basis
        std::vector<int>sub;
        for(int k=Result->childSize()-1;k>=0;k--){
            if(Result->child(k)->basisAbstract())sub.insert(sub.begin(),k);
            else Result->childErase(k);
        }
        Result->setBasis(BasisAbstract::factory(BasisSub::strDefinition(Result->basisAbstract(),sub)));
    }
}

