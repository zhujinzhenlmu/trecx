// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "discretizationHaCC.h"
#include "mpiWrapper.h"
#include "mo.h"
#include "ci.h"
#include "index.h"
#include "operatorData.h"
#include "basisfunctionciion.h"
#include "basisfunctioncineutral.h"

#include "haccInverse.h"
#include "inverseDvr.h"
#include "overlapDVR.h"
#include "printOutput.h"
#include "quantumChemicalInput.h"
#include "operator.h"

#include "parallelOperator.h"

using namespace std;
#include "eigenNames.h"


DiscretizationHaCC* DiscretizationHaCC::current=0;

class _SinvS:public OperatorAbstract {
    const DiscretizationHaCC * hacc;
public:
    _SinvS(const DiscretizationHaCC * Hacc):OperatorAbstract("test",Hacc->idx(),Hacc->idx()),hacc(Hacc){}
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
        *tempRHS()=Vec;
        tempRHS()->makeContinuous();
        hacc->idx()->inverseOverlap()->apply(A,*tempRHS(),0.,*tempLHS());
        tempLHS()->makeContinuous();
        tempLHS()->makeContinuous();
        hacc->idx()->overlap()->apply(1.,*tempLHS(),B,Y);
        Y.makeContinuous();
        dynamic_cast<const HaccInverse*>(hacc->idx()->inverseOverlap())->projectDual(Y);
    }
};

DiscretizationHaCC::DiscretizationHaCC(ReadInput &Inp, const string &Subset, const BasisFunction *Neut)
    :BasicDisc(Inp,Subset),anti_sym(true)
{
    if(current!=0)ABORT("only one single DiscretizationHaCC allowed");
    current=this;

    My4dArray_shared * vee;
    QuantumChemicalInput::read(Inp);
    vee=QuantumChemicalInput::ions->vee;
    MO = new mo(*QuantumChemicalInput::ions,&axis,true);     // axis already exists as base class constructor called
    mo::main = MO;

    mo MN(*QuantumChemicalInput::neutrals,&axis);
    MatrixXcd ov;
    MO->matrix_1e(ov,"1",&MN);
    double err = (ov-MatrixXcd::Identity(MO->NumberOfOrbitals(),MN.NumberOfOrbitals())).array().abs().maxCoeff();
    if(err>1e-10) ABORT("Neutral and Ions are not based on the same molecular orbitals. Error = "+tools::str(err));

    /// Get neutral and ionic basisfunctions
    ionBf=0;
    neutBf=const_cast<BasisFunctionCINeutral* >(dynamic_cast<const BasisFunctionCINeutral*>(Neut));
    for(const Index* s=idx();not s->isLeaf(); s=s->child(0)){
        if(s->axisName()=="Ion"){
            BasisAbstract* B = const_cast<BasisAbstract*>(s->basisAbstract());
            BasisFunction* F = const_cast<BasisFunction*>(B->PointerToFunctions());
            ionBf = dynamic_cast<BasisFunctionCIion*>(F);
            break;
        }
    }
    if(ionBf==0) ABORT("cannot find ion basXXX");
    if(neutBf==0) PrintOutput::warning("Working without neutrals - initial state may not be accurate");

    /// Setup \eta's
    ionBf->setup_crossterms(vee,neutBf);

    /// setup Rho depending on spin of active electron
    setup_Rho();
    MPIwrapper::Barrier();

    Gaunt::main.initialize(2*max((int)MO->AxisForString("Eta")->basDef[0].order,*max_element(MO->mo_l.begin(),MO->mo_l.end())));

    /// setup 2e helpers
    MO->setup_Rs(ionBf,neutBf);

    // overlap and inverse (with continuity!)
    s0=new OverlapDVR(idx());
    s0Inv=new InverseDVR(idx());

    idx()->setOverlap(new OperatorTree("OverAntisymm",OperatorDefinition("<<FullOverlap>>", idx()->hierarchy()),idx(), idx()));
    ParallelOperator par{ idx()->overlap() };
    par.bcast();
    idx()->setInverseOverlap(new HaccInverse{this});
}

DiscretizationHaCC::~DiscretizationHaCC()
{
    if(MO!=0) {delete MO; MO=0;}
}


void DiscretizationHaCC::setup_Rho(){

    int numI = ionBf->order;
    int numN = 0;
    if(neutBf!=0) numN=neutBf->order;
    vector<int > spin=ionBf->spin;
    vector<double > lc_factors=ionBf->lc_factors;
    int multIon = ionBf->multIon;

    int no_st = numI;
    if(numN!=0) no_st+=1;
    int d = MO->coef.rows();
    ionBf->Rho = MatrixXd::Zero(numI*d+numN,numI*d+numN);
    //    if(MO->col_data->no_electrons>1)    ionBf->Rho3 = MatrixXd::Zero(numI*d+numN,numI*d+numN);
    if(ionBf->nElectrons()>1)    ionBf->Rho3 = MatrixXd::Zero(numI*d+numN,numI*d+numN);

    for(int i=0;i<no_st;i++)
        for(int j=0;j<no_st;j++){

            // summing over the appropriate linear combination for the given multiplicities
            if(i<numI and j<numI){
                MatrixXd mat = MatrixXd::Zero(d,d);
                MatrixXd mat3 = MatrixXd::Zero(d,d);
                for(int m1=0;m1<multIon;m1++)
                    for(int m2=0;m2<multIon;m2++)
                        if(abs(lc_factors[m1]*lc_factors[m2])>1e-14){
                            mat += ionBf->rho1[multIon*i+m1][multIon*j+m2].transpose().block(spin[multIon*i+m1]*d,spin[multIon*j+m2]*d,d,d)*lc_factors[m1]*lc_factors[m2];
                            if(ionBf->nElectrons()>2)
                                mat3 += ionBf->rho3_vee[multIon*i+m1][multIon*j+m2].transpose().block(spin[multIon*i+m1]*d,spin[multIon*j+m2]*d,d,d)*lc_factors[m1]*lc_factors[m2];
                        }
                if(not anti_sym) mat = MatrixXd::Zero(mat.rows(),mat.cols());
                ionBf->Rho.block(i*d,j*d,d,d) = mat;
                if(ionBf->nElectrons()>2)
                    ionBf->Rho3.block(i*d,j*d,d,d) = mat3;
            }
            else if(i==numI and j<numI){
                MatrixXd mat = MatrixXd::Zero(numN,d);
                MatrixXd mat3 = MatrixXd::Zero(numN,d);
                for(int neut=0;neut<numN;neut++)
                    for(int m2=0;m2<multIon;m2++){
                        mat.row(neut) += ionBf->eta1[neut][multIon*j+m2].segment(spin[multIon*j+m2]*d,d)*lc_factors[m2];
                        //                        if(MO->col_data->no_electrons>1)
                        if(ionBf->nElectrons()>1)
                            mat3.row(neut) += ionBf->eta5_X_Vee[neut][multIon*j+m2].segment(spin[multIon*j+m2]*d,d) * lc_factors[m2];
                    }

                ionBf->Rho.block(i*d,j*d,numN,d) = -mat;      // extra minus for convenience in inverting
                if(ionBf->nElectrons()>1)
                    ionBf->Rho3.block(i*d,j*d,numN,d) = mat3;
            }
            else if(i<numI and j==numI){
                MatrixXd mat = MatrixXd::Zero(d,numN);
                MatrixXd mat3 = MatrixXd::Zero(d,numN);
                for(int neut=0;neut<numN;neut++)
                    for(int m1=0;m1<multIon;m1++){
                        mat.col(neut) += ionBf->eta1[neut][multIon*i+m1].segment(spin[multIon*i+m1]*d,d)*lc_factors[m1];
                        //                        if(MO->col_data->no_electrons>1)
                        if(ionBf->nElectrons()>1)
                            mat3.col(neut) += ionBf->eta5_X_Vee[neut][multIon*i+m1].segment(spin[multIon*i+m1]*d,d)*lc_factors[m1];
                    }

                ionBf->Rho.block(i*d,j*d,d,numN) = -mat;       // extra minus for convenience in inverting
                if(ionBf->nElectrons()>1)
                    ionBf->Rho3.block(i*d,j*d,d,numN) = mat3;
            }
        }
}

std::vector<int> DiscretizationHaCC::getIndices(const Index* Idx) const
{
    vector<int> blockIdx(4,INT_MAX);

    int count = 0;
    for(const Index* s=Idx; ; s=s->parent()){
        if(s->parent()==0) break;
        if(s->parent()->axisName()=="Eta") blockIdx[0]=s->physical();//s->nSibling()+s->parent()->basisAbstract()->param(0);
        else if(s->parent()->axisName()=="Phi") blockIdx[1]=s->physical();//s->parent()->basisAbstract()->parameters()[s->nSibling()];
        else if(s->parent()->axisName()=="Ion") blockIdx[2]=s->nSibling();
        else if(s->parent()->axisName()=="Rn")  blockIdx[3]=s->nSibling();
        else if(s->parent()->axisName()=="Hybrid") continue;
        else ABORT("Unknown axis name: "+s->parent()->axisName());
        count++;
    }
    if(count!=4)  {
        ABORT("Passed index does not have all the indices");
    }

    return blockIdx;
}

void DiscretizationHaCC::operatorData(std::string def, const Index * const IFloor, const Index * const JFloor, std::vector<UseMatrix> &umats) const
{

    if(def.substr(1,7)!="NONORTH"){
        BasicDisc::operatorData(def,IFloor,JFloor,umats);
    }
    else{
        /// Terms due to non orthogonality

        def=def.substr(8,def.size()-9);    // Modify def

        /// Get block info: Idx=[l,m,I,n]_i and Jdx==[l,m,I,n]_j
        vector<int > blockIdx,blockJdx;
        blockIdx=getIndices(IFloor);
        int l_i=blockIdx[0];
        int m_i=blockIdx[1];
        int I_i=blockIdx[2];
        int n_i=blockIdx[3];
        blockJdx=getIndices(JFloor);
        int l_j=blockJdx[0];
        int m_j=blockJdx[1];
        int I_j=blockJdx[2];
        int n_j=blockJdx[3];

        MatrixXcd mat = MatrixXcd::Zero(IFloor->sizeStored(),JFloor->sizeStored());

        if(def!="EEInt"){

            /// 1 particle operators

            if(n_i<MO->gauge_boundary and n_j<MO->gauge_boundary){
                int d = MO->NumberOfOrbitals();
                MatrixXcd Opi,Opj,Oi,Oj;
                MO->matrix_1e(Oi,"1",n_i,l_i,m_i);
                MO->matrix_1e(Oj,"1",n_j,l_j,m_j);
                MO->matrix_1e(Opi,def,n_i,l_i,m_i);
                MO->matrix_1e(Opj,def,n_j,l_j,m_j);

                if(Oi.adjoint()!=Oi.transpose()){
                    if(IFloor->basisIntegrable()->eta()!=1. or JFloor->basisIntegrable()->eta()!=1.){
                        ABORT("(developer) complex orbital expansion - cannot use complex scaling");
                    }
                }

                if(def=="1")
                    mat-=Oi.adjoint()*ionBf->Rho.block(I_i*d,I_j*d,d,d)*Oj;
                else{
                    mat-=Opi.adjoint()*ionBf->Rho.block(I_i*d,I_j*d,d,d)*Oj;
                    mat-=Oi.adjoint()*ionBf->Rho.block(I_i*d,I_j*d,d,d)*Opj;

                    if(ionBf->nElectrons()>1){  //rho2 term
                        MatrixXcd t;
                        ionBf->setup_rho2_X_op(t,def,I_i,I_j);
                        if(not Oi.isZero(1e-14) and not t.isZero(1e-14) and not Oj.isZero(1e-14))
                            mat-=Oi.adjoint()*t.adjoint()*Oj;       // \sum_abcd rho2 h1 O O
                    }
                }
            }
        }

        else{

            /// Two particle operators

            /// Hartree potential, it can go beyond gauge boundary
            if(n_i==n_j and n_i<MO->numberOfelementBeforecomplexscaling()){
                MatrixXcd r;
                MO->matrix_poly_2e_direct(r,blockIdx,blockJdx);
                mat += r;
            }

            /// Exchange terms, switched-off before gauge boundary
            if(n_i<MO->gauge_boundary and n_j<MO->gauge_boundary and anti_sym){

                MatrixXcd Oi,Oj;
                MO->matrix_1e(Oi,"1",n_i,l_i,m_i);
                MO->matrix_1e(Oj,"1",n_j,l_j,m_j);

                /// Standard exchange term

                MatrixXcd r;
                bool isnotZer = MO->matrix_poly_2e_xch_v2(r,blockIdx,blockJdx,ionBf);
                if(isnotZer) mat -= r;


                /// Other terms with 2-particle reduced density matrix

                //                if(MO->col_data->no_electrons>1){
                if(ionBf->nElectrons()>1){
                    int mmax = (MO->AxisForString("Phi")->basDef[0].order-1)/2;   // DVR basis mmax - check?
                    if(l_j<=MO->mo_l[n_j] and abs(m_j)<=MO->mo_m[n_j])        //type 3 terms
                        mat -= MO->R_O[I_i][I_j][n_i][l_i][m_i+min(l_i,mmax)]*Oj;

                    if(l_i<=MO->mo_l[n_i] and abs(m_i)<=MO->mo_m[n_i])
                        mat -= (MO->R_O[I_j][I_i][n_j][l_j][m_j+min(l_j,mmax)]*Oi).adjoint();
                }


                /// Term with 3-particle reduced density matrix

                //                if(l_i<=MO->mo_l[n_i] and l_j<=MO->mo_l[n_j] and abs(m_i)<=MO->mo_m[n_i] and abs(m_j)<=MO->mo_m[n_j] and MO->col_data->no_electrons>2){
                if(l_i<=MO->mo_l[n_i] and l_j<=MO->mo_l[n_j] and abs(m_i)<=MO->mo_m[n_i] and abs(m_j)<=MO->mo_m[n_j] and ionBf->nElectrons()>2){
                    int d = MO->coef.rows();
                    MatrixXd rho = ionBf->Rho3.block(I_i*d,I_j*d,d,d);
                    if(not(rho.isZero(1e-14)))
                        mat -= 0.5*Oi.adjoint()*rho*Oj;
                }

            }
        }

        umats.push_back(UseMatrix::Zero(mat.rows(),mat.cols()));
        Map<MatrixXcd>(umats[0].data(),umats[0].rows(),umats[0].cols()) = mat;
    }
}


unsigned int DiscretizationHaCC::mos() const  {return MO->NumberOfOrbitals();}
unsigned int DiscretizationHaCC::ions() const {return ionBf->order;}
