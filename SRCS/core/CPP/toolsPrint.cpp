#include "toolsPrint.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <complex>

#include "coefficients.h"
#include "index.h"

namespace ToolsPrint
{

void CoefByFloors(std::string FileName, const Coefficients *C){
    Coefficients c(C->idx());
    c=*C;
    c.purgeNearZeros(1.e-12);
    std::ofstream fil((FileName).c_str(),(std::ios_base::openmode) std::ios::beg);
    for(const Coefficients * f=c.firstLeaf();f!=0;f=f->nextLeaf(&c)){
        fil<<(Sstr+f->idx()->index()+std::vector<std::complex<double>>(f->anyData(),f->anyData()+f->size()))<<std::endl;
    }
}

}
