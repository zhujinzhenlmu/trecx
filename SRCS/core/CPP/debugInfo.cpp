#include "debugInfo.h"

#include "coefficients.h"

static bool _destruct=true;
namespace debug_trecx {
int rank=-1;
bool destruct(){return _destruct;}

void unsetDestruct(){_destruct=false;}

bool hasNan(const Coefficients *C){
    if(C->orderedData()){
        for(int k=0;k<C->size();k++)
            if(std::isnan(C->orderedData()[k].real()) or std::isnan(C->orderedData()[k].imag()))return true;
    }
    else
        for(int k=0;k<C->childSize();k++)
            if(hasNan(C->child(k)))return true;

    return false;
}
}
