// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "basisIntegrable.h"

#include "readInput.h"
#include "basisSet.h"
#include "useMatrix.h"
#include "basisMat1D.h"
#include "printOutput.h"

double BasisIntegrable::infty=DBL_MAX;

void BasisIntegrable::valDer(const UseMatrix & X,UseMatrix & Val, UseMatrix & Der, bool ZeroOutside) const{
    std::vector<std::complex<double> >v,d,x;
    for(int k=0;k<X.size();k++)x.push_back(X.data()[k]);
    valDer(x,v,d,ZeroOutside);
    Val=UseMatrix::UseMap(v.data(),x.size(),v.size()/x.size());
    Der=UseMatrix::UseMap(d.data(),x.size(),d.size()/x.size());
}
void BasisIntegrable::valDerD(const std::vector<double > & X,
            std::vector<std::complex<double> > & Val,
            std::vector<std::complex<double> > & Der, bool ZeroOutside) const{
    std::vector<std::complex<double> >z;
    for(double d: X)z.push_back(d);
    valDer(z,Val,Der,ZeroOutside);
}

UseMatrix BasisIntegrable::val(const UseMatrix &Coordinates, bool ZeroOutside) const{
    UseMatrix v,d;
    valDer(Coordinates,v,d,ZeroOutside);
    return v;
}

// this is really bad - we should go directly through the std::vector
std::vector<std::complex<double> > BasisIntegrable::val(double X) const {
    UseMatrix v,d,x(1,1);
    x(0,0)=X;
    valDer(x,v,d);
    return std::vector<std::complex<double> >(v.data(),v.data()+v.size());
}
unsigned int BasisIntegrable::quadSizeSafe() const{
    DEVABORT("obsolete - use order() + something instead");
    //HACK
    if(name().find("ssoc")!=std::string::npos){
        return order()+std::abs(int(parameters()[0]))+2;
    }
    return order()+2;
}

UseMatrix BasisIntegrable::der(const UseMatrix &Coordinates, bool ZeroOutside) const{
    UseMatrix v,d;
    valDer(Coordinates,v,d,ZeroOutside);
    return d;
}

std::complex<double> BasisIntegrable::eta() const {
    return _comSca->etaX(0.5*(upBound()+lowBound()));
}

void BasisIntegrable::quadRule(int N, UseMatrix &QuadX, UseMatrix &QuadW) const {
    std::vector<double> x,w;
    quadRule(N,x,w);
    if(x.size()!=N)DEVABORT(Sstr+"quadrature size"+x.size()+"!= N="+N+"for basis"+str());
    QuadX=UseMatrix(N,1);
    QuadW=UseMatrix(N,1);
    for(int k=0;k<x.size();k++){
        QuadX(k)=x[k];
        QuadW(k)=w[k];
    }
}

//void BasisIntegrable::quadRule(int N, std::vector<double> &QuadX, std::vector<double> &QuadW) const{
//    UseMatrix x,w;
//    quadRule(N,x,w);
//    for(int i=0;i<x.size();i++){
//        QuadX.push_back(x(i).real());
//        QuadW.push_back(w(i).real());
//    }
//}

std::vector<std::vector<std::complex<double> > > BasisIntegrable::transZeroValDer(double Q) const{
    // evaluate values and derivatives at Q
    std::vector<std::complex<double> >vv,dd;
    valDer(std::vector<std::complex<double> >(1,Q),vv,dd,false);

    // single non-zero function
    int iNzVal;
    double nzVal=0.;
    for(int k=0;k<vv.size();k++){
        if(vv[k].imag()!=0.)DEVABORT("not for complex basis: "+str());
        if(abs(vv[k])>1.e-12){
            if(nzVal!=0.)DEVABORT(Str("only evaluate at node of DVR basis, values at Q are:")+vv);
            nzVal=std::abs(vv[k]);
            iNzVal=k;
        }
    }

    // largest in magnitude derivative
    int iNzDer;
    double nzDer=0.;
    for(int k=0;k<dd.size();k++){
        if(k!=iNzVal and nzDer<abs(dd[k]))iNzDer=k;
        nzDer=std::max(nzDer,std::abs(dd[k]));
    }
    if(nzDer==0.)DEVABORT("only zero derivatives?");

    // initial transformation (see notes Stiffness.pdf)
    UseMatrix dMat=UseMatrix::Identity(size(),size());
    for(int col=0;col<size();col++)
        if(col!=iNzDer)dMat(iNzDer,col)=dd[col]/dd[iNzDer];

    // label as last and next-to-last functions
    for(int k=0;k<size();k++){
        std::swap(dMat.data()[size()*iNzVal+k],dMat.data()[size()*(size()-1)+k]);
        std::swap(dMat.data()[size()*iNzDer+k],dMat.data()[size()*(size()-2)+k]);
    }

    // get metric and (bottom up) Gram-Schmidt orthonormalize
    BasisMat1D ovr("1",this,this);
    UseMatrix uOvr;
    uOvr=ovr.useMat();
    dMat.gramSchmidt(uOvr,std::vector<unsigned int>(),true);

    // put non-zeros into 0,1
    for(int k=0;k<size();k++){
        std::swap(dMat.data()[k],dMat.data()[size()*(size()-1)+k]);
        std::swap(dMat.data()[iNzDer+k],dMat.data()[size()*(size()-2)+k]);
    }

    std::vector<std::vector<std::complex<double > > >wMat(size());
    for(int k=0;k<size();k++)
        for(int l=0;l<size();l++)
            wMat[k].push_back(dMat(l,k).complex());
    return wMat;
}
