// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "rotateBasis.h"

#include "vectorComplex.h"
#include "basisSet.h"
#include "index.h"
#include "coefficients.h"
#include "operator.h"
#include "operatorFloor.h"


using namespace std;

RotateBasis::RotateBasis(const Index * Idx, std::vector<double> RotAx)
    :angle(0.)
{   
    // set up table of basic matrices, if needed
    if(tabLrot[Idx->hash()].size()==0){
        // find matching levels, verify that eta-axis is assocLegendre, i.e. angular eigenfunctions

        // determine maximal angular momentum
        unsigned int LOrder;

        // determine m-basis and check that l-basis is assocLegendre
        BasisSet * basM;

        // discretization of sphere
        IndexR idx(basM,LOrder);

        // loop through angular momentum operators
        vector<std::string> lnam,ldef;
        lnam.push_back("LX");ldef.push_back("-i<sin(Q)><sqrt(1-Q*Q)_d>+i<cos(Q)_d><sqrt(1-Q*Q)/Q>");
        lnam.push_back("LY");ldef.push_back(" i<cos(Q)><sqrt(1-Q*Q)_d>+i<sin(Q)_d><sqrt(1-Q*Q)/Q>");
        lnam.push_back("LZ");ldef.push_back("-i<1_d>");

        // here, OperatorFloor is not useful, switch off locally
        bool oldUse=Operator::useOperatorFloor;
        Operator::useOperatorFloor=false;

        for(unsigned int k=0;k=lnam.size();k++){
            // set up operator
            Operator Lop(lnam[k],ldef[k],0,0,&idx,&idx);

            // create angular momentum matrix storage for k'th component
            tabLrot[Idx->hash()].push_back(vector<VectorComplex>());
            for(unsigned int l=0;l<LOrder;l++)
                tabLrot[Idx->hash()][k].push_back(VectorComplex(pow(2*l+1,2),0.));

            // contract into l-blocks
            extract(Lop,tabLrot[Idx->hash()][k]);
        }

        // restore previous value
        Operator::useOperatorFloor=oldUse;
    }

    // determine permuted coefficients with eta and phi at floor
    vector<unsigned int> perm,iPerm; // permutation and its inverse
    Coefficients orig(Idx);
    Coefficients* angC;
    orig.permute(perm,*angC);
    angC->treeOrderStorage();
    angC->permute(iPerm,*origView);


    // get spectral decomposition for RotAx
    double angle=sqrt(pow(RotAx[0],2)+pow(RotAx[1],2)+pow(RotAx[2],2));
    for(unsigned int l=0;l<tabLrot[Idx->hash()].size();l++){
        transAx.push_back(tabLrot[Idx->hash()][l][0]*(RotAx[0]/angle));
        transAx.back()+=  tabLrot[Idx->hash()][l][1]*(RotAx[1]/angle);
        transAx.back()+=  tabLrot[Idx->hash()][l][2]*(RotAx[2]/angle);

        // get eigenvectors
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> t(Eigen::Map<Eigen::MatrixXcd>(transAx.back().data(),2*l+1,2*l+1));
        for(unsigned int k=0;k<2*l+1;k++)
            if(abs(t.eigenvalues()[k]-(k-l))>1.e-10)ABORT("eigenvalues do not seem to be correct");
        for(unsigned int n=0,ni=0;n<2*l+1;n++)
            for(unsigned int i=0;i<2*l+1;i++,ni++)
                transAx.back()[ni]=t.eigenvectors()(i,n);
    }
}

void RotateBasis::extract(Operator &Op, std::vector<VectorComplex> &LRot){
    for(unsigned int m=0;m<Op.O.size();m++){
        unsigned int im=Op.O[m]->iIndex->nSibling();
        unsigned int jm=Op.O[m]->jIndex->nSibling();
        unsigned int mi=(unsigned int)abs(Op.O[m]->iIndex->basisSet()->parameters()[im]);
        unsigned int mj=(unsigned int)abs(Op.O[m]->jIndex->basisSet()->parameters()[jm]);

        UseMatrix mat;
        Op.O[m]->matrix(mat);
        if(mat.rows()!=LRot.size()-mi or mat.cols()!=LRot.size()-mj)
            ABORT("dimensions not what they are supposed to be");

        for(unsigned int ll=0;ll<min(mat.rows(),mat.cols());ll++)
            LRot[ll+max(mi,mj)][im+Op.O.size()*jm]=mat(ll,ll).complex();

    }
}


void RotateBasis::rotate(double Angle, const Coefficients *C, Coefficients *RotC)
{
    // map input coefficients onto l-wise sorted vector
    if(C->parent()==0)*origView=*C;

    Coefficients* anglev=angC->descend(angC->height()-2);
    complex<double>* dat=angC->orderedData();
    while(anglev!=0){
        vector<complex<double> >aux(2*transAx.size()-1);
        for(unsigned int l=0;l<transAx.size();l++){
            // multiply by transpose of eigenvector matrix
            Eigen::Map<Eigen::VectorXcd>(aux.data(),2*l-1)=
                    Eigen::Map<Eigen::MatrixXcd>(transAx[l].data(),2*l-1,2*l-1).transpose()
                    *Eigen::Map<Eigen::VectorXcd>(dat,2*l-1);

            // multiply by exp(-i al m)
            for(unsigned int m=0;m<2*l+1;l++)
                dat[m]*=exp(complex<double>(0,-Angle*(m-l)));

            // multiply by eigenvector matrix
            Eigen::Map<Eigen::VectorXcd>(dat,2*l-1)=
                    Eigen::Map<Eigen::MatrixXcd>(transAx[l].data(),2*l-1,2*l-1)
                    *Eigen::Map<Eigen::VectorXcd>(aux.data(),2*l-1);
            dat+=2*l+1;

        }
        anglev=anglev->nodeRight();
    }

    // map back
    *RotC=*origView;
}

RotateBasis::IndexR::IndexR(const BasisSet *Phi, unsigned int LOrder, double M){
    if(Phi!=0){
        BasisSetDef mDef;//HACK(Phi->def);
        mDef.order=2*LOrder-1;
        setBasis(BasisSet::get(mDef));
        for(unsigned int k=0;k<basisSet()->size();k++)
            childAdd(new IndexR(0,LOrder,basisSet()->parameters()[k]));
    }
    else if (LOrder>0) {
        setBasis(BasisSet::get(BasisSetDef(LOrder,-1.,2.,string("assocLegendre"),true,true,true,
                                          Coordinate::fromString("Eta"),vector<int>(),
                                          ComplexScaling(),false,vector<double>(1,abs(M)))));
        for(unsigned int k=0;k<basisSet()->size();k++)childAdd(new IndexR(0,0));
    } else {
        // lowest level, Coefficient
        setBasis(BasisSet::getDummy(1));
        unsetFloor();
    }
}










