#include "basisOrbital.h"

#include "operatorTree.h"
#include "operatorFloor.h"
#include "index.h"
#include "operatorDefinitionNew.h"
#include "readInput.h"
#include "printOutput.h"
#include "operatorHartree.h"

using namespace std;

std::map<std::string,const Index*> BasisOrbital::referenceIndex;
void BasisOrbital::addIndex(std::string Name, const Index* Idx){
    referenceIndex[Name]=Idx;
}

void BasisOrbital::generate() const {

    if(_orb.size()==0 or _orb[0].idx()==0)
        const_cast<BasisOrbital*>(this)->generateOrbitals(0);
    if(_orb.size()==0 or _orb[0].idx()==0)
        DEVABORT("default generation of orbitals failed, run generateOrbitals(Index) before accessing orbitals");
}

std::vector<double> BasisOrbital::expectationValues(string OpDef) const{
    if(_orb.size()==0)return vector<double>();
    if(_orb.size()>0 and _orb[0].idx()==0)ABORT("run generateOrbitals() before computing expectationValues");
    OperatorTree op(OpDef,OpDef,_orb[0].idx(),_orb[0].idx());
    vector<double> expec;
    for(Coefficients c: _orb)expec.push_back(op.matrixElement(c,c).real());
    return expec;
}


BasisOrbital::BasisOrbital(string Name, ReadInput *Inp):BasisAbstract(Name){
    if(Inp==0)return;
    // read plot definition
    int line(0);
    std::string plt;
    do{
        Inp->read(Name,"plot",plt,"","specify AxName:{lowbound}:upboud{:npts}, lowbound defaults to -upbound, npts defaults to 100",++line);
        _plotDef.push_back(plt);
    } while (plt!="");
    _plotDef.pop_back();
}

const Coefficients *BasisOrbital::orbital(int N) const{
    generate();
    if(size()<N+1)
        ABORT(Str("there are only")+size()+"orbitals, cannot return N="+N);
    return &_orb[N];
}

std::vector<const Coefficients*> BasisOrbital::orbitals() const {
    generate();
    vector<const Coefficients*> res;
    for(const Coefficients &c: _orb)res.push_back(&c);
    return res;
}

void BasisOrbital::orthonormalize(bool Warn){
    if(_orb.size()==0)return;
    const OperatorAbstract * overlap=_orb[0].idx()->overlap();
    Coefficients *sCk=overlap->tempLHS();
    int k=0;
    double errOrtho=0.;
    while(k<_orb.size()){
        overlap->apply(1.,_orb[k],0.,*sCk);
        double errNorm=std::abs(std::abs(_orb[k].innerProduct(sCk))-1.);
        if(errNorm>1.e-12){
            PrintOutput::warning(Str("states not normalized by"," ")+errNorm+"- may need more accurate basis");
        }
        complex<double> a;
        for(int l=0;l<k;l++){
            a=_orb[l].innerProduct(sCk);
            _orb[k].axpy(-a,&_orb[l]);
            errOrtho=max(errOrtho,abs(a));
        }
        a=_orb[k].innerProduct(sCk);
        if(abs(a)<1.e-20)ABORT("vectors linearly dependent");
        _orb[k]*=1./sqrt(a);
        k++;
    }
    if(errOrtho>1.e-12)
        PrintOutput::warning(Str("orthonormalizing: orbitals not orthogonal by maximally"," ")+errOrtho+"- may need more accurate basis");

}

static Eigen::MatrixXcd allCoefs( vector<const Coefficients*> ICoefs){
    if(ICoefs.size()==0)DEVABORT("emtpy vector of Coefficients");
    Eigen::MatrixXcd allMat(ICoefs[0]->size(),ICoefs.size());
    for(int k=0;k<ICoefs.size();k++)allMat.col(k)=Eigen::Map<Eigen::MatrixXcd>(ICoefs[k]->orderedData(),ICoefs[k]->size(),1);
    return allMat;
}

// true if time-depedent factors agree (or none on both)
bool equalTimeDep(const OperatorTree* A, const OperatorTree* B){
    if(A->floor()==0)return B->floor()==0;
    if(B->floor()==0)return false;
    return A->floor()->factor()==B->floor()->factor();
}

static void contract(OperatorTree* &Node, const OperatorTree * OpFull, vector<const Coefficients*> ICoefs, vector<const Coefficients*> JCoefs){

    if(OpFull->floor()){

        Eigen::MatrixXcd ijmat;
        if(OpFull->floor()->rows()==0){
            // some parts of the full operator may not have been set up - defer to post-processing
            if(dynamic_cast<const OperatorHartree*>(OpFull->floor())){
                Node->floor()=new OperatorDUM(1.);
                Node->floor()->setFactor(OpFull->floor()->factor());
                return;
            }
            else
                DEVABORT("empty floor: "+OpFull->strData(-1)+" - cannot contract");
        }

        Eigen::MatrixXcd fmat=OpFull->floor()->matrix();
        Eigen::MatrixXcd imat =ICoefs.size()>0 ? allCoefs(ICoefs).adjoint()*fmat : fmat;
        ijmat=JCoefs.size()>0 ? imat*allCoefs(JCoefs) : imat;


        if(Node->floor()){
            if(Node->floor()->factor()!=OpFull->floor()->factor())
                DEVABORT("cannot contract, components have different time-depenendent factors:\n"
                         +OpFull->def());
            ijmat+=Node->floor()->matrix();
            delete Node->floor();
        }
        string hash(OpFull->def()+Node->iIndex->hash()+Node->jIndex->hash());
        Node->floor()=OperatorFloor::factory(vector<const Eigen::MatrixXcd*>(1,&ijmat),hash);
        Node->floor()->setFactor(OpFull->floor()->factor());
    }

    for (int n=0;n<OpFull->childSize();n++){

        vector<const Coefficients*>iCoefs;
        if(OpFull->child(n)->iIndex==OpFull->iIndex)
            iCoefs=ICoefs; // level does not change - keep coefs
        else for(const Coefficients*c: ICoefs)
            iCoefs.push_back(c->child(OpFull->child(n)->iIndex->nSibling()));

        vector<const Coefficients*>jCoefs;
        if(OpFull->child(n)->jIndex==OpFull->jIndex)
            jCoefs=JCoefs; // level does not change - keep coefs
        else for(const Coefficients*c: JCoefs)
            jCoefs.push_back(c->child(OpFull->child(n)->jIndex->nSibling()));

        const Index* iChild= ICoefs.size()>0 ? Node->iIndex : OpFull->child(n)->iIndex;
        const Index* jChild= JCoefs.size()>0 ? Node->jIndex : OpFull->child(n)->jIndex;

        // seek match: node or child (indices and possible time-dependent parameter)
        OperatorTree* nodeChild=0;
        if(Node->iIndex==iChild and Node->jIndex==jChild and equalTimeDep(Node,OpFull->child(n)))
            nodeChild=Node;
        else for(int k=0;k<Node->childSize();k++)
            if(Node->child(k)->iIndex==iChild and Node->child(k)->jIndex==jChild and equalTimeDep(Node->child(k),OpFull->child(n)))
                nodeChild=Node->child(k);

        // no match - create new
        if(nodeChild==0){
            Node->childAdd(new OperatorTree(OpFull->name,iChild,jChild));
            nodeChild=Node->childBack();
        }
        contract(nodeChild,OpFull->child(n),iCoefs,jCoefs);
    }
}

void BasisOrbital::attachOperator(OperatorTree* Node, std::string Name, const OperatorDefinition &Def, const Index* IIndex, const Index* JIndex){
    // get full indices on both sides
    const Index *iFull=IIndex,*jFull=JIndex;
    const BasisOrbital *ib,*jb;
    if(0!=(ib=dynamic_cast<const BasisOrbital*>(IIndex->basisAbstract())))iFull=ib->orbital(0)->idx();
    if(0!=(jb=dynamic_cast<const BasisOrbital*>(JIndex->basisAbstract())))jFull=jb->orbital(0)->idx();

    // get full operator
    OperatorTree oper(Name,Def,iFull,jFull);

    // recursively build
    vector<const Coefficients*> iCoefs,jCoefs;
    if(ib!=0)iCoefs=ib->orbitals();
    if(jb!=0)jCoefs=jb->orbitals();

    contract(Node,&oper,iCoefs,jCoefs);
}

void postProcess(OperatorTree* Op){
    // search for dummy floors

    // identify missing pieces
}


