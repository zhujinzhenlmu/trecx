// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "parallelCross.h"
#include "coefficients.h"
#include "index.h"

const Index* ParallelCross::index() const
{
    if(colBlock.size()==0 and rowBlock.size()==0)DEVABORT("emtpy cross");

    if(colBlock.size()>0)return colBlock[0]->cInOut[0]->idx();
    return rowBlock[0]->cInOut[1]->idx();
//    if(colBlock.size()>0)return colBlock[0]->oLeaf->jIndex;
//    return rowBlock[0]->oLeaf->jIndex;
}

// show row/column to which the cross belongs
std::string ParallelCross::str() const {
    std::string s;
    const Index* idx;
    if(colBlock.size()>0)     idx=colBlock[0]->cInOut[0]->idx();
    else if(rowBlock.size()>0)idx=rowBlock[0]->cInOut[1]->idx();
    s+=tools::str(idx->posIndex(),7)+"["+idx->hash()+"]";
    s+=":"+tools::str(int(rowBlock.size()))+"/"+tools::str(int(colBlock.size()));
    s+=" load "+tools::str(load(),2);

    return s;
}



