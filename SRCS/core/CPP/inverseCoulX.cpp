#include "inverseCoulX.h"
#include "index.h"
#include "operatorInverseCoulX.h"

InverseCoulX::InverseCoulX(const Index *Idx, unsigned int SubD, unsigned int SuperD, bool BandOvr):Inverse("Inv("+Idx->hierarchy()+")",Idx,Idx){
    opInvCoulX = new OperatorInverseCoulX(Idx,SubD,SuperD,BandOvr);
}

void InverseCoulX::apply0(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    opInvCoulX->apply(A,Vec,B,Y);
}
