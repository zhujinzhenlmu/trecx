// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "sod.h"
#include "My4dArray.h"
#include "extra_functions.h"

using namespace std;
#include "eigenNames.h"
using Eigen::RowVectorXi;


/*sod::sod(vector<int > spac, mo &Orb){

  this->spac = spac;
  this->Orb = &Orb;
  sort();
}*/
sod::sod(vector<int > spac, singleparticleorbitals &Orb){

  this->spac = spac;
  this->Orb = &Orb;
  sort();
}
void sod::sort(){
  
  // changes the indexing from columbus format, columbus always gives in sorted fashion, need not sort
  
  spin.resize(spac.size());
  spinup = 0;

  for(unsigned int i=0; i<spac.size(); i++){
    if(spac[i]<0){
      spin[i] = 0;
      spac[i] = abs(spac[i])-1;
    }
    else{
      spin[i] = 1;
      spac[i] = abs(spac[i])-1;
      spinup+=1;
    }
  }   
}

void sod::diff_det(const sod& dj, int ndiff, int& f_diff, vector<int >& diff_ind, int& sign){
  
  // Find the difference between two determinants.
  
  //auxiliary lists
  vector<int > li,lj;
  convert_to_combined_format(this->spac,this->spin,li);
  convert_to_combined_format(dj.spac,dj.spin,lj);

  sign=1;
  f_diff=0;
  int i,j,n = this->spac.size();
  vector<int> da,db;
  da.reserve(ndiff); db.reserve(ndiff);

  for (i=n-1;i>=0;i--)
    {for (j=n-1;j>=0;j--)
	if (li[i]==lj[j])
	  break;
      if (j==-1 and li[i]!=lj[0])
      	{da.push_back(i);
      	  f_diff+=1;
          if(f_diff > ndiff)
            return;
	}
    }

  f_diff=0;
  for (i=n-1;i>=0;i--)
    {for (j=n-1;j>=0;j--)
	if (lj[i]==li[j])
	  break;
      if (j==-1 and lj[i]!=li[0])
	{db.push_back(i);
	  f_diff+=1;
	}
    }
  
  if(da.size()!=db.size()){
    cout<<"some error in diff_det"<<endl;
    cout<<Map<RowVectorXi>(li.data(),li.size())<<endl<<Map<RowVectorXi>(lj.data(),lj.size())<<endl<<Map<RowVectorXi>(da.data(),da.size())<<endl<<Map<RowVectorXi>(db.data(),db.size())<<endl;
    exit(1);
  }
  
  if(f_diff > ndiff)
    return;

  diff_ind.reserve(2*f_diff);
  for(i=0;i<f_diff;i++){
    diff_ind.push_back(da[i]);
    diff_ind.push_back(db[i]);
  }

  if (f_diff<=ndiff){

      for(int ii=0;ii<f_diff;ii++)
        li[diff_ind[2*ii]]=lj[diff_ind[2*ii+1]]=1000+ii;     // Assuming we will not have a 1000

    int t=0,f;
    // creating maximum coincidence

    for(i=0;i<n;i++)
      if (li[i]!=lj[i])
	{for(j=0;j<n;j++)
	    {if (li[i]==lj[j])
		{t=j;
		  break;
		}
	    }
	  f=lj[i];
	  lj[i]=lj[t];
	  lj[t]=f;
	  sign*=-1;
	}
  }
}

complex<double > sod::sod_matel(string op, sod& dj){

  if(this->spinup!=dj.spinup) return 0;

  complex<double > res;
  if(op=="1" or op=="x" or op=="y" or op=="z" or op=="Lz" or op=="L^2"){
    int f_diff,sign;
    vector<int > diff_ind;
    diff_det(dj,1,f_diff,diff_ind,sign);
    MatrixXcd o;
    this->Orb->matrix_1e(o,op);
    if(f_diff>1)
      res = 0;
    else if(f_diff == 1){
      if(this->spin[diff_ind[0]] == dj.spin[diff_ind[1]]){
	if (sign>0)
	  res = o(this->spac[diff_ind[0]],dj.spac[diff_ind[1]]);
	else
	  res = -o(this->spac[diff_ind[0]],dj.spac[diff_ind[1]]);
      }
      else
	res = 0;
	}
    else if(f_diff == 0){
      res = 0;
      for(unsigned int i=0; i<this->spac.size(); i++)
	res += o(this->spac[i],this->spac[i]);
    }
  }  
  else if (op=="hamiltonian")
    res = this->sod_hamiltonian(dj);
  else
    ABORT("Operator not defined");

  return res;

}

void sod::sod_dyson(sod& dj, bool& res, int& pos, int& sign){

   // res = true if non-zero matrix element, else false

   if(this->spinup!=dj.spinup) {res = false; return ;}             

    int f_diff;
    vector<int > diff_ind;
    diff_det(dj,1,f_diff,diff_ind,sign);
    
    if(f_diff>1)
      {res = false; return ;}
    else if(f_diff == 1){
      if(this->spin[diff_ind[0]] == dj.spin[diff_ind[1]]){
          pos = this->spac[diff_ind[0]];
          res = true;
         }
      else
        res = false;
        }
    else if(f_diff == 0){
      cout<<"Something went wrong- this case should not occur";
    }
}

void sod::sod_dens1(sod &dj, bool &res, vector<int > &pos1, vector<int > &pos2, int &sign){

  // res = true if non-zero matrix element, else false

  if(this->spinup!=dj.spinup) {res = false; return ;}

  int f_diff;
  vector<int > diff_ind;
  diff_det(dj,1,f_diff,diff_ind,sign);

  if(f_diff>1)
    {res = false; return ;}
  else if(f_diff == 1){
    if(this->spin[diff_ind[0]] == dj.spin[diff_ind[1]]){
        pos1.push_back(this->spac[diff_ind[0]]);
        pos2.push_back(dj.spac[diff_ind[1]]);
        res = true;
        }
    else
      res = false;
      }
  else if(f_diff == 0){
      for(unsigned int ii=0;ii<this->spac.size();ii++){
          pos1.push_back(this->spac[ii]);
          pos2.push_back(dj.spac[ii]);
        }
      res = true;
  }

}

void sod::sod_dens2(sod &dj, bool &res, vector<Eigen::Vector4i > &pos, vector<int> &sign)
{
  sod di = *this;

  //Notation each element of pos has - abcd from notation \rho_{acbd} where a,b are from left function and c,d are from right function

  // res = true if non-zero matrix element, else false
  if(di.spinup!=dj.spinup) {res = false; return ;}

  int f_diff,sign1;
  vector<int > diff_ind;
  diff_det(dj,2,f_diff,diff_ind,sign1);

  if(f_diff>2)
    {res = false; return; }
  else if (f_diff==2) {
      res = false;
      if(di.spin[diff_ind[0]] == dj.spin[diff_ind[1]] and di.spin[diff_ind[2]] == dj.spin[diff_ind[3]]){
        pos.push_back(Eigen::Vector4i(di.spac[diff_ind[0]],dj.spac[diff_ind[1]],di.spac[diff_ind[2]],dj.spac[diff_ind[3]]));
        sign.push_back(sign1);
        res = true;
        }
      if(di.spin[diff_ind[0]] == dj.spin[diff_ind[3]] and di.spin[diff_ind[2]] == dj.spin[diff_ind[1]]){
        pos.push_back(Eigen::Vector4i(di.spac[diff_ind[0]],dj.spac[diff_ind[3]],di.spac[diff_ind[2]],dj.spac[diff_ind[1]]));
        int temp_sgn = sign1*-1;
        sign.push_back(temp_sgn);
        res = true;
        }
    }
  else if (f_diff==1) {
      res = false;
      if(di.spin[diff_ind[0]]==dj.spin[diff_ind[1]]){
          res = true;
          for(unsigned int i=0; i<di.spac.size(); i++){
            if((int)i==diff_ind[0])   continue;
            pos.push_back(Eigen::Vector4i(di.spac[diff_ind[0]],dj.spac[diff_ind[1]],di.spac[i],di.spac[i]));
            sign.push_back(sign1);
            if(di.spin[diff_ind[0]]==di.spin[i]){
               pos.push_back(Eigen::Vector4i(di.spac[diff_ind[0]],di.spac[i],di.spac[i],dj.spac[diff_ind[1]]));
               int temp_sgn = sign1*-1;
               sign.push_back(temp_sgn);
              }
          }
        }

    }
  else if (f_diff==0) {
      res = true;
      for(unsigned int i=0; i<di.spac.size(); i++)
        for(unsigned int j=0; j<i; j++){
          pos.push_back(Eigen::Vector4i(di.spac[i],di.spac[i],di.spac[j],di.spac[j]));
          sign.push_back(sign1);
          if (di.spin[i]==di.spin[j]){
            pos.push_back(Eigen::Vector4i(di.spac[i],di.spac[j],di.spac[j],di.spac[i]));
            int temp_sgn = sign1*-1;
            sign.push_back(temp_sgn);
            }
        }

    }
  //for(int i=0;i<pos.size();i++){
  //    cout<<pos[i].transpose()<<"    "<<sign[i]<<"  "<<res<<endl;
  //  }
}

void sod::sod_dens3(sod &dj, bool &res, vector<Matrix<int,6,1> > &pos, vector<int> &sign)
{
  sod di = *this;

  // res = true if non-zero matrix element, else false
  if(di.spinup!=dj.spinup) {res = false; return ;}

  int f_diff,sign1;
  vector<int > diff_ind;
  diff_det(dj,3,f_diff,diff_ind,sign1);

  //use notation rho_{ad be cf} , abc-left function indices and def-righ function indices
  // Each case would have 6 possible combinations:  abc x (def - dfe - edf + efd + fde - fed)

  vector<int > s1,s2,c1,c2;
  if(f_diff<=3){
    for(int i=0;i<f_diff;i++){
        s1.push_back(di.spin[diff_ind[2*i]]);
        s2.push_back(dj.spin[diff_ind[2*i+1]]);
        c1.push_back(di.spac[diff_ind[2*i]]);
        c2.push_back(dj.spac[diff_ind[2*i+1]]);
     }
    }

  vector<int > com1;
  convert_to_combined_format(c1,s1,com1);
  vector<int > com2;
  convert_to_combined_format(c2,s2,com2);

  vector<vector<int > > com1_list;
  vector<vector<int > > com2_list;

  if(f_diff>3) {res = false; return; }
  else if(f_diff==3){

      com1_list.push_back(com1);
      com2_list.push_back(com2);

    }
  else if(f_diff==2){
      for(unsigned int i=0;i<di.spac.size();i++){
          if((int)i == diff_ind[0] or (int)i == diff_ind[2]) continue;
          if(di.spin[i]==0)  {com1.push_back(-(di.spac[i]+1)); com2.push_back(-(di.spac[i]+1));}
          else               {com1.push_back(di.spac[i]+1);    com2.push_back(di.spac[i]+1);   }
          com1_list.push_back(com1);
          com2_list.push_back(com2);
          com1.pop_back();
          com2.pop_back();
        }
    }
  else if(f_diff==1){
      for(unsigned int i=0;i<di.spac.size();i++)
        for(unsigned int j=0;j<i;j++){
          if((int)i == diff_ind[0] or (int)j == diff_ind[0]) continue;
          if(di.spin[i]==0)  {com1.push_back(-(di.spac[i]+1)); com2.push_back(-(di.spac[i]+1));}
          else               {com1.push_back(di.spac[i]+1);    com2.push_back(di.spac[i]+1);   }
          if(di.spin[j]==0)  {com1.push_back(-(di.spac[j]+1)); com2.push_back(-(di.spac[j]+1));}
          else               {com1.push_back(di.spac[j]+1);    com2.push_back(di.spac[j]+1);   }
          com1_list.push_back(com1);
          com2_list.push_back(com2);
          com1.pop_back();
          com2.pop_back();
          com1.pop_back();
          com2.pop_back();
        }
    }
  else if(f_diff==0){
      for(unsigned int i=0;i<di.spac.size();i++)
        for(unsigned int j=0;j<i;j++)
          for(unsigned int k=0;k<j;k++){
            if(di.spin[i]==0)  {com1.push_back(-(di.spac[i]+1)); com2.push_back(-(di.spac[i]+1));}
            else               {com1.push_back(di.spac[i]+1);    com2.push_back(di.spac[i]+1);   }
            if(di.spin[j]==0)  {com1.push_back(-(di.spac[j]+1)); com2.push_back(-(di.spac[j]+1));}
            else               {com1.push_back(di.spac[j]+1);    com2.push_back(di.spac[j]+1);   }
            if(di.spin[k]==0)  {com1.push_back(-(di.spac[k]+1)); com2.push_back(-(di.spac[k]+1));}
            else               {com1.push_back(di.spac[k]+1);    com2.push_back(di.spac[k]+1);   }
            com1_list.push_back(com1);
            com2_list.push_back(com2);
            com1.pop_back();
            com2.pop_back();
            com1.pop_back();
            com2.pop_back();
            com1.pop_back();
            com2.pop_back();
          }
    }

  for(unsigned int i=0;i<com2_list.size();i++){
      int n=3;
      vector<int >  com22;
      com22 = com2_list[i];
      VectorXi v(n);
      for(int ii=0;ii<n;ii++)
        v(ii) = com22[ii];
      vector<VectorXi > perm;
      vector<bool> par;

      generate_permutations(v,perm,par);

      convert_to_seperate_format(com1_list[i],c1,s1);

      for(unsigned int ii=0;ii<perm.size();ii++){

          com22.clear();
          for(int jj=0;jj<n;jj++)
            com22.push_back(perm[ii](jj));
          convert_to_seperate_format(com22,c2,s2);
          //check if spins are equal
          bool nz=true;
          for(unsigned int j=0;j<s1.size();j++)
            if(s1[j]!=s2[j]){
                nz=false;
                break;
              }
          if(nz){
              VectorXi t(2*n);
              for(unsigned int j=0;j<c1.size();j++){
                  t(2*j) = c1[j];
                  t(2*j+1) = c2[j];
                }
              pos.push_back(t);

              if(par[ii])
                sign.push_back(sign1);
              else
                sign.push_back(-sign1);
            }
        }
      }

  if(pos.size()==0)
    res = false;
  else
    res = true;

}

void sod::sod_dens_n(int n, sod &dj, bool &res, vector<VectorXi > &pos, vector<int> &sign)
{

  sod di = *this;

  // if number of electrons is less than a the reduced density matrix asked for
  if(n>(int)di.spac.size()) {res = false; return ;}

  // res = true if non-zero matrix element, else false
  if(di.spinup!=dj.spinup) {res = false; return ;}

  int f_diff,sign1;
  vector<int > diff_ind;
  diff_det(dj,n,f_diff,diff_ind,sign1);

  //use notation rho_{ad be cf} , abc-left function indices and def-righ function indices
  // Each case would have 6 possible combinations:  abc x (def - dfe - edf + efd + fde - fed)

  vector<int > s1,s2,c1,c2;
  if(f_diff<=n){
    for(int i=0;i<f_diff;i++){
        s1.push_back(di.spin[diff_ind[2*i]]);
        s2.push_back(dj.spin[diff_ind[2*i+1]]);
        c1.push_back(di.spac[diff_ind[2*i]]);
        c2.push_back(dj.spac[diff_ind[2*i+1]]);
     }
    }

  vector<int > com1;
  convert_to_combined_format(c1,s1,com1);
  vector<int > com2;
  convert_to_combined_format(c2,s2,com2);

  vector<vector<int > > com1_list;
  vector<vector<int > > com2_list;

  if(f_diff>n) {res = false; return; }
  else if(f_diff==n){

      com1_list.push_back(com1);
      com2_list.push_back(com2);

    }
  else if(f_diff==n-1 and n-1>=0){
      for(unsigned int i=0;i<di.spac.size();i++){
          //if((int)i == diff_ind[0] or (int)i == diff_ind[2]) continue;
          vector<int > temp_di(n-1);                                                             //temp vector to store only first coordinate diff_indices
          for(unsigned int pp=0;pp<temp_di.size();pp++)
            temp_di[pp]=diff_ind[2*pp];
          if(std::find(temp_di.begin(),temp_di.end(),(int)i)!=temp_di.end())  continue;           //condition means that i is already present, so should not be used.

          if(di.spin[i]==0)  {com1.push_back(-(di.spac[i]+1)); com2.push_back(-(di.spac[i]+1));}
          else               {com1.push_back(di.spac[i]+1);    com2.push_back(di.spac[i]+1);   }
          com1_list.push_back(com1);
          com2_list.push_back(com2);
          com1.pop_back();
          com2.pop_back();
        }
    }
  else if(f_diff==n-2 and n-2>=0){
      for(unsigned int i=0;i<di.spac.size();i++)
        for(unsigned int j=0;j<i;j++){
          //if((int)i == diff_ind[0] or (int)j == diff_ind[0]) continue;
          vector<int > temp_di(n-2);                                                             //temp vector to store only first coordinate diff_indices
          for(unsigned int pp=0;pp<temp_di.size();pp++)
            temp_di[pp]=diff_ind[2*pp];
          if(std::find(temp_di.begin(),temp_di.end(),(int)i)!=temp_di.end())  continue;           //condition means that i is already present, so should not be used.
          if(std::find(temp_di.begin(),temp_di.end(),(int)j)!=temp_di.end())  continue;           //condition means that j is already present, so should not be used

          if(di.spin[i]==0)  {com1.push_back(-(di.spac[i]+1)); com2.push_back(-(di.spac[i]+1));}
          else               {com1.push_back(di.spac[i]+1);    com2.push_back(di.spac[i]+1);   }
          if(di.spin[j]==0)  {com1.push_back(-(di.spac[j]+1)); com2.push_back(-(di.spac[j]+1));}
          else               {com1.push_back(di.spac[j]+1);    com2.push_back(di.spac[j]+1);   }
          com1_list.push_back(com1);
          com2_list.push_back(com2);
          com1.pop_back();
          com2.pop_back();
          com1.pop_back();
          com2.pop_back();
        }
    }
  else if(f_diff==n-3 and n-3>=0){
      for(unsigned int i=0;i<di.spac.size();i++)
        for(unsigned int j=0;j<i;j++)
          for(unsigned int k=0;k<j;k++){
            if(di.spin[i]==0)  {com1.push_back(-(di.spac[i]+1)); com2.push_back(-(di.spac[i]+1));}
            else               {com1.push_back(di.spac[i]+1);    com2.push_back(di.spac[i]+1);   }
            if(di.spin[j]==0)  {com1.push_back(-(di.spac[j]+1)); com2.push_back(-(di.spac[j]+1));}
            else               {com1.push_back(di.spac[j]+1);    com2.push_back(di.spac[j]+1);   }
            if(di.spin[k]==0)  {com1.push_back(-(di.spac[k]+1)); com2.push_back(-(di.spac[k]+1));}
            else               {com1.push_back(di.spac[k]+1);    com2.push_back(di.spac[k]+1);   }
            com1_list.push_back(com1);
            com2_list.push_back(com2);
            com1.pop_back();
            com2.pop_back();
            com1.pop_back();
            com2.pop_back();
            com1.pop_back();
            com2.pop_back();
          }
    }

  for(unsigned int i=0;i<com2_list.size();i++){

      vector<int >  com11,com22;
      com11 = com1_list[i];
      com22 = com2_list[i];
      VectorXi v1(n),v2(n);
      for(int ii=0;ii<n;ii++){
        v1(ii) = com11[ii];
        v2(ii) = com22[ii];
        }
      vector<VectorXi > perm1,perm2;
      vector<bool> par1,par2;

        generate_permutations(v1,perm1,par1);
        generate_permutations(v2,perm2,par2);

        for(unsigned int kk=0;kk<perm1.size();kk++){

          com11.clear();
          for(int jj=0;jj<n;jj++)
            com11.push_back(perm1[kk](jj));
          convert_to_seperate_format(com11,c1,s1);

          for(unsigned int ii=0;ii<perm2.size();ii++){

              com22.clear();
              for(int jj=0;jj<n;jj++)
                com22.push_back(perm2[ii](jj));
              convert_to_seperate_format(com22,c2,s2);
              //check if spins are equal
              bool nz=true;
              for(unsigned int j=0;j<s1.size();j++)
                if(s1[j]!=s2[j]){
                    nz=false;
                    break;
                  }
              if(nz){
                  VectorXi t(2*n);
                  for(unsigned int j=0;j<c1.size();j++){
                      t(2*j) = c1[j];
                      t(2*j+1) = c2[j];
                    }

                      pos.push_back(t);

                      if(par1[kk])
                        sign.push_back(sign1);
                      else
                        sign.push_back(-sign1);
                      if(not par2[ii])
                        sign[sign.size()-1]*=-1;
                    }
            }
          }
        }

  if(pos.size()==0)
    res = false;
  else
    res = true;

}

void sod::sod_matel_vector(VectorXcd& res,string op,sod& dj){

  // should be used in following way <bound | op | continuum >

  if(this->spinup!=dj.spinup) return ;
  
  if(op=="1" or op=="x" or op=="y" or op=="z"){
    int f_diff,sign;
    vector<int > diff_ind;
    diff_det(dj,1,f_diff,diff_ind,sign);
    if(f_diff>1)
      return ;
    MatrixXcd o;

    this->Orb->matrix_1e(o,op);
    for(int r=0; r<res.size(); r++){
      if(f_diff>1)
	res(r) = 0;
      else if(f_diff == 1){
	if(this->spin[diff_ind[0]] == dj.spin[diff_ind[1]]){
	  if (sign>0)
	    res(r) = o(this->spac[diff_ind[0]],dj.spac[diff_ind[1]]+r);
	  else
	    res(r) = -o(this->spac[diff_ind[0]],dj.spac[diff_ind[1]]+r);
	}
	else
	  res(r) = 0;
      }
      else if(f_diff == 0){
	res(r) = 0;
	for(unsigned int i=0; i<this->spac.size(); i++)
	  res(r) += o(this->spac[i],this->spac[i]+r);
      }
    }
  }
  else{
    cout<<"operator incorrect "<<endl;
    exit(1);
  }

}

void sod::sod_dipoles_vector(VectorXcd& resx,VectorXcd& resy,VectorXcd& resz,sod& dj){

    int f_diff,sign;
    vector<int > diff_ind;
    diff_det(dj,1,f_diff,diff_ind,sign);
    if(f_diff>1)
      return ;
    for(int r=0; r<resx.size(); r++){
      if(f_diff == 1){
        if(this->spin[diff_ind[0]] == dj.spin[diff_ind[1]]){
          if (sign>0){
            MatrixXcd o;
            this->Orb->matrix_1e(o,"x");
            resx(r) =  o(this->spac[diff_ind[0]],dj.spac[diff_ind[1]]+r);
            this->Orb->matrix_1e(o,"y");
            resy(r) =  o(this->spac[diff_ind[0]],dj.spac[diff_ind[1]]+r);
            this->Orb->matrix_1e(o,"z");
            resz(r) =  o(this->spac[diff_ind[0]],dj.spac[diff_ind[1]]+r);
          }
          else{
            MatrixXcd o;
            this->Orb->matrix_1e(o,"x");
            resx(r) = -o(this->spac[diff_ind[0]],dj.spac[diff_ind[1]]+r);
            this->Orb->matrix_1e(o,"y");
            resy(r) = -o(this->spac[diff_ind[0]],dj.spac[diff_ind[1]]+r);
            this->Orb->matrix_1e(o,"z");
            resz(r) = -o(this->spac[diff_ind[0]],dj.spac[diff_ind[1]]+r);
         }
        }
        else{
          resx(r) = 0;
          resy(r) = 0;
          resz(r) = 0;
        }
      }
      else if(f_diff == 0){
        resx(r) = 0;
        resy(r) = 0;
        resz(r) = 0;
        for(unsigned int i=0; i<this->spac.size(); i++){
          MatrixXcd o;
          this->Orb->matrix_1e(o,"x");
          resx(r) += o(this->spac[i],this->spac[i]+r);
          this->Orb->matrix_1e(o,"y");
          resy(r) += o(this->spac[i],this->spac[i]+r);
          this->Orb->matrix_1e(o,"z");
          resz(r) += o(this->spac[i],this->spac[i]+r);
        }
      }
    }
}

complex<double > sod::sod_hamiltonian(sod& dj){

  sod di = *this;

  int f_diff,sign;
  vector<int > diff_ind;
  diff_det(dj,2,f_diff,diff_ind,sign);

  // 1 particle integrals
  complex<double > val1 = 0;
  MatrixXcd h1,kin,pot;
  this->Orb->matrix_1e(kin,"kin");     // currently has pnly kinetic energy--  NEED to add POTENTIAL ENERGY
  this->Orb->matrix_1e(pot,"pot");
  h1=kin+pot;

  if(f_diff > 1)
    val1 = 0;
  else if(f_diff==1){
    if(this->spin[diff_ind[0]] == dj.spin[diff_ind[1]]){
      if (sign>0)
	val1 = h1(this->spac[diff_ind[0]],dj.spac[diff_ind[1]]);
      else
	val1 = -h1(this->spac[diff_ind[0]],dj.spac[diff_ind[1]]);
    }
    else
      val1 = 0;
  }
  else if(f_diff == 0){
    val1 = 0;
    for(unsigned int i=0; i<this->spac.size(); i++)
      val1 += h1(this->spac[i],this->spac[i]);   
  }

  // 2 particle integrals
  My4dArray vee;
  this->Orb->matrix_2e(vee);
  complex<double > val2=0;

  if(f_diff > 2)
    val2 = 0;
  else if(f_diff==2){
    val2 = 0;
    if(di.spin[diff_ind[0]] == dj.spin[diff_ind[1]] and di.spin[diff_ind[2]] == dj.spin[diff_ind[3]])
      val2 += vee.val_at(di.spac[diff_ind[0]],dj.spac[diff_ind[1]],di.spac[diff_ind[2]],dj.spac[diff_ind[3]]);
    if(di.spin[diff_ind[0]] == dj.spin[diff_ind[3]] and di.spin[diff_ind[2]] == dj.spin[diff_ind[1]])
      val2 -= vee.val_at(di.spac[diff_ind[0]],dj.spac[diff_ind[3]],di.spac[diff_ind[2]],dj.spac[diff_ind[1]]);
  }
  else if(f_diff==1){
    val2 = 0;
    if(di.spin[diff_ind[0]]==dj.spin[diff_ind[1]])
      for(unsigned int i=0; i<di.spac.size(); i++){
        if((int)i==diff_ind[0])   continue;
        val2 += vee.val_at(di.spac[diff_ind[0]],dj.spac[diff_ind[1]],di.spac[i],di.spac[i]);
	if(di.spin[diff_ind[0]]==di.spin[i])
	  val2-=vee.val_at(di.spac[diff_ind[0]],di.spac[i],di.spac[i],dj.spac[diff_ind[1]]);
      }	
  }
  else if(f_diff == 0){
    val2 = 0;
    for(unsigned int i=0; i<di.spac.size(); i++)
      for(unsigned int j=0; j<i; j++){
        val2+=vee.val_at(di.spac[i],di.spac[i],di.spac[j],di.spac[j]);
	if (di.spin[i]==di.spin[j])
	  val2-=vee.val_at(di.spac[i],di.spac[j],di.spac[j],di.spac[i]);
      }
  }

  val2*=sign;

  return val1+val2;
}

void sod::sod_extend(int spin){
  // and add new function to spin orbital determinant
  // n is the new function number
  
  int n = this->Orb->NumberOfOrbitals();
  this->spac.push_back(n);
  this->spin.push_back(spin);

  if(spin==1)
    this->spinup+=1;
}

ostream & operator <<(ostream &os, const sod &s)
{
  for(unsigned int i=0;i<s.spac.size();i++){
      if(s.spin[i]==0)
        os<<-(s.spac[i]+1)<<" ";
      else
        os<<s.spac[i]+1<<" ";
    }
  os<<endl;

  return os;

}


void sod::convert_to_combined_format(vector<int> spc, vector<int> spn, vector<int>& res)
{
  res.clear();
  res.resize(spc.size());
  for(unsigned int i=0; i<spc.size();i++){
      if(spn[i]==0)
        res[i] = -(spc[i]+1);
      else if(spn[i]==1)
        res[i] = spc[i]+1;
      else{
        cout<<"Unexpected paramter: sod::convert_to_combined_format\n";
        exit(1);
        }
    }
}

void sod::convert_to_seperate_format(vector<int> com, vector<int> &spc, vector<int> &spn)
{
  spc.clear();
  spn.clear();
  spc.resize(com.size());
  spn.resize(com.size());

  for(unsigned int i=0;i<com.size();i++){
      if(com[i]<0){
          spn[i]=0;
          spc[i]=abs(com[i])-1;
        }
      else if(com[i]>0){
          spn[i]=1;
          spc[i]=abs(com[i])-1;
        }
      else{
          cout<<"Unexpected paramter: sod::convert_to_combined_format\n";
          exit(1);
        }
    }
}

void sod::sod_spindens_upto_n(int n, sod &dj, vector<bool > &res1, vector<vector<VectorXi > > &pos1, vector<vector<int> > &sign_final)
{

  sod di = *this;

  // if number of electrons is less than a the reduced density matrix asked for

  //if(n>di.spac.size()) {res1.resize(n); for(unsigned int i=0;i<res1.size(); i++) res1[i] = false; return ;}

  int f_diff,sign1;
  vector<int > diff_ind;
  diff_det(dj,n,f_diff,diff_ind,sign1);

  int n1=n;
  pos1.resize(n1);
  res1.resize(n1);
  sign_final.resize(n1);

  vector<int > di_comb,dj_comb;
  convert_to_combined_format(di.spac,di.spin,di_comb);
  convert_to_combined_format(dj.spac,dj.spin,dj_comb);

  //use notation rho_{ad be cf} , abc-left function indices and def-righ function indices
  // Each case would have 6 possible combinations:  abc x (def - dfe - edf + efd + fde - fed)

  for(int n=1; n<=n1;n++){

      vector<int > sign; bool res; vector<VectorXi> pos;
      vector<int > com1,com2;
      if(f_diff<=n){
          for(int i=0;i<f_diff;i++){
              com1.push_back(di_comb[diff_ind[2*i]]);
              com2.push_back(dj_comb[diff_ind[2*i+1]]);
            }
        }
      vector<vector<int > > com1_list;
      vector<vector<int > > com2_list;

      // Find all the additional indices in case of (fdiff<n)
      if(f_diff>n)
        {res = false;  sign_final[n-1] = sign;
          pos1[n-1] = pos;
          res1[n-1] = res;
          continue;
        }
      else if(f_diff==n){
          com1_list.push_back(com1);
          com2_list.push_back(com2);
        }
      else if(f_diff==n-1 and n-1>=0){             // need 1 extra index
          com1_list.reserve(di.spac.size());
          com2_list.reserve(di.spac.size());
          for(unsigned int i=0;i<di.spac.size();i++){

              vector<int > temp_di(n-1);                                                             //temp vector to store only first coordinate diff_indices
              Map<VectorXi>(temp_di.data(),temp_di.size()) = Map<VectorXi,0,Eigen::InnerStride<2> >(diff_ind.data(),diff_ind.size()/2);
              if(std::find(temp_di.begin(),temp_di.end(),(int)i)!=temp_di.end())  continue;           //condition means that i is already present, so should not be used.

              com1.push_back(di_comb[i]); com2.push_back(di_comb[i]);
              com1_list.push_back(com1);
              com2_list.push_back(com2);
              com1.pop_back();
              com2.pop_back();
            }
        }
      else if(f_diff==n-2 and n-2>=0){            // need 2 extra indices
          com1_list.reserve(std::pow(di.spac.size(),2)/2);
          com2_list.reserve(std::pow(di.spac.size(),2)/2);
          for(unsigned int i=0;i<di.spac.size();i++)
            for(unsigned int j=0;j<i;j++){

                vector<int > temp_di(n-2);                                                             //temp vector to store only first coordinate diff_indices
                Map<VectorXi>(temp_di.data(),temp_di.size()) = Map<VectorXi,0,Eigen::InnerStride<2> >(diff_ind.data(),diff_ind.size()/2);
                if(std::find(temp_di.begin(),temp_di.end(),(int)i)!=temp_di.end())  continue;           //condition means that i is already present, so should not be used.
                if(std::find(temp_di.begin(),temp_di.end(),(int)j)!=temp_di.end())  continue;           //condition means that j is already present, so should not be used

                com1.push_back(di_comb[i]); com2.push_back(di_comb[i]);
                com1.push_back(di_comb[j]); com2.push_back(di_comb[j]);
                com1_list.push_back(com1);
                com2_list.push_back(com2);
                com1.erase(com1.end()-2,com1.end());
                com2.erase(com2.end()-2,com2.end());
              }
        }
      else if(f_diff==n-3 and n-3>=0){             // need 3 extra indices
          com1_list.reserve(std::pow(di.spac.size(),3)/6);
          com2_list.reserve(std::pow(di.spac.size(),3)/6);
          for(unsigned int i=0;i<di.spac.size();i++)
            for(unsigned int j=0;j<i;j++)
              for(unsigned int k=0;k<j;k++){
                  com1.push_back(di_comb[i]); com2.push_back(di_comb[i]);
                  com1.push_back(di_comb[j]); com2.push_back(di_comb[j]);
                  com1.push_back(di_comb[k]); com2.push_back(di_comb[k]);

                  com1_list.push_back(com1);
                  com2_list.push_back(com2);
                  com1.erase(com1.end()-3,com1.end());
                  com2.erase(com2.end()-3,com2.end());
                }
        }

      //Find all permutations due to anti-symmetrization
      if(com1_list.size()>0){
          pos.reserve(com2_list.size()*std::pow(factorial(com1_list[0].size()),2));
          sign.reserve(com2_list.size()*std::pow(factorial(com1_list[0].size()),2));
        }
      for(unsigned int i=0;i<com2_list.size();i++){
          VectorXi v1(n),v2(n);
          v1 = Map<VectorXi>(com1_list[i].data(),n);
          v2 = Map<VectorXi>(com2_list[i].data(),n);

          vector<VectorXi > perm1,perm2;
          vector<bool> par1,par2;

          generate_permutations(v1,perm1,par1);
          generate_permutations(v2,perm2,par2);

          for(unsigned int kk=0;kk<perm1.size();kk++)
            for(unsigned int ii=0;ii<perm2.size();ii++){

                pos.push_back(VectorXi(2*n));
                Map<VectorXi, 0, Eigen::InnerStride<2> >(pos[pos.size()-1].data(),n) = perm1[kk];        //left hand indices at even positions
                Map<VectorXi, 0, Eigen::InnerStride<2> >(pos[pos.size()-1].data()+1,n) = perm2[ii];      //right hand indices at odd positions

                if(par1[kk] == par2[ii])      //same sign
                  sign.push_back(sign1);
                else
                  sign.push_back(-sign1);
              }
        }

      if(pos.size()==0)
        res = false;
      else
        res = true;

      sign_final[n-1] = sign;
      pos1[n-1] = pos;
      res1[n-1] = res;

    }

}


