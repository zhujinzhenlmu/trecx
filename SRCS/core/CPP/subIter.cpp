// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "subIter.h"

SubIter::SubIter()
{
}


void SubIter::eigen(unsigned int Nvec, std::vector<Coefficients *> Rvec, std::complex<double> Eval, double Eps){
    // check Rvec, possibly fill with random values and orthogonalize

    // extend to working size Nwork

    // iterate:
    // generate new set of vectors from old by applying preconditioned operator

    // solve larger eigenproblem

    // reduce to Nwork

    // convergence criterion: check residues
}
