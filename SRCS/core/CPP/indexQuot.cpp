// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "indexQuot.h"

#include <string>

using namespace std;

IndexQuot::IndexQuot(const Index *Full, const Index *Fac)
{
    if(Full->isRoot()){
        // check
        for(int k=0;Fac->descend(k)!=0;k++)
            if(Fac->descend(k)->axisLevel(Full)==npos){
                cout<<"Fac\n"<<Fac->str()<<endl;
                cout<<"Full\n"<<Full->str()<<endl;
                ABORT("not all levels of Fac in Full");
            }
    }

    while(Full->axisLevel(Fac)!=npos and not Full->isLeaf()){
        Full=Full->descend();
        if(Full==0)return;
    }
    nodeCopy(Full,false);
    for (int k=0;k<Full->childSize();k++)
        childAdd(new IndexQuot(Full->child(k),Fac));

    if(Full->isRoot())sizeCompute();
}
