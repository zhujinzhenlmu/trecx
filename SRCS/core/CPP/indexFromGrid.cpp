#include "indexFromGrid.h"
#include "basisGrid.h"

using namespace std;

IndexFromGrid::IndexFromGrid(const Index *IdxGrid, std::vector<string> Deflate, vector<const Index*> Path)
{

    setAxisName(IdxGrid->axisName());

    Deflate.resize(Path.size(),"1"); // supplement deflate factor where missing
    setBasis(basisFromGrid(IdxGrid,Deflate[IdxGrid->depth()],Path));

    Path.push_back(this);
    for(int k=0;k<basisAbstract()->size();k++)
        childAdd(new IndexFromGrid(IdxGrid->child(k),Deflate,Path));
}

const BasisAbstract * IndexFromGrid::basisFromGrid(const Index* GridIdx, string ContractFactor, std::vector<const Index*> Path) {
    if(not GridIdx->basisAbstract()->isGrid())return GridIdx->basisAbstract();
    const BasisGrid *g=BasisGrid::factory(GridIdx->basisAbstract());

    Coordinate coor=Coordinate::fromString(GridIdx->axisName());
    if(coor.defaultFunction()=="useIndex")return GridIdx->basisAbstract();
    if(not GridIdx->subEquivalent())return GridIdx->basisAbstract();

    Str db(coor.defaultFunction(),"");
    vector<double>par;
    if(tools::findFirstOutsideBrackets(db,"{","[","]")){
        string depAx=tools::stringInBetween(db,"{","}"); // dependent axis
        if(GridIdx->axisName().find_first_of("0123456789")!=string::npos)
            depAx+=GridIdx->axisName().substr(GridIdx->axisName().find_first_of("0123456789")); // append numbering (if any)
        for(const Index* depIdx: Path){
            if(depIdx->axisName()==depAx){
                db=Str(db.substr(0,db.find("{")),"")+"["+depIdx->basisAbstract()->physical(depIdx->childSize())+"]";
                par.push_back(depIdx->basisAbstract()->physical(depIdx->childSize()));
            }
        }
    }
    int order=GridIdx->basisAbstract()->size()/tools::string_to_int(ContractFactor);
    BasisSetDef def(order,coor.qmin,coor.qmax-coor.qmin,coor.defaultFunction(),true,true,true,
                    coor,{0,order-1},ComplexScaling(),false,par);

    return BasisAbstract::factory(def);
}
