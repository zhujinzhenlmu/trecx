#include "spectrumLinCom.h"

#include "qtEigenDense.h"
#include <vector>
#include <map>
#include "constants.h"
#include "tools.h"
#include "str.h"

#include "readInput.h"
#include "printOutput.h"

#include "spectrumPlot.h"
#include "coefficients.h"

#include "algebra.h"
#include "vinayData.h"
#include "index.h"
#include "eigenTools.h"

using namespace std;

void SpectrumLinCom::read(string &RangePhiCEO, string &RangeAlign, string &RangeAver){
    ReadInput::main.read("LinCom","phiCEO",RangePhiCEO,ReadInput::flagOnly,"(rad) linearly combine for carrier-envelop offset (for range, e.g., -phiCEO=[0,pi/2,11])",1,"phiCEO");
    ReadInput::main.read("LinCom","alignmentAngle",RangeAlign,ReadInput::flagOnly,"(deg) linearly combine for alignement (for range -align=[0,70,36])",1,"align");
    ReadInput::main.read("LinCom","averageAlignement",RangeAver,ReadInput::flagOnly,"(deg) linearly combine for alignement -average)",1,"average");
    ReadInput::main.exclude("LinCom","LinCom","alignmentAngle","averageAlignement");
}

bool SpectrumLinCom::found(){
    std::string dum;
    read(dum,dum,dum); // make this appear in the doc;
    return  ReadInput::main.found("LinCom","phiCEO","phiCEO") or
            ReadInput::main.found("LinCom","alignmentAngle","align") or
            ReadInput::main.found("LinCom","averageAlignement","average");
}

/// input Dirs must be either VinayData/.. or have linp-files <br>
SpectrumLinCom::SpectrumLinCom(std::vector<string> Dirs)
{
    if(Dirs.size()<2)ABORT("single input file - cannot define linear combination");
    string outputDir=Dirs[0];
    if(outputDir.find("VinayData")!=string::npos)outputDir=Dirs[0].substr(0,Dirs[0].find("/ampl"));

    SpectrumPlot plt(outputDir,ReadInput::main);
    vector<string> kinds={"phiCEO","polarAngle"};

    PrintOutput::title("AMPLITUDE FILES");
    PrintOutput::paragraph();
    PrintOutput::newRow();
    PrintOutput::rowItem("File");
    PrintOutput::rowItem("alignment");
    PrintOutput::rowItem("phiCEO");
    string sampleGrid="";
    for(std::string inp: Dirs){
        vector<string> filePar=tools::splitString(inp,':');
        if(filePar[0]=="")filePar[0]=Dirs[0];

        amplFiles.push_back(filePar[0]);
        if(amplFiles.back().find("VinayData")==string::npos){
            _ampl.push_back(*plt.amplFromFile(amplFiles.back()+"/ampl"));

            _phiCEO.push_back(getValue("phiCEO[1]",kinds,filePar));
            if(0.!=getValue("phiCEO[2]",kinds,filePar))ABORT("need phiCEO==0 for all other components");
            _theAlign.push_back( getValue("polarAngle[1]",kinds,filePar));
            if(_theAlign.back()!=getValue("polarAngle[2]",kinds,filePar))ABORT("only for parallel laser components");
            _theAlign.back()*=math::pi/180.; //< input is (regretably) in degrees

        }
        else{
            VinayData vind(amplFiles.back());
            if(channel=="")channel=vind.channel();
            else if (channel!=vind.channel())ABORT(Str("amplitude files belong to different channels","\n")+amplFiles);
            _ampl.push_back(*vind.ampl());
            _theAlign.push_back(vind.polarAngle());
            _phiCEO.push_back(vind.phiCEO());

            if(sampleGrid!="" and sampleGrid!=vind.sampleGrid() )ABORT("amplitude grids differ, use -interpolate");
            sampleGrid=vind.sampleGrid();
        }

        PrintOutput::newRow();
        PrintOutput::rowItem(amplFiles.back());
        PrintOutput::rowItem(_theAlign.back());
        PrintOutput::rowItem(_phiCEO.back());
    }
    PrintOutput::paragraph();

    // lock all amplitudes' phases to same value at peak (XUV photo-electron)
    // global random phases may come from random initial state phase
    phaseLockAtMax(_ampl);

    string rangePhiCEO;
    string rangeAlign,rangeAver;
    read(rangePhiCEO,rangeAlign,rangeAver);

    int needFiles=1;
    if(rangePhiCEO==ReadInput::flagOnly){
        if(std::count(_phiCEO.begin(),_phiCEO.end(),_phiCEO[0])==_phiCEO.size())gridPhiCEO={_phiCEO[0]};
        else ABORT("phiCEO differs in ampl-files, specify -phiCEO");
    }
    else {
        needFiles*=2;
        gridPhiCEO=tools::rangeToGrid(rangePhiCEO);
        for(double g: gridPhiCEO)
            if(g<-6.2832 or 6.2832<g)ABORT(Str("enter phiCEO in the range [-2pi,2pi], found: ")+rangePhiCEO);
    }

    if(rangeAlign==ReadInput::flagOnly and rangeAver==ReadInput::flagOnly){
        if(std::count(_theAlign.begin(),_theAlign.end(),_theAlign[0])==_theAlign.size())gridAlign={_theAlign[0]};
        else ABORT("alignments differs in ampl-files, specify -align=...");
    }
    else {
        needFiles*=3;
        if(rangeAlign!=ReadInput::flagOnly){
            gridAlign=tools::rangeToGrid(rangeAlign);
        }
        else {
            gridAlign=tools::rangeToGrid(rangeAver);
            gridAlign.pop_back(); // do not include last boundary for orientation avaraging
        }
        if(0.!=gridAlign.back() and abs(gridAlign.back())<6.2832)
            PrintOutput::warning(Str("ARE YOU SURE? Small alignement angle(s), input is in degrees, found: ")+gridAlign.back());
        for(double & g: gridAlign)g*=math::pi/180.;
    }
    if(needFiles!=_ampl.size())ABORT("need exactly 3 files for -polarAngle, exactly 2 fro -phiCEO");
}

double SpectrumLinCom::getValue(string Which, std::vector<string> Names, std::vector<string> Values)
{
    if(Values.size()>1 and Names.size()+1!=Values.size())
        ABORT(Str("")+Names+"- more names than input parameters: "+Values);

    string which=Which.substr(0,Which.find("["));
    int line=tools::string_to_int(tools::stringInBetween(Which,"[","]"));

    if(find(Names.begin(),Names.end(),which)==Names.end())
        ABORT(Str("not a valid paramter")+Which+", allowed values:"+Names);

    string linpStr="noLinp";
    if(folder::exists(Values[0]+"/linp")){
        ReadInputList linp(Values[0]+"/linp");
        linpStr=linp.readValue("Laser",which,"noLinp","retrieve for SpectrumLinCom",line,"dum1","");
    }
    double linpVal=DBL_MAX;
    if(linpStr!="noLinp")linpVal=Algebra(linpStr).val(0.).real();

    double val=linpVal;
    int pos=find(Names.begin(),Names.end(),which)-Names.begin();
    if (pos+1<Values.size()){
        val=tools::string_to_double(Values[pos+1]);
        if(linpVal!=DBL_MAX and abs(val-linpVal)>1.e-8)
            ABORT(Str("input value")+Which+"="+val+"inconsistent with value"+linpStr+"found on"+Values[0]+"/linp");
    }
    return val;
}

Coefficients SpectrumLinCom::amplPhiCEO(const vector<Coefficients> & Ampl,const vector<double> & PhiCEO,const vector<double> & polarAngle, double Phi, double Polar) const{
    if(Ampl.size()==2){
        if(polarAngle[0]!=polarAngle[1])
            DEVABORT(Str("cannot do -phiCEO when alignements's differ:")+polarAngle);

        if(PhiCEO[0]==PhiCEO[1]){
            Coefficients inter(Ampl[0]);
            PrintOutput::DEVwarning("HACK for comparing amplitudes: eqal PhiCEO, return difference");
            return inter.axpy(-1.,Ampl[1],1.);
        }
        vector<double> u(transPhi(PhiCEO[0],PhiCEO[1],Phi));

        Coefficients inter(Ampl[0]);
        return inter.axpy(u[1],Ampl[1],u[0]);
    }
    else if(Ampl.size()==3){
        if(PhiCEO[0]!=PhiCEO[1] or PhiCEO[0]!=PhiCEO[2])
            DEVABORT(Str("cannot do interpolate for alignement when phiCEO's differ:")+PhiCEO);

        vector<double> u(transPolar(polarAngle,Polar));

        Coefficients inter(Ampl[0]);
        inter.axpy(u[1],Ampl[1],u[0]);
        return inter.axpy(u[2],Ampl[2],1.);
    }
    else
        ABORT(Str("for now need either 2 or 3 files, found:")+amplFiles);
}

std::vector<double> SpectrumLinCom::transPhi(double Alfa, double Beta, double Gamma) const{
    if(Alfa==Beta){
        if(Gamma==Alfa)return {1.,0.};
        ABORT("equal input angle, cannot transform to different angle");
    }
    if(abs(fmod(Alfa-Beta,math::pi))<0.2*math::pi or abs(fmod(Alfa-Beta,math::pi))>0.8*math::pi )
        PrintOutput::warning(Sstr+"alfa beta ="+Alfa+Beta+"little angular difference (%pi):"
                             +fmod(Alfa-Beta,math::pi)+"- results may be inaccurate");

    Eigen::Matrix2d t;
    t<<     sin(Alfa),cos(Alfa),
            sin(Beta),cos(Beta);
    Eigen::Vector2d u;
    u<<sin(Gamma),cos(Gamma);
    u=t.inverse().transpose()*u;
    return vector<double>(u.data(),u.data()+u.size());
}

std::vector<double> SpectrumLinCom::transPolar(vector<double> Angle, double Gamma) const {
    if(Angle[0]==Gamma)return {1.,0.,0.};
    if(Angle.size()<3)ABORT("need three polar angles");
    if(Angle[0]==Angle[1] or Angle[1]==Angle[2] or Angle[2]==Angle[0])
        ABORT(Str("need three different polarAngle's for interpolation, found:")+Angle);

    Eigen::Matrix3d t;
    t<<     std::pow(cos(Angle[0]),2),cos(Angle[0])*sin(Angle[0]),std::pow(sin(Angle[0]),2),
            std::pow(cos(Angle[1]),2),cos(Angle[1])*sin(Angle[1]),std::pow(sin(Angle[1]),2),
            std::pow(cos(Angle[2]),2),cos(Angle[2])*sin(Angle[2]),std::pow(sin(Angle[2]),2);

    Eigen::Vector3d u;
    u<< std::pow(cos(Gamma),2),cos(Gamma)*sin(Gamma),std::pow(sin(Gamma),2);

    u=t.inverse().transpose()*u;
    return vector<double>(u.data(),u.data()+u.size());
}

std::string SpectrumLinCom::strFractionOfPi(double AngleRadians){
    vector<int> frac=tools::fractionSmallPrimes(AngleRadians/math::pi);
    std::string phiPi=tools::str(AngleRadians/math::pi,3)+"pi";
    if(frac[1]!=0){
        if(frac[0]==0)
            phiPi="0";
        else {
            phiPi="pi";
            if(frac[0]==-1)phiPi="-"+phiPi;
            else if(frac[0]!=1)phiPi =tools::str(frac[0])+phiPi;
            if(frac[1]!=1)phiPi+=tools::str(frac[1]);
        }
    }
    return phiPi;
}

void SpectrumLinCom::plotAverage(SpectrumPlot &Plt){

    Plt.setInfo(info());

    bool average=ReadInput::main.found("LinCom","averageAlignement","average");
    double scale=1.;
    if(average)scale=1./double(gridAlign.size());

    string tag ;
    string exten="c"+channel;
    string phiPi="all"+tools::str(gridPhiCEO.size());
    string aliPi="Aver"+tools::str(gridAlign.size());
    for(double phi: gridPhiCEO){
        std::vector<Coefficients> inter;
        if(_ampl.size()%2==0){
            for(int iEta=0;iEta<_ampl.size();iEta+=2){
                inter.push_back(amplPhiCEO(vector<Coefficients>(_ampl.begin()+iEta,    _ampl.begin()+iEta+2),
                                           vector<double>(    _phiCEO.begin()+iEta,  _phiCEO.begin()+iEta+2),
                                           vector<double>(  _theAlign.begin()+iEta,_theAlign.begin()+iEta+2),phi,_theAlign[iEta]));
            }
        }

        if(gridPhiCEO.size()==1)phiPi=strFractionOfPi(phi);
        tag=(phiPi.find("all")==0)?tools::str(phi):""; // if all in one file need tag

        for(double the: gridAlign){
            if(not average)
                aliPi=strFractionOfPi(the);
            if(inter.size()==0)
                Plt.basFromGrid(amplPhiCEO(_ampl,_phiCEO,_theAlign,phi,the),1); // single alignment
            else if(inter.size()==1)
                Plt.basFromGrid(inter[0],1); // only Eta
            else if(inter.size()==3)
                Plt.basFromGrid(amplPhiCEO(inter,{phi,phi,phi},{_theAlign[0],_theAlign[2],_theAlign[4]},phi,the),1); // interpolation for algnement
            // generate plot
            string dum;
            if(Plt.what().find("[")!=string::npos){
                // tilt the axis into laser axis
                vector<string> sAng=tools::splitString(tools::stringInBetween(Plt.what(),"[","]"),',');
                string def=Plt.what().substr(0,Plt.what().find("[")+1);
                if(Plt.what().find("cone")==0)
                    def+=sAng[0]+","+tools::str(Algebra::constantValue(sAng[1])+the)+sAng[2]+"]";
                else if(Plt.what().find("zone")==0)
                    def+=tools::str(Algebra::constantValue(sAng[0])+the)+","+tools::str(Algebra::constantValue(sAng[1])+the)+"]";
                Plt.addIntegratedPlot(def,Plt.ampl(),dum,scale,tag);
            }
            else
                Plt.addPlot(Plt.ampl(),dum,scale,tag);

            // write and clear
            if(not average and tag=="")
                Plt.write(exten+"_alig"+aliPi+"_CEO"+phiPi);

        }
        if(average and tag==""){
            Plt.write(exten+"_align"+aliPi+"_CEO"+phiPi);
        }
    }
    if(tag!="")
        Plt.write(exten+"_align"+aliPi+"_CEO"+phiPi);
}


vector<string> SpectrumLinCom::info() const {
    vector<string> inf({"linear combination from files"});
    for (int k=0;k<amplFiles.size();k++)
        inf.push_back(Str(amplFiles[k],"")
                      +" phiCEO="+tools::str(_phiCEO[k],3)
                      +" polarAngle="+tools::str(_theAlign[k],3)
                      );

    return inf;
}

void SpectrumLinCom::phaseLockAtMax(std::vector<Coefficients> &Ampl) const{
    if(not Ampl[0].anyData())DEVABORT("need contiguous storage on Ampl");
    // locate maximum amplitude element
    int kPos=std::max_element(Ampl[0].anyData(),Ampl[0].anyData()+Ampl[0].size(),
            [](std::complex<double>a,std::complex<double>b){return std::norm(a)<std::norm(b);})-Ampl[0].anyData();
    double arg0=std::arg(Ampl[0].anyData()[kPos]);
    for(Coefficients & c: Ampl){
        std::complex<double> phas=c.anyData()[kPos];
        if(ReadInput::main.flag("DEBUGphaseLock","lock relative phases")){
            Sstr+"MODIFY relative phase"+(std::arg(phas)-arg0)+Sendl;
            c.scale(std::abs(phas)/phas);
        }
        else
            Sstr+"relative phase"+(std::arg(phas)-arg0)+Sendl;
    }
}

void SpectrumLinCom::test(const std::vector<Coefficients> & Ampl,const std::vector<double> PhiCeo, const std::vector<double> TheAlign) const {
    if(Ampl.size()!=2)DEVABORT("for now only for 2");
    std::vector<Coefficients>altAmpl;
    std::vector<double>altPhi,altThe;
    altThe=TheAlign;
    for(int k=0;k<2;k++){
        altPhi.push_back(PhiCeo[k]+(k+1)*math::pi/11.*8.);
        altAmpl.push_back(amplPhiCEO(Ampl,PhiCeo,TheAlign,altPhi[k],altThe[k]));
    }

    for(int k=0;k<2;k++){
        Coefficients rec(amplPhiCEO(altAmpl,altPhi,altThe,PhiCeo[k],TheAlign[k]));
        Str mess;
        if((rec-=Ampl[k]).isZero(1.e-8*Ampl[k].maxCoeff()))mess+="OK";
        else                                               mess+="FAIL";
        Sstr+mess+"by"+(rec.norm()/Ampl[k].norm())+k+"at phi="+PhiCeo[k]+"from"+altPhi+Sendl;
    }

    SpectrumPlot plt("./",ReadInput::main);
    for(int k=0;k<8;k++){
        Coefficients inpC(*plt.amplFromFile("rabitt/000"+tools::str(k)+"/ampl"));
        Coefficients rec(amplPhiCEO(Ampl,PhiCeo,TheAlign,k*math::pi/8,altThe[k]));
        rec.cwiseDivide(inpC);
        //        Sstr+"ratio"+k+"\n"+inpC.str(102)+Sendl;
        //        Sstr+"ratio (polar)"+k+"\n"+rec.str(303)+Sendl;
        Sstr+"direct (polar)"+k+"\n"+inpC.str(303)+Sendl;
    }
    Coefficients rec(Ampl[0]);
    rec.cwiseDivide(Ampl[1]);
    Sstr+"ampls\n"+Ampl[0].str(3)+Sendl;
    Sstr+"ratio of ampls (polar)\n"+rec.str(303)+Sendl;

}



