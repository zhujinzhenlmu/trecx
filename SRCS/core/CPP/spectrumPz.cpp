#include "spectrumPz.h"

#include "polylagrange.h"
#include "basisGrid.h"
#include "coefficients.h"
#include "coefficientsPermute.h"
#include "index.h"
#include "plot.h"
#include "plotKind.h"
#include "readInput.h"

using namespace std;

SpectrumPz::SpectrumPz(const Index *InIdx)
    :toKEta(0)
{
    const Index* idxKEta;
    // for computing 2d kRn-Eta spectrum
    const PlotKind *p=PlotKind::definePlot("intPhi",InIdx->hierarchy());

    // need double of polynomial degree for exact interpolation
    for(std::string ax: p->axes())
        if(ax.find("Eta")==0)
            const_cast<PlotKind*>(p)->resize(ax,2*InIdx->axisIndex(ax)->basisAbstract()->size()-1);

    toKEta=new Plot(InIdx,p);
    idxKEta=toKEta->plot().idx();

    // prepare kz or kz1.kz2 index
    vector<const BasisAbstract*>bas;
    vector<string> axZ;
    vector<double> zGrid;
    for(string ax: {"kRn","kRn1","kRn2"}){
        if(idxKEta->axisIndex(ax)!=0){
            // kZ grid to be same as kRn on [-kMax,kMax]
            const BasisGrid* kRn=BasisGrid::factory(idxKEta->axisIndex(ax)->basisGrid());
            zGrid.clear();
            for(int k=kRn->size()-1;k>=0;k--)zGrid.push_back(-kRn->mesh()[k]);
            zGrid.push_back(0.);
            for(int k=0;k<kRn->size();k++)   zGrid.push_back( kRn->mesh()[k]);

            { // extend grid
                int scal;
                ReadInput::main.read("DEBUG","scale",scal,"1","increase z-grid",1,"DEBUGscal");
                std::vector<double> zg;
                for (int z=0;z<zGrid.size()-1;z++){
                    for(int k=0;k<scal;k++){
                        zg.push_back(zGrid[z]+k*(zGrid[z+1]-zGrid[z])/scal);
                    }
                }
                zg.push_back(zGrid.back());
                std::swap(zg,zGrid);
            }

            bas.push_back(BasisGrid::factory(zGrid));
            axZ.push_back(ax.replace(ax.begin(),ax.begin()+3,"kZ"));
        }
    }
    _zIndex=new Index(bas,axZ);
    vector<double> kGrid(BasisGrid::factory(idxKEta->basisAbstract())->mesh());
    double deltaK=kGrid[1]-kGrid[0];
    for(int k=1;k<kGrid.size();k++)
        if(abs(kGrid[k]-kGrid[k-1]-deltaK)>1.e-5*abs(deltaK))ABORT(Str("Pz-spectrum requires equidistant momentum grid, is\n")+kGrid);

    // quadrature grid and weight for the kRho-integration: kQ-point quadrature nInt intervals of [0,kRmax]
    int interSize,kQ,scal;
    ReadInput::main.read("DEBUG","kQ",kQ,"20","quadrature points",1,"DEBUGkQ");
    ReadInput::main.read("DEBUG","inter",interSize,"4","quadrature points",1,"DEBUGinter");


    if(kGrid.size()<interSize)ABORT(Sstr+"need at least "+interSize+"kRn-points for interpolation");
    int nInt=int(kGrid.size()/kQ);
    double sizInt=kGrid.back()/nInt;
    std::vector<double> qGrid,qWeig,g,w;
    OrthogonalLegendre leg;
    leg.quadratureGauss(kQ,g,w);
    for(int n=0;n<nInt;n++){
        for(int k=0;k<g.size();k++){
            qGrid.push_back((n+0.5*(g[k]+1.))*sizInt);
            qWeig.push_back(0.5*w[k]*sizInt);
        }
    }

    PolyLagrange<double> lagEta(idxKEta->descend()->basisGrid()->mesh());
    PolyLagrange<double> lagRad(std::vector<double>(kGrid.begin(),kGrid.begin()+interSize));
    interp.resize(zGrid.size());
    Sout+"zGrid"+zGrid.size()+zGrid[0]+zGrid.back()+Sendl;
    for(int z=0;z<interp.size();z++){
        for (int k=0;k<kGrid.size();k++)
            interp[z].push_back(std::vector<double>(lagEta.values(0.).size(),0.));
        for (int i=0;i<qGrid.size();i++){
            double rad=std::sqrt(std::pow(zGrid[z],2)+std::pow(qGrid[i],2));
            double eta=rad>1.e-10?zGrid[z]/rad:0.;

            if(rad<=kGrid.back()){
                // values of lagrange-polynomial at eta
                std::vector<double> etaInter=lagEta.values(eta);

                // values of lagrange-polynomials in kRn-neighborhood
                int kLow=max(0,min(int(kGrid.size())-interSize,int(rad/deltaK)-interSize/2));
                //                Sout+"kLow"+kGrid[kLow]+rad+qGrid[i]+zGrid[z]+Sendl;
                std::vector<double> radInterWeig=lagRad.values(rad-kGrid[kLow]);
                for(double & rVal: radInterWeig)rVal*=qWeig[i]*qGrid[i];

                // add to [eta] x [kLow,kLow+interSize-1] patch
                for(int k=0;k<interSize;k++)
                    for(int j=0;j<interp[z][kLow+k].size();j++)
                        interp[z][kLow+k][j]+=radInterWeig[k]*etaInter[j];
            }
        }

    }
}

Coefficients SpectrumPz::compute(const Coefficients *C) const {
    Coefficients specZ(_zIndex,0.);
    compute(&toKEta->generate(*C).plot(),&specZ,1.);
    // as result will be interpreted as "amplitude" for plotting, take square root
    for(int k=0;k<specZ.size();k++)specZ.anyData()[k]=std::sqrt(specZ.anyData()[k]);
    return specZ;
}
void SpectrumPz::compute(const Coefficients *SpecKEta, Coefficients *SpecZ, double Factor) const {
    if(SpecZ->isLeaf()){
        // add up contributions from each grid kRn grid point
        for(int z=0;z<SpecZ->size();z++)
            for(int k=0;k<SpecKEta->childSize();k++){
                for(int e=0;e<interp[z][k].size();e++)
                    SpecZ->orderedData()[z]+=Factor*interp[z][k][e]*SpecKEta->child(k)->orderedData()[e];
            }
    }
    for(int z=0;z<SpecZ->childSize();z++)
        for(int k=0;k<SpecKEta->childSize();k++)
            for(int e=0;e<interp[z][k].size();e++)
                compute(SpecKEta->child(k)->child(e),SpecZ->child(z),interp[z][k][e]);


}
