// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "indexSurface.h"
#include "basisGrid.h"

using namespace std;

IndexSurface::IndexSurface(const Index *From, std::vector<double> Radius, unsigned int NSurf)
{
    if(From->isLeaf())ABORT(From->root()->str()+"\n\nNo unbounded axis found in Index");

    Index::nodeCopy(From,false); // true copy, not view
    fromIndex.push_back(From);

    // second occurance of name indicates multi-piece axis
    const Index* fIndex=From->descend()->axisIndex(From->axisName());

    if(fIndex==0 or NSurf>0)
    {   // not desired surface - descend
        if(0!=fIndex)NSurf--;
        for(unsigned int k=0;k<From->childSize();k++)childAdd(new IndexSurface(From->child(k),Radius,NSurf));
    }

    else
    {
        // indicate surface level
        setAxisName("surf"+From->axisName());
        fromIndex.push_back(From);
        vector<unsigned int> elemN;
        for(unsigned int k=0;k<Radius.size();k++)
        {
            // locate element containing Radius
            const Index* elem=From->descend();
            unsigned int toFunc=fIndex->depth()-elem->depth();

            for(;elem!=0;elem=elem->rightSibling()){

                Index* elemFunc=elem->descend(toFunc); // descend to function level
                double lB=elemFunc->basisSet()->lowBound(),uB=elemFunc->basisSet()->upBound();
                // locate inside element; if on boundary, choose element nearer to 0
                if(     (lB>=0 and lB< Radius[k] and Radius[k]<=uB) or
                        (lB< 0 and lB<=Radius[k] and Radius[k]< uB)    )
                {
                    // attach copy of tree that contains surface
                    for(unsigned int l=childSize();l>0;l--)childErase(l-1);
                    elemN.push_back(elem->nSibling());
                    childAdd(new Index(0,From->child(elemN.back())));

                    // insert v/d at matching level
                    for(Index *vd=childBack()->descend(toFunc);vd!=0;vd=vd->nodeRight(),elemFunc=elemFunc->nodeRight()){
                        // erase function level
                        for(unsigned int l=vd->childSize();l>0;l--)vd->childErase(l-1);
                        // insert v/d level
                        // NOTE: we need all possible levels below to be equivalent
                        for(unsigned int l=0;l<2;l++){
                            vd->childAdd(new Index(0,elemFunc->child(0)));
                                vd->child(l)->setBasis(BasisGrid::factory(std::vector<double>(1,Radius[k])));
                            vd->child(l)->leafAdd();
                        }

                        vd->setBasis(BasisSet::getDummy(2));
                        vd->setAxisName("v/d");
                    }
                    break;
                }
            }
            if(elem==0)ABORT("surface outside all elements: "+tools::str(Radius[k]));
        }
        // basis on surf-level is a grid of element numbers
            std::vector<double> el;
            for(unsigned int k=0;k<elemN.size();k++)el.push_back(double(elemN[k]));
            setBasis(BasisGrid::factory(el));
    }

    if(From->depth()==0){
        // set floor to below v/d level (or to From floor, if deeper)
        resetFloor(max(From->firstFloor()->depth(),axisIndex("v/d")->depth()+1));
    }
}
