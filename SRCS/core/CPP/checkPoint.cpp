#include "checkPoint.h"
#include "wavefunction.h"
#include "readInput.h"
#include "printOutput.h"
#include "mpiWrapper.h"
#include "coefficients.h"
#include "discretization.h"
#include "dirent.h"
#include <cstdio>
#include <fstream>
#include <sstream>
#include <iomanip>

CheckPoint CheckPoint::main;

Wavefunction *CheckPoint::read(const Discretization *D) const {
    std::ifstream stream((ReadInput::main.output() + "wfCheckPoint").c_str(),
                         (std::ios_base::openmode) std::ios::beg | std::ios::binary);
    if (not stream.is_open()) return 0;
    Wavefunction *wf = new Wavefunction(0., new Coefficients(D->idx()));
    wf->read(stream, true);
    stream.close();
    return wf;
}

void CheckPoint::write(const Wavefunction &Wf) {
    if (initial) {
        last_write = MPI_Wtime();
        initial = false;
        return;
    }

    if ((MPI_Wtime() - last_write) < CHECKPOINT_EVERY_S and countsPerCheck == INT_MAX) return;
    if (iterateCounts % 100 != 0 and countsPerCheck == INT_MAX) return;
    if (iterateCounts % countsPerCheck != 0 and countsPerCheck != INT_MAX) return;
    sendReq.resize(MPIwrapper::Size());
    recvReq.resize(MPIwrapper::Size());
    if (countsPerCheck == INT_MAX)
        if (MPIwrapper::isMaster()) {
            countsPerCheck = iterateCounts;
            for (unsigned int i = 1; i < MPIwrapper::Size(); i++)
                MPIwrapper::Send(countsPerCheck, i, i);
        } else
            MPIwrapper::Recv(countsPerCheck, 0, MPIwrapper::Rank());
    if (MPIwrapper::isMaster()) {
        std::string filename = ReadInput::main.output() + "wfCheckPoint";
        for (int i = 0;; i++) {
            std::ostringstream filenameBu;
            filenameBu << filename << ".";
            filenameBu << std::setfill('0') << std::setw(5) << i;
            if (!std::ifstream(filenameBu.str()).good()) {
                if (rename(filename.c_str(), filenameBu.str().c_str())) {
                    PrintOutput::warning("Could not backup wfCheckPoint: " + filenameBu.str());
                }
                break;
            }
        }
        std::ofstream stream(filename.c_str(), (std::ios_base::openmode) std::ios::beg | std::ios::binary);
        Wf.write(stream, true);
        stream.close();
    }


    last_write = MPI_Wtime();
}

void CheckPoint::writeTime(double time) {
    if (MPIwrapper::isMaster()) {
        std::string filename = ReadInput::main.output() + "surfaceTime";
        std::ofstream stream(filename.c_str(), (std::ios_base::openmode) std::ios::beg | std::ios::binary);
        tools::write(stream, time);
        stream.close();
    }
}

double CheckPoint::readTime() {
    std::ifstream stream((ReadInput::main.output() + "surfaceTime").c_str(),
                         (std::ios_base::openmode) std::ios::beg | std::ios::binary);

    if (not stream.is_open()) return -DBL_MAX;
    double time;
    tools::read(stream, time);
    return time;
}

bool CheckPoint::startWrite() {
    iterateCounts += 1;
    if (initial) {
        last_write = MPI_Wtime();
        initial = false;
        return false;
    }
    // before defining countsPerChek
    if ((MPI_Wtime() - last_write) < CHECKPOINT_EVERY_S and countsPerCheck == INT_MAX) return false;
    // after defining countsPerCheck
    if (iterateCounts % countsPerCheck != 0 and  countsPerCheck != INT_MAX) return false;
    return true;

}
