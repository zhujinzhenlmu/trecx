// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "inverseDvr.h"

#include "mpiWrapper.h"
#include "readInput.h"
#include "operatorTree.h"
#include "coefficients.h"
#include "coefficientsLocal.h"
#include "index.h"
#include "str.h"
#include "timer.h"
using namespace std;

InverseDVR::InverseDVR(const Index *Idx)
    :Inverse("Inv("+Idx->hierarchy()+")",Idx,Idx),diagonalLocal(0)
{
    // extract diagonal into Coefficients structure, force check
    Coefficients diag(Idx);
    diag.reset(iIndex);
    diag=Idx->localOverlap()->diagonal(true);

    // make continuous
    diag.makeContinuous();

    // get inverse
    _diagonal=new Coefficients(diag.idx(),1.);
    const_cast<Coefficients*>(_diagonal)->treeOrderStorage();
    const_cast<Coefficients*>(_diagonal)->cwiseDivide(diag);

    // create hierachy until first continuity
    if(Idx->continuity()==Index::npos)
        for(int k=0;k<_diagonal->childSize();k++)
            childAdd(new InverseDVR(_diagonal->child(k)));
}

InverseDVR::InverseDVR(const Coefficients *Diagonal)
    :Inverse("Inv("+Diagonal->idx()->hierarchy()+")",Diagonal->idx(),Diagonal->idx()),
      _diagonal(Diagonal),diagonalLocal(0)
{
    // descend hierarchy until first continuity level
    if(Diagonal->parent()==0 or Diagonal->parent()->idx()->continuity()==Index::npos)
        for(int k=0;k<Diagonal->childSize();k++)
            Diagonal->idx()->child(k)->setInverseOverlap(new InverseDVR(Diagonal->child(k)));
}

void InverseDVR::apply0(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    if(A!=1.)ABORT("A!=1 not implemented");
    if(B!=0.)*tempLHS()=Y;
    Y=Vec;
    Y.cwiseMultiply(*_diagonal);
    if(B!=0.)Y.axpy(B,tempLHS());
}

void InverseDVR::parallelSetup() const{
    const_cast<InverseDVR*>(this)->diagonalLocal=CoefficientsLocal::view(const_cast<Coefficients*>(_diagonal));
}

void InverseDVR::apply0(std::complex<double> A, const CoefficientsLocal &Vec, std::complex<double> B, CoefficientsLocal &Y) const{
    if(A!=1.)ABORT("A!=1 not implemented");
    if(B!=0.)ABORT("B!=0 not implemented");
    if(diagonalLocal==0)ABORT("call parallelSetup() before use with CoefficientsLocal");
    Y=Vec;
    Y.cwiseMultiply(*diagonalLocal);
}
