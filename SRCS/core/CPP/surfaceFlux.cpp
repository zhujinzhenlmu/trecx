// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "surfaceFlux.h"
#include "readInput.h"
#include "printOutput.h"
#include "discretizationSurface.h"
#include "newtonInterpolator.h"
#include "timer.h"
#include "str.h"
// for regions:
#include "discretizationtsurffspectra.h"
#include "debugInfo.h"

using namespace std;

SurfaceFlux::SurfaceFlux(ReadInput &inp, DiscretizationSurface *SurfD, std::string Region, int NSurf):
    Wavefunction(SurfD),_surfaceDisc(SurfD),_timeBegin(DBL_MAX),_interPol(0),_eps(1.e-12)
{  

    inp.read("SurfaceFlux", "interpolSize", _interSize,"4", "number of wf's for interpolation",1,"interP");
    inp.read("SurfaceFlux", "readSize", readSize,"500", "size of surface buffer",1,"readBuf");

    std::vector<std::string>infCoorList=DiscretizationTsurffSpectra::splitRegion(Region);

    string pre="";

    if(infCoorList.size()>1){
        std::vector<std::string>regions, names(infCoorList.size()-1);
        DiscretizationTsurffSpectra::getRegions(0,0,infCoorList.size()-1, infCoorList, regions, names);
        pre = "S_"+regions[NSurf]+"/";
    }

    // Open again
    string surfValBin=inp.outputTopDir()+SurfD->mapFromParent()->name;  // the surface files use this convention, also in main_tRecX
    _surfaceStream.open( (surfValBin).c_str(), ios::in|ios::binary);
    if(!_surfaceStream.is_open()) {
        vector<string> pars = tools::splitString(static_cast<DiscretizationSurface* >(SurfD)->mapFromParent()->name,'<');
        vector<string> lev=tools::splitString(pars[0],'.');
        string pre="";
        if(lev[lev.size()-2].find("k")==0 and lev.back().find("ValDer")==0)pre="S0_";
        if(lev[lev.size()-2].find("ValDer")==0 and lev.back().find("k")==0)pre="S1_";

        // Open again
        surfValBin=inp.outputTopDir()+pre+DiscretizationSurface::prefix+pars[0];  // the surface files use this convention, also in main_tRecX
        _surfaceStream.open( (surfValBin).c_str(), ios::in|ios::binary);
    }
    if(not _surfaceStream.is_open()){//HACK - guess file name from pars[0]
        PrintOutput::warning("failed to open surface file: "+surfValBin+" - no tSurff spectra calculated");
        _timeBegin=DBL_MAX;
    }
    else {
        PrintOutput::DEVmessage("reading from surface file "+surfValBin);
        _surfaceStream.seekg(0, _surfaceStream.beg);
        // read Index for Surface
        Index surfI(_surfaceStream,true);
        Wavefunction temp(&surfI);
        temp.read(_surfaceStream,false);
        _timeBegin=temp.time;
        _surfaceStream.seekg(0, _surfaceStream.beg); //rewind
    }
    _interPol = new NewtonInterpolatorWF(SurfD, _interSize);
    time =_timeBegin;
}

SurfaceFlux::SurfaceFlux(const std::string SurfaceFile, int InterSize, int ReadSize)
    :_interSize(InterSize),readSize(ReadSize),_surfaceDisc(0),_eps(1.e-12)
{
    _surfaceStream.open( (SurfaceFile).c_str(), ios::in|ios::binary);
    if(not _surfaceStream.is_open()){//HACK - guess file name from pars[0]
        PrintOutput::warning("failed to open surface file: "+SurfaceFile+" - no tSurff spectra calculated");
        _timeBegin=DBL_MAX;
    }
    else {
        PrintOutput::DEVmessage("reading from surface file "+SurfaceFile);
        // get initial time
        _surfaceStream.seekg(0, _surfaceStream.beg);
        _idx.reset(new Index(_surfaceStream));
        coefs=new Coefficients(_idx.get());
        read(_surfaceStream,false);
        _timeBegin=time;
    }
    _interPol = new NewtonInterpolatorWF(coefs->idx(),_interSize);
}

SurfaceFlux::~SurfaceFlux()
{
    if (_surfaceStream.is_open()) {_surfaceStream.close();}
    if(_interPol!=0) {delete _interPol; _interPol=0;}
    for (int k=0;k<_inputBuffer.size();k++)delete _inputBuffer[k];
}

TIMER(read,)
TIMER(interpolate,)
TIMER(interCoeff,)
bool SurfaceFlux::update(double Time, bool Derivative){

    STARTDEBUG(read);
    while(_inputBuffer.size()<_interSize or
          Time-_eps>_inputBuffer[_inputBuffer.size()-_interSize/2-1]->time)
    {
        // update buffer (here, we may read only subset)
        if(_inputBuffer.size()==0){
            _surfaceStream.seekg(0,_surfaceStream.beg);
                Index surfI(_surfaceStream);
                if(not idx()->treeEquivalent(&surfI))
                    ABORT("indices do not match\n"+Index::failureCompatible+"\n"+surfI.str()+"\ncoef\n"+idx()->str()+"\nindices do not match");
        }

        // exempt initial buffer setup from time-critical
        bool old=Coefficients::timeCritical;
        Coefficients::timeCritical=false;
        if(_inputBuffer.size()<readSize)_inputBuffer.push_back(new Wavefunction(idx(),0.));
        else                            _inputBuffer.push_back(_inputBuffer[0]);

        Coefficients::timeCritical=old; // restore

        _inputBuffer.back()->read(_surfaceStream,false);

        // no more wavefunctions, remove invalid buffer
        if(not _surfaceStream.good()){
            _inputBuffer.pop_back();
            break;
        }

        // maximal buffer length reached - remove
        if(_inputBuffer.size()>readSize)_inputBuffer.erase(_inputBuffer.begin());

        _eps=(abs(_inputBuffer.back()->time)+abs(_inputBuffer.front()->time))*1.e-12;
    }
    STOPDEBUG(read);

    if(Time+_eps<_inputBuffer.front()->time)
        ABORT("Time<lowest time, larger _inputBuffer may be needed: "+tools::str(Time)+"<"+tools::str(_inputBuffer.front()->time));
    if(Time-_eps>_inputBuffer.back()->time)
        ABORT("Time > largest time on file: "+tools::str(Time)+" > "
              +tools::str(_inputBuffer.back()->time)+": propagation may have been truncated");


    // if Time not in interpolation, get new interpolation
    if(not _interPol->inInterval(Time)){
        STARTDEBUG(interCoeff);
        // locate time in _inputBuffer
        int iBeg=0,iEnd=_inputBuffer.size();
        while(iBeg+1<iEnd){
            if(_inputBuffer[(iBeg+iEnd)/2]->time<Time)iBeg=(iBeg+iEnd)/2;
            else                                      iEnd=(iBeg+iEnd)/2;
        }
        iBeg=min(max(0,iBeg-_interSize/2),int(_inputBuffer.size()-_interSize));

        _interPol->computePolynomialCoefficients(vector<Wavefunction*>(_inputBuffer.begin()+iBeg,_inputBuffer.begin()+iBeg+_interSize));
        STOPDEBUG(interCoeff);
    }

    STARTDEBUG(interpolate);
    if(Derivative)_interPol->timeDerivative(Time, this);
    else          _interPol->getInterpolatedWF(Time, this);
    STOPDEBUG(interpolate);

    return true;

}
double SurfaceFlux::FluxBufferBeginTime() const{
    return _timeBegin;
}
const Index* SurfaceFlux::idx() const {return coefs->idx();}
