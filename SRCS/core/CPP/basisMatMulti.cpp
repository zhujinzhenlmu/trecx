// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "basisMatMulti.h"

#include "tools.h"
#include "algebra.h"
#include "basisAbstract.h"
#include "basisDvr.h"

#include <vector>
using namespace std;

#include "basisMat1D.h"

/// Mat = <IBas0|<IBas1|...<IBasN| Pot |JBasN>...|JBas1>|JBas0>
/// <br> Pot=factor_string<{}><{}>...<algebraic expression of coordinate names> (we might replace this with <{algebraic expression}>)
/// <br>  factor_string must be convertible to Algebra
/// <br> example:   Op=0.5<{}><{}><X*X+Y*Y+Z*Z> for the HO potential in cartesian coordinates
void BasisMatMulti::_construct(std::string Op, const Index * IIndex, const Index* JIndex){

     if(Op.find("<{}>")==string::npos)return; // not a multi-dimensional multiplication

    // do the basic checks here...

    string fac=Op.substr(0,Op.find("<"));
    if(fac=="")fac="1";
    Algebra facAlg(fac);
    if(not facAlg.isAlgebra())return;

    // recursively construct values
    std::vector<complex<double> >diag;
    construct(Op.substr(Op.find("<")),IIndex,JIndex,diag);

    Eigen::Map<Eigen::VectorXcd> d(diag.data(),diag.size());
    d*=facAlg.val(0.);
    _mat=d.asDiagonal();
}

void BasisMatMulti::construct(std::string Op, const Index * IIndex, const Index* JIndex, std::vector<std::complex<double> > & Diag){

    // for speed, these checks should be moved to top level
    if(IIndex->axisName()!=JIndex->axisName())
        ABORT(Str("multiplicative operators only between equal coordinates, found:")+IIndex->axisName()+JIndex->axisName()+"for"+Op);
    if(not IIndex->basisAbstract()->isDVR())ABORT("only for DVR basis, found "+IIndex->basisAbstract()->str());
    if(not (IIndex->basisAbstract()->operator==(*JIndex->basisAbstract())))
        ABORT(Str("multiplicative operators only for identical basis, found:")+IIndex->basisAbstract()->str()+JIndex->basisAbstract()->str());
    const BasisDVR* dvr=dynamic_cast<const BasisDVR*>(JIndex->basisAbstract());
    if(dvr==0)ABORT("cast to dvr failed");

    if(Op.find("<{}>")==0){
        // complex scaling for weight

        // get weights and support points for basis functions
        BasisMat1D one("<1>",IIndex->basisAbstract(),JIndex->basisAbstract());
        BasisMat1D qqq("<Q>",IIndex->basisAbstract(),JIndex->basisAbstract());
        if(one.isEmpty() or qqq.isEmpty())ABORT(Str("fail")+one.isEmpty()+qqq.isEmpty());

        for(int k=0;k<IIndex->basisAbstract()->size();k++)
        {
            std::complex<double> weigJacValsq=one.mat()(k,k);
            complex<double> q=(qqq.mat()(k,k)/weigJacValsq);

            // substitute coordinate with q value
            string op(Op.substr(Op.find("<{}>")+4));
            size_t pos;
            // Algebra accepts complex constant only as (constR+i*(constI)):
            while(string::npos!=(pos=op.find(IIndex->axisName())))
                op.replace(pos,IIndex->axisName().length(),"("+tools::str(q.real(),15)+"+i*("+tools::str(q.imag(),15)+"))");

            int newDiag=Diag.size(); // begin of new portion
            construct(op,IIndex->child(k),JIndex->child(k),Diag);
            for(int k=newDiag;k<Diag.size();k++)Diag[k]*=weigJacValsq;
        }
    }
    else{
        // last factor should hold the algebraic expression
        size_t pos;
        while(string::npos!=(pos=Op.find(IIndex->axisName())))Op.replace(pos,IIndex->axisName().length(),"Q");
        BasisMat1D bm(Op,IIndex->basisAbstract(),JIndex->basisAbstract());
        if(bm.isEmpty())ABORT("failed to evaluate: "+Op);
        for(int k=0;k<bm.mat().rows();k++)Diag.push_back(bm.mat()(k,k));
    }
}


//const UseMatrix BasisMatMulti::useMat() const {
//    return UseMatrix::UseMap(const_cast<BasisMatMulti*>(this)->_mat.data(),_mat.rows(),_mat.cols());
//}

//const std::vector<const Eigen::MatrixXcd*> BasisMatMulti::mats() const {
//    return std::vector<const Eigen::MatrixXcd*>(1,&_mat);
//}
