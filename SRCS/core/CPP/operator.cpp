// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "operator.h"
//#include <boost/lexical_cast.hpp>
#include "parallel.h"
#include "str.h"

//resolve forward declarations
#include "wavefunction.h"
#include "coefficients.h"
#include "coefficientsFloor.h"
#include "discretization.h"
#include "discretizationHybrid.h"
#include "wavefunction.h"
#include "index.h"
#include "parameters.h"
#include "operatorData.h"
#include "operatorSingle.h"
#include "operatorFloor.h"
#include "operatorTensor.h"
#include "operatorAddvector.h"
#include "printOutput.h"
#include "readInput.h"
#include "operatorFloorEE.h"
#include "operatorTree.h"
#include "basisMat.h"

#include "timer.h"
#include "tRecXchecks.h"
#include "eigenSolverAbstract.h"

using namespace std;
using namespace tools;
#include "eigenNames.h"


static unsigned int currentFile=0;       ///< will be set to the class static fileCount
static int cccount=0;

bool Operator::useOperatorFloor=true;
bool Operator::fuseOp=true;
bool Operator::useTensor=false;
bool Operator::flat=true;

// File-local variables
static double skipped=0.,passed=0.;       ///< count the skipped/passed multiplies
static string previousSuppressDefinition="NONE";

Operator::Operator():OperatorAbstract("NoName",0,0),_parent(0),ownsData(false),oFloor(0),tensor(0),preTensor(0),hackTree(0){}
Operator::Operator(const Operator &Other)
    :OperatorAbstract(Other.name,iIndex,jIndex),
      definition(Other.definition),_parent(0),ownsData(true),oFloor(0),tensor(0),preTensor(0),hackTree(Other.hackTree)
{
    ownsData=true;
    if(Other.oFloor!=0)ABORT("sorry not implemented");
    if(Other.tensor!=0 or preTensor!=0)ABORT("for now cannot copy tensor'd Operators");
    for(int k=0;k<Other.childSize();k++){
        O.push_back(new Operator(*Other.O[k]));
        O.back()->_parent=this;
    }

}


Operator::Operator(const Index* IIndex, const Index* JIndex, OperatorSingle* OSing, string Name)
    :OperatorAbstract(Name,IIndex,JIndex),_parent(0),ownsData(true),oFloor(0),tensor(0),preTensor(0),hackTree(0)

{
    if(OSing!=0){
        definition=OSing->definition;
        o.push_back(OSing);
    }
}

string Operator::eigenMethod="auto";          ///< allow arnoldi eigensolver (not stable at times)
complex<double> Operator::Arp::shift=complex<double>(100.);
void Operator::readControls(ReadInput &Inp){
    EigenSolverAbstract::readControls(Inp);
    Operator::eigenMethod=EigenSolverAbstract::method;
    double inShift;
     Inp.read("Eigensolver","shift",inShift,"100","shift Arpack eigensolver to avoid zero's arising from continuity projections if positive eigenvalues are searched for");
    Operator::Arp::shift=inShift;
    ReadInput::main.read("_EXPERT_","newFloor",useOperatorFloor,"false","admit use of tensor products",0,"newFloor");
    ReadInput::main.read("_EXPERT_","suppressTensor",Operator::useTensor,"false","admit use of tensor products",0,"noTensor");
    Operator::useTensor=not Operator::useTensor;

    Inp.read("_EXPERT_","fuseOperator",fuseOp,"true","fuse pieces of the operator as much as possible",1,"fuse");
    Operator::useTensor=useTensor and (not fuseOp);

    Inp.read("_EXPERT_","flatDer",Operator::flat,"true","absorb the inverse into new Floor operators",1,"flatDer");
    if(flat){
        //        OperatorFloor::absorbInverse=true;
        useOperatorFloor=true;
        useTensor=false;
    }

    PrintOutput::DEVmessage("Expert controls: fuse="+tools::str(fuseOp)
                            +", useTensor="+tools::str(useTensor)
                            +", newFloor="+tools::str(useOperatorFloor)
                            +", flatDerivative="+tools::str(Operator::flat)
                            //                         +", absorbInverse="+tools::str(OperatorFloor::absorbInverse)
                            );
}

// create a map operator between two standard index hierarchies
Operator::Operator(const Index *I, const Index *J, string DerAxis)
    :OperatorAbstract("Map",I,J),_parent(0),ownsData(true),definition("I<-J"),oFloor(0),tensor(0),preTensor(0),hackTree(0)
{
    if(I->axisName()!=J->axisName())ABORT("constructor only for maps between equal axisName");

    string mapKind="map";
    if(DerAxis==I->axisName() and not I->basisSet()->isIndex())mapKind="mapDer";
    if(I->axisName()!=J->axisName())ABORT("cannot map between different axes: "+I->axisName()+" <-- "+J->axisName());
    if(DerAxis!=""){
        name+="_D("+DerAxis+")";
    }

    Tensor* t=0;
    tensor=0;
    preTensor=0;
    if(not I->hasFloor()){
        if(I->subEquivalent()){
            tensor=new Tensor("<"+mapKind+">",I,J);
            if(tensor->mat.isIdentity(1.e-12)){
                delete tensor;
                tensor=0;
            }
            t=tensor;
        } else if (J->subEquivalent()){
            preTensor=new Tensor("<"+mapKind+">",I,J);
            if(preTensor->mat.isIdentity(1.e-12)){
                delete preTensor;
                preTensor=0;
            }
            t=preTensor;
        } else {
            UseMatrix m;
            BasisMat::get(mapKind,*I->basisSet(),*J->basisSet(),m);
            if(not m.isIdentity(1.e-12)){
                cout<<m.str("map",2)<<endl;
                cout<<"I:\n"<<I->str()<<endl;
                cout<<"J:\n"<<J->str()<<endl;
                cout<<"equiv: "<<I->subEquivalent("I")<<J->subEquivalent("J")<<endl;
                ABORT("not tensor product");
            }
        }
    }

    // set up map between sub-blocks
    const Index * M=I;
    if(t!=0)M=t->I;

    if(not M->hasFloor()){
        for(unsigned int m=0;m<min(M->childSize(),J->childSize());m++){
            if(t==0 and M->basisAbstract()==J->basisAbstract()){
                O.push_back(new Operator(M->child(m),J->child(m),DerAxis));
            }
            else if(t==tensor){
                if(M->child(m)->basisSet()->isGrid())
                    O.push_back(new Operator(M->child(m),match(M->child(m),J),DerAxis));
                else
                    O.push_back(new Operator(match(J->child(m),M),J->child(m),DerAxis));

            } else if (t==preTensor) {
                if(I->child(m)->basisSet()->isGrid())
                    O.push_back(new Operator(I->child(m),match(I->child(m),M),DerAxis));
                else
                    O.push_back(new Operator(match(M->child(m),I),M->child(m),DerAxis));
            }
            if(O.back()->isZero()){
                delete O.back();
                O.pop_back();
            }
        }
    }

    // set up map between floors (if any)
    if(M->hasFloor() ^ J->hasFloor())ABORT("index hierarchies must have the same depth");
    if(M->hasFloor() and M->sizeStored()>0 and J->sizeStored()>0){

        vector<UseMatrix> mat(M->basProd().size());
        for(unsigned int n=0;n<M->basProd().size();n++){
            BasisMat::get(mapKind,*M->basProd()[n],*J->basProd()[n],mat[n]);
            if(mat[n].isZero(1.e-12)){
                mat.clear();
                break;
            }
        }
        vector<const UseMatrix*>pMat;
        if(mat.size()>0){
            for(unsigned int k=mat.size();0<k;k--)pMat.push_back(&mat[k-1]);
            oFloor=OperatorFloor::factory(pMat,mapKind);
            if(oFloor==0){
                ABORT("setup of oFloor failed: "+mapKind);
            }
        }
    }
}


const Index* Operator::match(const Index *M, const Index *Parent){
    if(M->parent()->childSize()==Parent->childSize())
        return Parent->child(M->nSibling());

    if(not (M->parent()->basisSet()->isIndex() and Parent->basisSet()->isIndex()))
        ABORT("match() only for index-type functions\n"+M->parent()->str()+"\n"+Parent->str());

    unsigned int mcont=M->parent()->continuity(),icont=Parent->continuity();
    if(mcont==Index::npos or icont==Index::npos)ABORT("non-trivial match only on continuity level");
    icont-=Parent->depth()+1;
    const BasisSet* bM=M->descend(mcont-M->depth())->basisSet();
    for(unsigned int k=0;k<Parent->childSize();k++){
        const BasisSet* bParent=Parent->child(k)->descend(icont)->basisSet();
        if(bM->hasOverlap(*bParent))return Parent->child(k);
    }
    cout<<"bM "<<bM->str()<<endl;
    cout<<"par "<<Parent->child(0)->basisSet()->str()<<endl;
    ABORT("no match found\n"+M->parent()->str()+"\n"+Parent->str());
    return 0;
}

Operator::Operator(const Operator* Op, Index *I, Index *J)
    :OperatorAbstract(Op->name+"_subBlock",I,J),_parent(0),ownsData(false),definition("subBlock"),oFloor(0),preTensor(0),hackTree(0)
{
    if(Op->tensor!=0)ABORT("sub-block in presence of a tensor factor not implemented");
    tensor=0;
    for(unsigned int k=0;k<Op->O.size();k++){
        if(Op->O[k]->iIndex==I->parent() and Op->O[k]->jIndex==J->parent()){
            Operator Sub(Op->O[k],I,J);
            for (unsigned int l=0;l<Sub.O.size();l++){
                if(not Sub.O[l]->isZero(1.e-12))O.push_back(Sub.O[l]);
            }
        }
        else if(Op->O[k]->iIndex==I and Op->O[k]->jIndex==J and not Op->O[k]->isZero(1.e-12)){
            O.push_back(Op->O[k]);
        }
    }
    if(tensor!=0 and preTensor!=0)ABORT("can only have pre- OR post-tensor");
}


TIMERRECURSIVE(operator,)

bool Operator::constructHybrid(const string &Name, const string &Def, const Discretization *IDisc, const Discretization *JDisc){
    if(IDisc->name.find("(+)")==string::npos)return false;

    // check for hybrid discretization
    const DiscretizationHybrid* iHyb=dynamic_cast<const DiscretizationHybrid*>(IDisc);
    const DiscretizationHybrid* jHyb=dynamic_cast<const DiscretizationHybrid*>(JDisc);
    if(iHyb==0 and jHyb==0)return false;
    if(iHyb==0 or jHyb==0)ABORT("cannot mix hybrid with standard discretization");

    for(int i=0;i<iHyb->comp.size();i++)
        for(int j=0;j<jHyb->comp.size();j++){
            // extract blocks starting with <i,j> from definition
            string defij=OperatorData::extractBlock(Def,i,j);

            if(defij!=""){
                cout<<"defij "<<defij<<" "<<iHyb->comp[i]->idx()->hierarchy()<<" "<<iHyb->comp[j]->idx()->hierarchy()<<endl;
                // convert defij to standard form
                defij=OperatorData::expandStandard(defij,iHyb->comp[j]);

                // construct and attach to top level operator
                O.push_back(new Operator(Name,defij,iHyb->comp[i],iHyb->comp[j],iHyb->comp[i]->idx(),iHyb->comp[j]->idx(),1.,1));
            }
        }
}

Operator::Operator(const string &Name, const string &Def, const Discretization *IDisc, const Discretization *JDisc, const Index *IIndex, const Index *JIndex,
                   complex<double> Multiplier, unsigned int Level, std::complex<double> *TFac)
    :OperatorAbstract(Name,IIndex,JIndex),_parent(0),ownsData(true),definition(Def),oFloor(0),hackTree(0){


    if(iIndex==0)iIndex=IDisc->idx();
    if(jIndex==0)jIndex=JDisc->idx();

    tensor=0;
    preTensor=0;
    if(abs(Multiplier)<1.e-14)PrintOutput::warning("found near-zero multiplier in setup of operator "+definition);

    // detect hybrid discretization
    if(Level==0 and constructHybrid(Name,Def,IDisc,JDisc))return;

    // expand standard operators
    string def(OperatorData::expandStandard(definition,iIndex->hierarchy()));
    if(def.find("<<")!=string::npos)ABORT("expansion of definition failed");
    if(definition!=def and iIndex!=jIndex)ABORT("cannot expand definition between two different index sets: "+definition);
    definition=def;


    if(iIndex->isRoot() and jIndex->isRoot() and name!="Commutator" and name!="surface2smoothSpecDisc"){
        vector<string> terms=OperatorData::terms(definition);
        if(iIndex->hierarchy().find("spec")!=string::npos or jIndex->hierarchy().find("spec")!=string::npos)
            OperatorDefinition::dropTerms(terms,iIndex,jIndex);      // For tsurff: construct operator strings from base disc strings
        definition="";
        for(unsigned int k=0;k<terms.size();k++) definition+=terms[k];
    }
    construct(0,0,Multiplier,TFac);

    if(tensor!=0 and preTensor!=0)ABORT("can only have pre- OR post-tensor");

    // convert only after possible fuse()
    if(useOperatorFloor and Level==0){
        Parameters::update(0.);
        convert();

        //        Parallel::operatorBcast(this);
        purge();
    }

}


/// OBSOLESCENT: rather unclean, replace calls to it by excpliclty constructed operator definition
Operator::Operator(const string Name, const string TensorDef, Index *IIndex, Index *JIndex, vector<Operator*> Op, vector<OperatorSingle*> Os)
    :OperatorAbstract(Name,IIndex,JIndex),_parent(0),ownsData(false),oFloor(0),hackTree(0){
    // check for consistency
    if(Op.size()>0 and (Op[0]->iIndex->parent()!=iIndex or Op[0]->jIndex->parent()!=jIndex))
        ABORT("suboperators incorrectly placed in hierarchy");
    if(Os.size()>0 and (Os[0]->iIndex!=iIndex or Os[0]->jIndex!=jIndex)){
        ABORT("floor suboperators incorrectly placed in hierarchy");
    }

    if(Op.size()>0)definition=Op[0]->definition+TensorDef;
    if(Os.size()>0)definition=TensorDef;
    O=Op;
    o=Os;
    preTensor=0;
    tensor=0;
    if(not iIndex->hasFloor()){
        // try tensor
        tensor=new Tensor(TensorDef,iIndex,jIndex);

        if(not tensor->Y==0
                or tensor->mat.isIdentity(1.e-12)
                or not iIndex->subEquivalent()
                or not jIndex->subEquivalent()){
            if(not tensor->mat.isDiagonal(1.e-12))PrintOutput::message("possible tensor use suppressed by input "+definition);
            delete tensor;
            tensor=0;
        }
    }
    if(Name=="invS0" and tensor!=0){
        tensor->mat.print("overlap");
        ABORT("for now, no true tensors allowed in overlap");
    }
}

void Operator::construct(const Discretization* IDisc, const Discretization* JDisc, complex<double> Multiplier, complex<double>* TFac) {
    if(name.find("volkov")!=string::npos)cout<<"name="+name+"   "+definition<<endl;
    // remove leftover quotation marks
    definition=tools::stringInBetween(definition,"'","'");


    // expand standard operators

    string def(definition);
    if(definition.find("<<")!=string::npos){
        if(iIndex==0)ABORT("cannot expand definition without Discretization: "+definition);
        def=OperatorData::expandStandard(definition,iIndex->hierarchy());
    }
    if(definition!=def and iIndex!=jIndex)ABORT("cannot expand definition between two different discretizations: "+definition);
    definition=def;


    vector<string> terms=OperatorData::terms(definition);
    vector<size_t> leftSub, rightSub;
    OperatorData::substitutionSpec spec;
    string termParameter;
    if(terms.size()>1){
        for(unsigned int n=0;n<terms.size();n++){
            if(Multiplier==0.)continue;
            O.push_back(new Operator(name,terms[n],IDisc,JDisc,iIndex,jIndex,Multiplier,1,TFac));

            if(O.back()->isZero(1.e-12)){
                delete O.back();
                O.pop_back();
            }
        }
        fuse(O); // fuse blocks with equal indices
        split(O);
        goto Return;
    }
    else {
        definition=terms[0];
        // on top level of a single term, modify its definition (HACK for hybrid)
        if(iIndex->isRoot() and jIndex->isRoot() or iIndex->parent()->axisName()=="Hybrid"){
            if(name!="Commutator" and name!="surface2smoothSpecDisc")    // HACK as modifyDefinition doesn't work always
                OperatorData::modifyDefinition(definition,iIndex,jIndex);
            if(Operator::useTensor and fuseOp)
                PrintOutput::warning("simultaneous use of fuse() and true tensor may lead to incorrect results: "+definition,1);
        }

        //HACK this operator should go to a mapTo/mapFrom
        if(name=="surface2smoothSpecDisc"){
            OperatorData::substitutionIndices(definition, spec,iIndex->axisName(),jIndex->axisName(),leftSub, rightSub);
        }

        // get factor, keep separate if time-dependent
        termParameter=OperatorData::parameter(definition);
        complex<double>* tFac=Parameters::pointer(termParameter);
        if(not Parameters::isFunction(termParameter)){
            Multiplier*=*tFac;
            if(Multiplier==0.)PrintOutput::warning("zero multiplier?");
        } else {
            if(TFac!=0)ABORT("product of time-dependent parameters not allowed");
            TFac=tFac;
        }
    }
    if(iIndex->hasFloor() and jIndex->hasFloor() and Multiplier!=0.) {
        o.clear();
        O.clear();


        if(OperatorData::isStandard(definition)){
            // standard setup can be expensive, on local host and broadcast later
            oFloor=Parallel::notOnHost; // forces non-zero interpretation of Op
//            if(Parallel::operatorAssign(this))
            {
                oFloor=0;
                vector<string> terms=OperatorData::terms(definition);

                for(unsigned int n=0;n<terms.size();n++){
                    if(terms[n].find("|")!=string::npos){
                        // inhomogenous term: add a fixed vector
                        DEVABORT("OperatorAddvector no longer supported");
                        //o.push_back(new OperatorAddvector(terms[n],IDisc,JDisc,iIndex,jIndex,Multiplier));
                    }
                    else {
                        // standard tensor product operator
                        o.push_back(new OperatorTensor(terms[n],IDisc,JDisc,iIndex,jIndex,Multiplier));
                    }
                    o.back()->iIndex=iIndex;
                    o.back()->jIndex=jIndex;

                    //need to keep o's for parallel setup
                    if(o.back()->isZero(1.e-12)){
                        delete o.back();
                        o.pop_back();
                    }
                }
                if(o.size()>1)PrintOutput::message("multiple floor terms "+definition);
            }
        }
        else if(definition=="eeInt6DHelium" or definition=="+eeInt6DHelium")
            ABORT("path disabled - specify Helium electron repulsion as +[[eeInt6DHelium]] instead");
        else if(definition.find("[[")!=string::npos){
            // eventually, all special cases above should be moved to this syntax

            // remove parameter of this term from definition
            string specialDef=tools::cropString(definition.substr(termParameter.length()));

            OperatorFloor *of=OperatorFloor::specialFactory(name,specialDef,iIndex,jIndex,Multiplier);
            if (of==0)
                PrintOutput::warning("failed to create special OperatorFloor "+definition,3);
            else if (not of->isZero()){
                O.push_back(new Operator(iIndex,jIndex,0,name));
                O.back()->oFloor=of;
//                Parallel::operatorAssign(O.back());
            }
            else {
//                Parallel::operatorAssign(this);
                delete of;
                of = 0;
            }

        }
        else ABORT("failed to create operator "+definition+" (note: describe non-standard operators as [[non-standard name]])");

        if(o.size()>0)o[0]->timeDepFac=TFac;
        if(oFloor!=0)oFloor->setFactor(TFac);

    } else if(iIndex->hasFloor() and not jIndex->hasFloor()){
        //push jblock
        o.clear();
        for (unsigned int j=0;j<jIndex->childSize();j++) {
            if(Multiplier==0.)continue;
            string newDef(OperatorData::substitute(definition,vector<size_t>(0),rightSub,0,jIndex,spec, 0, j));
            O.push_back(new Operator(name,newDef,IDisc,JDisc,iIndex,jIndex->child(j),Multiplier,1,TFac));
            if (O.back()->isZero(1.e-12)) { // immediately remove operators found to be 0
                delete O.back();
                O.pop_back();
            }
        }
    } else if(jIndex->hasFloor() and not iIndex->hasFloor()){
        //push iblock
        o.clear();
        for (unsigned int j=0;j<iIndex->childSize();j++) {
            if(Multiplier==0.)continue;
            string newDef(OperatorData::substitute(definition,leftSub,vector<size_t>(0),iIndex, 0, spec, j, 0));
            if(tensor!=0)ABORT("at last");
            O.push_back(new Operator(name,newDef,IDisc,JDisc,iIndex->child(j),jIndex,Multiplier,1,TFac));

            if (O.back()->isZero(1.e-12)) { // immediately remove operators found to be 0
                delete O.back();
                O.pop_back();
            }
        }
    }
    else
    {
        UseMatrix mult;
        const Index * tIndex=iIndex;

        // set up the tensor factor (defaults to 1)
        preTensor=0;
        tensor=new Tensor(OperatorData::first(definition),IDisc,JDisc,iIndex,jIndex);
        if(Tensor::keep(iIndex,jIndex,tensor,definition,mult))tIndex=tensor->I;

        // multiply by host level factor
        mult*=Multiplier;

        // apply operator constraints (for speeding up the setup)
        if(ReadInput::main.flag("DEBUGnoQualifiers","do not apply extra constraints"))
            PrintOutput::warning("all qualifiers are disabled");
        else
        {
            int level=iIndex->depth();
            if(iIndex->root()->axisName()=="Hybrid")level--; //HACK (lots of these, to be eliminated)
            OperatorData::constrain(mult,definition,level);
        }

        o.clear();

        string newDef(OperatorData::substitute(definition,leftSub, rightSub, iIndex,jIndex, spec,0,0));
        for (int j=0;j<jIndex->childSize();j++) {
            for (int i=0;i<tIndex->childSize();i++) {
                if(mult(i,j)==0.)continue;

                if(not leftSub.empty() or not rightSub.empty())
                    newDef=OperatorData::substitute(definition,leftSub, rightSub, iIndex,jIndex, spec, i, j);
                // set up, unless we know=0:
                if(not OperatorData::isZero(definition,iIndex,jIndex))
                {
                    O.push_back(new Operator(name,OperatorData::remainder(newDef),IDisc,JDisc,
                                             tIndex->child(i),jIndex->child(j),mult(i,j),1,TFac));
                    if (O.back()->isZero(1.e-12)) {
                        delete O.back();
                        O.pop_back();
                    }
                }
            }
        }

        if(o.size()>0 and O.size()>0) ABORT("Operator::construct(): nonfloor level has operatorsingle");
        fuse(O); // try fusing blocks with equal indices
        split(O);
    }

Return:
    for(unsigned int k=0;k<O.size();k++){
        if(O[k]->isZero(1.e-12)){
            delete O[k];
            O.erase(O.begin()+k);
        }
    }
    if(iIndex->isRoot() and jIndex->isRoot()){
        PrintOutput::progressStop();
    }
    if(tensor!=0 and preTensor!=0)ABORT("can only have pre- OR post-tensor");
}



Discretization* Operator::dataDisc(Discretization *Idisc, Discretization *Jdisc){
    if(Idisc==Jdisc)return Idisc;
    if(Idisc->parent==Jdisc)return Idisc;
    if(Jdisc->parent==Idisc)return Jdisc;
    cout << Idisc->name << " " << Jdisc->name << endl;
    ABORT("Operator::dataDisc: cannot assign data: two discretizations not in parent-child relation");
    return Idisc;  // Dead code, cannot be reached because of ABORT
}

Operator::~Operator() {
    if(iIndex == 0 or jIndex == 0)
        ABORT("operator destruction must happen BEFORE destruction of index!");

    for(unsigned int i=0;i<o.size();i++){
        if((definition!="mapToParent") and (definition!="mapFromParent")){
            if(ownsData and o[i]!=0)delete o[i];
            o[i] = 0;
        }
    }
    o.clear();

    for (unsigned int ij=0;ij<O.size();ij++){
        if(ownsData and O[ij]!=0)delete O[ij];
        O[ij] = 0;
    }
    O.clear();

    if(ownsData){
        delete tensor;
        delete preTensor;
        delete oFloor;
    }
    tensor=0;
    preTensor=0;
    oFloor=0;
}

//complex<double> Operator::matrixElementUnscaled(const Coefficients &wf1, const Coefficients &wf2) const{
//    apply(1.,wf2,0.,*tempLHS());
//    return wf1.innerProductUnscaled(tempLHS());
//}

Operator* Operator::nodeAt(const std::vector<unsigned int> & Idx) const{
    if(Idx.size()==0)return const_cast<Operator*>(dynamic_cast<const Operator*>(this));
    if(Idx[0]>=childSize())return 0;
    if(Idx.size()==1)return const_cast<Operator*>(this)->child(Idx[0]);
    return const_cast<Operator*>(this)->child(Idx[0])->nodeAt(std::vector<unsigned int>(Idx.begin()+1,Idx.end()));
}

void Operator::allMatrixElements(std::vector<const Coefficients *> Vecs, std::vector<const OperatorAbstract*> Ops, string File) const {
    if(Ops.size()==0)return;
    PrintOutput::message("matrix elements on file "+File);
    if(iIndex!=jIndex)ABORT("only for operator within an index space");
    vector<Coefficients> opVec;
    for(unsigned int m=0;m<Ops.size();m++){
        if(Ops[m]->iIndex!=iIndex or Ops[m]->jIndex!=iIndex)ABORT("only for operators within an index space");
        opVec.push_back(Coefficients(iIndex));
    }
    vector<complex<double> > norm,ener,matel(Ops.size());

    ofstream file;
    file.open(File.c_str());
    for(unsigned int m=0;m<Ops.size();m++)file<<"# "+Ops[m]->name<<endl;
    file<<"# Ener1      Ener2";
    for(unsigned int m=0;m<Ops.size();m++)file<<setw(12)<<" "+Ops[m]->name;
    file<<endl;


    for(unsigned int k=0;k<Vecs.size();k++){
        norm.push_back(1./sqrt(jIndex->localOverlap()->matrixElement(*Vecs[k],*Vecs[k])));
        ener.push_back(matrixElement(*Vecs[k],*Vecs[k])*norm[k]*norm[k]);
        for(unsigned int m=0;m<Ops.size();m++){
            opVec[m].setToZero();
            Ops[m]->apply(1.,*Vecs[k],0.,opVec[m]);
        }
        for(unsigned int l=0;l<=k;l++){
            for(unsigned int m=0;m<opVec.size();m++)
                matel[m]=Vecs[l]->innerProduct(&opVec[m])*norm[k]*norm[l];
            // print if any non-zero element
            if(tools::anyElementAbs(matel,tools::larger,1.e-10)){
                file<<setw(12)<<setprecision(8)<<ener[l]<<" "<<ener[k];
                for(unsigned int m=0;m<matel.size();m++)file<<" "<<matel[m];
                file<<endl;
            }
        }
    }
}

bool Operator::isZero(double Eps) {
    if(iIndex->sizeStored()==0)return true;
    if(jIndex->sizeStored()==0)return true;
    if(tensor!=0 and tensor->mat.isZero(1.e-12))return true;
    if(oFloor!=0)return false;
    for(unsigned    int i=0;i<o.size();i++)
        if(not o[i]->isZero(Eps)) return false;
    for(unsigned int n=0;n<O.size();n++)
        if(not O[n]->isZero(Eps))return false;
    return true;
}

double Operator::nonZeroBlocks() const {
    double nonz=0;
    // block count, avoid double counting of floors
    for(unsigned int k=0;k<O.size();k++)
        nonz+=O[k]->nonZeroBlocks();
    if(oFloor!=0){
        nonz+=iIndex->sizeStored()*jIndex->sizeStored();
    }
    else
        for(unsigned int k=0;k<o.size();k++)
            nonz+=o[k]->iIndex->sizeStored()*o[k]->jIndex->sizeStored();
    return nonz;
}
double Operator::nonZeros() const {
    double nonz=0;
    // block count, avoid double counting of floors
    for(unsigned int k=0;k<O.size();k++)
        nonz+=O[k]->nonZeros();
    if(oFloor!=0){
        UseMatrix mat;
        oFloor->matrix(mat);
        nonz+=mat.nonZeros();
    }
    else
        for(unsigned int k=0;k<o.size();k++)nonz+=o[k]->oNonzeros;
    return nonz;
}

TIMERRECURSIVE(axpy,)
TIMERSAMPLE(axpy_dyn,)
TIMERSAMPLE(newFloor,)
TIMERSAMPLE(oldFloor,)
void Operator::apply(std::complex<double> Alfa, const Coefficients &Xin, std::complex<double> Beta, Coefficients &Z) const
{
    STARTDEBUG(axpy);
    Z*=Beta;


    if(jIndex!=Xin.idx())ABORT("right hand operator index does not match coefficients: "+name);
    if(iIndex!=Z.idx())ABORT("left hand operator index does not match coefficients: "+name);

    // FLOOR LEVEL
    if(oFloor!=0){
        vector<complex<double> > x(Xin.floorData(),Xin.floorData()+Xin.idx()->sizeStored());
        oFloor->apply(Alfa,x.data(),Xin.idx()->sizeStored(),1.,Z.floorData(),Z.idx()->sizeStored());
        oFloor->applyLeftOverlap(Z.floorData());
        //        }
    } else {
        if(o.size()>1)ABORT("only single floor allowed");
        for(unsigned int ij=0;ij<o.size();ij++){
            if(o[ij]->timeDepFac!=0)Alfa*=*o[ij]->timeDepFac;
            DEVABORT("out of service");
//            o[ij]->axpy(Alfa,*(Xin.getFloor()),1.,*(Z.getFloor()));
        }
    }

    if(O.size()==0)goto Return;

    Coefficients * Y,*X;
    // no tensor factor: directly write from input to output
    Y=&Z;
    X=const_cast<Coefficients*>(&Xin);
    if(tensor!=0){
        Y=tensor->Y;
        Y->setToZero();    }
    else if(preTensor!=0){
        X=preTensor->Y;
        X->setToZero();
    }


    // apply the tensor factor befor descending from this level
    if(preTensor!=0){
        if(&Xin==X)ABORT("Xin==X");
        if(*(preTensor->factor)!=0.){
            for (unsigned int n=0;n<preTensor->mat.cols();n++){
                for (unsigned int m=0;m<preTensor->mat.rows();m++){
                    if(preTensor->mat(m,n)==0.)continue;
                    if(preTensor->mat(m,n)!=0.)X->child(m)->axpy(preTensor->mat(m,n),Xin.child(n));
                }
            }
        }
    }

    // aplly all sub-operators
    for(unsigned int n=0;n<O.size();n++){
        const Coefficients* jVec=X;
        Coefficients* iVec = Y;
        if(child(n)->iIndex!=iIndex)iVec=iVec->child(child(n)->iIndex->nSibling());
        if(child(n)->jIndex!=jIndex)jVec=jVec->child(child(n)->jIndex->nSibling());
        child(n)->apply(Alfa,*jVec,1.,*iVec);
    }

    // apply the tensor factor after returning to this level
    if(tensor!=0){
        if(&Z==Y)ABORT("Z==Y");
        if(*(tensor->factor)!=0.){
            for (unsigned int n=0;n<tensor->mat.cols();n++){
                for (unsigned int m=0;m<tensor->mat.rows();m++){
                    if(tensor->mat(m,n)==0.)continue;
                    if(tensor->mat(m,n)!=0.)Z.child(m)->axpy(tensor->mat(m,n),Y->child(n));
                }
            }
        }
    }

Return:
#ifdef _PARALLEL_
    if(Z.index->isRoot())Parallel::synchronize(&Z);     // check for completion of receives and add buffers into Z
#endif
    STOPDEBUG(axpy);

}

void Operator::applyAdjoint(std::complex<double> Alfa, const Coefficients &X, std::complex<double> Beta, Coefficients &Z) const{
    ABORT("remains to be implemented");
}

void Operator::update(double Time, const Coefficients* CurrentVec){Parameters::update(Time);}

void Operator::axpy(const Wavefunction& X, Wavefunction& Y, bool transpose) const {

    Y.time=X.time;
    Parameters::update(X.time);
    if(transpose) ABORT("Operator::axpy: transpose not implemented yet");
    axpy(*X.coefs,*Y.coefs);
}

void Operator::matrixDirect(UseMatrix & Mat) const {
    vector<vector<complex<double> > > mat;
    matrix(mat);
    // NOTE: mat[n] is n'th column of actual matrix
    Mat=UseMatrix::Zero(mat[0].size(),mat.size());
    for (unsigned int n=0;n<mat.size();n++)
        for(unsigned int m=0;m<mat[n].size();m++)
            Mat(m,n)=mat[n][m];
}

void Operator::matrix(vector<vector<complex<double> > >& Mat, std::string Constraint) const { // brute force matrix calculation
    Coefficients one(jIndex);
    Coefficients opOne(iIndex);
    vector<complex<double>*> pOne;
    vector<complex<double>*> pOpOne;
    //one.pointerToC(pOne,vector<int>(0));
    //opOne.pointerToC(pOpOne,vector<int>(0));
    one.pointerToC(pOne);
    opOne.pointerToC(pOpOne);
    Mat.resize(0);
    for (unsigned int n=0;n<pOne.size();n++) {
        Mat.push_back(vector<complex<double> >(0) );
        opOne.setToZero();
        one.setToZero();
        *pOne[n]=1.;

        axpy(one,opOne);
        for (unsigned int m=0;m<pOpOne.size();m++) {
            Mat[n].push_back(*(pOpOne[m]));
        }
    }
}

TIMERRECURSIVE(matrixAdd,)
/// add factor*(operator matrix) and contract for continuity conditions
void Operator::matrixAdd(complex<double> factor, UseMatrix & Mat) const {
    STARTDEBUG(matrixAdd);

    // if matrix is not there yet, create proper size zero matrix
    if(Mat.size()==0)
        Mat=UseMatrix::Zero(iIndex->globalLength(),jIndex->globalLength());

    // get uncontracted global matrix
    UseMatrix tmat;
    matrix(tmat);

    // contract for continuity conditions and (optionally) re-sort
    matrixContract(tmat,Mat,factor);
    STOPDEBUG(matrixAdd);
}

// create floor-diagonal submatrix
void Operator::floorDiagonalMatrix(UseMatrix & Mat, unsigned int I0) const {
    if(tensor!=0 or preTensor!=0)ABORT("only in absence of tensor factors");
    UseMatrix mat;
    if(iIndex->hasFloor() and (O.size()==0 or O[0]->iIndex!=iIndex))
        // single block on floor level
    {
        matrix(mat);
        Mat.block(I0,I0,mat.rows(),mat.cols())+=mat;
    }
    else
    {
        // not final floor yet
        for(unsigned int k=0;k<O.size();k++){
            if(O[k]->iIndex==O[k]->jIndex){
                // only the diagonal blocks
                O[k]->floorDiagonalMatrix(Mat,I0+O[k]->iIndex->posIndex(iIndex));
            }
        }
    }
}

/// write operator matrix into Matrix (without imposing continuity!)
TIMERRECURSIVE(matrix,)
void Operator::matrix(UseMatrix & Mat) const {
    STARTDEBUG(matrix);

    // create zero matrix
    Mat=UseMatrix::Zero(iIndex->sizeStored(),jIndex->sizeStored());

    // continue descent into hierarchy
    UseMatrix mat;
    complex<double>fac=1.;
    unsigned int i0,j0;
    if(tensor==0){
        for(unsigned int ij=0;ij<O.size();ij++){
            O[ij]->matrix(mat);
            DEVABORT("out of seervice");
//            i0=iIndex->position(O[ij]->iIndex);
//            j0=jIndex->position(O[ij]->jIndex);
            Mat.block(i0,j0,mat.rows(),mat.cols())+=mat*fac;
        }
        if(oFloor==0){
            for(unsigned int ij=0;ij<o.size();ij++){
                o[ij]->matrix(mat);
                Mat+=mat*fac;
            }
        }
        else {
            oFloor->matrix(mat);
            if(mat.rows()!=Mat.rows() or mat.cols()!=Mat.cols()){
                cout<<"matrices do not match"<<mat.strShape()<<" "<<Mat.strShape()<<endl;
//                mat.print("mat",2);
//                cout<<"lhs\n"<<iIndex->str()<<endl;
//                cout<<"rhs\n"<<jIndex->str()<<endl;
            }
            Mat+=mat*fac;
        }

    }
    else
    {
        fac=*tensor->factor;
        // note: this may be inconsistent with global sorting!
        unsigned int j,k;
        for (unsigned int ij=0;ij<O.size();ij++){
            k=O[ij]->iIndex->nSibling();
            j=O[ij]->jIndex->nSibling();
            if(k!=j)ABORT("lower tensor factors must be on diagonal");
            for(unsigned int i=0;i<tensor->mat.rows();i++){
                if(tensor->mat(i,k)==0.)continue;
                O[ij]->matrix(mat);
                Mat.block(i*mat.rows(),k*mat.cols(),mat.rows(),mat.cols())=mat*(tensor->mat(i,k)*fac);
            }
        }
        for (unsigned int ij=0;ij<o.size();ij++){
            j=o[ij]->jIndex->nSibling();
            k=o[ij]->jIndex->nSibling();
            if(k!=j)ABORT("lower tensor factors must be on diagonal");
            for(unsigned int i=0;i<tensor->mat.rows();i++){
                if(tensor->mat(i,k)==0.)continue;
                o[ij]->matrix(mat);
                Mat.block(i*mat.rows(),k*mat.cols(),mat.rows(),mat.cols())=mat*(tensor->mat(i,k)*fac);
            }
        }
    }
    STOPDEBUG(matrix);
}

void Operator::eigenValues(Operator & Overlap, vector<std::complex<double> > &Eval,
                           unsigned int MaxN, double Emin, double Emax,bool excludeEnergyRange){
    vector<Coefficients*> Dum;
    eigen(Overlap,Eval,Dum,MaxN,Emin,Emax,excludeEnergyRange,false);
}

struct rankedValue{ double v; unsigned int pos; rankedValue(double V,unsigned int P):v(V),pos(P){}};
static bool lessValue(const rankedValue & x,const rankedValue & y){return x.v<y.v;} // for comparisons below

void Operator::eigen(const OperatorAbstract & Overlap, vector<std::complex<double> > &Eval, std::vector<Coefficients*> &Evec,
                     unsigned int MaxN, double Emin, double Emax, bool excludeEnergyRange,
                     bool Vecs, const string Sort, const string Constraints) const {
    if (iIndex!=jIndex)ABORT("operator left and right index do not match");
    if(Overlap.iIndex!=Overlap.jIndex)ABORT("overlap left and right index do not match");
    if(iIndex!=Overlap.iIndex)ABORT("operator="+name+" and overlap do not share index: "
                                    +tools::str(iIndex)+" vs. "+tools::str(Overlap.iIndex));

    if(iIndex->isRoot()){
        // inform if somewhat larger problem
        if(iIndex->sizeStored()>500)
            PrintOutput::DEVmessage(Str("Eigensolver problem size dim=")+iIndex->sizeStored());
        Eval.clear();
        Evec.clear();

        if(Constraints.length()!=0 and
                Constraints.find("ZeroBoundary=")==string::npos and
                Constraints.find("FloorDiagonal")==string::npos
                )
            ABORT("undefined Constraints: "+Constraints+", admissible: ZerorBoundary=all,ZeroBoundary=val1,val2,..., FloorDiagonal");
    }

    // probe for Operator type overlap
    const Operator * castOvr=dynamic_cast<const Operator*>(&Overlap);

    // solve eigenproblem for each block (recursive)
    if (eigenMethod!="Arpack" and iIndex->sizeCompute()>10
            and isBlockDiagonal() and castOvr!=0 and castOvr->isBlockDiagonal() and iIndex->childSize()!=0
            or diagonalConstraint(Constraints)){
        PrintOutput::message("Eigensolver: "+tools::str(iIndex->childSize())+" diagonal blocks on sub-operator ("+tools::str(iIndex->index(),",",0)+")");
        for (unsigned int k=0;k<iIndex->childSize();k++) {
            // collect blocks for current sub-index
            vector<Coefficients*> Bvec;
            vector<complex<double> > Bval;
            Operator Ovr(castOvr,iIndex->child(k),iIndex->child(k));
            Operator Opr(this,iIndex->child(k),iIndex->child(k));

            // get block-wise eigenvectors
            Opr.eigen(Ovr,Bval,Bvec,MaxN,Emin,Emax,excludeEnergyRange,Vecs,Sort,Constraints);

            // put block eigenvectors into next higher level coefficient (for now, set unused coefficients to zero)
            for (unsigned int l=0;l<Bval.size();l++){
                Eval.push_back(Bval[l]);
                if(Vecs){
                    Evec.push_back(new Coefficients(iIndex,0.)); // create zero vector
                    DEVABORT("out of use");
                    //Evec.back()->child(k)->attach(Bvec[l]);          // move pointers to output vectors
                }
            }
        }
    } else {
        // no further blocking, solve eigenproblem
        if(Evec.size()>1)ABORT("need Evec.size()<=1, is: "+tools::str(Evec.size())+"can at most contain starting vector");


        if(      eigenMethod=="Arpack" or
                 (eigenMethod=="auto" and (100*MaxN)<=iIndex->globalLength() and iIndex->globalLength()>=500 and Constraints=="")
                 )
        {
            if(Constraints!="")ABORT("constraints "+Constraints+" not implemented for Arpack");
            complex<double> inputShift=Arp::shift; // save as local manipulations may occur

            // few eigenvectors, case for Arnoldi solver
            Arp A(this);
            if(iIndex->isRoot())
                PrintOutput::message("Arpack eigensolver for "+tools::str(MaxN)+" vector(s) of a size="+tools::str(A.lvec)+" problem");

            if(excludeEnergyRange)ABORT("for now, cannot exclude energy range with Arpack");
            string which=Sort;
            if(Emin>0. and Emax>=DBL_MAX/4.){
                Arp::shift=Emin-100.;
                which="LargeAbs";
            }


            if(eigenMethod!="Arpack")
                if((100*MaxN)>iIndex->globalLength() or iIndex->globalLength()<500)
                    PrintOutput::warning("\"Arpack\" may be inefficient for "+tools::str(min(MaxN,iIndex->globalLength()))
                                         +" "+which+" eigenvectors out of "+tools::str(iIndex->globalLength())
                                         +" - try \"Lapack\" or \"auto\"");

            // use highest Evec als starting vector
            if(eigenMethod=="Arpack")
                A.eigen(Eval,Evec,min(MaxN,iIndex->sizeStored()),which,Evec.size()==1); // restart if there is a guess vector
            else
                A.eigen(Eval,Evec,min(MaxN,iIndex->globalLength()),which,Evec.size()==1); // restart if there is a guess vector

            if(tools::anyElementAbs(Eval,smaller,1.e-10)){
                ABORT("\n Eigenvalue(s):\n "+tools::str(Eval,"\n ")
                      +"\n Arpack found eigenvalue near zero - very likely this is incorrect "
                      +"\n Use input Eigensolver: shift=alfa to internally use shifted values, current alfa="+tools::str(Arp::shift,3));
            }

            Arp::shift=inputShift; // restore input shift
        } else {
            if(not iIndex->isRoot())PrintOutput::outputLevel("low");
            // Lapack solver(s)
            PrintOutput::message("Lapack eigensolver for "+tools::str(min(MaxN,iIndex->globalLength()))
                                 +" vector(s) of a size="+tools::str(iIndex->globalLength())+" problem");

            if(iIndex->globalLength()>5000)PrintOutput::warning("large lapack problem (long times and large mememory will be required): size="
                                                                +tools::str(iIndex->globalLength()),3);
            // get full operator matrix
            UseMatrix mat;
            if(Constraints.find("FloorDiagonal")==string::npos)
                matrixAdd(1.,mat); // (recursively) add into global matrix storage
            else {
                UseMatrix mat0=UseMatrix::Zero(iIndex->sizeCompute(),iIndex->sizeCompute());
                floorDiagonalMatrix(mat0,0);
                matrixContract(mat0,mat);
            }


            // modify matrix by zero'ing rows and columns
            vector<unsigned int> zeroIndex;
            if(Constraints.find("ZeroBoundary=all")!=string::npos or Constraints.find("FloorDiagonal")!=string::npos){
                // set all boundary indices = 0
                iIndex->boundaryIndices(zeroIndex);
                //                cout<<"zeroIndex "<<tools::str(zeroIndex)<<endl;
            }
            else if(Constraints.find("ZeroBoundary=")!=string::npos){
                // set selected boundary indices = 0
                string bds=Constraints.substr(Constraints.find("ZeroBoundary=")+13);
                if(bds.find(":"))bds=bds.substr(0,bds.find(":"));
                vector<string> vbds=tools::splitString(bds,',');
                for(unsigned int k=0;k<vbds.size();k++)
                    iIndex->globalElementBoundary(tools::string_to_double(vbds[k]),zeroIndex);
            }
            else if(Constraints!="")ABORT(Str("unrecongnized operator constraint")+Constraints);

            // zero rows and columns corresponding to ZeroBoundary
            for(unsigned int k=0;k<zeroIndex.size();k++){
                mat.col(zeroIndex[k])*=0.;
                mat.row(zeroIndex[k])*=0.;
            }

            // get full overlap matrix
            UseMatrix globalVectors;
            UseMatrix ovr=UseMatrix::Zero(iIndex->globalLength(),jIndex->globalLength());
            Overlap.matrixAdd(1.,ovr);

            UseMatrix uEval;
            // STARTDEBUG(blockEigen);
            mat.eigenBlock(uEval,globalVectors,ovr,Vecs);
            // STOPDEBUG(blockEigen);

            vector<int> eigenvectors;
            unsigned int nEv=0;
            for(unsigned int k=0; k<uEval.size(); k++) {
                // acceptance for: excludeEngeryRange==true, eval.real() in [emin,emax]==false, and vice versa
                if(excludeEnergyRange != (Emin<=uEval(k).real() and uEval(k).real()<=Emax) ) {
                    nEv++;
                    Eval.push_back(uEval(k).complex());
                    if(Vecs)eigenvectors.push_back(k);
                }
            }

            if(Vecs){
                // copy back into coefficients
                iIndex->unGlobal(globalVectors,false,Evec,eigenvectors);
            }
        }

        // sort as desired
        tools::sortKey(Sort,Eval,Evec);

        // select Eigenvalues and vectors: outside range or exceeding the maximal count
        for(int k=Eval.size()-1;k>=0;k--){
            if(k>MaxN-1 or Eval[k].real()<Emin or Eval[k].real()>Emax){
                Eval.erase(Eval.begin()+k);
                if(Evec.size()>k){
                    delete Evec[k];
                    Evec.erase(Evec.begin()+k);
                }
            }
        }

        // pseudo-normalize
        for(unsigned int k=0;k<Evec.size();k++){
            *Evec[k]*=1./sqrt(Overlap.matrixElement(*Evec[k],*Evec[k],true));
        }

    }
    PrintOutput::outputLevel("full");
    if(iIndex->isRoot())PrintOutput::paragraph();


    if(iIndex->isRoot()){
        // final sorting before return
        tools::sortKey(Sort,Eval,Evec);

        // pseudo-normalize
        for(unsigned int k=0;k<Evec.size();k++){
            *Evec[k]*=1./sqrt(Overlap.matrixElement(*Evec[k],*Evec[k],true));
        }
    }

}

/// OnlyForDiagonalOperator requires operator to be actually diagonal
Coefficients  Operator::diagonal(bool OnlyForDiagonalOperator) const{
    if(OnlyForDiagonalOperator and iIndex!=jIndex)ABORT("operator is not  block diagonal");

    Coefficients res(iIndex,0.);
    if(childSize()>0 and child(0)->iIndex==iIndex){
        // in case there are multiple diagonal blocks...
        res=child(0)->diagonal(OnlyForDiagonalOperator);
        for(int k=1;k<childSize();k++)res+=child(0)->diagonal(OnlyForDiagonalOperator);
    }

    else {
        if(oFloor!=0 or o.size()!=0){
            // place diagonal into coefficients
            UseMatrix mat;
            if(oFloor!=0)
                oFloor->matrix(mat);
            else
                o[0]->matrix(mat);

            if(OnlyForDiagonalOperator and not mat.isDiagonal(1.e-12)){
                mat.print(Str("mat")+iIndex->str()+jIndex->str(),2);
                ABORT("operator floor is not diagonal");
            }
            // not fast, but save
            for(int k=0;k<min(iIndex->sizeStored(),jIndex->sizeStored());k++)
                res.data()[k]=mat(k,k).complex();
        }

        for(int k=0;k<childSize();k++){
            if(child(k)->iIndex==child(k)->jIndex)
                *res.child(child(k)->iIndex->nSibling())+=child(k)->diagonal(OnlyForDiagonalOperator);
            else
                if(OnlyForDiagonalOperator)ABORT("operator is not block-diagonal");
        }
    }
    return res;
}

bool Operator::isIdentity() const {
    if(tensor!=0 or preTensor!=0)return false;
    if(not isBlockDiagonal())return false;
    if(oFloor!=0){
        UseMatrix mat;
        oFloor->matrix(mat);
        if(not mat.isIdentity())return false;
    }
    for(int k=0;k<childSize();k++)
        if(not const_cast<Operator*>(this)->child(k)->isIdentity())return false;
    return true;
}

// true if blocked on the the next lower index level
bool Operator::isBlockDiagonal() const {
    if(childSize()==0)return false; // end of tree - probably operator abstract
    if(iIndex->continuity()!=Index::npos or jIndex->continuity()!=Index::npos)return false;
    if(iIndex->hasFloor() or jIndex->hasFloor())return false;
    if(oFloor!=0)return false;
    bool blockDiagonal=iIndex==jIndex;
    for(unsigned int k=0;k<childSize();k++){
        if(not blockDiagonal)break;
        if(child(k)->jIndex==jIndex)blockDiagonal&=child(k)->isBlockDiagonal(); // same level block - descend
        else                        blockDiagonal&=child(k)->iIndex==child(k)->jIndex;
    }
    return blockDiagonal;
}

bool Operator::diagonalConstraint(string Constraint) const{
    return Constraint.find("FloorDiagonal")!=string::npos and (iIndex->continuity()==Index::npos) and (jIndex->continuity()==Index::npos);
}

void Operator::checkVariable(std::string Message) const {
    vector<string> nonConst,timeDep;
    for(unsigned int k=0;k<Parameters::table.size();k++)
        if(Parameters::isFunction(Parameters::table[k].name) and definition.find(Parameters::table[k].name)!=string::npos){
            if(Parameters::table[k].name.find("[t]")!=string::npos)timeDep.push_back(Parameters::table[k].name);
            else                                                  nonConst.push_back(Parameters::table[k].name);
        }
    if(nonConst.size()+timeDep.size()>0){
        PrintOutput::newLine();
        PrintOutput::message("non-constant parameters in operator \""+name+"\": "+tools::str(timeDep,", ")+tools::str(nonConst,", "));


        if(nonConst.size()>0 and Message!="")PrintOutput::warning(Message);
    }
}

// check whether eigenvalue equation is fulfilled
void Operator::eigenVerify(const OperatorAbstract &Ovr, vector<complex<double> > &Eval, vector<Coefficients*> &Evec,
                           unsigned int print, double eps, bool Pseudo) const{
    if(tRecX::off("operator::eigen"))return;

    double epsRelDegen=1.e-12,epsAbsDegen=1.e-8; // level where to consider eigenvalues degenerated
    if(Eval.size()!=Evec.size())ABORT("Number of eigenvectors does not match number of eigenvalues");
    Coefficients check(iIndex);
    unsigned int cntE=0,cntO=0,cntP=0,cntD=0;
    double errE=0,errO=0,errP=0;
    vector<complex<double> > norm,pseudoNorm;

    for (unsigned int k=0;k<Eval.size();k++){
        // NOTE: the following should be replaced the OperatorAbstract::orthoNormalize and orthoNormalizeDegenerate
        Ovr.apply(1.,*Evec[k],0.,check);
        norm.push_back(      sqrt(Evec[k]->innerProduct(&check)));
        pseudoNorm.push_back(sqrt(Evec[k]->innerProduct(&check,Pseudo)));
        bool kDegen=false;
        for(unsigned int l=0;l<k;l++){
            complex<double> orth=      Evec[l]->innerProduct(&check);
            complex<double> pseudoOrth=Evec[l]->innerProduct(&check,Pseudo);
            if(abs(Eval[l]-Eval[k])>epsRelDegen*abs(Eval[l]) and abs(Eval[l]-Eval[k])>epsAbsDegen){
                // non-degenerate vectors must be (pseudo-)orthogonal
                if(abs(      orth/(      norm[k]*      norm[l]))>eps)cntO++;
                if(abs(pseudoOrth/(pseudoNorm[k]*pseudoNorm[l]))>eps)cntP++;
                errO=max(errO,abs(orth));
                errP=max(errP,abs(pseudoOrth));
            } else {
                cntD++;
                kDegen=true;
                Evec[k]->axpy(-pseudoOrth,Evec[l]);
            }
        }
        // re-normalize
        if(kDegen){
            Ovr.apply(1.,*Evec[k],0.,check);
            pseudoNorm[k]=sqrt(Evec[k]->innerProduct(&check,Pseudo));
        }

        //pseudo-normalize vector
        *Evec[k]*=1./pseudoNorm[k];
        check*=-Eval[k]/pseudoNorm[k];
        pseudoNorm[k]=1.;

        // calculate standard norm
        norm[k]=sqrt(Evec[k]->innerProduct(&check));

        // check eigenvector
        apply(1.,*Evec[k],1.,check);

        complex<double> diff=Ovr.matrixElement(check,check,Pseudo)/max(abs(Eval[k]),1.);
        if(abs(diff)>eps)cntE++;
        errE=max(errE,abs(diff));
    }
    if(cntE>0)
        PrintOutput::message("Verified "+tools::str(int(Eval.size()))+" eigenvectors: "+tools::str(cntE)+" relative errors > "
                             +tools::str(eps)+", max error="+tools::str(errE));
    if(Eval.size()>1 and cntP>0)
        PrintOutput::message("(Pseudo-)Orthogonality for "+tools::str(int(Eval.size()*(Eval.size()-1))/2)
                             +" pairs ("+tools::str(cntD)+" degenerated) "+tools::str(cntP)+" relative errors > "
                             +tools::str(eps)+", max error="+tools::str(errP));
    if(errE>1.e-8)PrintOutput::warning("serious eigenvector error");

    // try normalize:
    if(errP>1.e-8){
        cout<<"try normalize"<<endl;
        UseMatrix full,ovr;
        if(iIndex->globalLength()<2000){
            matrixAdd(1.,full);
            Ovr.matrixAdd(1.,ovr);
            if(not (full.isSymmetric() and ovr.isSymmetric())){
                if(not (full.isHermitian() and ovr.isHermitian()))
                    PrintOutput::warning("Operator is neither hermitian nor complex symmetric"); // give up, do nothing
                else
                    // standard-normalize
                    for(unsigned int k=0;k<Evec.size();k++)*Evec[k]*=1./norm[k];
            } else
                PrintOutput::DEVwarning("neither symmetric nor hermitian");
        }
    }

    if(print>0){
        PrintOutput::paragraph();
        PrintOutput::title("EIGENVALUES");
        PrintOutput::newRow();
        PrintOutput::rowItem("real");
        PrintOutput::rowItem("imag");
        for (unsigned int k=0;k<min(print,(unsigned int) Eval.size());k++){
            PrintOutput::newRow();
            PrintOutput::rowItem(Eval[k].real(),12);
            PrintOutput::rowItem(Eval[k].imag(),12);
        }
        PrintOutput::end();
    }
}


void Operator::show(string Kind) const {
    if(iIndex->depth()==0 and jIndex->depth()==0)cout<<definition<<" "<<name<<endl;
    if(Kind=="full")opShow(0);
    else if(Kind=="structure"){
        if(o.size()+O.size()>0 or oFloor!=0){
            if(tensor!=0){
                for(unsigned int l=0;l<iIndex->depth();l++)cout<<"   ";
                cout<<"Tensor(post) "<<tensor->mat.rows()<<" X "<<tensor->mat.cols()<<endl<<flush;
                cout<<tensor->mat.str("",2)<<endl;
            }
            if(preTensor!=0){
                for(unsigned int l=0;l<iIndex->depth();l++)cout<<"   ";
                cout<<"Tensor(pre) "<<preTensor->mat.rows()<<" X "<<preTensor->mat.cols()<<endl<<flush;
                cout<<preTensor->mat.str("",2)<<endl;
            }
            for(unsigned int l=0;l<iIndex->depth();l++)cout<<"   ";
            cout<<"("+tools::str(iIndex->index())+"|"+tools::str(jIndex->index())+") "<<flush;
            cout<<" "<<iIndex<<" X "<<jIndex<<flush;
            cout<<" non-zeros/block size: "<<tools::str(nonZeros(),12)<<"/"<<tools::str(nonZeroBlocks(),12);
            if(O.size()!=0)     cout<<",  sub: "<<O.size()<<flush;
            else if(o.size()!=0 or oFloor!=0){
                for(unsigned int k=0;k<o.size();k++)cout<<" "+o[k]->strDataStructure()<<flush;
//                if(oFloor!=0)cout<<"F "<<oFloor->strInfo();
                if(oFloor!=0)cout<<"\n"<<oFloor->str();
            }
        }
    } else {
        ABORT("cannot show Kind="+Kind);
    }
    cout<<endl;
    for(unsigned int k=0;k<O.size();k++)O[k]->show(Kind);
}

void Operator::opShow(int id) const
{
    cout<<"Level "<<iIndex->depth()<<": ("<<tools::str(iIndex->index())+" |"+tools::str(jIndex->index())<<")"<<endl;
    if(tensor!=0)tensor->mat.print();
    for(unsigned int i=0;i<O.size();i++){
        cout<<tools::str(jIndex->index(),",",2);
        cout<<tools::str(iIndex->index(),",",2);
        O[i]->opShow(id);
    }
    for(unsigned int i=0;i<o.size();i++){
        cout<<tools::str(jIndex->index(),",",2);
        cout<<tools::str(iIndex->index(),",",2);
        cout << endl;
        o[i]->show("");
    }
}

void Operator::fuse(std::vector<Operator *> &Ops){

    if(not fuseOp)return; // globally switch off

    vector<double>maxNorm;
    for (unsigned int k=0;k<Ops.size();k++){
        if(Ops[k]==0)ABORT("this should not happen");
        maxNorm.push_back(Ops[k]->norm());
        for(unsigned int l=Ops.size()-1;l>k;l--){
            maxNorm[k]=max(maxNorm[k],Ops[l]->norm()); // keep track of norms before fusing
            if(Ops[k]->absorb(Ops[l]))Ops.erase(Ops.begin()+l);
        }
    }
    // remove fused blocks that add to zero
    for(int k=Ops.size()-1;k>=0;k--){
        if(Ops[k]->isZero(1.e-12*max(1.,maxNorm[k]))){
            delete Ops[k];
            Ops.erase(Ops.begin()+k);
        }
    }
}
void Operator::split(std::vector<Operator *> &Ops){
    // create operator for each single and OperatorFloor
    for(unsigned int k=0;k<Ops.size();k++){
        unsigned int m=0;
        while(Ops[k]->o.size()>1){
            Ops.push_back(new Operator(Ops[k]->iIndex,Ops[k]->jIndex,Ops[k]->o.back(),Ops[k]->name+"_"+tools::str(++m)));
            Ops.back()->oFloor=0;
//            if(Ops.back()->o.size()>0)Parallel::operatorAssign(Ops.back());
            Ops[k]->o.pop_back();
        }
    }
}

const std::complex<double> * Operator::variableParameter() const {
    if(o.size()>0)return const_cast<const complex<double> *>(o[0]->timeDepFac);
    if(oFloor!=0)return const_cast<const complex<double> *>(oFloor->factor());
    for(int k=0;k<O.size();k++)
        if(O[k]->variableParameter()!=0)return O[k]->variableParameter();
    return 0;
}

bool Operator::absorb(Operator *&Other){
    // only operator with equal indices, equal time-dependence, and equal tensr can be absorbed
    if(iIndex!=Other->iIndex)return false;
    if(jIndex!=Other->jIndex)return false;
    if(variableParameter()!=Other->variableParameter())return false;
    if(tensor==0 ^ Other->tensor==0)return false; // only one tensor =0, cannot work
    if(tensor!=0 and not tensor->operator==(*(Other->tensor)))return false;
    if(preTensor==0 ^ Other->preTensor==0)return false; // only one tensor =0, cannot work
    if(preTensor!=0 and not preTensor->operator==(*(Other->preTensor)))return false;

    if(name.find("(fused)")==string::npos)name+="(fused)";
    definition+=" "+Other->definition;
    for(unsigned int l=0;l<Other->O.size();l++)O.push_back(Other->O[l]);
    for(unsigned int l=0;l<Other->o.size();l++)o.push_back(Other->o[l]);
    fuse(O);
    OperatorSingle::fuse(o);
    split(O);

    // remove pointers to subblocks, so subblocks do not get deleted, when Other is deleted;
    Other->O.clear();
    Other->o.clear();
    delete Other;
    Other=0;
    return true;
}

bool Operator::Tensor::operator==(const Tensor & Other) const {
    if(factor!=Other.factor)return false;
    if(mat!=Other.mat)return false;
    return true;
}


Operator::Tensor::~Tensor(){
    if(Y!=0){
        delete Y;
        delete I;
    }
}
Operator::Tensor::Tensor(const Tensor &other)
    : mat(other.mat),factor(other.factor),Y(0)
{
    if(other.Y!=0){
        ABORT("for now, cannot copy-construct Tensor with non-trivial Y,I");
    }
}

Operator::Tensor::Tensor(string Definition, const Discretization *Idisc, const Discretization *Jdisc, const Index *IIndex, const Index *JIndex){
    // default: no intermediate index needed
    I=IIndex;
    Y=0;

    // supplement bases if needed
    if(IIndex->basisSet()==0)ABORT("no basis defined for IIndex");
    if(JIndex->basisSet()==0)ABORT("no basis defined for JIndex");
    construct(Definition,IIndex,JIndex);
}
Operator::Tensor::Tensor(string Definition, const Index *IIndex, const Index *JIndex){
    construct(Definition,IIndex,JIndex);
}

//HACK map a subset of indices
static Eigen::MatrixXcd mapSubSet(const BasisSet* Ibas, const BasisSet* Jbas){
    // for now, only very special case
    Eigen::MatrixXcd res;
    return res; //disable
    if(Ibas->size()>Jbas->size())return res;
    if((not Ibas->isIndex() or not Jbas->isIndex()) and
            (not Ibas->isGrid() or not Jbas->isGrid())    )return res;
    if(Ibas->size()==Jbas->size())return Eigen::MatrixXcd::Identity(Ibas->size(),Jbas->size());
    if(Ibas->transMat()->size()==0)return res;
    return Eigen::MatrixXcd::Map(Ibas->transMat()->data(),Ibas->size(),Jbas->size());
}

void Operator::Tensor::construct(string Definition, const Index *IIndex, const Index *JIndex){
    // default: no tensor factor - no intermediate Index or storage
    I=IIndex;
    Y=0;
    if(IIndex==0 or JIndex==0){
        ABORT("error "+tools::str(IIndex)+" "+tools::str(JIndex));
    }
    mat=UseMatrix::Constant(IIndex->childSize(),JIndex->childSize(),1.);

    // floors do not have tensor multipliers
    if(IIndex->hasFloor() or JIndex->hasFloor() or IIndex->isLeaf() or JIndex->isLeaf())return;

    // make sure there is no leading or trailing whitespace
    Definition=cropString(Definition);

    // decide, whether tensor factor
    if(Definition.find('>')==string::npos){
        if(Definition.find("map")==0)
            Definition="<map>"; // maps can be defined
        else if(Definition.find("diagonal")!=string::npos){
            if(IIndex->depth()==tools::string_to_int(tools::stringInBetween(Definition,"[","]")))return; // on continuity level, no tensor, allow all
            else Definition="<Id>";
        }
        else
            return;
    }

    // get tensor matrix
    if(Definition.find("map")==0 or Definition.find("diagonal")==0)Definition="<map>";
    // special case: subset
    Eigen::MatrixXcd subSet=mapSubSet(IIndex->basisSet(),JIndex->basisSet());
    if(subSet.size()>0){
        cout<<"is subset"<<IIndex->basisSet()->str()<<" of "<<JIndex->basisSet()->str()<<endl;
        mat=UseMatrix::UseMap(subSet.data(),subSet.rows(),subSet.cols());
    }
    else
        OperatorData::get(Definition.substr(0,Definition.find('>')+1),IIndex->basisSet(),JIndex->basisSet(),mat,factor);

    // factor has actually fallen out of use
    factor=Parameters::pointer("1");

    // clean up near zeros
    mat.purge(1.e-11,1.e-13);

    I=new Index();
    const_cast<Index*>(I)->setAxisName(IIndex->axisName());

    // we are supplementing the constructor - need a lot of const_cast's
    if(IIndex->subEquivalent()){
        // post-application (old style)
        const_cast<Index*>(I)->setBasis(JIndex->basisAbstract());
        for(unsigned int m=0;m<JIndex->basisSet()->size();m++)
            const_cast<Index*>(I)->childAdd(new Index(I,IIndex->child(0)));
        Y=new Coefficients(I);
    }
    else if (JIndex->subEquivalent()){
        // pre-application (new addition)
        const_cast<Index*>(I)->setBasis(IIndex->basisSet());
        for(unsigned int m=0;m<IIndex->basisSet()->size();m++)
            const_cast<Index*>(I)->childAdd(new Index(I,JIndex->child(0)));
        Y=new Coefficients(I);
    }
    else {
        delete I;
    }
}

bool Operator::Tensor::keep(const Index* IIndex, const Index* JIndex, Tensor* &Tens, string Definition, UseMatrix &Mult){
    // decide whether to keep tensor
    if(Tens->Y==0){
        // tensor was not set up properly
        if(Tens->mat.size()>0)Mult=Tens->mat;
        else Mult=UseMatrix::Constant(IIndex->childSize(),JIndex->childSize(),1.);
        delete Tens;
        Tens=0;
    }
    else if( // set up, but not useful or not suitable
             Tens->mat.nonZeros(1.e-12)<=max(Tens->mat.rows(),Tens->mat.cols()) // sparse-no advantage
             or OperatorData::first(Definition).find("{}")!=string::npos        // multi-dimensional - not applicable
             or not IIndex->subEquivalent()                                     // non-equivalent indices - cannot do tensor
             or not JIndex->subEquivalent()){
        Mult=Tens->mat*(*(Tens->factor));
        delete Tens;
        Tens=0;
    }
    else if (not useTensor) {                                                  // suppress by choice
        Mult=Tens->mat*(*(Tens->factor));
        delete Tens;
        Tens=0;
        if(Definition!=previousSuppressDefinition){
            previousSuppressDefinition=Definition;
            //            PrintOutput::progress(" "+Definition);
        }
    }
    else {
        // DO use tensor - Mult is on on rhs indices
        Mult=UseMatrix::Identity(JIndex->sizeStored(),JIndex->sizeStored());
    }
    Mult.purge();
    return Tens!=0;
}

Operator::Arp::Arp(const Operator *O)
    :Arpack(1.e-9,5000),o(O),x(Coefficients(O->jIndex)),y(Coefficients(O->iIndex)),tmp(Coefficients(O->iIndex)){
    x.pointerToC(pX);
    y.pointerToC(pY);
    tmp.setToZero(); // make sure =0
    y.setToZero(); // make sure Y=0
    lvec=y.size();
}

void Operator::Arp::eigen(vector<std::complex<double> > &Eval, vector<Coefficients *> &Rvec,
                          unsigned int Nvec, const string &Which, bool Restart){

    complex<double> defaultShift=shift;
    if(Restart){
        // initialize with starting vector
        if(Rvec.size()!=1)ABORT("for restarting, supply exactly one Rvec as starting vector");
        y=*Rvec[0];
        //delete Rvec[0];
        Rvec.clear();
        rvec.resize(lvec);
        for(unsigned int i=0;i<lvec;i++)rvec[i]=*(pY[i]);
    }
    else if(Rvec.size()>0)
        ABORT("must enter eigen with empty Rvec, unless Restart");

    if(Which=="SmallAbs"){
        if(Eval.size()!=1)ABORT("for Which=SmallAbs provide guess energy value in Eval");
        shift=Eval[0];
    }

    // let ArpackFunction compute the vectors
    Arpack::eigen(Nvec,Which,Restart,true);

    // move into Rvec
    Eval.resize(Nvec);
    for(unsigned int k=0;k<Nvec;k++){
        Eval[k]=eval[k]+Operator::Arp::shift;
        
        for(unsigned int i=0;i<lvec;i++)*(pY[i])=rvec[i+k*lvec];
        Rvec.push_back(new Coefficients(y));
        Rvec.back()->treeOrderStorage();
    }

    // reset in case is had been changed
    shift=defaultShift;
}

double Operator::norm() const {
    double nrm=0.;
    if(childSize()>0){
        for(int k=0;childSize()<k;k++)max(nrm,const_cast<Operator*>(this)->child(k)->norm());
    } else {
        if(oFloor==0)nrm=o[0]->norm();
        else         nrm=oFloor->norm();
    }
    return nrm;
}

void Operator::Arp::apply(const std::complex<double> *X, std::complex<double> *Y) {

    // copy to Coefficients
    for (unsigned int i=0;i<pX.size();i++)*(pX[i])=*(X+i);

    // y=S^-1 Op x - shift*x
    x.makeContinuous();
    o->apply(1.,x,0.,tmp);
    tmp.makeContinuous();
    o->iIndex->inverseOverlap()->apply(1.,tmp,0.,y);
    y.axpy(-shift,&x);
    // copy back and reset y to zero
    for (unsigned int i=0;i<pX.size();i++)*(Y+i)=*(pY[i]);
}

void Operator::convert(){
    if(oFloor!=0){
        if(o.size()==0)return;
        ABORT("cannot convert, oFloor already exists: "+tools::str(oFloor));
    }
    if(o.size()==0){
        for(unsigned int k=0;k<O.size();k++)O[k]->convert();
        return;
    }
    if(o.size()>1)ABORT("can only convert single floor, is "+tools::str(o.size()));

    vector<UseMatrix> mat;
    vector<const UseMatrix*>pMats;

    if(o[0]->mats.size()==1){
        mat.push_back(UseMatrix());
        o[0]->matrix(mat[0]);
    } else {
        for(unsigned int i=0;i<o[0]->mats.size();i++){
            mat.push_back(UseMatrix());
            mat.back()=*(o[0]->mats[i]);
        }
    }

    for(unsigned i=0;i<mat.size();i++){
        mat[i].purge();
        mat[i].expand(); // factor handles only full matrices
        pMats.push_back(&mat[i]);
    }
    oFloor=OperatorFloor::factory(pMats,definition+iIndex->hash()+jIndex->hash());
    if(oFloor==0){
        show("structure");
        ABORT("conversion failed");
    }
    // need to delete o[0] after success
    if(oFloor!=0){
        oFloor->setFactor(o.back()->timeDepFac);
        delete o.back();
        o.clear();
    }

}

void Operator::purge(){
    if(o.size()>1)ABORT("multiple OperatorSingle no longer allowed");

    for(unsigned int k=O.size();0<k;k--)
    {
        O[k-1]->purge();
        if(O[k-1]->O.size()==0
                and (O[k-1]->oFloor==0 or O[k-1]->oFloor->norm()==0 or O[k-1]->oFloor==Parallel::notOnHost)
                and O[k-1]->o.size()==0)
            O.erase(O.begin()+k-1);
    }

    if(o.size()==1 and o[0]->isZero()){
        delete o[0];
        o.clear();
    }
}
