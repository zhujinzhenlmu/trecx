// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "operatorTree.h"
#include "tools.h"
#include "mpiWrapper.h"

#include "discretizationHybrid.h"

#include "printOutput.h"
#include "index.h"
#include "coefficients.h"
#include "operatorFloor.h"
#include "operatorFloorNonLin.h"
#include "operator.h"
#include "operatorData.h"
#include "parameters.h"
#include "operatorTensor.h"
#include "parallel.h"
#include "str.h"
#include "basisNdim.h"
#include "diagnose.h"

#include "operatorZG.h"
#include "operatorZD.h"
#include "operatorFloorEE.h"
#include "operatorZDxZG.h"
#include "operatorZGxZD.h"
#include "operatorZGxZG.h"
#include "operatorZGSblock.h"
#include "constrainedView.h"

#include "asciiFile.h"
#include "basisMat1D.h"

#include "parallelOperator.h"

#include "log.h"
#include "algebra.h"
#include "basisMatDependent.h"
#include "basisOrbital.h"
#include "basisOrbitalNumerical.h"
#include "basisExpIm.h"
#include "basisChannel.h"

#include "operatorFloorXC.h"
#include "operatorHartree.h"

using namespace std;

static double epsPurge=1.e-12;

static std::map<std::string,size_t> memoryUsage;

void OperatorTree::compare(const OperatorTree * Other, double Eps) const {
    BlockView bThis(this,BlockView::depthFloor,BlockView::depthFloor);
    BlockView bOther(Other,BlockView::depthFloor,BlockView::depthFloor);
    if(bThis.blockRows()!=bOther.blockRows())DEVABORT("block-rows differ");
    if(bThis.blockCols()!=bOther.blockCols())DEVABORT("block-cols differ");
    if(bThis.triplets()!=bOther.triplets())DEVABORT("block-triplets sizes differ");
    for (int t=0;t<bThis.triplets();t++){
        for(int o=0;o<bOther.triplets();o++){
            if(bThis.block(t,0)->iIndex->index()==bOther.block(o,0)->iIndex->index()
                    and bThis.block(t,0)->jIndex->index()==bOther.block(o,0)->jIndex->index()){
                if(bThis.blocks(t)!=bOther.blocks(o))DEVABORT("number of blocks differ");
                for(int b=0;b<bThis.blocks(o);b++){
                    UseMatrix tMat,oMat;
                    bThis.block(t,b)->matrix(tMat);
                    bOther.block(o,b)->matrix(oMat);
                    if(not (tMat-=oMat).isZero(Eps)){
                        ABORT(Str("matrices differ\n")+oMat.str("o")+"\n"+tMat.str("t"));
                    }
                }
            }
        }
    }
    return;
}

bool OperatorTree::constructHybrid(const string &Name, const OperatorDefinition &Def, const Index *IIndex, const Index *JIndex, std::complex<double> Mult) {
    bool ihyb = IIndex->isHybrid(), jhyb = JIndex->isHybrid();
    if(!ihyb && !jhyb) return false;
    if((ihyb && !jhyb) || (!ihyb && jhyb)) ABORT("cannot mix hybrid with standard discretization");
    
    for(std::size_t i = 0; i < IIndex->childSize(); ++i) {
        for(std::size_t j = 0; j < JIndex->childSize(); ++j) {
            string defij = OperatorData::extractBlock(Def, i, j);
            if(defij != "") {
                defij = OperatorData::expandStandard(defij, IIndex->child(j)->hierarchy());
                auto pos = defij.find("(+<1>)");
                if(pos != std::string::npos) defij = defij.substr(0, pos) + "<1>"; // HACK: expansion or parsing broken, I don't know/care
                childAdd(new OperatorTree(Name, OperatorDefinition(defij), IIndex->child(i), JIndex->child(j), Mult, { }, false));
            }
        }
    }
    return true;
}

OperatorTree::~OperatorTree(){
    ParallelOperator::unsetHost(this);
    if(not _view)delete oFloor;
    oFloor=0;
}

UseMatrix* OperatorTree::colMat;
std::vector<std::complex<double> > OperatorTree::colStor;

OperatorTree::OperatorTree(const std::string Name, const std::string Definition, const Index* IIndex, const Index* JIndex, OperatorFloor *OFloor)
    :OperatorAbstract(Name,IIndex,JIndex),oFloor(OFloor),_view(false){definition=Definition;}

TIMER(operFloor,opTree)
TIMER(operPar,opTree)
TIMER(floor2,opTree)
TIMER(fuse,opTree)
TIMERRECURSIVE(opAll,opTree)
TIMER(iSub,opTree)
TIMER(para,opTree)
TIMER(purge,opTree)
TIMER(dist,opTree)

static bool skipAxis(const Index* Idx){
    // these axis do not carry operators
    if(Idx->axisName().find("surf")==0)return true;
    if(Idx->continuity()!=Index::npos && Idx->axisName().find("Ion")!=0)return true;
    if(Idx->axisName().find("spec")==0){
        for(const Index* idx=Idx;idx!=0;idx=idx->descend())
            if("k"+Idx->axisName().substr(4)==idx->axisName())return true;
    }
    return false;
}
TIMER(parOptree,)
TIMER(fuseOptree,)
TIMER(purgeOptree,)
TIMER(distriOptree,)

OperatorTree::OperatorTree(const string Name, const OperatorDefinition & Definition, const Index *IIndex, const Index *JIndex,
                           std::complex<double> Multiplier, std::vector<std::complex<double> *> TFac, bool EntryLevel)
    :OperatorAbstract(Name,IIndex,JIndex),oFloor(0),_view(false)
{
    definition=Definition;
    Index::build=true;
    STARTDEBUG(opAll);
    if(EntryLevel){
        LOG_PUSH(Name);
        Parameters::updateToOne();
    }
    OperatorDefinition(Definition).syntax();
    
    if(IIndex->axisName()=="Orbital" or JIndex->axisName()=="Orbital"){
        BasisOrbital::attachOperator(this,Name,Definition,IIndex,JIndex);
    }
    else if(!constructHybrid(Name, Definition, IIndex, JIndex)) {
        
        if(Operator::useTensor and Operator::fuseOp)
            PrintOutput::warning("simultaneous use of fuse() and true tensor may lead to incorrect results: "+definition,1);
        
        if(abs(Multiplier)<epsPurge)
            PrintOutput::warning("found near-zero multiplier in setup of operator "+definition);
        
        // split into terms, unbracket, expand, such that each term[n] starts as factor<def>... or factor[[defSpecial]]...
        string icoor=iIndex->coordinates();
        string jcoor=jIndex->coordinates();
        if(icoor=="Ndim")icoor=iIndex->basisNdim()->quadCoor();
        if(jcoor=="Ndim")jcoor=jIndex->basisNdim()->quadCoor();
        vector<string> terms=OperatorData::singleTerms(Definition,icoor,jcoor);
        if(terms.size()>1){
            // ommit terms if subregion
            if(name!="Commutator")OperatorDefinition::dropTerms(terms,IIndex->hierarchy());
            for(unsigned int n=0;n<terms.size();n++){
                childAdd(new OperatorTree(name,OperatorDefinition(terms[n],""),iIndex,jIndex,Multiplier,TFac,false));
            }
        }
        else {
            // separate factor and operator
            OperatorDefinition termOper;
            string termFactor=OperatorDefinition(terms[0],"").parameter(termOper);
            // add to list of function multipliers or multiply
            if(Parameters::isFunction(termFactor)){
                // use variable factors always with + sign
                termFactor=tools::cropString(termFactor);
                if(termFactor[0]=='-'){
                    Multiplier*=-1.;
                    termFactor[0]='+';
                } else if(termFactor[0]!='+')
                    termFactor="+"+termFactor;
                TFac.push_back(Parameters::pointer(termFactor));
            }
            else Multiplier*=*Parameters::pointer(termFactor);
            // various special case modifications
            
            if((iIndex->hasFloor() and jIndex->hasFloor())) {
                
                // remove auxiliary setup info (like $BAND.0.0 etc.)
                if(termOper.find("$") != string::npos)
                    termOper=termOper.substr(0,termOper.find("$"));
                
                // various special case modifications
                
                // Does not work as OperatorFloorEE sets up another OperatorTree
                // STARTDEBUG(floor);
                
                if(termOper.find("|")!=string::npos)ABORT("inhomogenous term not implemented");
                OperatorFloor * of;
                STARTDEBUG(operFloor)
                        if(OperatorData::isStandard(definition) and
                           iIndex->basisAbstract()->basisNdim()==0 and
                           jIndex->basisAbstract()->basisNdim()==0){
                    of=Parallel::operatorFloor(iIndex, jIndex,
                                               [&](){ return OperatorFloor::factory(termOper,iIndex,jIndex,Multiplier); }
                    );
                }
                else {
                    of=Parallel::operatorFloor(iIndex, jIndex,
                                               [&](){
                        auto ret = OperatorFloor::specialFactory(name,termOper,iIndex,jIndex,Multiplier);
                        if(ret == nullptr) ret = new OperatorDUM(0.);
                        return ret; }
                    );
                }
                if (of==0)ABORT("Parallel::operatorFloor returned nullptr");
                
                // attach time-dependent factor to floor
                if(TFac.size()>0)of->setFactor(TFac[0]);
                if(TFac.size()>1)ABORT("multiple function factors no implemented yet");
                
                definition = termFactor+termOper;
                oFloor = of;
                STOPDEBUG(operFloor)
            }
            else
            {
                const Index *iSub0=iIndex;
                const Index *jSub0=jIndex;
                OperatorDefinition opSub=termOper;
                UseMatrix mult;
                // special treatment of finite element axes
                bool iFE=(iIndex->continuity()!=Index::npos);
                bool jFE=(jIndex->continuity()!=Index::npos);
                
                // treat various types of axes as if finite elements level
                iFE=iFE or skipAxis(iIndex);
                jFE=jFE or skipAxis(jIndex);

                // hybrid level - allOnes
                if(iIndex->axisName().find("&")!=string::npos){
                    if(jIndex->axisName().find("&")==string::npos)
                        DEVABORT("hybrid axes must be on same lelve, found"+iIndex->strData()+"|"+jIndex->strData());
                    if(Name=="InvOvr" or Name=="Overlap")
                        mult= UseMatrix::Identity(iIndex->childSize(), jIndex->childSize());
                    else
                        mult= UseMatrix::Constant(iIndex->childSize(), jIndex->childSize(), 1.);
                    iSub0=iSub0->child(0);
                    jSub0=jSub0->child(0);
                }
                else if(iFE and jFE){
                    if(iIndex->axisName()!=jIndex->axisName()  and iIndex->axisName().substr(0,4) != "spec")ABORT("situation not covered");
                    bool isNonorth = termOper.find("NONORTH") != std::string::npos;
                    if(!isNonorth && not termOper.isLocal(iIndex,jIndex) and terms[0].find("[[") != 1 and terms[0] != "[[allOnes]]")
                        ABORT("using non-local operator on FE axis");
                    else if(isNonorth)
                        mult = UseMatrix::Constant(iIndex->childSize(), jIndex->childSize(), 1.);
                    else
                        mult=UseMatrix::Identity(iIndex->childSize(),jIndex->childSize());
                    iSub0=iSub0->child(0);
                    jSub0=jSub0->child(0);
                }
                else if(iFE){
                    mult=UseMatrix::Constant(iIndex->childSize(),1,1.);
                    iSub0=iSub0->child(0);
                }
                else if(jFE){
                    mult=UseMatrix::Constant(1,jIndex->childSize(),1.);
                    jSub0=jSub0->child(0);
                }
                else {
                    if(not iSub0->hasFloor())iSub0=iSub0->child(0);
                    if(not jSub0->hasFloor())jSub0=jSub0->child(0);
                    
                    // split off first factor
                    if(termOper.isStandard(iIndex,jIndex)){
                        // first try new style
                        const BasisMatMatrix* mat=BasisMatMatrix::factory(OperatorData::first(termOper),iIndex->basisAbstract(),jIndex->basisAbstract());
                        if(mat){
                            mult=mat->useMat();}
                        else if(BasisMatDependent::modify(termOper,iIndex,jIndex)!=termOper){
                            mult=UseMatrix::Constant(iIndex->childSize(),jIndex->childSize(),1.);
                            termOper=BasisMatDependent::modify(termOper,iIndex,jIndex); // replace with modified operator
                            if(iIndex->axisName()=="Eta" and termOper.find("CO2")!=string::npos){
                                const BasisExpIm * b=dynamic_cast<const BasisExpIm*>(iIndex->parent()->basisAbstract());
                                if(b==0)DEVABORT("only for BasisExpIm");
                                int lim=7;
                                PrintOutput::DEVwarning(Sstr+"//HACK: constrained to l<"+lim+"in"+termOper,1);
                                lim-=std::abs(b->mValue(iIndex->nSibling()));
                                for(int j=0;j<mult.cols();j++)
                                    for(int i=0;i<mult.rows();i++)
                                        if(i>lim or j>lim)mult(i,j)=0.;
                            }
                        }
                        else {
                            if(OperatorData::first(termOper).find("allOnes")==string::npos
                                    and iIndex->basisAbstract()->str().find("CIion")==string::npos)
                                PrintOutput::DEVwarning("old style matrix for "+OperatorData::first(termOper)
                                                        +":\n"+iIndex->basisAbstract()->str()
                                                        +"\n"+jIndex->basisAbstract()->str());
                            if(OperatorData::first(termOper).find("<{")!=string::npos)
                                ABORT("cannot evaluate - likely \""+iIndex->axisName()+"\" is not a FE-DVR axis (need at least 2 elements)");
                            complex<double>* par;
                            OperatorData::get(OperatorData::first(termOper),iIndex->basisSet(),jIndex->basisSet(),mult,par);
                        }
                    } else {
                        // we cannot make any guesses about the locality
                        int iSiz=iIndex->childSize(),jSiz=jIndex->childSize();
                        if(iIndex->hasFloor())iSiz=1;
                        if(jIndex->hasFloor())jSiz=1;
                        mult=UseMatrix::Constant(iSiz,jSiz,1.);
                    }
                    
                    // constrain mult as given in definition (speeds up the setup)
                    opSub=termOper.constrain(mult,iIndex,jIndex);

                }
                if(mult.size()==0)DEVABORT(Str("mult was left undefined in")+name+iIndex->strData()+jIndex->strData());
                
                const Index* iPar=iSub0->parent();
                const Index* jPar=jSub0->parent();
                for(int j=0;j<mult.cols();j++){
                    for(int i=0;i<mult.rows();i++){
                        if(abs(mult(i,j).complex())<epsPurge)continue;
                        childAdd(new OperatorTree(Name,opSub,iPar->child(i),jPar->child(j),Multiplier*mult(i,j),TFac,false));
                        
                        // Delete sub-trees that do not contain at least one OperatorFloor
                        // and thus are identical to zero and will be purged under all circumstances
                        if(childBack()->childSize() == 0 and childBack()->floor() == 0){
                            childErase(childSize() - 1);
                        }
                    }
                }
                
            }
        }
        if(iIndex->overlap()!=0 and iIndex!=iIndex->overlap()->iIndex)DEVABORT(Str("bad\n"));
        
    }
    if(EntryLevel){
        postProcess();
        Parameters::restoreToTime();
        LOG_POP();
    }
    
    STOPDEBUG(opAll);
    Index::build=false;
}

void OperatorTree::postProcess(){

    // find (all) channel nodes
    for (std::string kind: {"XC","Hartree"}){
        if(definition.find(kind)!=std::string::npos){
            for(OperatorTree* op=this;op!=0;op=op->nodeNext()){
                if(OperatorData::terms(op->definition).size()==1
                        and op->definition.find(kind)!=string::npos
                        and op->iIndex->axisName()=="Channel"
                        and op->iIndex->basisAbstract()==op->jIndex->basisAbstract()
                        ){
                    std::string pot=definition.substr(definition.find(kind));
                    pot=pot.substr(0,pot.find(">"));
                    pot=tools::stringInBetween(pot,"[","]");
                    if(pot==kind)pot="CoulombEE"; // default potential

                    const BasisChannel* bI=dynamic_cast<const BasisChannel*>(op->iIndex->basisAbstract());
                    const BasisOrbitalNumerical* orbs=dynamic_cast<const BasisOrbitalNumerical*>(bI->orbs());
                    if(orbs==0)DEVABORT("need BasisOrbital, got"+bI->orbs()->str());
                    if(kind=="XC")OperatorFloorXC::postProcess(this,pot,bI->rho1(),*orbs);
                    else if(kind=="Hartree")OperatorHartree::postProcess(this,bI->rho1(),*orbs);
                    else DEVABORT("define post-processing for kind="+kind);
                }
            }
        }
    }
    
    LOG_I(name<<" -- after setup: ");
    for(auto v: sizeSummary()){
        LOG(v<<"-");
    }
    LOG("floor");
    
    STARTDEBUG(operPar)
            ParallelOperator par(this);
    //        PrintOutput::DEVwarning("operator fuse disabled");
    STOPDEBUG(operPar);
    STARTDEBUG(fuseOptree);
    par.fuse();
    STOPDEBUG(fuseOptree);

    LOG_I(name<<" -- after fuse: ");
    for(auto v: sizeSummary()){
        LOG(v<<"-");
    }
    LOG("floor");
    
    STARTDEBUG(purgeOptree);
    par.purge();
    STOPDEBUG(purgeOptree);
    
    LOG_I(name<<" -- after purge: ");
    for(auto v: sizeSummary()){
        LOG(v<<"-");
    }
    LOG("floor");
    
    par.setDistribution();
    PrintOutput::progressStop();
}
static string fail;

void OperatorTree::fuse(){
    
    if(not Operator::fuseOp)return; // globally switched off
    
    // vector<double>maxNorm;
    for (unsigned int k=0;k<childSize();k++){
        if(child(k)==0)ABORT("this should not happen");
        // maxNorm.push_back(child(k)->norm());
        for(unsigned int l=childSize()-1;l>k;l--){
            // maxNorm[k]=max(maxNorm[k],child(l)->norm()); // keep track of norms before fusing
            if(child(k)->absorb(child(l)))childEraseNode(l);
        }
    }
}
void OperatorTree::fuseBottomUp(){
    if(not Operator::fuseOp)return; // globally switch off
    for(int k=0;k<childSize();k++)child(k)->fuseBottomUp();
    fuse();
}
bool OperatorTree::absorb(OperatorTree *Other){
    //HACK for now, do not absorb on ion level: possibly basis Ion has incorrect index.
    
    // only operator with equal indices, equal time-dependence, and equal tensor can be absorbed
    if(iIndex!=Other->iIndex)return false;
    if(jIndex!=Other->jIndex)return false;
    if(oFloor!=0){
        if((iIndex->isHybrid() || jIndex->isHybrid()) && (oFloor->kind()=="DUM")!=(Other->oFloor->kind()=="DUM"))
            PrintOutput::DEVwarning(Str("absorb")+"\n"+strData(-1)+iIndex->basisAbstract()->str()
                                    +"\n"+Other->strData(-1)+Other->iIndex->basisAbstract()->str()+"\n");
        if(Other->iIndex->root()->hierarchy().find("ValDer") != string::npos)return false;
        //        if(ReadInput::main.flag("DEBUGCommutator","suppress parallel of commutator"))return false;
        if(not OperatorFloor::absorb(oFloor,Other->oFloor,Str(definition,"")+Other->definition+iIndex->label()+jIndex->label()))return false;
    }
    
    if(name.find("(fused)")==string::npos)name+="(fused)";
    definition+=" "+Other->definition;
    
    for(unsigned int l=0;l<Other->childSize();l++)childAdd(Other->child(l));
    
    fuse();
    return true;
}

void OperatorTree::floorInvert(){
    
    for(OperatorTree* oLeaf=firstLeaf();oLeaf!=0;oLeaf=oLeaf->nextLeaf()){
        if (oLeaf->oFloor!=0 && dynamic_cast<OperatorDUM*>(oLeaf->oFloor) == 0){
            if(oLeaf->oFloor!=0){
                UseMatrix mat;
                oLeaf->oFloor->matrix(mat);
                if(mat.rows()!=mat.cols())ABORT(Sstr+"cannot invert floor"+oLeaf->index()+"of"+name+"at"+iIndex->strData()+jIndex->strData());
                mat=mat.inverse();
                delete oLeaf->oFloor;
                oLeaf->oFloor=OperatorFloor::factory(
                            std::vector<const UseMatrix*>(1,const_cast<const UseMatrix*>(&mat)),
                            Str(oLeaf->def())+"_inv:"+this);
            }
        }
    }
}

OperatorTree & OperatorTree::add(const OperatorTree *Term){
    
    // nothing to be added
    if(Term->isZero())
        goto Return;
    
    // add to zero
    if(isZero()){
        for(int k=0;k<Term->childSize();k++){
            childAdd(Term->child(k)->deepCopy());
            goto Return;
        }
    }
    
    if(iIndex!=Term->iIndex or jIndex!=Term->jIndex)
        ABORT("adding requires identical index blocks");
    
    if(Term->isLeaf() or isLeaf()){
        cout<<"Term\n"<<Term->str()<<endl;
        cout<<"leftn"<<str()<<endl;
        ABORT("cannot add leaf's");
    }
    
    if(!iIndex->isHybrid()) {
        if(Term->height()>height()){
            if(Term->child(0)->iIndex!=Term->iIndex or Term->child(0)->jIndex!=Term->jIndex){
                ABORT("hierarchies differ");
            }
            // insert dummy hiearchy level in present
            childAdd(new OperatorTree(name,iIndex,jIndex));
            childBack()->nodeCopy(this,false);
            for(int k=0;k<childSize()-1;k++)
                childBack()->childAdd(child(k));
            for(int k=childSize()-2;k>=0;k--)childEraseNode(k);
        }
        if(Term->height()!=height()){
            cout<<"Term1"<<str()<<endl;
            cout<<"Term2"<<Term->str()<<endl;
            ABORT("adding requires identical operator hierarchies (for now)");
        }
    }
    
    name+="+"+Term->name;
    definition+=" + ("+Term->definition+")";
    for(int k=0;k<Term->childSize();k++){
        childAdd(Term->child(k)->deepCopy());
        childBack()->name += Term->child(k)->name;
        childBack()->definition += Term->child(k)->definition;
    }
Return:
    ParallelOperator par(this);
    par.fuse();
    par.purge();
    return *this;
}

void OperatorTree::nodeCopy(const OperatorTree* other, bool View){
    iIndex = other->iIndex;
    jIndex = other->jIndex;
    oFloor = other->oFloor;
    if(not View and other->oFloor!=0){
        if     (dynamic_cast<OperatorZG*>(other->oFloor)!=0)oFloor=new OperatorZG(*dynamic_cast<OperatorZG*>(other->oFloor));
        else if(dynamic_cast<OperatorZD*>(other->oFloor)!=0)oFloor=new OperatorZD(*dynamic_cast<OperatorZD*>(other->oFloor));
        else if(dynamic_cast<OperatorZGxZG*>(other->oFloor)!=0)oFloor=new OperatorZGxZG(*dynamic_cast<OperatorZGxZG*>(other->oFloor));
        else if(dynamic_cast<OperatorZDxZG*>(other->oFloor)!=0)oFloor=new OperatorZDxZG(*dynamic_cast<OperatorZDxZG*>(other->oFloor));
        else if(dynamic_cast<OperatorZGxZD*>(other->oFloor)!=0)oFloor=new OperatorZGxZD(*dynamic_cast<OperatorZGxZD*>(other->oFloor));
        else if(dynamic_cast<OperatorZero*>(other->oFloor)!=0)oFloor=new OperatorZero(*dynamic_cast<OperatorZero*>(other->oFloor));
        else if(dynamic_cast<OperatorFloorEE*>(other->oFloor)!=0)oFloor=new OperatorFloorEE(*dynamic_cast<OperatorFloorEE*>(other->oFloor));
        else if(dynamic_cast<OperatorZGSblock*>(other->oFloor)!=0)oFloor=new OperatorZGSblock(*dynamic_cast<OperatorZGSblock*>(other->oFloor));
        else if(dynamic_cast<OperatorDUM*>(other->oFloor)!=0)oFloor=new OperatorDUM(*dynamic_cast<OperatorDUM*>(other->oFloor));
        else
            PrintOutput::DEVwarning("in OperatorTree::nodeCopy -- "
                                    +other->oFloor->kind()
                                    +" not copied, only pointed to -- may break, if one of the terms is deleted",1);
    }
}


OperatorTree::OperatorTree(const Operator* A)
    :OperatorAbstract(A->name,A->iIndex,A->jIndex),oFloor(A->oFloor),_view(true)
{
    definition=A->definition;
    // handle legacy operator
    if(A->o.size()>0){
        _view=false;
        UseMatrix mat;
        A->o[0]->matrix(mat);
        oFloor=OperatorFloor::factory(vector<const UseMatrix*>(1,&mat),Str("legacyOverlap")+iIndex->label()+"_"+jIndex->label());
    }
    if(A->tensor!=0 or A->preTensor!=0)ABORT("tensor structured operator cannot be converted to OperatorTree (yet)");
    for(int k=0;k<A->O.size();k++)childAdd(new OperatorTree(A->O[k]));
}

UseMatrix OperatorTree::matrixBlocks(unsigned int IDepth, unsigned int JDepth) const {
    // determine matrix sizes
    int idim=1,jdim=1;
    // input depth is relative to presen node, internal depth from top of tree
    IDepth+=iIndex->depth();
    JDepth+=jIndex->depth();
    const Index *idx=iIndex,*jdx=jIndex;
    while(idx->descend()!=0 and not idx->hasFloor() and idx->depth()<IDepth)idx=idx->descend();
    while(jdx->descend()!=0 and not jdx->hasFloor() and jdx->depth()<JDepth)jdx=jdx->descend();
    IDepth=idx->depth();
    JDepth=jdx->depth();
    while(0!=(idx=idx->nodeRight(iIndex)))idim++;
    while(0!=(jdx=jdx->nodeRight(jIndex)))jdim++;

    const OperatorTree* opp=this;
    while(opp->iIndex->depth()<IDepth or opp->jIndex->depth()<JDepth)opp=opp->descend();

    UseMatrix mat=UseMatrix::Constant(idim,jdim,0.);
    for(const OperatorTree* b=this;b!=0;b=b->nodeNext(this))
        if(b->iIndex->depth()==IDepth and b->jIndex->depth()==JDepth){
            int i=b->iIndex->levelRank(iIndex->parent()),j=b->jIndex->levelRank(jIndex->parent());
            mat(i,j)=max(mat(i,j).real(),b->norm());
        }
    return mat;
}

void OperatorTree::matrix(UseMatrix &Mat) const{
    if(isHuge())return;
    if(Mat.size()==0)
        Mat=UseMatrix::Zero(iIndex->sizeStored(),jIndex->sizeStored());
    if(isLeaf())
        OperatorAbstract::matrix(Mat);
    else{
        for(int k=0;k<childSize();k++){
            UseMatrix mat;
            child(k)->matrix(mat);
            Mat.block(child(k)->iIndex->posIndex(iIndex),child(k)->jIndex->posIndex(jIndex),mat.rows(),mat.cols())+=mat;
        }
    }
}

Eigen::MatrixXcd  & OperatorTree::matrix(Eigen::MatrixXcd & Mat, const Index* ISub, const Index* JSub) const {
    if(isHuge())return Mat;
    if(ISub==0)ISub=iIndex;
    if(JSub==0)JSub=jIndex;
    if(Mat.rows()!=ISub->sizeStored() and Mat.cols()!=JSub->sizeStored()){
        if(Mat.size()!=0)DEVABORT("entering with non-matching matrix");
        Mat=Eigen::MatrixXcd::Zero(ISub->sizeStored(),JSub->sizeStored());
    }
    if(oFloor!=0){
        Eigen::MatrixXcd mat=oFloor->matrix();
        Mat.block(iIndex->posIndex(ISub),jIndex->posIndex(JSub),mat.rows(),mat.cols())+=mat;
    }
    else {
        for(int k=0;k<childSize();k++){
            // descend of sub-index is in current or current is part of subtree
            if(     (child(k)->iIndex->isSubtree(ISub) or ISub->isSubtree(child(k)->iIndex) ) and
                    (child(k)->jIndex->isSubtree(JSub) or JSub->isSubtree(child(k)->jIndex) ) )
                child(k)->matrix(Mat,ISub,JSub);
        }
    }
    return Mat;
}

void OperatorTree::matrixContracted(UseMatrix &Mat) const{
    // entry level - define the numbering and create matrix
    vector<unsigned int> iCont=iIndex->contractedNumbering();
    vector<unsigned int> jCont=jIndex->contractedNumbering();
    // last index is largest
    Mat=UseMatrix::Zero(iCont.back()+1,jCont.back()+1);
    _matrixContracted(Mat,iIndex,jIndex,iCont,jCont);
}

TIMER(contractM,)
TIMER(cMat,)
void OperatorTree::_matrixContracted(UseMatrix &Mat, const Index *IRoot, const Index *JRoot, std::vector<unsigned int> ICont, std::vector<unsigned int> JCont) const
{
    if(iIndex->continuity()!=Index::npos or jIndex->continuity()!=Index::npos){
        // continuity()!=Index::npos indicates that a level is to be contracted
        UseMatrix mat,cmat;
        STARTDEBUG(contractM);
        matrix(mat);
        STOPDEBUG(contractM);
        STARTDEBUG(cMat);
        matrixContract(mat,cmat);
        STOPDEBUG(cMat);
        Mat.block(ICont[iIndex->posIndex(IRoot)],JCont[jIndex->posIndex(JRoot)],cmat.rows(),cmat.cols())=cmat;
    }
    else
        for(int k=0;k<childSize();k++){
            child(k)->_matrixContracted(Mat,IRoot,JRoot,ICont,JCont);
        }
}

OperatorTree::OperatorTree(const OperatorAbstract *A, const Index* IIndex , const Index* JIndex)
    :OperatorAbstract(A->name,IIndex,JIndex),oFloor(0),_view(false)
{
    
    if(iIndex==0)iIndex=A->iIndex;
    if(jIndex==0)jIndex=A->jIndex;
    
    if(not jIndex->hasFloor()){
        for(int k=0;k<jIndex->childSize();k++){
            childAdd(new OperatorTree(A,iIndex,jIndex->child(k)));
            if(childBack()->isZero())childPop();
        }
    }
    
    else {
        if(iIndex==A->iIndex){
            // get matrix columns
            //            vector<complex<double> > colStor;
            colStor.clear();
            A->subMatrix(colStor,iIndex,jIndex);
            colMat=new UseMatrix(UseMatrix::UseMap(colStor.data(),iIndex->sizeStored(),jIndex->sizeStored()));
        }
        if(iIndex->hasFloor()){
            // place block into floor
            UseMatrix fMat;
            fMat=colMat->block(iIndex->posIndex(A->iIndex),0,iIndex->sizeStored(),jIndex->sizeStored());
            oFloor=OperatorFloor::factory(vector<const UseMatrix*>(1,&fMat.purge()),A->name);
        }
        else {
            for(int k=0;k<iIndex->childSize();k++){
                childAdd(new OperatorTree(A,iIndex->child(k),jIndex));
                if(childBack()->isZero())childPop();
            }
        }
        if(iIndex==A->iIndex){
            delete colMat;
        }
    }
    
}

static int cnt=0;
void OperatorTree::apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const {
    if(&Vec==&Y)DEVABORT("Vec and Y must not be the same vector");
    int host=ParallelOperator::getHost(this);
    
    if(B!=0.){
        bool crit=Coefficients::timeCritical;
        Coefficients::timeCritical=false;
        *tempLHS()=Y;
        Coefficients::timeCritical=crit;
    }
    // apply only if overall, distributed or local to thread
    if(        host==ParallelOperator::distributed
               or host==ParallelOperator::all
               or host==MPIwrapper::Rank()
               ){
        _apply(A,Vec,0.,Y);
    }
    
    if(host==ParallelOperator::distributed and MPIwrapper::Size()>1){
        Y.treeOrderStorage();
        // HACK: in Hybrid Disc, if Y is Ionic coefficient, it will still yield Y.data() == 0, because
        // the contiguous floor data is put at the Hybrid hierarchy level
        // --> needs more thoughtful fix!
        if(Y.data() == 0)
            MPIwrapper::AllreduceSUM(const_cast<Coefficients*>(Y.parent())->data() + 1,Y.parent()->size() - 1);
        else {
            MPIwrapper::AllreduceSUM(Y.data(),Y.size());
        }
    }
    if(B!=0.)Y.axpy(B,tempLHS());
}

void OperatorTree::_apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    Y*=B;
    if(iIndex!=Y.idx())ABORT("lhs index does not match: oper\n"+iIndex->str()+"output\n"+Y.idx()->str());
    if(jIndex!=Vec.idx() and not jIndex->hasFloor())DEVABORT("rhs index does not match: \nOper\n"+jIndex->str()+"\nVec\n"+Vec.idx()->str());
    if(floor()!=0 and floor()->kind()!="DUM"){
        Coefficients* jVec=const_cast<Coefficients*>(&Vec); // not nice...
        oFloor->apply(A,jVec->data(),Vec.size(),1.,Y.data(),Y.size());
        return;
    }
    
    
    for (int n=0;n<childSize();n++){
        const Coefficients* jVec=&Vec;
        Coefficients* iVec = &Y;
        
        if(child(n)->iIndex!=iIndex)iVec=iVec->child(child(n)->iIndex->nSibling());
        if(child(n)->jIndex!=jIndex and not jIndex->hasFloor())jVec=jVec->child(child(n)->jIndex->nSibling());
        child(n)->_apply(A,*jVec,1.,*iVec);
    }
}

void OperatorTree::applyTranspose(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    Y*=B;
    if(jIndex!=Y.idx())ABORT("rhs index does not match input Vector");
    if(iIndex!=Vec.idx())ABORT("lhs index does not match input Vector");
    if(oFloor!=0){
        Coefficients* jVec=const_cast<Coefficients*>(&Vec); // not nice...
        vector<complex<double> > vec(jVec->data(),jVec->data()+Vec.size());
        vector<complex<double> > y(Y.data(),Y.data()+Y.size());
        ABORT("re-implement this");
        //        oFloor->axpyTranspose(A,vec,1.,y);
        for(int k=0;k<Y.size();k++)Y.data()[k]=y[k];
        return;
    }
    
    for (int n=0;n<childSize();n++){
        Coefficients* jVec=&Y;
        const Coefficients* iVec = &Vec;
        if(child(n)->iIndex!=iIndex)iVec=iVec->child(child(n)->iIndex->nSibling());
        if(child(n)->jIndex!=jIndex)jVec=jVec->child(child(n)->jIndex->nSibling());
        child(n)->applyTranspose(A,*iVec,1.,*jVec);
    }
}

bool OperatorTree::isDiagonal() const {
    
    if(isLeaf())
        return floor()==0 or (iIndex==jIndex and floor()->isDiagonal());
    
    if(not isBlockDiagonal())
        return false;
    
    for(int k=0;k<childSize();k++)
        if(not child(k)->isDiagonal())
            return false;
    
    return true;
}

bool OperatorTree::isBlockDiagonal() const {
    if(isLeaf())return false;
    for(int k=0;k<childSize();k++)
        if(child(k)->iIndex!=child(k)->jIndex)return false;
    return iIndex->continuity()==Index::npos;
}

bool OperatorTree::isZero(double Eps) const {
    if(iIndex->sizeStored()==0)return true;
    if(jIndex->sizeStored()==0)return true;
    for(int k=0;k<childSize();k++)
        if(not child(k)->isZero(Eps))return false;
    if(oFloor==0 or oFloor->norm()==0.)return true;  // no child > eps and there is no floor
    return oFloor->norm()<=Eps;
}

Coefficients  OperatorTree::diagonal(bool OnlyForDiagonalOperator) const{
    if(OnlyForDiagonalOperator and iIndex!=jIndex)ABORT("operator is not  block diagonal");
    
    Coefficients res(iIndex,0.);
    if(childSize()>0 and child(0)->iIndex==iIndex){
        // in case there are multiple diagonal blocks...
        res=child(0)->diagonal(OnlyForDiagonalOperator);
        for(int k=1;k<childSize();k++)res+=child(0)->diagonal(OnlyForDiagonalOperator);
    }
    
    else {
        if(oFloor!=0){
            if(floor()->cols()>0){
                // place diagonal into coefficients
                UseMatrix mat;
                oFloor->matrix(mat);
                
                if(OnlyForDiagonalOperator and not mat.isDiagonal(1.e-12)){
                    mat.print(Str("mat\n")+iIndex->str()+jIndex->str(),2);
                    ABORT("operator floor is not diagonal");
                }
                // not fast, but save
                for(int k=0;k<min(iIndex->sizeStored(),jIndex->sizeStored());k++)
                    res.data()[k]=mat(k,k).complex();
            }
            if(ParallelOperator::getHost(this)==ParallelOperator::distributed)
                MPIwrapper::AllreduceSUM(res.data(),res.size());
        }
        for(int k=0;k<childSize();k++){
            if(child(k)->iIndex==child(k)->jIndex)
                *res.child(child(k)->iIndex->nSibling())+=child(k)->diagonal(OnlyForDiagonalOperator);
            else
                if(OnlyForDiagonalOperator)ABORT("operator is not block-diagonal");
        }
    }
    return res;
}

double OperatorTree::norm() const {
    if(iIndex->sizeStored()==0)return 0.;
    if(jIndex->sizeStored()==0)return 0.;
    
    double nrm;
    if(oFloor!=0)
        nrm=oFloor->norm();
    else {
        nrm=0.;
        for(int k=0;k<childSize();k++)nrm=max(nrm,child(k)->norm());
    }
    return nrm;
}

void OperatorTree::purge(double Eps){
    for(int k=childSize();k>0;k--){
        child(k-1)->purge(Eps);
        if(child(k-1)->isZero(Eps))childErase(k-1);
    }
}

void OperatorTree::purge(purgeCriterion Crit){
    for(int k=childSize();k>0;k--){
        child(k-1)->purge(Crit);
        if((*Crit)(child(k-1)))childErase(k-1);
    }
}

const OperatorFloor* OperatorTree::floor() const{
    return const_cast<const OperatorFloor*>(oFloor);
}

OperatorFloor*& OperatorTree::floor() {
    return oFloor;
}

std::string OperatorTree::strData(int Digits) const {
    Str s("","");
    if(parent()==0)s=s+name+"\n";
    if(MPIwrapper::Size()>1)s=s+" <"+ParallelOperator::strHost(this)+">";
    if(oFloor!=0){
        Str proc("","");
        if(Digits<0)s=s+" F("+oFloor->kind()+")"+proc;
        else        s=s+proc+"\n (F) "+oFloor->str(Digits);
    }
    s=s+" "+tools::str(&*this)+": "+tools::str(norm());
    s=s+" <"+iIndex->index()+"|"+jIndex->index()+"> ("+iIndex->sizeCompute()+"x"+jIndex->sizeCompute()+") ["+childSize()+"] "+iIndex->axisName();
    if(iIndex->axisName()!=jIndex->axisName())s+"-"+jIndex->axisName();
    if(definition.length()>60)s+": "+definition.substr(0,60)+"...";
    else s=s+": "+definition;
    s=s+" ptr: "+iIndex+" | "+jIndex;
    return s;
}

long OperatorTree::applyCount() const{
    if(oFloor!=0) return oFloor->applyCount();
    
    long result=0;
    for(unsigned int i=0; i<childSize(); i++){
        result+=child(i)->applyCount();
    }
    
    return result;
}

OperatorTree* OperatorTree::testOp(int Dim, string File){
    if(File=="")File=BasicDisc::generateTestInputs(0,0,Dim);
    const BasicDisc * D=new BasicDisc(File);
    
    string def="0.5<<Laplacian>>+0.5<<Parabolic>>";
    return new OperatorTree("testOp",def,D->idx(),D->idx());
    
}

size_t OperatorTree::diagnoseSizeOfNode() const {
    size_t siz=0,sizAll=0;
    if(iIndex->parent()==0)
        PrintOutput::DEVmessage(Str("OperatorTree sizes: ")+memoryUsage["def"]+":"+sizAll);
    return sizAll;
}

void OperatorTree::updateNonLin(double Time, const Coefficients *C){
    if(floor()!=0 and floor()->kind()!="DUM"){
        OperatorFloorNonLin* fNL=dynamic_cast<OperatorFloorNonLin*>(floor());
        if(fNL)fNL->updateNonLin(time, C);
        return;
    }
    for (int n=0;n<childSize();n++){
        child(n)->updateNonLin(Time, C);
    }
}


