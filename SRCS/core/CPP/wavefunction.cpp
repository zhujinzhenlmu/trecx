// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "wavefunction.h"

//resolve forward declarations
#include "threads.h"
#include "coefficients.h"
#include "discretization.h"
#include "discretizationDerived.h"
#include "operator.h"
#include "discretizationGrid.h"
#include "index.h"

using namespace std;
using namespace tools;

Wavefunction::Wavefunction(const Discretization* D, double Time) :time(Time),_map(false){coefs=new Coefficients(D->idx());}
Wavefunction::Wavefunction(double Time, Coefficients *C):time(Time),_map(false){coefs=new Coefficients(*C);}
Wavefunction::Wavefunction(const Index* Idx, double Time):time(Time),_map(false){coefs=new Coefficients(Idx);}

Wavefunction::~Wavefunction(){if(not _map)delete coefs;}


Wavefunction& Wavefunction::operator=(const Wavefunction& rhs)
{
    if(&rhs==this)return *this;

    if(coefs==0){
        coefs=new Coefficients(*rhs.coefs);
        coefs->treeOrderStorage();
    }
    else{
        (*coefs)=(*(rhs.coefs));
    }
    time = rhs.time;
    return *this;
}

Wavefunction::Wavefunction(const Wavefunction& rhs)
{
    coefs=new Coefficients(*rhs.coefs);
    coefs->treeOrderStorage();
    time=rhs.time;
}

const Wavefunction Wavefunction::map(double Time, const Coefficients* C)
{
    Wavefunction wf;
    (&wf)->time=Time;
    (&wf)->coefs=const_cast<Coefficients*>(C);
    return wf;
}

Wavefunction& Wavefunction::operator+=(const Wavefunction& rhs)
{
    (*coefs)+=(*(rhs.coefs));
    return *this;
}

Wavefunction& Wavefunction::operator-=(const Wavefunction& rhs)
{
    (*coefs) -= (*(rhs.coefs));
    return *this;
}

Wavefunction& Wavefunction::operator*=(std::complex< double > x)
{
    (*coefs) *= x;
    return *this;
}

void Wavefunction::setToZero() {
    coefs->setToZero();
}

void Wavefunction::setToConstant(complex<double> c){
    coefs->setToConstant(c);
}


void Wavefunction::setToRandom(int seed){
    if(seed!=0)srand48(seed);
    coefs->setToRandom();
}

void Wavefunction::makeContinuous() {
    coefs->makeContinuous();
}
void Wavefunction::show(int Digits){cout<<coefs->str(Digits)<<endl;}

/// header: reposition file to beginning and write header information
void Wavefunction::write(ofstream &stream, bool header) const{
    coefs->write(stream,header);
    tools::write(stream,time);
    stream.flush(); // this is important, as else the outfile may remain dangling
}
/// header: reposition file to beginning and write header information
void Wavefunction::print(ofstream &stream, string Header) const {
    if(MPIwrapper::isMaster(MPIwrapper::worldCommunicator())){
        if(Header!=""){
            stream <<"# "+Header<<endl;
            stream <<"# time abs(0), arg(0), ..."<<endl;
        }
        stream<<time;
    }
    coefs->print(stream);
}
/// header: reposition file to beginning and read header information
void Wavefunction::read(ifstream &stream,bool header){
    ///< header: read from beginning of file, including structural information
    int posFile=int(stream.tellg());
    coefs->read(stream,header);
    tools::read(stream,time);
}

double Wavefunction::readLastTime(ifstream &Inp){
    ABORT("havn't gotten this to work...");
    if(not Inp.is_open())ABORT("not a good input stream");
    double t=-DBL_MAX;
    Inp.seekg (8,Inp.end);
    cout<<" "<<t<<" "<<Inp.tellg()<<endl;

    tools::read(Inp,t);
    return t;
}

double Wavefunction::maxCoeff()
{
    return coefs->maxCoeff();
}
