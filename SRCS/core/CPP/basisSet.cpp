// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply.
//
// See terms of use in the LICENSE file included with the source distribution
//
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "basisSet.h"
#include <memory>
#include <cfloat>  // floating point limits
#include "qtEigenDense.h"
#include "coordinate.h"
#include "complexScaling.h"
#include "axis.h"
#include "printOutput.h"
#include "readInput.h"
#include "index.h"
#include "basisMat.h"
#include "basisNdim.h"
#include "basisMat1D.h"


#include "basisDvr.h"
#include "basisGridQuad.h"

using namespace std;
using namespace tools;

int BasisSet::nextLabel=1; ///< lable=0 is reserved for non-basis

// temporary solutions until basis-set magement is revised
map<unsigned int,BasisSet*> BasisSet::dummyBasis;
deque<BasisSet> BasisSet::gridBasis=deque<BasisSet>(0);
deque<BasisSet> BasisSet::genBasis=deque<BasisSet>(0);

BasisSet * BasisSet::getDummy(unsigned int Order){
    if(Order<1)ABORT("cannot create zero size dummy basis");
    if(dummyBasis.count(Order)==0)dummyBasis[Order]=new BasisSet(Order);
    return dummyBasis[Order];
}

BasisSet * BasisSet::getRightIn(BasisSetDef Def){
    BasisSetDef def(Def);
    def.funcs="rightInFlux";
    def.order=2;
    def.margin[0]=0;
    def.margin[1]=1;
    // check for previous occurance
    for(unsigned int i=0;i<genBasis.size();i++)
        if(genBasis[i].def==def)
            return &genBasis[i];
    genBasis.push_back(BasisSet(def));
    return &(genBasis.back());
}

//BasisSet * BasisSet::getGrid(const Coordinate Coor, const UseMatrix Points, const UseMatrix Weights){
//    for(int i=0; i<gridBasis.size(); i++){
//        if(gridBasis[i].def.coor.name() == Coor.name() and
//                gridBasis[i].Points() == Points and gridBasis[i].Weights() == Weights)
//            return &(gridBasis[i]);
//    }
//    gridBasis.push_back(BasisSet(Coor,Points,Weights));
//    return &(gridBasis.back());
//}

//NOTE: eventually, Def and this function should be moved to BasisAbstract
const BasisAbstract * BasisSet::getAbstract(BasisSetDef Def){
    const BasisAbstract* b=BasisAbstract::factory(Def);
    if(b!=0)return b;
    return get(Def);
}

BasisSet * BasisSet::get(BasisSetDef Def){
    // check for previous occurance
    for(unsigned int i=0;i<genBasis.size();i++)
        if(genBasis[i].def==Def)return &genBasis[i];
    genBasis.push_back(BasisSet(Def));
    return &(genBasis.back());
}

BasisSet::BasisSet(const BasisSet & other):BasisIntegrable(other.lowBound(),other.upBound()){
    def=other.def;
    fs=other.fs;
    _parameters=other._parameters;
    upB=other.upB;
    lowB=other.lowB;
    trans=other.trans;
    points=other.points;
    weights=other.weights;
    fdOrder=other.fdOrder;
    _jac=other._jac;
    _comSca=other._comSca;

    label=BasisSet::nextLabel;
    BasisSet::nextLabel++;
}

BasisSet::~BasisSet(){}
BasisSet & BasisSet::operator=(const BasisSet & other){
    if(this!=&other){
        def=other.def;
        fs=other.fs;
        upB=other.upB;
        lowB=other.lowB;
        trans=other.trans;
        points=other.points;
        weights=other.weights;
        fdOrder=other.fdOrder;
        _jac=other._jac;
        _comSca=other._comSca;

        label=BasisSet::nextLabel;
        BasisSet::nextLabel++;
    }
    return *this;
}

bool BasisSet::operator==(const BasisAbstract & Other) const {

    if(this==&Other)return true;
    const BasisSet* other=dynamic_cast<const BasisSet*>(&Other);
    if(other==0)return Other==*this;

    if(isGrid() and other->isGrid()){
        if(points !=other->points) return false;
        if(weights!=other->weights) return false;
        return true;
    }
    if(trans.cols()!=other->trans.cols())return false;
    if(trans.rows()!=other->trans.rows())return false;
    if(fdOrder!=other->fdOrder) return false;
    if(not (def==other->def)) return false;
    return true;
}

BasisSet::BasisSet():BasisIntegrable(0.,0.),upB(-DBL_MAX),lowB(DBL_MAX),fdOrder(0){_name="BasisSet";label=nextLabel;nextLabel++;trans=UseMatrix();}

BasisSet::BasisSet(const Coordinate &Coor, const UseMatrix &Points, const UseMatrix &Weights)
    :BasisIntegrable(Points(0).real(),Points(Points.size()-1).real()),
      points(Points),weights(Weights),fdOrder(0),fs(0)
{
    _name="BasisSet";
    def=BasisSetDef();
    lowB=0.;
    upB=1.;
    fdOrder=0;
    vector<double> p,w;
    for(unsigned int i=0;i<Points.size();i++){
        p.push_back(Points(i).real());
        w.push_back(Weights(i).real());
    }
    fs=new BasisFunctionGrid(p,w);
    _parameters = fs->par;
    trans=UseMatrix::Identity(points.size(),points.size());
    def.coor=Coor;
    def.funcs="grid";
    def.exactIntegral = true;
    def.order=points.size();
    if(points.size()>0){
        def.shift=points(0).real();
        def.scale=points(points.size()-1).real()-def.shift;
    }
    if(points.size()>0){
        // supply weights, if needed
        if(weights.size()==0){
            // for deducing weights, grid must be strictly incrementing
            for (unsigned int i=1;i<points.size();i++)
                if(points(i).real()<=points(i-1).real())ABORT("give strictly incrementing grid or supply weights");
            weights=UseMatrix(points.size(),1);
            if(points.size()>1){
                weights(0)=1./(points(1)-points(0));
                weights(weights.size()-1)=1./(points(points.size()-1)-points(size()-2));
                for (unsigned int i=1;i<points.size()-1;i++)weights(i)=2./(points(i+1)-points(i-1));
            } else {
                weights(0)=1.;
            }
        }
        lowB=Points(0).real();
        upB=Points(Points.size()-1).real();
    }
    if(weights.size()!=points.size())ABORT("number of quadrature points does not match number of grid points");

    nextLabel++;
    if(isGrid())_name="grid";
}

BasisSet::BasisSet(const BasisSetDef &Def )
    :BasisIntegrable(Def.lowBound(),Def.upBound()),fdOrder(0),def(Def),label(nextLabel)
    // NOTE: initializer list needs to be in the exact sequence of definition of variables
    //       otherwise compiler warns about "reordering"
    // Deriv=true: 1st and next to last have value=1, 2nd and last have derivative=1
{
    if(Def.funcs.find("cosSin")!=string::npos)DEVABORT("replace with BasisCosSin");
    _name="BasisSet";
    upB=Def.upBound();
    lowB=Def.lowBound();
    UseMatrix funOvr;

    unsigned int defOrder=Def.order;
    fs=BasisFunction::get(Def.funcs,defOrder,Def.par);
    _parameters=fs->par;

    trans = UseMatrix::Identity(defOrder, defOrder);

    // special case: (equidistant) grid
    if(isGrid()) {
        //        if(Def.par.size()!=2)ABORT("need exactly 2 parameters: finDiff order and exponent");
        points =UseMatrix(defOrder,1);
        for(unsigned int i=0;i<points.size();i++)
            points(i)=Def.shift+(i+1)*Def.scale/double(defOrder+1);
        weights=UseMatrix::Constant(defOrder,1,1.);
        return;
    }

    if(not isIndex()){
        _jac=std::shared_ptr<Jacobian>(new Jacobian1(eta()));
        _comSca=std::shared_ptr<ComplexScaling>(new ComplexScaling(def.comSca));
    }
    // special case: useIndex and rightIn (trans = Identity)
    if (isIndex() or fs->name()=="incoming"){
        funOvr = UseMatrix::Identity(defOrder, defOrder);
    } else {
        // create Overlap Matrix
        BasisMat1D ovr;
        if(not (ovr=BasisMat1D("<1>",this,this)).isEmpty()){
            funOvr=ovr.useMat();
        } else {
            vector<string> op(1,"1"),par(1,"");
            BasisMat::get(op,vector<const BasisSet*>(1,this),vector<const BasisSet*>(1,this),funOvr,par);
        }
        if(jacobian(2.)!=1. and imag(eta())!=0.)ABORT("complex scaling only for Jacobian = 1");
        if(Def.funcs.find("assoc")!=string::npos and not funOvr.isDiagonal(1.e-11)){
            funOvr.print();
            ABORT("overlap not identity\n"+str());
        }
        funOvr/=eta(); // (valid only for Jacobian=1): undo complex scaling
        funOvr.purge();
    }
    construct(defOrder,def.funcs,def.first,def.last,def.coor,def.margin,def.deriv,funOvr);
    if(ReadInput::main.flag("DEBUGplotBasis","plot all basis functions into run output directory"))plot(ReadInput::main.output());

    // constrain lower orders
    if(Def.minOrder>0){
        if(not (Def.first and Def.last))ABORT("cannot set minimal order on fem-coordinated: "+str());
        UseMatrix red;
        red=trans.block(0,Def.minOrder,trans.rows(),trans.cols()-Def.minOrder);
        trans.swap(red);
    }

}

BasisSet::BasisSet(unsigned int Order)
    :BasisIntegrable(0.,max(double(Order)-1.,1.)),fdOrder(0),fs(new BasisFunctionIndex(Order)),label(nextLabel){
    _name="BasisSet";
    def.funcs="useIndex";
    def.order=Order;
    _parameters=fs->par;
    lowB=0;
    upB=max(Order,(unsigned int)(1))-1;
    construct(Order,def.funcs,true,true,Coordinate::fromString("dum"),vector<int>(0),false,UseMatrix());
}

void BasisSet::construct(unsigned int Order, std::string Function, const bool First, const bool Last,
                         const Coordinate & Coor, vector<int> Margin, bool Deriv, const UseMatrix & funOvr){
    if(Order==0)return; // emtpy basis
    // initial set with dummy transform, get fundamental overlap matrix
    nextLabel++; // increment label

    // for complex scaled elements, strictly real functions must be used (else, complex symmetry is lost)
    if(def.comSca.eta!=1.){
        UseMatrix vals=fs->val(UseMatrix::RandomReal(100,1));
        if(not vals.isReal(0.))ABORT("cannot use complex functions on complex scaled element: "+def.str());
    }

    if(not funOvr.isReal()){
        funOvr.print("overlap "+str());
        funOvr.show("overlap "+str());
        ABORT("fundamental overlap not real: "+str());
    }

    // default margins 0,1
    if(Margin.size()==0){
        Margin.push_back(0);
        Margin.push_back(1);
    }

    trans = UseMatrix::Identity(Order, Order)/sqrt(abs(def.scale)); // default is to stretch to current interval

    // special cases: unstransformed functions (trans = Identity)
    //HACK:
    if(fs->name()=="incoming"){
        trans=UseMatrix::Identity(2,2);
        trans(1,1)=def.upBound()-def.lowBound();
    }
    if(isIndex() or Function=="incoming" or isCIstate())return;

    // make sure scaling is not inside interval
    double eps=(def.upBound()-def.lowBound())*1.e-12;
    if((def.lowBound()+eps<def.comSca._r0lower and def.comSca._r0lower<def.upBound()-eps) or
            (def.lowBound()+eps<def.comSca._r0upper and def.comSca._r0upper<def.upBound()-eps))
        ABORT("border of complex region must not be inside element: "
              +tools::str(def.lowBound())+" "+tools::str(def.upBound())+" "
              +tools::str(def.comSca._r0lower)+" "+tools::str(def.comSca._r0upper));

    if(BasisMat::femDVR and not def.exactIntegral){
        if(not funOvr.isDiagonal(1.e-12)){
            funOvr.print("overlap: "+str());
            ABORT("in femDVR, overlap must be diagonal");
        }
        if(fs->name().find("DVR")!=0 and fs->name().find("sqrt*DVR")!=0)DEVABORT("cannot use femDVR with this basis: "+fs->name());
    };

    // single element and no boundary conditions, no need to get a transformation matrix
    if(def.first and def.last){
        if(not Coor.zeroLow and not Coor.zeroUp)return;
    }

    // 2 x Order matrix of (untransformed) boundary values
    UseMatrix coor(2,1);
    coor(0,0)=def.shift;
    if(BasisMat::femDVR){
        UseMatrix q,w;
        fs->dvrRule(q,w);
        coor(1,0)=def.shift+q(Order-1).real()*def.scale;
    } else {
        coor(1,0)=def.shift+def.scale;
    }
    // negative interval reverses left/right end points
    if(def.scale<0){
        complex<double> x=coor(1,0).complex();
        coor(1,0)=coor(0,0);
        coor(0,0)=x;
    }

    if(fs->leftZero() or fs->rightZero()){
        if(not def.first and not def.last)
            ABORT("zero boundary value functions on element that is neither first nor last: "+str());
    }

    unsigned int cond=2;
    if(Deriv)cond=4; // match values and derivatives
    if(Order<cond){
        DEVABORT("minimum number of functions on element="+tools::str((int) cond)+", is: "+tools::str(Order)+" coordinate: "+Coor.cString);
    }
    UseMatrix boundary(cond,Order);
    eps=1.e-14;
    int count=0;
    while (not check(coor,funOvr,eps,cond)) {
        count++;
        // gradually relax accuracy goals
        if(count%3==0) {
            eps*=1.e1;
            if(eps>1.e-10)
                PrintOutput::warning("relaxed accuracy for basis overlap eps="+tools::str(eps)+" - approaching ill-conditioning: "+str());
        }
        if(count>20){
            PrintOutput::warning("basis setup failed with tolerance "+tools::str(eps));
            (trans.transpose()*funOvr*trans).show("Overlap");
            ABORT("setup failed with tolerance="+tools::str(eps)+" for "+str());
        }

        // transformation matrix to standard boundaries
        UseMatrix vals,ders;
        valDer(coor,vals,ders);
        if(cond==2){
            boundary.row(0)=vals.row(0);
            boundary.row(1)=vals.row(1);
        } else {
            if(BasisMat::femDVR)ABORT("continous derivatives not implmeneted for femDVR");
            boundary.row(0)=vals.row(0);
            boundary.row(1)=vals.row(1);
            boundary.row(2)=ders.row(0);
            boundary.row(3)=ders.row(1);
        }

        UseMatrix cinv,binv,tran0=UseMatrix::Identity(Order,Order);
        cinv=boundary.block(0,0,cond,cond);
        binv=cinv.inverse();
        tran0.block(0,0,cond,cond)=binv;
        if(Order>cond){
            tran0.block(cond,cond,Order-cond,Order-cond)=UseMatrix::Identity(Order-cond,Order-cond);
            tran0.block(0,cond,cond,Order-cond)=(binv*boundary.block(0,cond,cond,Order-cond));
            tran0.block(0,cond,cond,Order-cond)*=-1.;
        }
        //        trans*=tran0; // update transformation
        Eigen::Map<Eigen::MatrixXcd>(trans.data(),trans.rows(),trans.cols())*=Eigen::Map<Eigen::MatrixXcd>(tran0.data(),tran0.rows(),tran0.cols());

        UseMatrix rightcols;
        if(cond==2){ //HACK
            // Gram-Schmidt orthonormalize proper subset
            for (int j=(int)(Order-1);j>-1;j--){
                if(min(Order-cond,Order-cond+1-j)>0){
                    rightcols=trans.rightCols(min(Order-cond,Order-cond+1-j));
                    trans.col(j)-=rightcols*(rightcols.transpose()*funOvr*trans.col(j));
                }
                complex<double> norm=sqrt(trans.col(j).dot(funOvr*trans.col(j)));
                if(abs(norm)<1.e-12*abs(funOvr(j,j).complex())){
                    funOvr.print("overlap");
                    plot(ReadInput::main.output()+"Basis:singular");
                    trans=UseMatrix::Identity(trans.rows(),trans.cols());
                    plot(ReadInput::main.output()+"Functions:singular");
                    ABORT("singular basis "+str()+"(also see Basis:singular and Functions:singular)");
                }
                if(j>(int)(cond-1))(trans.col(j))/=norm;
            }
        }
    } // while (not check(funOvr,eps,cond))

    // orthogonalize completely if no lower or upper boundary is needed
    if (def.first and (not def.coor.zeroLow or fs->asympZero()))
        trans.col(1)-=trans.col(0)*(trans.col(0).transpose()*funOvr*trans.col(1))/(trans.col(0).dot(funOvr*trans.col(0)));
    if (def.last and (not def.coor.zeroUp or fs->asympZero()))
        trans.col(0)-=trans.col(1)*(trans.col(1).transpose()*funOvr*trans.col(0))/(trans.col(1).dot(funOvr*trans.col(1)));

    // in DVR, we want to normalize all non-margin functions to 1
    if(BasisMat::femDVR){
        if(def.first)trans.col(0)/=sqrt(trans.col(0).dot(funOvr*trans.col(0)));
        if(def.last )trans.col(1)/=sqrt(trans.col(1).dot(funOvr*trans.col(1)));
    }

    // sort margins
    vector<int> marg(Margin);
    if(Deriv)marg[1]--;

    UseMatrix tran0(UseMatrix::Zero(Order,Order)); // zero matrix
    tran0.col(marg[0])=trans.col(0);
    tran0.col(marg[1])=trans.col(1);
    if(Deriv){
        tran0.col(marg[0]+1)=trans.col(2);
        tran0.col(marg[1]+1)=trans.col(3);
    }
    int nextAvailable=0;
    for (unsigned int j=cond;j<Order;j++){
        while(not tran0.col(nextAvailable).isZero(0.))nextAvailable++;
        tran0.col(nextAvailable)=trans.col(j);
    }

    // keep boundary function if no boundary or val(+infty)= 0
    if(Margin[0]>Margin[1])ABORT("lower margin index must not be larger than upper");
    int k=0;
    for (int j=0; j<(int)tran0.cols();j++){
        if(j==Margin[0]
                and First
                and Coor.zeroLow
                and not (def.scale<0 and fs->asympZero())
                )continue; // omit lower boundary function
        if(j==Margin[1]
                and Last
                and Coor.zeroUp
                and not (def.scale>0 and fs->asympZero())
                )continue; // omit upper boundary function
        trans.col(k)=tran0.col(j);
        k++;
    }
    tran0=trans.leftCols(k);
    trans=tran0;

    if(Deriv){
        if(def.comSca.kind=="ECS" or def.comSca.kind=="modECS"){
            trans.col(1)*=eta();
            trans.col(trans.cols()-1)*=eta();
        } else if(def.comSca.kind=="modPML" or def.comSca.kind=="PML") { /* do nothing here! */}
        else if(def.comSca.kind=="PML") { /* do nothing here! */ }
        else if(def.comSca.kind=="UNSCALED") { /* do nothing here! */ }
        else ABORT("not defined for absorption kind="+def.comSca.kind);
    }

    // useful for checking basis costruction (every once in a while)
    // plot(ReadInput::main.output());
}

void BasisSet::plot(string Dir) const{
    UseMatrix q(201,1),val,der;
    double scal=def.scale;
    if(fs->leftZero() or fs->rightZero())scal*=20;
    for(unsigned int k=0;k<q.rows();k++)q(k)=def.shift+scal*k/double(q.rows()-1);

    valDer(q,val,der);
    ofstream out;
    string file=Dir+"Basis:"+name()+"_"+tools::str(label);
    out.open(file.c_str());
    if (not out.is_open())ABORT("could not open file "+file);
    for(unsigned int k=0;k<val.rows();k++){
        out<<q(k).real();
        for (unsigned int l=0;l<val.cols();l++)
            out<<", "<<val(k,l).real()<<", "<<der(k,l).real();
        out<<endl;
    }
}


double BasisSet::parameter(int K, string Kind) const{
    string nam=def.name().substr(0,def.name().find("{"));
    if(Kind=="m" and (nam=="expIm" or nam=="cosSin")){
        return parameters()[iFunction(K)];
    }
    else if (Kind=="l" and nam=="assocLegendre"){
        return iFunction(K)+abs(parameters()[0]);
    }
    else ABORT("no parameter \""+Kind+"\" for basis set "+str()+def.name());
}

std::string BasisSet::parameterString(int K) const{
    string s;
    if(def.name()=="expIm" or def.name()=="cosSin"){
        return "m="+tools::str(int(parameters()[iFunction(K)]),2);
    }
    else if (def.name().find("assocLegendre")!=string::npos){
        return "l="+tools::str(iFunction(K)+abs(parameters()[0]),2);
    }
    else
        return "";

}

unsigned int BasisSet::iFunction(int K) const{
    if(trans.col(K).nonZeros()!=1)ABORT("not a single BasisFunction for BasisSet function no.="+tools::str(K));
    unsigned int loc=trans.col(K).locNonZero()[0];
    if (loc>=trans.rows()){
        trans.print("transformation");
        cout<<"loc "<<tools::str(trans.col(K).locNonZero());
        ABORT("not a permutation of BasisFunction for BasisSet function no.="+tools::str(K));
    }
    return loc;
}

UseMatrix BasisSet::val(const UseMatrix &X, bool ZeroOutside) const {
    UseMatrix V,D;
    valDer(X,V,D,ZeroOutside);
    return V;
}
UseMatrix BasisSet::der(const UseMatrix &X, bool ZeroOutside) const {
    UseMatrix V,D;
    valDer(X,V,D,ZeroOutside);
    return D;
}

void BasisSet::valDer(const std::vector<std::complex<double> > &X, std::vector<std::complex<double> > &Val, std::vector<std::complex<double> > &Der, bool ZeroOutside) const{
    UseMatrix v,d;
    valDer(UseMatrix::UseMap(const_cast<vector<complex<double> >*>(&X)->data(),X.size(),1),v,d,ZeroOutside);
    Val=std::vector<std::complex<double> >(v.data(),v.data()+v.size());
    Der=std::vector<std::complex<double> >(d.data(),d.data()+d.size());
}

void BasisSet::valDer(const UseMatrix &X, UseMatrix &Val, UseMatrix &Der, bool ZeroOutside) const {
    UseMatrix S(X);
    if(def.shift!=0.)S+=UseMatrix::Constant(S.rows(),1,-def.shift);
    if(def.scale!=1.)S*=(1./def.scale);
    fs->valDer(S,Val,Der,ZeroOutside);
    //    Val*=trans;
    //    Der*=trans;
    Eigen::Map<Eigen::MatrixXcd>(Val.data(),Val.rows(),Val.cols())*=Eigen::Map<Eigen::MatrixXcd>(trans.data(),trans.rows(),trans.cols());
    Eigen::Map<Eigen::MatrixXcd>(Der.data(),Der.rows(),Der.cols())*=Eigen::Map<Eigen::MatrixXcd>(trans.data(),trans.rows(),trans.cols());
    Der/=def.scale;
    // Treat grid differently: return value if sufficiently near to a grid point, else return 0
    if (X.size()!=Val.size() and isGrid()) {
        UseMatrix tempVal(UseMatrix::Zero(X.size(),1));
        for (unsigned int i=0; i!=tempVal.size(); ++i) {
            for (unsigned int j=0; j!=points.size(); ++j) {
                if (norm(X(i)-points(j))<1.e-10){
                    tempVal(i)=weights(j);
                    break;
                }
            }
        }
        Val.swap(tempVal);
    }

}

UseMatrix BasisSet::matrixWeigJacobian(const UseMatrix & X, const UseMatrix & W, bool Eta) const{
    UseMatrix res=UseMatrix::Zero(X.rows(),X.rows());
    if(Eta and def.coor.jaco!=Coordinate::J_one)ABORT("for now, only jacobian=1 allowed for complex scaling");
    switch(def.coor.jaco) {
    case Coordinate::J_one: for (unsigned int i=0;i<X.rows();i++)res(i,i)=W(i);return res;
    case Coordinate::J_val: for (unsigned int i=0;i<X.rows();i++)res(i,i)=X(i)*W(i);if(Eta)res*=eta();return res;
    case Coordinate::J_squ: for (unsigned int i=0;i<X.rows();i++)res(i,i)=X(i)*X(i)*W(i);if(Eta)res*=eta()*eta();return res;
    default:
        ABORT("Jacobian not defined for code: "+tools::str(def.coor.jaco)+", basis: "+str());
    }

}

double BasisSet::lowBound() const {
    if(lowB==DBL_MAX){
        if(isGrid()){
            if(points.size()==0)return lowB=0.;
            lowB=points(0).real();
        }
    }
    return lowB;
}
double BasisSet::scale() const {
    if(not isGrid())return def.scale;
    if(points.size()==0 or points.size()==1)return 1.;
    return points(points.size()-1).real()-points(0).real();
}
double BasisSet::upBound() const {
    if(upB==-DBL_MAX){
        if(isGrid()){
            if(points.size()==0)upB=0.;
            upB=points(points.size()-1).real();
        }
    }
    return upB;
}

unsigned int BasisSet::lowerMargin() const {
    if(def.margin[0]!=0)ABORT("use only for first function with non-zero boundary");
    return 0;
}
unsigned int BasisSet::upperMargin() const {
    if(def.margin[1]!=def.order-1)ABORT("use only for last function with non-zero right boundary");
    return size()-1;
}

unsigned int BasisSet::defaultNQuad() const{
    if(fs->name().find("assocLegendre")==0)
        return fs->order+(unsigned int)abs(parameters()[0]);
    else
        return fs->order;
}

void BasisSet::quadRule(int N, std::vector<double> & QuadX, std::vector<double> & QuadW) const
{
    UseMatrix x,w;
    quadRule(N,x,w);
    for(int k=0;k<x.size();k++){
        QuadX.push_back(x(k).real());
        QuadW.push_back(w(k).real());
    }
}
void BasisSet::quadRule(unsigned int N, UseMatrix &QuadX, UseMatrix &QuadW) const
{
    if(isGrid()){
        QuadX=points;
        QuadW=weights;
    } else {
        fs->quadRule(N,QuadX,QuadW);
        QuadW*=abs(def.scale);
        QuadX*=def.scale;
        QuadX+=UseMatrix::Constant(QuadX.rows(),1,def.shift);
    }
}

void BasisSet::dvrRule(UseMatrix &QuadX, UseMatrix &QuadW, bool All) const{
    if(isGrid()){
        QuadX=points;
        QuadW=weights;
    }

    else {
        // DVR functions are NOT in ascending order
        // this is a side-effect of running through the general FEM setup
        // where the first two functions are assumed to be suitable to give lower/upper margin values

        //        UseMatrix pts_mat,wgs_mat;
        //        vector<const BasisSet*>vB(1,this);
        //        BasisMat::get(vector<string>(1,"1"),vB,vB,wgs_mat,vector<string>(1,""));
        //        BasisMat::get(vector<string>(1,"Q"),vB,vB,pts_mat,vector<string>(1,""));
        //        if(not pts_mat.isDiagonal(1.e-12) or not wgs_mat.isDiagonal(1.e-12)){
        //            ABORT("do not use dvrRule for non-DVR basis: "+name());
        //        }

        UseMatrix grid,weig;
        fs->dvrRule(grid,weig);
        weig*=abs(def.scale);
        grid*=def.scale;
        grid+=UseMatrix::Constant(grid.size(),1,def.shift);

        if(All){
            QuadX=grid;
            QuadW=weig;
        } else {
            UseMatrix vals=val(grid);

            // get the permutation
            QuadX=UseMatrix(size(),1);
            QuadW=UseMatrix(size(),1);
            for(int k=0;k<size();k++){
                for(int l=0;l<grid.size();l++){
                    if(abs(vals(l,k))>1.e-12){
                        QuadX(k)=grid(l);
                        QuadW(k)=weig(l);
                        break;
                    }
                }
            }
        }
    }
}


bool BasisSet::check(const UseMatrix & Coor, const UseMatrix & funOvr, double eps, int cond){
    // boundary values: (0,1) functions must have low/up=1, all others =0
    UseMatrix m;
    m=val(Coor);
    m.block(0,0,2,2)-=UseMatrix::Identity(2,2);
    if (not m.isZero(eps)) {
        return false;
    }

    if(cond==4){
        m=der(Coor);
        m.block(0,2,2,2)-=UseMatrix::Identity(2,2);
        if (not m.isZero(eps)) return false;
    }

    if(cond==2){ // hack
        // overlap must be one except for (0,1) functions
        m=trans.transpose()*funOvr*trans;
        m.block(cond,cond,m.rows()-cond,m.cols()-cond)-=UseMatrix::Identity(m.rows()-cond,m.cols()-cond);
        if (not m.block(0,cond,m.rows(),m.cols()-cond).isZero(eps))return false;
    }
    return true;
}

bool BasisSet::isAbsorptive() const {
    if(def.comSca.kind=="ECS")return eta()!=1.;
    else if (def.comSca.kind=="PML")return eta()!=0.;
    else if (def.comSca.kind=="UNSCALED")return false;
    else ABORT("isAbsorptive not defined for comSca.kind="+def.comSca.kind);
    return false;
}
bool BasisSet::isIntegrable(const BasisSet &other) const {
    if(name()==other.name()) return true;
    if(tools::stringInBetween(nameStr,"*","]")==tools::stringInBetween(nameStr,"*","]"))return true;
    if(name()=="polynomial" and other.fs->name()=="incoming") return true;
    return false;
}
complex<double> BasisSet::eta() const {return def.comSca.etaX(0.5*(lowBound()+upBound()));}


unsigned int BasisSet::size() const{
    if(points.size()>0)return points.size();
    return trans.cols();
}

void BasisSet::Test(bool Print)
{// set up a few basis sets, checks are internal during setup
    double x0,x1;
    vector<int> marg(2,0);
    marg[1]=2;
    if(Print){
        BasisSet(BasisSetDef(4,0.,4.,"polynomial",false,true,false,Coordinate::fromString("Rn"),marg)).plot("");
        BasisSet(BasisSetDef(3,4.,6.,"polynomial",false,false,false,Coordinate::fromString("Rn"),marg)).plot("");
        BasisSet(BasisSetDef(3,10.,1.,"polExp[0.1]",false,false,true,Coordinate::fromString("Rn"),marg)).plot("");
    }
    if(Print)cout<<"OK basisSet test - find plots on files Basis:...\n";
}
bool BasisSet::hasOverlap(const BasisSet &other) const{
    if(&other==0)return false;
    double eps=1.e-10*min(abs(other.scale()),abs(scale()));
    // overlap by an open set
    if(not ( (upBound()-other.lowBound())<=eps or (other.upBound()-lowBound())<=eps))
        return true;

    // special cases
    // point in [low,upp]
    if(isGrid() and size()==1){
        if(upBound()-other.lowBound()==0.)return true;
        if(lowBound()-other.upBound()==0.)return true;
    }
    if(other.isGrid() and other.size()==1){
        if(other.upBound()-lowBound()==0.)return true;
        if(other.lowBound()-upBound()==0.)return true;
    }

    return false;

}
bool BasisSet::shareBoundary(const BasisSet &other) const{
    double eps=1.e-7*min(abs(other.scale()),abs(scale()));
    // neighboring intervals share boundary:
    return abs(upBound()-other.lowBound())<eps or abs(other.upBound()-lowBound())<eps;
}


void BasisSet::show(const string & Text) const {
    if(Text!="")cout<<Text<<" ";
    else        cout<<"BasisSet: ";
    cout<<str()<<endl;
}
unsigned int BasisSet::order()const{
    if(name().find("ssoc")!=string::npos)
        return size()+std::abs(int(parameters()[0]));
    return std::max(trans.rows(),points.size());
}

string BasisSet::strDefinition() const {
    if(isGrid()){
        if(weights.size()==points.size())return BasisGridQuad::factory(this)->strDefinition();
        else if(weights.size()==0)       return BasisGrid::factory(this)->strDefinition();
        else DEVABORT(Str("basis is neither plain nor quadratur grid, #pts=")+points.size()
                      +" #weights="+weights.size());
    }

    if(str().find("useIndex")!=string::npos){
        Str s("Coef:","");
        return s+order();
    }

    if(str().find("assocLegendre")!=string::npos){
        std::string s("AssociatedLegendre: ");
        s+=tools::str(int(def.par[0]))+","+tools::str(size());
        return s;
    }

    ABORT("not implemented for "+name()+" ("+str()+")");
}

string BasisSet::str(int Level) const {
    string s;
    if(def.par.size()>0)
        s=def.str()+" par: "+tools::str(def.par);
    else
        s=def.str()+" <"+tools::str(label)+"> ";
    if(Level==0)return s;

    s+="\n"+points.str(" points",2);
    s+="\n"+weights.str("weights",2);
    return s;
}
string BasisSet::strFunc(const int I) const {
    if(def.funcs.find("expIm")!=string::npos)
        return tools::str(parameters()[I]);
    else if(def.funcs.find("cosSin")!=string::npos){
        if(I%2==0)return "c"+tools::str(I/2);
        else      return "s"+tools::str((I+1)/2);
    }
    else if(def.funcs.find("assocLeg")!=string::npos)
        return tools::str(I+abs(def.par[0]))+"m"+tools::str(def.par);
    else
        return tools::str(I);
}

double BasisSet::physical(int Index) const{
    // Magnetic momentum
    if(def.funcs.find("expIm")!=std::string::npos or def.funcs.find("cosSin")!=std::string::npos){
        return parameters()[Index];
    }

    // Angular momentum
    if(def.funcs.find("assocLegendre")!=std::string::npos){
        return abs(param(0)) + Index;
    }

    // Grid
    if(def.funcs.find("grid")!=std::string::npos){
        return points(Index).real();
    }

    // Grid
    if(def.funcs.find("grid")!=std::string::npos){
        return points(Index).real();
    }

    // Default
    return Index;
}
