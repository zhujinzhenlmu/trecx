#include "tsurffSource.h"

#include "mpiWrapper.h"
#include "threads.h"

#include "surfaceFlux.h"
#include "discretization.h"
#include "discretizationSurface.h"
#include "discretizationtsurffspectra.h"
#include "operatorDefinitionNew.h"
#include "parameters.h"
#include "index.h"
#include "timer.h"
#include "printOutput.h"

#include "gaunt.h"

#include "operatorTreeVolkov.h"
#include "volkovGrid.h"

#include "log.h"
#include "operatorTreeExplorer.h"
#include "pulse.h"
#include "readInputList.h"
#include "basisGrid.h"
#include "coefficientsPermute.h"
#include "fileTools.h"
#include "timeCritical.h"

#include "debugInfo.h"

static CoefficientsLocal* _viewSource=0;
static CoefficientsLocal* _viewSourceDamped=0;


static int surfaceNumber(std::string Ax, const Index* Idx){
    const Index* idx=Idx;
    int surfNum=0;
    while(idx!=0 and idx->axisName()!=Ax){
        if(idx->continuity()!=Index::npos){
            surfNum++;
            idx=idx->descend();
        } else {
            idx=idx->nodeNext();
        }
    }
    if(idx==0)ABORT("axis "+Ax+" is not unbound axis in "+Idx->hierarchy());
    return surfNum;
}

// wrapper form momenutum grid (from DiscretizationTsurffSpectra)
std::vector<double> momenta(ReadInput & Inp, int radialPoints=DiscretizationTsurffSpectra::_defaultRadialPoints){
    // get the input flags
    // default value for radialPoints
    double maxEnergy=std::max(3*Pulse::current.omegaMax(),12.*Pulse::current.uPonderomotive()),minEnergy=0.;
    bool kGrid=true;
    DiscretizationTsurffSpectra::readFlags(Inp,radialPoints,minEnergy,maxEnergy,kGrid,true);

    std::vector<double> _momenta(radialPoints);
    DiscretizationTsurffSpectra::computeMomenta(minEnergy,maxEnergy,_momenta,kGrid);
    if (_momenta.size()==0) { ABORT("specify at least one momentum"); }
    return _momenta;
}

double readTurnoff(ReadInput& Inp){
    std::string def=tools::str(4.*math::pi/Pulse::current.omegaMin());
    double res;
    Inp.read("Source","turnOff",res,def,"linearly turn off source over this time-interval, default = 1 OptCyc",1,"tAver");
    return res;
}
double readEndIntegration(ReadInput& Inp){
    double res;
    Inp.read("Source","end",res,"Infty","stop time-integration here",1,"tMax");
    return res;
}
void TsurffSource::allReads(ReadInput & Inp){
    readTurnoff(Inp);
    readEndIntegration(Inp);
    nextRegion(Inp,"");
    readSymmetry12(Inp);
}

void TsurffSource::readTurnOff(ReadInput &Inp, double &EndProp){
    _turnoffRange=readTurnoff(Inp);
    _endIntegration=readEndIntegration(Inp);
    if(_endIntegration<DBL_MAX/2 and _endIntegration>EndProp)
        PrintOutput::warning(Str("spectral integration stops before time-propagtion:")+_endIntegration+"<"+EndProp);
    _endIntegration=std::min(EndProp,_endIntegration);
    EndProp=_endIntegration;

    if(_turnoffRange>0.){
        _currentSourceDamped.reset(new Coefficients(idx()));
        //HACK for parallel, need CoefficientsLocal view (to be replaced)
        _viewSourceDamped=CoefficientsLocal::view(_currentSourceDamped.get());
    }
}


void TsurffSource::generate(const std::vector<std::string> AxisPath, ReadInput &Inp, const Index *Parent, Index *&Surface, Index *&Source){
    // Surface discretization
    std::vector<double> radii;
    int nMomenta=DiscretizationTsurffSpectra::_defaultRadialPoints;
    if(AxisPath.size()>1){
        // get surface and spectral data from previous calculations
        ReadInputList linp(Inp.outputTopDir()+"S_"+AxisPath[AxisPath.size()-2]+"/linp");
        std::string linpStr;

        linpStr=linp.readValue("Surface","points",ReadInput::noDefault,"retrieve for TsurffSource",1,"dum1","");
        for(std::string rStr: tools::splitString(linpStr,' '))radii.push_back(tools::string_to_double(rStr));
        linpStr=linp.readValue("Spectrum","radialPoints",ReadInput::noDefault,"retrieve for TsurffSource",1,"dum2","");
        nMomenta=tools::string_to_int(linpStr);
    } else {
        Inp.read("Surface","points",radii,"","save values and derivatives at surface points (blank-separated list)");
    }
    Surface=new DiscretizationSurface::IndexS(Parent,radii,surfaceNumber(AxisPath[0],Parent));
    Source= new DiscretizationTsurffSpectra::IndexTsurffSpectra(Surface,momenta(Inp,nMomenta));
    if(AxisPath.size()==1)return;

    delete Surface;
    Index* oldSource=Source;
    generate(std::vector<std::string>(AxisPath.begin()+1,AxisPath.end()),Inp,oldSource,Surface,Source);
    delete oldSource;
}

std::string TsurffSource::str() const {

    Str s("","");
    for(const Index* ix=idx();ix;ix=ix->descend()){
        if(ix->axisName().find("k")==0){
            s=s+"\n"+ix->axisName()+SEP(", ");
            s=s+ix->basisAbstract()->size()+tools::str(pow(ix->basisGrid()->mesh()[0],2)*0.5,3);
            s=s+tools::str(pow(ix->basisGrid()->mesh().back(),2)*0.5,3);
            s=s+tools::str(_endIntegration,3);
            s=s+SEP(" ");
            if(_turnoffRange>0.)s=s+"[turn off: "+_turnoffRange+"]";
            std::string space="";
            for(int k=1;space=="" and k<ix->basisGrid()->size()-1;k++)
                if(     std::abs(ix->basisGrid()->mesh()[k-1]+ix->basisGrid()->mesh()[k+1]-2.*ix->basisGrid()->mesh()[k])>
                        std::abs(ix->basisGrid()->mesh()[k-1]-ix->basisGrid()->mesh()[k+1])*1.e-12)space="NOT k-equidistant";
            s=s+space;
        }
    }
    if(s!="")s=s.substr(1);
    return s;
}

std::vector<std::string> TsurffSource::unboundAxes(ReadInput & Inp){
    std::vector<Axis> ax;
    Axis::fromFile(Inp,ax);
    std::vector<std::string> unbound;
    for(const Axis & a: ax)
        if(a.upperEnd()>DBL_MAX/2. or a.lowerEnd()<-DBL_MAX/2)unbound.push_back(a.name);
    return unbound;

}

bool TsurffSource::readSymmetry12(ReadInput &Inp){
    bool symm12;
    Inp.read("Spectrum","symmetry12",symm12,"false","assume 1<->2 exchange symmetry and avoid unnecessary regions");
    return symm12;
}

static std::string _nextRegion(ReadInput &Inp,std::string Region){

    if(MPIwrapper::isMaster()){
        bool symm12=TsurffSource::readSymmetry12(Inp);

        // if not set yet, try reading:
        std::string inRegion;
        Inp.read("Source","Region",inRegion,"","axis perpendicular to surface of subregion",1,"region");
        if(inRegion!=""){
            PrintOutput::message("calculation forced in region "+inRegion);
            return inRegion;
        }

        // check for completed initial run
        std::string outf=Inp.outputTopDir()+PrintOutput::outExtension;
        if(not  folder::exists(outf) or
                tools::findFirstLine(outf,{"accept/reject","TimePropagator"})==""){
            return Region="";
        }

        std::vector<Axis> ax;
        Axis::fromFile(Inp,ax);
        std::vector<std::string> unbound(TsurffSource::unboundAxes(Inp));

        // single coordinate regions
        std::string skip="";
        for(std::string reg: unbound){
            std::string outf=Inp.outputTopDir()+"S_"+reg+"/"+PrintOutput::outExtension;
            if(not  folder::exists(outf) or
                    tools::findFirstLine(outf,{"accept/reject","TimePropagator"})==""){
                if(symm12 and reg.back()!='1')skip+=reg+",";
                else                  return Region=reg;
            }
        }


        // double coordinate regions
        for(std::string reg1: unbound)
            for(std::string reg2: unbound){
                if(reg1==reg2)continue;
                std::string outf=Inp.outputTopDir()+"S_"+reg1+"."+reg2+"/"+PrintOutput::outExtension;
                if(not folder::exists(outf) or
                        tools::findFirstLine(outf,{"accept/reject","TimePropagator"})==""){
                    if(symm12 and reg2.back()!='2')skip+=reg1+"."+reg2;
                    else                   return Region=reg1+"."+reg2;
                }
            }

        // all subregions have been done
        return Region=Sstr+"unbound axes:"+unbound;
    }
}
std::string TsurffSource::nextRegion(ReadInput &Inp, std::string Region){
    std::string region;
    if(MPIwrapper::isMaster())region=_nextRegion(Inp,Region);
    MPIwrapper::Bcast(region,MPIwrapper::master());
    return region;
}

void TsurffSource::print(std::string SourceFile) const {

    //    PrintOutput::title("Integration for spectral amplitude");
    PrintOutput::lineItem("source(s)",SourceFile);
    PrintOutput::newLine();
    PrintOutput::lineItem("end",_endIntegration);
    if(_turnoffRange>0.)
        PrintOutput::lineItem("turnOff",_turnoffRange);
    else
        PrintOutput::lineItem("turnOff","ABRUPT (Caution)");

    PrintOutput::paragraph();
    PrintOutput::paragraph();
    bool head=true;
    for(const Index* ix=idx();ix;ix=ix->descend()){
        if(ix->axisName().find("k")==0){
            if(head){
                head=false;
                PrintOutput::newRow();
                PrintOutput::rowItem("axis");
                PrintOutput::rowItem("Points");
                PrintOutput::rowItem("Emin");
                PrintOutput::rowItem("Emax");
                PrintOutput::rowItem("spacing");
            }
            PrintOutput::newRow();
            PrintOutput::rowItem(ix->axisName());
            PrintOutput::rowItem(ix->basisGrid()->size());
            PrintOutput::rowItem(pow(ix->basisGrid()->mesh()[0],2)*0.5);
            PrintOutput::rowItem(pow(ix->basisGrid()->mesh().back(),2)*0.5);
            std::string space="k-equidistant";
            for(int k=1;space=="" and k<ix->basisGrid()->size()-1;k++)
                if(     std::abs(ix->basisGrid()->mesh()[k-1]+ix->basisGrid()->mesh()[k+1]-2.*ix->basisGrid()->mesh()[k])>
                        std::abs(ix->basisGrid()->mesh()[k-1]-ix->basisGrid()->mesh()[k+1])*1.e-12)space="NOT k-equidistant";
            PrintOutput::rowItem(space);
        }
    }

}

TsurffSource::TsurffSource(const std::vector<std::string> & AxisPath, const Index *Parent, ReadInput &Inp){
    //    readTurnOff(Inp,DBL_MAX);
    //    if(_endIntegration>_endTurnOff)ABORT("source turn off time after end time");
    DEVABORT("not activated");
    Index *surfI, *sourceI;
    //NOTE: surfI should be recoverd from surface_* file rather than reconstructed from input
    generate(AxisPath,Inp,Parent,surfI,sourceI);

    //    flux=new SurfaceFlux(AxisPath,Inp,surfI);
    commutator=new OperatorTree("Commutator",OperatorDefinitionNew("<<Commutator>>", flux->idx()->hierarchy()),flux->idx(), flux->idx());

    wfSurface = new Coefficients(flux->idx());
    wfSurface->treeOrderStorage();

    wfSmoothSpecDisc = new Coefficients(sourceI);
    wfSmoothSpecDisc->treeOrderStorage();

    //    tempSmoothSpecDisc = new Wavefunction(smoothSpecDisc);
    tempSmoothSpecDisc = new Coefficients(*wfSmoothSpecDisc);
    tempSmoothSpecDisc->treeOrderStorage();

    volkovPhase = new VolkovGrid(flux,idx());

}

TsurffSource::TsurffSource(const DiscretizationTsurffSpectra* SmoothSpecDisc, ReadInput& Inp,
                           std::vector<DiscretizationSurface*>SurfD, std::string Region, int SurfNumber): smoothSpecDisc(SmoothSpecDisc),
    _endIntegration(DBL_MAX),_turnoffRange(0.)
{
    timeCritical::suspend();
    const Discretization* D = dynamic_cast<Discretization*>(SurfD[SurfNumber]);
    while(D->parent)D=D->parent;
    //AS this works, but is logically not every clean, cast should be to discretization derived, and stop when fails

    // Initialize Surface Flux
    LOG_PUSH("SurfaceFlux");
    // extract file name from surface discretization
    std::string file;
    std::vector<std::string> axes=tools::splitString(SurfD[SurfNumber]->idx()->hierarchy(),'.');
    for(std::string ax: axes)
        if(ax.find("k")==0)file+="_"+ax.substr(1);
    if(file!="")file="S"+file+"/";
    for(std::string ax: axes)
        if(ax.find("ValDer")==0)file+=DiscretizationSurface::prefix+ax.substr(6);
    file=Inp.outputTopDir()+file;
    flux= new SurfaceFlux(file);

    LOG_POP();

    LOG_PUSH("smoothSpecDisc");
    // determine number of k-grid points for present source
    // from Index's of remaining surfD (i.e. surfD[k], k!=SurfNumber)
    const Index* surfIdx = SurfD[SurfNumber]->idx();
    if(SurfD.size()>1){ // sources from one or more subregions
        for(;surfIdx!=0;surfIdx=surfIdx->descend()){
            if(surfIdx->axisName().find("surf") == 0) break;
        }
        if(surfIdx == 0) DEVABORT("no surface found in index\n"+SurfD[SurfNumber]->idx()->str());
        std::string kGrid="k"+surfIdx->axisName().substr(4);

        surfIdx = SurfD[(SurfNumber+1)%SurfD.size()]->idx();
        for(;surfIdx!=0;surfIdx=surfIdx->descend()){
            if(surfIdx->axisName()==kGrid) break;
        }
        if(surfIdx==0) DEVABORT("no surface found in index\n"+SurfD[(SurfNumber+1)%SurfD.size()]->idx()->str());
        int radialPoints=surfIdx->basisAbstract()->size();

        smoothSpecDisc = dynamic_cast<DiscretizationTsurffSpectra*>(
                    DiscretizationTsurffSpectra::factoryTsurff(SurfD[SurfNumber],Inp,radialPoints));
    }
    if(smoothSpecDisc==0){
        smoothSpecDisc = dynamic_cast<DiscretizationTsurffSpectra*>(
                    DiscretizationTsurffSpectra::factoryTsurff(SurfD[SurfNumber],Inp));
    }

    LOG_POP();

    commutator=new OperatorTree("Commutator",OperatorDefinitionNew("<<Commutator>>", flux->idx()->hierarchy()),flux->idx(), flux->idx());


    LOG_PUSH("wavefunctions");
    wfSurface = new Coefficients(flux->idx());
    wfSurface->treeOrderStorage();

    //    wfSmoothSpecDisc = new Wavefunction(smoothSpecDisc);
    wfSmoothSpecDisc = new Coefficients(smoothSpecDisc->idx());
    wfSmoothSpecDisc->treeOrderStorage();

    //    tempSmoothSpecDisc = new Wavefunction(smoothSpecDisc);
    tempSmoothSpecDisc = new Coefficients(*wfSmoothSpecDisc);
    tempSmoothSpecDisc->treeOrderStorage();
    LOG_POP();

    Parameters::update(flux->FluxBufferBeginTime());
    _currentTime=flux->FluxBufferBeginTime();

    LOG_PUSH("Volkov");

    bool useGrid=true;

    // Not pretty, but should detect Helium 6D
    if(idx()->hierarchy().find("Eta1") != std::string::npos and idx()->hierarchy().find("Eta2") != std::string::npos){
        useGrid = false;
    }
    if(MPIwrapper::Size()>1 and not useGrid){
        useGrid=true;
        PrintOutput::DEVwarning("forcing useGrid in parallel code");
    }
    if(ReadInput::main.found("_EXPERT_","usegrid","DEBUGusegrid")){
        PrintOutput::warning("forcing grid usage");
        ReadInput::main.read("_EXPERT_","usegrid",useGrid,ReadInput::noDefault,"force grid true/false",0,"DEBUGusegrid");
    }

    //    useGrid=true;

    if(!useGrid)
        PrintOutput::message("Applying Volkov Phase without grid");

    if(useGrid){
        volkovPhase = new VolkovGrid(D, flux, smoothSpecDisc);
    }else{
        std::string kLevel;
        for(const Index *s=flux->idx(),*t=idx();;s=s->child(0), t=t->child(0)){

            if(s->axisName().size()>6 and t->axisName().size()>1)
                if(s->axisName().substr(0,6)=="ValDer" and t->axisName().substr(0,1)=="k"){
                    kLevel = t->axisName();
                    break;
                }else if(s->axisName().substr(0,6) == "ValDer"){
                    if(t->child(0)->axisName().substr(0,1) == "k"){
                        kLevel = t->child(0)->axisName();
                        break;
                    }
                }
            if(t->isBottom() or s->isBottom())break;
        }

        /*
                 * Not exactly pretty, but should do
                 */
        OperatorTreeVolkov::AxisDescriptor ax = {"NONE", "NONE", kLevel};
        for(const Index* s=idx(); not s->isLeaf(); s=s->descend()){
            if(kLevel.find("kRn")==0 and s->axisName().find("Phi")==0){
                if(kLevel.size()>3 and s->axisName().size()>3){
                    if(kLevel.substr(3,1) == s->axisName().substr(3,1)) ax.phi = s->axisName();
                }else if(kLevel.size() == 3 and s->axisName().size() == 3){
                    ax.phi = s->axisName();
                }
            }
            if(kLevel.find("kRn")==0 and s->axisName().find("Eta")==0){
                if(kLevel.size()>3 and s->axisName().size()>3){
                    if(kLevel.substr(3,1) == s->axisName().substr(3,1)) ax.eta = s->axisName();
                }else if(kLevel.size() == 3 and s->axisName().size() == 3){
                    ax.eta = s->axisName();
                }
            }
        }

        PrintOutput::DEVmessage("Applying Volkov Phase on "+ax.phi+"."+ax.eta+"."+ax.k);
        // Don't use idx() due to parallelization
        volkovPhase = new OperatorTreeVolkov(wfSmoothSpecDisc->idx(), wfSmoothSpecDisc->idx(), ax);
    }
    delete _viewSource,_viewSourceDamped; //HACK get rid of this soon
    _viewSource=CoefficientsLocal::view(wfSmoothSpecDisc);
    if(_currentSourceDamped)_viewSourceDamped=CoefficientsLocal::view(_currentSourceDamped.get());
    LOG_POP();
    timeCritical::resume();
}


static int nCalls=0;
TIMER(fluxUpdate,)
TIMER(applyCommutator,)
TIMER(smoothSpec,)
TIMER(volkov,)
CoefficientsLocal* TsurffSource::UpdateSource(double Time){
    //HACK for use in parallel

    STARTDEBUG(fluxUpdate);
    _currentTime=Time;
    if(not flux->update(Time)) return 0;
    STOPDEBUG(fluxUpdate);


    Parameters::update(Time);

    STARTDEBUG(applyCommutator);
    commutator->apply(1., *flux->coefs, 0., *wfSurface);
    STOPDEBUG(applyCommutator);
    STARTDEBUG(smoothSpec);
    //HACK until index is consistent
    *smoothSpecDisc->mapFromParent()->tempRHS()=*wfSurface;
    smoothSpecDisc->mapFromParent()->apply(1.,*smoothSpecDisc->mapFromParent()->tempRHS(), 0., *tempSmoothSpecDisc);
    STOPDEBUG(smoothSpec);

    START(volkov);
    volkovPhase->update(Time);
    volkovPhase->apply(1., *tempSmoothSpecDisc, 0., *wfSmoothSpecDisc);
    STOP(volkov);
    return CurrentSource();
}

int x12count=0;
CoefficientsLocal *TsurffSource::CurrentSource(){
    if(_x12Source){
        if(++x12count>1000){
            ABORT("x21");
        }
        *wfSmoothSpecDisc+=_x12Source->fromOrig(*wfSmoothSpecDisc);
        wfSmoothSpecDisc->scale(0.5);
    }
    if(_turnoffRange==0. or _currentTime<_endIntegration-_turnoffRange){
        return _viewSource;
    }
    if(_currentTime>_endIntegration){
        PrintOutput::warning("time beyond  integration time",1);
        wfSmoothSpecDisc->setToZero();
        return _viewSource;
    }
    // linearly turn off source
    *_currentSourceDamped=*wfSmoothSpecDisc;
    _currentSourceDamped->scale((_endIntegration-_currentTime)/(_turnoffRange));
    return _viewSourceDamped;
}

double TsurffSource::SourceBufferBeginTime()
{
    return flux->FluxBufferBeginTime();     // Should be used only in the beginning
}

const Index* TsurffSource::idx() const{return wfSmoothSpecDisc->idx();}

std::vector<double> TsurffSource::surfaceRadius(const std::string Axis) const {
    return flux->idx()->grid("ValDer"+Axis);
}

