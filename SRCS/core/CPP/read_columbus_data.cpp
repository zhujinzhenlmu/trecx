// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include <sstream>
#include "read_columbus_data.h"
#include <iomanip>
#include<fcntl.h>
#include<sstream>

//#include <boost/algorithm/string.hpp>
//#include <boost/filesystem.hpp>
#include "readInput.h"
#include "mpiWrapper.h"
#include "readInput.h"
#include "printOutput.h"

#include <unistd.h> //WARNING: this is POSIX, breaks on Windows, needed for "read" below

using namespace std;
#include "eigenNames.h"



ReadInput* columbus_data::HACKinput=&ReadInput::main;

void columbus_data::read_cif(bool ion)
{
    PrintOutput::paragraph();
    if(ion)
        PrintOutput::title("Columbus data: Ion");
    else
        PrintOutput::title("Columbus data: Neutral");

    HACKinput->read("Columbus","path",col_path,ReadInput::noDefault,"Columbus run path");
    if(ion==false)  col_path += "/neutral";

    HACKinput->read("Columbus","DetTol",cif.det_tol,ReadInput::noDefault,"threshold for ci coefficients");
    HACKinput->read("Columbus","SceTol",cif.sce_cutoff,ReadInput::noDefault,"tolerance for single center expansion");
    HACKinput->read("Columbus","MultTol",cif.multipole_cutoff,tools::str(1e-20),"tolerance for multopole expansion");
    HACKinput->read("Columbus","Ions",cif.numI,ReadInput::noDefault,"number of ions");
    HACKinput->read("Columbus","MultF",cif.multF,"1","final spin multiplicity");
    if(cif.multF!=1)ABORT("for now, only singlets (spin multiplicity = 1)");
    string instDir;
    HACKinput->read("Columbus","instDir",instDir,ReadInput::noDefault,"installation directory");
    HACKinput->read("Columbus","dets",det_cutoff,tools::str(99999999),"maximum number of determinants to be considered in the CI basis expansion");
    HACKinput->read("Columbus","threshold",det_threshold,tools::str(0.),"CI expansion coefficient threshold all determinants have to fulfill");

    std::string pth = HACKinput->output();
    if(!MPIwrapper::isMaster()) pth = pth.substr(0, pth.length() - 2);
    if(ion)         cif.path = pth+"ion";
    if(ion==false)  cif.path = pth+"neutral";

    PrintOutput::message("Running python script columbus_data_processing.py with determinants tolerance: "+tools::str(cif.det_tol));
    if(MPIwrapper::isMaster())
        system(("python "+instDir+"/SRCS/columbus_data_processing.py "+col_path+" "+tools::str(cif.det_tol)+" "+cif.path).c_str());
    MPIwrapper::Barrier();
}

columbus_data::columbus_data(bool ion):QuantumChemicalData(),vee(NULL){

    PrintOutput::DEVmessage(Str("columbus_data, ion=")+ion);

    //Need to provide the path to the home directory of the columbus calculation
    read_cif(ion);

    this->home = cif.path;
    ifstream file((this->home+"/infofl").c_str());
    if(!file) ABORT("Could not open " + this->home + "/infofl");
    getline(file,this->sym.symbol);        // The first line of this file has symmetry stored

    int t = file.tellg();
    string str;
    getline(file,str);                     //Read irreps info

    file >> sym.no_of_irreps;
    sym.orbitals_per_irrep = VectorXi::Zero(sym.no_of_irreps);

    file.seekg(t);
    for(int i=0; i<sym.no_of_irreps; i++){
        file>>sym.orbitals_per_irrep(i);
        //cout<<sym.orbitals_per_irrep(i)<<endl;
    }
    file.close();
    PrintOutput::lineItem("Symmetry",sym.symbol);

    file.open((this->home+"/cidrtin").c_str());
    getline(file,str);
    getline(file,str);
    file >> no_electrons;
    file.close();
    PrintOutput::lineItem("Number of electrons",no_electrons);

    read_matrices();

    //dummy read - to save geometry data
    VectorXd e;
    Matrix<int, Dynamic, 3> power;
    Matrix<double, Dynamic, 3> coord;
    read_primitive_gaussian_info(e,power,coord);
}

columbus_data::~columbus_data()
{
    if(vee!=NULL) {delete vee; vee=NULL;}
    //    system(("rm -rf "+home).c_str()); // remove the columbus folder
    MPIwrapper::Barrier();
}

void columbus_data::read_primitive_gaussian_info(VectorXd& exponent, Matrix<int, Dynamic, 3>& power, Matrix<double, Dynamic, 3>& coord){

    //Read the geometry of molecule

    ifstream file((this->home+"/geom").c_str());
    if(!file) ABORT("Could not open geom");
    vector<string > atom_sym;                    //atomic symbol
    vector<int > atom_index;                     //index of that atom e.g. if 2 oxygen atoms , the O1 and O2
    vector<RowVector3d > coordinates;                  //coordinates of the atoms

    int g_count=0,h_count=0;
    Matrix<int, 15, 3> g_power;
    g_power<< 4,0,0, 3,1,0, 3,0,1, 2,2,0, 2,1,1,
            2,0,2, 1,3,0, 1,2,1, 1,1,2, 1,0,3,
            0,4,0, 0,3,1, 0,2,2, 0,1,3, 0,0,4;
    Matrix<int, 21, 3> h_power;
    h_power<< 5,0,0, 4,1,0, 4,0,1, 3,2,0, 3,1,1, 3,0,2, 2,3,0, 2,2,1,  2,1,2, 2,0,3,
            1,4,0, 1,3,1, 1,2,2, 1,1,3, 1,0,4, 0,5,0, 0,4,1, 0,3,2, 0,2,3,
            0,1,4, 0,0,5;


    while(!file.eof()){
        string a;
        double scr;
        file >> a >> scr;
        if(file.eof())
            break;

        int no_prev_occurences = 0;
        for(unsigned int i=0; i<atom_sym.size(); i++)
            if(atom_sym[i] == a)
                no_prev_occurences+=1;

        atom_sym.push_back(a);
        atom_index.push_back(no_prev_occurences+1);
        coordinates.push_back(RowVector3d(0,0,0));

        this->charge.push_back(scr);
        file >> coordinates[coordinates.size()-1](0) >> coordinates[coordinates.size()-1](1) >> coordinates[coordinates.size()-1](2) >> scr;
        this->cord.push_back(coordinates[coordinates.size()-1]);

    }
    file.close();

    PrintOutput::title("Geometry information");
    PrintOutput::newRow();
    PrintOutput::rowItem("Atom");
    PrintOutput::rowItem("No.");
    PrintOutput::rowItem("X");
    PrintOutput::rowItem("Y");
    PrintOutput::rowItem("Z");
    PrintOutput::newRow();
    for(unsigned i=0; i<atom_sym.size(); i++){
        PrintOutput::rowItem(atom_sym[i]);
        PrintOutput::rowItem(atom_index[i]);
        PrintOutput::rowItem(coordinates[i](0));
                PrintOutput::rowItem(coordinates[i](1));
                PrintOutput::rowItem(coordinates[i](2));
                PrintOutput::newRow();
    }

    this->no_atoms = atom_sym.size();

    file.open((this->home+"/LISTINGS/hermitls.sp").c_str());

    while(!file.eof()){
        string str;
        getline(file,str);
        if(str=="  Orbital exponents and contraction coefficients"){
            getline(file,str);
            getline(file,str);
            getline(file,str);
            break;
        }
    }

    vector<double > temp_exponents;
    vector<RowVector3d > temp_coord;
    vector<RowVector3i > temp_power;
    int prev_space = 1;              // prev_space=1  -> 1 line after space,  prev_space=2  -> 2 line after space,  prev_space=3  -> remaining,
    RowVector3d c;
    RowVector3i p;
    while(!file.eof()){
        string str;
        string atom,orb,no;
        double exp;
        getline(file,str);
        if(str==""){
            prev_space = 1;
            continue;
        }
        if(str=="  Contracted Orbitals")
            break;
        if(prev_space==1){
            istringstream iss(str);
            int dummy;
            iss >> atom >> no >> orb;
            if(orb.length()>4){
                string temp1,temp2;
                string t;
                t += orb[0];
                if(not(t=="f" or t=="g" or t=="h" )){
                    cout<<"case not covered!!!!!!!!!!!!"<<endl;
                    exit(1);
                }
                temp1+=orb[0];
                temp1+=orb[1];
                temp1+=orb[2];
                temp1+=orb[3];

                for(unsigned int i=4;i<orb.length();i++)
                    temp2+=orb[i];
                dummy = atoi(temp2.c_str());
                orb = temp1;
                iss >> exp;
            }
            else{
                iss >> dummy >> exp;
            }
            vector<string > split_no;
            DEVABORT("get non-boost");//boost::algorithm::split( split_no, no, boost::algorithm::is_any_of( "#" ) );
            if(split_no.size()==1)
                split_no.push_back(split_no[0]);
            //cout << atom << "  " << split_no[0] <<"#"<<split_no[1]<<"  "<< orb << "  " << exp <<endl;
            //cin.get();
            //set the coordinate.
            unsigned int i;
            for(i=0; i<atom_sym.size(); i++)
                if(atom_sym[i] == atom and atom_index[i]==atoi(split_no[1].c_str()))
                    break;
            c = coordinates[i];
            //set the power
            if(orb=="s")
                p = RowVector3i(0,0,0);
            else if(orb=="px")
                p = RowVector3i(1,0,0);
            else if(orb=="py")
                p = RowVector3i(0,1,0);
            else if(orb=="pz")
                p = RowVector3i(0,0,1);
            else if(orb=="dxx")
                p = RowVector3i(2,0,0);
            else if(orb=="dxy")
                p = RowVector3i(1,1,0);
            else if(orb=="dxz")
                p = RowVector3i(1,0,1);
            else if(orb=="dyy")
                p = RowVector3i(0,2,0);
            else if(orb=="dyz")
                p = RowVector3i(0,1,1);
            else if(orb=="dzz")
                p = RowVector3i(0,0,2);
            else if(orb=="fxxx")
                p << 3,0,0;
            else if(orb=="fxxy")
                p << 2,1,0;
            else if(orb=="fxxz")
                p << 2,0,1;
            else if(orb=="fxyy")
                p << 1,2,0;
            else if(orb=="fxyz")
                p << 1,1,1;
            else if(orb=="fxzz")
                p << 1,0,2;
            else if(orb=="fyyy")
                p << 0,3,0;
            else if(orb=="fyyz")
                p << 0,2,1;
            else if(orb=="fyzz")
                p << 0,1,2;
            else if(orb=="fzzz")
                p << 0,0,3;
            else if(orb=="g500"){
                p << g_power.row(g_count);
                vector<string > split_no1;
                DEVABORT("get non-boost");//boost::algorithm::split( split_no1, no, boost::algorithm::is_any_of( "#" ) );
                if(split_no1.size()==1){
                    g_count++;
                    if(g_count==15)
                        g_count = 0;
                }
                else if(split_no1.size()==2 and sym.symbol=="d2h"){
                    if(split_no1[1]=="2"){
                        g_count++;
                        if(g_count==15)
                            g_count = 0;
                    }
                }
            }
            else if(orb=="h600"){
                p << h_power.row(g_count);
                vector<string > split_no1;
                DEVABORT("get non-boost");//boost::algorithm::split( split_no1, no, boost::algorithm::is_any_of( "#" ) );
                if(split_no1.size()==1){
                    h_count++;
                    if(h_count==21)
                        h_count = 0;
                }
                else if(split_no1.size()==2 and sym.symbol=="d2h"){
                    if(split_no1[1]=="2"){
                        h_count++;
                        if(h_count==21)
                            h_count = 0;
                    }
                }
            }
            prev_space = 2;
        }
        else if(prev_space==2){
            istringstream iss(str);
            int dummy;
            iss >> atom >> orb;
            if(orb.length()==5)
                iss >> dummy >> exp;
            else
                iss >> exp;
            //cout << exp <<endl;
            prev_space = 3;
        }
        else if(prev_space==3){
            istringstream iss(str);
            int dummy;
            iss >> dummy >> exp;
            //cout << exp <<endl;
        }
        else{
            cout<<"Something went wrong"<<endl;
        }

        temp_exponents.push_back(exp);
        temp_power.push_back(p);
        temp_coord.push_back(c);
    }
    file.close();

    exponent = VectorXd::Zero(temp_exponents.size());
    power    = Matrix<int, Dynamic, 3>::Zero(temp_exponents.size(),3);
    coord    = Matrix<double, Dynamic, 3>::Zero(temp_exponents.size(),3);
    for(unsigned int i=0; i<temp_exponents.size(); i++){
        exponent(i) = temp_exponents[i];
        power.row(i)    = temp_power[i];
        coord.row(i)    = temp_coord[i];
        //cout<<setw(10)<<exponent(i)<<setw(10)<<power.row(i)<<setw(30)<<coord.row(i)<<endl;
    }
}

void columbus_data::read_contraction_coefficients(MatrixXd& trans){

    ifstream file((this->home+"/LISTINGS/hermitls.sp").c_str());
    if(!file) ABORT("Could not open hermitls.sp");

    while(!file.eof()){
        string str;
        getline(file,str);
        if(str=="  Orbital exponents and contraction coefficients"){
            getline(file,str);
            getline(file,str);
            getline(file,str);
            break;
        }
    }
    int no_primitives=0;
    while(!file.eof()){
        string str;
        getline(file,str);
        if(str!="")
            no_primitives+=1;
        if(str=="  Contracted Orbitals"){
            no_primitives-=1;
            break;
        }
    }

    string str;
    getline(file,str);
    getline(file,str);
    getline(file,str);
    int no_ao = 0;
    while(str!=""){
        getline(file,str);
        no_ao+=1;
    }
    file.close();

    file.open((this->home+"/LISTINGS/hermitls.sp").c_str());
    //cout<<"Number of primitive gaussians "<<no_primitives<<endl;
    //cout<<"Number of contracted orbitals "<<no_ao<<endl;

    trans = MatrixXd::Zero(no_ao,no_primitives);

    while(!file.eof()){
        string str;
        getline(file,str);
        if(str=="  Orbital exponents and contraction coefficients"){
            getline(file,str);
            getline(file,str);
            getline(file,str);
            break;
        }
    }

    DEVABORT("get non-boost");
    /*
    vector<list<double > > data;
    data.resize(no_primitives);
    int prev_space = 1;              // prev_space=1  -> 1 line after space,  prev_space=2  -> 2 line after space,  prev_space=3  -> remaining,

    while(!file.eof()){
        string str,prev_string;
        string atom,orb,no;
        double exp;
        int dummy;
        getline(file,str);
        //cout<<str<<endl;
        if(str==""){
            prev_space = 1;
            continue;
        }
        if(str=="  Contracted Orbitals")
            break;
        if(prev_space==1){
            istringstream iss(str);
            iss >> atom >> no >> orb;
            if(orb.length()>4){
                string temp1,temp2;
                string t;
                t += orb[0];
                if(not(t=="f" or t=="g" or t=="h" )){
                    cout<<"case not covered!!!!!!!!!!!!"<<endl;
                    exit(1);
                }
                temp1+=orb[0];
                temp1+=orb[1];
                temp1+=orb[2];
                temp1+=orb[3];

                for(unsigned int i=4;i<orb.length();i++)
                    temp2+=orb[i];
                dummy = atoi(temp2.c_str());
                orb = temp1;
                iss >> exp;
            }
            else{
                iss >> dummy >> exp;
            }
            prev_space=2;
            double coef;
            while(iss){
                iss >> coef;
                if(coef!=0.0)
                    data[dummy-1].push_back(coef);
            }
            if(coef!=0.0)
                data[dummy-1].pop_back();           //Remove the last double occurence
        }
        else if(prev_space==2){
            istringstream iss(str);
            char a1;
            iss >> atom >> a1 >> a1 >> a1 >> a1 >> a1 >> dummy >> exp;
            double coef;
            while(iss){
                iss >> coef;
                if(coef!=0.0)
                    data[dummy-1].push_back(coef);
            }
            if(coef!=0.0)
                data[dummy-1].pop_back();           //Remove the last double occurence
            prev_space=3;
        }
        else if(prev_space==3){
            istringstream iss(str);
            iss >> dummy >> exp;
            double coef;
            while(iss){
                iss >> coef;
                if(coef!=0.0)
                    data[dummy-1].push_back(coef);
            }
            if(coef!=0.0)
                data[dummy-1].pop_back();           //Remove the last double occurence
}
else{
cout<<"Something went wrong"<<endl;
}
}
        */
    //for(unsigned int i=0; i<data.size(); i++){
    //  for(list<double >::iterator j = data[i].begin(); j!=data[i].end(); ++j)
    //    cout<< *j <<"  ";
    //  cout<<endl;
    //}
    getline(file,str);
    getline(file,str);
    while(!file.eof()){

        getline(file,str);
        if(str=="")
            break;
        istringstream iss(str);
        string atom,no,orb;
        int ao_no;
        iss >> ao_no >> atom >> no >> orb;
        vector<int > coef_ind;
        while(iss){
            int ind;
            iss >> ind;
            coef_ind.push_back(ind);
        }
        coef_ind.pop_back();           //Removing the last double occurence

        DEVABORT("get non-boost");
        /*
 for(unsigned int i=0; i<coef_ind.size(); i++){
            trans(ao_no-1,coef_ind[i]-1) = *(data[coef_ind[i]-1].begin());
            data[coef_ind[i]-1].pop_front();
        }
 */
    }
    file.close();
    //for(int i=0; i<trans.rows();i++){
    // cout<<trans.row(i)<<endl;
    // cin.get();
    //}
}

void columbus_data::read_mo_coefficients(MatrixXd& trans,int no_ao){

    if(!this->sym.symbol.compare("c1") or !this->sym.symbol.compare("C1") or !this->sym.symbol.compare("c1 ") or !this->sym.symbol.compare("C1 ")){

        ifstream file((this->home+"/MOCOEFS/mocoef_mc.sp").c_str());
        if(!file){
            PrintOutput::message("MCSF not performed, reading SCF orbitals");
            file.open((this->home+"/MOCOEFS/mocoef_scf.sp").c_str());
            if(!file) ABORT("Could not open mocoef_scf.sp");
        }
        string str;

        //Reading headers on first 6 lines
        for(int i=0;i<6;i++){
            getline(file,str);
            //cout<<str<<endl;
        }

        int no_mo;
        file>>no_mo;
        if(MPIwrapper::isMaster())
            cout<<"No of mos "<<no_mo<<endl;

        //Reading headers on next 3 lines
        for(int i=0;i<4;i++)
            getline(file,str);

        // Now starts the reading of the coefficients
        trans = MatrixXd::Zero(no_mo,no_ao);
        for(int i=0;i<no_mo;i++)
            for(int j=0;j<no_ao;j++)
                file>>trans(i,j);

        //cout<<trans<<endl;
        file.close();
    }

    else if(!this->sym.symbol.compare("d2h") or !this->sym.symbol.compare("D2h")){

        ifstream file((this->home+"/MOCOEFS/mocoef_mc.sp").c_str());
        if(!file){
            PrintOutput::message("MCSF not performed, reading SCF orbitals");
            file.open((this->home+"/MOCOEFS/mocoef_scf.sp").c_str());
            if(!file) ABORT("Could not open mocoef_scf.sp");
        }
        string str;
        do{
            getline(file,str);
        }while(str!="mocoef");
        file >>str;

        // Now starts the reading of coefficients
        int no_mo = sym.orbitals_per_irrep.sum();
        trans = MatrixXd::Zero(no_mo,no_ao);

        int pos=0;
        for(int irr=0; irr<sym.no_of_irreps; irr++){
            for(int i=0; i<sym.orbitals_per_irrep[irr]; i++)
                for(int j=0; j<sym.orbitals_per_irrep[irr]; j++)
                    file>>trans(pos+i,pos+j);
            pos+=sym.orbitals_per_irrep[irr];
        }

        //for(int i=0;i<trans.rows();i++){
        // cout<<trans.row(i)<<endl;
        // cin.get();
        //}
        file.close();
    }

    else{
        cout<<"Read routine not yet implemented for this symmery :"<<sym.symbol<<endl;
        exit(1);
    }

    // Remove the frozen virtual orbitals
    //  if(MPIwrapper::isMasterthreadOfthisNode())
    if(MPIwrapper::isMaster())
        system(("cat "+this->home+"/LISTINGS/cidrtls.sp | grep \"number of frozen virtual orbitals\" > fvos.txt 2>&1").c_str());
    //system(("cat "+this->home+"/LISTINGS/cidrtls.sp | grep \"number of frozen virtual orbitals\" >& fvos.txt").c_str());
    MPIwrapper::Barrier();
    string str;
    int fv;
    fstream f("fvos.txt");
    f >> str; f >> str; f >> str; f >> str; f >> str; f >> str;
    f >> fv;
    f.close();

    MatrixXd temp = trans;
    trans = temp.block(0,0,temp.rows()-fv,temp.cols());

    // Remove the frozen virtual orbitals
    //  if(MPIwrapper::isMasterthreadOfthisNode())
    if(MPIwrapper::isMaster())
        system(("cat "+this->home+"/LISTINGS/cidrtls.sp | grep \"number of frozen core orbitals\" > fvos.txt 2>&1").c_str());
    //system(("cat "+this->home+"/LISTINGS/cidrtls.sp | grep \"number of frozen core orbitals\" >& fvos.txt").c_str());
    MPIwrapper::Barrier();
    int fc;
    f.open("fvos.txt");
    f >> str; f >> str; f >> str; f >> str; f >> str; f >> str;
    f >> fc;
    f.close();

    temp = trans;
    trans = temp.block(fc,0,temp.rows()-fc,temp.cols());
}

void columbus_data::read_sao_coefficients(MatrixXd& trans,int no_ao){

    //Reads the transformation coefficients from Atomic orbitals to Symmetry Atomic Orbitals from "hermitls.sp" file in the "LISTINGS" directory
    if(!this->sym.symbol.compare("c1") or !this->sym.symbol.compare("C1") or !this->sym.symbol.compare("c1 ") or !this->sym.symbol.compare("C1 "))
        trans = MatrixXd::Identity(no_ao,no_ao);

    else if(!this->sym.symbol.compare("d2h") or !this->sym.symbol.compare("D2h")){

        trans = MatrixXd::Zero(no_ao,no_ao);

        ifstream file((this->home+"/LISTINGS/hermitls.sp").c_str());
        if(!file) ABORT("Could not open hermitls.sp");
        string str;

        vector<string > check_points;
        check_points.resize(sym.no_of_irreps);
        check_points[0] = "  Symmetry  Ag ( 1)";
        check_points[1] = "  Symmetry  B3u( 2)";
        check_points[2] = "  Symmetry  B2u( 3)";
        check_points[3] = "  Symmetry  B1g( 4)";
        check_points[4] = "  Symmetry  B1u( 5)";
        check_points[5] = "  Symmetry  B2g( 6)";
        check_points[6] = "  Symmetry  B3g( 7)";
        check_points[7] = "  Symmetry  Au ( 8)";

        vector<string > check_points_no;
        check_points_no.resize(sym.no_of_irreps);
        check_points_no[0] = "  No orbitals in symmetry  Ag ( 1)";
        check_points_no[1] = "  No orbitals in symmetry  B3u( 2)";
        check_points_no[2] = "  No orbitals in symmetry  B2u( 3)";
        check_points_no[3] = "  No orbitals in symmetry  B1g( 4)";
        check_points_no[4] = "  No orbitals in symmetry  B1u( 5)";
        check_points_no[5] = "  No orbitals in symmetry  B2g( 6)";
        check_points_no[6] = "  No orbitals in symmetry  B3g( 7)";
        check_points_no[7] = "  No orbitals in symmetry  Au ( 8)";

        int pos =0;
        for(int irr=0; irr<sym.no_of_irreps; irr++){

            do{
                getline(file,str);
                //cout<<str<<endl;
            }while(str.compare(check_points[irr]) and str.compare(check_points_no[irr]));
            getline(file,str);                    //Read the blank line

            // read symmetry Ag
            int r1,r2;
            char sign;
            for(int i=0;i<sym.orbitals_per_irrep[irr];i++){
                getline(file,str);
                istringstream iss(str);
                int str_count=0;
                do{
                    string sub;
                    iss >> sub;
                    str_count++;
                } while (iss);
                str_count-=1;

                if (str_count==7){
                    istringstream iss(str);
                    iss >> r1 >> str >> r2 >> str >> r1 >> sign >> r2;
                    trans(pos+i,r1-1) = 1;
                    if(sign == '+')
                        trans(pos+i,r2-1) = 1;
                    else if(sign == '-')
                        trans(pos+i,r2-1) = -1;
                }
                else if(str_count==5){
                    istringstream iss(str);
                    iss >> r1 >> str >> r2 >> str >> r1;
                    trans(pos+i,r1-1) = 1;
                }

            }
            pos+=sym.orbitals_per_irrep[irr];
        }
        //for(int i=0;i<trans.rows();i++){
        //cout<<trans.row(i)<<endl;
        // cin.get();
        //}

    }

    else{
        cout<<"Reading columbus information for this symmetry "<<this->sym.symbol<<" not yet implemented"<<endl;
        exit(1);
    }

    //cout<<trans<<endl;
}

void columbus_data::read_matrices(){

    //This function is currently implemented to read Overlap, Kinetic energy, potential energy and Electron repulsion integrals at Molecular Orbital level.
    //Potential energy and Electron repulsion integrals are used for further calculations
    //Kinetic Energy and Overlap integrals can be used to check if the molecular orbitals are read correctly. - It would be a compulsary check by default

    //  cout<<"Please make sure that you have executed the python script after columbus calculation"<<endl<<endl;

    int mat_size = sym.orbitals_per_irrep.sum();

    // Remove the frozen virtual orbitals
    //  if(MPIwrapper::isMasterthreadOfthisNode())
    if(MPIwrapper::isMaster())
        //system(("cat "+this->home+"/LISTINGS/cidrtls.sp | grep \"number of frozen virtual orbitals\" >& fvos.txt").c_str());
        system(("cat "+this->home+"/LISTINGS/cidrtls.sp | grep \"number of frozen virtual orbitals\" > fvos.txt 2>&1").c_str());
    MPIwrapper::Barrier();
    string str;
    int fv;
    fstream f("fvos.txt");
    f >> str; f >> str; f >> str; f >> str; f >> str; f >> str;
    f >> fv;
    f.close();

    // Remove the frozen virtual orbitals
    //  if(MPIwrapper::isMasterthreadOfthisNode())
    if(MPIwrapper::isMaster())
        system(("cat "+this->home+"/LISTINGS/cidrtls.sp | grep \"number of frozen core orbitals\" > fvos.txt 2>&1").c_str());
    //system(("cat "+this->home+"/LISTINGS/cidrtls.sp | grep \"number of frozen core orbitals\" >& fvos.txt").c_str());
    MPIwrapper::Barrier();
    int fc;
    f.open("fvos.txt");
    f >> str; f >> str; f >> str; f >> str; f >> str; f >> str;
    f >> fc;
    f.close();

    mat_size -= (fc+fv);

    //Read Overlap integrals
    ifstream file((this->home+"/WORK/Overlap").c_str());
    if(!file) ABORT("Could not open WORK/Overlap");
    Overlap = MatrixXd::Zero(mat_size,mat_size);

    while(!file.eof()){
        double integ;
        int ind1, ind2;
        file >> integ>> ind1 >> ind2;

        Overlap(ind1-1,ind2-1) = integ;
        Overlap(ind2-1,ind1-1) = integ;
    }
    file.close();

    //Read Kinetic Energy matrix elements
    file.open((this->home+"/WORK/Kinetic_Energy").c_str());
    KineticEnergy = MatrixXd::Zero(mat_size,mat_size);

    while(!file.eof()){
        double integ;
        int ind1, ind2;
        file >> integ>> ind1 >> ind2;

        KineticEnergy(ind1-1,ind2-1) = integ;
        KineticEnergy(ind2-1,ind1-1) = integ;
    }
    file.close();

    // Read Potential Energy matrix elements
    file.open((this->home+"/WORK/Potential_Energy").c_str());
    Potential = MatrixXd::Zero(mat_size,mat_size);

    while(!file.eof()){
        double integ;
        int ind1, ind2;
        file >> integ>> ind1 >> ind2;

        Potential(ind1-1,ind2-1) = integ;
        Potential(ind2-1,ind1-1) = integ;
    }
    file.close();

    //Read Electron repulsion integrals and indices

    // First calculate the number of non-zero matrix elements
    file.open((this->home+"/WORK/Electron_Repulsion").c_str());
    int count = 0;
    while(!file.eof()){
        string str;
        getline(file,str);
        count++;
    }
    file.close();

    //cout<<"Not reading 2-e integrals"<<endl;
    //return;

    // Now copy the matrix elements
    Elec_Rep_Ind = Matrix<int, Dynamic, 4>::Zero(count,4);
    Elec_Rep_Int = VectorXd::Zero(count);
    file.open((this->home+"/WORK/Electron_Repulsion").c_str());

    count=0;
    while(!file.eof()){
        int ind1,ind2,ind3,ind4;
        double integ;
        file >> integ >> ind1 >> ind2 >> ind3 >> ind4 ;
        if(ind1>Overlap.rows() or ind2>Overlap.rows() or ind3>Overlap.rows() or ind4>Overlap.rows())  {
            cout<<count<<endl;
            continue;
            //ABORT("readcolumbusdata::readmatrices(): error with indices");
        }
        Elec_Rep_Int(count) = integ;
        Elec_Rep_Ind.row(count) = RowVector4i(ind1-1,ind2-1,ind3-1,ind4-1);
        count++;
    }
    file.close();

    vee = new My4dArray_shared(Overlap.rows());
    //  if(MPIwrapper::isMaster()){
    //      vee.resize(Overlap.rows(),Overlap.rows(),Overlap.rows(),Overlap.rows());
    for(int i=0;i<Elec_Rep_Int.size();i++){
        int I = Elec_Rep_Ind(i,0);
        int J = Elec_Rep_Ind(i,1);
        int K = Elec_Rep_Ind(i,2);
        int L = Elec_Rep_Ind(i,3);
        vee->assign(I,J,K,L,Elec_Rep_Int(i));
        vee->assign(J,I,K,L,Elec_Rep_Int(i));
        vee->assign(I,J,L,K,Elec_Rep_Int(i));
        vee->assign(J,I,L,K,Elec_Rep_Int(i));
        vee->assign(K,L,I,J,Elec_Rep_Int(i));
        vee->assign(L,K,I,J,Elec_Rep_Int(i));
        vee->assign(K,L,J,I,Elec_Rep_Int(i));
        vee->assign(L,K,J,I,Elec_Rep_Int(i));
    }
    //    }
    MPIwrapper::Barrier();
}


void columbus_data::read_determinants(vector<vector<int > >& det, VectorXcd& coef, int state ){

    if(!this->sym.symbol.compare("c1") or !this->sym.symbol.compare("C1") or !this->sym.symbol.compare("c1 ") or !this->sym.symbol.compare("C1 ")){

        int ff,no_states,no_dets;

        ff=open((this->home+"/WORK/eivectors.red").c_str(),O_RDONLY);
        int i=read(ff,&no_states,sizeof(no_states));
        if(state>no_states){
            cout<<"columbus_data::read_determinants - the state asked for does not exist"<<endl;
            exit(1);
        }

        if(i==-1)
            cout<<"Error occured while reading : read_determinants()"<<endl;
        i=read(ff,&no_dets,sizeof(no_dets));
        if(i==-1)
            cout<<"Error occured while reading : read_determinants()"<<endl;

        if(MPIwrapper::isMaster())
            cout<<"Number of determinants in the calculation: "<<no_dets<<endl;

        coef = VectorXcd::Zero(no_dets);

        for(int n=0;n<no_states;n++)
            for(int j=0;j<no_dets;j++){
                double c;
                i=read(ff,&c,sizeof(c));
                if(n==state-1)
                    coef(j) = c;
            }

        det.resize(no_dets);
        for(int ii=0;ii<no_dets;ii++)
            det[ii].resize(no_electrons);

        ff=open((this->home+"/WORK/slaterfile.red").c_str(),O_RDONLY);
        for(int j=0;j<no_dets;j++)
            for(int l=0;l<no_electrons;l++){
                short m;
                i=read(ff,&m,sizeof(m));
                det[j][l] = m;
            }
    }

    else if(!this->sym.symbol.compare("d2h") or !this->sym.symbol.compare("D2h")){

        int ff,no_states,no_dets;

        ff=open((this->home+"/WORK/eivectors.red").c_str(),O_RDONLY);
        int i=read(ff,&no_states,sizeof(no_states));
        if(i==-1)
            cout<<"Error occured while reading : read_determinants()"<<endl;
        i=read(ff,&no_dets,sizeof(no_dets));
        if(i==-1)
            cout<<"Error occured while reading : read_determinants()"<<endl;

        //cout<<"Number of determinants in the calculation: "<<no_dets<<endl;
        coef = VectorXcd::Zero(no_dets);

        for(int n=0;n<no_states;n++)
            for(int j=0;j<no_dets;j++){
                double c;
                i=read(ff,&c,sizeof(c));
                if(n==state-1)
                    coef(j) = c;
            }

        det.resize(no_dets);
        for(int ii=0;ii<no_dets;ii++)
            det[ii].resize(no_electrons);

        ff=open((this->home+"/WORK/slaterfile.red").c_str(),O_RDONLY);
        for(int j=0;j<no_dets;j++){
            for(int l=0;l<no_electrons;l++){
                short m;
                i=read(ff,&m,sizeof(m));
                if(i==-1)
                    cout<<"Error occured while reading : read_determinants()"<<endl;
                int irrp_no = abs(m/4096);                    // columbus way of encoding the function index
                int fn_no = m%4096;                           // columbus way of encoding the function index
                det[j][l] = (this->sym.orbitals_per_irrep.segment(0,irrp_no).sum()+abs(fn_no))*(fn_no/abs(fn_no));
                //cout<<det[j][l]<<"  ";
            }
            //cout<<endl;
            //cin.get();
        }

        //Remove the terms below the tolerance limit
        vector<vector<int > > det_temp;
        vector<complex<double > > coef_temp;

        double tol = cif.det_tol;
        for(int i=0;i<coef.size();i++)
            if(abs(coef(i))>tol){
                coef_temp.push_back(coef(i));
                det_temp.push_back(det[i]);
            }

        coef = VectorXcd::Zero(coef_temp.size());
        for(int i=0;i<coef.size();i++)
            coef(i) = coef_temp[i];
        det = det_temp;

        if(MPIwrapper::isMaster())
            cout<<"Number of determinants in the calculation within the tolerance "<<tol<<" is "<<coef.size()<<endl;
    }

    else{
        cout<<"columbus_data::read_determinants: Reading columbus information for this symmetry "<<this->sym.symbol<<" not yet implemented"<<endl;
        exit(1);
    }


}

void columbus_data::read_determinants_allstates(vector<vector<int > >& det, vector<VectorXcd >& coef){

    if(!this->sym.symbol.compare("c1") or !this->sym.symbol.compare("C1") or !this->sym.symbol.compare("c1 ") or !this->sym.symbol.compare("C1 ")){

        int ff,no_states,no_dets;

        ff=open((this->home+"/WORK/eivectors.red").c_str(),O_RDONLY);
        int i=read(ff,&no_states,sizeof(no_states));
        if(i==-1)
            cout<<"Error occured while reading : read_determinants()"<<endl;
        i=read(ff,&no_dets,sizeof(no_dets));
        if(i==-1)
            cout<<"Error occured while reading : read_determinants()"<<endl;

        cout<<"Number of determinants in the calculation: "<<no_dets<<endl;
        coef.resize(no_states);

        for(int n=0;n<no_states;n++){
            coef[n] = VectorXcd::Zero(no_dets);
            for(int j=0;j<no_dets;j++){
                double c;
                i=read(ff,&c,sizeof(c));
                coef[n](j) = c;
            }
        }

        det.resize(no_dets);
        for(int ii=0;ii<no_dets;ii++)
            det[ii].resize(no_electrons);

        ff=open((this->home+"/WORK/slaterfile.red").c_str(),O_RDONLY);
        for(int j=0;j<no_dets;j++)
            for(int l=0;l<no_electrons;l++){
                short m;
                i=read(ff,&m,sizeof(m));
                det[j][l] = m;
            }
    }

    else if(!this->sym.symbol.compare("d2h") or !this->sym.symbol.compare("D2h")){

        int ff,no_states,no_dets;

        ff=open((this->home+"/WORK/eivectors.red").c_str(),O_RDONLY);
        int i=read(ff,&no_states,sizeof(no_states));
        if(i==-1)
            cout<<"Error occured while reading : read_determinants()"<<endl;
        i=read(ff,&no_dets,sizeof(no_dets));
        if(i==-1)
            cout<<"Error occured while reading : read_determinants()"<<endl;

        cout<<"Number of determinants in the calculation: "<<no_dets<<endl;
        coef.resize(no_states);

        for(int n=0;n<no_states;n++){
            coef[n] = VectorXcd::Zero(no_dets);
            for(int j=0;j<no_dets;j++){
                double c;
                i=read(ff,&c,sizeof(c));
                coef[n](j) = c;
            }
        }

        det.resize(no_dets);
        for(int ii=0;ii<no_dets;ii++)
            det[ii].resize(no_electrons);

        ff=open((this->home+"/WORK/slaterfile.red").c_str(),O_RDONLY);
        for(int j=0;j<no_dets;j++){
            for(int l=0;l<no_electrons;l++){
                short m;
                i=read(ff,&m,sizeof(m));
                if(i==-1)
                    cout<<"Error occured while reading : read_determinants()"<<endl;
                int irrp_no = abs(m/4096);                    // columbus way of encoding the function index
                int fn_no = m%4096;                           // columbus way of encoding the function index
                det[j][l] = (this->sym.orbitals_per_irrep.segment(0,irrp_no).sum()+abs(fn_no))*(fn_no/abs(fn_no));
                //cout<<det[j][l]<<"  ";
            }
        }
    }

    else{
        cout<<"Reading columbus information for this symmetry "<<this->sym.symbol<<" not yet implemented- function read_determinants_allstates"<<endl;
        exit(1);
    }

}

int columbus_data::read_multiplitcity()
{
    ifstream file;
    file.open((this->home+"/cidrtin").c_str());
    if(!file) ABORT("Could not open cidrtin");

    string str;
    int mult;
    getline(file,str);
    file >> mult;

    return mult;
}
