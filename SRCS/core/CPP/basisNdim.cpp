// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "basisNdim.h"
#include "readInput.h"

#include "operatorDefinition.h"
#include "useMatrix.h"
#include "index.h"
#include "tools.h"
#include "coordinateTrans.h"
#include "operatorNdim.h"
#include "basisProd.h"
#include "basisPolarOff.h"
#include "basisPolar2D.h"
#include "printOutput.h"
#include "str.h"
#include "basisMat.h"
#include "operatorData.h"
#include "inverseDvr.h"
#include "discretizationHybrid.h"

using namespace std;

std::map<const std::string,std::map<const Index*,std::map<const Index*,UseMatrix > > > BasisNdim::opers;
std::map<std::string,const BasisAbstract*> BasisNdim::bases;

std::map<std::string,const OperatorTree*> BasisNdim::basicOp;

const BasisAbstract* BasisNdim::factory(std::string Name){
    if(bases.count(Name)==0)return 0;
    return bases[Name];
}


void BasisNdim::read(ReadInput &Inp){
    bases["PolarOffCenter"]=new BasisPolarOff(Inp);
    bases["Polar2D"]=new BasisPolar2D(Inp);
}

static void basisNodes(const Index *Idx,vector<vector<double> > & Node){
    if(Idx->isLeaf())return;

    if(Idx->basisIntegrable()!=0){
        if(Node.size()==0)Node.resize(1,vector<double>());

        for(int k=Node.size()-1;k>=0;k--){
            Node.push_back(Node[k]);
            Node[k].push_back(Idx->basisIntegrable()->lowBound()); // attach new lower boundary
            Node.back().push_back(Idx->basisIntegrable()->upBound());
        }
        basisNodes(Idx->child(0),Node);
    }
    else
        for(int k=0;k<Idx->childSize();k++)basisNodes(Idx,Node);
}

void BasisNdim::matrix(const string &Op, const Index* IIndex, const Index* JIndex, UseMatrix &Mat, complex<double> Multiplier)
{

    if(opers.count(Op)==0 or
            opers[Op].count(IIndex)==0 or
            opers[Op][IIndex].count(JIndex)==0)
    {

        const Index* iRoot=IIndex;
        const Index* jRoot=JIndex;


        // try cast to multi-dimensional
        const BasisNdim * iNdim=iRoot->basisNdim();
        const BasisNdim * jNdim=jRoot->basisNdim();
        if(iNdim==0 and jNdim==0)PrintOutput::warning("at least one basis should be BasisNdim - else use standard product");

        if(iNdim==0){
            vector<vector<double> > node;
            basisNodes(IIndex,node);
        }

        if(iNdim==0){iRoot=jNdim->rootIndex(IIndex);iNdim=BasisProd::factory(iRoot,jRoot);}
        if(jNdim==0){jRoot=iNdim->rootIndex(JIndex);jNdim=BasisProd::factory(jRoot,iRoot);}
        OperatorNdim opNd(Op,iNdim->_quadCoor,jNdim->_quadCoor);

        // auxiliary indexing for fast use in loop
        vector<const Index*> iFloor,jFloor;
        vector<int> iPos,jPos;
        vector<vector<complex<double>*> >mats;
        for(const Index * jF=jRoot->firstFloor();jF!=0;jF=jF->nodeRight(jRoot)){
            jFloor.push_back(jF);
            jPos.push_back(jF->posIndex(jRoot));
        }
        for(const Index * iF=iRoot->firstFloor();iF!=0;iF=iF->nodeRight(iRoot)){
            iFloor.push_back(iF);
            iPos.push_back(iF->posIndex(iRoot));
        }
        for(int iF=0;iF<iFloor.size();iF++){
            mats.push_back(vector<complex<double>*>());
            for(int jF=0;jF<jFloor.size();jF++){
                opers[Op][iFloor[iF]][jFloor[jF]]=UseMatrix::Zero(iFloor[iF]->sizeStored(),jFloor[jF]->sizeStored());
                mats.back().push_back(opers[Op][iFloor[iF]][jFloor[jF]].data());
            }
        }

        // loop through all points
        for(int pt=0;pt<iNdim->_valDer.size();pt++){
            // loop through all terms
            for(int trm=0;trm<opNd.terms();trm++){

                // evaluate function for term
                vector<complex<double> > cgrid;
                complex<double> fij=1.;
                for(int k=0;k<iNdim->_quadGrid[pt].size();k++)
                    cgrid.push_back(iNdim->_comSca[k].xScaled(iNdim->_quadGrid[pt][k]));
                fij=jNdim->_quadWeig[pt]*opNd.term(trm).kernel(cgrid);
                int ivd=opNd.term(trm).ivd();
                int jvd=opNd.term(trm).jvd();

                // add contribution to all matrix elements
                for(int jF=0;jF<jFloor.size();jF++){
                    for(int j=0;j<jFloor[jF]->sizeStored();j++){
                        complex<double> fijBas=fij*jNdim->_valDer[pt][jPos[jF]+j][jvd];
                        for(int iF=0;iF<iFloor.size();iF++){
                            int ij=j*iFloor[iF]->sizeStored();
                            for(int i=0;i<iFloor[iF]->sizeStored();i++,ij++){
                                mats[iF][jF][ij]+=fijBas*iNdim->_valDer[pt][iPos[iF]+i][ivd];
                            }
                        }
                    }
                }
            }
        }

        for(int iF=0;iF<iFloor.size();iF++)
            for(int jF=0;jF<jFloor.size();jF++){
                opers[Op][iFloor[iF]][jFloor[jF]].purge();
            }
    }
    if(opers[Op].count(JIndex)!=0 and opers[Op][JIndex].count(IIndex)!=0){
        if(not (opers[Op][JIndex][IIndex]-opers[Op][IIndex][JIndex].transpose()).isZero(1.e-12*max(1.,opers[Op][JIndex][IIndex].maxAbsVal()))){
            opers[Op][IIndex][JIndex].print("mat IJ");
            opers[Op][JIndex][IIndex].adjoint().print("mat JI");
            ABORT("asymmetric");
        }
    }

    // get operator from storage and multiply
    Mat=opers[Op][IIndex][JIndex]*Multiplier;
}

void BasisNdim::productGridWeig(const Index* Idx, std::vector<std::vector<double> > & Grid, std::vector<double> & Weig,
                                int MinQuad,std::vector<double>GridK,double WeigK)
{
    if(GridK.size()==0){
        Grid.clear();
        Weig.clear();
    }
    if(Idx->isLeaf()){
        Grid.push_back(GridK);
        Weig.push_back(WeigK);
        return;
    }

    UseMatrix pts,wgs;
    //NOTE: orders are somewhat sloppy...
    int addN=max(int(Idx->basisSet()->order()),MinQuad);
    Idx->basisSet()->quadRule(Idx->basisSet()->order()+addN,pts,wgs);

    for(int k=0;k<wgs.size();k++)
        wgs(k)*=Idx->basisSet()->jacobian(pts(k).real());

    GridK.resize(GridK.size()+1);
    for(int k=0;k<pts.size();k++){
        GridK.back()=pts(k).real();
        productGridWeig(Idx->descend(),Grid,Weig,MinQuad,GridK,WeigK*wgs(k).real());
    }
}

const Index* BasisNdim::rootIndex(const Index *Idx) const{

    // find coordinates in sequence, starting from last
    vector<string> coor=tools::splitString(_quadCoor,'.');
    Idx=Idx->firstFloor();
    for(int k=coor.size();k>0;k--)
        while(Idx!=0 and Idx->axisName()!=coor[k-1])Idx=Idx->parent();
    return Idx;
}

void BasisNdim::mainQuadValDer() {
    // transform grid and first derivatives to zero-center
    _comSca.resize(dim()); // cannot complex scale off-center basis

    CoordinateMap toMain=CoordinateTrans::fromCartesian(_quadCoor);
    JacobianMap jacToMain=CoordinateTrans::jacCartesian(_quadCoor);
    IntegrationFactor facMain=CoordinateTrans::integrationFactor(_quadCoor);
    NablaFactor nabMain=CoordinateTrans::nablaFactor(_quadCoor);

    for(int pt=0;pt<_quadGrid.size();pt++){
        // map from ndim to main coordinates (going through shared reference, usually cartesian)
        vector<double>ndimCoor(_quadGrid[pt]);
        _quadGrid[pt]=(toMain(toCartesian(ndimCoor)));

        // adjust quadrature weights and function to refer to main coordinates (origin at 0 0 0)
        double factorRatio=facMain(_quadGrid[pt])/absFactor(ndimCoor);
        _quadWeig[pt]/=pow(factorRatio,2);

        // create the transformation at point
        Eigen::MatrixXd trans=jacobianToNdim(ndimCoor);
        trans=Eigen::Map<Eigen::MatrixXd>(jacToMain(_quadGrid[pt]).data(),dim(),dim())*trans.inverse().eval();
        trans*=factorRatio; // for integrations wrt main coordinate system

        // transform to value and gradient wrt main coordinate system
        for(int ibas=0;ibas<_valDer[pt].size();ibas++){
            for(int i=0;i<dim();i++)_valDer[pt][ibas][1+i]-=
                    _valDer[pt][ibas][0]*nablaFactor(ndimCoor,i)/absFactor(ndimCoor); // d/dr' f - f/r'
            Eigen::Map<Eigen::VectorXcd>(_valDer[pt][ibas].data()+1,dim())=
                    trans*Eigen::Map<Eigen::VectorXcd>(_valDer[pt][ibas].data()+1,dim());
            _valDer[pt][ibas][0]*=factorRatio; // for integrations wrt main coordinate system
            for(int i=0;i<dim();i++)_valDer[pt][ibas][1+i]+=
                    _valDer[pt][ibas][0]*nabMain(_quadGrid[pt],i)/facMain(_quadGrid[pt]); //  + f*r/(r')^2
        }
    }
}
std::string BasisNdim::mainCoor(ReadInput & Inp){
    // get the reference coordinates from input
    string s;
    for(int l=1;not Inp.endCategory("Axis",l);l++)
        if(s.find(Axis::readName(Inp,l))==string::npos)
            s+=Axis::readName(Inp,l)+".";
    if(s.find("Ndim.")!=0)ABORT("need Ndim as first axis, is: "+s);
    return s.substr(5,s.length()-6);
}


std::string BasisNdim::str(int Level) const{
    std::string s="ndim("+_quadCoor+")";

    if(Level==0)return s;

    for(int pt=0;pt<_quadGrid.size();pt++){
        s+="\n"+tools::str(pt)+": "+tools::str(_quadGrid[pt]);
        for(int ibas=0;ibas<size();ibas++){
            s+="\n"+tools::str(ibas,5)+": "+tools::str(_valDer[pt][ibas]);
        }
    }
    return s;
}

void BasisNdim::test(){
    return;
    vector<double> cart;
    for (int k=0;k<dim();k++)cart.push_back(rand()%2048/(2048.));

    CoordinateMap toMain=CoordinateTrans::fromCartesian(quadCoor());
    CoordinateMap fromMain=CoordinateTrans::toCartesian(quadCoor());

    vector<double> orig;
    orig=fromMain(toMain(cart));
    for(int k=0;k<dim();k++)
        if(abs(orig[k]-cart[k])>1.e-12){
            ABORT(Str("transformation to coordinates ")+quadCoor()+"failed, trans="+orig+"init="+cart);
        }
    PrintOutput::DEVmessage("OK coordinate trans to quad="+quadCoor());

    orig=toCartesian(CoordinateTrans::fromCartesian(_ndimCoor)(cart));
    for(int k=0;k<dim();k++)
        if(abs(orig[k]-cart[k])>1.e-12){
            ABORT(Str("transformation to coordinates ")+_ndimCoor+"failed, trans="+orig+"init="+cart);
        }
    PrintOutput::DEVmessage("OK coordinate trans to ndim="+_ndimCoor);
}







