// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "test_evaluator.h"

//#include <boost/test/unit_test.hpp>

#include "evaluator.h"

using namespace tSurffTools;
using namespace tSurffTools::tests;
//using namespace boost::unit_test;

void tSurffTools::tests::test_buffer_double()
{
    CircularQueue<double> myBuffer(10);
    for (int i=0; i!=myBuffer.size(); ++i) {
        myBuffer[i]=double(i);
    }

    // Check operator[]
    DEVABORT("get non-boost");//BOOST_CHECK_EQUAL(myBuffer[-1], 9.);
    DEVABORT("get non-boost");//BOOST_CHECK_EQUAL(myBuffer[-99], 1.);
    DEVABORT("get non-boost");//BOOST_CHECK_EQUAL(myBuffer[10], 0.);
    DEVABORT("get non-boost");//BOOST_CHECK_EQUAL(myBuffer[1000], 0.);
    myBuffer[0]=1.;
    DEVABORT("get non-boost");//BOOST_CHECK_EQUAL(myBuffer[0], 1.);

    // Check operator<<
    for (int i=0; i!=myBuffer.size(); ++i) {
        myBuffer << double(i+10);
        DEVABORT("get non-boost");//BOOST_CHECK_EQUAL(myBuffer[i], double(i+10));
    }
    myBuffer << 0. << 1.; //check concatenation
    DEVABORT("get non-boost");//BOOST_CHECK_EQUAL(myBuffer[0], 0.);
    DEVABORT("get non-boost");//BOOST_CHECK_EQUAL(myBuffer[1], 1.);

    // Check front, back and push_back
    for (int i=0; i!=myBuffer.size(); ++i) {
        myBuffer.pop_back()=double(100+i);
    }
    DEVABORT("get non-boost");//BOOST_CHECK_EQUAL(myBuffer.pop_back(), double(100));
    DEVABORT("get non-boost");//BOOST_CHECK_EQUAL(myBuffer.back(), double(101));
    DEVABORT("get non-boost");//BOOST_CHECK_EQUAL(myBuffer.front(), double(100));
}
