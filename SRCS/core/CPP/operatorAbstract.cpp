// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "operatorAbstract.h"

#include <stdlib.h>

#include "printOutput.h"
#include "index.h"
#include "coefficients.h"
#include "operator.h"
#include "operatorTree.h"
#include "overlapDVR.h"
#include "constrainedView.h"

#include "timer.h"
#include "str.h"
#include "parameters.h"
#include "timeCritical.h"
#include "readInput.h"

using namespace std;

OperatorAbstract::~OperatorAbstract(){
    delete _tempRHS;
    delete _tempLHS;
}

Coefficients* OperatorAbstract::tempLHS() const {
    if(iIndex==0)return 0;
    if(_tempLHS==0){
        const_cast<OperatorAbstract*>(this)->_tempLHS=new Coefficients(iIndex);
        const_cast<OperatorAbstract*>(this)->_tempLHS->treeOrderStorage();
    }
    return _tempLHS;
}
Coefficients* OperatorAbstract::tempRHS() const {
    if(jIndex==0)return 0;
    if(_tempRHS==0){
        timeCritical::suspend(); // exempted from time-critical warnings
        const_cast<OperatorAbstract*>(this)->_tempRHS=new Coefficients(jIndex);
        const_cast<OperatorAbstract*>(this)->_tempRHS->treeOrderStorage();
        timeCritical::resume();
    }
    return _tempRHS;
}
void OperatorAbstract::axpy(std::complex<double> Alfa,const Coefficients& X,std::complex<double> Beta, Coefficients& Y, const double Time)
{
    update(Time);
    apply(Alfa,X,Beta,Y);
}

bool OperatorAbstract::isIdentity(double Eps, bool Stochastic) const {
    tempRHS()->setToRandom();
    *tempLHS()=*tempRHS();
    apply(-1.,*tempRHS(),1.,*tempLHS());
    if(not tempLHS()->isZero(Eps))return false;
    if(not Stochastic) DEVABORT("only stochastic test implemented");
    return true;
}

bool OperatorAbstract::isZero(double Eps, bool Stochastic) const {
    tempRHS()->setToRandom();
    *tempLHS()=*tempRHS();
    apply(-1.,*tempRHS(),0.,*tempLHS());
    if(not tempLHS()->isZero(Eps))return false;
    if(not Stochastic) DEVABORT("only stochastic test implemented");
    return true;
}


UseMatrix OperatorAbstract::matrix(std::vector<Coefficients *> Left, std::vector<Coefficients *> Right) const{
    Coefficients opTimesRight(jIndex);
    UseMatrix res(Left.size(),Right.size());
    for(int j=0;j<Right.size();j++){
        apply(1.,*Right[j],0.,opTimesRight);
        for(int i=0;i<Left.size();i++)
            res(i,j)=Left[i]->innerProduct(&opTimesRight,false);
    }
    return res;
}
Eigen::MatrixXcd OperatorAbstract::matrix() const{
    if(isHuge())return Eigen::MatrixXcd();
    vector<complex<double>> mat;
    matrix(mat);
    return Eigen::Map<Eigen::MatrixXcd>(mat.data(),iIndex->size(),jIndex->size());
}

bool OperatorAbstract::isHuge(std::string Message) const{
    if(ReadInput::main.flag("DEBUGallowHugeMatrix","suppress checks on huge matrices"))return false;
    if(iIndex->size()*jIndex->size()>9000000){
        PrintOutput::warning(Sstr+"huge matrix"+name+"dimensions"+iIndex->size()+"x"+jIndex
                             +"suppress check by command line option -DEBUGallowHugeMatrix"+Message);
        return true;
    }
    return false;
}

void OperatorAbstract::matrix(UseMatrix &Mat) const{
    if(isHuge())return;
    vector<complex<double> > mat;
    matrix(mat);
    Mat=UseMatrix::UseMap(mat.data(),iIndex->sizeCompute(),jIndex->sizeCompute());
}

TIMER(matrix,)
void OperatorAbstract::matrix(std::vector<std::complex<double> > &Mat) const{
    if(isHuge())return;
    STARTDEBUG(matrix);
    if(Parameters::currentTime()==-DBL_MAX){
        Parameters::updateToOne();
        const OperatorTree * oTree=dynamic_cast<const OperatorTree *>(this);
        if(oTree==0 or oTree->def().find("[t]")!=string::npos)
            PrintOutput::DEVwarning("no time has been set - setting all time-dependent parameters =1");
    }

    iIndex->sizeCompute();
    jIndex->sizeCompute();
    Coefficients a(iIndex),b(jIndex);
    a.treeOrderStorage();
    b.treeOrderStorage();
    Mat.assign(iIndex->sizeStored()*jIndex->sizeStored(),0.);
    for(int j=0,ij=0;j<jIndex->sizeStored();j++){
        b.setToZero();
        a.setToZero();
        b.storageData()[j]=1.;
        apply(1.,b,0.,a);
        for(int k=0;k<iIndex->sizeStored();k++,ij++){
            Mat[ij]=a.storageData()[k];
        }
    }
    Parameters::restoreToTime();

	STOPDEBUG(matrix)
}

void OperatorAbstract::matrixAdd(std::complex<double> factor, UseMatrix &Mat) const{
    vector<complex<double> > mat;
    matrix(mat);
    Mat=UseMatrix::UseMap(mat.data(),iIndex->sizeStored(),jIndex->sizeStored());
    matrixContract(Mat,Mat,factor);
}

complex<double> OperatorAbstract::matrixElement(const Coefficients &Ci, const Coefficients &Cj, bool pseudoScalar) const{
    apply(1.,Cj,0.,*tempLHS());
    tempLHS()->makeContinuous();
    return Ci.innerProduct(tempLHS(),pseudoScalar);
}
complex<double> OperatorAbstract::matrixElementUnscaled(const Coefficients &Ci, const Coefficients &Cj) const{
    apply(1.,Cj,0.,*tempLHS());
    complex<double> tmp=Ci.innerProductUnscaled(tempLHS());
    return tmp;
}

void multiplicities(vector<unsigned int>Glob,vector<unsigned int>& Mult){
    for(int k=0;k<Glob.size();k++){
        Mult.push_back(0);
        for(int l=0;l<Glob.size();l++)
            if(Glob[k]==Glob[l])Mult[k]++;
    }
}

TIMER(contCol,)
TIMER(contRow,)
TIMER(contMult,)

/// GMat with continuity imposed (Mat and GMat may be the same UseMatrix)
void OperatorAbstract::matrixContract(const UseMatrix &Mat,UseMatrix & GMat,complex<double> Factor,std::vector<int>ISort,std::vector<int>JSort) const {
    // get global indices
    STARTDEBUG(contMult);
    vector<unsigned int>iglob(iIndex->contractedNumbering());
    vector<unsigned int>jglob(jIndex->contractedNumbering());
    vector<unsigned int>imult,jmult;
    multiplicities(iglob,imult);
    multiplicities(jglob,jmult);

    vector<unsigned int> ig0,jg0,im0,jm0;
    iIndex->contractedNumbering(ig0,im0);
    jIndex->contractedNumbering(jg0,jm0);
    for(int k=0;k<ig0.size();k++){
        if(ig0[k]!=iglob[k] or im0[k]!=imult[k]){
            Sstr+"error"+k+ig0[k]+iglob[k]+im0[k]+imult[k]+Sendl;
        }
    }
    STOPDEBUG(contMult);

    // check matrix dimensions
    if(iglob.size()!=Mat.rows() or jglob.size()!=Mat.cols())
        ABORT("input matrix dimensions do not match index size "
              +tools::str(int(iglob.size()))+" X "+tools::str(int(jglob.size()))+" vs. "+tools::str(Mat.rows())+" "+tools::str(Mat.cols()));

    STARTDEBUG(contCol);
    // contract columns
    UseMatrix cmat=UseMatrix::Zero(Mat.rows(),jIndex->globalLength());
    if(JSort.size()>0 and JSort.size()!=cmat.cols())ABORT("JSort does not match global length");
    for (unsigned int j=0;j<Mat.cols();j++){
        unsigned int js=jglob[j];
        if(JSort.size()>0)js=JSort[jglob[j]];
        cmat.block(0,js,Mat.rows(),1)+=Mat.col(j)*sqrt(1./double(jmult[j]));
    }
    STOPDEBUG(contCol);

    STARTDEBUG(contRow);
    // contract rows
    unsigned int isiz=*std::max_element(iglob.begin(),iglob.end())-*std::min_element(iglob.begin(),iglob.end())+1;
    unsigned int jsiz=*std::max_element(jglob.begin(),jglob.end())-*std::min_element(jglob.begin(),jglob.end())+1;
    GMat=UseMatrix::Zero(isiz,jsiz);
    if(ISort.size()>0 and ISort.size()!=GMat.rows())ABORT("ISort does not match global length");
    for (unsigned int i=0;i<cmat.rows();i++){
        unsigned int is=iglob[i];
        if(ISort.size()>0)is=ISort[iglob[i]];
        // going through UseMatrix is highly inefficient here...
        // GMat.block(is,0,1,cmat.cols())+=cmat.row(i)*Factor*sqrt(1./double(imult[i]));
        for(int k=0,kg=is,kc=i;k<cmat.cols();k++,kg+=GMat.leadDim(),kc+=cmat.leadDim())
            GMat.data()[kg]+=cmat.data()[kc]*Factor*sqrt(1./double(imult[i]));
    }
    STOPDEBUG(contRow);
}

bool OperatorAbstract::isBlockDiagonal() const {
    const OperatorAbstract * op=this;
    if(dynamic_cast<const ConstrainedView*>(this)!=0)dynamic_cast<const ConstrainedView*>(this)->isBlockDiagonal();
    if(dynamic_cast<const OverlapDVR*>(this)!=0)return iIndex->continuity()==Index::npos;
    if(dynamic_cast<const OperatorTree*>(this)!=0)return dynamic_cast<const OperatorTree*>(this)->isBlockDiagonal();
    if(dynamic_cast<const Operator*>(this)!=0)return dynamic_cast<const Operator*>(this)->isBlockDiagonal();
    return false;
}

bool OperatorAbstract::isDiagonal() const {
    DEVABORT("no generic implemementation");
    return false;
}

void OperatorAbstract::subMatrix(std::vector<std::complex<double> > &Mat, const Index *ISub, const Index *JSub) const{
    Coefficients a(iIndex),b(jIndex);
    a.treeOrderStorage();
    b.treeOrderStorage();
    Mat.resize(ISub->sizeStored()*JSub->sizeStored());

    for(int j=JSub->posIndex(jIndex),ij=0;j<JSub->posIndex(jIndex)+JSub->sizeStored();j++){
        b.setToZero();
        b.data()[j]=1.;
        apply(1.,b,0.,a);
        for(int i=ISub->posIndex(iIndex);i<ISub->posIndex(iIndex)+ISub->sizeStored();i++,ij++){
            Mat[ij]=a.data()[i];
        }
    }
}

void OperatorAbstract::orthoNormalizeDegenerate(std::vector<std::complex<double> > Eval, std::vector<Coefficients *> Evec, bool Pseudo) const{
    double epsRelDegen=1.e-12,epsAbsDegen=1.e-8; // level where to consider eigenvalues degenerated

    if(Eval.size()!=Evec.size())ABORT("number of eigenvalues and eigenvectors does not match");

    // orthonormalize (near-)degenerate subspaces
    for(int k=0;k<Evec.size();k++){
        vector<Coefficients*>sub(1,Evec[k]);
        for(int l=k+1;l<Eval.size();l++){
            if(abs(Eval[l]-Eval[k])<epsRelDegen*abs(Eval[k])
                    or abs(Eval[l]-Eval[k])<epsAbsDegen)
                sub.push_back(Evec[l]);
        }
        orthoNormalize(sub,Pseudo);
    }
}

void OperatorAbstract::orthoNormalize(std::vector<Coefficients *> & C, bool pseudo) const {
    if(C.size()==0)return;
    Coefficients *sCk=tempLHS();
    for(int k=0;k<C.size();k++){
        apply(1.,*C[k],0.,*sCk);
        for(int l=0;l<k;l++)C[k]->axpy(-C[l]->innerProduct(sCk,pseudo),C[l]);
        complex<double> a=C[k]->innerProduct(sCk,pseudo);
        if(abs(a)<1.e-12){
            if(abs(a)==0.)ABORT("vectors linearly dependent");
            PrintOutput::warning("near-linearly dependent vectors");
        }
        *C[k]*=1./sqrt(a);
    }
}

// TODO: Copied from operatorFloor.cpp...
static Timer TIMERcost("cost","INTERNAL","operatorAbstract.cpp");
double OperatorAbstract::applicationCost() const{
    
    Coefficients x(jIndex);
    Coefficients y(iIndex);

    x.treeOrderStorage();
    y.treeOrderStorage();

    x.setToRandom();
    y.setToRandom();

    TIMERcost.start();
    TIMERcost.stopTimer();
    double startSecs=TIMERcost.secs();
    TIMERcost.start();

    for(unsigned int k=0;k<10;k++){
        apply(1.,x,0.,y);
        apply(1.,x,0.,y);
        apply(1.,x,0.,y);
        apply(1.,x,0.,y);
        apply(1.,x,0.,y);
        apply(1.,x,0.,y);
        apply(1.,x,0.,y);
        apply(1.,x,0.,y);
        apply(1.,x,0.,y);
        apply(1.,x,0.,y);
    }

    TIMERcost.stopTimer();
    return 1.e5*(TIMERcost.secs()-startSecs);

}

double OperatorAbstract::norm() const{ABORT("not implemented");}

std::string OperatorAbstract::str(std::string Info) const
{
    Str s("");
    s=s+iIndex->index()+" | "+jIndex->index();
    if(Info=="")return s+name+": "+iIndex->hierarchy()+" <-- "+jIndex->hierarchy();
    else return s+Info;
}
