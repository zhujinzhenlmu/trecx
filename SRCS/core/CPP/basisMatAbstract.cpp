#include "basisMatAbstract.h"

const UseMatrix BasisMatAbstract::useMat() const {
    return UseMatrix::UseMap(const_cast<BasisMatAbstract*>(this)->_mat.data(),_mat.rows(),_mat.cols());
}

const std::vector<const Eigen::MatrixXcd*> BasisMatAbstract::mats() const {
    return std::vector<const Eigen::MatrixXcd*>(1,&_mat);
}
