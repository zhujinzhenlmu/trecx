#include "coulXDerNew.h"
#include "inverse.h"
#include "parameters.h"
#include "coefficients.h"
#include "operatorTree.h"
#include "surfaceConstraint.h"
#include "index.h"

CoulXDerNew::CoulXDerNew(const OperatorAbstract *Op, SurfaceConstraint *SurfConstr)
    :_op(Op), _inv(Op->iIndex->inverseOverlap()),_surfConstr(SurfConstr),OperatorAbstract("CoulXDerNew",Op->iIndex,Op->jIndex)
{
    tempCoeffs = new Coefficients(Op->iIndex,0.);
}

CoulXDerNew::~CoulXDerNew(){
    if(tempCoeffs!=0) delete tempCoeffs; tempCoeffs=0;
}

void CoulXDerNew::apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    if(A!=1.)DEVABORT("A!=1 not implemented for CoulXDer::apply()!");
    if(B!=0.)DEVABORT("B!=0 not implemented for CoulXDer::apply()!");
    _op->apply(std::complex<double>(0.,-1.)*A,Vec,B,Y);
    _inv->apply(1.,Y,0.,*tempCoeffs);
    Y=*tempCoeffs;
    _surfConstr->apply(1.,*tempCoeffs,1.,Y);
}

void CoulXDerNew::update(double Time, const Coefficients* CurrentVec){
    time=Time;
    Parameters::update(Time);
    _surfConstr->update(Time);
}
