// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "evaluator6D.h"

#include <algorithm>
//#include <boost/math/special_functions/bessel.hpp>
//#include <boost/lexical_cast.hpp>
#include <fstream>
#include <math.h>

#include "newtonInterpolator.h"
#include "tools.h"
#include "readInput.h"
#include "readInputList.h"
#include "pulse.h"
#include "discretization.h"
#include "discretizationDerived.h"
#include "discretizationSurface.h"
#include "discretizationtsurffspectra.h"
#include "discretizationHybrid.h"
#include "operatormapchannelssurface.h"
#include "discretizationFactor.h"
#include "basicDisc.h"
#include "operator.h"
#include "coefficients.h"
#include "coefficientsFloor.h"
#include "plot.h"
#include "index.h"
#include "axis.h"
#include "surfaceFlux.h"
#include "tsurffSource.h"
#include "asciiFile.h"
#include "permuteOperatorTree.h"
#include "coefficientsWriter.h"

#include "printOutput.h"
#include "read_columbus_data.h"

using namespace std;
using namespace tSurffTools;

//const double Evaluator6D::timeResolution = 0.1;
const string Evaluator6D::inputExtension = "inpc";


Evaluator6D::Evaluator6D(ReadInput &inp) :
    D(0),
    tSource(0),
    result(0),
    beginAveraging(DBL_MAX),
    averagedResult(0),
    overwrite(true),
    channelHelper(0)
{
    PrintOutput::title("Spectra for "+inp.output());
    // get the basics from file
    outputDir=inp.output();
    string amplFile=inp.output()+"ampl";
    Pulse::read(inp,false);
    ReadInput::main.read("Evaluator", "noOverwrite",overwrite,ReadInput::flagOnly, "suppress overwriting of spectrum",1,"noOvr");
    overwrite=not overwrite;
    ReadInput::main.read("Plot","what",plotWhat,"partial",
                         "read from ampl-file and create plot of:"
                         +string(" grid[file]...draw the plot info from file")
                         +string(" total, partial, partialIon")
                         +string(", intPhi...2d integrated over Phi")
                         +string(", intK...integrated over phi and k")
                         +string(", cutPhi[n]...n cuts at phi angles")
                         +string(", cutEta[n]...n cuts at eta=cos(theta)")
                         +string(", sumZ...sum over Z (for XZ or YZ)")
                         +string(", sumY...sum over Y (for XY or YZ)")
                         +string(", sumX...sum over X (for XY or XZ)")
                         ,1,"plot");
    // check whether grid file can be opened
    if(plotWhat.find("grid")==0){
        char * dum[1];
        ReadInput(tools::stringInBetween(plotWhat,"[","]"),1,dum);
    }

    ReadInput::main.read("Plot","ampl",amplNum,"-1","specify amplitude file is to read",1,"ampl");
    ReadInput::main.read("Plot","compute",forceCompute,ReadInput::flagOnly,"force re-computation of ampl file",1,"compute");

    // standard benchmark calculations
    if(ReadInput::main.found("Spectrum","benchmark","benchmark")){
        plotWhat="benchmark";
        forceCompute=true;
    }

    // set up discretizations
    D =Discretization::factory(inp);

    // read flag values, print output
    bool kGrid=true;
    int radialPoints;
    double maxEnergy;
    getFlags(inp,radialPoints,maxEnergy,kGrid);
    if(D->continuityLevel.size()<2)
        print(radialPoints,maxEnergy,kGrid);
    // Surface discretization
    vector<double> radii;
    inp.read("Surface","points",radii,"","save values and derivatives at surface points (blank-separated list)");

    string ChanAxis;
    string Chandef="";
    inp.read("Channel","Axis",ChanAxis,Chandef,"Ionic channel Axis");
    vector<DiscretizationSurface* > S;
    if(ChanAxis==Chandef){
        if(D->idx()->continuity(1)==Index::npos){
            S.push_back(DiscretizationSurface::factory(D, radii, 0));
        }
        else if(D->idx()->continuity(2)==Index::npos){
            for(int k=0;k<2;k++){
                // not general case for region names, but shorter and faster
                ReadInputList inpList(inp.outputTopDir()+"S_"+D->getAxis()[D->continuityLevel[k]].name+"/linp");
                S.push_back(new DiscretizationSurface(new DiscretizationTsurffSpectra(new DiscretizationSurface(D,radii,k),inpList),radii,0));
            }
        }
        else{
            ABORT("Unknown case for hierarchy "+tools::str(D->idx()->hierarchy()));
        }
    }
    else{//channel spectra
        channelHelper = new OperatorMapChannelsSurface(inp,D);
        S.push_back(channelHelper->pointerToChannelSurface());
    }
    PrintOutput::newLine();

//    for(unsigned int k=0;k<S.size();k++)tSource.push_back(new TsurffSource(0, inp,S,Region,k));

    tSource.push_back(new TsurffSource({"Rn1","Rn2"},D->idx(),inp));
    tSource.push_back(new TsurffSource({"Rn2","Rn1"},D->idx(),inp));
    PrintOutput::paragraph();
    result = new Coefficients(tSource[0]->idx(), 0.);   // using the 0th. All others should be the same
    averagedResult = new Coefficients(*result);
    averagedResult->treeOrderStorage();

    // determine whether re-compute is needed
    forceCompute=forceCompute or not folder::exists(amplFile);
    if(not forceCompute){
        if(amplFile+"0"!=tools::newFile(amplFile)){
            if(amplNum=="-1")ABORT("there are multiple amplitude files, specify by -ampl=n, -ampl=x for no number");
            if(amplNum!="x")amplFile+=amplNum;
            PrintOutput::message("reading from amplitude file "+amplFile);
        }
        ifstream ampl(amplFile.c_str(),(ios_base::openmode) ios::beg|ios::binary);
        if(averagedResult->read(ampl,true)){
            int code;
            ampl>>code;
        } else {
            PrintOutput::message("data on "+outputDir+"ampl do not match input, ommit all flags or specify -compute for re-compute");
            exit(1);
        }
    }

}

void Evaluator6D::getFlags(ReadInput & Inp,int & radialPoints, double & maxEnergy, bool & kGrid){

    // default values
    Inp.read("DEBUG","kpoints",DiscretizationTsurffSpectra::_defaultRadialPoints,"100","change radial points, make sure to use consistently",1,"DEBUGkpoints");
    radialPoints=DiscretizationTsurffSpectra::_defaultRadialPoints;
    bool eGrid=false;
    maxEnergy=max(3*Pulse::current.omegaMax(),12.*Pulse::current.uPonderomotive());

    // try get flags from spec-file
    ifstream spec((outputDir+"spec").c_str(),std::ios::in);
    string line;
    string defAver,defStep,defTmax;
    while(not forceCompute and getline(spec,line) and line[0]=='#'){
        if(line.find("-nR="   )!=string::npos)radialPoints=tools::string_to_int(line.substr(line.find("=")+1));
        if(line.find("-eGrid=")!=string::npos)eGrid=tools::string_to_bool(line.substr(line.find("=")+1));
        if(line.find("-Emax=" )!=string::npos)maxEnergy=tools::string_to_double(line.substr(line.find("=")+1));
        if(line.find("-tStep=")!=string::npos)defStep=line.substr(line.find("=")+1);
        if(line.find("-tAver=")!=string::npos)defAver=line.substr(line.find("=")+1);
        if(line.find("-tMax=" )!=string::npos)defTmax=line.substr(line.find("=")+1);
    }


    // overwrite flags by input
    double minEnergy;
    DiscretizationTsurffSpectra::readFlags(Inp,radialPoints,minEnergy,maxEnergy,kGrid,D->continuityLevel.size()<2);

    double tAveraging;
    Inp.read("TimePropagation","end",runPropagationTime,tools::str(Pulse::gettEnd(),12),"end of time-propagation in run original",1);
    integrationTime=runPropagationTime;
    if(defAver=="")defAver=tools::str(4.*math::pi/Pulse::current.omegaMin());
    if(defTmax=="")defTmax=tools::str(integrationTime);
    if(defStep=="")defStep=tools::str(2*math::pi/(16*maxEnergy));
    ReadInput::main.read("Spectrum","timeResolution", timeResolution,defStep, "time-integration step, default = 2pi/(16 Emax)",1,"tStep");
    ReadInput::main.read("Spectrum","tMax",integrationTime,defTmax,"time limit for integration",1,"tMax");
    ReadInput::main.read("Spectrum","averaging", tAveraging,defAver, "interval for averaging, default = 1 OptCyc",1,"tAver");
    beginAveraging=max(Pulse::gettEnd(),integrationTime-tAveraging); // for finite pulses, endPrint=end of pulse

    // these will be saved into spec-file
    inputFlags.push_back("-nR="+tools::str(radialPoints));
    inputFlags.push_back("-tMax="+tools::str(integrationTime));
    inputFlags.push_back("-tAver="+tools::str(tAveraging));
    inputFlags.push_back("-Emax="+tools::str(maxEnergy));
    inputFlags.push_back("-eGrid="+tools::str(not kGrid));
    inputFlags.push_back("-tStep="+tools::str(timeResolution));

}

void Evaluator6D::print(int radialPoints, double maxEnergy,bool kGrid) const {
    PrintOutput::paragraph();
    string type="energy";
    if(kGrid)type="momentum";
    PrintOutput::lineItem("points",radialPoints);
    PrintOutput::lineItem("kind",type);
    PrintOutput::newLine();
    PrintOutput::lineItem("maxEnergy",maxEnergy);
    PrintOutput::newLine();
    PrintOutput::lineItem("integration",integrationTime);
    PrintOutput::lineItem("step",timeResolution);
    PrintOutput::newLine();
    PrintOutput::lineItem("averaging time",max(0.,integrationTime-beginAveraging));
    // warn about too short averaging
    if(integrationTime-beginAveraging<2.*math::pi/Pulse::current.omegaMin()*(1.-1.e-10)){
        PrintOutput::newLine();
        string mess("spectral averaging time below recommended minimum of 1 OptCyc = ");
        mess+=tools::str(2.*math::pi/Pulse::current.omegaMin(),5)+"\n";
        mess+="artefacts due to Rydberg population may appear\n";
        if(integrationTime-Pulse::gettEnd()>=2.*math::pi/Pulse::current.omegaMin()*(1.-1.e-10))
            mess+="input larger Spectrum:averaging or leave blank for default";
        else
            mess+="tRecX time propagation too short - redo with at least 1 OptCyc after end of pulse";
        PrintOutput::warning(mess);
    }
    PrintOutput::paragraph();

}

Evaluator6D::~Evaluator6D() {
    if (result!=0) {if(result!=averagedResult)delete result;result=0;}
    if (averagedResult!=0) {delete averagedResult;}
    if (tSource.size()!=0)
        for(unsigned int k=0;k<tSource.size();k++){
            delete tSource[k]; tSource[k]=0;
        }
    if(channelHelper!=0) {delete channelHelper; channelHelper=0;}
}

void Evaluator6D::computeSpectrum(double Tend)
{
    if(not forceCompute)return; // no computing desired

    for(int k=0;k<tSource.size();k++)
        if(tSource[k]->SourceBufferBeginTime()==DBL_MAX)
            PrintOutput::title(Str("no source no.")+k+"- end of calculation");

    PrintOutput::message("(re-)computing spectral amplitudes");

    Coefficients *WfSpecDisc;
    double beg_time = tSource[0]->SourceBufferBeginTime();
    for(unsigned int src=1;src<tSource.size();src++) beg_time = max(beg_time,tSource[src]->SourceBufferBeginTime());

    int percNext=0;
    averagedResult->setToZero();
    result->setToZero();

    Coefficients tmpWriter(result->idx());
    SiSpec writer(result->idx(), outputDir);

    for(double time = beg_time; time<=integrationTime;){

        // monitor progress
        int perc=100.*(time-beg_time+integrationTime*1.e-12)/(integrationTime-beg_time);
        if(percNext<=perc){
            if(percNext==0){
                PrintOutput::subTitle("\n integrating for spectra...");
                PrintOutput::newRow();
                PrintOutput::rowItem("(%)");
                PrintOutput::rowItem("  time ");
            }
            percNext=perc+1;
            PrintOutput::newRow();
            PrintOutput::rowItem(perc);
            PrintOutput::rowItem(time);
            PrintOutput::flush();
        }

        for(unsigned int src=0;src<tSource.size();src++){
            // update source to time
            tSource[src]->UpdateSource(time);
            WfSpecDisc = tSource[src]->CurrentSource();
            *result+=(*WfSpecDisc);
            if (time>=beginAveraging)*averagedResult+=(*result);
        }

        time += timeResolution;

        tmpWriter = *result;
        tmpWriter *= timeResolution;
        writer.average(&tmpWriter, time);

        if(time>integrationTime) {integrationTime=time; break;}     // If the integration limit misses by one step, just change it,
    }
    *result*=timeResolution;
    *averagedResult*=(pow(timeResolution,2)/(integrationTime-beginAveraging));
    if (averagedResult->isZero(1.e-12)) {averagedResult=result;} //probably averaging supposed to start after time propagation
    PrintOutput::paragraph();
    // save to file
    string amplFile=outputDir+"ampl";
    if(not overwrite)amplFile=tools::newFile(amplFile);
    // save the spectral amplitudes to binary file
    ofstream ampl(amplFile.c_str(),(ios_base::openmode) ios::beg|ios::binary);
    averagedResult->write(ampl,true);
    ampl<<int(0); // dummy code - for later use
}

// count how often the input grid was read
static int readGridInput=0;

//void Evaluator6D::plotSpectrum(ReadInput &Inp,const Coefficients & SpecInf, double Tend, const Coefficients * ExternalSmooth,string ExtName)
void Evaluator6D::plotSpectrum(ReadInput &Inp, const Coefficients & AmplitudeSmooth,string ExtName)
{

    const Discretization* Disc=D;
    if(dynamic_cast<const DiscretizationHybrid*>(D)!=0)Disc=dynamic_cast<const DiscretizationHybrid*>(D)->haCC();

    string specFile,amplFile=outputDir+"ampl";
    specFile=outputDir+"spec"+ExtName;
    if(not overwrite)specFile=tools::newFile(specFile);

    // spectrum parameters
    vector<string> head(inputFlags);
    head.insert(head.begin(),1,string("--- flag inputs (or default values) ---"));

    if(plotWhat.find("grid")==0){
        // read plot definition from file
        string file=tools::stringInBetween(plotWhat,"[","]");
        if(file==plotWhat)ABORT("specify file name as grid[fileName]");

        ReadInput gridInp(file,-1);
        Plot plot(tSource[0]->smoothSpecDisc,gridInp,true);
        gridInp.finish();
        if(1>readGridInput++)plot.print();// print only once
        plot.plot(*averagedResult,specFile,head,"",true);
    }
    else {
        switch (Disc->getAxis().size()) {
        case 1:
        {
            // for maintenance purposes, this should be done through Plot-class
            // for 1d, there is only one sensible way to plot:

            vector<string>axis,use;
            vector<unsigned int> points(1,0);
            vector<vector<double> >bounds(1,vector<double>(2,0.));
            axis.push_back("kX");
            use.push_back("g");
            Plot plot(tSource[0]->idx(),axis,use,points,bounds);
            plot.print();
            plot.plot(AmplitudeSmooth,specFile,head,"",true);
            break;
        }
        case 2:
        {
            vector<string>axis,use;
            vector<unsigned int> points(1,0);
            vector<vector<double> >bounds(1,vector<double>(2,0.));
            for(int i=0; i<D->continuityLevel.size();i++){
                axis.push_back("k"+D->getAxis()[D->continuityLevel[i]].name);
            }
            use.push_back("g");
            points.push_back(0);
            bounds.push_back(vector<double>(2,0.));
            use.push_back("g");

            // If one has the axes X and Y, -plot=sumZ is possible.
            // In that case, the partial spectrum is computed.
            // Maybe, this should be changed.
            if(plotWhat=="sum"+D->getAxis()[D->continuityLevel[1]].name)
                use.back()="s";
            else if(plotWhat=="sum"+D->getAxis()[D->continuityLevel[0]].name){
                axis[0]="k"+D->getAxis()[D->continuityLevel[1]].name;
                axis[1]="k"+D->getAxis()[D->continuityLevel[0]].name;
                use.back()="s";
            }

            Plot plot(tSource[0]->idx(),axis,use,points,bounds);
            plot.print();
            plot.plot(AmplitudeSmooth,specFile,head,"",true);
            break;
        }
        case 3:
        {
            // map to smooth spectral discretization
            Coefficients smoothResult(tSource[0]->idx());
            smoothResult.treeOrderStorage();
            averagedResult->treeOrderStorage();
            if(ExtName!="")
                smoothResult=AmplitudeSmooth;
            else {
                smoothResult=*averagedResult;
            }

            // switch between different forms of plotting
            vector<string>axis(1,"kRn"),use(1,"g");
            vector<unsigned int> points(1,0);
            vector<vector<double> >bounds(1,vector<double>(2));

            // never sum over specRn
            axis.push_back("specRn");
            use.push_back("p");
            points.push_back(0);
            bounds.push_back(vector<double>(2,0.));

            // always plot total spectrum
            Plot total(tSource[0]->idx(),axis,use,points,bounds);
            total.plot(smoothResult,specFile.substr(0,specFile.rfind("/"))+"/spec_total",head,"",true);

            if(plotWhat=="total"){
                //
            }

            else if(plotWhat=="partial"){

                // partial (default)
                axis.push_back("Eta");
                use.push_back("p");
                points.push_back(0);
                bounds.push_back(vector<double>(2,0.));

                axis.push_back("Phi");
                use.push_back("p");
                points.push_back(0);
                bounds.push_back(vector<double>(2,0.));
            }

            else if(plotWhat=="benchmark"){
                // line-outs in Eta-direction
                axis[0]="Eta";
                points[0]=501;
                use[0]='g';
                bounds[0][0]=-1.;
                bounds[0][1]=1.;

                // radial points
                axis.push_back("kRn");
                use.push_back("p");
                points.push_back(0);
                bounds.push_back(vector<double>(2,0.));

            }

            else if (plotWhat=="intPhi"){
                use.push_back("g");
                axis.push_back("Eta");
                points.push_back(101);
                bounds.push_back(vector<double>(2,-1.));
                bounds.back()[1]=1.;
            }

            else if (plotWhat.find("cutEta")==0){
                use.push_back("g");
                axis.push_back("Phi");
                points.push_back(101);
                bounds.push_back(vector<double>(2,0.));
                bounds.back()[1]=8.*atan(1.);

                int pts=9;
                if(plotWhat.find("[")!=string::npos)
                    pts=tools::string_to_int(tools::stringInBetween(plotWhat,"[","]"));
                head.insert(head.begin(),1,Str("")+pts+"equidistand cuts in Eta=[-1,1]");
                use.push_back("p");
                axis.push_back("Eta");
                points.push_back(pts);
                bounds.push_back(vector<double>(2,-1.));
                bounds.back()[1]=1.;
            }

            else if (plotWhat.find("cutPhi")==0){

                axis.push_back("Eta");
                use.push_back("g");
                points.push_back(101);
                bounds.push_back(vector<double>(2,-1.));
                bounds.back()[1]=1.;

                int pts=9;
                if(plotWhat.find("[")!=string::npos)
                    pts=tools::string_to_int(tools::stringInBetween(plotWhat,"[","]"));
                head.insert(head.begin(),1,Str()+pts+("equidistant cuts in Phi=[0,2pi]"));
                axis.push_back("Phi");
                use.push_back("p");
                points.push_back(pts);
                bounds.push_back(vector<double>(2,0.));
                bounds.back()[1]=8.*atan(1.);

            }

            else
                ABORT("undefined spectrum plot: "+plotWhat+", admissible: total,partial,intPhi,cutPhi,cutEta");


            Plot plot(tSource[0]->idx(),axis,use,points,bounds);
            plot.print();
            plot.plot(smoothResult,specFile,head,"",true);

            break;
        }
        case 4:
        {
            /// case haCC

            // map to smooth spectral discretization
            Coefficients smoothResult(tSource[0]->idx());
            smoothResult=*averagedResult;

            // switch between different forms of plotting
            vector<string>axis(1,"kRn"),use(1,"g");
            vector<unsigned int> points(1,0);
            vector<vector<double> >bounds(1,vector<double>(2,0.));
            if(plotWhat=="total"){
                // all set
            }

            else if(plotWhat=="partialIon"){

                // partial (default)
                axis.push_back("Vec");
                use.push_back("p");
                points.push_back(0);
                bounds.push_back(vector<double>(2,0.));
            }

            else if(plotWhat=="partial"){

                // partial (default)
                axis.push_back("Vec");
                use.push_back("p");
                points.push_back(0);
                bounds.push_back(vector<double>(2,0.));

                axis.push_back("Eta");
                use.push_back("p");
                points.push_back(0);
                bounds.push_back(vector<double>(2,0.));

                axis.push_back("Phi");
                use.push_back("p");
                points.push_back(0);
                bounds.push_back(vector<double>(2,0.));
            }
            else
                ABORT("undefined spectrum plot: "+plotWhat+", admissible: total,partial,intPhi,cutPhi");

            Plot plot(tSource[0]->idx(),axis,use,points,bounds);
            plot.print();
            plot.plot(smoothResult,specFile,head,"",true);
            break;
        }
        case 6:
        {

            if(channelHelper==0){

                // switch between different forms of plotting
                vector<string>axis(1,"kRn1"),use(1,"g");
                vector<unsigned int> points(1,0);
                vector<vector<double> >bounds(1,vector<double>(2,0.));

                axis.push_back("kRn2");
                use.push_back("g");
                points.push_back(0);
                bounds.push_back(vector<double>(2,0.));
                if(plotWhat=="total"){
                    cout<< "we print the toal ones"<< endl;
                    // all set
                }else if(plotWhat == "partial"){
                    // partial (default)
                    axis.push_back("Eta1");
                    use.push_back("p");
                    points.push_back(0);
                    bounds.push_back(vector<double>(2,0.));

                    axis.push_back("Phi1");
                    use.push_back("p");
                    points.push_back(0);
                    bounds.push_back(vector<double>(2,0.));

                    axis.push_back("Eta2");
                    use.push_back("p");
                    points.push_back(0);
                    bounds.push_back(vector<double>(2,0.));

                    axis.push_back("Phi2");
                    use.push_back("p");
                    points.push_back(0);
                    bounds.push_back(vector<double>(2,0.));

                    axis.push_back("kRn2");
                    use.push_back("p");
                    points.push_back(0);
                    bounds.push_back(vector<double>(2,0.));
                }else if(plotWhat == "intK"){
                    // TODO! Jacobian
                    axis.clear();
                    use.clear();
                    points.clear();
                    bounds.clear();

                    axis.push_back("Eta1");
                    use.push_back("g");
                    points.push_back(50);
                    bounds.push_back({-1., 1.});

                    axis.push_back("Eta2");
                    use.push_back("g");
                    points.push_back(50);
                    bounds.push_back({-1., 1.});
                }
                else
                    ABORT("undefined spectrum plot: "+plotWhat+", admissible: total,partial,intPhi,cutPhi,intK");
                //                tSource[0]->gridSpecDisc->getMapTo()->axpy(*averagedResult, smoothResult);
                Plot plot(tSource[0]->idx(),axis,use,points,bounds);
                plot.print();
                plot.plot(*averagedResult,specFile,head,"",true);
            }
            else{ // with ionic channels

                // switch between different forms of plotting
                vector<string>axis(1,"Vec"),use(1,"p");         //channel
                vector<unsigned int> points(1,0);
                vector<vector<double> >bounds(1,vector<double>(2,0.));

                string IonAxes;
                Inp.read("Channel","Axis",IonAxes,ReadInput::noDefault,"Ionic channel Axis");
                int chanInt;
                if(IonAxes=="Phi1 Eta1 Rn1") chanInt=2;
                else if(IonAxes=="Phi2 Eta2 Rn2") chanInt=1;
                else ABORT("Unknown case: "+IonAxes);

                axis.push_back("kRn"+tools::str(chanInt));
                use.push_back("g");
                points.push_back(0);
                bounds.push_back(vector<double>(2,0.));


                if(plotWhat=="total"){
                    // all set
                }
                else if(plotWhat=="partial"){
                    // partial (default)
                    axis.push_back("Eta"+tools::str(chanInt));
                    use.push_back("p");
                    points.push_back(0);
                    bounds.push_back(vector<double>(2,0.));

                    axis.push_back("Phi"+tools::str(chanInt));
                    use.push_back("p");
                    points.push_back(0);
                    bounds.push_back(vector<double>(2,0.));
                }

                Plot plot(tSource[0]->idx(),axis,use,points,bounds);
                plot.print();
                plot.plot(*averagedResult,specFile,head,"",true);
            }
            break;
        }
        default:
            ABORT(Str("No Implementation of")+Disc->getAxis().size()+"D plot so far\nDiscretization:"+Disc->str());
        }
    }


    PrintOutput::paragraph();
    PrintOutput::message("amplitudes file "+amplFile);
    string saveCopy=specFile+"_"+plotWhat.substr(0,plotWhat.find("["));
    if(abs(integrationTime-runPropagationTime)>runPropagationTime*1.e-2)
        saveCopy+="_tMax="+tools::str(integrationTime,4);
    AsciiFile(specFile).copy(saveCopy,true);
    PrintOutput::message("   spectra file "+specFile
                         +" (saved copy in "+saveCopy.substr(saveCopy.rfind('/'))+")");
}
tSurffTools::Evaluator6D::SiSpec::~SiSpec(){delete avg;}

tSurffTools::Evaluator6D::SiSpec::SiSpec(const Index* Idx, std::string OutputDir, double TStore):
    avg(new Coefficients(Idx)), tStore(TStore), spec(OutputDir + "si_spec"),
    tLastAverage(-DBL_MAX), tBeginAverage(-DBL_MAX), tEndAverage(-DBL_MAX){

    if(tStore == 0.){
        tStore = 0.25 * 2.*math::pi/Pulse::current.omegaMin() * (1. - 1.e-6);
    }

    avg->setToZero();
}
        
void tSurffTools::Evaluator6D::SiSpec::average(Coefficients* C, double Time){
    if(tBeginAverage == -DBL_MAX){
        tBeginAverage = Pulse::current.gettEnd();
        tEndAverage = tBeginAverage + tStore;
        tLastAverage = tBeginAverage;

        // Mark Pulse end time by a dummy entry
        // Set order==1 in spec.py to account for this
        avg->setToZero();
        spec.write(Pulse::current.gettEnd(), *avg);
    }

    if(Time < tBeginAverage) return;

    if((Time - tEndAverage) > -1.e-9){
        *C *= (tEndAverage - tLastAverage);
        *avg += *C;
        *C *= 1./(tEndAverage - tLastAverage);

        *avg *= 1./tStore;
        spec.write(tEndAverage, *avg);

        tBeginAverage = tEndAverage;
        tEndAverage = tBeginAverage + tStore;
        tLastAverage = tBeginAverage;
        avg->setToZero();
    }

    *C *= (Time - tLastAverage);
    *avg += *C;
    tLastAverage = Time;
}

void tSurffTools::computeMomenta(double maxEnergy, double energyResolution, vector<double>& momenta)
{
    momenta.resize(0);
    for (double e=0.; e<=maxEnergy; e+=energyResolution) {momenta.push_back(sqrt(2.*e));}
}

unsigned int tSurffTools::sizeOfAxis(const Discretization *disc, string name)
{
    vector<Axis> axis(disc->getAxis());
    for (vector<Axis>::const_iterator it(axis.begin()); it!=axis.end(); ++it) {
        if (it->name==name) return it->maxSize();
    }
}
