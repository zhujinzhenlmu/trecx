// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "inFlux.h"
#include "discretization.h"
#include "axis.h"
#include "basisSet.h"
#include "basisMat.h"

#include "parameters.h"
#include "readInput.h"

//#include <fftw3.h>

using namespace std;

InFlux InFlux::main;

InFlux::InFlux(ReadInput &Inp,const Discretization* Disc, string HamDef):disc(Disc)
{
    pCentral=0.; ///< no incoming flux
    if(not Inp.found("Incoming"))return;

    Inp.read("Incoming","shape",shape,"gauss","pulse shapes: gauss,cos2,cos4,cos8");
    Inp.read("Incoming","width",pWidth,"1","spectral width of incoming pulse");
    Inp.read("Incoming","momentum",pCentral,"1","central momentum of incoming pulse");
    Inp.read("Incoming","time0",time0,"0","time where phase is flat");
    Inp.read("Incoming","lPartial",lPartial,"-1","partial wave");
    string axName;
    Inp.read("Incoming","axis",axName,"automatic","axis to use for incoming wave");

    axis=0;
    for (vector<Axis>::const_iterator a=Disc->axis.begin(); a!=Disc->axis.end(); a++) {
        if(a->name==axName){
            axis=&*a;
            break;
        } else if(axName=="automatic" and a->coor.qmax>DBL_MAX/2){
            if(axis!=0)ABORT("multiple infinite axis in discretization "+Disc->name+", must specify Scatter: axis");
            axis=&*a;
        }
    }
    if(axis==0)ABORT("could not identify incoming axis "+axName+" in discretzation "+Disc->name);

    rIn=axis->comsca.r0up();
    Inp.read("Incoming","radius",rIn,tools::str(rIn),"radius to pick up incoming flux (default=complex scaling radius)");
    if(rIn>DBL_MAX/2)ABORT("no complex scaling radius on axis="+axis->name+", must give radius");

    // incoming entry element
    kEntry=0;
    for(;kEntry<axis->basDef.size();kEntry++)
        if(abs(axis->bas(axis->basDef[kEntry])->upBound()-rIn)<1.e-3*abs(axis->bas(axis->basDef[kEntry])->scale()))
            break;
    if(kEntry==axis->basDef.size())
        ABORT("no element boundary near entry radius="+tools::str(rIn)+"\nAxis: "+axis->str());

    // create an entry in BasisMat::tableInts
    for(unsigned int k=0;k<axis->basDef.size();k++){
        unsigned int label=axis->bas(axis->basDef[k])->label;
        unsigned int size=axis->bas(axis->basDef[k])->size();
        if(BasisMat::tableInts.size()<=label)BasisMat::tableInts.resize(label+1);
        BasisMat::tableInts[label]["incomingFlux[t]"]=UseMatrix::Constant(size,size,double(k==kEntry));
    }

    // add update function to table
    // OperatorAddvector::addUpdate(tFlux,"incomingFlux[t]");
    DEVABORT("OperatorAddvector no longer supported");
    printOut();

    // get rightFlux basis set
    BasisSetDef def=axis->basDef[kEntry];
    const BasisSet * entry=BasisSet::get(def);
    rightFlux=BasisSet::getRightIn(def);

    // plot for check
    rightFlux->plot(Inp.output());
    entry->plot(Inp.output());

    //------------------------------------------------------------------------
    // HACK: the following is specific for 1d and time-independent problem

    // create overlap and flux matrix elements
    UseMatrix ovr,del;
    BasisMat::get(vector<string>(1,"1"),vector<const BasisSet*>(1,entry),vector<const BasisSet*>(1,rightFlux),ovr,vector<string>(1,""));
    BasisMat::get(vector<string>(1,"d_1_d"),vector<const BasisSet*>(1,entry),vector<const BasisSet*>(1,rightFlux),del,vector<string>(1,""));

    // place into storage for update
    for(unsigned int k=0;k<entry->size();k++){
        matel.push_back(vector<complex<double> >(4));
        matel[k][0]=del(k,0).complex()*0.5;
        matel[k][1]=del(k,1).complex()*0.5;
        matel[k][2]=ovr(k,0).complex();
        matel[k][3]=ovr(k,1).complex();
    }
    matel[entry->def.margin[1]][1]+=0.5; // boundary value from partial integration
    main=*this;
}

void InFlux::printOut() const {
    PrintOutput::title("ICOMING FLUX");
    PrintOutput::lineItem("Axis",axis->name);
    PrintOutput::lineItem("Shape",shape);
    PrintOutput::lineItem("Width",pWidth);
    PrintOutput::lineItem("Momentum",pCentral);
    PrintOutput::paragraph();
}

void InFlux::vals(const double Time, vector<complex<double> >& Vals){
    // Vals: incoming value, incoming derivative, -i * time-derivatives
    /// rather dum - incoming plane wave
    Vals.resize(4);
    complex<double>argT=complex<double>(0.,-0.5*pCentral*pCentral);
    complex<double>argR=complex<double>(0.,-pCentral);
    complex<double> expArg=exp(argR*rIn+argT*Time);
//    cout<<"amp "<<Time<<" "<<expArg<<" "<<abs(expArg)<<endl;
    Vals[0]=expArg;
    Vals[1]=argR*Vals[0];
    Vals[2]=complex<double>(0.,-1.)*argT*expArg;
    Vals[3]=argR*Vals[2];
}


void InFlux::setVals()
{
    // get energy grid

    // evaluate energy amplitude and derivatives

    // Fourier transform

//    // set up interpolation table
//    alglib::real_1d_array x = "[-1.0,-0.5,0.0,+0.5,+1.0]";
//    alglib::complex_1d_array y = "[+1.0,0.25,0.0,0.25,+1.0]";

//    // need to buil it all
//    alglib::spline1dbuildcubic(x, y, valTab);
//    complex<double> v = spline1dcalc(valTab, 0.);


//    // here is how to use FFTW...
//    fftw_complex *in, *out;
//    fftw_plan p;

//    unsigned int N=4;
//    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
//    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
//    p = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

//    fftw_execute(p); /* repeat as needed */

//    fftw_destroy_plan(p);
//    fftw_free(in); fftw_free(out);
}


void InFlux::tFlux(double Time, std::vector<std::complex<double> > &C){
    if(main.matel.size()==0 or C.size()==0)return; // no update on this element
    vector<complex<double> > v;
    main.vals(Time,v);
    if(main.matel.size()!=C.size())ABORT("coefficient size does not match update size: "+tools::str(int(main.matel.size()))+" vs. "+tools::str(int(C.size())));
    for(unsigned int k=0;k<C.size();k++)
        C[k]=main.matel[k][0]*v[0]+main.matel[k][1]*v[1]+main.matel[k][2]*v[2]+main.matel[k][3]*v[3];
}

string InFlux::inFlux() const
{
    if(pCentral==0.)return "";
    if(lPartial>=0)ABORT("partial wave case not implemented yet");

    string s="";
    for (unsigned int k=0;k<disc->axis.size();k++){
        if(disc->axis[k].name==axis->name){
            string b=tools::str(axis->basDef[kEntry].lowBound())+","+tools::str(axis->basDef[kEntry].upBound());
            s+="|incomingFlux[t]>";
        }
        else
            s+="|Id>";
    }
    cout<<"flux: "+s<<endl;
    return s;
}
