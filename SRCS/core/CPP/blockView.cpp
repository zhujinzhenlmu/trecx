// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "blockView.h"

#include <utility>

#include "timer.h"
#include "str.h"
#include "index.h"
#include "operatorFloor.h"
#include "parameters.h"
#include "printOutput.h"

#include "log.h"
#include "treeIterator.h"

using namespace std;

void BlockView::apply(complex<double> A, const Coefficients &Vec,  complex<double> B, Coefficients &Y) const
{
    ABORT("not implemented");
}

void BlockView::Triplet::add(const OperatorAbstract *Block){
    block.push_back(Block);
}
BlockView::BlockView(const OperatorAbstract * Op)
    :OperatorAbstract("view("+Op->name+")",Op->iIndex,Op->jIndex){
    _trip.push_back(Triplet(0,0));
    _trip.back().block.push_back(Op);
}

BlockView::BlockView(const OperatorTree * Op, int IDepth, int JDepth)
    :OperatorAbstract("view("+Op->name+")",Op->iIndex,Op->jIndex)
{
    if(IDepth==depthContinuity)IDepth=Op->iIndex->continuity();
    if(JDepth==depthContinuity)JDepth=Op->jIndex->continuity();

    if(IDepth == depthFloor) IDepth = level_iterator<Index>::custom_level;
    if(JDepth == depthFloor) JDepth = level_iterator<Index>::custom_level;

    /*
     * This setup can take a considerable percentage of the whole
     * tRecX setup time. Performance bottleneck are the calls to
     * levelRank. That is why we use this rather ugly caching.
     */
    map<const Index*, std::pair<int, std::size_t>> levelRanks; // pair.first: level rank, pair.second: index depth
    
    auto generate_ranks = [&levelRanks](const Index* idx, int depth) {
        int rank = 0;
        level_iterator<Index> it;
        if(depth == level_iterator<Index>::custom_level) {
            it = level_iterator<Index>(idx, [](const Tree<Index>* tr) {
                return static_cast<const Index*>(tr)->hasFloor();
            });
        }
        else it = level_iterator<Index>(idx, depth);
        for(; it != level_iterator<Index>{ }; ++it, ++rank)
            levelRanks.emplace(static_cast<const Index*>(*it), std::make_pair(rank, it->depth()));
        return rank; // return the level size
    };

    // determine matrix sizes
    int idim = generate_ranks(Op->iIndex, IDepth);
    int jdim;
    if(Op->iIndex != Op->jIndex) jdim = generate_ranks(Op->jIndex, JDepth);
    else                         jdim = idim;
    
    vector< vector<pair<int, int> > > loc(idim);
    for(const OperatorTree* b=Op;b!=0;b=b->nodeNext(Op)){
        auto iit = levelRanks.find(b->iIndex);
        auto jit = levelRanks.find(b->jIndex);
        if(iit != levelRanks.end()) IDepth = iit->second.second;
        else                        continue;
        if(jit != levelRanks.end()) JDepth = jit->second.second;
        else                        continue;

       if(b->isSubtree(Op)
                and b->iIndex->depth()==IDepth and b->jIndex->depth()==JDepth
                and (b->isLeaf()
                     or  (b->iIndex!=b->child(0)->iIndex and b->jIndex!=b->child(0)->jIndex))
                )
        {
            // int i = b->iIndex->levelRank(0);
            // int j = b->jIndex->levelRank(0);
            int i = levelRanks[b->iIndex].first;
            int j = levelRanks[b->jIndex].first;

            bool new_location = true;
            for(auto& v: loc[i]){
                if(v.first == j){
                    _trip[v.second].block.push_back(b);
                    new_location = false;
                    break;
                }
            }
            
            if(new_location){
                _trip.push_back(Triplet(i,j));
                loc[i].push_back({ j, _trip.size() - 1 });
                _trip.back().block.push_back(b);
            }
        }
    }

}

string BlockView::normsMatrix(int Digits){
    UseMatrix nrm=UseMatrix::Zero(blockRows(),blockCols());
    for(int k=0;k<_trip.size();k++){
        complex<double> a=0.;
        for (int l=0;l<_trip[k].block.size();l++){
            Str("block")+k+l+_trip[k].i+_trip[k].j+_trip[k].block[l]+Str::print;
            a+=_trip[k].block[l]->norm();
        }
        nrm(_trip[k].blockRow(),_trip[k].blockCol())=a;
    }
    return nrm.str("",Digits);
}

int BlockView::subDiagonals() const {
    int d=0;
    for(int i=0;i<_trip.size();i++){
        const OperatorAbstract *b=_trip[i].block[0];
        d= max(d,int(b->jIndex->posIndex()+b->jIndex->sizeStored())-1-int(b->iIndex->posIndex()));
    }
    return d;
}

int BlockView::superDiagonals() const {
    int d=0;
    for(int i=0;i<_trip.size();i++){
        const OperatorAbstract *b=_trip[i].block[0];
        d= max(d,int(b->iIndex->posIndex()+b->iIndex->sizeStored())-1-int(b->jIndex->posIndex()));
    }
    return d;
}

vector< complex<double> > BlockView::bandedStorage() const {
    int col=jIndex->sizeStored(),sup=superDiagonals();
    int ld=subDiagonals()+sup+1;
    vector< complex<double> > stor(ld*col,0.);

    for(int k=0;k<_trip.size();k++){
        int i0=_trip[k].block[0]->iIndex->posIndex(),m=_trip[k].block[0]->iIndex->sizeStored();
        int j0=_trip[k].block[0]->jIndex->posIndex(),n=_trip[k].block[0]->iIndex->sizeStored();
        vector< complex<double> > mat;
        for(int l=0;l<_trip[k].block.size();l++){
            _trip[k].block[l]->OperatorAbstract::matrix(mat);
            for(int j=0,ji=0;j<n;j++)
                for(int i=0;i<m;i++,ji++)
                    stor[sup+(i0+i-j0+j) + (j0+j)*ld]+=mat[ji];
        }
    }
}

//HACK
void BlockView::multiplicities(const Index * Idx, vector<unsigned int> &Glob,int & Dim,  vector<double>& Norm){
    vector<unsigned int> mult;
    Idx->contractedNumbering(Glob,mult);
    for(int k=0;k<mult.size();k++)Norm.push_back(1./sqrt(double(mult[k])));
    Dim=* max_element(Glob.begin(),Glob.end())-* min_element(Glob.begin(),Glob.end())+1;
}

void BlockView::sparseMatrix(Eigen::SparseMatrix< complex<double> > &Mat,bool Contract) const {

    vector<Eigen::Triplet< complex<double> > > list;

    int idim,jdim;
    vector<unsigned int> iglob,jglob;
    vector<double>inrm,jnrm;
    if(Contract){
        multiplicities(iIndex,iglob,idim,inrm);
        multiplicities(jIndex,jglob,jdim,jnrm);
    }
    else {
        for(int k=0;k<iIndex->sizeStored();k++)iglob.push_back(k);
        for(int k=0;k<jIndex->sizeStored();k++)jglob.push_back(k);
        idim=iglob.size();
        jdim=iglob.size();
    }


    // convert to triples wrt to global index, normalize where multiplicities
    idim=0;
    jdim=0;
    Parameters::updateToOne(); // set all time-dependent parameters =1
    for(int k=0;k<_trip.size();k++){
        int i0=_trip[k].block[0]->iIndex->posIndex(iIndex),m=_trip[k].block[0]->iIndex->sizeStored();
        int j0=_trip[k].block[0]->jIndex->posIndex(jIndex),n=_trip[k].block[0]->jIndex->sizeStored();
        vector< complex<double> > mat;
        for(int l=0;l<_trip[k].block.size();l++){
            _trip[k].block[l]->matrix(mat);
             for(int j=j0,ij=0;j<j0+n;j++){
                for(int i=i0;i<i0+m;i++,ij++){
                    complex<double> matij=mat[ij];
                    if(inrm.size()>0)matij*=inrm[i];
                    if(jnrm.size()>0)matij*=jnrm[j];
                    idim=max(idim,(int)iglob[i]);
                    jdim=max(jdim,(int)jglob[j]);
                    if(mat[ij]!=0.)list.push_back(Eigen::Triplet< complex<double> >(iglob[i],jglob[j],matij));
                }
            }
        }
    }
    Parameters::restoreToTime();
    idim++;
    jdim++;

    Mat.resize(idim,jdim);
    Mat.setFromTriplets(list.begin(),list.end(),[] (const  complex<double> & a,const  complex<double> &b) { return a+b; });
}

void BlockView::splitByFactors(std::vector<BlockView> &Part, std::vector<std::complex<double>* > &Factor){

    for(int k=0;k<_trip.size();k++){
        for(int l=0;l<_trip[k].block.size();l++){
            const OperatorTree* op=dynamic_cast<const OperatorTree*>(_trip[k].block[l]);
            if(op==0 or op->floor()==0)ABORT(Str("only implemented for floor blocks")+op);
            int iPart=std::find(Factor.begin(),Factor.end(),op->floor()->factor())-Factor.begin();
            if(iPart>=Factor.size()){
                // create index structure, but remove all blocks
                Part.push_back(*this);
                for(int m=0;m<_trip.size();m++)Part.back()._trip[m].block.clear();
                Factor.push_back(op->floor()->factor());
            }
            Part[iPart]._trip[k].add(op); // add to matching part
        }
    }

    // remove all triplets that have remained empty
    for(int k=0;k<Part.size();k++){
        for(int l=Part[k]._trip.size()-1;l>=0;l--){
            if(Part[k]._trip[l].block.size()==0)
                Part[k]._trip.erase(Part[k]._trip.begin()+l);
        }
    }
}

int BlockView::blockRows() const {
    int n=0;
    for(int k=0;k<_trip.size();k++)n= max(n,_trip[k].blockRow());
    return n+1;
}

int BlockView::blockCols() const {
    int n=0;
    for(int k=0;k<_trip.size();k++)n= max(n,_trip[k].blockCol());
    return n+1;
}

string BlockView::str() const {
    Str s;
    for(int k=0;k<_trip.size();k++)
    {
        s=s+tools::str(_trip[k].i)+" "+tools::str(_trip[k].j)+" \n ";
        for(int l=0;l<_trip[k].block.size();l++)_trip[k].block[l]->str()+" \n ";
    }
    return s;
}


