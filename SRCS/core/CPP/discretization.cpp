// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "discretization.h"

//resolve forward declarations
#include "wavefunction.h"
#include "index.h"
#include "multiIndex.h"
#include "axis.h"
#include "printOutput.h"
#include "parallel.h"
#include "operatorData.h"
#include "discretizationHaCC.h"
#include "discretizationHybrid.h"
#include "discretizationHaCC.h"
#include "basisMat.h"
#include "indexConstraint.h"
#include "visualizeAngularDistribution.h"

#include <memory>
#include "memInfo.h"
#include "operatorFloor.h"
#include "axisTree.h"
#include "indexNew.h"
#include "overlapDVR.h"

using namespace std;
using namespace tools;

Discretization::~Discretization(){
}

Discretization * Discretization::factory(ReadInput &Inp){

    if(DiscretizationHybrid::isHybrid(Inp))
        return new DiscretizationHybrid(Inp);
    vector<Axis> ax;
    Axis::fromFile(Inp,ax);
    for(int k=0;k<ax.size();k++)
        if(ax[k].name=="Ion"){
            return new DiscretizationHaCC(Inp);
        }

    IndexConstraint::main.read(Inp);
    return new BasicDisc(Inp,"", &IndexConstraint::main);
}



string Discretization::str(unsigned int Brief) const{

    string s,b;
    s="Discretzation: "+name+"\n";
    if(parent!=0)s+="Parent discretization: "+parent->name+"\n";
    s+="Hierarchy: "+hierarchy[0];
    b=hierarchy[0];
    for(unsigned int n=1;n<hierarchy.size();n++){
        if(Brief==0)s+="->"+hierarchy[n];
        if(n>0)b+=".";
        b+=hierarchy[n];
    }
    s+="\n";
    s+="Continuity levels: ";
    for(unsigned int d=0;d<continuityLevel.size();d++)s+=" "+tools::str(hierarchy[continuityLevel[d]]);
    s+="\n";
    b+="=";
    for(unsigned int n=0;n<axis.size();n++){
        s+=axis[n].str(Brief)+"\n";
        if(n>0)b+=".";
        b+=axis[n].str(Brief);
    }

    if(idx()!=0)s+=idx()->str();

    if(Brief>0)return b;
    return s;

}

void Discretization::print(string File, string Title) const {
    PrintOutput::set(File);

    if(Title==""){
        if(BasisMat::femDVR)Title="DVR";
        else                Title="FEM";
        Title+=" --- DISCRETIZATION";
    }
    PrintOutput::title(Title);
    PrintOutput::paragraph();

    PrintOutput::lineItem("Name",name);
    if(parent!=0)PrintOutput::lineItem("Parent",parent->name);

    PrintOutput::end();

    PrintOutput::paragraph();
    if(axis.size()>0)Axis::print(axis);
    else {
        PrintOutput::lineItem("Coordinates",idx()->coordinates());
        PrintOutput::paragraph();
    }


    if(constraint!=0){
        if(constraint->constraints().size()==1)
            PrintOutput::lineItem("Constraint",constraint->constraints()[0]->str());
        else if (constraint->constraints().size()>1){
            PrintOutput::paragraph();
            PrintOutput::newRow();
            PrintOutput::rowItem("Constraints");
            for(int k=0;k<constraint->constraints().size();k++){
                PrintOutput::newRow();
                PrintOutput::rowItem(constraint->constraints()[k]->str());
            }
        }
    }
#ifdef _DEVELOP_
    //    PrintOutput::paragraph();
    //    PrintOutput::lineItem("sizes of 1st floor / total", std::to_string(Idx->firstFloor()->levelSize())
    //                          +"/"+std::to_string(Idx->sizeCompute()));
    //    VisualizeAngularDistribution::printAllPossible(Idx);
#endif
}

Coefficients& Discretization::inverseOverlap(Coefficients &coeffs,Index* index, bool &CoeffsPerformInvOv) {
    CoeffsPerformInvOv = true;
    return coeffs;
}

void Discretization::operatorData(string def, const Index * const IFloor, const Index * const JFloor, vector<UseMatrix> & Mats) const
{

    Mats.clear();
    vector<complex<double>*> dummyPointer;
    vector<string> dep;
    OperatorData::dependence(def,dep);
    vector<unsigned int> posHierarchy;
    for(unsigned int k=0;k<dep.size();k++)posHierarchy.push_back((unsigned int)(tools::locateElement(hierarchy,dep[k])));
    if(dep.size()>0 and tools::anyElement(posHierarchy,tools::equal,(unsigned int)(string::npos)))
        ABORT("argument in \""+def+"\" does not match hierarchy"+tools::str(hierarchy,"-"));
    if(dep.size()<2){
        OperatorData::get(def,IFloor->basProd(),JFloor->basProd(),Mats,dummyPointer);
    } else if (dep.size()==2
               and tools::anyElement(continuityLevel,tools::equal,(int)posHierarchy[0])
               and tools::anyElement(continuityLevel,tools::equal,(int)posHierarchy[1]))
    {
        if(not BasisMat::femDVR)ABORT("non-tensor product on floor level only for femDVR");
        OperatorData::get(def,IFloor->basProd(),JFloor->basProd(),Mats,dummyPointer);
    } else if(dep.size()>2) {
        ABORT("at most 2-dimensional floors implemented");
    } else {
        vector<const BasisSet*>iBas,jBas;
        vector<unsigned int> iSub,jSub;
        // get basis functions and index related to dependence
        for(unsigned int k=0;k<dep.size();k++){
            unsigned int lev=tools::locateElement(hierarchy,dep[k]);
            if(lev>=hierarchy.size())ABORT("dependence "+dep[k]+" not found in hierarchy "+tools::str(hierarchy,"-"));
            // get basis set
            vector<const Index *>iPath(IFloor->path()),jPath(JFloor->path());
            unsigned int floorLev=tools::locateElement(continuityLevel,int(lev));
            if(floorLev<continuityLevel.size()){
                iBas.push_back(IFloor->basProd()[floorLev]);
                jBas.push_back(JFloor->basProd()[floorLev]);
            } else {
                iBas.push_back(iPath[lev]->basisSet());
                jBas.push_back(jPath[lev]->basisSet());
                iSub.push_back(iPath[lev]->nSub(iPath[lev+1]));
                jSub.push_back(jPath[lev]->nSub(jPath[lev+1]));
            }
        }
        OperatorData::get(def,iBas,jBas,Mats,dummyPointer,iSub,jSub);
    }
}

/** For one continuous dimension the return value may be 0 or 1

    For two continuous dimensions the return value may be 0 or N, where N is the number of coefficients belonging to the other contiuous dimesions.
    See HeliumDiscretization for an example.
 */
int Discretization::marginSize(const vector<const BasisSet*> &Bas, const int ContIdx, const int LowUp) const {

    if(axis.size()==0)ABORT("no axis defined, explicitly define int marginSize for present discretization");

    // first and last elements have no final margins
    if(Bas[ContIdx]->first() and LowUp==0) return 0;
    if(Bas[ContIdx]->last()  and LowUp==1) return 0;

    // else all coefficients in perpendicular direction need to be matched
    int marg=1;
    for(unsigned int d=0;d<Bas.size();d++)
        if((int) d!=ContIdx)marg*=Bas[d]->size();
    return marg;
}

// floor index of margin coefficients in direction ContIdx on side LowUp
int Discretization::marginIndex(const vector<const BasisSet*> &Bas, const int ContIdx, const int LowUp, const int M) const {

    if(axis.size()==0)ABORT("no axis defined, define 'marginIndex' for present discretization");

    // we assume a product basis on each voxel, sorted "row-wise", i.e. higher indices run faster
    // brute force but safe: run through margin indices until you reach M:
    vector<int> m;
    for(unsigned int d=0;d<Bas.size();d++)m.push_back(Bas[d]->size());
    MultiIndex mI(m); // generate multi-index
    // go trough all margin indices until you hit M
    // surprisingly, this seems to work...
    int total=-1,margin=-1;
    vector<int> i;
    while(mI.next(i)){
        total++;
        if(i[ContIdx]==LowUp*(m[ContIdx]-1))margin++;
        if(margin==M)return total;
    }
    cout<<"marginSize: "<<marginSize(Bas,ContIdx,LowUp)<<endl;
    cout<<"Margin index: "<<M<<" "<<" "<<total<<" "<<margin<<endl;
    ABORT("margin index exceeds margin size");
    exit(1);
}

void Discretization::getBasis(const vector<const Index*> &Path, vector<const BasisSet*> & Basis) const {
    if(Basis.size()>0)ABORT("must enter getBasis with zero size Basis");

    // no axes defined (non-standard discretization)
    if(axis.size()!=hierarchy.size()-1)ABORT("must define axes  for using default getBasis");

    // floor block: axes are the corresponding continuity levels
    if(Path.size()>=axis.size()){
        for (unsigned int c=0;c<continuityLevel.size();c++){
            unsigned int lAxis=continuityLevel[c];
            Basis.push_back(blockBasis(lAxis,Path[lAxis]->childSize(),Path)); // factor function on floor level
        }
    }

    // hierarchy block: single basis set
    else {
        // identify axis
        int lAxis=Path.size();
        for (unsigned int c=0;c<continuityLevel.size();c++){
            if(continuityLevel[c]==int(Path.size())){
                Basis.assign(1,BasisSet::getDummy(axis[lAxis].basDef.size()));
                return; // continuity levels have their functions on the floor
            }
        }
        Basis.push_back(blockBasis(lAxis,0,Path));
    }
}

const BasisSet* Discretization::blockBasis(unsigned int LAxis, unsigned int N, const vector<const Index *> &Path) const {
    BasisSetDef def(axis[LAxis].basDef[N]);
    return BasisSet::get(def);
}

void Discretization::setAxis(ReadInput &In, string Subset){
    // get the axes
    Axis::fromFile(In,axis,Subset); // read all axes

    if(axis.size()==0){
        string mess="no axis found";
        if(Subset!="")mess+=" for subset \""+Subset+"\"";
        ABORT(mess);
    }


    // set the hierarchy names
    for(unsigned int n=0;n<axis.size();n++){
        hierarchy.push_back(axis[n].coor.name());
        if((axis[n].basDef.size()>1 and axis[n].basDef[0].name()!="useIndex") or
                (axis[n].basDef.size()==1 and axis[n].basDef[0].name()=="grid"))
            continuityLevel.push_back(n); // multiple elements require continuity
        if(n>0)name+=".";
        name+=axis[n].coor.name();
    }
    if(continuityLevel.size()==0){
        if(axis.back().name.find("Neutral")==string::npos and
                axis.back().name.find("Ion")==string::npos
                )
        {
            PrintOutput::warning("did not find continuity level - assume last level as single element axis");
        }

    }
}

/// generate name from axis names, set up indices, basis, inverse etc.
void Discretization::construct(){
    // all split coordinates go on floor level
    hierarchy.push_back("f");
    for(unsigned int n=0;n<axis.size();n++){
        if(axis[n].basDef.size()>1)hierarchy.back()+=hierarchy[n];
    }
    if(_axisTree==0 and axis.size()>0)_axisTree.reset(new AxisTree(axis));

    Index::build=true;
    idx()=new IndexNew(_axisTree.get(),constraint);

    Inverse::factory(idx());
    idx()->assignOverlap(idx()->overlap());
    Parallel::setSort(this);
    idx()->testInverseOverlap();
    name=idx()->coordinates();
}

string Discretization::coordinates() const {
    return idx()->coordinates();
}
