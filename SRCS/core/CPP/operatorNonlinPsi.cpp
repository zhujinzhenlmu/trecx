#include "operatorNonlinPsi.h"

#include "index.h"
#include "operatorMap.h"

OperatorNonlinPsi::OperatorNonlinPsi(std::string Def, const Index *IIndex, const Index *JIndex)
    :OperatorFloor(IIndex->sizeStored(),JIndex->sizeStored(),"NonlinPsi")
{
    if(IIndex!=JIndex)DEVABORT("OperatorNonlinPsi is strictly local");
    if(IIndex->basisSetGrid()==0 or JIndex->basisSetGrid()==0)DEVABORT("OperatorNonlinPsi only for grid (at present)");
}
void OperatorNonlinPsi::axpy(const std::complex<double> &Alfa, const std::complex<double> *X, unsigned int SizX,
                             const std::complex<double> &Beta, std::complex<double> *Y, unsigned int SizY) const{
    for(int k=0;k<SizX;k++)Y[k]=Beta*Y[k]+Alfa*X[k]*std::norm(X[k]);
}
