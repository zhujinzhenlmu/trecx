#include "bandedOverlap.h"
#include "operatorFloor.h"
#include "index.h"

BandedOverlap::BandedOverlap(Index *Idx, unsigned int SubD, unsigned int SuperD):OperatorTree("Overlap",Idx,Idx){
    if(Idx->hasFloor()){
        UseMatrix mat;
        Idx->overlap()->matrix(mat);
        // "mat.reband" requires "friend class IndexCoulX;" in useMatrix.h!
        UseMatrix *bandMat = new UseMatrix(mat.reband(SubD,SuperD,true));
        floor() = OperatorFloor::factory(std::vector<const UseMatrix*>(1,bandMat),"BandOvr"+Idx->hash()+Idx->hash());
    }
    else{
        for(int k=0;k<Idx->childSize();k++)
            childAdd(new BandedOverlap(Idx->child(k),SubD,SuperD));
    }
}
