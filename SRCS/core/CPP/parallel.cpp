// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "parallel.h"

#define _PARALLEL_

#include "readInput.h"
#include "discretization.h"
#include "index.h"
#include "coefficients.h"
#include "coefficientsGlobal.h"
//#include "operator.h"
#include "operatorFloor.h"
#include "derivativeBlock.h"
#include "parallelProcess.h"
#include "parallelCross.h"
#include "parallelLayout.h"
#include "operatorTensor.h"
#include "basisMat1D.h"
#include "basisMat2D.h"
#include "basisMatMulti.h"

#include "printOutput.h"

#include "mpiWrapper.h"
#include "parallelBuffer.h"
#include "parallelOperator.h"
#include "parallelLayout.h"

#include "log.h"

#include "toolsPrint.h"

static unsigned int processLevel=4;

using namespace std;


unsigned int Parallel::sizeNode=4;
unsigned int Parallel::sizeBoard=2;
unsigned int Parallel::sizeCPU=2;

map<const Index*,vector<unsigned int> > Parallel::indexSort;
map<std::string,unsigned int> Parallel::_indexOwner;

map<std::string,unsigned int> Parallel::_operatorHost;
map<std::string,unsigned int> Parallel::_grainHost; ///< index of process that hosts iIndex,jIndex block
std::unique_ptr<ParallelLayout> Parallel::_layout;

Parallel::~Parallel(){
    //    delete proc;
    //    proc=0;
    for(auto p: cross)delete p;
    for(auto p: grain)delete p;
}

void Parallel::setSort(const Discretization *Disc){
    if(Disc==0)return;
    if(Disc->idx()->sizeCompute()==0)return; // empty discretization, no sorting

    // create given sorting
    vector<unsigned int> s;
    for(unsigned int k=0;k<Disc->idx()->firstFloor()->depth();k++)s.push_back(k);

    //HACK - will fail for hybrid hierarchies
    int itop=0;
    for(const Index* idx=Disc->idx();idx!=0;idx=idx->descend()){
        if(idx->continuity()!=Index::npos)std::swap(s[itop++],s[idx->depth()]);
    }

    indexSort[Disc->idx()]=s;

    _layout.reset(new ParallelLayout(Disc->idx(),""));
    _layout->setFloorHosts(Disc->idx());
}

void Parallel::construct(unsigned int NProc, unsigned int Level)
{
    if(Level==0){
        for(unsigned int n=0;n<NProc;n++)childAdd(new Parallel(new ParallelProcess(MPIwrapper::Size(),n)));
        return;
    }

    unsigned int nsub=0;
    if(Level==3)nsub=sizeNode;
    if(Level==2)nsub=sizeBoard;
    if(Level==1)nsub=sizeCPU;

    while(NProc>nsub){
        childAdd(new Parallel(nsub,Level-1));
        NProc-=nsub;
    }
    childAdd(new Parallel(NProc,Level-1));

    if(Level==3){
        for(Parallel* p=descend(4);p!=0;p=p->nodeRight()){
            _process.push_back(p->proc);
            _process.back()->numb=_process.size()-1;
        }
    }
}

void Parallel::setToZeroLHS() {
    for(Coefficients* f: _process[MPIwrapper::Rank()]->_outFloors)f->setToZero();
}

void Parallel::apply(const std::complex<double> &Alfa) {

    proc=process(MPIwrapper::Rank());

    unsigned int curBegin=0,curEnd=MPIwrapper::Size();
    curBegin=MPIwrapper::Rank();
    curEnd=curBegin+1;

    ParallelProcess * curProc;

    // initiate send to other processes
    for(unsigned int cur=curBegin;cur<curEnd;cur++){
        curProc=process(cur);
        for(unsigned int n=0;n<MPIwrapper::Size();n++){
            if(process(n)->recvBuf[curProc->number()].size()>0){
                if(curProc==proc)
                    proc->sendTo(process(n)->number(),process(n)->recvC[curProc->number()],process(n)->recvBuf[curProc->number()]);
            }
        }
    }

    // apply to local data and initiate send
    for(unsigned int cur=curBegin;cur<curEnd;cur++){
        curProc=process(cur);
        for(unsigned int n=0;n<curProc->sendB.size();n++){
            if(curProc->sendBuf.size()>0){
                // apply and send
                applySubset(curProc->sendB[n],Alfa);
                if(curProc==proc)
                    proc->sendTo(process(n)->number(),curProc->sendC[process(n)->number()],curProc->sendBuf[process(n)->number()]);
            }
        }
    }

    // all strictly local operations
    for(unsigned int cur=curBegin;cur<curEnd;cur++){
        curProc=process(cur);
        applySubset(curProc->localB,Alfa);
    }


    // receive rhs and apply, result is local
    for(unsigned int cur=curBegin;cur<curEnd;cur++){
        curProc=process(cur);
        for(unsigned int n=0;n<MPIwrapper::Size();n++){
            if(curProc->recvB[n].size()>0){
                // receive and apply
                if(curProc==proc)
                    curProc->recvFrom(process(n)->number(),curProc->recvC[process(n)->number()],curProc->recvBuf[process(n)->number()]);
                applySubset(curProc->recvB[n],Alfa);
            }
        }
    }

    // receive results from others
    for(unsigned int cur=curBegin;cur<curEnd;cur++){
        curProc=process(cur);
        for(unsigned int n=0;n<curProc->sendB.size();n++){
            if(curProc==proc)
                curProc->addFrom(process(n)->number(),process(n)->sendC[proc->number()],process(n)->sendBuf[proc->number()]);
        }
    }

    // collect all local request and wait for completion
    vector<MPI_Request>req;
    for(unsigned int k=0;k<MPIwrapper::Size();k++){
        if(proc->sendBuf[k].size()>0                   )req.push_back(proc->sendBuf[k].req);
        if(process(k)->recvBuf[proc->number()].size()>0)req.push_back(process(k)->recvBuf[proc->number()].req);
    }
    MPIwrapper::Waitall(req);
}

void Parallel::keepLocal(Coefficients* C){
    for(Coefficients* f=const_cast<Coefficients*>(C->firstLeaf());f!=0;f=f->nextLeaf())
        if(owner(f->idx())!=MPIwrapper::Rank())f->setToZero();
}

static unsigned int cnt1=0,cnt2=0,cnt3=0,cnt4=0;

void Parallel::applySubset(const std::vector<const DerivativeBlock *> &Block,std::complex<double>Alfa) const
{
    for(std::vector<const DerivativeBlock*>::const_iterator b=Block.begin();b!=Block.end();b++)
    {

        complex<double> alfa(Alfa);
        if((*b)->oLeaf->floor()->factor()!=0){
            if(*(*b)->oLeaf->floor()->factor()==0.)continue;
        }

        // application threshold defined, skip if below
        if((*b)->eps>=0 and max(abs(alfa.real()),abs(alfa.imag()))*(*b)->cInOut[0]->norm() <= (*b)->eps)continue;

        //NOTE: use of complex multiplications seems to be faster then rearranging real and imag parts
        (*b)->oLeaf->floor()->apply(alfa,(*b)->cInOut[0]->floorData(),(*b)->cInOut[0]->size(),
                1.                      ,(*b)->cInOut[1]->floorData(),(*b)->cInOut[1]->size());
    }
}

string Parallel::str() const {
    string s="total load ="+tools::str(load())+"     (pre-receive)->[oper]->(post-send)    dataSize{#blocks}";
    for(unsigned int k=0;k<MPIwrapper::Size();k++){
        const ParallelProcess * procHere=process(k);
        s+="\nProcess no "+tools::str(procHere->number());
        if(procHere==0)
            s+=" (unused)";
        else
            s+=",\tloadDelta="+tools::str(int(100*(MPIwrapper::Size()*procHere->load()/load()-1.)),3)
                    +"%,\t"+procHere->str();
    }
    return s;
}

string Parallel::strDist() {
    std::string s;
    for(typename std::map<string,unsigned int>::iterator it=_indexOwner.begin();it!=_indexOwner.end();it++)
        s+=Str("","")+it->first+"="+it->second+" ";
    return s.substr(0,s.find_last_not_of(" "));
}


void Parallel::addBlock(DerivativeBlock *Block){
    // search grains
    std::stringstream hash;
    hash << Block->cInOut[0] << Block->cInOut[1];
    auto it = grainMap.find(hash.str());
    if(it == grainMap.end()){
        grain.push_back(new ParallelGrain());
        grainMap[hash.str()] = grain.back();
        grain.back()->block.push_back(Block);
    }else{
        it->second->block.push_back(Block);
    }
}


void Parallel::addGrain(ParallelGrain *Grain, const string & SendReceive){

    if(SendReceive!="send" and SendReceive!="receive" and SendReceive!="either")
        ABORT("SendReceive must be \"send\", \"receive\",or \"either\", is: "+SendReceive);

    if(Grain->block.size()==0)return; // empty grain

    // check for existing stripes
    unsigned int iRow=0,iCol=0;
    for(;iRow<cross.size();iRow++)if(cross[iRow]->index()==Grain->block[0]->cInOut[1]->idx())break;
    for(;iCol<cross.size();iCol++)if(cross[iCol]->index()==Grain->block[0]->cInOut[0]->idx())break;

    // no matching stripe, create new
    if(iCol==cross.size() or iRow==cross.size())cross.push_back(new ParallelCross());

    // send or receive blocks:
    // (1) minimize communication
    // (2) balance load between send and receive branches
    bool send=Grain->leftIndex()->sizeStored()<Grain->rightIndex()->sizeStored() or
            (Grain->leftIndex()->sizeStored()<=Grain->rightIndex()->sizeStored() and  cross[iCol]->load()<=cross[iRow]->load());
    send=SendReceive=="send" or (SendReceive!="receive" and send);
    if(send){
        for(unsigned int k=0;k<Grain->block.size();k++)cross[iCol]->colBlock.push_back(Grain->block[k]);
    }
    else {
        for(unsigned int k=0;k<Grain->block.size();k++)cross[iRow]->rowBlock.push_back(Grain->block[k]);
    }

    if(cross.back()->load()==0.)cross.pop_back(); // newly created cross was not used, remove
}

double Parallel::load() const{
    double l=0;
    if(proc!=0)return l+=proc->load();
    for(unsigned int k=0;k<childSize();k++)l+=child(k)->load();
    return l;
}


static bool smallerCross(const ParallelCross * A, const ParallelCross * B){
    for(int k=0;k<A->index()->index().size();k++){
        if(A->index()->index()[k]<B->index()->index()[k])return true;
        if(A->index()->index()[k]>B->index()->index()[k])return false;
    }
    return false;
}

void sortCrosses(std::vector<ParallelCross*> & Cross){
    std::sort(Cross.begin(),Cross.end(),smallerCross);
}


void Parallel::distribute(const string & SendReceive){

    // (re-)arrange ParallelGrain's into ParallelCross's
    for(unsigned int k=0;k<cross.size();k++)delete cross[k];
    cross.clear();
    for(unsigned int k=0;k<grain.size();k++)addGrain(grain[k],SendReceive);
    // get total load
    double loadPerProc=0.;
    for(unsigned int k=0;k<cross.size();k++)loadPerProc+=cross[k]->load();
    loadPerProc=loadPerProc/double(MPIwrapper::Size()); // assumes homogenous floating performance
    //for the subinterval
    bool usekGrid = false;

    ParallelLayout lay(cross[0]->index()->root(),"");
    lay.sort(cross);

    double curLoad=0.;
    int curProc=0;
    for(ParallelCross* c: cross)
    {
        if(owner(c->index())==none){
            Parallel::setIndexOwner(c->index(),curProc);
            curLoad+=c->load();
            if(curLoad>loadPerProc*(curProc+1)){
                curProc=std::min(curProc+1,MPIwrapper::Size());
            }
        }
    }


    // fill up processes
    for(unsigned int k=0;k<cross.size();k++)
    {
        unsigned int nCur;
        if(cross[k]->index()->hierarchy().find("k") != string::npos and MPIwrapper::Size() > 1)
            usekGrid = true;
        if(usekGrid){
            _indexOwner[cross[k]->index()->hash()] = cross[k]->index()->index()[0];
            nCur = cross[k]->index()->index()[0];
        }else{
            if(_indexOwner.count(cross[k]->index()->hash())==1){
                // index already has owner
                nCur=owner(cross[k]->index());
            }
            else {
                // find first unfilled process or take lowest filled
                double loadMin=DBL_MAX;
                for(unsigned int n=0;n<MPIwrapper::Size();n++){
                    double l=process(n)->load();
                    if(loadMin>l){
                        loadMin=l;
                        nCur=n;
                    }
                    if((n>0 and l<loadPerProc) or l<loadPerProc*0.95){
                        nCur=n;
                        break;
                    }
                }
                _indexOwner[cross[k]->index()->hash()]=nCur;
            }
        }

        if(MPIwrapper::isMaster())process(nCur)->addCross(cross[k]);
    }
    // synchronize index ownership
    syncIndexOwner();
    // on slaves, assign cross to process
    if(not MPIwrapper::isMaster())
        for(int k=0;k<cross.size();k++){
            process(_indexOwner[cross[k]->index()->hash()])->addCross(cross[k]);
        }
    for(unsigned int k=0;k<MPIwrapper::Size();k++)process(k)->setSendRecv(SendReceive);
}

OperatorFloor* Parallel::notOnHost=new OperatorDUM();

OperatorFloor* Parallel::operatorFloor(const Index *iIndex, const Index *jIndex, std::function<OperatorFloor*()> factory){

    // get current host (or assign new)
    string iHash=iIndex->hash(),jHash=jIndex->hash();
    string hash=iHash+jHash;
    if(_grainHost.count(hash)==0){
        if(_layout)_grainHost[hash]=_layout->floorHost(jIndex);
        if(_layout and _grainHost[hash]==none)_grainHost[hash]=_layout->floorHost(iIndex);
        if(_grainHost[hash]==none)_grainHost[hash]=_grainHost.size()%MPIwrapper::Size();
    }
    if(MPIwrapper::Size()>1 and
            iIndex->hierarchy().find("ValDer") != string::npos and
            jIndex->hierarchy().find("ValDer") != string::npos
            )_grainHost[hash] = all;
    int host=_grainHost[hash];

    return host==all or host==MPIwrapper::Rank() ? factory() : new OperatorDUM(0.);
}

void Parallel::addBlocks(std::vector<DerivativeBlock> & Blocks, const std::string & SendReceive){
    // lump together blocks with equal indices into ParallelGrain's
    for(unsigned int k=0;k<Blocks.size();k++)addBlock(&Blocks[k]);

    distribute(SendReceive);
    // create buffers and sender/recipient lists
    for(unsigned int n=0;n<MPIwrapper::Size();n++)process(n)->setBuffers(this);
}
unsigned int Parallel::owner(const Index* Idx){
    if(MPIwrapper::Size()==1)return 0;
    if(_indexOwner.count(Idx->hash())==1)return _indexOwner[Idx->hash()];
    return none;
}

/// distribute Coefficients into local
void Parallel::scatter(CoefficientsGlobal *Glob, CoefficientsLocal *Loc, unsigned int From){
    MPIwrapper::ScatterV(Glob->storageData(),Glob->sizes().data(),Loc->storageData(),Loc->size(),From);
}

/// gather Coefficients into Global
void Parallel::gather(CoefficientsGlobal *Glob, CoefficientsLocal *Loc, unsigned int To){
    MPIwrapper::GatherV(Loc->storageData(),Loc->size(),Glob->storageData(),Glob->sizes().data(),To);
}

void Parallel::allGather(CoefficientsGlobal *Glob, CoefficientsLocal *Loc){
    MPIwrapper::AllGatherV(Loc->storageData(),Loc->size(),Glob->storageData(),Glob->sizes().data());
}

/// various tests for parallel operations on Coefficients
void Parallel::test(Coefficients *C){
    MPIwrapper::Barrier();

    Coefficients c(*C);
    CoefficientsGlobal globalC(c.idx());
    CoefficientsGlobal f(c.idx());
    CoefficientsLocal  locC(c.idx());
    c.setToRandom();
    globalC=c;
    if(not (c-=globalC).isZero())ABORT("assignement to global failed");

    c=globalC;
    Parallel::scatter(&globalC,&locC,MPIwrapper::master());
    Parallel::gather(&f,&locC,MPIwrapper::master());
    MPIwrapper::Barrier();

    if(MPIwrapper::isMaster()){
        globalC-=f;
        if(not globalC.isZero())DEVABORT(globalC.str(2)+"\nscatter/gather failed");

        c=f;
        if(not (f-=c).isZero())DEVABORT(globalC.str(2)+"\nback assignement failed");
    }

    MPIwrapper::Barrier();

    globalC.setToRandom();
    // make sure we have the same on all processes
    // why did this ever work w/o this???
    MPIwrapper::AllreduceSUM(globalC.storageData(),globalC.size());
    f=globalC;
    Parallel::keepLocal(&f);
    MPIwrapper::AllreduceSUM(f.storageData(),f.size());
    if(not (globalC-=f).isZero())ABORT("local/sum failed");


    CoefficientsGlobal * viewC=CoefficientsGlobal::view(&c); // data-contiguous global view of c;
    viewC->setToRandom();
    // make sure we have the same on all processes
    MPIwrapper::AllreduceSUM(viewC->storageData(),viewC->size());


    globalC=c;
    CoefficientsLocal *  viewG=CoefficientsLocal::view(&globalC);  // data-contiguous local view on g

    Parallel::gather(viewC,viewG,MPIwrapper::master());

    if(MPIwrapper::isMaster()){
        if(not (c-=globalC).isZero())DEVABORT("global/local views failed");
    }

    Parallel::scatter(&globalC,&locC,MPIwrapper::master());
    if(not (*viewG-=locC).isZero())
        DEVABORT(Str()+MPIwrapper::Rank()+"\n"+viewG->str(1)+"\n"+locC.str(1)+"\nscatter to views failed");
    else
        PrintOutput::DEVmessage(Str("scatter OK"));
}

void Parallel::operatorStructureBcast(OperatorTree *Op){
    // remove all dummies

    // from each process:

}
void Parallel::gatherAllEigen(const Index *Idx, std::vector<std::complex<double> >& Eval,
                              std::vector<Coefficients *> &EigenVec, std::vector<Coefficients *> &DualVec){

    if(MPIwrapper::Size()==1)return;

    vector<int> siz={(int)Eval.size(),(int)EigenVec.size(),(int)DualVec.size()};
    MPIwrapper::AllreduceSUM(siz.data(),3);
    // end time-consuming
    if(siz[0]==0)return;

    // pack and gather
    ParallelBuffer bufE,bufV,bufD;
    bufE.add(Eval);bufE.allGather();
    for(int k=0;k<EigenVec.size();k++)bufV.add(EigenVec[k]);bufV.allGather();
    for(int k=0;k<DualVec.size();k++) bufD.add( DualVec[k]);bufD.allGather();

    // extend storage
    Eval.resize(siz[0]);
    for(int k=EigenVec.size();k<siz[1];k++)EigenVec.push_back(new Coefficients(Idx));
    for(int k= DualVec.size();k<siz[2];k++) DualVec.push_back(new Coefficients(Idx));

    // overwrite by gathered data
    bufE.extract(0,Eval);
    for(int k=0,pos=0;k<siz[1];k++)pos=bufV.extract(pos,EigenVec[k]);
    for(int k=0,pos=0;k<siz[2];k++)pos=bufD.extract(pos, DualVec[k]);

    //    if(MPIwrapper::isMaster()){
    //        for(int k=0;k<EigenVec.size();k++)
    //            ToolsPrint::CoefByFloors(Str("eigen","_")+Idx->index()[0]+Idx->index()[1]+k,EigenVec[k]);
    //    }
}

void Parallel::unsetIndexOwner(const Index *Idx){
    auto pOwn=_indexOwner.find(Idx->hash());
    if(pOwn!=_indexOwner.end())_indexOwner.erase(pOwn);
}

void Parallel::setIndexOwner(const Index *Idx, int Proc){
    if(MPIwrapper::Size()>1 and owner(Idx)!=Parallel::none)DEVABORT("Index has owner, cannot be set");
    _indexOwner[Idx->hash()]=Proc;
}

void Parallel::syncIndexOwner(){

    vector<int> own;
    for(map<std::string,unsigned int>::iterator it=_indexOwner.begin();it!=_indexOwner.end();it++)
        own.push_back(it->second);

    own.push_back(_indexOwner.size());

    MPIwrapper::Bcast(own.data(),own.size(),MPIwrapper::master());

    if(_indexOwner.size()!=own.back())ABORT(Str("failed basic consistency requirement: index size differs, on master=")
                                            +own.back()+"on thread="+_indexOwner.size());
    int k=0;
    for(map<std::string,unsigned int>::iterator it=_indexOwner.begin();it!=_indexOwner.end();it++,k++)
        it->second=own[k];
}


void Parallel::timer(){
    string file=ReadInput::main.output()+"timer";
    ofstream out(file.c_str());
    Timer::write(out);
}
