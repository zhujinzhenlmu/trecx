#include "indexNew.h"

#include "printOutput.h"
#include "overlapDVR.h"
#include "inverseDvr.h"
#include "inverseFem.h"
#include "axisTree.h"
#include "basisOrbital.h"
#include "indexConstraint.h"
#include "basisDvr.h"

using namespace std;

bool IndexNew::doNotUse=false;

void buildOverlap() {


}


void IndexNew::addNonEmpty(AxisTree* Ax, const IndexConstraint* Constraint,std::vector<unsigned int>  Pos,
                           std::vector<const Index *> Path,std::vector<int> & Removed)
{
    if(Constraint !=0 and not Constraint->includes(Path, Pos)){
        Removed.push_back(Pos.back());
    }
    else {
        Index* child;
        try{child=new IndexNew(Ax,Constraint,Pos,Path);
        }catch(Index::empty_subtree_exception& ex){
            delete child;
            child=0;
        }
        if(child != 0){
            if(not noDum or Ax)
                childAdd(child);
            else {
                _size=basisAbstract()->size();
                delete child;
            }

        }else{
            Removed.push_back(Pos.back());
            delete child;
        }
    }

}

void IndexNew::setBasisRemoved(const std::vector<int> & Removed){
    if(Removed.size() == basisAbstract()->size()){
        throw Index::empty_subtree_exception();
    }
    if(Removed.size() > 0){
        if(dynamic_cast<const BasisDVR*>(basisAbstract()))ABORT("cannot constrain DVR basis (for now)");
        setBasis(basisAbstract()->remove(Removed));
    }
}


IndexNew::IndexNew(const AxisTree *Ax, const IndexConstraint* Constraint, std::vector<unsigned int> Pos, std::vector<const Index *> Path)
    :IndexNew()
{
    vector<int> removed;
    if(Ax==0){
        // end of axis hierarchy
        setBasis(BasisSet::getDummy(1));
        setAxisName("NONE");
        unsetFloor();
    }
    else {
        Pos.push_back(0);
        Path.push_back(this);
        setAxisName(Ax->name);
        setAxisSubset(Ax->subset());
        if(Ax->basDef.size()==1 or Ax->bases.size()==1){

            if(Ax->bases.size()==1)
                setBasis(Ax->bases[0]);
            else {
                BasisSetDef curDef=Ax->basDef[0].resolveDependence(Pos,Path);
                setBasis(BasisSet::getAbstract(curDef));
            }
            if(basisAbstract()->size()==0)ABORT("zero-size basis on"+strData());

            // descend to next level axes
            for(unsigned int k=0;k<basisAbstract()->size();k++){
                Pos.back()=k;
                if (Ax->childSize()>1){
                    // hybrid axis, insert extra level
                    Pos.push_back(0);
                    Path.push_back(this);
                    string hybnam=Ax->child(0)->name;
                    for(int k=1;k<Ax->childSize();k++)hybnam+="&"+Ax->child(k)->name;

                    childAdd(new Index());
                    childBack()->setAxisName(hybnam);
                    childBack()->setBasis(BasisSet::getDummy(Ax->childSize()));
                    for(int k=0;k<Ax->childSize();k++){
                        Pos.back()=k;
                        childBack()->childAdd(new IndexNew(Ax->child(k),Constraint,Pos,Path));
                    }
                    Path.pop_back();
                    Pos.pop_back();
                }
                else {
                    addNonEmpty(Ax->descend(),Constraint,Pos,Path,removed);
                }
            }
        }

        else if(Ax->basDef.size()>1){
            // finite element axis
            // we may add a BasisFE... to replace Dummy
            setBasis(BasisSet::getDummy(Ax->basDef.size()));

            AxisTree * axt=const_cast<AxisTree*>(Ax);
            for(unsigned int k=0;k<basisAbstract()->size();k++){
                // append single element floor axis
                Pos.back()=k;
                BasisSetDef bDef=Ax->basDef[k];
                //not very pretty...
                axt->firstLeaf()->childAdd(new AxisTree(Axis(Ax->name,Ax->comsca,bDef)));
                childAdd(new IndexNew(axt->child(0),Constraint,Pos,Path));
                axt->descend(axt->height()-1)->childPop();
            }
        }
        setBasisRemoved(removed);
        Pos.pop_back();
        Path.pop_back();
    }


    if(Path.size()==0){
        sizeCompute();

        //HACK bad hacking for floor positions: looks like inconsistent use of floor concept in hacc
        if(Ax->height()==2 and Ax->coor.name()=="Vec" and Ax->child(0)->coor.name()=="Ion"){
            resetFloor(1);
        }
        else if(Ax->firstLeaf()->coor.name()=="Ion" or Ax->firstLeaf()->coor.name()=="Neutral")
            resetFloor(0); //haCC uses full basis
        else if(descend(Ax->height())->isLeaf())
            resetFloor(height()+int(noDum)-1); // no finite elements, take next-to-last index as floor
        else {
            std::vector<std::string> feAx;
            setFloorFE(feAx);
        }


        // register main and potential sub-indices
        setAxisSubset("main");
        for (const Index* idx=this;idx!=0;idx=idx->nodeNext()){
            if(idx->axisSubset()=="main" or idx->parent()->axisSubset()!=idx->axisSubset())
                BasisOrbital::addIndex(idx->axisSubset(),idx);
        }

        // compute overlap matrices in places where it makes sense
        localOverlapAndInverse(0,0);
        if(isOverlapDiagonal()){
            setOverlap(new OverlapDVR(this));
            setInverseOverlap(Inverse::factory(this));
        }
        else {
            setOverlap(localOverlap());
            setInverseOverlap(Inverse::factory(this));
        }
    }

}
