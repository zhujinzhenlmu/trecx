// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "overlapDVR.h"

#include "coefficients.h"
#include "coefficientsLocal.h"
#include "index.h"

using namespace std;

OverlapDVR::OverlapDVR(const Index *Idx)
    :OperatorTree("Ovr("+Idx->hierarchy()+")",Idx,Idx)
{
    // extract diagonal into Coefficients structure, force check
    _diagonal.reset(iIndex);
    _diagonal=Idx->localOverlap()->diagonal(true);

    // make continuous
    _diagonal.makeContinuous();
    diagonalLocal=&_diagonal;

    for(int k=0;k<_diagonal.childSize();k++)childAdd(new OverlapDVR(_diagonal.child(k)));
}

OverlapDVR::OverlapDVR(Coefficients* Diagonal):OperatorTree("Overlap(DVR)",Diagonal->idx(),Diagonal->idx()){
    diagonalLocal=Diagonal;
    if(Diagonal->idx()->continuity()==Index::npos)
        for(int k=0;k<Diagonal->childSize();k++)childAdd(new OverlapDVR(Diagonal->child(k)));
}

void OverlapDVR::apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    if(B!=0.)*tempLHS()=Y;
    Y=Vec;
    Y.scale(A);
    Y.cwiseMultiply(*diagonalLocal);
    Y.axpy(B,tempLHS());
}
std::vector<std::complex<double> > OverlapDVR::diagonal() const{
    return std::vector<std::complex<double>>(diagonalLocal->orderedData(),diagonalLocal->orderedData()+diagonalLocal->size());
}
