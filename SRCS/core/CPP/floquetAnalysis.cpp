// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "floquetAnalysis.h"

#include <sstream>
#include <iostream>
#include <string>

// for GetDiscSubregion
#include "operatorData.h"
#include "operator.h" // this can go (minor changes)
#include "discretization.h"
#include "readInput.h"
#include "printOutput.h"

using namespace std;

FloquetAnalysis::FloquetAnalysis(ReadInput & Inp)
{
    Inp.read("FloquetAnalysis","minE",eMin,"-2","lower boundary for field free energies");
    Inp.read("FloquetAnalysis","maxE",eMax,"0","upper boundary for field free energies");
    Inp.read("FloquetAnalysis","minOvr",minOvr,"0.7","minimal overlap for a match");
    Inp.read("FloquetAnalysis","maxImag",maxImag,"1.e-9","maximal modulus of imaginary part");
}

void FloquetAnalysis::run(Operator * H0, Discretization* D)
{
    ofstream floquet;
    floquet.open((ReadInput::main.output()+"floquet").c_str());
    floquet<<"#    ReFloq  ImFloq  <Floq|Free>   ReFree   ImFree  "<<endl;

    // get the definition of a field free operator
    vector<string> terms = OperatorData::terms(H0->definition);
    string hamFieldFree = "";
    for(unsigned int i = 0; i < terms.size(); i++){
        string ti=terms[i];
        int ileft=ti.find("<"),icent=ti.find(","),irigh=ti.find(">");
        string left =tools::cropString(ti.substr(ileft+1,icent-ileft-1));
        string right=tools::cropString(ti.substr(icent+1,irigh-icent-1));
        if(left==right)hamFieldFree += terms[i];
    }
    if(hamFieldFree=="")ABORT("could not extract any diagonal blocks from "+H0->definition);
    PrintOutput::message("The field free hamiltonian is "+hamFieldFree);

    // construct field free operator
    const Operator* H1 = new Operator("HamFieldFree", hamFieldFree, D, D);

    // get eigenvalues and vectors
    vector<complex<double> > eFloq, eFree;
    vector<Coefficients*> vFloq, vFree;
    H0->eigen(*(D->idx()->overlap()), eFloq, vFloq, D->idx()->sizeCompute());
    H1->eigen(*(D->idx()->overlap()), eFree, vFree, D->idx()->sizeCompute());

    // find large overlaps with field free states and write to floquet
    Coefficients sFree(D->idx());
    for(int i = 0; i < eFree.size(); i ++){
        if(eMin<=eFree[i].real() and eMax>=eFree[i].real()
                and abs(eFree[i].imag())<maxImag)
        {
            D->idx()->overlap()->apply(1.,*vFree[i],0.,sFree);
            for(int j = 0; j < eFloq.size(); j++){
                double ovr = abs(vFloq[j]->innerProduct(&sFree,true));
                if(ovr>1.+1.e-5)PrintOutput::warning("overlap>1, is "+tools::str(ovr));
                if(ovr > minOvr){
                    floquet<<eFloq[j].real()<< ", "<< eFloq[j].imag()<<", "<<abs(ovr)<<", "<<eFree[i].real()<< ", "<< eFree[i].imag()<< endl;
                    break;
                }
            }
        }
    }
    floquet.close();
    PrintOutput::message("selected Floquet energies on "+ReadInput::main.output()+"floquet",0,true);
}
