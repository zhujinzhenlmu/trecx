// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "plotSpectral.h"

#include "discretizationSpectral.h"
#include "index.h"
#include "operatorDiagonal.h"
#include "asciiFile.h"

using namespace std;

PlotSpectral::~PlotSpectral(){delete tmp;}

PlotSpectral::PlotSpectral(const DiscretizationSpectral * Spec):spec(Spec)
{
    tmp=new Coefficients(spec->mapFromParent()->iIndex);
}

void PlotSpectral::plot(const Coefficients &C, const std::string &File, const std::vector<std::string> &Head, string Tag, bool OverWrite) const{


    spec->mapFromParent()->apply(1.,C,0.,*tmp);

    std::vector<std::vector<double> > cols(4);
    intoCols(spec->spectralValues,tmp,cols);

    AsciiFile plotFile(File);
    vector<string>comm(Head);
    comm.push_back("");
    comm.push_back("Re(E)     Im(E)     Re(C)     Im(C)");
    plotFile.writeComments(comm);
    plotFile.writeCols(cols);
}

void PlotSpectral::intoCols(OperatorDiagonal *Eigen, Coefficients *C, std::vector<std::vector<double> > &Cols) const{
    if(C->isLeaf()){
        for(unsigned int k=0;k<C->idx()->sizeCompute();k++){
            Cols[0].push_back(Eigen->diagonal()[k].real());
            Cols[1].push_back(Eigen->diagonal()[k].imag());
            Cols[2].push_back(C->data()[k].real());
            Cols[3].push_back(C->data()[k].imag());
        }
    }
    else
        for(unsigned int k=0;k<C->childSize();k++)intoCols(Eigen->child(k),C->child(k),Cols);
}
