// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "mo.h"
#include "gaunt.h"
#include <fstream>
#include <iomanip>
//#include <boost/math/special_functions/spherical_harmonic.hpp>
#include "Eigen/SVD"

#include "coefficients.h"
#include "coefficientsFloor.h"
#include "index.h"
#include "mpiWrapper.h"
#include "sphericalHarmonicReal.h"
#include "algebra.h"

#include "basisDvr.h"

using namespace std;
#include "eigenNames.h"

TIMER(diag,vlqq)
TIMER(diag1,vlqq)
TIMER(diag2,vlqq)
TIMER(diag3,vlqq)
TIMER(diag4,vlqq)
TIMER(diag5,vlqq)
TIMER(off,vlqq)
TIMER(r1r2,vlqq)
void mo::Vlqq_matrices(MatrixXd &res, int order1, int order2, int Langle, int n1, int n2){

    /** @brief computes @f$ V^l_{q q'} @f$ matrices - radial part of multipole expansion needed for electron repulsion integrals
     */

    // n1,n2 are the finite elements over which r1 and r2 integrations have to be performed

    if (order1<0 or order2<0 or Langle<0){
        cout<<"something wrong with Vlnn function inputs\n";
        exit(1);
    }

    int quadorder = (max(order1,order2)+Langle+2);

    std::unique_ptr<BasisIntegrable> B1(new BasisDVR(radialDef(n1)));// BasisSet::get(radialDef(n1));
    std::unique_ptr<BasisIntegrable> B2(new BasisDVR(radialDef(n2)));// = BasisSet::get(radialDef(n2));

    double x0_1,x1_1,x0_2,x1_2;
    x0_1 = B1->lowBound();
    x1_1 = B1->upBound();
    x0_2 = B2->lowBound();
    x1_2 = B2->upBound();

    scaled_legendre pn_1(x0_1,x1_1,order1);
    scaled_legendre pn_2(x0_2,x1_2,order2);

    if(n1==n2){
        START(diag)
                //setting up quadrature points and weights

                VectorXd quad1,weig1;
        vector<VectorXd > quad2,weig2;
        p_roots(quadorder,quad1,weig1,x0_1,x1_1);
        quad2.resize(quadorder);
        weig2.resize(quadorder);

        START(diag1)
                for(int r1=0;r1<quadorder;r1++){

            if(x0_2<quad1(r1) and x1_2>quad1(r1)){
                VectorXd quad21,weig21,quad22,weig22;
                p_roots(quadorder,quad21,weig21,x0_2,quad1(r1));
                p_roots(quadorder,quad22,weig22,quad1(r1),x1_2);
                quad2[r1].resize(2*quadorder);
                weig2[r1].resize(2*quadorder);
                quad2[r1] << quad21, quad22;
                weig2[r1] << weig21, weig22;
            }
            else{
                p_roots(quadorder,quad2[r1],weig2[r1],x0_2,x1_2);
            }
        }
        STOP(diag1);

        START(diag2)

                //setting up values of all polynomials at the quadrature points and weights

                MatrixXd val1;
        vector<MatrixXd > val2;

        pn_1.values(quad1,val1);
        val2.resize(quadorder);

        for(int r1=0;r1<quadorder;r1++)
            pn_2.values(quad2[r1],val2[r1]);


        //Evaluating V^l_{nn'} matrix

        res = MatrixXd::Zero(order1,order2);
        STOP(diag2)

                START(diag3)
                for(int r1=0;r1<quad1.size();r1++)
                for(int r2=0;r2<0.5*quad2[r1].size();r2++) // Employing symmetry of r1,r2
                res+=val1.col(r1)*val2[r1].col(r2).transpose()*weig1(r1)*weig2[r1](r2)*std::pow(min(quad1(r1),quad2[r1](r2)),Langle)/std::pow(max(quad1(r1),quad2[r1](r2)),Langle+1);
        STOP(diag3)

                MatrixXd res_temp = res + res.transpose();
        res = res_temp;


        //converting multipole operator to a quadrature grid : V^l_{qq'} = P_{qn} * V^l_{nn'} * P_{n'q'}

        MatrixXd pnq;
        if(order1!=order2)  ABORT("Unexpected error: mo::Vlqq_matrices");
        p_roots(order1,quad1,weig1,x0_1,x1_1);
        //lobatto_quadrature(order,quad1,weig1,x0_1,x1_1);
        pn_1.values(quad1,pnq);
        MatrixXd res1 = pnq.transpose()*res*pnq;

        START(diag4)
                res = ((weig1*weig1.transpose()).array()*res1.array()).matrix();

        STOP(diag4);
        STOP(diag)
    }
    else{
        START(off)
                //setting up quadrature points and weights

                VectorXd quad1,weig1,quad2,weig2;
        p_roots(quadorder,quad1,weig1,x0_1,x1_1);
        p_roots(quadorder,quad2,weig2,x0_2,x1_2);

        //setting up values of all polynomials at the quadrature points and weights

        MatrixXd val1,val2;

        pn_1.values(quad1,val1);
        pn_2.values(quad2,val2);

        //Evaluating V^l_{nn'} matrix

        res = MatrixXd::Zero(order1,order2);

        START(r1r2)
                for(int r1=0;r1<quad1.size();r1++)
                for(int r2=0;r2<quad2.size();r2++)
                res+=val1.col(r1)*val2.col(r2).transpose()*weig1(r1)*weig2(r2)*std::pow(min(quad1(r1),quad2(r2)),Langle)/std::pow(max(quad1(r1),quad2(r2)),Langle+1);
        STOP(r1r2)


                //converting multipole operator to a quadrature grid : V^l_{qq'} = P_{qn} * V^l_{nn'} * P_{n'q'}

                MatrixXd pnq1,pnq2;
        p_roots(order1,quad1,weig1,x0_1,x1_1);
        p_roots(order2,quad2,weig2,x0_2,x1_2);
        pn_1.values(quad1,pnq1);
        pn_2.values(quad2,pnq2);
        MatrixXd res1 = pnq1.transpose()*res*pnq2;

        res = ((weig1*weig2.transpose()).array()*res1.array()).matrix();
        STOP(off)

    }
}
std::vector<Eigen::MatrixXd> mo::Vlqq_matrices_new(int order1, int order2, int LambdaMax, int n1, int n2){

    /** @brief computes @f$ V^l_{q q'} @f$ matrices - radial part of multipole expansion needed for electron repulsion integrals
     */

    // n1,n2 are the finite elements over which r1 and r2 integrations have to be performed

    if (order1<0 or order2<0 or LambdaMax<0){
        cout<<"something wrong with Vlnn function inputs\n";
        exit(1);
    }

    int quadorder = (max(order1,order2)+LambdaMax+2);

    std::unique_ptr<BasisIntegrable> B1(new BasisDVR(radialDef(n1)));// BasisSet::get(radialDef(n1));
    std::unique_ptr<BasisIntegrable> B2(new BasisDVR(radialDef(n2)));// = BasisSet::get(radialDef(n2));

    double x0_1,x1_1,x0_2,x1_2;
    x0_1 = B1->lowBound();
    x1_1 = B1->upBound();
    x0_2 = B2->lowBound();
    x1_2 = B2->upBound();

    scaled_legendre pn_1(x0_1,x1_1,order1);
    scaled_legendre pn_2(x0_2,x1_2,order2);

    std::vector<Eigen::MatrixXd> res(LambdaMax,MatrixXd::Zero(order1,order2));
    if(n1==n2){
        START(diag)
                //setting up quadrature points and weights

                VectorXd quad1,weig1;
        vector<VectorXd > quad2,weig2;
        p_roots(quadorder,quad1,weig1,x0_1,x1_1);
        quad2.resize(quadorder);
        weig2.resize(quadorder);

        START(diag1)
                for(int r1=0;r1<quadorder;r1++){

            if(x0_2<quad1(r1) and x1_2>quad1(r1)){
                VectorXd quad21,weig21,quad22,weig22;
                p_roots(quadorder,quad21,weig21,x0_2,quad1(r1));
                p_roots(quadorder,quad22,weig22,quad1(r1),x1_2);
                quad2[r1].resize(2*quadorder);
                weig2[r1].resize(2*quadorder);
                quad2[r1] << quad21, quad22;
                weig2[r1] << weig21, weig22;
            }
            else{
                p_roots(quadorder,quad2[r1],weig2[r1],x0_2,x1_2);
            }
        }
        STOP(diag1);

        START(diag2);

        //setting up values of all polynomials at the quadrature points and weights

        MatrixXd val1;
        vector<MatrixXd > val2;

        pn_1.values(quad1,val1);
        val2.resize(quadorder);

        for(int r1=0;r1<quadorder;r1++)
            pn_2.values(quad2[r1],val2[r1]);


        //Evaluating V^l_{nn'} matrix

        STOP(diag2);

        START(diag3);
        for(int r1=0;r1<quad1.size();r1++)
            for(int r2=0;r2<0.5*quad2[r1].size();r2++)
            { // Employing symmetry of r1,r2
                Eigen::MatrixXd mm=val1.col(r1)*val2[r1].col(r2).transpose()*weig1(r1)*weig2[r1](r2);
                double rFactor=1./max(quad1(r1),quad2[r1](r2));
                double rRatio=min(quad1(r1),quad2[r1](r2))*rFactor;
                for(int la=0;la<LambdaMax;la++,rFactor*=rRatio)res[la]+=mm*rFactor;
            }
        STOP(diag3);

        for (Eigen::MatrixXd &r: res){
            MatrixXd res_temp = r + r.transpose();
            r = res_temp;
        }


        //converting multipole operator to a quadrature grid : V^l_{qq'} = P_{qn} * V^l_{nn'} * P_{n'q'}

        MatrixXd pnq;
        if(order1!=order2)  ABORT("Unexpected error: mo::Vlqq_matrices");
        p_roots(order1,quad1,weig1,x0_1,x1_1);
        //lobatto_quadrature(order,quad1,weig1,x0_1,x1_1);
        pn_1.values(quad1,pnq);

        START(diag4);
        for (Eigen::MatrixXd &r: res){
            MatrixXd res1 = pnq.transpose()*r*pnq;
            r = ((weig1*weig1.transpose()).array()*res1.array()).matrix();
        }
        STOP(diag4);
        STOP(diag)
    }
    else{
        START(off)
                //setting up quadrature points and weights

                VectorXd quad1,weig1,quad2,weig2;
        p_roots(quadorder,quad1,weig1,x0_1,x1_1);
        p_roots(quadorder,quad2,weig2,x0_2,x1_2);

        //setting up values of all polynomials at the quadrature points and weights

        MatrixXd val1,val2;

        pn_1.values(quad1,val1);
        pn_2.values(quad2,val2);

        //Evaluating V^l_{nn'} matrix

        START(r1r2);
        for(int r1=0;r1<quad1.size();r1++)
            for(int r2=0;r2<quad2.size();r2++){
                Eigen::MatrixXd mm=val1.col(r1)*val2.col(r2).transpose()*weig1(r1)*weig2(r2);
                double rFactor=1./max(quad1(r1),quad2(r2));
                double rRatio=min(quad1(r1),quad2(r2))*rFactor;
                for(int la=0;la<LambdaMax;la++,rFactor*=rRatio)res[la]+=mm*rFactor;
            }
        STOP(r1r2);


        //converting multipole operator to a quadrature grid : V^l_{qq'} = P_{qn} * V^l_{nn'} * P_{n'q'}

        MatrixXd pnq1,pnq2;
        p_roots(order1,quad1,weig1,x0_1,x1_1);
        p_roots(order2,quad2,weig2,x0_2,x1_2);
        pn_1.values(quad1,pnq1);
        pn_2.values(quad2,pnq2);
        for(Eigen::MatrixXd &r: res){
            MatrixXd res1 = pnq1.transpose()*r*pnq2;
            r = ((weig1*weig2.transpose()).array()*res1.array()).matrix();
        }
        STOP(off)

    }
    return res;
}

std::pair<std::unique_ptr<AlgebraTrunc>, Eigen::VectorXd> get_smoothener(mo* mo_ptr, int na, int qmax = -1);

void mo::setup_R_dir(BasisFunctionCIion *ion){

    /** @brief constructs object helper object @f$ R_{dir} @f$
   *
   */

    vector<int> spin = ion->spin;
    vector<double> lc_facs = ion->lc_factors;

    if(vlqq.size()==0)
        setup_vlqq();
    int m_gmax = (*max_element(mo_m.begin(),mo_m.end()));
    int l_gmax = *max_element(mo_l.begin(),mo_l.end());
    int lmax = AxisForString("Eta")->basDef[0].order;   // DVR basis lmax
    int mmax = (AxisForString("Phi")->basDef[0].order-1)/2;   // DVR basis mmax - check??
    int numI = AxisForString("Ion")->basDef[0].order;

    int lambda_upper_limit = 2*min(lmax,l_gmax);
    for(unsigned int Ia=0;Ia<numI;Ia++){
        R_dir.push_back(vector<vector<vector<vector<VectorXcd> > > >());
        for(unsigned int Ib=0;Ib<numI;Ib++){
            R_dir[Ia].push_back(vector<vector<vector<VectorXcd> > >());
            for(int na=0;na<numberOfelementBeforecomplexscaling();na++){
                R_dir[Ia][Ib].push_back(vector<vector<VectorXcd> >());
                for(int L=0;L<=lambda_upper_limit;L++){
                    R_dir[Ia][Ib][na].push_back(vector<VectorXcd>());
                    int M_low = max(max(-2*m_gmax,-2*mmax),-L);
                    int M_up = min(min(2*m_gmax,2*mmax),L);
                    for(int M=M_low;M<=M_up;M++){
                        R_dir[Ia][Ib][na][L].push_back(VectorXcd::Zero(g_o[na]));
                    }
                }
            }
        }
    }

    int tot_count = std::pow(numI,2);
    int IJ_slice = floor(double(tot_count)/double(MPIwrapper::Size()));
    int IJ_start = MPIwrapper::Rank()*IJ_slice;
    int IJ_end   = (MPIwrapper::Rank()+1)*IJ_slice;
    if(MPIwrapper::Rank() == MPIwrapper::Size()-1)  IJ_end = tot_count;

    if(tot_count<MPIwrapper::Size()){
        IJ_start = MPIwrapper::Rank();
        IJ_end   = MPIwrapper::Rank()+1;
    }

    int par_count = 0;  // counter to know if the thread needs to compute it
    for(unsigned int Ia=0;Ia<numI;Ia++)
        for(unsigned int Ib=0;Ib<numI;Ib++){

            if(not(par_count>=IJ_start and par_count<IJ_end)) {par_count++; continue;}             // If does not belong to this thread, continue
            par_count++;

            int mult_ion = ion->rho1.size()/numI;
            MatrixXd rho_t = MatrixXd::Zero(NumberOfOrbitals(),NumberOfOrbitals());
            for(int m1=0;m1<mult_ion;m1++)
                for(int m2=0;m2<mult_ion;m2++)
                    if(spin[mult_ion*Ia+m1] == spin[mult_ion*Ib+m2]){
                        int d = NumberOfOrbitals();
                        rho_t += (ion->rho1[mult_ion*Ia+m1][mult_ion*Ib+m2].block(0,0,d,d)
                                + ion->rho1[mult_ion*Ia+m1][mult_ion*Ib+m2].block(d,d,d,d)) * lc_facs[m1] * lc_facs[m2];
                    }

            vector<MatrixXcd > P;
            if(not rho_t.isZero(1e-14)){
                for(int na=0;na<numberOfelementBeforecomplexscaling();na++){
                    MatrixXcd p = sce[na].adjoint()*rho_t*sce[na];
                    P.push_back(p);
                }
            }

            if(rho_t.isZero(1e-14)) continue;

            for(int L=0;L<=lambda_upper_limit;L++){
                double four_piL = 4*math::pi/(2*L+1);
                int M_low = max(max(-2*m_gmax,-2*mmax),-L);
                int M_up = min(min(2*m_gmax,2*mmax),L);
                for(int M=M_low;M<=M_up;M++){
                    for(int nb=0;nb<numberOfelementBeforecomplexscaling();nb++){
                        VectorXcd temp = VectorXcd::Zero(g_o[nb]);

                        int count1=0;
                        for(int mi=-mo_m[nb]; mi<=mo_m[nb]; mi++)
                            for(int li=abs(mi); li<=mo_l[nb]; li++){
                                int count2=0;
                                for(int mj=-mo_m[nb]; mj<=mo_m[nb]; mj++)
                                    for(int lj=abs(mj); lj<=mo_l[nb]; lj++){
                                        double gf=Gaunt::main.coeff(li,L,lj,mi,M,mj);
                                        if(abs(gf)<1e-14) {count2+=g_o[nb]; continue;}
                                        gf *= four_piL;
                                        temp += gf*P[nb].block(count1,count2,g_o[nb],g_o[nb]).diagonal();
                                        count2+=g_o[nb];
                                    }
                                count1+=g_o[nb];
                            }
                        for(int na=0;na<numberOfelementBeforecomplexscaling();na++)
                            R_dir[Ia][Ib][na][L][M-M_low] += (vlqq[na][nb][L]*temp.asDiagonal()).rowwise().sum();
                    }
                }
            }
        }

    //collect all the data
    MPIwrapper::Barrier();

    for(int na=0;na<numberOfelementBeforecomplexscaling();na++) {
        std::pair<std::unique_ptr<AlgebraTrunc>, Eigen::VectorXd> smoothener = get_smoothener(this, na);
        for(unsigned int Ia=0;Ia<numI;Ia++)
            for(unsigned int Ib=0;Ib<numI;Ib++){
                for(int L=0;L<=lambda_upper_limit;L++){
                    int M_low = max(max(-2*m_gmax,-2*mmax),-L);
                    int M_up = min(min(2*m_gmax,2*mmax),L);
                    for(int M=M_low;M<=M_up;M++) {
                        VectorXcd temp = R_dir[Ia][Ib][na][L][M-M_low];
#ifndef _NOMPI_
                        MPI_Allreduce(temp.data(),R_dir[Ia][Ib][na][L][M-M_low].data(),temp.size(),MPI_DOUBLE_COMPLEX,MPI_SUM,MPIwrapper::communicator());
#endif
                        if(smoothener.first != nullptr) {
                            for(std::size_t c = 0; c < smoothener.second.rows(); ++c)
                                R_dir[Ia][Ib][na][L][M-M_low](c) *= smoothener.first->val(smoothener.second(c));
                        }
                        MPIwrapper::Barrier();
                    }
                }
            }
    }
    MPIwrapper::Barrier();
}

TIMER(RO1,)
TIMER(RO2,)
TIMER(RO3,)
TIMER(RO4,)
TIMER(RO5,)
TIMER(RO6,)
void mo::R_O_helper(vector<vector<vector<vector<MatrixXcd > > > > &v_d){

    START(RO1);
    int l_gmax = (*max_element(mo_l.begin(),mo_l.end()));
    int m_gmax = (*max_element(mo_m.begin(),mo_m.end()));
    int lmax = AxisForString("Eta")->basDef[0].order;   // DVR basis lmax
    int mmax = (AxisForString("Phi")->basDef[0].order-1)/2;   // DVR basis mmax - check??
    int nmax = numberOfelementBeforecomplexscaling();

    if(vlqq.size()==0)
        setup_vlqq();

    clock_t start = clock();

    STOP(RO1);
    START(RO2);
    //compute the direct potential V_d{k1,k2}^{L,M}(qn) // v_d[n][L][M][q](k1,k2)
    for(int na=0;na<nmax;na++){
        v_d.push_back(vector<vector<vector<MatrixXcd> > >());
        int lambda_upper_limit = min(2*l_gmax,lmax+mo_l[na]);     // min(2*lgmax,lmax+lg_max)
        for(int L=0;L<=lambda_upper_limit;L++){
            v_d[na].push_back(vector<vector<MatrixXcd> >());
            int M_low = max(max(-2*m_gmax,-mmax-m_gmax),-L);
            int M_up = min(min(2*m_gmax,mmax+m_gmax),L);
            for(int M=M_low;M<=M_up;M++){
                v_d[na][L].push_back(vector<MatrixXcd>());
                for(int q=0;q<g_o[na];q++)
                    v_d[na][L][M-M_low].push_back(MatrixXcd::Zero(coef.rows(),coef.rows()));
            }
        }
    }
    STOP(RO2);
    START(RO3);

    //parallelizing the following operation
    int tot_count1 = min(2*l_gmax,lmax+l_gmax)+1;
    int IJ_slice1 = floor(double(tot_count1)/double(MPIwrapper::Size()));
    int IJ_start1 = MPIwrapper::Rank()*IJ_slice1;
    int IJ_end1   = (MPIwrapper::Rank()+1)*IJ_slice1;
    if(MPIwrapper::Rank() == MPIwrapper::Size()-1)  IJ_end1 = tot_count1;

    int par_count1=0;
    int lambda_upper_limit = min(2*l_gmax,lmax+l_gmax);
    for(int L=0;L<=lambda_upper_limit;L++){
        if(not(par_count1>=IJ_start1 and par_count1<IJ_end1)) {par_count1++; continue;}             // If does not belong to this thread, continue

        double pi4L = (4*math::pi/(2*L+1));
        int M_low = max(max(-2*m_gmax,-mmax-m_gmax),-L);
        int M_up = min(min(2*m_gmax,mmax+m_gmax),L);
        par_count1++;

        // this is a nightmare loop and SLOW.
        for(int nb=0;nb<nmax;nb++){
            int count1=0;
            for(int mi=-mo_m[nb]; mi<=mo_m[nb]; mi++)
                for(int li=abs(mi); li<=mo_l[nb]; li++){

                    for(int M=M_low;M<=M_up;M++){
                        MatrixXcd temp2 = MatrixXcd::Zero(coef.rows(),g_o[nb]);
                        int count2=0;
                        for(int mj=-mo_m[nb]; mj<=mo_m[nb]; mj++)
                            for(int lj=abs(mj); lj<=mo_l[nb]; lj++){
                                double gf=Gaunt::main.coeff(li,L,lj,mi,M,mj);
                                if(abs(gf)<1e-14) {count2+=g_o[nb]; continue;}
                                temp2 += gf*sce[nb].block(0,count2,coef.rows(),g_o[nb]);
                                count2+=g_o[nb];
                            }
                        if(temp2.isZero(1e-14))  continue;
                        MatrixXcd aa = pi4L * sce[nb].block(0,count1,coef.rows(),g_o[nb]).conjugate();

                        for(int na=0;na<nmax;na++){
                            if(L>min(2*l_gmax,lmax+mo_l[na]))  continue;
                            for(int q=0;q<g_o[na];q++)
                                v_d[na][L][M-M_low][q] += aa*vlqq[na][nb][L].row(q).asDiagonal()*temp2.transpose();
                        }
                    }
                    count1+=g_o[nb];
                }
        }
    }
    STOP(RO3);
    START(RO4);

    //collect all the data
    MPIwrapper::Barrier();
    for(int na=0;na<nmax;na++){
        int lambda_upper_limit = min(2*l_gmax,lmax+mo_l[na]);     // min(2*lgmax,lmax+lg_max)
        for(int L=0;L<=lambda_upper_limit;L++){
            int M_low = max(max(-2*m_gmax,-mmax-m_gmax),-L);
            int M_up = min(min(2*m_gmax,mmax+m_gmax),L);
            for(int M=M_low;M<=M_up;M++)
                for(int q=0;q<g_o[na];q++){
                    MatrixXcd temp = v_d[na][L][M-M_low][q];
#ifndef _NOMPI_
                    MPI_Allreduce(temp.data(),v_d[na][L][M-M_low][q].data(),temp.rows()*temp.cols(),MPI_DOUBLE_COMPLEX,MPI_SUM,MPIwrapper::communicator());
#endif
                    MPIwrapper::Barrier();
                }
        }
    }
    MPIwrapper::Barrier();
    STOP(RO4);
}

void mo::setup_R_O(const BasisFunctionCIion* ion){

    typedef Map<Matrix<double,Dynamic,Dynamic,RowMajor> > MapEigRM;

    int lmax = AxisForString("Eta")->basDef[0].order;   // DVR basis lmax
    int mmax = (AxisForString("Phi")->basDef[0].order-1)/2;   // DVR basis mmax - check??
    int nmax = numberOfelementBeforecomplexscaling();
    int l_gmax = (*max_element(mo_l.begin(),mo_l.end()));
    int m_gmax = (*max_element(mo_m.begin(),mo_m.end()));
    clock_t start = clock();

    vector<vector<vector<vector<MatrixXcd > > > > v_d;
    R_O_helper(v_d);

    if(poly_val.size()==0)
        setup_pol_val();

    //compute R_O[Ia][Ib][n][la][ma](k3,q)

    int tot_count=0;
    for(unsigned int Ia=0;Ia<col_data->cif.numI;Ia++){
        R_O.push_back(vector<vector<vector<vector<MatrixXcd> > > >());
        for(unsigned int Ib=0;Ib<col_data->cif.numI;Ib++){
            R_O[Ia].push_back(vector<vector<vector<MatrixXcd> > >());
            for(int na=0;na<nmax;na++){
                R_O[Ia][Ib].push_back(vector<vector<MatrixXcd> >());
                for(int la=0;la<=lmax;la++){
                    R_O[Ia][Ib][na].push_back(vector<MatrixXcd>());
                    for(int ma=-min(la,mmax);ma<=min(la,mmax);ma++){
                        R_O[Ia][Ib][na][la].push_back(MatrixXcd::Zero(coef.rows(),g_o[na]));
                        tot_count++;
                    }
                }
            }
        }
    }

    //parallelizing
    tot_count /= std::pow(col_data->cif.numI,2);       // parallelize with na,la,ma
    int IJ_slice = floor(double(tot_count)/double(MPIwrapper::Size()));
    int IJ_start = MPIwrapper::Rank()*IJ_slice;
    int IJ_end   = (MPIwrapper::Rank()+1)*IJ_slice;
    if(MPIwrapper::Rank() == MPIwrapper::Size()-1)  IJ_end = tot_count;


    int par_count=0;  // counter to know if the thread needs to compute it
    for(int na=0;na<nmax;na++)
        for(int la=0;la<=lmax;la++)
            for(int ma=-min(la,mmax);ma<=min(la,mmax);ma++){

                if(not(par_count>=IJ_start and par_count<IJ_end)) {par_count++; continue;}             // If does not belong to this thread, continue
                par_count++;

                vector<vector<MatrixXcd> > v1;
                v1.resize(coef.rows());
                for(int mm=0;mm<coef.rows();mm++)
                    v1[mm].resize(g_o[na]);
                for(int mm=0;mm<coef.rows();mm++)
                    for(int q=0;q<g_o[na];q++)
                        v1[mm][q] = MatrixXcd::Zero(coef.rows(),coef.rows());

                int lambda_upper_limit = min(2*l_gmax,lmax+mo_l[na]);     // min(2*lgmax,lmax+lg_max)
                for(int L=0;L<=lambda_upper_limit;L++){
                    int M_low = max(max(-2*m_gmax,-mmax-m_gmax),-L);
                    int M_up = min(min(2*m_gmax,mmax+m_gmax),L);
                    for(int M=M_low;M<=M_up;M++){

                        MatrixXcd temp = MatrixXcd::Zero(coef.rows(),g_o[na]);
                        int count1=0;
                        for(int mi=-mo_m[na]; mi<=mo_m[na]; mi++)
                            for(int li=abs(mi); li<=mo_l[na]; li++){
                                double gf = Gaunt::main.coeff(li,L,la,mi,M,ma);
                                if(abs(gf)<1e-14) {count1+=g_o[na]; continue;}

                                temp += gf*sce[na].block(0,count1,coef.rows(),g_o[na]);
                                count1+=g_o[na];

                            }
                        if(temp.isZero(1e-14)) continue;
                        for(int q=0;q<g_o[na];q++)
                            for(int i=0;i<coef.rows();i++)
                                v1[i][q] += v_d[na][L][M-M_low][q]*temp(i,q);
                    }
                }

                for(unsigned int Ia=0;Ia<col_data->cif.numI;Ia++)
                    for(unsigned int Ib=0;Ib<col_data->cif.numI;Ib++){

                        for(int q=0;q<g_o[na];q++){
                            int shift=0;
                            int shift_add = coef.rows()*coef.rows();
                            int d = coef.rows();
                            for(int l=0;l<coef.rows();l++)
                                for(int i=0;i<coef.rows();i++){

                                    if(Ia>=Ib){
                                        if(not MapEigRM(ion->rho2_lc[Ia][Ib]->data()+shift,d,d).isZero(1e-14) and not v1[i][q].isZero(1e-14))
                                            R_O[Ia][Ib][na][la][ma+min(la,mmax)](l,q) += (v1[i][q].array()*MapEigRM(ion->rho2_lc[Ia][Ib]->data()+shift,d,d).array()).matrix().sum();
                                    }
                                    else{
                                        if(not MapEigRM(ion->rho2_lc[Ib][Ia]->data()+shift,d,d).isZero(1e-14) and not v1[l][q].isZero(1e-14))
                                            R_O[Ia][Ib][na][la][ma+min(la,mmax)](i,q) += (v1[l][q].array()*MapEigRM(ion->rho2_lc[Ib][Ia]->data()+shift,d,d).transpose().array()).matrix().sum();
                                    }
                                    shift+=shift_add;
                                }
                        }
                    }
            }

    //collect all the data
    MPIwrapper::Barrier();
    for(unsigned int Ia=0; Ia<col_data->cif.numI;Ia++)
        for(unsigned int Ib=0;Ib<col_data->cif.numI; Ib++)
            for(int na=0;na<nmax;na++)
                for(int la=0;la<=lmax;la++)
                    for(int ma=-min(la,mmax);ma<=min(la,mmax);ma++){
                        MatrixXcd temp = R_O[Ia][Ib][na][la][ma+min(la,mmax)];
#ifndef _NOMPI_
                        MPI_Allreduce(temp.data(),R_O[Ia][Ib][na][la][ma+min(la,mmax)].data(),temp.size(),MPI_DOUBLE_COMPLEX,MPI_SUM,MPIwrapper::communicator());
#endif
                        MPIwrapper::Barrier();
                        temp = R_O[Ia][Ib][na][la][ma+min(la,mmax)].transpose();
                        R_O[Ia][Ib][na][la][ma+min(la,mmax)] = poly_val[na]*temp;
                    }
    MPIwrapper::Barrier();


}

void mo::setup_R_d(BasisFunctionCIion* ion){

    int mmax = (AxisForString("Phi")->basDef[0].order-1)/2;   // DVR basis mmax - check??
    int lmax = AxisForString("Eta")->basDef[0].order;   // DVR basis lmax
    int nmax = numberOfelementBeforecomplexscaling();
    int l_gmax = (*max_element(mo_l.begin(),mo_l.end()));
    int m_gmax = (*max_element(mo_m.begin(),mo_m.end()));
    int mult_ion = ion->multIon;

    clock_t start = clock();

    vector<vector<vector<vector<MatrixXcd> > > > v_d;
    R_O_helper(v_d);

    if(poly_val.size()==0)
        setup_pol_val();

    //compute R_d[[Ia][na][la][ma](neut,k)

    int tot_count=0;
    for(unsigned int Ia=0;Ia<col_data->cif.numI;Ia++){
        R_d.push_back(vector<vector<vector<MatrixXcd> > >());
        for(int na=0;na<nmax;na++){
            R_d[Ia].push_back(vector<vector<MatrixXcd> >());
            for(int la=0;la<=lmax;la++){
                R_d[Ia][na].push_back(vector<MatrixXcd>());
                for(int ma=-min(la,mmax);ma<=min(la,mmax);ma++){
                    R_d[Ia][na][la].push_back(MatrixXcd::Zero(poly_val[na].rows(),ion->eta1.size()));
                    tot_count++;
                }
            }
        }
    }

    //parallelizing
    tot_count /= col_data->cif.numI;       // parallelize with na,la,ma
    int IJ_slice = floor(double(tot_count)/double(MPIwrapper::Size()));
    int IJ_start = MPIwrapper::Rank()*IJ_slice;
    int IJ_end   = (MPIwrapper::Rank()+1)*IJ_slice;
    if(MPIwrapper::Rank() == MPIwrapper::Size()-1)  IJ_end = tot_count;

    int par_count=0;  // counter to know if the thread needs to compute it
    for(int na=0;na<nmax;na++)
        for(int la=0;la<=lmax;la++)
            for(int ma=-min(la,mmax);ma<=min(la,mmax);ma++){

                if(not(par_count>=IJ_start and par_count<IJ_end)) {par_count++; continue;}             // If does not belong to this thread, continue
                par_count++;

                vector<vector<MatrixXcd> > v1;
                v1.resize(coef.rows());
                for(int mm=0;mm<coef.rows();mm++)
                    v1[mm].resize(g_o[na]);
                for(int mm=0;mm<coef.rows();mm++)
                    for(int q=0;q<g_o[na];q++)
                        v1[mm][q] = MatrixXcd::Zero(coef.rows(),coef.rows());

                int lambda_upper_limit = min(2*l_gmax,lmax+mo_l[na]);     // min(2*lgmax,lmax+lg_max(na))
                for(int L=0;L<=lambda_upper_limit;L++){
                    int M_low = max(max(-2*m_gmax,-mmax-m_gmax),-L);
                    int M_up = min(min(2*m_gmax,mmax+m_gmax),L);
                    for(int M=M_low;M<=M_up;M++){

                        MatrixXcd temp = MatrixXcd::Zero(coef.rows(),g_o[na]);
                        int count1=0;
                        for(int mi=-mo_m[na]; mi<=mo_m[na]; mi++)
                            for(int li=abs(mi); li<=mo_l[na]; li++){
                                double gf = Gaunt::main.coeff(li,L,la,mi,M,ma);
                                if(abs(gf)<1e-14) {count1+=g_o[na]; continue;}

                                temp += gf*sce[na].block(0,count1,coef.rows(),g_o[na]);
                                count1+=g_o[na];
                            }
                        if(temp.isZero(1e-14)) continue;
                        for(int q=0;q<g_o[na];q++)
                            for(int i=0;i<coef.rows();i++)
                                v1[i][q] += v_d[na][L][M-M_low][q]*temp(i,q);
                    }
                }
                for(unsigned int Ia=0;Ia<col_data->cif.numI;Ia++)
                    for(int nn=0;nn<ion->eta1.size();nn++){

                        RowVectorXcd t = RowVectorXcd::Zero(g_o[na]);
                        for(int q=0;q<g_o[na];q++){
                            int d = coef.rows();
                            for(int m1=0;m1<mult_ion;m1++){
                                int shift = ion->spin[mult_ion*Ia+m1]*4*d*d*d;
                                for(int mm=0;mm<d;mm++){
                                    t(q) += ((Map<Matrix<double,Dynamic,Dynamic,RowMajor> >(ion->eta3[nn][mult_ion*Ia+m1].data()+shift,2*d,2*d).block(0,0,d,d)+
                                             Map<Matrix<double,Dynamic,Dynamic,RowMajor> >(ion->eta3[nn][mult_ion*Ia+m1].data()+shift,2*d,2*d).block(d,d,d,d)).transpose().array()*v1[mm][q].array()).sum()*ion->lc_factors[m1];
                                    shift += 4*d*d;
                                }
                            }
                        }
                        R_d[Ia][na][la][ma+min(la,mmax)].col(nn) += t * poly_val[na].transpose();
                    }
            }

    //collect all the data
    MPIwrapper::Barrier();
    for(unsigned int Ia=0; Ia<col_data->cif.numI;Ia++)
        for(int na=0;na<nmax;na++)
            for(int la=0;la<=lmax;la++)
                for(int ma=-min(la,mmax);ma<=min(la,mmax);ma++){
                    MatrixXcd temp = R_d[Ia][na][la][ma+min(la,mmax)];
#ifndef _NOMPI_
                    MPI_Allreduce(temp.data(),R_d[Ia][na][la][ma+min(la,mmax)].data(),temp.size(),MPI_DOUBLE_COMPLEX,MPI_SUM,MPIwrapper::communicator());
#endif
                    MPIwrapper::Barrier();
                }
    MPIwrapper::Barrier();


}

void mo::setup_Rs(BasisFunctionCIion* ion, BasisFunctionCINeutral* Neut){
    setup_R_dir(ion);
    if(Neut!=0)
        setup_R_d(ion);
    if(col_data->no_electrons>1)
        setup_R_O(ion);
}

void mo::setup_vlqq(){

    /** @brief sets vlqq matrices
   */

    clock_t start = clock();

    for(int na=0;na<numberOfelementBeforecomplexscaling();na++){
        vlqq.push_back(vector<vector<MatrixXd> >());
        for(int nb=0;nb<numberOfelementBeforecomplexscaling();nb++){
            vlqq[na].push_back(vector<MatrixXd>());
            int lambda_upper_limit = 2*max(max(mo_l[na],mo_l[nb]),(int)AxisForString("Eta")->basDef[0].order);

            if(oldCode())
                for(int lambda=0;lambda<=lambda_upper_limit;lambda++){
                    MatrixXd t;
                    Vlqq_matrices(t,g_o[na],g_o[nb],lambda,na,nb);
                    vlqq[na][nb].push_back(t);
                }

            else
                vlqq[na][nb]=Vlqq_matrices_new(g_o[na],g_o[nb],lambda_upper_limit+1,na,nb);
        }
    }

}

void mo::setup_3dquadrature(Matrix<double ,Dynamic, 3>& xyz,VectorXd& theta_quad, VectorXd& theta_weig, VectorXd& phi_quad, VectorXd& phi_weig, VectorXd r_pts, int lmax, int mmax, bool is_nucl_pot){

    /** @brief  sets up a 3d quadrature grid in spherical coordinates
     *
     */

    // More relevant for molecules: Find the atom positions and split the integrals into intervals
    vector<double > th_a, ph_a;
    for(int na=0;na<col_data->no_atoms;na++){
        double r = sqrt(std::pow(col_data->cord[na](0),2) + std::pow(col_data->cord[na](1),2) + std::pow(col_data->cord[na](2),2));
                                                                     if(abs(r)<1e-14)    continue;          // do nothing new
                                 th_a.push_back(col_data->cord[na](2)/r);

                                 if(abs(col_data->cord[na](0))<1e-14 and abs(col_data->cord[na](1))<1e-14)
                                                                             ph_a.push_back(0.0);
                                        else if(abs(col_data->cord[na](0))<1e-14 and col_data->cord[na](1)>1e-14)
                                                    ph_a.push_back(math::pi/2.0);
                                                else if(abs(col_data->cord[na](0))<1e-14 and col_data->cord[na](1)<1e-14)
                                                            ph_a.push_back(3.0*math::pi/2.0);
                                                        else{
                                                            ph_a.push_back(atan(col_data->cord[na](1)/col_data->cord[na](0)));
                                                            if(ph_a[ph_a.size()-1]<0)
                                                            ph_a[ph_a.size()-1] += math::pi;
                                                        }
    }

    vector<double > th_bound,ph_bound;          // boundaries
    th_bound.push_back(-1.0);  th_bound.push_back(1.0);
    ph_bound.push_back(0.0);   ph_bound.push_back(2*math::pi);

    // Insert extra points from non-centered atoms if nuclear potential
    if(is_nucl_pot){
        for(unsigned int na=0;na<th_a.size();na++){
            if(std::find(th_bound.begin(),th_bound.end(),(double)th_a[na])==th_bound.end())
                th_bound.insert(th_bound.end()-1,th_a[na]);
            if(std::find(ph_bound.begin(),ph_bound.end(),(double)ph_a[na])==ph_bound.end())
                ph_bound.insert(ph_bound.end()-1,ph_a[na]);
        }
        //      for(int i=0;i<th_bound.size();i++)
        //        cout<<th_bound[i]<<"  ";
        //      cout<<endl;
        //      for(int i=0;i<ph_bound.size();i++)
        //        cout<<ph_bound[i]<<"  ";
        //      cout<<endl;
    }

    vector<VectorXd > th_quad,th_weig,ph_quad,ph_weig;
    th_quad.resize(th_bound.size()-1);  th_weig.resize(th_bound.size()-1); ph_quad.resize(ph_bound.size()-1); ph_weig.resize(ph_bound.size()-1);
    //setting piece-wise \theta, \phi quadrature
    for(unsigned int t=0; t<th_bound.size()-1; t++)
        p_roots(lmax+1,th_quad[t],th_weig[t],th_bound[t],th_bound[t+1]);
    for(unsigned int t=0; t<ph_bound.size()-1; t++){
        ph_quad[t] = VectorXd::Zero(2*mmax+1);
        ph_weig[t] = VectorXd::Zero(2*mmax+1);
        for(int i=-mmax; i<=mmax; i++){
            ph_quad[t](i+mmax) = (ph_bound[t+1]-ph_bound[t])*(double)i/(2.0*(double)mmax+1);
            ph_weig[t](i+mmax) = (ph_bound[t+1]-ph_bound[t])/(2.0*(double)mmax+1);
        }
    }

    //Attach all the pieces
    theta_quad =  VectorXd::Zero((lmax+1)*th_quad.size());
    theta_weig =  VectorXd::Zero((lmax+1)*th_quad.size());
    phi_quad   =  VectorXd::Zero((2*mmax+1)*ph_quad.size());
    phi_weig   =  VectorXd::Zero((2*mmax+1)*ph_quad.size());
    for(unsigned int i=0;i<th_quad.size();i++){
        theta_quad.segment(i*(lmax+1),lmax+1) = th_quad[i];
        theta_weig.segment(i*(lmax+1),lmax+1) = th_weig[i];
        phi_quad.segment(i*(2*mmax+1),2*mmax+1) = ph_quad[i];
        phi_weig.segment(i*(2*mmax+1),2*mmax+1) = ph_weig[i];
    }

    //Tranforming (r,\theta,\phi) quadrature to (x,y,z) quadrature
    xyz = Matrix<double, Dynamic, 3>::Zero(r_pts.size()*theta_quad.size()*phi_quad.size(),3);
    //follow loop order r,theta,phi
    int count=0;
    for(int r =0; r<r_pts.size(); r++)
        for(int t=0; t<theta_quad.size(); t++)
            for(int p=0; p<phi_quad.size(); p++){
                xyz(count,0) = r_pts(r)*sqrt(1.0-theta_quad(t)*theta_quad(t))*cos(phi_quad(p));
                xyz(count,1) = r_pts(r)*sqrt(1.0-theta_quad(t)*theta_quad(t))*sin(phi_quad(p));
                xyz(count,2) = r_pts(r)*theta_quad(t);
                count++;
            }
}

void mo::expansion_dilmq_x_ylm(MatrixXcd &res, int n, bool der_r, bool for_nucl_pot){

    /** @brief creates an expansion  @f$ \phi^i = d^i_{mlk} y_{lm} @f$ where i runs over functions on finite-element n.
     *
     */
    //relies on lmax and mmax already set in primitive gaussians - might need change for molecules !!!!!!!!!

    int lmax = mo_l[n];
    int mmax = mo_m[n];
    int qmax = g_o[n];     // A parameter that needs to be checked!!!!!!!

    double x0,x1;
    x0 = radialDef(n).lowBound();
    x1 = radialDef(n).upBound();

    VectorXd r_quad,r_weig;
    if(for_nucl_pot){
        vector<double > pts;
        pts.push_back(x0);
        for(int na=0;na<col_data->no_atoms;na++){
            double r_0 = sqrt(std::pow(col_data->cord[na](0),2)+std::pow(col_data->cord[na](1),2)+std::pow(col_data->cord[na](2),2));
                                                                         if(r_0>x0 and r_0<x1)
                                                                         pts.push_back(r_0);
        }
        pts.push_back(x1);
        if(pts.size()==2)
            p_roots(qmax,r_quad,r_weig,x0,x1);
        else if(pts.size()>2){
            r_quad = VectorXd::Zero(qmax*(pts.size()-1));
            r_weig = VectorXd::Zero(qmax*(pts.size()-1));
            for(unsigned int p=0;p<pts.size()-1;p++){
                VectorXd tr,tw;
                p_roots(qmax,tr,tw,pts[p],pts[p+1]);
                r_quad.segment(p*qmax,qmax) = tr;
                r_weig.segment(p*qmax,qmax) = tw;
            }
            qmax = r_quad.size();                         // modify qmax
        }
        else
            ABORT(Str("pts.size()=")+pts.size()+"not admissible");
    }
    else
        p_roots(qmax,r_quad,r_weig,x0,x1);


    //HACK outside Vinay's range, use default quadrature
    quadRuleRn(n,r_quad.rows(),r_quad,r_weig);


    int lm_tot = 0;
    for(int m=-mmax;m<=mmax;m++)
        for(int l=abs(m);l<=lmax;l++)
            lm_tot++;

    int slice = lm_tot/MPIwrapper::Size();
    int lm_low = slice*MPIwrapper::Rank();
    int lm_up = slice*(MPIwrapper::Rank()+1);
    if(MPIwrapper::Rank()==MPIwrapper::Size()-1)   lm_up = lm_tot;

    res = MatrixXcd::Zero(NumberOfOrbitals(),lm_tot*qmax);

    int m_quad = mmax;
    int l_quad = lmax;

    double errortolerance = 1e-10;
    double error_m = 2*errortolerance;
    Eigen::MatrixXcd testmatrix_m = Eigen::MatrixXcd::Ones(NumberOfOrbitals(),lm_tot*qmax);

    while(error_m>errortolerance){

        MatrixXcd result_m = Eigen::MatrixXcd::Ones(NumberOfOrbitals(),lm_tot*qmax);

        double error = 2.0*errortolerance;
        Eigen::MatrixXcd testmatrix = MatrixXcd::Zero(NumberOfOrbitals(),lm_tot*qmax);

        while(error>errortolerance){

            //Find values of mos at the 3d quadrature grid
            MatrixXd mo_val;
            Matrix<double,Dynamic,3> xyz;
            VectorXd theta_quad,theta_weig,phi_quad,phi_weig;
            setup_3dquadrature(xyz,theta_quad,theta_weig,phi_quad,phi_weig,r_quad,l_quad,m_quad);

            this->values(mo_val,xyz);
            if(der_r){
                MatrixXd aux(mo_val);
                this->derivatives(mo_val,xyz,"r");
                for(int j=0,count=0;j<qmax;j++)
                    for(int t=0; t<theta_quad.size(); t++)
                        for(int p=0; p<phi_quad.size(); p++,count++)
                            mo_val.col(count)+=aux.col(count)/r_quad(j);
            }


            //construct matrix c[i,j] i->number of mos and j->number of l,m,n sets,, n is the number of polynomials

            MatrixXcd result = MatrixXcd::Zero(NumberOfOrbitals(),lm_tot*qmax);

            vector<vector<vector<double > > > weigs;
            for(int j=0;j<qmax;j++){
                weigs.push_back(vector<vector<double> >());
                for(int t=0; t<theta_quad.size(); t++){
                    weigs[j].push_back(vector<double >());
                    for(int p=0; p<phi_quad.size(); p++){
                        weigs[j][t].push_back(theta_weig(t)*phi_weig(p)*r_quad(j));             //Includes r_quad: quadrature point
                    }
                }
            }

            int pos =0;
            int lm_t = 0;
            for(int m=-mmax; m<=mmax; m++)
                for(int l=abs(m); l<=lmax; l++){
                    if(not(lm_t>=lm_low and lm_t<lm_up)) {lm_t++; pos+=qmax; continue;}
                    int count=0;
                    for(int j=0;j<qmax;j++){
                        DEVABORT("replace with non-boost solution");
                        //                        for(int t=0; t<theta_quad.size(); t++)
                        //                            for(int p=0; p<phi_quad.size(); p++){
                        //                                result.col(pos+j)+=weigs[j][t][p]*mo_val.col(count)*conj(boost::math::spherical_harmonic(l, m, acos(theta_quad(t)), phi_quad(p)));
                        //                                count++;
                        //                            }
                    }
                    pos+=qmax;
                    lm_t++;
                }

            MatrixXcd temp = result;
#ifndef _NOMPI_
            MPI_Allreduce(temp.data(),result.data(),temp.rows()*temp.cols(),MPI_DOUBLE_COMPLEX,MPI_SUM,MPIwrapper::communicator());
            MPIwrapper::Barrier();
#endif
            error = (result-testmatrix).array().abs().maxCoeff();

            testmatrix=result;
            result_m=result;//.cast<std::complex<double > >();

            if(l_quad > 1000){
                if( error>errortolerance ){ cout << "\tmatrix error in mo_2e::exp . quadorder=" << l_quad << ",mquad= "<<m_quad<<", n=" << n << "  error= "<<error<<endl; break;}
                else break;
            }
            if(col_data->no_atoms==1)     l_quad+=1;
            else                          l_quad+=20;
        }
        error_m = (result_m-testmatrix_m).array().abs().maxCoeff();
        if(col_data->no_atoms==1){
            m_quad+=1;
            l_quad-=1;
        }
        else{
            m_quad+=1;
            l_quad-=20;
        }

        testmatrix_m=result_m;
        res=result_m;

        if(m_quad > 40){
            if( error_m>errortolerance and error>errortolerance){ cout << "\tmatrix error in mo_2e::exp . quadorder=" << m_quad << ", n=" << n<<"  error_l="<<error<<"  error_m="<<error_m<<endl; break;}
            else break;
        }
    }
}

void mo::setup_pol_val(){

    if(poly_val.size()!=0) {ABORT("mo::setup_pol_val() - values seems to be already set - wrong call");}

    int nElem=radialSize();
    for(int n=0;n<nElem;n++){
        //      for(int n=0;n<numberOfelementBeforecomplexscaling();n++){

        BasisSetDef def=radialDef(n);
        std::unique_ptr<BasisIntegrable> B(new BasisDVR(def));//= BasisSet::get(def);
        VectorXd r_quad,r_weig;
        //        quadRuleRn(n,g_o[n],r_quad,r_weig);
        quadRuleRnNew(B.get(),g_o[n],r_quad,r_weig);

        MatrixXd val(B->size(),g_o[n]),der(B->size(),g_o[n]);

        UseMatrix X=UseMatrix::Zero(r_quad.rows(),r_quad.cols());
        Map<VectorXcd>(X.data(),X.size()) = r_quad.cast<complex<double> >();
        UseMatrix valUm, derUm;
        B->valDer(X,valUm,derUm);
        for(int i=0;i<B->size();i++)
            for(int k=0;k<g_o[n];k++){
                val(i,k) = valUm(k,i).real();
                der(i,k) = derUm(k,i).real();
            }
        poly_val.push_back(val);
        poly_der.push_back(der);
    }
}

//functions to decide L,M limits of multipole expansion for exchange term : to exploit the bandedness
inline int mo::L_max(int la, int na){

    return la+mo_l[na];
}

inline int mo::L_min(int la,int na){

    //  return abs(la-mo_l[na]);

    int lmax_g = mo_l[na];

    if(la-lmax_g > 0)
        return abs(la-lmax_g);
    else
        return 0;

    // return 0;                                              //default parameter for checking
}

inline int mo::M_min(int L, int ma, int na){

    return min(max(-L,ma-mo_m[na]),L);
    //  int mmax_g = mo_m[na];
    //  if(ma-mmax_g > -L){
    //      if(ma-mmax_g<=L)
    //        return ma-mmax_g;
    //      else
    //        return L;
    //    }
    //  else
    //    return -L;

    /*  return -L;*/                                              //default parameter for checking
}

inline int mo::M_max(int L, int ma, int na){

    return max(min(L,ma+mo_m[na]),-L);

    //  return L;                                               //default parameter for checking

}

void mo::SVD_UV(const BasisFunctionCIion* ion, int Ia, int Ib, MatrixXcd &U, MatrixXcd &V){

    Matrix<int,2,1> aaa; aaa << Ia,Ib;      //storage
    string op="svd";
    MatrixXcd* r_temp = svd_U.retrieve(op,aaa);
    if(r_temp!=NULL){
        U = *r_temp;
        r_temp = svd_V.retrieve(op,aaa);
        V = *r_temp;
        return;
    }

    int mosize = NumberOfOrbitals();
    int mult_ion = ion->rho1.size()/col_data->cif.numI;
    MatrixXd rho = MatrixXd::Zero(mosize,mosize);

    for(int m1=0;m1<mult_ion;m1++)
        for(int m2=0;m2<mult_ion;m2++)
            rho+=ion->rho1[mult_ion*Ia+m1][mult_ion*Ib+m2].block(ion->spin[mult_ion*Ib+m2]*mosize,ion->spin[mult_ion*Ia+m1]*mosize,mosize,mosize) * ion->lc_factors[m1] *ion->lc_factors[m2];

    Eigen::JacobiSVD<MatrixXd > svd_rho(rho, Eigen::ComputeThinU | Eigen::ComputeThinV);
    int nnz = 0;
    for(int i=0;i<svd_rho.singularValues().size();i++)
        if(abs(svd_rho.singularValues()(i))>1e-12)
            nnz++;
    U = MatrixXcd::Zero(coef.rows(),nnz);
    MatrixXcd V1 = MatrixXcd::Zero(coef.rows(),nnz);
    VectorXcd sv = VectorXcd::Zero(nnz);

    int count=0;
    for(int i=0;i<svd_rho.singularValues().size();i++)
        if(abs(svd_rho.singularValues()(i))>1e-12){
            U.col(count) = svd_rho.matrixU().col(i).cast<std::complex<double > >();
            V1.col(count) = svd_rho.matrixV().col(i).cast<std::complex<double > >();
            sv(count)    = svd_rho.singularValues().cast<std::complex<double > >()(i);
            count++;
        }
    if(not (rho.cast<std::complex<double > >()-U*sv.asDiagonal()*V1.adjoint()).isZero(1e-12))
        PrintOutput::warning(Str("Error in SVD "," ")+(rho.cast<std::complex<double > >()-U*sv.asDiagonal()*V1.adjoint()).array().abs().maxCoeff());

    V = sv.asDiagonal()*V1.adjoint();

    svd_U.add(op,aaa,U);
    svd_V.add(op,aaa,V);
}

bool mo::matrix_poly_2e_xch_v2(MatrixXcd &res, const vector<int> &blockIdx, const vector<int> &blockJdx, const BasisFunctionCIion *ion)
{
    /** @brief  computes @f$ \sum\limits_{i,j} \langle \chi_{\alpha} \phi_i | V_{ee} | \phi_j \chi_{\beta} \rangle \rho_{ij} @f$
   * Returns false if 0 matrix
   *
   */

    int la=blockIdx[0];
    int ma=blockIdx[1];//-min(import_data::main.mmax,la);
    int Ia=blockIdx[2];
    int na=blockIdx[3];
    int lb=blockJdx[0];
    int mb=blockJdx[1];//-min(import_data::main.mmax,lb);
    int Ib=blockJdx[2];
    int nb=blockJdx[3];

    if(min(L_max(la,na),L_max(lb,nb))-max(L_min(la,na),L_min(lb,nb))<0)  return false;

    MatrixXcd tga,tgb;
    {
        Matrix<int,4,1> aaa; aaa << Ia,Ib,na,nb;      //storage
        string op="svd";
        MatrixXcd* r_temp = svd_V_tg.retrieve(op,aaa);
        if(r_temp!=NULL){
            tga = *r_temp;
            r_temp = svd_U_tg.retrieve(op,aaa);
            tgb = *r_temp;
        }
        else{
            MatrixXcd U,V_adj;
            SVD_UV(ion,Ia,Ib,U,V_adj);
            tga = V_adj*sce[na];
            tgb = U.adjoint()*sce[nb];
            svd_V_tg.add(op,aaa,tga);
            svd_U_tg.add(op,aaa,tgb);
        }
    }

    int nnz = tga.rows();
    if(nnz==0 or min(L_max(la,na),L_max(lb,nb))-max(L_min(la,na),L_min(lb,nb))<0) return false;

    int lmax = AxisForString("Eta")->basDef[0].order;   // DVR basis lmax
    int mmax = (AxisForString("Phi")->basDef[0].order-1)/2;   // DVR basis mmax - check??

    // setup gaunt coeffs as function of element number
    if(gaunt_xch.size()!=numberOfelementBeforecomplexscaling()){
        gaunt_xch.clear();
        for(int n=0;n<numberOfelementBeforecomplexscaling();n++){
            gaunt_xch.push_back(new Gaunt());
            gaunt_xch.back()->initialize(2*max(lmax,mo_l[n]));
        }
    }

    if(vlqq.size()==0)
        setup_vlqq();

    if(poly_val.size()==0)
        setup_pol_val();

    //setup Sab matrix
    MatrixXcd  S = MatrixXcd::Zero(g_o[na],g_o[nb]);
    //  int L_zero = 0;
    // A faster version
    for(int L=max(L_min(la,na),L_min(lb,nb));L<=min(L_max(la,na),L_max(lb,nb));L++){

        int Mmin = max(M_min(L,ma,na),M_min(L,mb,nb));
        int Mmax = min(M_max(L,ma,na),M_max(L,mb,nb));

        //Find the common blocks in M and perform the inner product
        int width = Mmax-Mmin+1;
        if(width<=0) continue;

        if(vlqq[na][nb][L].isZero(col_data->cif.multipole_cutoff))  continue;

        MatrixXcd Ra = MatrixXcd::Zero(g_o[na],width*nnz);
        for(int M=Mmin;M<=Mmax;M++)
            for(int mi=-mo_m[na],pos=0;mi<=mo_m[na];mi++)
                for(int li=abs(mi);li<=mo_l[na]; li++, pos+=g_o[na]){
                    double gf = gaunt_xch[na]->coeff(la,li,L,ma,mi,M);
                    if(abs(gf)>1e-14)
                        Ra.block(0,(M-Mmin)*nnz,g_o[na],nnz) += gf*tga.block(0,pos,nnz,g_o[na]).adjoint();
                }

        MatrixXcd Rb = MatrixXcd::Zero(g_o[nb],width*nnz);
        for(int M=Mmin;M<=Mmax;M++)
            for(int mi=-mo_m[nb],pos=0;mi<=mo_m[nb];mi++)
                for(int li=abs(mi);li<=mo_l[nb]; li++, pos+=g_o[nb]){
                    double gf = gaunt_xch[nb]->coeff(lb,li,L,mb,mi,M);
                    if(abs(gf)>1e-14)
                        Rb.block(0,(M-Mmin)*nnz,g_o[nb],nnz) += gf*tgb.block(0,pos,nnz,g_o[nb]).adjoint();
                }

        S += 4*math::pi/(2*L+1)*(vlqq[na][nb][L].array() * (Ra.conjugate()*Rb.transpose()).array()).matrix();
    }

    //Evaluate final Mab matrix
    if(not S.isZero(1e-14)){
        /*std::pair<std::unique_ptr<AlgebraTrunc>, Eigen::VectorXd> smoothener_na = get_smoothener(this, na),
                                                                  smoothener_nb = get_smoothener(this, nb);
        if(smoothener_na.first != nullptr && smoothener_nb.first == nullptr) {
            Eigen::VectorXd nasmooth(smoothener_na.second.size());
            Eigen::VectorXd nbsmooth = Eigen::VectorXd::Constant(g_o[nb], 1.);
            for(std::size_t n = 0; n < nasmooth.rows(); ++n) nasmooth(n) = smoothener_na.first->val(smoothener_na.second(n)).real();
            S = (S.array() * (nasmooth * nbsmooth.transpose()).array()).matrix();
        }
        else if(smoothener_na.first == nullptr && smoothener_nb.first != nullptr) {
            Eigen::VectorXd nbsmooth(smoothener_nb.second.size());
            Eigen::VectorXd nasmooth = Eigen::VectorXd::Constant(g_o[na], 1.);
            for(std::size_t n = 0; n < nbsmooth.rows(); ++n) nbsmooth(n) = smoothener_nb.first->val(smoothener_nb.second(n)).real();
            S = (S.array() * (nasmooth * nbsmooth.transpose()).array()).matrix();
        }
        else if(smoothener_na.first != nullptr && smoothener_nb.first != nullptr) {
            Eigen::VectorXd nasmooth(smoothener_na.second.size());
            Eigen::VectorXd nbsmooth(smoothener_nb.second.size());
            for(std::size_t n = 0; n < nasmooth.rows(); ++n) nasmooth(n) = smoothener_na.first->val(smoothener_na.second(n)).real();
            for(std::size_t n = 0; n < nbsmooth.rows(); ++n) nbsmooth(n) = smoothener_nb.first->val(smoothener_nb.second(n)).real();
            S = (S.array() * (nasmooth * nbsmooth.transpose()).array()).matrix();
        }*/
        res = poly_val[na]*S*poly_val[nb].transpose();
        return true;
    }
    else
        return false;

}

void mo::matrix_poly_2e_direct(MatrixXcd& res, const vector<int> &blockIdx, const vector<int> &blockJdx){

    /** @brief  computes @f$ \sum\limits_{i,j} \langle \chi_{\alpha} \phi_i | V_{ee} | \chi_{\beta} \phi_j \rangle \rho_{ij} @f$
   *
   */

    int la=blockIdx[0];
    int ma=blockIdx[1];
    int Ia=blockIdx[2];
    int na=blockIdx[3];
    int lb=blockJdx[0];
    int mb=blockJdx[1];
    int Ib=blockJdx[2];
    int nb=blockJdx[3];

    if(poly_val.size()==0)
        setup_pol_val();

    MatrixXd* val1 = &poly_val[na];
    MatrixXd* val2 = &poly_val[nb];

    // Direct term is zero if the two polynomials don't lie on same interval
    if(na!=nb){
        res = MatrixXcd::Zero(val1->rows(),val2->rows());
        return ;
    }
    int lmax = AxisForString("Eta")->basDef[0].order;   // DVR basis lmax
    int mmax = (AxisForString("Phi")->basDef[0].order-1)/2;   // DVR basis mmax - check??
    int lambda_upper_limit = 2*min(lmax,*max_element(mo_l.begin(),mo_l.end()));

    VectorXcd R = VectorXcd::Zero(g_o[na]);
    for(int L=0;L<=lambda_upper_limit;L++){
        int M_low = max(max(-2*(*max_element(mo_m.begin(),mo_m.end())),-2*mmax),-L);
        int M_up = min(min(2*(*max_element(mo_m.begin(),mo_m.end())),2*mmax),L);
        for(int M=M_low;M<=M_up;M++){
            double gf1=Gaunt::main.coeff(lb,L,la,mb,M,ma);
            if(abs(gf1)<1e-14) continue;
            R += gf1*R_dir[Ia][Ib][na][L][M-M_low];
        }
    }

    res = *val1 * R.asDiagonal() * val2->transpose();
}



