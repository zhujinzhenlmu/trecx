// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "basisMat.h"
#include <Eigenvalues>
#include "useMatrix.h"
#include "tools.h"
#include "functionOneArg.h"
#include "userFunctions.h"
#include <map>
#include <cmath>
#include "printOutput.h"

#include "stringTools.h"

#include "integrate.h"
#include "multiIndex.h"
#include "finDiff.h"
#include "operatorFactor.h"
#include "basisfunctionciion.h"
#include "basisfunctioncineutral.h"
#include "dvrMat.h"

using namespace std;


bool UserFunctions::haveBeenSet=false;
void UserFunctions::set(){
    if(haveBeenSet)return;
    haveBeenSet=true;
    list();
}

//====================================================
// this must be hidden from -Ofast optimization
// moved to file "basisMat_noopt.cpp" and compiled with -O2
//
//void BasisMat::funcDefaults()
//{
//    if(funcDefaultsSet)return;
//    funcDefaultsSet=true;
//    basisMatFuncSet(new basisMatRsq());
//    basisMatFuncSet(new basisMatHarm2());
//    basisMatFuncSet(new basisMatSpherBessel());
//    basisMatFuncSet(new basisMatExpI());
//    UserFunctions::set();
//}
//==========================================================================


// actually declare the static variable here
vector<vector<map<string,UseMatrix> > > BasisMat::table;

map<string,map<string,const UseMatrix*> >  BasisMat::tableMats;
vector<map<string,UseMatrix> >BasisMat::tableInts;
map<string,basisMatFunc*> BasisMat::BasisMatFuncs;

bool BasisMat::funcDefaultsSet=false;
bool BasisMat::femDVR=true;

void BasisMat::cleanUp(){
 for(auto p: BasisMatFuncs)delete p.second;
}

basisMatFunc* BasisMat::getFunction(string Definition){
    // set up all predefined functions
    funcDefaults();

    // check for previous definition
    if(not tools::hasKey(BasisMat::BasisMatFuncs,Definition)){
        const Algebra *a=new Algebra(Definition);
        if(not a->isAlgebra())
            ABORT("Cannot find basisMatFunc: "+Definition+"\n\nAvailable basisMatFunc's: \n"+tools::listMapKeys(BasisMat::BasisMatFuncs,"\n")+
                  "\n\nExpression = "+Definition+
                  "\nNot in list and not an algebra: "+Algebra::failures+
                  "\nDid you try to use basisMatFunc as element of algebra?");
        BasisMat::BasisMatFuncs[Definition]=new basisMatAlgebra(a);
    }
    return BasisMatFuncs[Definition];
}

unsigned int BasisMat::extraPoints(kind Kind, const BasisSet & Bas)///< additional points for accurate quadrature
{
    if(Bas.isIndex())return 0; // no quadratures on dummy
    switch(Kind){
    default: ABORT("define additional quadrature points for kind");
    case map:
        return 10;
    }

}

void BasisMat::getInts(string Name, string Pars, const BasisSet &iBas, UseMatrix &mat){

    //    if(iBas.eta()!=1.)PrintOutput::warning("cannot compute integrals in scaled region");
    //    if(iBas.eta()!=1.)ABORT("cannot compute integrals in scaled region");

    // search in table, extend if needed
    if(tableInts.size()<iBas.label+1)tableInts.resize(iBas.label+1);
    mat=tableInts[iBas.label][Name];
    if (mat.size()==0) {
        // need to create matrix

        // extract parameter values
        vector<double> par;
        vector<string> pars=tools::splitString(Pars,',');
        for (unsigned int i=0;i<pars.size();i++)par.push_back(tools::string_to_double(pars[i]));

        // get integration points
        UseMatrix x,w;
        if (Name=="delta"){
            x=UseMatrix::Constant(1,1,par[0]);
            w=UseMatrix::Constant(1,1,1.);
        } else {
            if(iBas.isIndex())iBas.quadRule(iBas.order(),x,w); // discrete must match points
            else   iBas.quadRule(iBas.order()+10,x,w);
        }

        // evaluate basis
        UseMatrix iVal,iDer;
        iBas.valDer(x,iVal,iDer);

        // get the function
        //        const FunctionOneArg *f=FunctionOneArg::get(Name+"["+Pars+"]");

        UseMatrix funcVal(x.size(),1),vInts(iBas.size(),1);
        // switch between functions
        //        for(unsigned int i=0;i<x.size();i++)
        //            funcVal(i)=f->val(x(i).real());
        basisMatFunc* f=getFunction(Name);
        for(unsigned int i=0;i<x.size();i++)f->operator()(x(i));

        // integrate
        vInts=iVal.adjoint()*w.cwiseProduct(funcVal);

        // place on diagonal of matrix
        mat=UseMatrix::Zero(iBas.size(),iBas.size());
        for(unsigned int i=0;i<mat.cols();i++)mat(i,i)=vInts(i);
    }
}

void BasisMat::get(string StrKind, const BasisSet &iBas, const BasisSet &jBas, UseMatrix & mat, string Par){


    complex<double> I (0.0,1.0);
    BasisMat::kind Kind=BasisMat::kindStr(StrKind);
    if(isLocal(Kind) and not iBas.hasOverlap(jBas)) return; /// !!! assumes local operators !!!
    if(not iBas.isIntegrable(jBas) and Kind!=map)ABORT("cannot compute <"+iBas.str()+"|"+StrKind+"|"+jBas.str()+">");

    // extend table where needed
    table.resize(max((int)table.size(),(int)iBas.label+1));
    table[iBas.label].resize(max((int)table[iBas.label].size(),(int)jBas.label+1));

    // if empty matrix
    string recKind=StrKind;
    if(Par!="")recKind+="["+Par+"]";
    mat=table[iBas.label][jBas.label][recKind];
    if (mat.size()==0) {
        // map between basis sets
        if(Kind==map){
            if (&iBas==&jBas) {
                mat=UseMatrix::Identity(iBas.size(),jBas.size());
            } else if(iBas.isIndex() or jBas.isIndex()){
                PrintOutput::warning("EXPERT: poorly defined map involving index-type basis");
                mat=UseMatrix::Constant(iBas.size(),jBas.size(),1.);
            } else if (iBas.isGrid() and not jBas.isGrid()) {
                mat=jBas.val(iBas.points);
            } else if (not iBas.isGrid() and jBas.isGrid()) {
                mat=iBas.val(jBas.points).transpose()*iBas.matrixWeigJacobian(jBas.points,jBas.weights);
            } else if (iBas.isGrid() and jBas.isGrid()) {
                if(iBas.points!=jBas.points){
                    iBas.show("map from");
                    jBas.show("      to");
                    ABORT("map between different grids not defined");
                }
                mat=UseMatrix::Identity(iBas.points.size(),jBas.points.size());
            } else if (iBas.lowBound()>=jBas.upBound() or iBas.upBound()<=jBas.lowBound()) {
                mat=UseMatrix::Zero(iBas.size(),jBas.size());
                cout<<"case zero overlap"<<endl;
            } else if (true){
                get(vector<string>(1,"1"),vector<const BasisSet*>(1,&iBas),vector<const BasisSet*>(1,&jBas),mat,vector<string>(1,""));
                UseMatrix ovr;
                get(vector<string>(1,"1"),vector<const BasisSet*>(1,&iBas),vector<const BasisSet*>(1,&iBas),ovr,vector<string>(1,""));
                ovr.solve(mat);
                mat.purge();
            } else {
                iBas.show("map from");
                jBas.show("      to");
                ABORT("map not defined");
            }
            return;
        }

        else if (StrKind=="mapDer") {
            if(jBas.isIndex() or jBas.isGrid() or not iBas.isGrid())
                ABORT("derivative map only from functions to grid, is:\n"+iBas.str()+" <-- "+jBas.str());
            mat=jBas.der(iBas.points);
            return;
        }
    }
    ABORT("case no longer covered: "+StrKind);
}

void BasisMat::mapSet(const BasisSet &iBas, const BasisSet &jBas, UseMatrix & mat){


    complex<double> I (0.0,1.0);
    if(not iBas.hasOverlap(jBas)) return; /// !!! assumes local operators !!!

    // extend table where needed
    table.resize(max((int)table.size(),(int)iBas.label+1));
    table[iBas.label].resize(max((int)table[iBas.label].size(),(int)jBas.label+1));

    mat=table[iBas.label][jBas.label]["mapSet"];
    // if empty matrix
    if (mat.size()==0) {
        // map between basis sets
        if (&iBas==&jBas) {
            mat=UseMatrix::Identity(iBas.size(),jBas.size());

        } else if(iBas.isIndex() and jBas.isIndex()){
            mat=UseMatrix::Constant(iBas.size(),jBas.size(),1.);

        } else if (iBas.isGrid() and not jBas.isGrid()) {
            mat=jBas.val(iBas.points);

        } else if (not iBas.isGrid() and jBas.isGrid()) {
            mat=iBas.val(jBas.points).transpose()*iBas.matrixWeigJacobian(jBas.points,jBas.weights);

        } else if (iBas.isGrid() and jBas.isGrid()) {
            if(iBas.points!=jBas.points){
                iBas.show("map from");
                jBas.show("      to");
                ABORT("map between different grids not defined");
            }
            mat=UseMatrix::Identity(iBas.points.size(),jBas.points.size());

        } else if (iBas.lowBound()>=jBas.upBound() or iBas.upBound()<=jBas.lowBound()) {
            mat=UseMatrix::Zero(iBas.size(),jBas.size());
        } else {
            bool saveDVR=femDVR;
            femDVR=false; // use exact integrals for mapping
            UseMatrix ovr;
            get(vector<string>(1,"1"),vector<const BasisSet*>(1,&iBas),vector<const BasisSet*>(1,&jBas),mat,vector<string>(1,""));
            get(vector<string>(1,"1"),vector<const BasisSet*>(1,&iBas),vector<const BasisSet*>(1,&iBas),ovr,vector<string>(1,""));

            mat=ovr.solve(mat);
            mat.purge();
            femDVR=saveDVR; // restore DVR status
        }
        table[iBas.label][jBas.label]["mapSet"]=mat;
    }

}

string BasisMat::stripDeriv_d(const string Oper){
    size_t i0=0,i1=Oper.length();
    if(i1>1){
        if(Oper.substr(0,2)=="d_")i0=2;
        if(Oper.substr(Oper.length()-2,2)=="_d")i1=Oper.length()-2;
    }
    return Oper.substr(i0,i1-i0);
}

TIMER(get,)
TIMER(getDVR,)
TIMER(getRecurs,)
void BasisMat::get(const std::vector<string> Oper,
                   const std::vector<const BasisSet*> IBas, const std::vector<const BasisSet *> JBas,
                   UseMatrix &mat, const vector<string> & Par,
                   const vector<unsigned int> &ISub, const vector<unsigned int> &JSub
                   )
{

    // create list of functions
    funcDefaults();

    // special cases
    if(Oper.size()==1){

        // old style matrix calculation
        if(BasisMat::kindStr(Oper[0])!=noKind){
            if(IBas.size()>1)ABORT("old style operators only with single basis set");
            get(Oper[0],*IBas[0],*JBas[0],mat);
            return;
        }


        if(IBas[0]->isIndex() and JBas[0]->isIndex()
                and OperatorFactor::matrix.count(Oper[0])!=0){
            mat=OperatorFactor::matrix[Oper[0]];
            if(mat.rows()!=IBas[0]->size() or mat.cols()!=JBas[0]->size())
                ABORT("OperatorFactor::matrix["+Oper[0]+"] "+mat.strShape()+" does not match basis size "+tools::str(IBas[0]->size())+" x "+tools::str(JBas[0]->size()));
            ABORT("debug: factor matrix");
            return;
        }

        // non-tensor dependence across index hierarchy
        if(Oper[0]=="{}"){
            if(Oper[0].find("_d")!=string::npos or Oper[0].find("d_")!=string::npos)
                ABORT("non-tensor operators not implemented with derivatives: "+Oper[0]);
            if(femDVR and (IBas[0]->fs->name().find("DVR")==0))
                mat=UseMatrix::Identity(IBas[0]->size(),JBas[0]->size());
            else
                mat=UseMatrix::Constant(IBas[0]->size(),JBas[0]->size(),1.);
            return;
        }

        // special cases for identity
        if((Oper[0]=="One" or Oper[0]=="1") and IBas.size()==JBas.size()){
            bool allGrid=true;
            for(unsigned int k=0;k<IBas.size();k++)
                allGrid=IBas[k]->isGrid() and JBas[k]->isGrid();
            if( (IBas[0]->isIndex() and JBas[0]->isIndex()) or
                    allGrid){
                mat=UseMatrix::Identity(IBas[0]->size(),JBas[0]->size());
                return;
            }
        }
        if((Oper[0]=="allOnes")){
            mat=UseMatrix::Constant(IBas[0]->size(),JBas[0]->size(),1.);
            return;
        }

        if(Oper[0]=="mapDer"){
            get("mapDer",*IBas[0],*JBas[0],mat);
            return;
        }

        // finite-difference scheme
        if(IBas.size()==1 and IBas[0]->size()>3 and IBas[0]==JBas[0] and JBas[0]->isGrid()){
            if(IBas[0]!=JBas[0])ABORT("grid operators only between identical grids "+IBas[0]->str()+" | "+JBas[0]->str());
            FinDiff::matrix(Oper[0],(unsigned int)(JBas[0]->param(0)),JBas[0]->points,JBas[0]->weights,
                    JBas[0]->def.comSca.r0up(),JBas[0]->def.comSca.eta,JBas[0]->def.par[1],mat,FinDiff::FDgrid);
            return;
        }

        if(Oper[0].find("Delta[")!=string::npos){
            if(not IBas[0]->isIndex() or not JBas[0]->isIndex() )
                ABORT(Oper[0]+" only with index basis, is:\n"+IBas[0]->str()+"\n"+JBas[0]->str());
            int off=tools::string_to_int(tools::stringInBetween(Oper[0],"[","]"));
            mat=UseMatrix::Constant(IBas[0]->size(),JBas[0]->size(),0.);
            for(int k=max(0,-off);k<min(mat.rows(),mat.cols()-off);k++){
                mat(k,k+off)=1.;
            }
            return;
        }

        // CIion
        if(IBas[0]->name()=="CIion"){
            mat=dynamic_cast<const BasisFunctionCIion*>(IBas[0]->PointerToFunctions())->matrix_1e(Oper[0]);
            return;
        }
        if(IBas[0]->name()=="CIneut"){
            mat=dynamic_cast<const BasisFunctionCINeutral*>(IBas[0]->PointerToFunctions())->matrix_1e(Oper[0]);
            return;
        }
    }

    if(not IBas[0]->isIntegrable(*JBas[0]))ABORT("cannot compute <"+IBas[0]->str()+"|"+Oper[0]+"|"+JBas[0]->str()+">");

    // re-construct full operator name for table
    string hashMat;
    if(Par.size()!=Oper.size())ABORT("specify as many Par's as there are operator factors");
    for(unsigned int k=0;k<Oper.size();k++){
        string recOper=Oper[k];
        if(Par[k]!="")recOper+="["+Par[k]+"]";
        hashMat+=recOper;
        if(k<Oper.size()-1)hashMat+="*";
        // re-set function parameters for integral calculation
        getFunction(stripDeriv_d(Oper[k]))->set(Par[k]);
    }

    // get hash for operator and bases
    for(unsigned int k=0;k<IBas.size();k++)hashMat+="|"+tools::str(IBas[k]->label);
    for(unsigned int k=0;k<JBas.size();k++)hashMat+="|"+tools::str(JBas[k]->label);
    if(femDVR)hashMat="DVR:"+hashMat;
    else      hashMat="FEM:"+hashMat;


    if(not tools::hasKey(tableMats,hashMat)){
        if(IBas.size()!=JBas.size())ABORT("number of left and right hand bases differ");
        if(ISub.size()!=JSub.size())ABORT("number of left and right hand subindices differ");
        // single operator and single basis

        // multi-dimensional integrals
        Ints integ=Ints(Oper,IBas,JBas,1.e-10,1.e-10);

        vector<vector<double> > vol;
        for(unsigned int k=0;k<IBas.size();k++){
            vol.push_back(vector<double>(3));
            vol.back()[0]=IBas[k]->lowBound();
            vol.back()[1]=IBas[k]->upBound();
            vol.back()[2]=IBas[k]->scale();
        }
        integ.message="";
        for(unsigned int k=0;k<Oper.size();k++)integ.message+="<"+IBas[k]->str()+"| "+Oper[k]+"| "+JBas[k]->str()+">";

        UseMatrix fullMat;

        if(femDVR and not IBas[0]->def.exactIntegral){
            STARTDEBUG(getDVR);
            hashMat += "NotExact";
            integ.nQuad.clear();
            integ.kind="lobatto";
            integ.kindInf="radau";
            if(IBas[0]->isIndex())integ.kind="index";
            for(unsigned int k=0;k<IBas.size();k++){
                integ.nQuad.push_back(vector<unsigned int>(1,IBas[k]->order()));

                // as functions may be discontinuous at boundary, use "open interval"
                // NOTE: this has been moved into BasisMat::Ints::func
                // where it affects only the function, not the basis
                //  if(vol[k][0]>-DBL_MAX/2)vol[k][0]+=1.e-14*abs(vol[k][2]);
                //  if(vol[k][1]< DBL_MAX/2)vol[k][1]-=1.e-14*abs(vol[k][2]);
            }

            if(IBas.size()==1){
                DvrMat(Oper[0],*DvrBasis::get(IBas[0]),*DvrBasis::get(JBas[0])).useMatrix(fullMat);
            }

            else
            {
                fullMat=integ.nDim(vol,BasisMat::Ints::func);
            }
            fullMat.purge();
            STOPDEBUG(getDVR);
        }
        else{
            hashMat += "Exact";
            if(IBas[0]->isGrid()) {

                if (JBas[0]->isGrid()) { // grid to grid
                    fullMat=UseMatrix::Zero(IBas[0]->size(), JBas[0]->size());
                    if (IBas[0]->points.operator==(JBas[0]->points)) {
                        for (unsigned int i=0; i!=fullMat.rows(); ++i) {
                            fullMat(i,i)=BasisMatFuncs[Oper.back()]->operator()(IBas[0]->points(i));
                        }
                    }
                    else if (fullMat.cols()==2) { // assume spherBessel or ExpI
                        BasisMatFuncs[Oper.back()]->set(Par.back());
                        for (unsigned int i=0; i!=fullMat.rows(); ++i) {
                            fullMat(i,std::atoi(Par[0].c_str()))=BasisMatFuncs[Oper.back()]->operator()(IBas.back()->points(i));
                        }
                    }
                }
                else {
                    ABORT("map implementation pending");
                }
            } else {
                STARTDEBUG(getRecurs);
                fullMat=integ.recursive(vol,BasisMat::Ints::func);
                STOPDEBUG(getRecurs);
            }
        }

        UseMatrix tmp;
        if(ISub.size()==0){
            tmp=fullMat;
            tableMats[hashMat][hashIndex(ISub,JSub)]=new UseMatrix(tmp);
        } else {
            // extract lowest level blocks from matrix
            if(ISub.size()!=IBas.size()-1)ABORT("there must be exactly one fewer subindex than basis set");
            vector<int> iMult,jMult;
            for(unsigned int k=0;k<IBas.size()-1;k++)iMult.push_back(IBas[k]->size());
            for(unsigned int k=0;k<JBas.size()-1;k++)jMult.push_back(JBas[k]->size());

            MultiIndex iii(iMult),jjj(jMult);
            iii.first(iMult);
            unsigned int ibl=0;
            do{
                jjj.first(jMult);
                unsigned int jbl=0;
                do{
                    // must copy, else c++ just makes the pointer permanent, but the data is not owned by block
                    tmp=fullMat.block(ibl,jbl,IBas.back()->size(),JBas.back()->size());
                    tableMats[hashMat][hashIndex(iMult,jMult)]=new UseMatrix(tmp);
                    jbl+=JBas.back()->size();
                } while (jjj.next(jMult));
                ibl+=IBas.back()->size();
            } while (iii.next(iMult));

        }
    }
    mat=*tableMats[hashMat][hashIndex(ISub,JSub)];
}

void BasisMat::getMultip(const string Oper, std::vector<const BasisSet *> IBas, std::vector<const BasisSet *> JBas, std::vector<UseMatrix *> &mat, const string &Par)
{
    if(IBas.size()!=2)ABORT("only implemented for two dimensions");

    // check string
    if(IBas.size()!=JBas.size())ABORT("numbers of tensor factor do not match");
    if(tools::subStringCount(Oper,"{")!=tools::subStringCount(Oper,"}"))ABORT("Brackets count {...} does not match "+Oper);
    if(tools::subStringCount(Oper,"<")!=tools::subStringCount(Oper,">"))ABORT("Brackets count <...> does not match "+Oper);
    if(tools::subStringCount(Oper,"{")!=tools::subStringCount(Oper,"<"))ABORT("Brackets count {}...{} does not match "+Oper);
    if(tools::subStringCount(Oper,"{")!=IBas.size())ABORT("OperatorMultip string "+Oper+" does not match number of basis functions "+tools::str(IBas.size()));

    // extract function from last factor
    string opFunc=Oper.substr(Oper.rfind("<"));
    opFunc=tools::stringInBetween(opFunc,"<","{");
    basisMatFunc * func=getFunction(opFunc);
    func=&func->set(Par);

    // get quadrature grid and values for each factor
    vector<unsigned int> dims;
    vector<UseMatrix> poin,weig;
    unsigned int total=1;
    for(unsigned int k=0;k<JBas.size();k++){

        // here we should check whether there is a common quadrature grid for left and right basis

        poin.push_back(UseMatrix());
        weig.push_back(UseMatrix());
        JBas[k]->quadRule(JBas.size()+4,poin.back(),weig.back());
        dims.push_back(poin.size());
        mat.push_back(new UseMatrix(JBas[k]->val(poin.back())));
        total*=poin.size();
    }

    // get values x weights
    vector<complex<double> >q(2);
    mat.insert(mat.begin(),new UseMatrix(total,total,0,0)); // insert diagonal matrix
    mat.insert(mat.begin(),new UseMatrix(total,total,0,0)); // insert auxiliary storage
    unsigned int ij=0;
    for(unsigned int i=0;i<poin[0].size();i++){
        q[0]=poin[0](i);
        for(unsigned int j=0;j<poin[0].size();j++,ij++){
            q[1]=poin[1](j);
            mat[0]->operator()(ij,ij)=func->operator()(q)*weig[0](i)*weig[1](j);
        }
    }
}

string BasisMat::hashIndex(const std::vector<unsigned int> &IMult, const std::vector<unsigned int> &JMult)
{return tools::str(IMult,",")+"|"+tools::str(JMult,",");}
string BasisMat::hashIndex(const std::vector<int> &IMult, const std::vector<int> &JMult)
{return tools::str(IMult,",")+"|"+tools::str(JMult,",");}


void BasisMat::Test(bool Print){
    PrintOutput::warning("BasisMat::Test() not functional - skipped");
    return;
    // compute the harmonic oscillator for testing
    int elem=10, order=10;
    double rmax=50.,x0,x1;
    vector<int> marg(2,0);
    marg[1]=order-1;
    int ntot=elem*(order-1);
    string ax="Rn";
    ComplexScaling coms(ax,0.,5.,0.,"ECS");
    UseMatrix Hmat=UseMatrix::Zero(ntot,ntot),Smat=UseMatrix::Zero(ntot,ntot);
    BasisSetDef def(order,0.,1.,"legendre",false,false,false,Coordinate::fromString(ax),marg);
    BasisSet sBas(def);
    UseMatrix mat;
    x1=0;
    int i0=0;
    for (int n=0;n<elem;n++){
        x0=x1;
        x1=x0+rmax/elem;
        def=BasisSetDef(order,x0,x1-x0,"legendre",false,n==0,n==elem-2,Coordinate::fromString(ax),marg,coms);
        sBas=BasisSet(def);
        sBas.show();
        vector<string>par(1,"");
        vector<const BasisSet*>bas(1,&sBas);
        get(vector<string>(1,"1"),bas,bas,mat,par);
        Smat.block(i0,i0,mat.rows(),mat.cols())+=(mat);
        get(vector<string>(1,"d_J_d"),bas,bas,mat,par);
        mat*=0.5;
        Hmat.block(i0,i0,mat.rows(),mat.cols())+=mat;
        get(vector<string>(1,"J/q"),bas,bas,mat,par);
        mat*=-1.;
        Hmat.block(i0,i0,mat.rows(),mat.cols())+=mat;
        i0+=mat.rows()-1;
    }
    UseMatrix Eval;
    Hmat.eigenValues(Eval,Smat);
    if(Print)Eval.transpose().block(0,0,1,10).print("Eigenvalues",12);
}

BasisMat::Ints::Ints(const vector<string> Oper, const std::vector<const BasisSet *> &IBas, const std::vector<const BasisSet *> &JBas,
                     double AccRel, double AccAbs, std::vector<std::vector<unsigned int> > NQuad,const string Kind, string KindInf)
    :Tools(AccRel,AccAbs,NQuad,Kind,KindInf, 20),iBas(IBas),jBas(JBas),dim(0),iDer(false),jDer(false)
{
    if(iBas.size()!=jBas.size())
        ABORT("number of left and right hand bases do not match: "+tools::str(int(iBas.size()))+" / "+tools::str(int(jBas.size())));

    // basis values or derivatives
    if(Oper[0].length()>1){
        if(Oper[0].substr(0,2)=="d_")iDer=true;
        if(Oper[0].substr(Oper[0].length()-2,2)=="_d")jDer=true;
    }
    if((iDer or jDer) and Oper.size()>1)
        ABORT("cannot combine derivatives with product of functions: "+tools::str(Oper));

    // set the integration function
    currentFunc.clear();
    for(unsigned int k=0;k<Oper.size();k++)
        currentFunc.push_back(getFunction(stripDeriv_d(Oper[k])));


    // default quadrature points
    if(nQuad.size()==0){
        for(unsigned int k=0;k<iBas.size();k++)
            nQuad.push_back(vector<unsigned int>(1,max(iBas[k]->size(),jBas[k]->size())+4));
    }
}

std::vector<const BasisSet*> BasisMat::Ints::currentBas;
std::vector<basisMatFunc*> BasisMat::Ints::currentFunc;

UseMatrix BasisMat::Ints::func(const vector<double>&Q){
    std::vector<std::complex<double> >zQ;
    for(unsigned int k=0;k<currentBas.size();k++){

        // functions may have discontinuities or  singularities at the interval boundaries
        // --- evaluate slightly away from boundaries ---
        // basis functions are continuous there and can be evaluated exactly at the boundaries
        // with FEM-DVR, most functions will be exactly =0 at the quadrature points, always evaluate there
        double eps=abs(1.e-14*currentBas[k]->scale()); // scale() can be negative
        zQ.push_back(currentBas[k]->def.comSca.xScaled(min(currentBas[k]->upBound()-eps,max(currentBas[k]->lowBound()+eps,Q[k]))));
    }
    complex<double> val=1.;
    for(unsigned int k=0;k<currentFunc.size();k++)
        val*=(*currentFunc[k])(zQ);
    return UseMatrix::Constant(1,1,val);
}

UseMatrix BasisMat::Ints::nDim(const std::vector<std::vector<double> > Vol,
                               const std::function<UseMatrix (std::vector<double>)> Func,
                               const std::vector<double>Params){

    // n-dimensional integration, recursive procedure starting from the last variable

    if(dim==0){
        xvals.resize(Vol.size());
        currentBas=iBas;
    }
    if(dim==Vol.size()){
        funcCount++;
        return UseMatrix::Constant(1,1,Func(xvals)(0,0));
    }
    // return matrix corresponding to lower level
    vector<double> qPoin(nQuad[dim][0]);
    vector<double> qWeig(qPoin.size());
    quadRule(Vol[dim],qPoin,qWeig);

    // next lower result dimension
    unsigned int mSize=1,nSize=1;
    for(unsigned int k=dim+1;k<iBas.size();k++)mSize*=iBas[k]->size();
    for(unsigned int k=dim+1;k<jBas.size();k++)nSize*=jBas[k]->size();
    UseMatrix block(mSize,nSize),blockI(mSize,nSize);

    // result storage
    UseMatrix result=UseMatrix::Zero(mSize*iBas[dim]->size(),nSize*jBas[dim]->size());

    dim++;
    for (unsigned int k=0;k<qPoin.size();k++){
        xvals[dim-1]=qPoin[k];
        UseMatrix iVal,jVal;
        if(iDer)iVal=iBas[dim-1]->der(UseMatrix::Constant(1,1,qPoin[k]))/iBas[dim-1]->eta();
        else    iVal=iBas[dim-1]->val(UseMatrix::Constant(1,1,qPoin[k]));
        if(jDer)jVal=jBas[dim-1]->der(UseMatrix::Constant(1,1,qPoin[k]))/jBas[dim-1]->eta();
        else    jVal=jBas[dim-1]->val(UseMatrix::Constant(1,1,qPoin[k]));
        block=nDim(Vol,Func,Params)*qWeig[k]*iBas[dim-1]->jacobian(qPoin[k])*iBas[dim-1]->eta();

        // multiply by factor functions and add into larger matrix
        // NOTE: element-wise access below is slow, replace in due time
        unsigned int ibl=0;
        double maxIVal=iVal.maxAbsVal();
        double maxJVal=jVal.maxAbsVal();
        for (unsigned int m=0;m<iVal.cols();m++){
            if(abs(iVal(m).complex())>maxIVal*1.e-10){

                if(iBas[dim-1]->isAbsorptive()){
                    blockI=block*iVal(m).complex();
                } else {
                    blockI=block*conj(iVal(m).complex());
                }
                unsigned int jbl=0;
                for (unsigned int n=0;n<jVal.cols();n++){
                    if(abs(jVal(n).complex())>maxJVal*1.e-10)result.block(ibl,jbl,block.rows(),block.cols())+=blockI*jVal(n);
                    jbl+=block.cols();
                }
            }
            ibl+=block.rows();
        }
    }
    dim--;
    return result;
}
