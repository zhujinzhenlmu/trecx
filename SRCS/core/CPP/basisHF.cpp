#include "basisHF.h"
#include "index.h"
#include "coefficients.h"

BasisHF::BasisHF(int NOrbs, const Index* Idx):BasisOrbitalNumerical("*:HF"){
    _orb.resize(NOrbs,Coefficients(Idx));

    for(int i=0;i<NOrbs;i++)_select.push_back(i);
}

void BasisHF::reset(const std::vector<const Coefficients*> & Orbs) const
{
        for(int k=0;k<Orbs.size();k++)_orb[k]=*Orbs[k];
}
