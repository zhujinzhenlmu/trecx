// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply.
//
// See terms of use in the LICENSE file included with the source distribution
//
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "timePropagatorOutput.h"

#include <complex>
#include "threads.h"
#include "timer.h"
#include "timeCritical.h"
#include "checkpoint.h"
#include "wavefunction.h"
#include "index.h"
#include "operator.h"
#include "printOutput.h"
#include "plot.h"
#include "readInput.h"
#include "coefficientsWriter.h"
#include "channelsSubregion.h"

#include "operatorDefinition.h"

#include "parallelOperator.h"
#include "timePropagatorOutput.h"
#include "coefficientsViewDeep.h"
#include "discretizationtsurffspectra.h"
#include "discretizationSurface.h"
#ifdef _USE_FFTW_
#include <fftw3.h>
#endif
using namespace std;

TIMER(outWrite,)
TIMER(outWrite1,)
TIMER(outWrite2,)
TIMER(outWrite3,)
TIMER(outWrite4,)
TIMER(outWrite5,)

int TimePropagatorOutput::obj_count = 0;


TimePropagatorOutput::TimePropagatorOutput(std::vector<OperatorAbstract *> Print, std::string ExpecFile, double PrintInterval,
                                           std::string WriteDir, const PlotCoefficients *WfPlot, std::vector<const OperatorAbstract *> Write,
                                           double WriteInterval, bool WriteAscii)
    :tStart(DBL_MAX),tEnd(-DBL_MAX),dir(WriteDir),wfPlot(WfPlot),expecOp(Print),printInterval(PrintInterval),
      writeOp(Write),writeInterval(WriteInterval),countPrint(0),countWrite(0),discSpec(0),_channels(0),expecStream(0),
      expecSample(100)
{

    obj_count++;
    timer=new Timer("propIntern"+tools::str(obj_count),"","timePropagator");
    if(wfPlot!=0){
        double t=printInterval;
        if(Units::isDefined("OptCyc"))t=Units::convert(t,"DEFAULT_SYSTEM","OptCyc");
        wfPlot->setPlotInterval(t,false);
    }

    if(WriteDir!="" and MPIwrapper::isMaster()){
        Checkpoint chPt(WriteDir);
        for(unsigned int k=0;k<Write.size();k++) {
            const Index* jI=Write[k]->iIndex;
            Coefficients cK(jI);
            const Coefficients* jK=Threads::join(cK);
            if (Threads::isMaster()){
                std::string fileName = Write[k]->name;
                if(chPt())std::rename((WriteDir+fileName).c_str(),(WriteDir+fileName+"_crashed").c_str());

                streamBin.push_back(new ofstream((WriteDir+fileName).c_str(),(ios_base::openmode) ios::beg|ios::binary));
                if(not ReadInput::main.flag("DEBUGskipCheck","DEBUGskipCheck"))
                jK->idx()->write(*streamBin.back()); // coefficients outputs go with Index info

                if(chPt()){
                    // copy crashed into new up to checkpoint time
                    ifstream ifil((WriteDir+fileName+"_crashed").c_str(),(ios_base::openmode) ios::beg|ios::binary);
                    if(ifil){
                        Coefficients fC(jK->idx());
                        bool head=true;
                        double timeOnFile;
                        do{
                            if (not fC.read(ifil,head))break;
                            tools::read(ifil,timeOnFile);
                            if(timeOnFile>=chPt.time())break;
                            fC.write(*streamBin.back(),false);
                            tools::write(*streamBin.back(),timeOnFile);
                            head=false;
                        } while(true);
                        std::remove((WriteDir+fileName+"_crashed").c_str());
                    }
                    else {
                        streamBin.pop_back();
                        PrintOutput::warning(Sstr+"has checkpoint at"+chPt.time()+"but no matching output file"+(WriteDir+fileName)+" - file will not be written");
                    }
                }
                if(WriteAscii or ReadInput::main.flag("DEBUGascii","force asci output for all data files")){
                    if(chPt.time()==-DBL_MAX)streamAsc.push_back(new ofstream((WriteDir+Write[k]->name+"Ascii").c_str(),(ios_base::openmode) ios::beg));
                    else PrintOutput::warning("cannot use ASCII output when restarting from checkpoint",1);
                }
            }

            if(ExpecFile!=""){
                // only master writes expectation values - do not create empty files
                if(chPt())std::rename((ExpecFile).c_str(),(ExpecFile+"_crashed").c_str());
                expecStream=new ofstream();
                expecStream->open(ExpecFile.c_str(),(ios_base::openmode) ios::beg);
                ifstream ifil((ExpecFile+"_crashed").c_str(),(ios_base::openmode) ios::beg);
                if(ifil){
                    std::string line;
                    while(getline(ifil,line)){
                        if(line[0]!='#'){
                            std::stringstream slin(line);
                            double time;
                            slin>>time;
                            if(time+printInterval*1.e-10>chPt.time())break;
                        }
                        *expecStream<<line<<std::endl;
                    }
                }
                std::remove((ExpecFile+"_crashed").c_str());
            }
        }
    }

    // storage for converting functions before write
    for(unsigned int k=0;k<Write.size();k++){
        temp.push_back(Coefficients(Write[k]->iIndex));
        temp.back().treeOrderStorage();
    }

    // always write first function
    lastTimeWritten=-DBL_MAX;

    // minimal time-interval for writing
    if(WriteInterval<0.)writeInterval=printInterval;

    if(ReadInput::main.flag("DEBUGfft","fourier transform of coefficients to plot")){
        for(unsigned int k=0;k<10;k++)PrintOutput::warning("huge coefs storage may be created");
        if(Print.size()==0)ABORT("for using DEBUGfft specify at least one operator to print");
        coefsT.resize(Print[0]->iIndex->sizeStored());
    }

}

bool inClosedInterval(double X,double From,double To,double Eps=1.e-10){
    if(Eps==0)return From<=X and X<=To;
    return From-Eps*max(abs(From),1.)<X and X<To+Eps*max(abs(To),1.);
}

void TimePropagatorOutput::print(const Coefficients *Wf, double Time, double TimeCPU){

    timeCritical::suspend();
    double growingNorm=1.;
    double time=Time;
    if(printInterval!=DBL_MAX){

        if(countPrint==0){
            PrintOutput::title("TIME PROPAGATION ("+ReadInput::main.output()+")");

            if(Time>tStart){
                PrintOutput::paragraph();
                PrintOutput::message(Sstr+"resuming from time"+Time+"after start"+tStart);
            }

            if(expecOp.size()>2){
                // list definitions of expectation value operators
                PrintOutput::paragraph();
                for(int k=2;k<expecOp.size();k++){
                    PrintOutput::lineItem("<"+expecOp[k]->name+">",expecOp[k]->def());
                    PrintOutput::newLine();
                }
            }

            PrintOutput::paragraph();
            PrintOutput::newRow();
            PrintOutput::rowItem("   CPU ");
            PrintOutput::rowItem(" (%)");
            PrintOutput::rowItem("   Time");
            for(unsigned int i=0;i<expecOp.size();i++){
                if(expecOp[i]->name.length()>16)
                    PrintOutput::rowItem("<"+expecOp[i]->name.substr(0,13)+"...>");
                else
                    PrintOutput::rowItem("<"+expecOp[i]->name+">");
            }
            //HACK re-establish distribution pattern of operators
            for(int k=0;k<expecOp.size();k++){
                ParallelOperator::setDistribution(expecOp[k]);
            }
            for(auto o: expecOp){
                o->update(Time,Wf);
                if(o->def().find("MeanEE")!=string::npos)dynamic_cast<OperatorTree*>(o)->updateNonLin(Time,Wf);
            }
        }
        PrintOutput::newRow();
        PrintOutput::rowItem(TimeCPU);
        PrintOutput::rowItem((Time-tStart)/(tEnd-tStart)*100.);

        // write near-zero time as 0
        if(abs(time)<abs(tStart)*1.e-12)time=0.;
        PrintOutput::rowItem(time,6);


        vector<unsigned int>compExp;
        for(unsigned int i=0;i<expecOp.size();i++){
            expecOp[i]->update(Time,Wf);
            complex<double> expec=expecOp[i]->matrixElementUnscaled(*Wf,*Wf);
            expec=Threads::sum(expec);
            PrintOutput::rowItem(real(expec),14);
            if(i>abs(expec.imag())>abs(expec.real())*1.e-12  and abs(expec.imag())>1.e-12)compExp.push_back(i);
            if(expecOp[i]->name.find("Overlap")!=string::npos)growingNorm=abs(expec);
        }

        for(unsigned int k=0;k<compExp.size();k++)
            if(expecOp[compExp[k]]->name!="Hamiltonian" and expecOp[compExp[k]]->name!="HamInitial")
                PrintOutput::warning("complex expectation value of operator "+expecOp[compExp[k]]->name,5);

        double t=time;
        if(Units::isDefined("OptCyc"))t=Units::convert(time,"DEFAULT_SYSTEM","OptCyc");
        vector<string> head(1,string("time="+tools::str(time,8)));
        const Coefficients *joinedWf=Threads::join(*Wf);
        if(wfPlot and Threads::isMaster())wfPlot->plot(*joinedWf,dir+wfPlot->briefName()+tools::str(t,5),head);

        PrintOutput::flush();
        if(growingNorm>1.2){
            MPIwrapper::Bcast(&growingNorm,1,MPIwrapper::Rank(Threads::all()));
            PrintOutput::warning(Str("likely instability - try smaller scaling angle or smaller operator threshold, norm=")+growingNorm);
        }
        if(Threads::isMaster())Checkpoint::write(dir,Time,joinedWf);

        write(Wf,Time,true); // force write whenever there is print
        flush();
        if(growingNorm>1.e10)MPIwrapper::Abort(1);

        countPrint++;
        timeCritical::resume();
    }
}

void TimePropagatorOutput::flush() const {
    for(auto b: streamBin)b->flush();
    for(auto a: streamAsc)a->flush();
    if(expecStream)expecStream->flush();
    PrintOutput::flush();
}

void TimePropagatorOutput::close(){
    if(expecStream)expecStream->flush();
    for(unsigned int c=0;c<streamBin.size();c++){
        streamBin[c]->close();
        if(streamBin[c]!=0) delete streamBin[c];
        streamBin[c] = 0;
    }
    for(unsigned int c=0;c<streamAsc.size();c++){
        streamAsc[c]->close();
        if(streamAsc[c]!=0) delete streamAsc[c];
        streamAsc[c] = 0;
    }
}

static int cntWrite=0;
void TimePropagatorOutput::write(const Coefficients *C, double Time, bool Force){

    if(lastTimeWritten>=Time)return; // never write backward or duplicate times
    if(_channels)_channels->average(C, Time);
    if(not Force and Time<nextWriteTime())return;
    START(outWrite);
    lastTimeWritten=Time;

    cntWrite++;

    // header on expec file
    if(expecStream and not expecStream->tellp()){
        for(unsigned int k=0;k<expecOp.size();k++)*expecStream<<"# "+expecOp[k]->name<<endl;
        *expecStream<<"#      Time         CPU";
        for(unsigned int i=0;i<expecOp.size();i++)*expecStream<<setw(20)<<"<"+expecOp[i]->name+">";
        *expecStream<<endl;
    }

    double t=Units::isDefined("OptCyc")? Units::convert(Time,"DEFAULT_SYSTEM","OptCyc") : Time;
    if(wfPlot){
        // on this level, true plots will only be done if in append mode
        const Plot* plot=dynamic_cast<const Plot*>(wfPlot);
        if(not plot or plot->isAppend()){
            const Coefficients *joinedC=Threads::join(*C);
            // only Threads master writes
            if(Threads::isMaster())
                wfPlot->plot(*joinedC,dir+wfPlot->briefName(),std::vector<std::string>(),tools::str(t,12));
        }
    }

    // apply operators and write wave functions
    STARTDEBUG(outWrite2);
    for(unsigned int k=0;k<writeOp.size();k++){
        const_cast<OperatorAbstract*>(writeOp[k])->axpy(1.,*C,0.,temp[k],Time);
        const Coefficients *joinedTemp=Threads::join(temp[k]);
        if(Threads::isMaster()){
            joinedTemp->write(*streamBin[k],false);
            tools::write(*streamBin[k],Time);
            if(streamAsc.size()>k){
                *streamAsc[k]<<Time;
                joinedTemp->print(*streamAsc[k]);
                streamAsc[k]->flush();
            }
        }
    }
    STOPDEBUG(outWrite2);

    // expectation values (if specified)
    STARTDEBUG(outWrite1);
    if((Force or (expecSample and not (countWrite%expecSample))) and expecOp.size()){
        if(expecStream)*expecStream<<setw(20)<<setprecision(12)<<Time<<setw(12)<<setprecision(4)<<timer->secs();
        for(unsigned int i=0;i<expecOp.size();i++){
            STARTDEBUG(outWrite3);
            complex<double> expec=expecOp[i]->matrixElementUnscaled(*C,*C);
            STOPDEBUG(outWrite3);
            STARTDEBUG(outWrite4);
            MPIwrapper::Barrier();
            if(i>1 and abs(expec.imag())>abs(expec.real())*1.e-12  and abs(expec.imag())>1.e-12
                    and expecOp[i]->name!="Hamiltonian")
                PrintOutput::warning("complex expectation value of operator "+expecOp[i]->name,5);
            double expecRe=Threads::sum(expec.real());
            STOPDEBUG(outWrite4);
            STARTDEBUG(outWrite5);
            if(expecStream)*expecStream<<setw(20)<<setprecision(12)<<expecRe;
            STOPDEBUG(outWrite5);
        }
        if(expecStream)*expecStream<<std::endl;
    }
    STOPDEBUG(outWrite1);
    countWrite++;
    STOP(outWrite);
}

void TimePropagatorOutput::coefsFFT() {
    if(not Operator::flat)return;
    if(coefsT.size()==0)return;
#ifdef _USE_FFTW_
    ofstream fftStream((ReadInput::main.output()+"coefs").c_str(),(ios_base::openmode) ios::beg);
    PrintOutput::paragraph();
    unsigned int nOmega=coefsT[0].size()/400;
    PrintOutput::message("Fourier transform of Coefficients on file "+ReadInput::main.output()+"coefs");

    fftw_complex *in, *out;
    fftw_plan p;
    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * coefsT[0].size());
    out =(fftw_complex*) fftw_malloc(sizeof(fftw_complex) * coefsT[0].size());
    vector<double> smooth(coefsT[0].size());
    for  (unsigned int k=0;k<smooth.size();k++)
        smooth[k]=pow(sin(k*math::pi/double(smooth.size())),8);
    for(unsigned int k=0;k<coefsT.size();k++){
        p = fftw_plan_dft_1d(coefsT[0].size(), in, out, FFTW_FORWARD, FFTW_ESTIMATE);
        for (unsigned int l=0;l<coefsT[k].size();l++){*(in+l)[0]=norm(coefsT[k][l])*smooth[l];*(in+l)[1]=0;}

        fftw_execute(p); /* repeat as needed */

        // output can be read into gnuplot
        double sum=0.;
        for (unsigned int l=0;l<coefsT[k].size();l++){
            sum+=(pow(*(in+l)[0],2)+pow(*(in+l)[1],2));
            if((l+1)%nOmega==0){
                fftStream<<sum<<endl;
                sum=0.;
            }
        }
        fftStream<<endl;

    }
    fftw_destroy_plan(p);
    fftw_free(in); fftw_free(out);
#endif
}

TimePropagatorOutput& TimePropagatorOutput::withChannelsSubregion(ChannelsSubregion* Channels){
    _channels=Channels;
    return *this;
}
