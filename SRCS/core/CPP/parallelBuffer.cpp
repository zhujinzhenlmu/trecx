// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "parallelBuffer.h"
#include "coefficients.h"
#include <numeric>
#include "str.h"
#include "timer.h"
void ParallelBuffer::add(const Coefficients *C){
    if(C->isContiguous())
        add(C->anyData(),C->size());
    else
        for(int k=0;k<C->childSize();k++)add(C->child(k));

}

int ParallelBuffer::extract(int Pos,Coefficients *C){
    int pos(Pos);
    if(C->isContiguous())
        pos=extract(pos,C->anyData(),C->size());
    else
        for(int k=0;k<C->childSize();k++)pos=extract(pos,C->child(k));
    return pos;
}

void ParallelBuffer::allGather(){
    std::vector<int> gsiz(MPIwrapper::Size(),0);
    gsiz[MPIwrapper::Rank()]=val.size();
    MPIwrapper::AllreduceSUM(gsiz.data(),gsiz.size());// time-consuming -not a large amount when multiple nodes
    std::vector<std::complex<double> > gval(std::accumulate(gsiz.begin(),gsiz.end(),0));
    if(gval.size()==0)return; //empty
    MPIwrapper::AllGatherV(val.data(),val.size(),gval.data(),gsiz.data());
    std::swap(val,gval);
}
