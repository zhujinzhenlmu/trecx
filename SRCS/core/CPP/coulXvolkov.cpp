#include "coulXvolkov.h"

#include "index.h"
#include "coefficientsFunction.h"

CoulXVolkov::CoulXVolkov(const Index *Idx){
   if(Idx->root()==Idx and Idx->hierarchy()!="Phi1.Eta1.kRn1.Phi2.Eta2.Rn2")DEVABORT("CoulXVolkov only for hierachy()==Phi1.Eta1.kRn1.Phi2.Eta2.Rn2");

   if(Idx->axisName()=="Rn2"){
       // workaround, just not to touch VolkovPhase constructor for now
       const Discretization *D=new _disc(Idx);
       _volk=std::shared_ptr<VolkovPhase>(new VolkovPhase(D,"Rn1"));
   }
   else {
       for(int k=0;k<Idx->childSize();k++)childAdd(new CoulXVolkov(Idx->child(k)));
   }
}

void CoulXVolkov::update(double Time, const Coefficients* CurrentVec){

 if(isLeaf())
     time=Time;
 else
     for (int k=0;k<childSize();k++)child(k)->update(Time);
}

void CoulXVolkov::apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    if(isLeaf())
        _volk->multiply(&Vec,&Y,time);
    else
        for(int k=0;k<childSize();k++)child(k)->apply(A,*Vec.child(k),B,*Y.child(k));
}
