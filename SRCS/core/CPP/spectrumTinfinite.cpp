// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "spectrumTinfinite.h"

#include <complex>

#include "readInput.h"
#include "operator.h"
#include "operatorTree.h"
#include "index.h"
#include "tree.h"
#include "discretizationSpectral.h"
#include "operatorDiagonal.h"
#include "wavefunction.h"
#include "operatorDefinition.h"
//#include "tsurffsource.h"
#include "tsurffSource.h"
#include "evaluator.h"
#include "discretizationSurface.h"
#include "discretizationtsurffspectra.h"
#include "surfaceFlux.h"
#include "plotSpectral.h"
#include "printOutput.h"
#include "basicDisc.h"
#include "resolvent.h"
#include "timer.h"

#ifndef _SEDNA_
#include "spectrumCoulomb.h"
#endif

using namespace std;

SpectrumTinfinite::SpectrumTinfinite(ReadInput &Inp, ReadInput &RunInp)
{
    RunInp.read("Operator","hamiltonian",hamDef,ReadInput::noDefault,"Hamiltonian definition");
    Inp.read("Analysis","points",radii,"","conpute spectra at these radii",0,"radii");
    Inp.read("Analysis","stretch",stretch,"1.","compute spectra at these radii",0,"stretch");
    if(radii.size()==0)
        RunInp.read("Surface","points",radii,"","save values and derivatives at surface points (blank-separated list)");

}


TIMER(hamilt,)
bool SpectrumTinfinite::compute(const tSurffTools::Evaluator &TSurff,vector<Coefficients> & Spec,double &Tend, Coefficients & AmpInfty){

    const Discretization * disc;
    disc=TSurff.D;


    if(true or disc->idx()->hierarchy()!="Phi.Eta.Rn.Rn"){
        PrintOutput::DEVwarning("SpectrumTinfinte temporarily disabled");
//        PrintOutput::warning("only for Phi.Eta.Rn, is: "+disc->Idx->hierarchy()+" --- not low energy spectrum will be computed");
        return false;
    }

    // get extended discretization
    double rMax=radii.maxAbsVal();
    Axis extA=TSurff.D->getAxis()[2];
    if(rMax>extA.boxsize()){
        BasisSetDef def=extA.basDef[extA.basDef.size()-2];
        def.scale*=stretch;
        while(extA.basDef.back().shift<rMax){
            // insert a copy of next-to-last element
            def.shift+=def.scale;
            extA.basDef.insert(extA.basDef.end()-1,1,def);
            extA.basDef.back().shift=def.shift+def.scale;
        }
        // adjust complex scaling (at large radii, only small angles work)
        extA.comsca=ComplexScaling("Rn",0.1,extA.basDef.back().shift,-extA.basDef.back().shift,"ECS");
        extA.basDef.back().order=min(int(extA.basDef.back().order*2),60); // we use a small scaling angle, ensure good absorption
        for(int k=0;k<extA.basDef.size();k++)extA.basDef[k].comSca=extA.comsca;
        vector<Axis>newAx(TSurff.D->getAxis());
        newAx[2]=extA;

        PrintOutput::title("Original and extended discretizations");
        disc=new BasicDisc(newAx);
    }

    if(abs(radii.back()-disc->getAxis()[2].boxsize())>1.)radii.push_back(disc->getAxis()[2].boxsize());

    PrintOutput::title(Str("Spectral analysis at end of pulse at surface radii =")+radii);

    // wf at end of pulse
    Wavefunction wfEnd(TSurff.D);
    ifstream stream((TSurff.outputDir+"wfEndPulse").c_str(),(ios_base::openmode) ios::beg|ios::binary);
    wfEnd.read(stream,true);
    if(wfEnd.coefs->norm()==0.)PrintOutput::warning(Str("zero function on")+TSurff.outputDir+"wfEndPulse");

    STARTDEBUG(hamilt);
    OperatorTree * hamiltonian=new OperatorTree("Hamiltonian",OperatorDefinition(hamDef,disc->idx()->hierarchy()),disc->idx(),disc->idx());
    STOPDEBUG(hamilt);

    Tend=DBL_MAX;
    Spec.resize(radii.size());
    for(int k=0;k<radii.size();k++){
        PrintOutput::message(Str("Surface at Rn=")+radii[k]);

        PrintOutput::message("Operator setup");
        DiscretizationSurface dSurf(disc,vector<double>(1,radii[k]),0);
        Operator commutator("Commutator",OperatorDefinition::get("Commutator",dSurf.idx()->hierarchy()),&dSurf,&dSurf);
        DiscretizationTsurffSpectra dKmom(&dSurf,ReadInput::main);
        Operator toKmom("surface2smoothSpecDisc",OperatorDefinition::get("surface2smoothSpecDisc",dSurf.idx()->hierarchy()),&dKmom,&dSurf);

        DEVABORT("needs re-implement");
//        PrintOutput::message("Apply resolvent");
//        Spec[k].reset(TSurff.tSource[0]->idx());
//        Spec[k].setToZero();
//        mapResolvent(wfEnd.coefs,Spec[k],hamiltonian,dynamic_cast<Operator*>(dSurf.mapFromParent()),commutator.child(1),toKmom.child(0));

//        // integration to infinity from standard surface has been calculated
//        if(disc==TSurff.D and abs(TSurff.tSource[0]->surfaceRadius("Rn")[0]-radii[k])<1.e-12){
//            PrintOutput::message(Str("Saving integral to infinity for Rc =")+radii[k]);
//            AmpInfty.reset(TSurff.tSource[0]->gridSpecDisc->Idx);
//            AmpInfty.treeOrderStorage();
//            AmpInfty.setToZero();
//            TSurff.tSource[0]->gridSpecDisc->mapFromParent()->axpy(Spec[k],AmpInfty);

//            Tend=wfEnd.time;
//            TSurff.tSource[0]->Volkov->multiply(&AmpInfty,&AmpInfty,Tend);

//        }
    }

    delete hamiltonian;
    return true;
}

void SpectrumTinfinite::mapResolvent(Coefficients * Wf, Coefficients &Spec, OperatorTree * Hamiltonian,
                                     Operator *ToSurface, Operator *Commutator, Operator *ToSpectrum)
{
    if(Wf->idx()->axisName()!="Rn"){

        for(int k=0;k<Wf->childSize();k++){
            int com=0,spe=0;
            while(Commutator->child(com)->jIndex->index()!=ToSurface->child(k)->iIndex->index()
                  or Commutator->child(com)->iIndex->index()!=Commutator->child(com)->jIndex->index())com++;
            while(ToSpectrum->child(spe)->jIndex->index()!=Commutator->child(com)->iIndex->index()
                  or ToSpectrum->child(spe)->iIndex->index()!=ToSpectrum->child(spe)->jIndex->index())spe++;
            while(Hamiltonian->descend()->iIndex==Hamiltonian->iIndex)
                Hamiltonian=Hamiltonian->descend();

            mapResolvent(Wf->child(k),*Spec.child(k),Hamiltonian->child(k),
                         ToSurface->child(k),Commutator->child(com),ToSpectrum->child(spe));
        }
        return;
    }

    Coefficients wf(Wf->idx());
    Coefficients wfX(Hamiltonian->iIndex,0.);
    wf.treeOrderStorage();
    wfX.treeOrderStorage();

    wf=*Wf;
    if(wf.idx()==wfX.idx()){
        wfX=wf;
    }
    else {
        int nBox=0;
        for(int k=0;k<Wf->idx()->childSize() and Wf->idx()->child(k)->basisSet()->eta()==1.;k++)
            nBox+=Wf->idx()->child(k)->basisSet()->size();
        for(int k=0;k<nBox;k++)wfX.data()[k]=wf.data()[k];
        wfX.data()[nBox]=wf.data()[nBox-1]; // throw in the first function in new element
    }

    Coefficients surf(ToSurface->iIndex);
    Coefficients comm(Commutator->iIndex);
    Coefficients kgrid(ToSpectrum->iIndex);
    Coefficients spec(ToSpectrum->iIndex);
    kgrid.treeOrderStorage();
    spec.treeOrderStorage();

    // momentum grid
    vector<double> kMom=ToSpectrum->iIndex->grid("kRn");

    if(kgrid.size()!=kMom.size())ABORT("something wrong");
    for(int k=0;k<kMom.size();k++){

        // do the loop only over momenta that remain contained in box
        Resolvent resolv(Hamiltonian,pow(kMom[k],2)*0.5);
        if(not resolv.verify(Hamiltonian,1.e-10))ABORT("incorrect resolvent");
        Coefficients ovrWf(wfX.idx()),resWf(wfX.idx());
        // apply i(H-k^2/2 S)^-1 S
        resolv.jIndex->overlap()->apply(1,wfX,0.,ovrWf);
        resolv.apply((0.,1.),ovrWf,0.,resWf);

        ToSurface->apply(1.,resWf,0.,surf); // to surface
        Commutator->apply(1.,surf,0.,comm); // apply commutator
        ToSpectrum->apply(1.,comm,0,kgrid); // to k-grid
        spec.data()[k]=kgrid.data()[k];
    }
    Spec=spec;
}
