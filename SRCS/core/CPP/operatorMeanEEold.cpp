#include "operatorMeanEEold.h"



bool OperatorMeanEEold::_store = false;
bool OperatorMeanEEold::_iterations = false;
bool OperatorMeanEEold::_noInteraction = false;
std::complex<double> OperatorMeanEEold::_matrixElement = 0.;
std::vector<double> OperatorMeanEEold::Jdown;
std::vector<double> OperatorMeanEEold::Jup;

OperatorMeanEEold::OperatorMeanEEold(const std::string Name, const std::string Def, const Index *IIndex, const Index *JIndex)
    :OperatorFloor(IIndex->sizeCompute(),JIndex->sizeCompute(),"MeanEE")
{

}

OperatorMeanEEold::OperatorMeanEEold(const std::vector<int> &Info, const std::vector<std::complex<double> > &Buf)
    : OperatorFloor("MeanEE")
{
    unpackBasic(Info,Buf);
}

OperatorMeanEEold::OperatorMeanEEold(std::string Pot, const Index* IIndex, const Index* JIndex, std::complex<double> Multiplier)
    : OperatorFloor("MeanEE"),
    _nodeVal((dynamic_cast<const BasisDVR*>(IIndex->basisAbstract()))->valNodes()),
    _nBeg((dynamic_cast<const BasisDVR*>(IIndex->basisAbstract()))->nBeg())
{
    dat=0;
    oNorm=1.;
    // zero norm in absorptive range or when non-local
    if(IIndex->basisIntegrable()->isAbsorptive()
            or IIndex->basisIntegrable()->lowBound()!=JIndex->basisIntegrable()->lowBound()
            or IIndex->basisIntegrable()->upBound()!=JIndex->basisIntegrable()->upBound()
            )
        oNorm=0.;

    bi=dynamic_cast<const BasisDVR*>(IIndex->basisAbstract());
    bj=dynamic_cast<const BasisDVR*>(JIndex->basisAbstract());

    if(bi==0)DEVABORT("not a DVR basis: "+IIndex->basisAbstract()->str());
    if(bj==0)DEVABORT("not a DVR basis: "+JIndex->basisAbstract()->str());
    if(bi->nodes()!=bj->nodes())DEVABORT("DVR nodes do not match: "+IIndex->basisAbstract()->str()+IIndex->basisAbstract()->str());

    for(std::complex<double> c: bi->valNodes())_basSq.push_back(std::norm(c));
    _storedRho.assign(bi->size(),0.);
    _storedCoeff.assign(bi->size(),0.);
    //std::vector<std::vector<std::complex<double>>> aux(bi->size(), std::vector<std::complex<double>>(bi->size(),0.));
    //_upTriangle=aux;
    _lowTriangle.assign(bi->size(),0.);
    _upTriangle.assign(bi->size(),0.);

    bi->dvrRule(_dvrPoints,_dvrWeights);
    _JupBoundary=bj->upBound();
    _JlowBoundary=bj->lowBound();

    _nSibling=JIndex->nSibling();
    //std::cout<<"_nSibling="<<_nSibling<<", _dvrPoints[0]="<<_dvrPoints[0]<<std::endl;
    _levelSize=JIndex->levelSize();
}

void OperatorMeanEEold::axpy(const std::complex<double> & Alfa, const std::complex<double>* X, unsigned int SizX, const std::complex<double> & Beta, std::complex<double>* Y, unsigned int SizY) const
{

    if(_iterations){          // Governs whether the iterative method is used
        if(_store){           // Governs storing in iterative method
            for(int i=0;i<SizX;i++){
                const_cast<OperatorMeanEEold*>(this)->_storedRho[i]=std::norm(_nodeVal[i]*X[i]);
                const_cast<OperatorMeanEEold*>(this)->_storedCoeff[i]=X[i];
                const_cast<OperatorMeanEEold*>(this)->calculateLowTriangle(SizX,i);
                const_cast<OperatorMeanEEold*>(this)->calculateUpTriangle(SizX,i);
            }
            const_cast<OperatorMeanEEold*>(this)->calculateJup(SizX);   //  Here we calculate integrals that will contribute
            const_cast<OperatorMeanEEold*>(this)->calculateJdown(SizX); //  to different V(a_i) in different combinations
            //std::cout<<"Jup[0]="<<Jup[0]<<std::endl;
        }
        if(_noInteraction){
            for(int i=0;i<SizX;i++){
            Y[i]=Beta*Y[i];
            }
        }
        else {
            //std::cout<<"Jdown.size()="<<Jdown.size()<<std::endl;
            //std::cout<<"_levelSize="<<_levelSize<<std::endl;

            double sumDown=0;                       //Here the upper rectangle is computed. It is independent of i (index of SizX)
            for(int k=0; k<_nSibling; k++){
                sumDown+=Jdown[k];

            }
            double sumUp=0;                         //Here the lower rectangle is computed. It is independent of i (index of SizX)
            for(int k=_nSibling+1; k<_levelSize; k++){
                sumUp+=Jup[k];
            }
            std::complex<double> V;
            for(int i=0;i<SizX;i++){
                V=_dvrWeights[i]*_basSq[i]*_dvrPoints[i]*( (sumUp+_upTriangle[i])*_dvrPoints[i]+sumDown+_lowTriangle[i] );
                //Y[i]=Beta*Y[i]+Alfa*(V*4.*3.1416+OperatorMeanEE::_matrixElement)*X[i];
                Y[i]=Beta*Y[i]+Alfa*V*4.*3.14*X[i];
                //Y[i]=Beta*Y[i];
            }
        }
    }
    else {
        for(int k=0;k<SizX;k++){
            Y[k]=Beta*Y[k]+Alfa*_dvrWeights[k]*std::norm(_basSq[k])*std::norm(X[k])*X[k];
            DEVABORT("not implemented");
        }
    }
}

void OperatorMeanEEold::pack(std::vector<int> &Info, std::vector<std::complex<double> > &Buf) const
{
    Buf.insert(Buf.end(),dat->begin(),dat->end());
    packBasic(Info,Buf);
}

void OperatorMeanEEold::calculateJup(int SizX){
    double sum=0;
    if(_nSibling==0)Jup.push_back(-9999.);
    else{
        //std::cout<<"_nSibling="<<_nSibling<<std::endl;
        for(int m=0; m<bi->size(); m++){
            sum+=_dvrWeights[m]*_storedRho[m]*_dvrPoints[m];
        }
        Jup.push_back(sum);
    }
}
void OperatorMeanEEold::calculateJdown(int SizX){
    if(_nSibling==_levelSize-1)Jdown.push_back(-9999.);
    else{
        double sum=0;
        for(int m=0; m<bi->size(); m++){
            sum+=_dvrWeights[m]*_storedRho[m]*_dvrPoints[m]*_dvrPoints[m];
        }
        Jdown.push_back(sum);
    }
}

void OperatorMeanEEold::calculateUpTriangle(int SizX, int i){

    double k;
    k = (_JupBoundary-_dvrPoints[i])/(_JupBoundary-_JlowBoundary);
    double r;
    std::vector<std::complex<double>> BasisVals;
    std::complex<double> sumM=0;
    for(int m=0; m<bi->size(); m++){
        r=_dvrPoints[i]+(_dvrPoints[m]-_JlowBoundary)*k;
        BasisVals=bj->val(r);
        std::complex<double> psiofr=0;
        for(int j=0; j<bi->size(); j++){
            psiofr+=BasisVals[j]*_storedCoeff[j];
        }
        sumM+=std::norm(psiofr)*_dvrWeights[m]*r;
    }
    _upTriangle[i]=k*sumM;
}

void OperatorMeanEEold::calculateLowTriangle(int SizX, int i){
    double k;
    k = (_dvrPoints[i]-_JlowBoundary)/(_JupBoundary-_JlowBoundary);
    double r;
    std::vector<std::complex<double>> BasisVals;
    std::complex<double> sumM=0;
    for(int m=0; m<bi->size(); m++){
        r=_dvrPoints[i]+(_dvrPoints[m]-_JupBoundary)*k;
        BasisVals=bj->val(r);
        std::complex<double> psiofr=0;
        for(int j=0; j<bi->size(); j++){
            psiofr+=BasisVals[j]*_storedCoeff[j];
        }
        sumM+=std::norm(psiofr)*_dvrWeights[m]*r*r;
    }
    _lowTriangle[i]=k*sumM;             // without 1/dvr
}


