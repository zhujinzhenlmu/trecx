// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "spectralCut.h"

#include "readInput.h"
#include "printOutput.h"
#include <string>
#include "discretization.h"
#include "basicDisc.h"
#include "discretizationSpectral.h"
#include "operatorData.h"
#include "operator.h"
#include "operatorTensorId.h"
#include "operatorDiagonal.h"

using namespace std;

/// map from full to spectral subspace(>Emax) and back to full
/// spectral cuts are applied to factor operators Factor (x) Id
/// Factor's are set up by constraining a full uperator to a subset of the axes
SpectralCut::SpectralCut(const Discretization *Disc, ReadInput &Inp,std::string DefaultOper)
{
    double eCut;
    int lin=1;
    string oper=DefaultOper;
    string subAx=Disc->coordinates();

    while(not Inp.endCategory("SpectralCut",lin)){

        Inp.read("SpectralCut","eMax",eCut,"Infty","remove eigenvalues > eMax",lin);
        Inp.read("SpectralCut","operator",oper,oper,"cut wrt to this operator",lin);
        Inp.read("SpectralCut","axes",subAx,subAx,"axis subset in the format \"X.Y.Eta\" (default - all axes)",lin);
        lin++;
        if(oper=="")ABORT("SpectralCut: empty operator or no default given");

        // (we may separate the following into a "setup" routine)

        // extract the subset of axes
        vector<string> subStr=tools::splitString(subAx,'.');
        vector<Axis> ax;
        vector<int> subset;
        for(int k=0;k<Disc->getAxis().size();k++)
            for(int l=0;l<subStr.size();l++)
                if(subStr[l]==Disc->getAxis()[k].name){
                    ax.push_back(Disc->getAxis()[k]);
                    subset.push_back(k);
                }

        // get the reduced operator definition (all non-factor terms will be omitted)
        string expOper=OperatorData::expandStandard(oper,Disc);
        cout<<"start "<<eCut<<" "<<oper<<" "<<subAx<<" "<<endl;
        vector<string> terms=OperatorData::terms(expOper);
        oper="";
        for(int k=0;k<terms.size();k++){
            // keep only standard single-coordinate products
            if(terms[k].find("[[")==string::npos and terms[k].find("{}")==string::npos)
            {
                //                oper+=seps[k];
                vector<string> fac=tools::splitString(terms[k],'>');
                //HACK for now, we throw away the banding information
                for(int l=0;l<subset.size();l++)oper+=fac[subset[l]]+">";
            }
        }

        // construct a BasicDisc from axis subset
        subD=new BasicDisc(ax);

        // set up the operator
        OperatorTree factor("fact",OperatorDefinition(oper,subD->idx()->hierarchy()),subD->idx(),subD->idx());

        // get the spectral disc
//        ConstrainedView * view =new ConstrainedView::factory(&factor);
        specD= new DiscretizationSpectral(subD,&factor,eCut);
        if(specD->idx()!=0){

            from.push_back(new OperatorTensorId(specD->mapFromParent(),Disc->idx()));

            to.push_back(new OperatorTensorId(specD->mapToParent(),from.back()->iIndex,Disc->idx()));

            // test projector property
            Coefficients ran(Disc->idx());
            Coefficients sav(to.back()->iIndex);
            ran.setToRandom();
            for(int k=0;k<2;k++){
                apply(1.,ran,0.,sav);
                if(k==0)ran=sav;
            }
            if(not (ran-=sav).isZero(1.e-10)){
                ABORT("spectral cut failed");
            }
            PrintOutput::message("size "+tools::str(specD->idx()->sizeStored())+" spectral projector above eCut="+tools::str(eCut)+", axes="+subAx);
            PrintOutput::message("for spectral operator="+factor.def());
        }
        else
            PrintOutput::warning("ZERO spectral projector above eCut="+tools::str(eCut)+", axes="+subAx+", operator="+factor.def());
    }
}

void SpectralCut::apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    for(int k=0;k<from.size();k++){
        from[k]->apply(1,Vec,0.,*from[k]->tempLHS());
        to[k]->apply(A,*from[k]->tempLHS(),B,Y);
    }
}

SpectralCut::~SpectralCut(){
    for(int k=0;k<from.size();k++)delete from[k];
    for(int k=0;k<  to.size();k++)delete to[k];
}
