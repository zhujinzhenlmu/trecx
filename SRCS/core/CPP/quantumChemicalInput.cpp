// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "quantumChemicalInput.h"
#include "read_columbus_data.h"
#include "sao.h"
#include "mo.h"
#include "str.h"
#include "asciiFile.h"
#include "printOutput.h"
#include "readInput.h"

using namespace std;
#include "eigenNames.h"
#include "basisMO.h"
#include "vectorValuedFunction.h"


QuantumChemicalInput* QuantumChemicalInput::ions=0;
QuantumChemicalInput* QuantumChemicalInput::neutrals=0;

void QuantumChemicalInput::read(ReadInput &Inp){
    if((ions==0) != (neutrals==0))ABORT("both, ions and neutrals must be set up simultaneously");
    if(ions!=0){
        // already set up - check consistency
        if(not ions->originalSource.find(Inp.output()))ABORT("ions not set up from requested source");
        if(not neutrals->originalSource.find(Inp.output()))ABORT("neutrals source "+neutrals->originalSource+" does not match run "+Inp.output());
        if(not     ions->originalSource.find(Inp.output()))ABORT("ion source "     +ions->originalSource    +" does not match run "+Inp.output());
    }
    else {
        if(Inp.found("Chemical")){
            // use precomputed data
            ions=new QuantumChemicalInput(Inp,"Ion");
            neutrals=new QuantumChemicalInput(Inp,"Neutral");

            // make MO's available as VectorValuedFunction
            std::string name=neutrals->_source.substr(0,neutrals->_source.length()-1);
            name=name.substr(name.rfind("/")+1);
            VectorValuedFunction::add(name,std::shared_ptr<VectorValuedFunction>(new BasisMO(neutrals)));
            VectorValuedFunction::add(name+"|ion",std::shared_ptr<VectorValuedFunction>(new BasisMO(ions)));
            PrintOutput::message("added molecular orbitals "+name+" and "+name+"|ion");

        }
        else if(Inp.found("Columbus")){
            // extract from COLUMBUS structured data
            ions=new QuantumChemicalInput(true);
            neutrals=new QuantumChemicalInput(false);
        }
        Inp.exclude("Chemical","Columbus");
    }
}

QuantumChemicalInput::QuantumChemicalInput(ReadInput &Inp, string Dir)
    :QuantumChemicalData(),det_cutoff(0),det_threshold(0.){
    double dblval;
    Inp.read("Chemical","data",_source,ReadInput::noDefault,"pre-computed and formated chemical structure data");
    Inp.read("Chemical","dets",det_cutoff,"99999999","maximum number of determinants to be considered in the CI basis expansion");

    Inp.read("Chemical","threshold",dblval,tools::str(cif.det_tol),"CI expansion coefficient threshold all determinants have to fulfill");
    if(cif.det_tol!=0. and dblval<cif.det_tol)PrintOutput::warning(Str("requested lower threshold than for original det_tol=")+cif.det_tol);
    cif.det_tol=dblval;

    Inp.read("Chemical","sceTolerance",dblval,tools::str(cif.sce_cutoff),"tolerance for the single-center-expansion");
    if(cif.sce_cutoff!=0. and dblval<cif.sce_cutoff)PrintOutput::warning(Str("requested lower sceTolerance than for original sce_cutoff=")+cif.sce_cutoff);
    cif.sce_cutoff=dblval;

    if(_source.rfind("/")!=_source.length()-1)_source+="/";
    read(_source+Dir);

    // construct auxiliary data
    molOrb = new mo(*this,0);
}


QuantumChemicalInput::QuantumChemicalInput(bool Ion)
    :QuantumChemicalData(),det_cutoff(0),det_threshold(0.)
{
    coluData = new columbus_data(Ion);
    originalSource=coluData->col_path;
    PrintOutput::message("read from COLUMBUS directory "+originalSource);

    cif=coluData->cif;

    // copy the parts of columbus data that is used outside
    charge=coluData->charge;
    cord=coluData->cord;
    no_atoms=coluData->no_atoms;
    no_electrons=coluData->no_electrons;
    det_cutoff=coluData->det_cutoff;
    det_threshold=coluData->det_threshold;

    Overlap=coluData->Overlap;
    KineticEnergy=coluData->KineticEnergy;
    Potential=coluData->Potential;

    //--------------------------------------------------------------------------------------
    // storage that is NOT in coluData
    coluData->read_primitive_gaussian_info(exponents,pow,coord);

    coluData->read_contraction_coefficients(aoCoef); //Returns a matrix with size:  num. of Atomic orbitals x num. of primitive gaussians

    _multiplicity=coluData->read_multiplitcity();
    coluData->read_sao_coefficients(saoCoef,ao(*this).coef.rows());  //Returns a matrix with size:  num. of symmetry Atomic orbitals x Atomic Orbitals

    coluData->read_mo_coefficients(moCoef,sao(*this).number_saos());      //Returns a matrix with size:  num. of molecular orbitals x num. of atomic orbitals

    // CI data, somewhat clumsy
    vector<vector<int > > determinants;
    coluData->read_determinants_allstates(determinants,ciCoef);
    if(determinants[0].size()!=no_electrons)ABORT("here I misunderstood something");
    for(int k=0;k<determinants.size();k++)columbusDets.insert(columbusDets.end(),determinants[k].begin(),determinants[k].end());

    // not nice - this may be a huge array
    vee=new My4dArray_shared(moCoef.rows());
    for(int k=0;k<coluData->vee->size();k++)vee->data()[k]=coluData->vee->data()[k];

    // save to ion or neutral data directory
    string outDir=coluData->HACKinput->output()+"Ion/";
    if(not Ion)outDir=coluData->HACKinput->output()+"Neutral/";

    // keep these checks until larger runs have been proven to work
    dimensionMatch("ao",aoCoef.cols(),exponents.size());
    dimensionMatch("sao",saoCoef.cols(),aoCoef.rows());
    dimensionMatch("mo",moCoef.cols(),saoCoef.rows());
    dimensionMatch("ovr",Overlap.cols(),moCoef.rows());
    dimensionMatch("kin",KineticEnergy.cols(),moCoef.rows());
    dimensionMatch("potC",Potential.cols(),moCoef.rows());
    dimensionMatch("potR",Potential.rows(),moCoef.rows());
    dimensionMatch("vee",vee->d,moCoef.rows());
    dimensionMatch("veeSize",vee->size(),std::pow(moCoef.rows(),4));

    delete coluData;

    // construct auxiliary data
    molOrb = new mo(*this,0);

    // save all input data to output directory and recover from there
    write(outDir);
}

std::vector<sod> QuantumChemicalInput::dets() {
    vector<sod> res;
    for(vector<int>::iterator c=columbusDets.begin();c!=columbusDets.end();c+=no_electrons)
        res.push_back(sod(vector<int>(c,c+no_electrons),*molOrb)); //CAUTION: the det's will keep pointers to the MO's
    return res;
}


void QuantumChemicalInput::write(string Dir) const{

    if(Dir.rfind("/")!=Dir.length()-1)Dir+="/"; // make sure there is an end
    folder::create(Dir);

    AsciiFile f(Dir+"pars");
    f.writeComments(std::vector<string>(1,"(DO NOT EDIT) control parameters for QuantumChemicalInput"));

    // convert to flat (vector) formats
    vector<complex<double> >ciFlat;
    vector<int>ciSize;
    vector<double> flatCord;
    for(int k=0;k<ciCoef.size();k++){
        ciSize.push_back(ciCoef[k].size());
        ciFlat.insert(ciFlat.end(),ciCoef[k].data(),ciCoef[k].data()+ciCoef[k].size());
    }
    for(int k=0;k<charge.size();k++)flatCord.insert(flatCord.end(),cord[k].data(),cord[k].data()+3);

    f.writeTagged("source",originalSource);
    f.writeTagged("path",cif.path);
    f.writeTagged("det_tol",cif.det_tol);
    f.writeTagged("sce_cutoff",cif.sce_cutoff);
    f.writeTagged("multipole_cutoff",cif.multipole_cutoff);
    f.writeTagged("multF",cif.multF);
    f.writeTagged("charge",charge);
    f.writeTagged("cord",flatCord);
    f.writeTagged("no_atoms",no_atoms);
    f.writeTagged("no_electrons",no_electrons);
    f.writeTagged("multiplicity",_multiplicity);

    f.writeTagged("nMO",moCoef.rows());
    f.writeTagged("nAO",aoCoef.rows());
    f.writeTagged("nSAO",saoCoef.rows());
    f.writeTagged("nGauss",exponents.size());
    f.writeTagged("nDets",columbusDets.size());

    f.writeTagged("nCI",ciCoef.size());
    f.writeTagged("ciSize",ciSize);

    AsciiFile(Dir+"Overlap").write(Overlap.data(),Overlap.size());
    AsciiFile(Dir+"KineticEnergy").write(KineticEnergy.data(),KineticEnergy.size());
    AsciiFile(Dir+"Potential").write(Potential.data(),Potential.size());
    AsciiFile(Dir+"Vee").write(vee->data(),vee->size());
    AsciiFile(Dir+"exponents").write(exponents.data(),exponents.size());
    AsciiFile(Dir+"pow").write(pow.data(),pow.size());
    AsciiFile(Dir+"coor").write(coord.data(),coord.size());
    AsciiFile(Dir+"aoCoef").write(aoCoef.data(),aoCoef.size());
    AsciiFile(Dir+"saoCoef").write(saoCoef.data(),saoCoef.size());
    AsciiFile(Dir+"moCoef").write(moCoef.data(),moCoef.size());
    AsciiFile(Dir+"ciCoef").write(ciFlat.data(),ciFlat.size());
    AsciiFile(Dir+"dets").write(columbusDets.data(),columbusDets.size());


}

void QuantumChemicalInput::read(string Dir) {

    if(Dir.rfind("/")!=Dir.length()-1)Dir+="/"; // make sure there is an end

    AsciiFile f(Dir+"pars");


    vector<double> flatCord;
    vector<complex<double> >ciFlat;
    vector<int>ciSize;
    int moRows,aoRows,saoRows,expSize,detsSize,nCI,ciTot=0;

    cif.numI=-1; // number of ions will be determined by axis input
    f.readTagged("source",originalSource);
    f.readTagged("path",cif.path);
    f.readTagged("det_tol",cif.det_tol);
    f.readTagged("sce_cutoff",cif.sce_cutoff);
    f.readTagged("multipole_cutoff",cif.multipole_cutoff);
    f.readTagged("multF",cif.multF);
    f.readTagged("charge",charge);
    f.readTagged("cord",flatCord);
    f.readTagged("no_atoms",no_atoms);
    f.readTagged("no_electrons",no_electrons);
    f.readTagged("multiplicity",_multiplicity);

    f.readTagged("nMO",moRows);
    f.readTagged("nAO",aoRows);
    f.readTagged("nSAO",saoRows);
    f.readTagged("nGauss",expSize);
    f.readTagged("nCI",nCI);
    f.readTagged("ciSize",ciSize);
    f.readTagged("nDets",detsSize);

    // prepare storage according to sizes
    if(ciSize.size()!=nCI)ABORT("something wrong");
    ciTot=0;
    for(int k=0;k<ciSize.size();k++)ciTot+=ciSize[k];

    pow.resize(expSize,3);
    coord.resize(expSize,3);
    exponents.resize(expSize);
    aoCoef.resize(aoRows,expSize);
    saoCoef.resize(saoRows,aoRows);
    moCoef.resize(moRows,saoRows);
    Overlap.resize(moRows,moRows);
    KineticEnergy.resize(moRows,moRows);
    Potential.resize(moRows,moRows);
    ciFlat.resize(ciTot);
    vee=new My4dArray_shared(moRows);
    columbusDets.resize(detsSize);

    AsciiFile(Dir+"Overlap").read(Overlap.data(),Overlap.size());
    AsciiFile(Dir+"KineticEnergy").read(KineticEnergy.data(),KineticEnergy.size());
    AsciiFile(Dir+"Potential").read(Potential.data(),Potential.size());
    AsciiFile(Dir+"Vee").read(vee->data(),vee->size());
    AsciiFile(Dir+"exponents").read(exponents.data(),exponents.size());
    AsciiFile(Dir+"pow").read(pow.data(),pow.size());
    AsciiFile(Dir+"coor").read(coord.data(),coord.size());
    AsciiFile(Dir+"aoCoef").read(aoCoef.data(),aoCoef.size());
    AsciiFile(Dir+"saoCoef").read(saoCoef.data(),saoCoef.size());
    AsciiFile(Dir+"moCoef").read(moCoef.data(),moCoef.size());
    AsciiFile(Dir+"ciCoef").read(ciFlat.data(),ciFlat.size());
    AsciiFile(Dir+"dets").read(columbusDets.data(),columbusDets.size());

    // convert to data format
    cord.clear();
    for(double* c=flatCord.data();c<flatCord.data()+flatCord.size();c+=3)cord.push_back(Eigen::Map<RowVector3d>(c));

    ciCoef.clear();
    complex<double>*c=ciFlat.data();
    for(int k=0;k<ciSize.size();k++){
        ciCoef.push_back(Eigen::Map<VectorXcd>(c,ciSize[k]));
        c+=ciSize[k];
    }


    // supplement the data path
    originalSource=Dir+"\nfrom "+originalSource;

    PrintOutput::message("QuantumChemicalInput from "+originalSource,0,true);
}
