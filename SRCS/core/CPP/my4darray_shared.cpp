// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "my4darray_shared.h"
#include "mpiWrapper.h"
#include <iostream>
#include <string>
//#include <boost/lexical_cast.hpp>

using namespace std;

int My4dArray_shared::obj_count = 0;
int My4dArray_shared::extra_index = 0;

My4dArray_shared::My4dArray_shared(int size, bool cout_info):isZero(false)
{
  this->d = -1;          // indicates if already constructed
  if(size!=0) construct(size,cout_info);
}

void My4dArray_shared::construct(int size, bool cout_info){

  obj_count++;

  if(this->d != -1)ABORT("My4dArray_shared::construct Already constructed: should not be called");
  this->d = size;

#ifndef __NO_SHM_4d__
  MPIwrapper::Barrier();
  int id = MPIwrapper::Rank();
  if(id==0 and cout_info) cout << "creating sharedMemoryXcdVector ... "<<obj_count;
  MPIwrapper::Barrier_slowButHard();

  // id=0 needs to allocate the memory
  if(id == 0){
      int sizeofsharedmemory = std::pow(size,4)*sizeof(double);

      // find a name for the shared memory that is not already used (by other running programs) and allocate
      bool foundAfreeName = false;
      while(foundAfreeName==false){
          DEVABORT("get non-boost solution");
//          shared_memory_name = "My4dArray_shared"+boost::lexical_cast<string>(extra_index)+boost::lexical_cast<string>(obj_count);
//         try{
//            this->segment = new boost::interprocess::managed_shared_memory(boost::interprocess::create_only, shared_memory_name.c_str(), size_t(sizeofsharedmemory+1000));
//          }
//          catch(boost::interprocess::interprocess_exception e){
//            if(cout_info)
//              cout << "MPIwrapper::SharedMemoryXcdVector::SharedMemoryXcdVector: failed to create shared memory object. trying with different name ... ";
//            extra_index += 20;
//            continue;
//          }
          foundAfreeName = true;
        }

      // construct the vector in this segment
      string vectorname = shared_memory_name+"MyVector";
      const ShmemAllocator alloc_inst(this->segment->get_segment_manager());
      x = this->segment->construct<MyVector>(vectorname.c_str())(alloc_inst);
      x->resize(std::pow(d,4));
      (Eigen::Map<Eigen::VectorXd>(&((*x)[0]),x->size())).setZero();
      if(cout_info)
        std::cout <<" Vector size: " << x->size() << " | Vector capacity: " << x->capacity() << " | Memory Free: " << this->segment->get_free_memory() << std::endl;
      // delete segment to reopen as open only (this is not necessary, it is basically stupid to do it like this)
      delete this->segment;
    }
  MPIwrapper::Barrier_slowButHard();

  // communicate the found name of the the shared memory
  int send = obj_count;
  int send_extra = extra_index;
#ifndef _NOMPI_
  MPI_Bcast(&send,1,MPI_INT,0,MPIwrapper::communicator());
  MPI_Bcast(&send_extra,1,MPI_INT,0,MPIwrapper::communicator());
#endif
  //cout << id << ": " << send << endl;
  DEVABORT("get non-boost solution");
//  shared_memory_name = "My4dArray_shared"+boost::lexical_cast<string>(send_extra)+boost::lexical_cast<string>(send);
  string vectorname = shared_memory_name+"MyVector";

  // all processes open the shared memory and map it as a vector
  DEVABORT("get non-boost solution");
//  this->segment = new boost::interprocess::managed_shared_memory(boost::interprocess::open_only, shared_memory_name.c_str());
  x = this->segment->find<MyVector>(vectorname.c_str()).first;

  MPIwrapper::Barrier_slowButHard();
#else
  int d4 = std::pow(this->d,4);
  x = new MyVector[d4];
#endif
}

My4dArray_shared::~My4dArray_shared(){
  if(not isZero){
#ifndef __NO_SHM_4d__
      MPIwrapper::Barrier_slowButHard();
      string vectorname = shared_memory_name+"MyVector";
      int id = MPIwrapper::Rank();
      if(id==0) cout << "destroying sharedMemoryXcdVector ..." << endl;

      if(id == 0) this->segment->destroy<MyVector>(vectorname.c_str());
      delete this->segment;
      DEVABORT("get non-boost soltion");
//      boost::interprocess::shared_memory_object::remove(shared_memory_name.c_str());

      MPIwrapper::Barrier_slowButHard();
#else
      delete[] x;
#endif
    }
}

double * My4dArray_shared::data(){
#ifndef __NO_SHM_4d__
  return &((*x)[0]);
#else
  return x;
#endif
}

unsigned int My4dArray_shared::size(){
  return std::pow(this->d,4);
}

void My4dArray_shared::assign(int l1, int l2, int l3, int l4, double num){

  if(isZero)  ABORT("My4dArray_shared::assign - Turned to read_only status");
  (data())[(find_pos(l1,l2,l3,l4))] = num;

}

int My4dArray_shared::find_pos(int l1, int l2, int l3, int l4)
{
//  return l1+d*(l2+d*(l3+d*l4));

    return l4+d*(l3+d*(l2+d*l1)); // Row major - to keep it consitent with My4dArray that uses boost (stored in row major)
}

void My4dArray_shared::addto(int l1, int l2, int l3, int l4, double num){

  if(isZero)  ABORT("My4dArray_shared::addto - Turned to read_only status");
  (Eigen::Map<Eigen::VectorXd>(&((data())[0]),size()))(find_pos(l1,l2,l3,l4)) += num;

}

double My4dArray_shared::val_at(int l1, int l2, int l3, int l4){

  if(isZero)   return 0.0;
  return (data())[(find_pos(l1,l2,l3,l4))];

}

double My4dArray_shared::dot(My4dArray_shared *b){

  //returns \sum_{a,b,c,d} this(a,b,c,d)*b(a,b,c,d)


  if(this->d!=b->d)
    ABORT("Dimension don't match My4dArray_shared::dot(My4dArray_shared& b) - cannot perform this operation");

  if(b->isZero or isZero) return 0.0;
  return ((Eigen::Map<Eigen::ArrayXd>(&((data())[0]),size()))*(Eigen::Map<Eigen::ArrayXd>(&((b->data())[0]),size()))).sum();
}

double My4dArray_shared::dot(My4dArray &b)
{
  //returns \sum_{a,b,c,d} this(a,b,c,d)*b(a,b,c,d)


  if(this->d!=b.d1 or this->d!=b.d2 or this->d!=b.d3 or this->d!=b.d4)
    ABORT("Dimension don't match My4dArray_shared::dot(My4dArray& b) - cannot perform this operation");

  if(b.isZero or isZero) return 0.0;
  return ((Eigen::Map<Eigen::ArrayXd>(&((data())[0]),size()))*(Eigen::Map<Eigen::ArrayXd>(b.A.data(),size()))).sum();

}

void My4dArray_shared::remove_zero(){

  // helper function - usable for matrices that are setup once and not modified again. If entire matrix is zero,
  // it deallocates memory and declares the matrix as readonly

  isZero = (Eigen::Map<Eigen::ArrayXd>(&((data())[0]),size())).isZero(1e-14);
  if(isZero){
#ifndef __NO_SHM_4d__
      // Delete stuff
      MPIwrapper::Barrier_slowButHard();
      string vectorname = shared_memory_name+"MyVector";
      int id = MPIwrapper::Rank();
      if(id==0) cout << "destroying sharedMemoryXcdVector ..." << endl;

      if(id == 0) this->segment->destroy<MyVector>(vectorname.c_str());
      delete this->segment;
      DEVABORT("get non-boost solution");
//      boost::interprocess::shared_memory_object::remove(shared_memory_name.c_str());

      MPIwrapper::Barrier_slowButHard();
#else
      delete[] x;
#endif
    }
}

