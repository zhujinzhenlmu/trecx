// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "extra_functions.h"
#include <fstream>
//#include <boost/math/special_functions/legendre.hpp>
//#include <boost/math/special_functions/laguerre.hpp>

#include "lapacke.h"
#include "abort.h"

using namespace std;
#include "eigenNames.h"


void h_roots(int N , VectorXd &x, VectorXd &w){
	
  alglib::ae_int_t n=N;
  alglib::ae_int_t info;
  alglib::real_1d_array xq;
  alglib::real_1d_array wq;
  alglib::gqgenerategausshermite(n , info, xq, wq);

  x = VectorXd::Zero(N);
  w = VectorXd::Zero(N);
	
  for(int i=0; i < N; i++){
    w(i)=wq[i];
    x(i)=xq[i];
  }	
}

void p_roots(int N , VectorXd &x, VectorXd &w, double lb, double ub){
	
  alglib::ae_int_t n=N;
  alglib::ae_int_t info;
  alglib::real_1d_array xq;
  alglib::real_1d_array wq;
  alglib::gqgenerategausslegendre(n , info, xq, wq);
	
  x = VectorXd::Zero(N);
  w = VectorXd::Zero(N);

  for(int i=0; i < N; i++){
    w(i)=wq[i];
    x(i)=xq[i];
  }	

  x = lb + (x.array()+1.0).array()*(ub-lb)/2.0;
  w = w*(ub-lb)/2.0;
	
}

void l_roots(int quadOrder, VectorXd &x, VectorXd &w, double x0, double alpha){

  //laguerre exponential roots

  alglib::ae_int_t er;
  alglib::real_1d_array x1;
  alglib::real_1d_array w1;
  alglib::gqgenerategausslaguerre(quadOrder, 0.0, er,x1,w1);
  double temp = 1/(alpha);

  x = VectorXd::Zero(quadOrder);
  w = VectorXd::Zero(quadOrder);

  for(int i=0;i<quadOrder;i++){
    w(i)=w1[i];
    x(i)=x0+x1[i]*temp;
  }

}

void lobatto_quadrature(int order,VectorXd & quadraturePoints,VectorXd & weights, double lb, double ub){

  int N=order;                   
  quadraturePoints = VectorXd::Zero(N);
  weights = VectorXd::Zero(N);
  alglib::real_1d_array alpha;
  alglib::real_1d_array beta;
  alglib::real_1d_array x1;
  alglib::real_1d_array w1;
  alpha.setlength(N-1);
  beta.setlength(N-1);
  x1.setlength(N);
  w1.setlength(N);
  alglib::ae_int_t info;

  for(int i=0;i<N-1;i++)
    alpha(i)=0;

  for(int i=0;i<N-1;i++)
    beta(i)=double(i*i)/double(4*i*i-1);

  alglib::gqgenerategausslobattorec(alpha, beta, (double)2.0, (double)(-1.0) , (double)1, N, info, x1, w1);

  if (info!=1)
    cout<<"Error in Gauss-Lobatto quadrature. Error code "<<info<<endl;

  double temp = (ub-lb)/2.0;
  for(int i=0;i<N;i++){
    quadraturePoints(i)=lb+(x1[i]+1.0)*temp;
    weights(i)=w1[i]*temp;
  }

}

// defining factorial
long factorial (int a)
{
  if (a > 1)
    return (a * factorial (a-1));
  else
    return 1;
}
// defining a binomial function
double binomial(int n,int l){
  return factorial(n)/(factorial(n-l)*factorial(l));
}

bool parity_of_permutation(VectorXi &o, VectorXi p){

  if(o.rows()!=p.rows() or o.cols()!=p.cols()){
      cout<<"Unexpected input: parity_of_permutation()\n";
      exit(1);
    }
  //returns true if even permutation, else returns false

  bool res = true;
  for(int i=0;i<o.size();i++){
      int pos=0;
      for(int j=0;j<o.size();j++){
          if(o[i]==p[j]){
              pos=j;
              break;
            }
        }
      if(pos!=i){
          int temp = p[i];
          p[i] = p[pos];
          p[pos] = temp;
          res = not(res);
        }
    }
  if(not(o==p)) cerr<<"Did not reach complete overlap: cannot be wrong parrity\n";
  return res;
}

void generate_permutations(VectorXi &o, vector<VectorXi > &p, vector<bool > &par){

  clock_t start = clock();
  //generates all permutations along with parity of a given vector

  p.clear();
  par.clear();

  VectorXi b = o;

  do{
      p.push_back(b);
      par.push_back(parity_of_permutation(o,b));
    } while(prev_permutation(b.data(),b.data()+b.size()));

  b=o;

  while(next_permutation(b.data(),b.data()+b.size())){
      p.push_back(b);
      par.push_back(parity_of_permutation(o,b));
    }
//#ifndef JAKOB
//  permute_time += (double)(clock()-start)/CLOCKS_PER_SEC;
//#endif
}

scaled_legendre::scaled_legendre(double x0, double x1, int order){

    this->x0 = x0;
    this->x1 = x1;
    if (order<0){
        cout<<"Order cannot be negative\n";
        exit(1);
    }
    this->order = order;

    VectorXd r_quad,r_weig;
    p_roots(order,r_quad,r_weig,x0,x1);
    MatrixXd r_vals;
    this->values(r_quad,r_vals);

    MatrixXd temp = MatrixXd::Zero(order,order);
    for(int i=0;i<order;i++)
        for(int j=0;j<order;j++)
            for(int r=0;r<order;r++)
                temp(i,j)+=r_vals(i,r)*r_vals(j,r)*r_weig(r);

    this->norms = temp.diagonal();
}

void scaled_legendre::values(const VectorXd &x, MatrixXd &vals){

    VectorXd x1;
    x1 = x.array() - this->x0;
    x1*=(2.0/(this->x1-this->x0));
    x1 = x1.array()-1.0;

    vals = MatrixXd::Zero(this->order,x1.size());

    for(int i=0;i<x1.size();i++){
        DEVABORT("replace with local legendre, do not use boost");
//        vals(0,i) = boost::math::legendre_p(0,x1[i]);
//        vals(1,i) = boost::math::legendre_p(1,x1[i]);
//        for(int j=2;j<this->order;j++)
//            vals(j,i) = boost::math::legendre_next(j-1, x1[i], vals(j-1,i), vals(j-2,i));
    }

    if(norms.size()!=0){
        for(int i=0;i<order;i++)
            vals.row(i)/=sqrt(this->norms(i));
    }
}


scaled_laguerreexpon::scaled_laguerreexpon(double x0, double alpha, int order)
{
  this->x0 = x0;
  this->alpha = alpha;
  if (order<0){
      cout<<"Order cannot be negative\n";
      exit(1);
  }
  this->order = order;

  VectorXd r_quad,r_weig;
  l_roots(order,r_quad,r_weig,x0,alpha);
  MatrixXd r_vals;
  this->values(r_quad,r_vals);

  MatrixXd temp = MatrixXd::Zero(order,order);
  for(int i=0;i<order;i++)
      for(int j=0;j<order;j++)
          for(int r=0;r<order;r++)
              temp(i,j)+=r_vals(i,r)*r_vals(j,r)*r_weig(r);

  this->norms = temp.diagonal();
}

void scaled_laguerreexpon::values(const VectorXd &x, MatrixXd &vals)
{
  VectorXd x1;
  x1 = alpha*(x.array()-x0);

  vals = MatrixXd::Zero(this->order,x1.size());

  for(int i=0;i<x.size();i++){
      DEVABORT("replace with non-boost solution");
//      vals(0,i) = boost::math::laguerre(0,x1[i]);
//      vals(1,i) = boost::math::laguerre(1,x1[i]);
//      for(int j=2;j<this->order;j++)
//          vals(j,i) = boost::math::laguerre_next(j-1, x1[i], vals(j-1,i), vals(j-2,i));
  }

  if(norms.size()!=0)
  for(int i=0;i<vals.cols();i++)
    vals.col(i) *= exp(-x1[i]/2);

  if(norms.size()!=0){
      for(int i=0;i<order;i++)
          vals.row(i)/=sqrt(this->norms(i));
  }
}
