#include "operatorDefinitionNew.h"

#include <vector>

#include "operatorData.h"
#include "operatorDefinition.h"

using namespace std;

OperatorDefinitionNew::OperatorDefinitionNew(const std::string Def, std::string Hierarchy)
    :OperatorDefinition(Def)
{
    if(Def=="")return;

    if(Def.find("<<1>>")!=string::npos)
        ABORT("OBSOLETE operator definition <<1>>, replace by <<Overlap>>");

    if(tools::subStringCount(Def,">>")!=tools::subStringCount(Def,"<<"))
        ABORT("unmatched <<...>> in "+Def);

    //HACK
    if(     Def == "+[[eeInt6DHelium]]" or
            Def == "+[[HartreeFock]]" or
            Def.find("[[haCC") != std::string::npos
             ) {
        assign(Def);
        return;
    }

    // remove hybrid axes
    std::string hierarchy;
    for(std::string ax: tools::splitString(Hierarchy,'.'))
        if(ax.find("&")==string::npos)hierarchy+="."+ax;
    hierarchy.erase(hierarchy.begin());

    // parent Hierarchy and original input coordinates
    string parentHier=parentHierarchy(hierarchy);
    string inputCoor=inputCoordinates(parentHier);

    // split into terms, apply to each term
    string def;
    vector<string>term=terms(Def);

    // empty operator - do nothing
    if(term[0]=="");

    // multiple terms
    else if(term.size()>1){
        for(int k=0;k<term.size();k++)
            def+=OperatorDefinitionNew(term[k],inputCoor);
    }


    // factor<singleFactor>OpStrX
    else if(""!=firstFactor(term[0])){
        string parF=parameter(term[0],"+");
        string singF=firstFactor(term[0]);
        string debug=OperatorDefinitionNew(remainder(term[0],singF),inputCoor.substr(inputCoor.find(".")+1));
        vector<string>termX=terms(debug);
        for(int k=0;k<termX.size();k++)
            def+=parameter(termX[k],parF)+compose(noParameter(singF),noParameter(termX[k]));
    }
    // factor<<PredefinedOp>>OpStrX
    else if(""!=firstPredefined(term[0])){
        if(term[0].find("<<")>term[0].find(">>"))
            ABORT("for now, cannot have products of pre-defined operators, is: "+Def+" of "+inputCoor);

        // get the remainder
        string predef=firstPredefined(term[0]);
        vector<string>termX=terms(remainder(term[0],predef));

        // remove trailing coordinates for expanding predef
        string predefCoor=inputCoor;
        for(size_t posF=termX[0].find("<");posF!=string::npos;posF=termX[0].find("<",posF+1)){
            predefCoor=predefCoor.substr(0,predefCoor.rfind('.')-1);
        }
        if(predefCoor=="")ABORT("too few coordinates for expanding "+Def+" given "+inputCoor);

        string param=parameter(term[0],"+");
        vector<string>termP=terms(OperatorDefinition(predef.substr(2,predef.length()-4),predefCoor));
        if(predef.find(termP[0])==2)ABORT("failed to resolve "+predef+" for "+predefCoor+" derived from "+inputCoor);
        for(int k=0;k<termP.size();k++){
            string parP=parameter(termP[k],param);
            for(int l=0;l<termX.size();l++){
                def+=parameter(termX[l],parP)+compose(noParameter(termP[k]),noParameter(termX[l]));
            }
        }
    }

    // OpStrX
    else
        def=OperatorDefinitionNew(term[0],inputCoor);

    // drop terms that have factors !=<1> on certain coordinates
    //if(Drop)def=OperatorDefinitionNew(def,inputCoor).dropTerms(hierarchy);

    // on final level, permute terms for floor on bottom
    assign(def);

    if(inputCoor!=Index::coordinates(parentHier))
        assign(permuteFEM(parentHier));

}


/// return vector of terms, single blank term if empty
std::vector<std::string> OperatorDefinitionNew::terms(const std::string Def){
    if(Def=="")return vector<string>(1,"");
    std::string def(Def);
    return OperatorData::terms(def);
}

std::string OperatorDefinitionNew::noSign(std::string Term){
    if(Term.find_first_not_of(" ")==string::npos)return "";
    Term=Term.substr(Term.find_first_not_of(" "));
    if(Term.find_first_of("+-")!=0)return Term;
    return Term.substr(1);
}

/// extract parameter para from Term, if Param!="" return product with proper sign
std::string OperatorDefinitionNew::parameter(std::string Term, std::string Param){
    string par=Term.substr(0,Term.find_first_of("<("));

    // sanity checks
    if (Term.find_first_not_of("( <",par.length())<Term.find("<",par.length()))
        ABORT("Illegal operator term "+Term
              +":\nParameter must be followed by first operator factor,\nTime-dependent factors cannot be in algebraic expression");

    if (par.find("[t]")!=string::npos and (par.find("[t]")+3<par.length() or par.find_first_of("*/")!=string::npos))
        ABORT("Illegal operator term "+Term
              +":\nTime-dependent factors cannot be in algebraic expression - put constant factors with the operator, e.g.  <d_1/137_d>");

    string sig=sign(par,sign(Param,"+"));
    string par0=noSign(par),Param0=noSign(Param);
    if(par0=="" or Param0=="")
        return   sig+Param0+par0;
    else
        return par="("+sig+Param0+"*"+par0+")";
}

/// remove sign from Par and return sign (no sign is returns +)
std::string OperatorDefinitionNew::sign(std::string Par, const std::string OtherSign){
    if(OtherSign!="+" and OtherSign!="-")ABORT("OtherSign must be + or -, is \""+OtherSign+"\"");

    if(Par.find_first_not_of(" ")==string::npos)return OtherSign;
    Par=Par.substr(Par.find_first_not_of(" "));
    string s="+";
    if(Par.find_first_of("+-")==0)s=Par.substr(0,1);
    if(OtherSign==s)return "+";
    return "-";
}

std::string OperatorDefinitionNew::remainder(std::string Term,std::string First){
    // remainder starts right after First (ignore $-constraint in First)
    string first=First.substr(0,First.find("$"));
    string rem=Term.substr(Term.find(first)+first.length());
    if(rem.find(">")<rem.find("$"))return rem; // actual remainder, not just constraint
    return string();
}

std::string OperatorDefinitionNew::noParameter(std::string Term){
    // noParameter starts at first of (,<,$,[[
    size_t r=min(Term.find_first_of("(<$"),Term.find("[["));
    if(r==string::npos)return ""; // no remainder
    return Term.substr(r);
}

std::string OperatorDefinitionNew::firstFactor(std::string Term){
    // the first factor is the first "<factorDev>" string
    // that is not preceded by >,]], or unclosed (

    string fac=Term.substr(0,Term.find(">",Term.find_first_of("<"))+1);
    if(fac=="")return ""; // no factor at all
    if(fac.find("<<")!=string::npos)return "";      // first is <<predefined>>
    if(fac.find("<")==string::npos)ABORT("bad factor "+fac+" in "+Term);
    if(     std::count(fac.begin(),fac.begin()+fac.find("<"),'(')!=
            std::count(fac.begin(),fac.begin()+fac.find("<"),')'))return ""; // is inside brackets
    if(fac.find("]]")!=string::npos)return "";  // preceeded by special operator

    // remove possible parameters
    fac=fac.substr(fac.find("<"));

    // include the first constraint (if any)
    if(fac!="" and Term.find("$")!=string::npos){
        string con=Term.substr(Term.find("$"));
        fac=fac+con.substr(0,con.find(".",con.find(".")+1));
    }
    return fac;
}

std::string OperatorDefinitionNew::firstPredefined(std::string Term){
    // the first factor is the first "<<predefined>>" string
    // that is not preceded by >,]], or unclosed (

    if(Term.find("<")==string::npos)ABORT("cannot find operator in "+Term);
    if(Term.find("<<")==string::npos)return "";
    string fac=Term.substr(0,Term.find("<<"));
    if(     std::count(fac.begin(),fac.end(),'(')!=
            std::count(fac.begin(),fac.end(),')'))return ""; // is inside brackets
    if(fac.find("]]")!=string::npos or fac.find(">")!=string::npos)return "";
    fac=Term.substr(Term.find("<<"));
    return fac.substr(0,Term.find(">>")+2);
}

std::string OperatorDefinitionNew::dropTerms(string Hierarchy) const{
    if(*this=="")return *this; // nothing to be dropped
    vector<string>term=terms(*this);
    OperatorDefinition::dropTerms(term,Index::coordinates(Hierarchy));
    string s;
    for(int k=0;k<term.size();k++)s+=term[k];
    return s;
}

std::string OperatorDefinitionNew::inputCoordinates(string Hierarchy){
    // remove the LOWER of any duplicate coordinate (i.e. remove the levels in the floor)
    vector<string> iH=tools::splitString(Hierarchy,'.');

    string s;
    for(int k=0;k<iH.size();k++){
        std::string thisAx=iH[k];
        if(thisAx[0]=='k')thisAx="spec"+thisAx.substr(1); // treat spec/k pair as FE
        if(std::find(iH.begin(),iH.begin()+k,thisAx)==(iH.begin()+k))s+="."+iH[k];
    }
    return s.substr(1); // remove leading "."

}

std::string OperatorDefinitionNew::permuteFEM(string Hierarchy){
    vector<string>term=terms(*this);
    string s;
    for(int k=0;k<term.size();k++){
        s+=OperatorDefinition(term[k]).axisToCoor(Hierarchy);
    }
    return s;
}

std::string OperatorDefinitionNew::permute(string Term, string InCoor, string OutCoor){
    if(InCoor==OutCoor)return *this;

    vector<string>inp=tools::splitString(InCoor,'.');
    vector<string>out=tools::splitString(OutCoor,'.');
    if(inp.size()!=out.size())ABORT("numbers of coordinates differ: "+InCoor+" --- "+OutCoor);
    vector<unsigned int>perm;
    for(int k=0;k<inp.size();k++)
        perm.push_back(std::find(inp.begin(),inp.end(),out[k])-inp.begin());
    if(*std::max_element(perm.begin(),perm.end())==inp.size())
        ABORT("coordinates not related by permutation: "+InCoor+" --- "+OutCoor);
    return opPermuted(Term,perm);
}

std::string OperatorDefinitionNew::compose(string Factor, string Term){
    if(Term=="" or Factor=="")return Factor+Term;
    if(Term.find("$")!=string::npos){
        // no explict constraint on factor, assume none
        if(Factor.find("$")==string::npos)
            Term.insert(Term.find(".",Term.find("$")),".*");
        else {
            // move factor constraint to overall
            Term.insert(Term.find(".",Term.find("$")),Factor.substr(Factor.find(".",Factor.find("$"))));
            Factor=Factor.substr(Factor.find("$"));
        }
    }
    else if(Factor.find("$")!=string::npos)
        ABORT(Str("if factor has a constraint, term must as well, found:")+Factor+Term);

    return Factor+Term;
}
