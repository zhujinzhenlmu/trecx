#include "coulXFlux.h"

#include "readInput.h"
#include "discretizationCoulXNew.h"
#include "basicDisc.h"
#include "discretizationSurface.h"
#include "discretizationtsurffspectra.h"
#include "timePropagator.h"
#include "timePropagatorOutput.h"
#include "printOutput.h"
#include "coulXDerNew.h"
#include "operatorDefinitionNew.h"
#include "surfaceConstraint.h"
#include "derivativeFlat.h"
#include "plot.h"
#include "basisGrid.h"

double CoulXFlux::_currentTime=DBL_MAX;

void CoulXFlux::integrateSource(double Time){
    if(_currentTime==DBL_MAX){
        intFlux=new Coefficients(_surfaceDisc->idx(),0.);
        _currentTime=Time;
        return;
    }
    // primitive integration
    double step=(Time-_currentTime)/std::max(int(1),int((Time-_currentTime)/0.01));
    for(double t=_currentTime;t<Time;t+=step){
        _inputFlux->update(t,true);
        SurfaceConstraint * f=dynamic_cast<SurfaceConstraint*>(_constr->firstLeaf());
        _constr->update(t);
        for(Coefficients* iC=intFlux->firstLeaf();iC!=0;iC=iC->nextLeaf()){
            for(int k=0;k<iC->size();k++)
                iC->floorData()[k]+=step*dynamic_cast<SurfaceConstraint::Floor*>(f->floor())->_derF[k];
            f=dynamic_cast<SurfaceConstraint*>(f->nextLeaf());
        }
    }
    _currentTime=Time;
}


CoulXFlux::CoulXFlux(ReadInput &Inp, DiscretizationSurface* SurfD, std::string Region, int NSurf)
{
    double rC,rX;
    rC=SurfD->idx()->axisIndex("ValDerRn")->basisGrid()->mesh()[0];
    ReadInput::main.read("CoulombX","radius",rX,tools::str(rC),"Coulomb extender radius",0,"coulX");

    if(rX<=rC)ABORT(Str("-coulX=")+rX+"<= Rc="+rC);

    _inputFlux=new SurfaceFlux(Inp,SurfD,Region,NSurf);

    // for now: CoulX k-grid same as for spectra
    int nRadial=0;
    double minE=0.,maxE=0.;
    bool kGrid=true;
    std::vector<double> momenta;
    DiscretizationTsurffSpectra::readFlags(Inp,nRadial,minE,maxE,kGrid,true);
    momenta.resize(nRadial);
    DiscretizationTsurffSpectra::computeMomenta(minE,maxE,momenta,kGrid);

    // in principle, any valid time-propagation 0 to rC can be put here
    // this is useful for debugging, where we may simply use a standard discretization
    // which should give valid results
    // the BesselCoulomb is meant to be particularly efficient for the purpose
    // in this way, the particular discretization and the extension procedure can be tested
    // independently
    //
    // we can try to solve the truncated Coulomb problem using, alternatively,
    // a standard discretization and the BesselCoulomb
    if(ReadInput::main.flag("besselCoulomb","use BesselCoulomb discretization"))
        _disc=new DiscretizationCoulXNew(SurfD,rC,rX,momenta,false);
    else {
        // extend the original discretization to Rx
        std::vector<Axis>xAx=SurfD->parent->getAxis();
        SurfD->parent->print("","parent");
        for(int k=0;k<xAx.size();k++)
            if(xAx[k].name=="Rn"){
                xAx[k].extendBox({0.,rX});
                xAx[k].truncateBox({0.,rC});
            }
        _disc=new BasicDisc(xAx);
    }

    _disc->print("","COULOMB EXTENDER DISCRETIZATION");
    // (require current exactly on element boundary)
    // we need to enforce current at the lower boundary of the element where it enters
    _surfRc=new DiscretizationSurface(_disc,std::vector<double>(1,rC*(1.+1.e-12)),0);
    double rcx;
    ReadInput::main.read("DEBUG","rcx",rcx,tools::str(rX),"pick up exended flux from here",1,"DEBUGrcx");
    _surfaceDisc=new DiscretizationSurface(_disc,std::vector<double>(1,rcx),0);

    _surfRc->print("","Rc - input surface");
    discSurf()->print("","Rx - output surface");

    // for now, angular discretizations of original and extender must agree
    if(not SurfD->idx()->treeEquivalent(_surfaceDisc->idx(),{"Phi","Eta"}))
        ABORT("extender angular discretization\n"+_surfaceDisc->idx()->str()+"\ndoes not match original\n"+SurfD->idx()->str());

    // we probably want a different TimePropagatorOutput (probably will be new class)
    double tBeg,tEnd,tPrint,tStore,accuracy,cutE,fixStep,applyThreshold;
    std::string propMethod;
    Units::setDefault("au"); // general default units
    TimePropagator::read(Inp,tBeg,tEnd,tPrint,tStore,accuracy,cutE,fixStep,applyThreshold,propMethod);
    OperatorTree *opCoulX = new OperatorTree("opCoulX",OperatorDefinitionNew("iLaserA0[t]<<D/DZ>>+0.5<<Laplacian>>-<<Coulomb>>",_disc->idx()->hierarchy()),
                                             _disc->idx(),_disc->idx());
    std::vector<OperatorAbstract*> printOps;
    // for debugging only - eventually remove print output or choose different format
    printOps.push_back(const_cast<OperatorAbstract*>(opCoulX->iIndex->overlap()));
    printOps.push_back(const_cast<OperatorTree*>(opCoulX));
    opCoulX->iIndex->testInverseOverlap();

    const DiscretizationSurface::Map* fromSurf=dynamic_cast<const DiscretizationSurface::Map*>(_surfRc->mapFromParent());
    DerivativeFlat* der= new DerivativeFlat(opCoulX,0.); //HACK - only to generate index ownership
    SurfaceConstraint * cons0=new SurfaceConstraint(_inputFlux,fromSurf);
    _constr=cons0;
    CoulXDerNew* coulXDer = new CoulXDerNew(opCoulX,cons0);

    OdeStep<LinSpaceMap<Coefficients>,Coefficients> *ode = new OdeRK4<LinSpaceMap<Coefficients>,Coefficients>(coulXDer);
    fixStep=0.0;
    ReadInput::main.read("DEBUG","accuracy",accuracy,tools::str(accuracy),"pick up exended flux from here",1,"acc");
    _prop = new TimePropagator(ode,new TimePropagatorOutput(printOps,"",DBL_MAX),accuracy,fixStep);


    // for debug
    _plot=std::shared_ptr<Plot>(new Plot(_disc->idx(),{"Rn"},{"g"},{401},{{0.,40.}}));
    _plotInterval=Units::convert(0.5,"OptCyc");
    _previousPlot=-_plotInterval;
    _plotFile=Inp.outputTopDir();
}


bool CoulXFlux::update(double Time, bool Derivative){
    if(_bWf.coefs==0){
        _bWf.coefs=new Coefficients(_disc->idx(),0.); // initial vector is zero - will be fed from source
        _bWf.time=Time;
        coefs=new Coefficients(_surfaceDisc->idx(),0.);
        time=Time;

        // create plots of source
        int bwf;
        ReadInput::main.read("DEBUG","bwf",bwf,"0","pick up exended flux from here",1,"DEBUGbwf");
        for(int k=1;k<=bwf;k++){
            _prop->propagate(&_bWf,k*_plotInterval); // solve differential equation for "\vec{b}"
            _plot->plot(*_bWf.coefs,_plotFile+"bwf",{"at time"},tools::str(_bWf.time/Units::convert(1.,"OptCyc"),2),true);
        }
        _bWf.coefs->setToZero();
        _bWf.time=Time;
    }
    _prop->propagate(&_bWf,Time); // solve differential equation for "\vec{b}"
    _surfaceDisc->mapFromParent()->apply(1.,*_bWf.coefs,0.,*coefs); // Rx surface flux into SurfaceFlux::coefs

    return true;
}
