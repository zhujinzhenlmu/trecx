// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "ci.h"
#include "time.h"
#include <cstdlib>
#include <set>

#include "multiArrayWrapper.h"
#include "mpiWrapper.h"
#include "printOutput.h"


using namespace std;
#include "eigenNames.h"


//ci::ci(mo &L_Orb, int state){
//    //reads the determinant list given by columbus and stores the determinants and coefficients in the coef vector and det vector

//    this->total_states = 1;
//    vector<vector<int > > determinants;
//    VectorXcd temp_coef;
//    L_Orb.col_data->read_determinants(determinants,temp_coef,state);
//    coef.push_back(temp_coef);

//    for(unsigned int i=0; i<determinants.size(); i++)
//        det.push_back(sod(determinants[i],L_Orb));

//}

//ci::ci(mo &L_Orb){
//    //reads the determinant list given by columbus and stores the determinants and coefficients in the coef vector and det vector - for all states found in columbus calculation directory

//    vector<vector<int > > determinants;
//    L_Orb.col_data->read_determinants_allstates(determinants,coef);

//    this->total_states = coef.size();

//    for(unsigned int i=0; i<determinants.size(); i++)
//        det.push_back(sod(determinants[i],L_Orb));

//}

ci::ci(columbus_data*ColuData,mo &L_Orb, int no_states, int start_state)
{
    vector<vector<int > > determinants;
    //    L_Orb.col_data->read_determinants_allstates(determinants,coef);
    ColuData->read_determinants_allstates(determinants,coef);

    if(start_state+no_states>coef.size()) ABORT("CI calculation does not have "+tools::str(no_states)+" states");
    vector<VectorXcd> newCoef = coef;
    coef.clear();

    for(int k=start_state;k<no_states;k++) coef.push_back(newCoef[k]);
    this->total_states = coef.size();

    for(unsigned int i=0; i<determinants.size(); i++)
        det.push_back(sod(determinants[i],L_Orb));
}

ci::ci(QuantumChemicalInput & ChemInp, mo &L_Orb, int no_states, int start_state)
{
    if(start_state!=0)ABORT("new constructor only for start_state=0");
    if(start_state+no_states>ChemInp.ciCoef.size()) ABORT("CI calculation does not have "+tools::str(no_states)+" states");
    coef=ChemInp.ciCoef;
    coef.resize(no_states);
    det=ChemInp.dets();
    total_states=coef.size();

    if(ChemInp.det_cutoff == 0 || det.size() < ChemInp.det_cutoff) return;

    // NOTE: this basis truncation is for debugging purposes - it prefers determinants in the low-lying channels
    // also, if refactored, this truncation code _only_ works _before_ the spin multiplets have been added to the basis,
    // otherwise, a valid multiplet basis is truncated in an unpredictable manner

    if(ChemInp.det_threshold < 0.) ABORT("negative determinant coefficient threshold");
    const std::size_t det_cutoff = ChemInp.det_cutoff; // we have at most det_cutoff determinants with coefs > coef_threshold
    decltype(coef) new_coef(coef.size(), Eigen::VectorXcd::Zero(no_states * det_cutoff)); // the new basis representation
    decltype(det) new_det;
    std::set<std::size_t> considered; // lookup structure to prevent multiple considerations of same determinants
    std::size_t n = 0;
    for(; n < no_states; ++n) { // prefer lower channels in determinant truncation
        std::size_t dets_taken = 0; // determinants taken for this CI state
        // sort the coefficients vector of this state in descending order
        std::vector<std::size_t> permutation(coef[n].rows()); // contains the indices of the determinants in descending
                                                              // relevance order
        for(std::size_t nn = 0; nn < permutation.size(); ++nn) permutation[nn] = nn;
        std::sort(begin(permutation), end(permutation), [this, n](std::size_t lhs, std::size_t rhs) {
            return std::abs(coef[n](lhs)) > std::abs(coef[n](rhs)); // descending order
        });
        for(std::size_t i = 0; i < permutation.size(); ++i) { // iterate over dets, beginning with most relevant
            if(dets_taken == det_cutoff) break;
            if(std::abs(coef[n](permutation[i])) > ChemInp.det_threshold) { // determinant is relevant
                auto result = considered.insert(permutation[i]); // result.second == false if already inserted
                if(result.second) { // determinant has not been considered yet
                    new_det.push_back(det[permutation[i]]); // re-label the perm(i)th determinant to i
                    new_coef[n](dets_taken) = coef[n](permutation[i]);
                    ++dets_taken;
                }
            }
        }
    }

    // shrink the coefficient vectors if less determinants than requested have coefficients above the threshold
    // (other code requires that det.size() == coef[n].rows())
    if(new_det.size() < no_states * det_cutoff) {
        for(std::size_t n = 0; n < no_states; ++n) new_coef[n].conservativeResize(new_det.size());
    }
    // re-normalize CI coefs
    for(std::size_t n = 0; n < no_states; ++n) {
        double norm = std::sqrt(std::real((new_coef[n].array() * new_coef[n].array()).sum()));
        new_coef[n] /= norm;
    }

    coef = std::move(new_coef);
    det = std::move(new_det);
    PrintOutput::DEVmessage(Str("Truncated determinants: working with ") + considered.size() + " relevant (> "
                            + ChemInp.det_threshold + ") determinant(s) from the lowest " + n + " channel(s).");

}

void ci::add_multiplets(int Mult)
{   
    mo *MO = dynamic_cast<mo*>(det[0].Orb);
    int mult=Mult;

    if(mult==1)   return;
    else if(mult==2){

        int no_orig_det = this->det.size();
        for(int i=0;i<no_orig_det;i++){
            vector<int > determinant;
            det[i].convert_to_combined_format(det[i].spac,det[i].spin,determinant);
            for(unsigned int d=0;d<this->det[i].spac.size();d++)

                //             if(std::find(det[i].spac.begin(),det[i].spac.begin()+d,det[i].spac[d])!=det[i].spac.begin()+d  or
                //                std::find(det[i].spac.begin()+d+1,det[i].spac.end(),det[i].spac[d])!=det[i].spac.end())
                //                determinant[d]*=1;           //if it occurs in pair do not swap the spin
                //             else
                determinant[d]*=-1;           // swap the spin to construct the other doublet

            this->det.push_back(sod(determinant,*MO));
            //           cout<<this->det[this->det.size()-1]<<endl;
        }
        vector<VectorXcd> new_coef;
        for(int i=0;i<total_states;i++){
            VectorXcd c = VectorXcd::Zero(2*this->coef[0].size());
            c.segment(0,this->coef[0].size()) = this->coef[i];
            new_coef.push_back(c);
            VectorXcd d = VectorXcd::Zero(2*this->coef[0].size());
            if(((this->det[i].spac.size()-1)/2)%2 == 1)
                d.segment(this->coef[0].size(),this->coef[0].size()) = -this->coef[i];
            else
                d.segment(this->coef[0].size(),this->coef[0].size()) = this->coef[i];
            new_coef.push_back(d);
        }
        this->total_states*=2;
        this->coef.clear();
        for(int i=0;i<total_states;i++)
            this->coef.push_back(new_coef[i]);

    }
    else ABORT("ci::add_multiplets():  Not yet defined for this multiplicity");
}

complex<double > ci::ci_matel(string op, ci* sj){

    if(sj==NULL)
        sj=this;

    if(this->total_states!=1 or sj->total_states!=1){
        cout<<"Function not implemented for this case"<<endl;
        exit(1);
    }

    complex<double > mat=0;


    int rank = 0;//MPI::COMM_WORLD.Get_rank(); //MPIwrapper::Rank();
    int size = 1;//MPI::COMM_WORLD.Get_size();
#ifndef _NOMPI_
    rank = MPIwrapper::Rank(); //MPIwrapper::Rank();
    size = MPIwrapper::Size();
#endif

    if (rank==0){
        int start_i = (int)(rank*this->det.size()/size);
        int end_i = (int)((rank+1)*this->det.size()/size);
        for(int i=start_i; i < end_i; i++)
            for(unsigned int j=0; j<sj->det.size(); j++){
                mat += this->det[i].sod_matel(op,sj->det[j])*this->coef[0](i)*sj->coef[0](j);
            }
        for(int i=1; i<size; i++){
            complex<double > value;
#ifndef _NOMPI_
            MPI_Status status;
            MPI_Recv(&value,1,MPI_DOUBLE_COMPLEX,i,1,MPIwrapper::communicator(),&status);
#endif
            mat+=value;
        }
        return mat;
    }
    else{
        int start_i = (int)(rank*this->det.size()/size);
        int end_i = (int)((rank+1)*this->det.size()/size);
        for(int i=start_i; i<end_i; i++)
            for(unsigned int j=0; j<sj->det.size(); j++)
                mat += this->det[i].sod_matel(op,sj->det[j])*this->coef[0](i)*sj->coef[0](j);
#ifndef _NOMPI_
        MPI_Send(&mat,1,MPI_DOUBLE_COMPLEX,0,1,MPIwrapper::communicator());
#endif
    }

    return mat;
}

void ci::ci_dyson(ci* sj, int state1, int state2){

    VectorXcd dyson_coef = VectorXcd::Zero(this->det[0].Orb->NumberOfOrbitals());

    int rank = 0;
    int size = 1;
#ifndef _NOMPI_
    rank = MPIwrapper::Rank();
    size = MPIwrapper::Size();
#endif
    if (rank==0){
        int start_i = (int)(rank*this->det.size()/size);
        int end_i = (int)((rank+1)*this->det.size()/size);
        for(int i=start_i; i < end_i; i++)
            for(unsigned int j=0; j<sj->det.size(); j++){
                int pos,sign;
                bool res;
                this->det[i].sod_dyson(sj->det[j], res, pos, sign);
                if (res){
                    if (sign>0)
                        dyson_coef(pos) += this->coef[state1](i)*sj->coef[state2](j);
                    else
                        dyson_coef(pos) -= this->coef[state1](i)*sj->coef[state2](j);
                }
            }
        for(int i=1; i<size; i++){
            VectorXcd value = VectorXcd::Zero(this->det[0].Orb->NumberOfOrbitals());
#ifndef _NOMPI_
            MPI_Status status;
            MPI_Recv(value.data(),value.size(),MPI_DOUBLE_COMPLEX,i,1,MPIwrapper::communicator(),&status);
#endif
            dyson_coef+=value;
        }
        double dyson_norm=0;
        cout<<"Dyson Orbital Coefficients in MO basis:  \n";
        for(int i=0;i<dyson_coef.size();i++){
            cout<<real(dyson_coef(i))<<endl;
            dyson_norm+=real(dyson_coef(i))*real(dyson_coef(i));
        }
        cout<<endl;
        //cout<<"Dyson Orbital Coefficients in AO basis:  \n";
        //VectorXcd dyson_ao = this->det[0].Orb->S.coef.transpose() * this->det[0].Orb->coef.transpose() * dyson_coef;
        //for(int i=0;i<dyson_ao.size();i++){
        //   cout<<real(dyson_ao(i))<<endl;
        //}
        cout<<"Norm square of dyson orbital :"<<dyson_norm<<endl;
    }
    else{
        int start_i = (int)(rank*this->det.size()/size);
        int end_i = (int)((rank+1)*this->det.size()/size);
        for(int i=start_i; i<end_i; i++)
            for(unsigned int j=0; j<sj->det.size(); j++){
                int pos,sign;
                bool res;
                this->det[i].sod_dyson(sj->det[j], res, pos, sign);
                if (res){
                    if (sign>0)
                        dyson_coef(pos) += this->coef[state1](i)*sj->coef[state2](j);
                    else
                        dyson_coef(pos) -= this->coef[state1](i)*sj->coef[state2](j);
                }

            }
#ifndef _NOMPI_
        MPI_Send(dyson_coef.data(),dyson_coef.size(),MPI_DOUBLE_COMPLEX,0,1,MPIwrapper::communicator());
#endif
    }

}

void ci::ci_dens1(MatrixXcd &res, ci *sj, int state1, int state2)
{
    res = MatrixXcd::Zero(this->det[0].Orb->NumberOfOrbitals(),this->det[0].Orb->NumberOfOrbitals());

    if(sj==NULL)
        sj=this;

    for(unsigned int i=0;i<this->det.size(); i++)
        for(unsigned int j=0;j<sj->det.size(); j++){
            bool r; vector<VectorXi > pos; vector<int > sign;
            this->det[i].sod_dens_n(1,sj->det[j],r,pos,sign);
            if(r){
                for(unsigned int k=0;k<pos.size();k++){
                    //cout<<pos[k].transpose()<<"  "<<sign[k]<<"  "<<this->coef[state1](i)*sj->coef[state2](j)<<endl;
                    if(sign[k]>0)
                        res(pos[k](0),pos[k](1))+=this->coef[state1](i)*sj->coef[state2](j);
                                else
                                res(pos[k](0),pos[k](1))-=this->coef[state1](i)*sj->coef[state2](j);
                }
            }
        }
    //cout<<endl<<endl;

}

void ci::ci_dens2(My4dArray &res, ci *sj, int state1, int state2)
{
    res.resize(det[0].Orb->NumberOfOrbitals(),det[0].Orb->NumberOfOrbitals(),det[0].Orb->NumberOfOrbitals(),det[0].Orb->NumberOfOrbitals());

    if(sj==NULL)
        sj=this;

    //complex<double > mat=0;
    for(unsigned int i=0;i<this->det.size(); i++)
        for(unsigned int j=0;j<sj->det.size(); j++){
            bool r; vector<VectorXi > pos; vector<int > sign;
            this->det[i].sod_dens_n(2,sj->det[j],r,pos,sign);
            if(r){
                for(unsigned int k=0;k<pos.size();k++){
                    complex<double > t1=(this->coef[state1](i)*sj->coef[state2](j));
                    if(abs(imag(t1))>1e-14){
                        cout<<"It should be real.... ci::ci_dens2\n";
                        exit(1);
                    }
                    double t = real(t1);
                    if(sign[k]>0)
                        res.addto(pos[k](0),pos[k](1),pos[k](2),pos[k](3),t);
                                else
                                res.addto(pos[k](0),pos[k](1),pos[k](2),pos[k](3),-t);
                                //cout<<res.val_at(0,1,0,1)<<"   "<<pos[k].transpose()<<"  "<<i<<"  "<<j<<endl;
                                //mat+=sign[k]*t*det[0].Orb->col_data->vee.val_at(pos[k](0,0),pos[k](1,0),pos[k](2,0),pos[k](3,0));
                }
            }
            //cout<<real(mat)<<endl;
        }

}

void ci::ci_dens3_X_vee(MatrixXcd &res, ci *sj, int state1, int state2)
{
    res = MatrixXcd::Zero(this->det[0].Orb->NumberOfOrbitals(),this->det[0].Orb->NumberOfOrbitals());

    if(sj==NULL)
        sj=this;

    My4dArray vee;
    det[0].Orb->matrix_2e(vee);

    for(unsigned int i=0;i<this->det.size(); i++)
        for(unsigned int j=0;j<sj->det.size(); j++){
            bool r; vector<Matrix<int,6,1> > pos; vector<int > sign;
            this->det[i].sod_dens3(sj->det[j],r,pos,sign);
            if(r){
                for(unsigned int k=0;k<pos.size();k++){
                    complex<double > t1=(this->coef[state1](i)*sj->coef[state2](j));
                    if(sign[k]>0)
                        res(pos[k](0),pos[k](1))+=t1*vee.val_at(pos[k](2),pos[k](3),pos[k](4),pos[k](5));
                                else
                                res(pos[k](0),pos[k](1))-=t1*vee.val_at(pos[k](2),pos[k](3),pos[k](4),pos[k](5));
                }
            }
        }
}

/*void ci::ci_spindens1(vector<vector<MatrixXcd > >&res, ci *sj, int state1, int state2)
{
  if(sj==NULL)
    sj=this;
  else {ABORT("case not implemented - ci::ci_spindens1");}

  if(state1==-1 and state2==-1){
      res.resize(total_states);
      for(int i=0;i<this->total_states;i++)
        res[i].resize(sj->total_states);
    }
  else {ABORT("Impossible case encountered - ci::ci_spindens1");}

  for(unsigned int i=0;i<res.size();i++)
    for(unsigned int j=0;j<res[i].size();j++)
      res[i][j] = MatrixXcd::Zero(2*this->det[0].Orb->NumberOfOrbitals(),2*this->det[0].Orb->NumberOfOrbitals());

  for(unsigned int i=0;i<this->det.size(); i++)
    for(unsigned int j=0;j<sj->det.size(); j++){

        MatrixXcd c1xc2 = MatrixXcd::Zero(this->total_states,sj->total_states);
        for(int c1=0;c1<this->total_states;c1++)
          for(int c2=0;c2<sj->total_states;c2++)
            c1xc2(c1,c2) = this->coef[c1](i)*sj->coef[c2](j);

        if(c1xc2.isZero(1e-14)) continue;
        bool r; vector<VectorXi > pos; vector<int > sign;
        this->det[i].sod_spindens_upto_n(1,sj->det[j],r,pos,sign);
        if(r){
            for(unsigned int k=0;k<pos.size();k++){
                //cout<<pos[k].transpose()<<"  "<<sign[k]<<"  "<<this->coef[state1](i)*sj->coef[state2](j)<<endl;
                for(int kk=0;kk<pos[k].size();kk++){
                  if(pos[k](kk)<0) pos[k](kk) = abs(pos[k](kk)) - 1;
                  else if(pos[k](kk)>0) pos[k](kk) = this->det[0].Orb->NumberOfOrbitals() + abs(pos[k](kk)) - 1;
                  else {ABORT("Invalid entry in ci::ci_spindens1");}
                  }
                //cout<<pos[k].transpose()<<"  "<<sign[k]<<"  "<<this->coef[state1](i)*sj->coef[state2](j)<<endl;
                for(int c1=0;c1<this->total_states;c1++)
                  for(int c2=0;c2<sj->total_states;c2++){
                      if(sign[k]>0)
                        res[c1][c2](pos[k](0),pos[k](1))+=c1xc2(c1,c2);
                      else
                        res[c1][c2](pos[k](0),pos[k](1))-=c1xc2(c1,c2);
                    }
              }
          }
    }

  //check that it is symmetric
  for(int c1=0;c1<this->total_states;c1++)
    for(int c2=0;c2<sj->total_states;c2++){
      if(c1==c2)
      for(int i=0;i<res[c1][c2].rows();i++)
        for(int j=0;j<res[c1][c2].cols();j++)
          if(abs(res[c1][c2](i,j)-res[c1][c2](j,i))>1e-14){
            cout<<"ci::ci_spindens1: Not symmetric  "<<i<<"  "<<j<<"  "<<abs(res[c1][c2](i,j)-res[c1][c2](j,i))<<endl;
            exit(1);
            }
      }
}

void ci::ci_spindens2(vector<vector<boost::multi_array<My4dArray, 4> > > &res, ci *sj, int state1, int state2)
{
  if(sj==NULL)
    sj=this;
  else {ABORT("case not implemented - ci::ci_spindens2");}

  if(state1==-1 and state2==-1){
      res.resize(total_states);
      for(int i=0;i<this->total_states;i++)
        res[i].resize(sj->total_states);

      for(int c1=0;c1<this->total_states;c1++)
        for(int c2=0;c2<sj->total_states;c2++){
            res[c1][c2].resize(boost::extents[2][2][2][2]);
            for(int i=0;i<2;i++)
                for(int j=0;j<2;j++)
                   for(int k=0;k<2;k++)
                      for(int l=0;l<2;l++)
                         res[c1][c2][i][j][k][l].resize(det[0].Orb->NumberOfOrbitals(),det[0].Orb->NumberOfOrbitals(),det[0].Orb->NumberOfOrbitals(),det[0].Orb->NumberOfOrbitals());
          }
    }
  else {ABORT("Impossible case encountered - ci::ci_spindens2");}

  for(unsigned int i=0;i<this->det.size(); i++)
    for(unsigned int j=0;j<sj->det.size(); j++){

        MatrixXcd c1xc2 = MatrixXcd::Zero(this->total_states,sj->total_states);
        for(int c1=0;c1<this->total_states;c1++)
          for(int c2=0;c2<sj->total_states;c2++)
            c1xc2(c1,c2) = this->coef[c1](i)*sj->coef[c2](j);

        if(c1xc2.isZero(1e-14)) continue;
        bool r; vector<VectorXi > pos; vector<int > sign;
        this->det[i].sod_spindens_upto_n(2,sj->det[j],r,pos,sign);
        if(r){
            for(unsigned int k=0;k<pos.size();k++){
                int spi,spj,spk,spl;
                //cout<<pos[k].transpose()<<endl;
                if(pos[k](0)<0) spi=0; else if(pos[k](0)>0) spi=1; else {ABORT("Invalid entry in ci::ci_spindens2");}
                if(pos[k](1)<0) spj=0; else if(pos[k](1)>0) spj=1; else {ABORT("Invalid entry in ci::ci_spindens2");}
                if(pos[k](2)<0) spk=0; else if(pos[k](2)>0) spk=1; else {ABORT("Invalid entry in ci::ci_spindens2");}
                if(pos[k](3)<0) spl=0; else if(pos[k](3)>0) spl=1; else {ABORT("Invalid entry in ci::ci_spindens2");}

                for(int kk=0;kk<pos[k].size();kk++){
                    if(pos[k](kk)!=0) pos[k](kk) = abs(pos[k](kk))-1;
                    else {ABORT("Invalid entry in ci::ci_spindens1");}
                  }

                for(int c1=0;c1<this->total_states;c1++)
                  for(int c2=0;c2<sj->total_states;c2++){
                      if(sign[k]>0)
                        res[c1][c2][spi][spj][spk][spl].addto(pos[k](0),pos[k](1),pos[k](2),pos[k](3),c1xc2(c1,c2));
                      else
                        res[c1][c2][spi][spj][spk][spl].addto(pos[k](0),pos[k](1),pos[k](2),pos[k](3),-c1xc2(c1,c2));
                    }
              }
          }
      }
  for(int c1=0;c1<this->total_states;c1++)
    for(int c2=0;c2<sj->total_states;c2++){
      for(int i=0;i<2;i++)
        for(int j=0;j<2;j++)
          for(int k=0;k<2;k++)
            for(int l=0;l<2;l++)
              res[c1][c2][i][j][k][l].remove_zero();
      }

}

void ci::ci_spindens3_X_vee(vector<vector<MatrixXcd> > &res, ci *sj, int state1, int state2)
{
  if(sj==NULL)
    sj=this;
  else {ABORT("case not implemented - ci::ci_spindens3_X_vee");}

  if(state1==-1 and state2==-1){
      res.resize(total_states);
      for(int i=0;i<this->total_states;i++)
        res[i].resize(sj->total_states);
    }
  else {ABORT("Impossible case encountered - ci::ci_spindens3_X_vee");}

  for(unsigned int i=0;i<res.size();i++)
    for(unsigned int j=0;j<res[i].size();j++)
      res[i][j] = MatrixXcd::Zero(2*this->det[0].Orb->NumberOfOrbitals(),2*this->det[0].Orb->NumberOfOrbitals());

  My4dArray vee;
  det[0].Orb->matrix_2e(vee);

  for(unsigned int i=0;i<this->det.size(); i++)
    for(unsigned int j=0;j<sj->det.size(); j++){

        MatrixXcd c1xc2 = MatrixXcd::Zero(this->total_states,sj->total_states);
        for(int c1=0;c1<this->total_states;c1++)
          for(int c2=0;c2<sj->total_states;c2++)
            c1xc2(c1,c2) = this->coef[c1](i)*sj->coef[c2](j);

        if(c1xc2.isZero(1e-14)) continue;
        bool r; vector<VectorXi > pos; vector<int > sign;
        this->det[i].sod_spindens_upto_n(3,sj->det[j],r,pos,sign);
        if(r){
            for(unsigned int k=0;k<pos.size();k++){

                if(pos[k](2)/abs(pos[k](2)) != pos[k](3)/abs(pos[k](3))) continue;
                if(pos[k](4)/abs(pos[k](4)) != pos[k](5)/abs(pos[k](5))) continue;

                for(int kk=0;kk<2;kk++){
                  if(pos[k](kk)<0) pos[k](kk) = abs(pos[k](kk)) - 1;
                  else if(pos[k](kk)>0) pos[k](kk) = this->det[0].Orb->NumberOfOrbitals() + abs(pos[k](kk)) - 1;
                  else {ABORT("Invalid entry in ci::ci_spindens3");}
                  }
                for(int kk=2;kk<6;kk++){
                  if(pos[k](kk)!=0) pos[k](kk) = abs(pos[k](kk)) - 1;
                  else {ABORT("Invalid entry in ci::ci_spindens3");}
                  }

                for(int c1=0;c1<this->total_states;c1++)
                  for(int c2=0;c2<sj->total_states;c2++){
                    if(sign[k]>0)
                        res[c1][c2](pos[k](0),pos[k](1))+=c1xc2(c1,c2)*vee.val_at(pos[k](2),pos[k](3),pos[k](4),pos[k](5));
                    else
                        res[c1][c2](pos[k](0),pos[k](1))-=c1xc2(c1,c2)*vee.val_at(pos[k](2),pos[k](3),pos[k](4),pos[k](5));
                    }
              }
          }
      }
  //cout<<res<<endl<<endl<<mo_shelf->rho1[state1][state2]<<endl<<endl;
  //cin.get();

  //check that it is symmetric
  for(int c1=0;c1<this->total_states;c1++)
    for(int c2=0;c2<sj->total_states;c2++){
      if(c1==c2)
      for(int i=0;i<res[c1][c2].rows();i++)
        for(int j=0;j<res[c1][c2].cols();j++)
          if(abs(res[c1][c2](i,j)-res[c1][c2](j,i))>1e-10){
            cout<<"ci::ci_spindens3_X_vee: Not symmetric  "<<i<<"  "<<j<<"  "<<abs(res[c1][c2](i,j)-res[c1][c2](j,i))<<endl;
            exit(1);
            }
      }
}
*/
void ci::ci_setup_all_dens(ci *sj)
{
    ABORT("You might want to call  ci::ci_setup_all_spindens instead");
    /*if(sj==NULL)
    sj=this;

  mo_shelf->rho1.resize(this->total_states);
  mo_shelf->rho2.resize(this->total_states);
  mo_shelf->rho3_vee.resize(this->total_states);
  for(int i=0;i<this->total_states;i++){
      mo_shelf->rho1[i].resize(sj->total_states);
      mo_shelf->rho2[i].resize(sj->total_states);
      mo_shelf->rho3_vee[i].resize(sj->total_states);
    for(int j=0;j<sj->total_states;j++){
        //ci_dens1(mo_shelf->rho1[i][j],this,i,j);
        ci_spindens1(mo_shelf->rho1[i][j],this,i,j);
        ci_dens2(mo_shelf->rho2[i][j],this,i,j);
        ci_dens3_X_vee(mo_shelf->rho3_vee[i][j],this,i,j);
      }
    }*/

}

void ci::ci_setup_all_spindens(My4dArray_shared* Vee,
                               vector<vector<MatrixXd> > &rho1, vector<vector<My4dArray_shared* > > &rho2, vector<vector<MatrixXd> > &rho3_vee, vector<double> lc_fac, vector<int > spin, MatrixXd* ene_2e, ci *sj)
{
    if(sj==NULL)
        sj=this;

    int mult_ion=lc_fac.size();  // if 0 don't compute rho2
    if(mult_ion==0) mult_ion=1;

    if(ene_2e!=NULL) *ene_2e = MatrixXd::Zero(total_states/mult_ion,sj->total_states/mult_ion);

    rho1.resize(total_states);
    if(lc_fac.size()!=0) rho2.resize(total_states/mult_ion);
    rho3_vee.resize(total_states);
    for(int i=0;i<this->total_states;i++){
        rho1[i].resize(sj->total_states);
        rho3_vee[i].resize(sj->total_states);
    }

    for(int c1=0;c1<this->total_states;c1++)
        for(int c2=0;c2<sj->total_states;c2++){
            rho1[c1][c2] = MatrixXd::Zero(2*this->det[0].Orb->NumberOfOrbitals(),2*this->det[0].Orb->NumberOfOrbitals());
            rho3_vee[c1][c2] = MatrixXd::Zero(2*this->det[0].Orb->NumberOfOrbitals(),2*this->det[0].Orb->NumberOfOrbitals());

            //           if(static_cast<mo*>(this->det[0].Orb)->col_data->no_electrons>1 and c1<total_states/mult_ion and c1>=c2 and lc_fac.size()!=0)
            if(nElectrons()>1 and c1<total_states/mult_ion and c1>=c2 and lc_fac.size()!=0)
                rho2[c1].push_back(new My4dArray_shared(det[0].Orb->NumberOfOrbitals()));
        }

    int i_slice = floor(double(this->det.size())/double(MPIwrapper::Size()));
    clock_t tot = 0;

    int i_start = MPIwrapper::Rank()*i_slice;
    int i_end = (MPIwrapper::Rank()+1)*i_slice;
    if(MPIwrapper::Rank()==MPIwrapper::Size()-1)  i_end=this->det.size();
    for(int i=i_start;i<i_end; i++)
        for(unsigned int j=0;j<sj->det.size(); j++){

            MatrixXd c1xc2 = MatrixXd::Zero(this->total_states,sj->total_states);
            for(int c1=0;c1<this->total_states;c1++)
                for(int c2=0;c2<sj->total_states;c2++){
                    c1xc2(c1,c2) = real(this->coef[c1](i)*sj->coef[c2](j));
                    if(abs(imag(this->coef[c1](i)*sj->coef[c2](j)))>1e-12)  ABORT("Imaginary part");
                }

            if(c1xc2.isZero(1e-14)) continue;
            vector<bool > r; vector<vector<VectorXi > >pos1; vector<vector<int > >sign_final;
            clock_t start =clock();
            this->det[i].sod_spindens_upto_n(3,sj->det[j],r,pos1,sign_final);
            tot += clock()-start;
            if(r[2]){
                vector<VectorXi> pos = pos1[2];
                vector<int> sign = sign_final[2];
                for(unsigned int k=0;k<pos.size();k++){

                    if(pos[k](2)/abs(pos[k](2)) != pos[k](3)/abs(pos[k](3))) continue;
                    if(pos[k](4)/abs(pos[k](4)) != pos[k](5)/abs(pos[k](5))) continue;

                    for(int kk=0;kk<2;kk++){
                        if(pos[k](kk)<0) pos[k](kk) = abs(pos[k](kk)) - 1;
                        else if(pos[k](kk)>0) pos[k](kk) = this->det[0].Orb->NumberOfOrbitals() + abs(pos[k](kk)) - 1;
                        else {ABORT("Invalid entry in ci::ci_spindens3");}
                    }
                    for(int kk=2;kk<6;kk++){
                        if(pos[k](kk)!=0) pos[k](kk) = abs(pos[k](kk)) - 1;
                        else {ABORT("Invalid entry in ci::ci_spindens3");}
                    }

                    for(int c1=0;c1<this->total_states;c1++)
                        for(int c2=0;c2<sj->total_states;c2++){
                            if(sign[k]>0)
                                //                                rho3_vee[c1][c2](pos[k](0),pos[k](1))+=c1xc2(c1,c2)*dynamic_cast<mo*>(det[0].Orb)->col_data->vee->val_at(pos[k](2),pos[k](3),pos[k](4),pos[k](5));
                                rho3_vee[c1][c2](pos[k](0),pos[k](1))+=c1xc2(c1,c2)*Vee->val_at(pos[k](2),pos[k](3),pos[k](4),pos[k](5));
                                        else
                                        //                                        rho3_vee[c1][c2](pos[k](0),pos[k](1))-=c1xc2(c1,c2)*dynamic_cast<mo*>(det[0].Orb)->col_data->vee->val_at(pos[k](2),pos[k](3),pos[k](4),pos[k](5));
                                        rho3_vee[c1][c2](pos[k](0),pos[k](1))-=c1xc2(c1,c2)*Vee->val_at(pos[k](2),pos[k](3),pos[k](4),pos[k](5));
                        }
                }
            }
            if(r[1]){
                vector<VectorXi> pos = pos1[1];
                vector<int> sign = sign_final[1];
                for(unsigned int k=0;k<pos.size();k++){
                    int spi,spj,spk,spl;
                    //cout<<pos[k].transpose()<<endl;
                    if(pos[k](0)<0) spi=0; else if(pos[k](0)>0) spi=1; else {ABORT("Invalid entry in ci::ci_spindens2");}
                    if(pos[k](1)<0) spj=0; else if(pos[k](1)>0) spj=1; else {ABORT("Invalid entry in ci::ci_spindens2");}
                    if(pos[k](2)<0) spk=0; else if(pos[k](2)>0) spk=1; else {ABORT("Invalid entry in ci::ci_spindens2");}
                    if(pos[k](3)<0) spl=0; else if(pos[k](3)>0) spl=1; else {ABORT("Invalid entry in ci::ci_spindens2");}

                    for(int kk=0;kk<pos[k].size();kk++){
                        if(pos[k](kk)!=0) pos[k](kk) = abs(pos[k](kk))-1;
                        else {ABORT("Invalid entry in ci::ci_spindens1");}
                    }

                    for(int c1=0;c1<this->total_states/mult_ion;c1++)
                        for(int c2=0;c2<=c1;c2++)
                            for(int m1=0;m1<mult_ion;m1++)
                                for(int m2=0;m2<mult_ion;m2++){
                                    int n_c1 = mult_ion*c1+m1; int n_c2 = mult_ion*c2+m2;
                                    if(lc_fac.size()!=0){
                                        if(spk==spl and spi==spin[mult_ion*c2+m2] and spj==spin[mult_ion*c1+m1]){
                                            if(sign[k]>0){
                                                rho2[c1][c2]->addto(pos[k](0),pos[k](1),pos[k](2),pos[k](3),c1xc2(n_c1,n_c2)*lc_fac[m1]*lc_fac[m2]);}
                                            else
                                                rho2[c1][c2]->addto(pos[k](0),pos[k](1),pos[k](2),pos[k](3),-c1xc2(n_c1,n_c2)*lc_fac[m1]*lc_fac[m2]);
                                        }
                                    }
                                    if(ene_2e!=NULL){         // compute 2-e energy
                                        if(spi==spj and spk==spl){
                                            double sn=1.0;
                                            if(sign[k]<=0) sn*=-1.0;
                                            if(lc_fac.size()!=0) sn*= lc_fac[m1]*lc_fac[m2];
                                            //                                            (*ene_2e)(c1,c2) += c1xc2(n_c1,n_c2)*dynamic_cast<mo*>(det[0].Orb)->col_data->vee->val_at(pos[k](0),pos[k](1),pos[k](2),pos[k](3))*sn;
                                            (*ene_2e)(c1,c2) += c1xc2(n_c1,n_c2)*Vee->val_at(pos[k](0),pos[k](1),pos[k](2),pos[k](3))*sn;
                                        }
                                    }
                                }
                }

            }
            if(r[0]){
                vector<VectorXi> pos = pos1[0];
                vector<int> sign = sign_final[0];
                for(unsigned int k=0;k<pos.size();k++){
                    for(int kk=0;kk<pos[k].size();kk++){
                        if(pos[k](kk)<0)
                            pos[k](kk) = abs(pos[k](kk)) - 1;
                        else if(pos[k](kk)>0)
                            pos[k](kk) = this->det[0].Orb->NumberOfOrbitals() + abs(pos[k](kk)) - 1;
                        else
                            ABORT("Invalid entry in ci::ci_spindens1");
                    }
                    for(int c1=0;c1<this->total_states;c1++)
                        for(int c2=0;c2<sj->total_states;c2++){
                            if(sign[k]>0)
                                rho1[c1][c2](pos[k](0),pos[k](1))+=c1xc2(c1,c2);
                            else
                                rho1[c1][c2](pos[k](0),pos[k](1))-=c1xc2(c1,c2);
                        }
                }
            }
        }
    MPIwrapper::Barrier();

    //Broadcast the matrices to all other threads and add the recieved matrices to own matrices
    for(int c1=0;c1<this->total_states;c1++)
        for(int c2=0;c2<sj->total_states;c2++){
            MatrixXd temp = rho1[c1][c2];
#ifndef _NOMPI_
            MPI_Allreduce(temp.data(),rho1[c1][c2].data(),temp.rows()*temp.cols(),MPI_DOUBLE,MPI_SUM,MPIwrapper::communicator());
            temp = rho3_vee[c1][c2];
            MPI_Allreduce(temp.data(),rho3_vee[c1][c2].data(),temp.rows()*temp.cols(),MPI_DOUBLE,MPI_SUM,MPIwrapper::communicator());
#endif
        }
    for(int c1=0;c1<this->total_states/mult_ion;c1++)
        for(int c2=0;c2<sj->total_states/mult_ion;c2++){
#ifndef _NOMPI_
#ifdef __NO_SHM_4d__
            std::unique_ptr<double[]> tmp{ new double[rho2[c1][c2]->size()] }; // no shared memory, so reduce this as well
            std::memcpy(tmp.get(), rho2[c1][c2]->data(), rho2[c1][c2]->size() * sizeof(double));
            MPI_Allreduce(tmp.get(), rho2[c1][c2]->data(), rho2[c1][c2]->size(), MPI_DOUBLE, MPI_SUM, MPIwrapper::communicator());
#endif
#endif
        }

#ifndef _NOMPI_
    MatrixXd temp = *ene_2e;
    MPI_Allreduce(temp.data(),ene_2e->data(),temp.rows()*temp.cols(),MPI_DOUBLE,MPI_SUM,MPIwrapper::communicator());
#endif
    MPIwrapper::Barrier();

    //check that it is symmetric
    for(int c1=0;c1<this->total_states;c1++)
        for(int c2=0;c2<sj->total_states;c2++){
            if(c1==c2){
                if(not(rho3_vee[c1][c2]-rho3_vee[c1][c2].adjoint()).isZero(1e-10))
                    ABORT("ci::ci_spindens3_X_vee: Not symmetric");
                if(not(rho1[c1][c2]-rho1[c1][c2].adjoint()).isZero(1e-10))
                    ABORT("ci::ci_spindens1: Not symmetric");
            }
        }

    //    if(static_cast<mo*>(this->det[0].Orb)->col_data->no_electrons>1){
    if(nElectrons()>1){
        for(int c1=0;c1<rho2.size();c1++)
            for(int c2=0;c2<=c1;c2++){
                rho2[c1][c2]->remove_zero();
            }
    }

    mo *MO = dynamic_cast<mo*>(det[0].Orb);
    // Check for rho1 for one electron ion
    if(this->det[0].spin.size()==1){
        for(int c1=0;c1<this->total_states;c1++)
            for(int c2=0;c2<sj->total_states;c2++){

                MatrixXcd tt = MatrixXcd::Zero(2*MO->coef.rows(),2*MO->coef.rows());

                for(int i=0;i<this->det.size();i++)
                    for(int j=0;j<sj->det.size();j++){
                        int k,l;
                        if(this->det[i].spin[0]==0) k = this->det[i].spac[0];
                        else                       k = this->det[i].spac[0] + MO->coef.rows();
                        if(sj->det[j].spin[0]==0)   l = sj->det[j].spac[0];
                        else                       l = sj->det[j].spac[0] + MO->coef.rows();

                        tt(k,l) += real(this->coef[c1](i)*sj->coef[c2](j));
                        if(abs(imag(this->coef[c1](i)*sj->coef[c2](j)))>1e-12) cout<<"There is an imaginary component\n";
                    }
                // compare with rho1
                for(int p=0;p<tt.rows();p++)
                    for(int q=0;q<tt.cols();q++)
                        if(abs(tt(p,q)-rho1[c1][c2](p,q))>1e-12){
                            cout<<c1<<" "<<c2<<" "<<p<<" "<<q<<" "<<tt(p,q)<<" "<<rho1[c1][c2](p,q)<<endl;
                            cin.get();
                        }
            }
    }

}

void ci::ci_setup_all_spindyson(My4dArray_shared * Vee,
                                vector<vector<VectorXd > > &eta1, vector<vector<multiarray3d > > &eta3, vector<vector<VectorXd > > &eta5, ci* sj)
{
#ifdef _USE_BOOST_
    // eta3 stored as eta3_{mkl} instead of eta3_{klm}
    mo* MO = dynamic_cast<mo*>(sj->det[0].Orb);
    //Add an extra orbital to ion determinants
    for(int i=0;i<sj->det.size();i++){
        sj->det[i].spac.push_back(MO->coef.rows());
        sj->det[i].spin.push_back(1);
    }

    //construct eta's from rho's
    eta1.resize(this->total_states);
    eta5.resize(this->total_states);
    eta3.resize(this->total_states);
    for(int i=0;i<this->total_states;i++){
        eta1[i].resize(sj->total_states);
        eta5[i].resize(sj->total_states);
        eta3[i].resize(sj->total_states);
    }

    for(int c1=0;c1<this->total_states;c1++)
        for(int c2=0;c2<sj->total_states;c2++){
            int a = 2*this->det[0].Orb->NumberOfOrbitals();
            eta1[c1][c2] = VectorXd::Zero(a);
            eta5[c1][c2] = VectorXd::Zero(a);
            DEVABORT("no-boost");//eta3[c1][c2].resize(boost::extents[a][a][a]);

            for(int p=0;p<a;p++)
                for(int q=0;q<a;q++)
                    for(int r=0;r<a;r++)
                        eta3[c1][c2][p][q][r] = 0;
        }

    int i_slice = floor(double(this->det.size())/double(MPIwrapper::Size()));
    clock_t tot = 0;

    int i_start = MPIwrapper::Rank()*i_slice;
    int i_end = (MPIwrapper::Rank()+1)*i_slice;
    if(MPIwrapper::Rank()==MPIwrapper::Size()-1)  i_end=this->det.size();
    for(int i=i_start;i<i_end; i++)
        for(unsigned int j=0;j<sj->det.size(); j++){

            MatrixXcd c1xc2 = MatrixXcd::Zero(this->total_states,sj->total_states);
            for(int c1=0;c1<this->total_states;c1++)
                for(int c2=0;c2<sj->total_states;c2++)
                    c1xc2(c1,c2) = this->coef[c1](i)*sj->coef[c2](j);

            if(c1xc2.isZero(1e-14)) continue;
            vector<bool > r; vector<vector<VectorXi > >pos1; vector<vector<int > >sign_final;
            clock_t start =clock();
            this->det[i].sod_spindens_upto_n(3,sj->det[j],r,pos1,sign_final);
            tot += clock()-start;
            if(r[2]){
                vector<VectorXi> pos = pos1[2];
                vector<int> sign = sign_final[2];
                for(unsigned int k=0;k<pos.size();k++){

                    if(pos[k](2)/abs(pos[k](2)) != pos[k](3)/abs(pos[k](3))) continue;
                    if(pos[k](4)/abs(pos[k](4)) != pos[k](5)/abs(pos[k](5))) continue;

                    for(int kk=0;kk<2;kk++){
                        if(pos[k](kk)<0) pos[k](kk) = abs(pos[k](kk)) - 1;
                        else if(pos[k](kk)>0) pos[k](kk) = this->det[0].Orb->NumberOfOrbitals() + abs(pos[k](kk)) - 1;
                        else {ABORT("Invalid entry in ci::ci_spindens3");}
                    }
                    for(int kk=2;kk<6;kk++){
                        if(pos[k](kk)!=0) pos[k](kk) = abs(pos[k](kk)) - 1;
                        else {ABORT("Invalid entry in ci::ci_spindens3");}
                    }

                    if(pos[k](1) != 2*MO->coef.rows())   continue;   // Donot take these terms
                    for(int c1=0;c1<this->total_states;c1++)
                        for(int c2=0;c2<sj->total_states;c2++){
                            if(sign[k]>0)
                                //                                eta5[c1][c2](pos[k](0))+=real(c1xc2(c1,c2)*MO->col_data->vee->val_at(pos[k](2),pos[k](3),pos[k](4),pos[k](5)));
                                eta5[c1][c2](pos[k](0))+=real(c1xc2(c1,c2)*Vee->val_at(pos[k](2),pos[k](3),pos[k](4),pos[k](5)));
                                                              else
                                                              //                                                              eta5[c1][c2](pos[k](0))-=real(c1xc2(c1,c2)*MO->col_data->vee->val_at(pos[k](2),pos[k](3),pos[k](4),pos[k](5)));
                                                              eta5[c1][c2](pos[k](0))-=real(c1xc2(c1,c2)*Vee->val_at(pos[k](2),pos[k](3),pos[k](4),pos[k](5)));
                        }
                }
            }
            if(r[1]){
                vector<VectorXi> pos = pos1[1];
                vector<int> sign = sign_final[1];
                for(unsigned int k=0;k<pos.size();k++){
                    for(int kk=0;kk<4;kk++){
                        if(pos[k](kk)<0) pos[k](kk) = abs(pos[k](kk)) - 1;
                        else if(pos[k](kk)>0) pos[k](kk) = this->det[0].Orb->NumberOfOrbitals() + abs(pos[k](kk)) - 1;
                        else {ABORT("Invalid entry in ci::ci_spindens2");}
                    }
                    if(pos[k](1)!= 2*MO->coef.rows() and pos[k](3)!=2*MO->coef.rows())   cout<<"Something went wrong eta3\n";        //consistency check
                    if(pos[k][3]!=2*MO->coef.rows()) continue;

                    for(int c1=0;c1<this->total_states;c1++)
                        for(int c2=0;c2<sj->total_states;c2++){
                            if(sign[k]>0)
                                eta3[c1][c2][pos[k](2)][pos[k](0)][pos[k](1)] += real(c1xc2(c1,c2));
                            else
                                eta3[c1][c2][pos[k](2)][pos[k](0)][pos[k](1)] += -real(c1xc2(c1,c2));
                        }
                    //                cout<<pos[k].transpose()<<"  "<<sign[k]<<"  "<<eta3[0][0][pos[k](0)][pos[k](1)][pos[k](2)]/* <<" "<<eta3[0][2][pos[k](0)][pos[k](1)][pos[k](2)]*/<<endl;
                    //                cin.get();
                }
            }
            if(r[0]){
                vector<VectorXi> pos = pos1[0];
                vector<int> sign = sign_final[0];
                for(unsigned int k=0;k<pos.size();k++){
                    //cout<<pos[k].transpose()<<"  "<<sign[k]<<"  "<<this->coef[state1](i)*sj->coef[state2](j)<<endl;
                    for(int kk=0;kk<pos[k].size();kk++){
                        if(pos[k](kk)<0) pos[k](kk) = abs(pos[k](kk)) - 1;
                        else if(pos[k](kk)>0) pos[k](kk) = this->det[0].Orb->NumberOfOrbitals() + abs(pos[k](kk)) - 1;
                        else {ABORT("Invalid entry in ci::ci_spindens1");}
                    }
                    //                cout<<this->det[i]<<endl<<sj->det[j]<<endl<<c1xc2<<endl;
                    //                cout<<pos[k].transpose()<<"  "<<sign[k]<<endl;
                    //                cin.get();
                    if(pos[k](1) != 2*MO->coef.rows())   cout<<"Something went wrong eta1\n";        //consistency check
                    for(int c1=0;c1<this->total_states;c1++)
                        for(int c2=0;c2<sj->total_states;c2++){
                            if(sign[k]>0)
                                eta1[c1][c2](pos[k](0))+=real(c1xc2(c1,c2));
                            else
                                eta1[c1][c2](pos[k](0))-=real(c1xc2(c1,c2));
                        }
                }
            }
        }
    MPIwrapper::Barrier();

    //Broadcast the matrices to all other threads and add the recieved matrices to own matrices
    for(int c1=0;c1<this->total_states;c1++)
        for(int c2=0;c2<sj->total_states;c2++){
            VectorXd temp = eta1[c1][c2];
#ifndef _NOMPI_
            MPI_Allreduce(temp.data(),eta1[c1][c2].data(),temp.rows()*temp.cols(),MPI_DOUBLE,MPI_SUM,MPIwrapper::communicator());
            temp = eta5[c1][c2];
            MPI_Allreduce(temp.data(),eta5[c1][c2].data(),temp.rows()*temp.cols(),MPI_DOUBLE,MPI_SUM,MPIwrapper::communicator());
#endif
            multiarray3d A_temp = eta3[c1][c2];
#ifndef _NOMPI_
            MPI_Allreduce(A_temp.data(),eta3[c1][c2].data(),std::pow(2*MO->coef.rows(),3),MPI_DOUBLE,MPI_SUM,MPIwrapper::communicator());
#endif

            //check for eta3
            int shift=0;
            for(int i=0;i<eta1[c1][c2].size();i++){
                //WARNING: the following is unclear, very likely, MO always no_electrons is number of electrons in ION
                //                if(abs(MO->col_data->no_electrons*eta1[c1][c2](i) - Map<MatrixXd>(eta3[c1][c2].data()+shift,2*MO->coef.rows(),2*MO->coef.rows()).trace())>1e-12){
                if(abs((nElectrons()-1)*eta1[c1][c2](i) - Map<MatrixXd>(eta3[c1][c2].data()+shift,2*MO->coef.rows(),2*MO->coef.rows()).trace())>1e-12){
                    cerr<<"Some problen with etas "<<c1<<" "<<c2<<" "<<i<<"  "<<abs(eta1[c1][c2](i) - Map<MatrixXd>(eta3[c1][c2].data()+shift,2*MO->coef.rows(),2*MO->coef.rows()).trace())<<endl;
                }
                shift+=4*MO->coef.rows()*MO->coef.rows();
            }
        }

    // Check for eta3 for one electron ion
    //    if(MO->col_data->no_electrons==1){
    //WARNING: the following is unclear, very likely, MO always no_electrons is number of electrons in ION
    if(nElectrons()==2){
        for(int c1=0;c1<this->total_states;c1++)
            for(int c2=0;c2<sj->total_states;c2++){
                multiarray3d t;

                int a = 2*this->det[0].Orb->NumberOfOrbitals();
                DEVABORT("no-boost");//t.resize(boost::extents[a][a][a]);

                for(int p=0;p<a;p++)
                    for(int q=0;q<a;q++)
                        for(int r=0;r<a;r++)
                            t[p][q][r] = 0;

                for(int i=0;i<this->det.size();i++)
                    for(int j=0;j<sj->det.size();j++){
                        int k,l,m;
                        if(this->det[i].spin[0]==0) k = this->det[i].spac[0];
                        else                       k = this->det[i].spac[0] + MO->coef.rows();
                        if(this->det[i].spin[1]==0) m = this->det[i].spac[1];
                        else                       m = this->det[i].spac[1] + MO->coef.rows();
                        if(sj->det[j].spin[0]==0)   l = sj->det[j].spac[0];
                        else                       l = sj->det[j].spac[0] + MO->coef.rows();

                        t[m][k][l] += real(this->coef[c1](i)*sj->coef[c2](j));
                        t[k][m][l] -= real(this->coef[c1](i)*sj->coef[c2](j));
                        if(abs(imag(this->coef[c1](i)*sj->coef[c2](j)))>1e-12) cout<<"There is an imaginary component\n";
                    }

                // compare with eta3
                for(int p=0;p<a;p++)
                    for(int q=0;q<a;q++)
                        for(int r=0;r<a;r++)
                            if(abs(t[p][q][r]-eta3[c1][c2][p][q][r])>1e-12){
                                cout<<c1<<" "<<c2<<" "<<p<<" "<<q<<" "<<r<<" "<<t[p][q][r]<<" "<<eta3[c1][c2][p][q][r]<<endl;
                            }
            }
    }

    MPIwrapper::Barrier();


    //Pop back extra orbital
    for(int i=0;i<sj->det.size();i++){
        sj->det[i].spac.pop_back();
        sj->det[i].spin.pop_back();
    }
#endif
}
/*
void ci::ci_matel_vector_multiple_states(VectorXcd& res,string op, ci* sj){

  //used for bound-continuum matrix elements: should be used in following way <bound | op | continuum >
  
  if(det[0].Orb->S.no_energ!=sj->total_states){
    cout<<"Number of scattering solutions is not equal to number of final ionic states- some mismatch"<<endl;
    cout<<"Number of states ="<<sj->total_states<<endl;
    cout<<"Number of scattering solutions ="<<det[0].Orb->S.no_energ<<endl;
}

  res = VectorXcd::Zero(det[0].Orb->S.no_fns);
  
  int rank = MPI::COMM_WORLD.Get_rank();
  int size = MPI::COMM_WORLD.Get_size();
  
  if (rank==0){
    int start_i = (int)(rank*this->det.size()/size);
    int end_i = (int)((rank+1)*this->det.size()/size);
    for(int i=start_i; i<end_i; i++)
      for(unsigned int j=0; j<sj->det.size(); j++){
    VectorXcd temp = VectorXcd::Zero(res.size());
    this->det[i].sod_matel_vector(temp,op,sj->det[j]);
    //res += temp*this->coef(i)*sj->coef(j);
    for(int s=0;s<det[0].Orb->S.no_energ;s++)
      res.segment(s*det[0].Orb->S.no_fns/det[0].Orb->S.no_energ,det[0].Orb->S.no_fns/det[0].Orb->S.no_energ) += temp.segment(s*det[0].Orb->S.no_fns/det[0].Orb->S.no_energ,det[0].Orb->S.no_fns/det[0].Orb->S.no_energ)*this->coef[0](i)*sj->coef[s](j);
      }
    for(int i=1; i<size; i++){
      VectorXcd value = VectorXcd::Zero(det[0].Orb->S.no_fns);
      MPI::Status status;
      MPI::COMM_WORLD.Recv(value.data(),value.size(),MPI::DOUBLE_COMPLEX,i,1,status);
      res+=value;
    }
  }
  else{
    int start_i = (int)(rank*this->det.size()/size);
    int end_i = (int)((rank+1)*this->det.size()/size);
    for(int i=start_i; i<end_i; i++)
      for(unsigned int j=0; j<sj->det.size(); j++){
    VectorXcd temp = VectorXcd::Zero(res.size());
    this->det[i].sod_matel_vector(temp,op,sj->det[j]);
    //res += temp*this->coef(i)*sj->coef(j);
    for(int s=0;s<det[0].Orb->S.no_energ;s++)
      res.segment(s*det[0].Orb->S.no_fns/det[0].Orb->S.no_energ,det[0].Orb->S.no_fns/det[0].Orb->S.no_energ) += temp.segment(s*det[0].Orb->S.no_fns/det[0].Orb->S.no_energ,det[0].Orb->S.no_fns/det[0].Orb->S.no_energ)*this->coef[0](i)*sj->coef[s](j);
      }
    MPI::COMM_WORLD.Send(res.data(),res.size(),MPI::DOUBLE_COMPLEX,0,1);
  }
}

void ci::ci_matel_vector(VectorXcd& res,string op, ci* sj, int state1, int state2){

    //used for bound-continuum matrix elements: should be used in following way <bound | op | continuum >
  int no_fns;
  if(det[0].Orb->ionized_electron == "plane")
    no_fns = det[0].Orb->P.k.rows();
  else if(det[0].Orb->ionized_electron == "scattering")
    no_fns = det[0].Orb->S.no_fns;
  else{
   cout<<"Something went wrong "<<endl;
   exit(1);
  }
  
  res = VectorXcd::Zero(no_fns);
  
  int rank = MPI::COMM_WORLD.Get_rank();
  int size = MPI::COMM_WORLD.Get_size();
  
  if (rank==0){
    int start_i = (int)(rank*this->det.size()/size);
    int end_i = (int)((rank+1)*this->det.size()/size);
    for(int i=start_i; i<end_i; i++)
      for(unsigned int j=0; j<sj->det.size(); j++){
    VectorXcd temp = VectorXcd::Zero(res.size());
    this->det[i].sod_matel_vector(temp,op,sj->det[j]);
    res += temp*this->coef[state1](i)*sj->coef[state2](j);
      }
    for(int i=1; i<size; i++){
      VectorXcd value = VectorXcd::Zero(no_fns);
      MPI::Status status;
      MPI::COMM_WORLD.Recv(value.data(),value.size(),MPI::DOUBLE_COMPLEX,i,1,status);
      res+=value;
    }
  }
  else{
    int start_i = (int)(rank*this->det.size()/size);
    int end_i = (int)((rank+1)*this->det.size()/size);
    for(int i=start_i; i<end_i; i++)
      for(unsigned int j=0; j<sj->det.size(); j++){
    VectorXcd temp = VectorXcd::Zero(res.size());
    this->det[i].sod_matel_vector(temp,op,sj->det[j]);
    res += temp*this->coef[state1](i)*sj->coef[state2](j);
      }
    MPI::COMM_WORLD.Send(res.data(),res.size(),MPI::DOUBLE_COMPLEX,0,1);
  }
}

void ci::ci_dipoles_vector(VectorXcd& resx, VectorXcd& resy, VectorXcd& resz, ci* sj, int state1, int state2){

    //used for bound-continuum matrix elements: should be used in following way <bound | op | continuum >

  int no_fns;
  if(det[0].Orb->ionized_electron == "plane")
    no_fns = det[0].Orb->P.k.rows();
  else if(det[0].Orb->ionized_electron == "scattering")
    no_fns = det[0].Orb->S.no_fns;
  else{
   cout<<"Something went wrong "<<endl;
   exit(1);
  }

  resx = VectorXcd::Zero(no_fns);
  resy = VectorXcd::Zero(no_fns);
  resz = VectorXcd::Zero(no_fns);

  int rank = MPI::COMM_WORLD.Get_rank();
  int size = MPI::COMM_WORLD.Get_size();

  if (rank==0){
    int start_i = (int)(rank*this->det.size()/size);
    int end_i = (int)((rank+1)*this->det.size()/size);
    for(int i=start_i; i<end_i; i++)
      for(unsigned int j=0; j<sj->det.size(); j++){
        VectorXcd tempx = VectorXcd::Zero(resx.size());
        VectorXcd tempy = VectorXcd::Zero(resy.size());
        VectorXcd tempz = VectorXcd::Zero(resz.size());
        this->det[i].sod_dipoles_vector(tempx,tempy,tempz,sj->det[j]);
        resx += tempx*this->coef[state1](i)*sj->coef[state2](j);
        resy += tempy*this->coef[state1](i)*sj->coef[state2](j);
        resz += tempz*this->coef[state1](i)*sj->coef[state2](j);
      }
    for(int i=1; i<size; i++){
      VectorXcd value = VectorXcd::Zero(no_fns*3);
      MPI::Status status;
      MPI::COMM_WORLD.Recv(value.data(),value.size(),MPI::DOUBLE_COMPLEX,i,1,status);
      resx+=value.segment(0,no_fns);
      resy+=value.segment(no_fns,no_fns);
      resz+=value.segment(2*no_fns,no_fns);
    }
  }
  else{
    int start_i = (int)(rank*this->det.size()/size);
    int end_i = (int)((rank+1)*this->det.size()/size);
    for(int i=start_i; i<end_i; i++)
      for(unsigned int j=0; j<sj->det.size(); j++){
        VectorXcd tempx = VectorXcd::Zero(resx.size());
        VectorXcd tempy = VectorXcd::Zero(resy.size());
        VectorXcd tempz = VectorXcd::Zero(resz.size());
        this->det[i].sod_dipoles_vector(tempx,tempy,tempz,sj->det[j]);
        resx += tempx*this->coef[state1](i)*sj->coef[state2](j);
        resy += tempy*this->coef[state1](i)*sj->coef[state2](j);
        resz += tempz*this->coef[state1](i)*sj->coef[state2](j);
      }
    VectorXcd value = VectorXcd::Zero(no_fns*3);
    value.segment(0,no_fns) = resx;
    value.segment(no_fns,no_fns) = resy;
    value.segment(2*no_fns,no_fns) = resz;
    MPI::COMM_WORLD.Send(value.data(),value.size(),MPI::DOUBLE_COMPLEX,0,1);
  }
}

void ci::ci_add_continuum(int spin){

  //Adds a continuum function with 'spin'
  
  for(unsigned int i=0; i<this->det.size(); i++)
    det[i].sod_extend(spin);

}
*/




ostream &operator <<(ostream &os, const ci &s)
{
    for(int i=0;i<s.total_states;i++){
        os<<"state "<<i+1<<endl;
        for(unsigned int j=0;j<s.det.size();j++)
            os<<s.det[j]<<"   "<<s.coef[i](j)<<endl;
    }

    return os;

}
