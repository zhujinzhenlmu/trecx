// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "inverse.h"
#include "useMatrix.h"
#include "tRecXchecks.h"
#include "index.h"

#include "overlapDVR.h"
#include "inverseDvr.h"
#include "inverseFem.h"
#include "printOutput.h"

using namespace std;

void Inverse::verify(const OperatorAbstract *Ovr) const{
    if(tRecX::off("inverseOverlap"))return;
    Coefficients one(iIndex);
    Coefficients sOne(iIndex);
    Coefficients res(iIndex);
    one.treeOrderStorage();
    sOne.treeOrderStorage();
    res.treeOrderStorage();

    UseMatrix Sinv(one.size(),one.size());
    UseMatrix S(one.size(),one.size());
    UseMatrix S_Sinv(one.size(),one.size());
    UseMatrix Sinv_S(one.size(),one.size());
    for(int k=0;k<one.size();k++){
        one.setToZero();
        one.storageData()[k]=1.;
        one.makeContinuous();

        Ovr->apply(1.,one,0.,sOne);
        sOne.makeContinuous();
        S.col(k)=UseMatrix::UseMap(sOne.storageData(),one.size(),1);

        apply(1.,sOne,0.,res);
        Sinv_S.col(k)=UseMatrix::UseMap(res.storageData(),one.size(),1);

        apply(1.,one,0.,sOne);
        Sinv.col(k)=UseMatrix::UseMap(sOne.storageData(),one.size(),1);

        Ovr->apply(1.,sOne,0.,res);
        res.makeContinuous();
        S_Sinv.col(k)=UseMatrix::UseMap(res.storageData(),one.size(),1);
    }
    Sinv_S.purge();
    S_Sinv.purge();
    S.purge();
    Sinv.purge();
    (S=(S-S.transpose())).purge().print("S",0);
//    S.purge().print("S",2);
    (Sinv=(Sinv-Sinv.transpose())).purge().print("Sinv",0);
    Sinv_S.print("S_invS",0);
    S_Sinv.print("S_Sinv",0);

}

const Inverse* Inverse::factory(const Index* Idx){
    if(Idx->inverseOverlap())return Idx->inverseOverlap(); // already set up
    if(Idx->overlap()==0)DEVABORT("setup overlap before use Inverse::factory");

    Index* idx=const_cast<Index*>(Idx);
    if(idx->isOverlapDiagonal()){
        idx->assignOverlap(new OverlapDVR(Idx));
        idx->setInverseOverlap(new InverseDVR(Idx));
    }
    else {
        if(idx->localOverlap()==0)DEVABORT("setup local overlap before use Inverse::factory");
        idx->setOverlap(idx->localOverlap());
        idx->setInverseOverlap(new InverseFEM(idx));
        UseMatrix ovr,inv;
        idx->overlap()->matrixAdd(1.,ovr);
        idx->inverseOverlap()->matrixAdd(1.,inv);
//        inv*=ovr;
        Eigen::Map<Eigen::MatrixXcd>(inv.data(),inv.rows(),inv.cols())*=Eigen::Map<Eigen::MatrixXcd>(ovr.data(),ovr.rows(),ovr.cols());
        if(not inv.isIdentity(1.e-10)){
            Sstr+ovr.str("ovr")+Sendl;
            Sstr+inv.str("inv")+Sendl;
        }
        else
            PrintOutput::DEVmessage("inverse OK (directly verified)");
    }

    idx->inverseOverlap()->assignToIndex(idx);
    return idx->inverseOverlap();
}

void Inverse::assignToIndex(Index *Idx) const {
    if(Idx!=iIndex)DEVABORT(Sstr+"index does not match inverse:"+Idx->strData()+iIndex->strData());
    Idx->setInverseOverlap(this);
    for(int k=0;k<childSize();k++)child(k)->assignToIndex(Idx->child(k));
}

