#include "hMatrix.h"

#include "readInput.h"
#include "matrixRk.h"
#include "coefficients.h"
#include "operatorTree.h"

/*
 * TruncationStrategy
 */
std::vector<unsigned int> HMatrix::TruncationStrategy::block(std::vector<unsigned int> path, unsigned int childSize){
    std::vector<unsigned int> res;
    
    if(childSize>=2){
        res.push_back(childSize/2);
        res.push_back(childSize-childSize/2);
    }

    return res;
}

std::vector<unsigned int> HMatrix::AdaptiveTruncationStrategy::block(std::vector<unsigned int> path, unsigned int childSize){
    std::vector<unsigned int> res;
    if(childSize>=2){
        res.push_back(childSize/2);
        res.push_back(childSize-childSize/2);
    }

    return res;
}


double HMatrix::TruncationStrategy::errorPercentage(const Index* iIndex, const Index* jIndex, const Index* iChildIndex, const Index* jChildIndex){
    //return double(iChildIndex->sizeStored()*jChildIndex->sizeStored())/double(iIndex->sizeStored()*jIndex->sizeStored()); // Area law

    double tot = iIndex->sizeStored()*jIndex->childSize() + jIndex->sizeStored()*iIndex->childSize(); // Perimeter law
    return (iChildIndex->sizeStored() + jChildIndex->sizeStored() )/tot;
}

bool HMatrix::TruncationStrategy::truncateToRk(const Index* iIndex, const Index* jIndex){
    if(iIndex->nSibling()!=jIndex->nSibling()) return true;
    if(iIndex->childSize()==0 or jIndex->childSize()==0) return true;
    if(iIndex->child(0)->childSize()==0 or jIndex->child(0)->childSize()==0) return true;

    return false;
}

bool HMatrix::AdaptiveTruncationStrategy::truncateToRk(const UseMatrix& original, HMatrix* hmat, MatrixRk* mat){

    mat->rankForAccuracy(0.,1.e-9);
    hmat->rankForAccuracy(0.,1.e-9);


    return mat->applyCount()<=hmat->applyCount()+4 /* Recursion penalty */ 
        or hmat->iIndex->childSize()==0 or hmat->jIndex->childSize()==0;

}

/*
 * HMatrixIndex
 */
void HMatrix::HMatrixIndex::create(std::vector<const Index*> leaves, std::vector<unsigned int> path, TruncationStrategy* strategy){
    std::vector<unsigned int> b = strategy->block(path, leaves.size());

    if(b.size()>0){
        int c=0;
        for(unsigned int i=0; i<b.size(); i++){
            std::vector<unsigned int> p = path;
            p.push_back(i);
            
            childAdd(new HMatrixIndex(std::vector<const Index*>(leaves.begin()+c,leaves.begin()+c+b[i]), p, strategy));
            c+=b[i];
        }

        if(c!=leaves.size()) ABORT("Blocking gone wrong!");
    }else{
        for(unsigned int i=0; i<leaves.size(); i++) childAdd(new HMatrixIndex(leaves[i], strategy));
    }
}


HMatrix::HMatrixIndex::HMatrixIndex(std::vector<const Index*> children, std::vector<unsigned int> path, TruncationStrategy* strategy): _base(0){
    create(children, path, strategy);
}


HMatrix::HMatrixIndex::HMatrixIndex(const Index* base, TruncationStrategy* strategy): _base(base){
    std::vector<const Index*> children;
    for(unsigned int i=0; i<base->childSize(); i++) children.push_back(base->child(i));
    
    if(children.size()>0) create(children, std::vector<unsigned int>(), strategy);

}

/*
 * HMatrixOperatorFloor and HMatrixOperatorAbstract
 */
HMatrix::HMatrixOperatorAbstract::HMatrixOperatorAbstract(const Index* IIndex, const Index* JIndex, const HMatrix* base): 
    OperatorAbstract("HMat", IIndex, JIndex),
    _base(base) {}

void HMatrix::HMatrixOperatorAbstract::apply(std::complex<double> A, const Coefficients &Vec, 
                                             std::complex<double> B, Coefficients &Y) const{

    const std::complex<double>* x = const_cast<Coefficients&>(Vec).data();
    std::complex<double>* y = Y.data();

    _base->axpy(A,x,B,y);
}

HMatrix::HMatrixOperatorFloor::HMatrixOperatorFloor(const HMatrix* base): 
    OperatorFloor(base->iIndex->sizeCompute(), base->jIndex->sizeCompute(), "HMat"),
    _base(base){}

void HMatrix::HMatrixOperatorFloor::axpy(const std::complex<double>& Alfa, const std::complex<double>* X, unsigned int SizX,
                                         const std::complex<double>& Beta, std::complex<double>* Y, unsigned int SizY) const{
    _base->axpy(Alfa, X, Beta, Y);
}
/*
 * HMatrix
 */
HMatrix::HMatrix(const HMatrixIndex* IIndex, const HMatrixIndex* JIndex, TruncationStrategy* strategy): 
    iIndex(IIndex),
    jIndex(JIndex),
    _storage(0),
    _strategy(strategy),
    _asOperatorFloor(0),
    _asOperatorAbstract(0){

    if(IIndex->_base!=0 and JIndex->_base!=0) _asOperatorAbstract = new HMatrixOperatorAbstract(IIndex->_base, JIndex->_base, this);
    _asOperatorFloor = new HMatrixOperatorFloor(this);
}


HMatrix::HMatrix(const HMatrixIndex* IIndex, const HMatrixIndex* JIndex, TruncationStrategy* strategy, const UseMatrix& mat):
    iIndex(IIndex),
    jIndex(JIndex),
    _storage(0),
    _strategy(strategy),
    _asOperatorFloor(0),
    _asOperatorAbstract(0){



    if(IIndex->_base!=0 and JIndex->_base!=0) _asOperatorAbstract = new HMatrixOperatorAbstract(IIndex->_base, JIndex->_base, this);
    _asOperatorFloor = new HMatrixOperatorFloor(this);

    AdaptiveTruncationStrategy* adaptiveStrategy = dynamic_cast<AdaptiveTruncationStrategy*>(strategy);

    for(unsigned int i=0; i<iIndex->childSize(); i++){
        for(unsigned int j=0; j<jIndex->childSize(); j++){
            const UseMatrix m = mat.block(
                    iIndex->child(i)->posIndex(iIndex),
                    jIndex->child(j)->posIndex(jIndex), 
                    iIndex->child(i)->sizeStored(), 
                    jIndex->child(j)->sizeStored()
            );


            // Blocking is fixed
            if(adaptiveStrategy==0){
                if(iIndex->child(i)->childSize()==0 or jIndex->child(j)->childSize()==0 or strategy->truncateToRk(iIndex->child(i), jIndex->child(j))){
                    childAdd(new MatrixRk(
                                dynamic_cast<HMatrixIndex*>(iIndex->child(i)), 
                                dynamic_cast<HMatrixIndex*>(jIndex->child(j)), 
                                strategy,
                                m
                            ));
                }else{
                    childAdd(new HMatrix(
                                dynamic_cast<HMatrixIndex*>(iIndex->child(i)), 
                                dynamic_cast<HMatrixIndex*>(jIndex->child(j)), 
                                strategy, 
                                m
                            ));
                }
            }
            
            // Blocking is adaptive
            else{

            
                MatrixRk* mat = new MatrixRk(
                            dynamic_cast<HMatrixIndex*>(iIndex->child(i)),
                            dynamic_cast<HMatrixIndex*>(jIndex->child(j)),
                            strategy,
                            m
                        );

                HMatrix* hmat = new HMatrix(
                            dynamic_cast<HMatrixIndex*>(iIndex->child(i)),
                            dynamic_cast<HMatrixIndex*>(jIndex->child(j)),
                            strategy,
                            m 
                        );
                
                if(adaptiveStrategy->truncateToRk(m, hmat, mat)){
                    delete hmat;
                    childAdd(mat);
                }else{
                    delete mat;
                    childAdd(hmat);
                }
            }
        }

    }

}

HMatrix::~HMatrix(){
    if(_storage!=0) delete _storage;
}

unsigned int HMatrix::storageRequirement() const{
    unsigned int res=0;
    for(unsigned int i=0; i<childSize(); i++) res+=child(i)->storageRequirement();
    return res;
}

void HMatrix::storage(std::complex<double>* storage){
    if(storage==0){
        if(_storage!=0) delete _storage;

        _storage = new std::vector<std::complex<double> >(storageRequirement());
        storage = _storage->data();
    }

    unsigned int c=0;
    for(unsigned int i=0; i<childSize(); i++){
        child(i)->storage(storage+c);
        c+=child(i)->storageRequirement();
    }
}

void HMatrix::setPositions(){
    _iPos.clear();
    _jPos.clear();
    for(unsigned int i=0; i<childSize(); i++){
        _iPos.push_back(child(i)->iIndex->posIndex(iIndex));
        _jPos.push_back(child(i)->jIndex->posIndex(jIndex));

        child(i)->setPositions();
    }
}

static void findLeaves(HMatrix* mat, std::vector<HMatrix*>& leaves){
    if(mat->childSize()==0) leaves.push_back(mat);
    for(unsigned int i=0; i<mat->childSize(); i++) findLeaves(mat->child(i), leaves);
}

/*
 * TODO: Memory leak as implemented here!
 */
HMatrix* HMatrix::flattened(){    
    std::vector<HMatrix*> leaves;
    findLeaves(this, leaves);

    unsigned int size=0;
    for(unsigned int i=0; i<leaves.size(); i++) size+=leaves[i]->iIndex->sizeStored()*leaves[i]->jIndex->sizeStored();

    HMatrix* res = new HMatrix(iIndex, jIndex, _strategy);
    for(unsigned int i=0; i<leaves.size(); i++) res->childAdd(leaves[i]);
    res->setPositions();

    return res;
}

HMatrix* HMatrix::truncate(const OperatorAbstract* op, TruncationStrategy* strategy){
    if(strategy==0){
        strategy = TruncationStrategy::instantiateDefault();
    }


    HMatrixIndex* iBlockedIndex = new HMatrixIndex(op->iIndex, strategy);
    HMatrixIndex* jBlockedIndex = new HMatrixIndex(op->jIndex, strategy);

    iBlockedIndex->resetFloor(0);
    jBlockedIndex->resetFloor(0);

    iBlockedIndex->sizeCompute();
    jBlockedIndex->sizeCompute();


    if(iBlockedIndex->sizeStored()!=op->iIndex->sizeStored()) ABORT("i blocking went wrong: "+std::to_string(iBlockedIndex->sizeStored())+"!="+std::to_string(op->iIndex->sizeStored()));
    if(jBlockedIndex->sizeStored()!=op->jIndex->sizeStored()) ABORT("j blocking went wrong: "+std::to_string(jBlockedIndex->sizeStored())+"!="+std::to_string(op->jIndex->sizeStored()));

    std::vector<std::complex<double> > m;
    op->matrix(m);
    UseMatrix mat(UseMatrix::UseMap(m.data(), op->iIndex->sizeStored(), op->jIndex->sizeStored()));

    HMatrix* res= new HMatrix(iBlockedIndex, jBlockedIndex, strategy, mat);
    res->setPositions();

    return res;
}

void HMatrix::truncateFloorLevel(OperatorTree* tree, TruncationStrategy* strategy){
    for(unsigned int i=0; i<tree->childSize(); i++){
        if(tree->child(i)->oFloor!=0){
            bool trunc=true;
            //Suppress truncation of diagonal matrices
            if(tree->child(i)->applyCount()<=std::min(tree->child(i)->iIndex->sizeStored(), tree->child(i)->jIndex->sizeStored()))
                trunc=false;
           
            if(trunc){
                HMatrix* hmat = truncate(tree->child(i), strategy);
                delete tree->child(i)->oFloor;
                tree->child(i)->oFloor = hmat->asOperatorFloor();
            }
        }else{
            truncateFloorLevel(tree->child(i), strategy);
        }
    }
}

void HMatrix::fullMatrix(UseMatrix& mat) const{
    if(parent()==0){
        mat=UseMatrix(iIndex->sizeStored(), jIndex->sizeStored());
    }

    for(unsigned int i=0; i<childSize(); i++){
        UseMatrix tmp = mat.block(
                    child(i)->iIndex->posIndex(iIndex), 
                    child(i)->jIndex->posIndex(jIndex), 
                    child(i)->iIndex->sizeStored(), 
                    child(i)->jIndex->sizeStored()
        );

        child(i)->fullMatrix(tmp);
    }
}

void HMatrix::axpy(std::complex<double> a, const std::complex<double>* x, std::complex<double> b, std::complex<double>* y) const{
    // IMPORTANT: Only descend with b==1.
    if(b!=1.){
        if(b==0.){
            for(unsigned int i=0; i<iIndex->sizeStored(); i++) y[i]=0.;
        } else {
            for(unsigned int i=0; i<iIndex->sizeStored(); i++) y[i]*=b;
        }
        b=1.;
    }
   
    for(unsigned int i=0; i<childSize(); i++){ 
        child(i)->axpy(a,
                x + _jPos[i], b,
                y + _iPos[i]
        );
    }
}


long HMatrix::applyCount() const{
    long res=0;
    for(unsigned int i=0; i<childSize(); i++) res+=child(i)->applyCount();
    return res;
}

void HMatrix::rank(unsigned int rank){
    for(unsigned int i=0; i<childSize(); i++) child(i)->rank(rank);
}


void HMatrix::accuracy(double& rel, double& abs, const UseMatrix* Mat) const{
    UseMatrix mat;
    if(Mat==0){
        fullMatrix(mat);
    }else{
        mat = *Mat;
    }

    UseMatrix mat_;
    asOperatorFloor()->matrix(mat_);

    double diffNormSquared=0.;
    double normSquared=0.;
    for(unsigned int i=0; i<mat.rows(); i++){
        for(unsigned int j=0; j<mat.cols(); j++){
            diffNormSquared+=std::pow(std::abs(mat(i,j)-mat_(i,j)), 2);
            normSquared+=std::pow(mat(i,j).real(),2)+std::pow(mat(i,j).imag(),2);
        }
    }

    rel = std::sqrt(diffNormSquared/normSquared);
    abs = std::sqrt(diffNormSquared);
}


void HMatrix::rankForAccuracy(double rel, double abs, const UseMatrix* Mat){
    UseMatrix mat;
    if(Mat==0){
        fullMatrix(mat);
    }else{
        mat = *Mat;
    }

    if(abs<=0.){
        double normSquared=0.;
        for(unsigned int i=0; i<mat.rows(); i++){
            for(unsigned int j=0; j<mat.cols(); j++){
                normSquared+=std::pow(mat(i,j).real(),2)+std::pow(mat(i,j).imag(),2);
            }
        }

        if(rel<=0.) rel=defaultRelativeAccuracy;
        abs = rel* std::sqrt(normSquared);
        rel = 0.;
    }

    for(unsigned int i=0; i<childSize(); i++){
        double perc = _strategy->errorPercentage(iIndex, jIndex, child(i)->iIndex, child(i)->jIndex);
        UseMatrix tmp = mat.block(
                    child(i)->iIndex->posIndex(iIndex),
                    child(i)->jIndex->posIndex(jIndex),
                    child(i)->iIndex->sizeStored(),
                    child(i)->jIndex->sizeStored()
        );

        child(i)->rankForAccuracy(0, abs*perc, &tmp);
    }

}

void HMatrix::fixedRankForAccuracy(double rel, double abs, const UseMatrix* Mat){
    UseMatrix mat;
    if(Mat==0){
        fullMatrix(mat);
    }else{
        mat = *Mat;
    }

    for(unsigned int r=0; r<=iIndex->sizeStored(); r++){
        rank(r);
        double rel_,abs_;
        accuracy(rel_,abs_);
        if(abs>0.){
            if(abs_<abs) return;
        }else if(rel>0.){
            if(rel_<rel) return;
        }else{
            if(rel_<defaultRelativeAccuracy) return;
        }
    }
}

void HMatrix::rank(OperatorTree* optree, unsigned int rank){
    for(unsigned int i=0; i<optree->childSize(); i++){
        HMatrixOperatorFloor* floor = dynamic_cast<HMatrixOperatorFloor*>(optree->child(i)->oFloor);
        if(floor==0){
            HMatrix::rank(optree->child(i), rank);
        }else{
            const_cast<HMatrix*>(floor->_base)->rank(rank);
        }
    }
}

int HMatrix::rank() const{
    if(childSize()==0) return -1;
    int rank=child(0)->rank();
    for(unsigned int i=1; i<childSize(); i++){
        rank=std::max(rank, child(i)->rank());   
    }

    return rank;
}

void HMatrix::fixedRankForAccuracy(OperatorTree* optree, double relAtEachFloor){
    if(relAtEachFloor<=0.) relAtEachFloor=defaultRelativeAccuracy;

    for(unsigned int i=0; i<optree->childSize(); i++){
        HMatrixOperatorFloor* floor = dynamic_cast<HMatrixOperatorFloor*>(optree->child(i)->oFloor);
        if(floor==0){
            HMatrix::fixedRankForAccuracy(optree->child(i), relAtEachFloor);
        }else{
            const_cast<HMatrix*>(floor->_base)->fixedRankForAccuracy(relAtEachFloor, 0.);
        }
    }
}

void HMatrix::accuracy(const OperatorTree* optree, double& abs){
    if(optree->parent()==0) abs=0.;

    for(unsigned int i=0; i<optree->childSize(); i++){
        HMatrixOperatorFloor* floor = dynamic_cast<HMatrixOperatorFloor*>(optree->child(i)->oFloor);
        if(floor==0){
            HMatrix::accuracy(optree->child(i), abs);
        }else{
            double _, abs_;
            floor->_base->accuracy(_, abs_);
            abs+=std::pow(abs_,2);
        }
    }

    if(optree->parent()==0){
        abs=std::sqrt(abs);
    }
}

std::vector<MatrixRk*> HMatrix::getLowrankMatrices(){
    std::vector<MatrixRk*> result;
    for(unsigned int i=0; i<childSize(); i++){
        std::vector<MatrixRk*> tmp = child(i)->getLowrankMatrices();
        result.insert(result.end(), tmp.begin(), tmp.end());
    }
    return result;
}

/*
 * Configuration
 */
std::string HMatrix::TruncationStrategy::defaultTruncationStrategy="";
double HMatrix::defaultRelativeAccuracy=1.e-9;

void HMatrix::TruncationStrategy::read(ReadInput& Inp){
    if(not Inp.found("HMatrix")) return;
    Inp.read("HMatrix","truncationStrategy",defaultTruncationStrategy,"P2","TruncationStrategy to be used as default");
}

HMatrix::TruncationStrategy* HMatrix::TruncationStrategy::instantiateDefault(){
    if(defaultTruncationStrategy=="adaptive") return new AdaptiveTruncationStrategy();
    else return new TruncationStrategy();
}

void HMatrix::read(ReadInput& Inp){
    if(not Inp.found("HMatrix")) return;
    Inp.read("HMatrix","relativeAccuracy",defaultRelativeAccuracy,"1.e-9","Default relative accuracy for HMatrix truncation");
    TruncationStrategy::read(Inp);
}

/*
 * TEMPORARY CODE
 */
void HMatrix::structureTikZ(std::ostream* output) const{
    std::string text="";

    if(iIndex->parent()==0 and jIndex->parent()==0)
        *output<<"\\draw (0,0) rectangle +("<<jIndex->sizeStored()<<",-"<<iIndex->sizeStored()<<");\n";

    for(unsigned int i=0; i<childSize(); i++) child(i)->structureTikZ(output);
}


static void printIndex(const Index* idx, std::ostream* output){

    std::vector<const Index*> path=idx->path();
    path.push_back(idx);
    
    for(unsigned int i=0; i<path.size()-1; i++){
        if(i!=0) *output<<"-";
        
        std::string dvr="";
        const BasisSet* iBas = path[i]->basisSet();
        if(iBas->name().find("DVR")!=std::string::npos){
            UseMatrix iGrid, iWeights;
            iBas->dvrRule(iGrid, iWeights);
            dvr=" DVR: "+tools::str(iGrid(path[i+1]->nSibling()).real());
        }
        *output<<path[i]->axisName()<<"("<<path[i+1]->nSibling()<<dvr<<")";
    }
    
}

void HMatrix::writeStructureTikZ(std::ostream* output) const{

    double rel, abs;
    accuracy(rel, abs);

    *output<<"\\documentclass{scrartcl}\n\\usepackage{tikz}\n\\begin{document}\n";
    *output<<"\\section*{";
    printIndex(iIndex->_base,output);
    *output<<", ";
    printIndex(jIndex->_base,output);
    *output<<", rel: "<<rel<<", abs: "<<abs<<"}\n";

    *output<<"\\resizebox{\\textwidth}{!}{\\begin{tikzpicture}\n";
    structureTikZ(output);
    *output<<"\\end{tikzpicture}}\n\\end{document}";
}

void HMatrix::collectDataForFloors(OperatorTree* optree, TruncationStrategy* strategy){
    std::vector<const OperatorTree*> optrees;
    std::vector<HMatrix*> hmats;
    for(const OperatorTree* l=optree->firstLeaf(); l!=0; l=l->nextLeaf()){
        optrees.push_back(l);
        hmats.push_back(HMatrix::truncate(l, strategy));
    }

    std::cerr<<"DONE TRUNCATING, WRITING RESULTS..."<<std::endl;

    /*
     * hmatrix_fixedrank
     */
    std::ofstream output;
    output.open((ReadInput::main.output()+"hmatrix_fixedrank").c_str());
    output<<"iIndex,jIndex,rank,appc(reg theo),appc(reg exp),appc(hmat theo),appc(hmat exp),diff(rel),diff(abs)"<<std::endl;

    for(unsigned int i=0; i<hmats.size(); i++){
        HMatrix* hmat = hmats[i];


        for(unsigned int k=0; k<10; k++){
            hmat->rank(k);

            printIndex(optrees[i]->iIndex, &output);
            output<<",";
            printIndex(optrees[i]->jIndex, &output);
            output<<","<<k<<",";
            output<<optrees[i]->applyCount()<<","<<optrees[i]->applicationCost()<<","<<hmat->applyCount()<<","<<hmat->asOperatorAbstract()->applicationCost()<<",";
        
            double rel, abs;
            hmat->accuracy(rel, abs);
            output<<std::setprecision(15)<<rel;
            output<<",";
            output<<std::setprecision(15)<<abs;
            output<<std::endl;

        }


    }
   
    output.close();


    /*
     * hmatrix_fixedaccuracy
     */
    output.open((ReadInput::main.output()+"hmatrix_fixedaccuracy").c_str());
    output<<"iIndex,jIndex,accuracy,appc(reg theo),appc(reg exp),appc(hmat theo),appc(hmat exp),diff(rel),diff(abs)"<<std::endl;

    for(unsigned int i=0; i<hmats.size(); i++){
        HMatrix* hmat = hmats[i];

        for(double acc=1.; acc>=1.e-12; acc/=10.){
            hmat->rankForAccuracy(acc, 0.);

            printIndex(optrees[i]->iIndex, &output);
            output<<",";
            printIndex(optrees[i]->jIndex, &output);
            output<<","<<acc<<",";
            output<<optrees[i]->applyCount()<<","<<optrees[i]->applicationCost()<<","<<hmat->applyCount()<<","<<hmat->asOperatorAbstract()->applicationCost()<<",";

            double rel, abs;
            hmat->accuracy(rel, abs);
            output<<std::setprecision(15)<<rel;
            output<<",";
            output<<std::setprecision(15)<<abs;
            output<<std::endl;

        }


    }
    
    output.close();


    /*
     * hmatrix_structure.[01234...].tex
     */

    for(unsigned int i=0; i<hmats.size(); i++){
        HMatrix* hmat = hmats[i];

        hmat->rankForAccuracy(1.e-9, 0.);
        double rel, abs;
        hmat->accuracy(rel, abs);
        
        output.open((ReadInput::main.output()+"hmatrix_structure."+std::to_string(i)+".tex").c_str());
        output<<"\\documentclass{scrartcl}\n\\usepackage{tikz}\n\\begin{document}\n";
        output<<"\\section*{";
        printIndex(optrees[i]->iIndex,&output);
        output<<", ";
        printIndex(optrees[i]->jIndex,&output);
        output<<", rel: "<<rel<<"}\n";

        output<<"\\resizebox{\\textwidth}{!}{\\begin{tikzpicture}\n";
        hmat->structureTikZ(&output);
        output<<"\\end{tikzpicture}}\n\\end{document}";
        output.close();

    }

    for(unsigned int i=0; i<hmats.size(); i++) delete hmats[i];

}

void HMatrix::collectDataForFloors2(OperatorTree* optree, std::ofstream* output){
    std::ofstream _output;
    if(output==0){
        _output.open((ReadInput::main.output()+"hmatrix_accuracy").c_str());
        _output<<"iIndex,jIndex,rank,diff(rel),diff(abs)"<<std::endl;
        output=&_output;
    }

    for(unsigned int i=0; i<optree->childSize(); i++){
        HMatrixOperatorFloor* floor = dynamic_cast<HMatrixOperatorFloor*>(optree->child(i)->oFloor);
        if(floor==0){
            HMatrix::collectDataForFloors2(optree->child(i), output);
        }else{
            const HMatrix* mat = floor->_base;
            printIndex(optree->child(i)->iIndex,output);
            *output<<",";
            printIndex(optree->child(i)->jIndex,output);
            *output<<","<<mat->rank()<<",";
            double rel, abs;
            mat->accuracy(rel, abs);
            *output<<std::setprecision(15)<<rel<<","<<abs<<std::endl;
        }
    }
}

void HMatrix::collectEpsilonF(OperatorTree* optree, std::ofstream* output){
    std::ofstream _output;
    if(output==0){
        _output.open((ReadInput::main.output()+"hmatrix_epsilonf").c_str());
        _output<<"iIndex,jIndex,submatIndex,k,epsilon,f"<<std::endl;
        output=&_output;
    }

    for(unsigned int i=0; i<optree->childSize(); i++){
        HMatrixOperatorFloor* floor = dynamic_cast<HMatrixOperatorFloor*>(optree->child(i)->oFloor);
        if(floor==0){
            HMatrix::collectEpsilonF(optree->child(i), output);
        }else{
            std::string iIdx, jIdx;
            std::ostringstream os1, os2;
            printIndex(optree->child(i)->iIndex, &os1);
            iIdx = os1.str();
            printIndex(optree->child(i)->jIndex, &os2);
            jIdx = os2.str();

            std::vector<MatrixRk*> mats = const_cast<HMatrix*>(floor->_base)->getLowrankMatrices();            
            for(unsigned int i=0; i<mats.size(); i++){
                for(unsigned int k=0; k<=std::min(mats[i]->iIndex->sizeStored(), mats[i]->jIndex->sizeStored()); k++){
                    double acc, _;
                    mats[i]->rank(k);
                    mats[i]->accuracy(_,acc);

                    *output<<iIdx<<","<<jIdx<<","<<i<<","<<k<<","<<std::setprecision(15)<<acc<<","<<mats[i]->applyCount()<<std::endl;
                }
            }
        }
    }
}

