// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "radialmultipole.h"
#include "useMatrix.h"
#include "qtEigenDense.h"
#include "basisMat.h"

using namespace std;
#include "eigenNames.h"


RadialMultipole::RadialMultipole(const BasisSetDef* Set1, const BasisSetDef* Set2, int lambdaMax, std::vector<int> order):lambdaMax(lambdaMax)
{
  // if order not given, set default
  if(order.size()==0){      
      order.push_back(2*Set1->order-1);
      order.push_back(2*Set2->order-1);

//      order.push_back(Set1->order);
//      order.push_back(Set2->order);
    }

//  // create new Basis definitions form original
//  BasisSet::Def D1(order[0],Set1->shift,Set1->scale,"assocLegendre",true,
//      true,true,Set1->coor,Set1->margin,Set1->comSca,Set1->deriv,vector<double>(1,0.));

//  BasisSet::Def D2(order[1],Set2->shift,Set2->scale,"assocLegendre",true,
//      true,true,Set2->coor,Set2->margin,Set2->comSca,Set2->deriv,vector<double>(1,0.));

  // create new Basis definitions form original
  BasisSetDef D1(order[0],Set1->shift,Set1->scale,Set1->funcs,true,
      Set1->first,Set1->last,Set1->coor,Set1->margin,Set1->comSca,Set1->deriv,Set1->par);

  BasisSetDef D2(order[1],Set2->shift,Set2->scale,Set2->funcs,true,
      Set2->first,Set2->last,Set2->coor,Set2->margin,Set2->comSca,Set2->deriv,Set2->par);

  // create new BasisSets
  B1 = new BasisSet(D1);
  B2 = new BasisSet(D2);

  // Evaluate the radial multipole integrals in this basis B1, B2
  // and store them in mats
  for(unsigned int k=0;k<=lambdaMax;k++){
      UseMatrix temp_mat;
      RadialMultipoleMatrix(temp_mat,k);
      mats.push_back(temp_mat);
    }
}

RadialMultipole::~RadialMultipole()
{
  if(B1!=0) {delete B1; B1=0;}
  if(B2!=0) {delete B2; B2=0;}
}

void RadialMultipole::MatrixOnGrid(std::vector<UseMatrix> &res, const UseMatrix &points1, const UseMatrix &weig1, const UseMatrix &points2, const UseMatrix &weig2)  const
{
  res.clear();
  for(int k=0;k<=lambdaMax;k++){
      UseMatrix temp;
      MatrixOnGrid(temp,k,points1,weig1,points2,weig2);
      res.push_back(temp);
    }
}

void RadialMultipole::MatrixOnGrid(UseMatrix &res, int lambda, const UseMatrix &points1, const UseMatrix &weig1, const UseMatrix &points2, const UseMatrix &weig2) const
{
  if(lambda>lambdaMax) ABORT("lambda > lambdaMax");

  //converting multipole operator to a quadrature grid : V^l_{qq'} = P_{qn} * V^l_{nn'} * P_{n'q'}

  UseMatrix pnq1 = B1->val(points1);
  UseMatrix pnq2 = B2->val(points2);
  UseMatrix res1 = pnq1*mats[lambda]*pnq2.transpose();
  res = (res1.cwiseProduct(weig1*weig2.transpose())).transpose();
}

void RadialMultipole::RadialMultipoleMatrix(UseMatrix &res, int lambda)
{
  if(lambda>lambdaMax) ABORT("lambda > lambdaMax");

  if( (B2->lowBound()>=B1->lowBound() and B2->lowBound()<B1->upBound()) and (B2->upBound()>B1->lowBound() and B2->upBound()<=B1->upBound()) ){  // Diaogonal case
      // Diagonal case:
      // More complicated: integrate over lower and upper "triangles separately"

      cout<<"Diagonal case: "<<B1->lowBound()<<" "<<B1->upBound()<<" X "<<B2->lowBound()<<" "<<B2->upBound()<<endl;


      UseMatrix quad1, weig1;
      B1->quadRule(B1->defaultNQuad()+lambda,quad1,weig1);

      vector<UseMatrix> quad2(quad1.size()), weig2(quad1.size());

      for(int r1=0;r1<quad1.size();r1++){
          B2->quadRule(B2->defaultNQuad()+lambda,quad2[r1],weig2[r1]);

          if(B2->lowBound()<quad1(r1).real() and B2->upBound()>quad1(r1).real()){
              // split into 2 have quadratures

              UseMatrix q1,w1;

              double rescfac = (B2->upBound()-B2->lowBound());
              // Rescale weights
              w1 = weig2[r1] * (quad1(r1).real()-B2->lowBound())/rescfac;

              // Rescale and re-shift points
              q1 = ( (quad2[r1] - B2->lowBound()) * (quad1(r1).real()-B2->lowBound())/rescfac ) + B2->lowBound();

              // Overwrite quad2[r1], weig2[r1]
              quad2[r1] = q1;
              weig2[r1] = w1;
            }
          else {ABORT("");}
        }

      // setting up values of all polynomials at the quadrature points and weights
      UseMatrix val1 = B1->val(quad1);
      vector<UseMatrix> val2(quad2.size());
      for(unsigned int k=0;k<val2.size();k++)
        val2[k] = B2->val(quad2[k]);

//      val1 = quad1;
//      for(unsigned int k=0;k<val2.size();k++)
//        val2[k] = quad2[k];

//      val1 = UseMatrix::Constant(quad1.rows(),quad1.cols(),1.);
//      for(unsigned int k=0;k<val2.size();k++)
//        val2[k] = UseMatrix::Constant(quad2[k].rows(),quad2[k].cols(),1.);

      //Evaluating V^l_{nn'} matrix
      res = UseMatrix::Zero(val1.cols(),val2[0].cols());

      for(int r1=0;r1<quad1.size();r1++)
        for(int r2=0;r2<quad2[r1].size();r2++)
          res += val1.row(r1).transpose()*val2[r1].row(r2)*weig1(r1).real()*weig2[r1](r2).real()
              *std::pow(min(quad1(r1).real(),quad2[r1](r2).real()),lambda)/std::pow(max(quad1(r1).real(),quad2[r1](r2).real()),lambda+1);

      UseMatrix res_temp = res.transpose();
      res = res+res_temp;

//      res.print("abc",10);
//      cin.get();
    }

  else{ //Non diagonal case

      //setting up quadrature points and weights
      UseMatrix quad1,weig1,quad2,weig2;
      B1->quadRule(B1->defaultNQuad()+lambda,quad1,weig1);
      B2->quadRule(B2->defaultNQuad()+lambda,quad2,weig2);

      //setting up values of all polynomials at the quadrature points and weights
      UseMatrix val1 = B1->val(quad1);
      UseMatrix val2 = B2->val(quad2);

      //Evaluating V^l_{nn'} matrix
      res = UseMatrix::Zero(val1.cols(),val2.cols());

      for(int r1=0;r1<quad1.size();r1++)
        for(int r2=0;r2<quad2.size();r2++)
          res += val1.row(r1).transpose()*val2.row(r2)*weig1(r1).real()*weig2(r2).real()
              *std::pow(min(quad1(r1).real(),quad2(r2).real()),lambda)/std::pow(max(quad1(r1).real(),quad2(r2).real()),lambda+1);

      cout<<"Non diagonal"<<endl;
    }

  // Overlap Matrix
  UseMatrix Ov1,Ov2;
  BasisMat::get(vector<string>(1,"1"),vector<const BasisSet*>(1,B1),vector<const BasisSet*>(1,B1),Ov1,vector<string>(1,""));
  BasisMat::get(vector<string>(1,"1"),vector<const BasisSet*>(1,B2),vector<const BasisSet*>(1,B2),Ov2,vector<string>(1,""));
  UseMatrix res_temp = Ov1.inverse()*res*Ov2.inverse();
  res = res_temp;

}
