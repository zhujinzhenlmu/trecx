// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef _SEDNA_
#include "spectrumCoulomb.h"

#include "discretizationGrid.h"
#include "discretizationtsurffspectra.h"
#include "wavefunction.h"
#include "coefficients.h"
#ifdef _USE_GSL_
#include <gsl/gsl_sf_coulomb.h>
#endif
//#include "tsurffsource.h"
#include "tsurffSource.h"
#include "orthopol.h"
#include "str.h"
#include "printOutput.h"

#include "lapacke.h"

using namespace std;


SpectrumCoulomb::SpectrumCoulomb(ReadInput &Inp, ReadInput &RunInp)
{
}

bool SpectrumCoulomb::compute(const tSurffTools::Evaluator &TSurff, Coefficients &Spec){

    if(TSurff.D->idx()->hierarchy()!="Phi.Eta.Rn.Rn")ABORT("only for Phi.Eta.Rn, is: "+TSurff.D->idx()->hierarchy());

    const DiscretizationDerived * kDisc=TSurff.tSource[0]->smoothSpecDisc;

    if(TSurff.D->getAxis()[2].name!="Rn")ABORT("unsuitable axes");
    double boxSize=TSurff.D->getAxis()[2].boxsize();
    int rPoints=TSurff.D->idx()->descend(2)->sizeStored();

    // get k-grid spacing (must be equidistant)
    vector<double> kSpec=kDisc->idx()->grid("kRn");
    if(kSpec.size()==0)ABORT("no grid on axis kRn");
    if(not tools::isEquidistant(kSpec))ABORT("spectral k-grid is not equidistant");

    // get the largest kDelta < pi/box that is commensurate with spectral grid spacing
    double kDelta;
    for(int n=1;math::pi/boxSize<(kDelta=(kSpec[1]-kSpec[0])/n);n++);

    // momentum fit grid that includes all spectral momentum points
    kFit.assign(1,kSpec.back());
    while(kFit[0]>(1.1*kDelta))kFit.insert(kFit.begin(),kFit[0]-kDelta); // factor 1.1: do not include k~0
    while(kFit.back()<rPoints*2*math::pi/boxSize)kFit.push_back(kFit.back()+kDelta);


    //    hydrogenTest();


    // wf at end of pulse
    Wavefunction wfEnd(TSurff.D);
    ifstream stream((TSurff.outputDir+"wfEndPulse").c_str(),(ios_base::openmode) ios::beg|ios::binary);
    wfEnd.read(stream,true);
    if(wfEnd.coefs->norm()==0.)ABORT("zero function on "+TSurff.outputDir+"wfEndPulse");

    // transform wfEnd to grid
    vector<vector<double> > box(1,vector<double>(2,1./kFit.size()));
    box[0][1]=boxSize-box[0][0]; // keep away from 0 and end of box
    DiscretizationGrid dGrid(TSurff.D->idx(),vector<string>(1,"Rn"),vector<unsigned int>(1,kFit.size()),box);
    vector<double> rGrid=dGrid.idx()->grid("Rn");
    if(rGrid.size()!=kFit.size()){
        ABORT(Str("rGrid and kMon sizes differ, maybe multiple or missing r-points","\n")+rGrid);
    }

    Str("      Spectral grid")+kSpec.back()+"spacing"+(kSpec[1]-kSpec[0])+"size"+kSpec.size()+Str::print;
    Str("Fit grid - momentum")+kFit.back()+"spacing"+(kFit[1]-kFit[0])+"size"+kFit.size()+Str::print;
    Str("Fit grid - position")+rGrid.back()+"spacing"+(rGrid[1]-rGrid[0])+"size"+rGrid.size()+Str::print;

    Coefficients cGrid(dGrid.idx());
    dGrid.mapFromParent()->apply(1.,*wfEnd.coefs,0.,cGrid);

    // evaluate the Coulomb functions for all L on the k-r grid
    if(TSurff.D->idx()->descend()->axisName()!="Eta")ABORT("unsuitable index hierarchy");
    vector<vector<double> > transL(TSurff.D->idx()->descend()->childSize(),vector<double>(kFit.size()*kFit.size()));
    int nMax=int(2./(kFit[1]-kFit[0])); // largest principal quantum number for bound states
    if(nMax*5>kFit.size())PrintOutput::warning(Str("too many bound states included: bound")+nMax+"compared to scattering"+kFit.size());
    if(kFit[kFit.size()-nMax]<kSpec.back())ABORT("DEVELOPER: error - too many bound states, cut into spectrum");
    for(int i=0;i<rGrid.size();i++){
        for(int k=0;k<kFit.size();k++)
        {
            // scattering functions
            vector<double> fc(hydrogenRadialScatter(transL.size()-1,kFit[k],rGrid[i]));
            for(int l=0;l<transL.size();l++)transL[l][i+k*kFit.size()]=fc[l]*sqrt((rGrid[1]-rGrid[0]));
        }

        // replace highest k's with relevant bound states
        for(int l=0;l<transL.size();l++)
            for(int n=l+1;n<nMax+1;n++){
                transL[l].at(i+kFit.size()*(kFit.size()-(n-l)))=hydrogenRadialBound(l,n,rGrid[i])*sqrt(rGrid[1]-rGrid[0]);
            }
    }

    // map grid to spectrum
    Spec.reset(kDisc->idx());
    map(&cGrid,&Spec,transL);

}

void SpectrumCoulomb::map(Coefficients *CGrid, Coefficients *Spec, vector<vector<double> >&TransL){

    if(CGrid->idx()->axisName()!="Rn"){
        for(int k=0;k<CGrid->childSize();k++){
            map(CGrid->child(k),Spec->child(k),TransL);
        }
    }

    else
    {
        Coefficients cgrid(CGrid->idx()),spec(Spec->idx());
        cgrid.treeOrderStorage();
        spec.treeOrderStorage();
        cgrid=*CGrid;

        Eigen::MatrixXd u,vT;
        Eigen::VectorXd sing;
        Eigen::VectorXcd uC(cgrid.size());

        svd(Eigen::Map<Eigen::MatrixXd>(TransL[CGrid->index()[1]].data(),cgrid.size(),cgrid.size()),u,sing,vT);

        uC=u.adjoint()*Eigen::Map<Eigen::VectorXcd>(cgrid.data(),cgrid.size());
        for(int k=0;k<uC.size();k++)
            if(abs(sing(k))>1.e-12)
                uC(k)/=sing(k);
            else
                uC[k]=0.;
        Eigen::VectorXcd s(vT.cols());
        s=vT.adjoint()*uC;

        // verify
        Str out= Str("norm, diff")+cgrid.norm();
        Eigen::Map<Eigen::VectorXcd>(cgrid.data(),cgrid.size())-=
                Eigen::Map<Eigen::MatrixXd>(TransL[CGrid->index()[1]].data(),cgrid.size(),cgrid.size())*s;
        out+cgrid.norm()+Str::print;


        // get spectral grid
        vector<double> kSpec(spec.idx()->grid("kRn"));
        if(kSpec.size()==0)ABORT(spec.idx()->str()+"\nno kRn axis found");

        // insert spectrum for subset
        for(int k=0,l=0;k<spec.size();k++){
            while(kFit[l]<kSpec[k]+(kSpec[1]-kSpec[0])*1e-10)l++;
            spec.data()[k]=s.data()[l];
        }
        *Spec=spec;
    }
}

double SpectrumCoulomb::hydrogenRadialBound(int L, int N, double Radius, double Charge) const {

    // scale by charge
    OrthogonalLaguerre lagL(2*L+1);
    double rho=2*Radius*Charge/N;
    if(N<L+1)ABORT(Str("invalid N=")+N+" and L="+L);
    double nrm=pow(N,3)*0.5;
    for(int k=N-L;k<=N+L;k++)nrm*=double(k);
    return lagL.val(N-L,rho).back()*sqrt(Radius*lagL.weight(rho)/nrm);

}

vector<double> SpectrumCoulomb::hydrogenRadialScatter(int Lmax, double K, double Radius, double Charge) const{
    vector<double> fc(Lmax+1);
    double fail_exponent;
#ifndef _USE_GSL_
    ABORT("for computing Coulomb spectrum, need compile with -D_USE_GSL");
#else
    if(gsl_sf_coulomb_wave_F_array(0.,Lmax,-Charge/K,Radius*K,fc.data(),&fail_exponent)){
        //        Str("exponent")+fail_exponent+Str::print;
        //        ABORT(Str("coulomb wave failed at r*k=")+(Radius*K)+"Lmax="+Lmax+" K="+K);
        for(int l=0;l<fc.size();l++)fc[l]*=exp(fail_exponent);
    }
#endif
    for(int l=0;l<fc.size();l++)fc[l]*=sqrt(2.); // factor 2 (?? - probably the rescaling from r to rho)
    return fc;
}

bool SpectrumCoulomb::hydrogenTest() const {

    int nMax=20,lMax=19,pMax=10;

    OrthogonalLaguerre lag(0);
    vector<double> xQuad,wQuad;
    lag.quadrature(nMax+1,xQuad,wQuad);
    for (int L=0;L<lMax+1;L++){
        for(int n=L+1;n<=nMax;n++){
            double ovrmn;
            for(int m=L+1;m<=n;m++){
                ovrmn=0.;
                double scal=(double(n*m)/double(m+n));
                for(int k=0;k<xQuad.size();k++){
                    double wExp=wQuad[k]*exp(xQuad[k]);
                    ovrmn+=hydrogenRadialBound(L,m,xQuad[k]*scal)*hydrogenRadialBound(L,n,xQuad[k]*scal)*wExp;
                }
                ovrmn*=scal;
                if(n!=m and abs(ovrmn)>1.e-12)ABORT(Str("orthogonality error at L,m,n=")+L+m+n+":"+ovrmn);
            }
            if(abs(ovrmn-1)>1.e-12)ABORT(Str("normalization error")+ovrmn);
        }
    }
    PrintOutput::message(Str("OK bound state orthonormalization up to nMax,Lmax=")+nMax+lMax);

    lag.quadrature(20*nMax,xQuad,wQuad); // quadrature is not exact, use excess order
    for(int n=1;n<=nMax;n++){
        for (int L=0;L<min(n,lMax+1);L++){
            for(int p=0;p<pMax;p++){
                double kMom=(p+1)/double(5*n);
                double ovrnp;
                ovrnp=0.;
                double scal=double(n);
                for(int k=0;k<xQuad.size();k++){
                    double wExp=wQuad[k]*exp(xQuad[k]);
                    ovrnp+=hydrogenRadialBound(L,n,xQuad[k]*scal)*hydrogenRadialScatter(min(L,n-1),kMom,xQuad[k]*scal,1.).back()*wExp;
                }
                ovrnp*=scal;
                if(abs(ovrnp)>1.e-10)ABORT(Str("orthogonality error at L,m,n=")+L+n+kMom+":"+ovrnp);
            }
        }
    }
    PrintOutput::message(Str("OK bound-continuum orthogonality up to nMax,Lmax,pMin,pMax=")+nMax+lMax+(1./double(5*nMax))+(pMax/double(5*nMax)));

}

void SpectrumCoulomb::svd(const Eigen::MatrixXcd &A, Eigen::MatrixXcd &U, Eigen::VectorXd &Sing, Eigen::MatrixXcd &Vadj){
    vector<complex<double> > a;
    for(int j=0;j<A.cols();j++)
        for (int i=0;i<A.rows();i++)
            a.push_back(A(i,j));

    U=Eigen::MatrixXcd(A.rows(),A.rows());
    Vadj=Eigen::MatrixXcd(A.cols(),A.cols());
    Sing=Eigen::VectorXd(min(A.rows(),A.cols()));


//    static void svDecompose(std::complex<double>* A, unsigned int rows, unsigned int cols, double* s, std::complex<double>* U, std::complex<double>* V){
//        STARTDEBUG(svDecompose)
//        LAPACKE_zgesdd(/* matrix order */ LAPACK_COL_MAJOR, /* jobz */ 'A', /* m */ rows,/* n */ cols, /* A */ A, /* LDA */ rows,
//                       /* s */ s,/* U */ U, /* LDU */ rows, /* VT */ V, /* LDVT */ cols);
    LAPACKE_zgesdd(LAPACK_COL_MAJOR,'S',A.rows(),A.cols(),a.data(),A.rows(),Sing.data(),U.data(),U.rows(),Vadj.data(),Vadj.rows());
}
    void SpectrumCoulomb::svd(const Eigen::MatrixXd &A, Eigen::MatrixXd &U, Eigen::VectorXd &Sing, Eigen::MatrixXd &Vtrans){
    vector<double>a(A.data(),A.data()+A.size());
    U=Eigen::MatrixXd(A.rows(),A.rows());
    Vtrans=Eigen::MatrixXd(A.cols(),A.cols());
    Sing=Eigen::VectorXd(min(A.rows(),A.cols()));


    //    lapack_int LAPACKE_dgesdd( int matrix_layout, char jobz, lapack_int m,
    //                               lapack_int n, double* a, lapack_int lda, double* s,
    //                               double* u, lapack_int ldu, double* vt,
    //                               lapack_int ldvt )
    LAPACKE_dgesdd(/* matrix order */ LAPACK_COL_MAJOR, /* jobz */ 'A', /* m */ A.rows(),/* n */ A.cols(), /* A */ a.data(), /* LDA */ A.rows(),
                   /* s */ Sing.data(),/* U */ U.data(), /* LDU */ U.rows(), /* VT */ Vtrans.data(), /* LDVT */ Vtrans.cols());

}
#endif

