#include "operatorFloorXC.h"

#include <memory>
#include "multipoleOrbital.h"
#include "multipolePotential.h"
#include "index.h"
#include "basisExpIm.h"
#include "basisOrbitalNumerical.h"
#include "operatorTree.h"
#include "gaunts.h"
#include "densityMatrix1.h"
#include "basisChannel.h"
#include "basisMO.h"
#include "patchRadial.h"

OperatorFloorXC::OperatorFloorXC(std::string Pot, const Index* AIndex, const Index* BIndex, std::complex<double> Multiplier)
{
    // create dummy floor with norm!=0, if both in unscaled region (criterion can be improved)
    UseMatrix mat=UseMatrix::Identity(AIndex->size(),BIndex->size());
    dat=0;
    oNorm=1.;
    mat*=Multiplier;
    construct(&mat,"XC");
    if(AIndex->basisIntegrable()->isAbsorptive() or BIndex->basisIntegrable()->isAbsorptive())oNorm=0.;
}

void OperatorFloorXC::postProcess(OperatorTree *Op, std::string Pot, const std::vector<std::vector<Eigen::MatrixXcd> > &RhoIJ,
                                  const BasisOrbitalNumerical & IOrb){

    DensityMatrix1 rho(RhoIJ); // initialize the density matrix
    for(PatchRadial<OperatorFloorXC> patch(Op,rho,"full");patch.leafs.size()>0;patch=PatchRadial<OperatorFloorXC>(Op,rho,"full")){
        // multipole potential
        MultipolePotential mpPot(0,Pot,patch.aBas(),patch.bBas());
        rho.patch(*patch.aBas(),*patch.bBas(),IOrb,IOrb); // get spherical representation of density matrix for present patch
        Gaunts gaunts;

        // NOTE: the radial resolution may not be good enough for
        // the density - we may need a higher resolution to get the integrals right(?)
        for(int a=0;a<patch.aBas()->size();a++)
            for(int b=0;b<patch.bBas()->size();b++)
            {
                // set radial points a,b where to evaluate rho[li,lj,mi,mj]
                rho.point(a,b);

                for(PatchRadial<OperatorFloorXC>::LeafM &leafM: patch.leafs){
                    int mpMax=std::min(rho.imMax()+leafM.mb,rho.jmMax()+leafM.ma);
                    int mpMin=std::max(rho.imMin()+leafM.mb,rho.jmMin()+leafM.ma);
                    for(int mp=mpMin;mp<mpMax+1;mp++){
                        DensityMatrix1::M* rhoM=rho(leafM.mb-mp,leafM.ma-mp); //  rho[mi,mj]: ma=mp+mj, mb=mp+mi

                        // extend Gaunts table for all l's in leafM (if needed)
                        int dum;
                        gaunts.vals(leafM.ma,mp,std::max(leafM.laMax(),leafM.lbMax()),std::min(rhoM->jlMax()+leafM.laMax(),rhoM->jlMax()+leafM.lbMax()),dum);

                        if(rhoM->isZero())continue;

                        for (PatchRadial<OperatorFloorXC>::LeafL & leafL: leafM.leafs){

                            int lpMin=std::max(abs(mp),std::max(abs(rhoM->jlMin()-leafL.la),abs(rhoM->ilMin()-leafL.lb)));
                            int lpMax=std::min(rhoM->jlMax()+leafL.la,rhoM->jlMax()+leafL.lb);
                            for (int lp=lpMin;lp<lpMax+1;lp++){
                                std::complex<double> vL=mpPot.vals(lp)(a,b);

                                // references to lists of non-zero Gaunt factors
                                int liMin,ljMin;
                                std::vector<double> & gI=gaunts.vals(mp,-leafM.mb,lp,leafL.lb,liMin);
                                std::vector<double> & gJ=gaunts.vals(mp,-leafM.ma,lp,leafL.la,ljMin);

                                // constrained by triangular ineq and parity
                                for(int li=liMin,gi=0;li<=leafL.lb+lp;li+=2,gi++){
                                    DensityMatrix1::L* rhoML=(*rhoM)(li); // rho[mi,mj,li]
                                    std::complex<double> vLgauntI=vL*gI[gi];

                                    // constrained by triangular ineq and parity
                                    for(int lj=ljMin,gj=0;lj<=leafL.la+lp;lj+=2,gj++){
                                        std::complex<double> vLgauntIJ=vLgauntI*gJ[gj];
                                        DensityMatrix1::LL* rhoMLL=(*rhoML)(lj); // rho[mi,mj,li]
                                        for (PatchRadial<OperatorFloorXC>::LeafC & leafC: leafL.leafs)
                                            (*leafC.mat)(a,b)+=vLgauntIJ*rhoMLL->densC[leafC.cc]; // rho[mi,mj,li,lj,cc](a,b)
                                    }
                                }
                            }
                        }
                    }
                }
            }
    }
}
