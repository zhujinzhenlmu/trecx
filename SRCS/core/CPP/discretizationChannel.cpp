// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "discretizationChannel.h"

#include <iostream>
#include <fstream>

#include "timePropagator.h"
#include "discretizationFactor.h"
#include "discretizationSpectral.h"
#include "discretizationSurface.h"
#include "index.h"
#include "axis.h"

using namespace std;

