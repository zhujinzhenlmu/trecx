// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "dvrMat.h"

#include "algebra.h"
#include "basisSet.h"
using namespace std;

std::map<int,const DvrBasis*> DvrBasis::_tab;
const DvrBasis* DvrBasis::get(const BasisSet* Bas){
    const DvrBasis * bas=_tab[Bas->Labelled::label()];
    if(bas!=0)return bas;
    _tab[Bas->Labelled::label()]=new DvrBasis(*Bas);
    return _tab[Bas->Labelled::label()];
}

DvrBasis::DvrBasis(const BasisSet &Bas):orig(&Bas){
    // re-sort quadratur points such that value matrix is diagonal
    UseMatrix v,d,x,w;
    Bas.dvrRule(x,w,true);
    Bas.valDer(x,v,d);

    // permute such that values are diagonal
    vector<int> ptBas(v.cols());
    for(int k=0;k<x.size();k++){
        bool found=false;
        for(int j=0;j<v.cols();j++){
            if(abs(v(k,j).complex())>1.e-10){
                ptBas[j]=k;
                found=true;
                break;
            }
        }
        if(not found)ptBas.push_back(k);
    }

    _points.resize(x.size());
    _weights.resize(x.size());
    for(int k=0;k<x.size();k++){
        _points[k] =x.data()[ptBas[k]].real();
        _weights[k]=w.data()[ptBas[k]].real();
    }

    // diagonal of values
    _vals.assign(_points.size(),0.);
    for(int i=0;i<Bas.size();i++)_vals[i]=v(ptBas[i],i).complex();

    // k'th row of d are derivatives at old point k for all functions
    _ders.resize(_points.size());
    for(int k=0;k<_points.size();k++)
        for(int i=0;i<Bas.size();i++)
            _ders[k].push_back(d.data()[ptBas[k]+d.rows()*i]);

    // complex scaled points
    for(int k=0;k<_points.size();k++)
        _pointsComplex.push_back(Bas.complexScaled(_points[k]));
}

complex<double> DvrBasis::eta() const{return orig->eta();}

DvrMat::DvrMat(std::string Def, const DvrBasis &IBas, const DvrBasis &JBas)
{
    string def=Def;
    bool iDer=Def.find("d_")==0;
    bool jDer=(Def.length()>1 and Def.find("_d")==Def.length()-2);
    if(iDer)def=def.substr(2);
    if(jDer)def=def.substr(0,def.length()-2);

    Algebra func(def);
    if(not func.isAlgebra())ABORT(Str("not a valid function definition:")+Def+func.failures);
    if(IBas.dvrPoints()!=JBas.dvrPoints())ABORT("Bases do not share DVR grid");

    _rows=IBas.size();
    _cols=JBas.size();
    if(iDer or jDer)_data.assign(_rows*_cols,0.);

    // complex scaling for d (eta r) and d/d(eta r)
    complex<double>fac;
    if(not (iDer or jDer)) fac=IBas.eta();
    else if(iDer and jDer) fac=1./IBas.eta();
    else fac=1.;

    int kPoints=IBas.size();
    // only when both derivatives all points contribute
    if(iDer and jDer)kPoints=IBas.dvrPoints().size();
    for(int k=0,k0=0;k<kPoints;k++,k0+=_rows){
        complex<double> funcWeig=func.val(IBas.dvrPointsComplex()[k])*fac*IBas.dvrWeights()[k];

        if(not (iDer or jDer) ){
            complex<double> funcWeigJ=funcWeig*JBas.val(k);
            _data.push_back(conj(IBas.val(k))*funcWeigJ);
        }
        else {
            if(not iDer){
                for(int j=0,j0=0;j<_cols;j++,j0+=+_rows){
                    complex<double> funcWeigJ=funcWeig*JBas.ders(k)[j];
                    _data[k+j0]=conj(IBas.val(k))*funcWeigJ;
                }
            }
            else if(not jDer){
                complex<double> funcWeigJ=funcWeig*JBas.val(k);
                for(int i=0;i<_rows;i++)_data[i+k0]=conj(IBas.ders(k)[i])*funcWeigJ;
            }
            else{
                for(int j=0,ij=0;j<_cols;j++){
                    complex<double> funcWeigJ=funcWeig*JBas.ders(k)[j];
                    for(int i=0;i<_rows;i++,ij++)_data[ij]+=conj(IBas.ders(k)[i])*funcWeigJ;
                }
            }
        }
    }
}


void DvrMat::useMatrix(UseMatrix & Mat) const {
    if(_data.size()!=_rows*_cols){
        Mat=UseMatrix::Zero(_rows,_cols);
        for(int k=0,kk=0;k<_cols;k++,kk+=_rows+1)Mat.data()[kk]=_data[k];
    }
    else{
        Mat=UseMatrix::Zero(_rows,_cols);
        for (int k=0;k<Mat.size();k++)Mat.data()[k]=_data[k];
    }
}
