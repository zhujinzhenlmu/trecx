// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "mapGauge.h"
#include "discretizationSurface.h"
#include "algebra.h"
#include "pulse.h"
#include "coefficientsFloor.h"
#include "basisGrid.h"

using namespace std;


MapGauge::MapGauge(const DiscretizationSurface *S)
    :OperatorAbstract(S->mapFromParent()->name,S->idx(),S->parent->idx())
{
    if(not Algebra::isAlgebra("Rg") or not Algebra("Rg").val(0.).real()!=0)ABORT("original is not mixed gauge");
    //CAUTION: must use new map to grid, old appears to be flawed in some situations (haCC)
    mapToSurface=S->sharedFromParent();
    gridAngular=new DiscretizationGrid(S->idx(),tools::splitString("Phi.Eta",'.'),
                                       vector<unsigned int>(),vector<vector<double> >(),false);
    double phi,eta;
    gPhas=new GaugePhase(gridAngular->idx(),phi,eta,rgrid,aDotUnit,phase);
}
MapGauge::~MapGauge(){
    delete gridAngular;
    delete gPhas;
}

void MapGauge::axpy(std::complex<double> Alfa, const Coefficients &X, std::complex<double> Beta, Coefficients &Y, double Time){
    update(Time);
    apply(Alfa,X,Beta,Y);
}
void MapGauge::apply(std::complex<double> Alfa, const Coefficients &X, std::complex<double> Beta, Coefficients &Y) const {
    Coefficients * cSurf=const_cast<Coefficients*>(&mapToSurface->lhsVector());
    Coefficients * cGrid=const_cast<Coefficients*>(&gridAngular->mapFromParent()->lhsVector());
    mapToSurface->apply(Alfa,X,0.,*cSurf);
    gridAngular->mapFromParent()->apply(1.,*cSurf,0.,*cGrid);
    applyPhase(cGrid,gPhas);
    gridAngular->mapToParent()->apply(1.,*cGrid,Beta,Y);
}
void MapGauge::applyPhase(Coefficients *Vec, const GaugePhase *Grid) const {
    // recursion terminates where pPhase is defined
    if(Grid->pPhase!=0){
        *Vec*=*Grid->pPhase;
        // length gauge: angular correction
        if(Grid->pADotUnit!=0)
            for(int k=0,l=Vec->size()/2;k<Vec->size()/2;k++,l++)
                Vec->floorData()[l]+=Vec->floorData()[k]*(complex<double>(0.,*Grid->pADotUnit));
    }
    else
        for(int k=0;k<Vec->childSize();k++)
            applyPhase(Vec->child(k),Grid->child(k));
}

void MapGauge::update(double Time, const Coefficients* CurrentVec){
    vector<double> a(Pulse::current.vecA(Time)); //watch out: vecA = (Az,Ax,Ay)
    for(int k=0;k<rgrid.size();k++){
        aDotUnit[k]=rgrid[k][0]*a[1]+rgrid[k][1]*a[2]+rgrid[k][2]*a[0];
        phase[k]=exp(complex<double>(0.,aDotUnit[k]*rgrid[k][3]));
    }
}

MapGauge::GaugePhase::GaugePhase(const Index* IdxAng,double Phi, double Eta,
                                 vector<vector<double> > &Unit,deque<double> &ADotUnit,deque<complex<double> > & Phase)
    :pPhase(0),pADotUnit(0){
    double rGauge=Algebra("Rg").val(0.).real();
    if(IdxAng->axisName().find("ValDer")!=string::npos){
        vector<double> coor(4);
        coor[0]=cos(Phi)*sqrt(1-Eta*Eta);
        coor[1]=sin(Phi)*sqrt(1-Eta*Eta);
        coor[2]=Eta;
        coor[3]=min(IdxAng->basisGrid()->mesh()[0],rGauge);
        int k=0;
        while(k<Unit.size() and Unit[k]!=coor)k++;
        if(k==Unit.size()){
            Unit.push_back(coor);
            ADotUnit.push_back(1.);
            Phase.push_back(1.);
        }
        pPhase=&Phase[k];
        if(not IdxAng->hasFloor())ABORT("presently we assume that ValDer is floor level");

        // length-gauge region: angular correction
        pADotUnit=0;
        if(rGauge-IdxAng->basisGrid()->mesh()[0]>-1.e-10)pADotUnit=&ADotUnit[k];

        return;
    }

    for(int k=0;k<IdxAng->childSize();k++){
        if(IdxAng->axisName().find("Phi")==0){
            if(IdxAng->axisName().length()!=3)
                ABORT("illegal phi-coordinate (for now, only single polar coordinats for MapGauge):"+IdxAng->axisName());
            Phi=IdxAng->basisGrid()->mesh()[k];
        }
        if(IdxAng->axisName()=="Eta")Eta=IdxAng->basisGrid()->mesh()[k];
        childAdd(new GaugePhase(IdxAng->child(k),Phi,Eta,Unit,ADotUnit,Phase));
    }
}
