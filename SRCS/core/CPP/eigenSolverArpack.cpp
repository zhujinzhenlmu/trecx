// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "eigenSolverArpack.h"

#include "index.h"
#include "inverse.h"
#include "coefficients.h"

void EigenSolverArpack::_compute(){
    // Checks
    if(_computeLeftVectors) ABORT("Left vectors currently not supported");

    ArpackWrapper w(_op, _tolerance, _maxIterations, _shift);
    std::vector<std::vector<std::complex<double> > > rightVectors;
    w.eigen(
        _eigenvalues,
        rightVectors,
        _numberEigenv,         
        _sort
    );

    for(unsigned int k=0;k<_eigenvalues.size();k++){
        _eigenvalues[k]=_eigenvalues[k]+w.shift(); // Undo shift
    }

    Coefficients tmp(_op->iIndex);
    std::vector<std::complex<double>* > pTmp;
    tmp.pointerToC(pTmp);

    for(unsigned int k=0;k<rightVectors.size();k++){
        for(unsigned int i=0;i<rightVectors[k].size();i++) *pTmp[i]=rightVectors[k][i];
        _rightVectors.push_back(new Coefficients(tmp));
    }
}

EigenSolverArpack::ArpackWrapper::ArpackWrapper(const OperatorAbstract* Op, double Tolerance, unsigned int MaxIterations, std::complex<double> Shift):
    Arpack(Tolerance, MaxIterations), op(Op), x(Op->iIndex), tmp(Op->iIndex), y(Op->iIndex), _shift(Shift){
    
    if(op->iIndex!=op->jIndex) ABORT("Only supported for square matrices");
  
    tmp.setToZero();
    y.setToZero();

    x.pointerToC(pX);
    y.pointerToC(pY);

    lvec = op->iIndex->sizeStored();
}

void EigenSolverArpack::ArpackWrapper::apply(const std::complex<double>* X, std::complex<double>* Y){
    // Copy data
    for(unsigned int i=0; i<pX.size(); i++) *pX[i]=X[i];
    
    // y = S^-1 A x - lambda * x
    x.makeContinuous();
    op->apply(1.,x,0.,tmp);
    tmp.makeContinuous();
    op->iIndex->inverseOverlap()->apply(1.,tmp,0.,y);
    y.axpy(-_shift, &x); 

    // Copy data back
    for(unsigned int i=0; i<pY.size(); i++) Y[i]=*pY[i];
}
