#include "basisAssocLeg.h"


BasisAssocLeg::BasisAssocLeg(std::string Def):BasisIntegrable(-1.,1.)
{
    if(Def.find("AssociatedLegendre:")!=0)DEVABORT("does not define AssociatedLegendre, is "+Def);
    std::vector<std::string> m_size(tools::splitString(Def.substr(std::string("AssociatedLegendre:").length()),','));
    _mAbs=tools::string_to_int(m_size[0]);
    _size=tools::string_to_int(m_size[1]);

    // temporarily, until established, test
    test();
}

// temporary for debug
#include "basisSetDef.h"
#include "basisSet.h"
#include "eigenTools.h"
#include "qtEigenDense.h"
void BasisAssocLeg::test(){
    // construct old style
    std::vector<std::complex<double>> x,valOld,derOld,valNew,derNew;
    BasisSet bas(BasisSetDef(size(),-1.,2.,"assocLegendre",true,true,true,Coordinate("Eta"),{0,-1},ComplexScaling(),false,std::vector<double>(1,double(_mAbs))));
    for(int k=0;k<11;k++)x.push_back(std::complex<double>(-1.+0.2*k,0.));
    bas.valDer(x,valOld,derOld,true);
    valDer(x,valNew,derNew);
    Eigen::MatrixXcd matNew=Eigen::Map<Eigen::MatrixXcd>(valNew.data(),x.size(),size());
    Eigen::MatrixXcd matOld=Eigen::Map<Eigen::MatrixXcd>(valOld.data(),x.size(),size());
    if(not (matNew-matOld).isZero(1.e-12)){
        Sstr+str()+"\n"+EigenTools::str(matNew,2)+Sendl;
        Sstr+bas.str()+"\n"+EigenTools::str(matOld,2)+Sendl;
        ABORT("BasisAssocLeg deviates from old BasisSet");
    }
}

void BasisAssocLeg::valDer(const std::vector<std::complex<double> > &X, std::vector<std::complex<double> > &Val, std::vector<std::complex<double> > &Der, bool ZeroOutside) const{
    OrthogonalNassocLegendre p(_mAbs);
    Val.resize(X.size()*size());
    Der.resize(X.size()*size());
    for(int i=0;i<X.size();i++){
        std::vector<double> v,d;
        p.valDer(size(),X[i].real(),v,d);
        for(int k=0;k<size();k++){
            Val[i+X.size()*k]=v[k];
            Der[i+X.size()*k]=d[k];
        }
    }
}

void BasisAssocLeg::quadRule(int N, std::vector<double> &QuadX, std::vector<double> &QuadW) const{
    OrthogonalLegendre().quadrature(N,QuadX,QuadW);
}
