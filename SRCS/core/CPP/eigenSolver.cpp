// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "eigenSolver.h"
#include "operatorTree.h"
#include "index.h"
#include "coefficients.h"
#include "str.h"
#include "printOutput.h"
#include "inverse.h"
#include "operatorAbstract.h"
#include "parallel.h"
#include "parallelOperator.h"
#include "mpiWrapper.h"

#include <FullPivLU.h>
#include "eigenTools.h"
#include "timer.h"

using namespace std;

static double epsilon=1.e-10;

EigenSolver::EigenSolver(double Emin, double Emax, int Nmax, bool RightVectors, bool DualVectors, bool ExcludeRange, string Method)
    :EigenSolverAbstract(),_method(Method)
{
    _numberEigenv=Nmax;
    selectEigenvalues(Emin,Emax,ExcludeRange);
    computeLeftVectors(DualVectors);
    computeRightVectors(RightVectors);
}


void EigenSolver::_compute()
{

    // descend in block-diagonal part
    const OperatorTree* OpTree=dynamic_cast<const OperatorTree*>(_op);
    const OperatorTree* OvrTree=dynamic_cast<const OperatorTree*>(_ovr);

    // re-distribute by diagonal blocks
    ParallelOperator parH(OpTree),parO(OvrTree);
    ParallelOperator::SavedHost savedH(&parH),savedO(&parO);
    parH.reDistribute(ParallelOperator::DiagonalBlockHost(OpTree));
    parO.reDistribute(ParallelOperator::DiagonalBlockHost(OvrTree));

    // diagonalize block-wise

    _computeBlock(OpTree,OvrTree);

    // restore original distribution
    parH.reDistribute(savedH);
    parO.reDistribute(savedO);

    if(MPIwrapper::Size()>1 and ParallelOperator::getHost(_op)==ParallelOperator::distributed){
        for(EigenSolver* leaf=firstLeaf();leaf!=0;leaf=leaf->nextLeaf()){
            Parallel::gatherAllEigen(leaf->_op->iIndex,leaf->_eigenvalues,leaf->_rightVectors,leaf->_dualVectors);
        }
    }
}

void EigenSolver::_computeBlock(const OperatorTree* OpTree, const OperatorAbstract *Ov)
{

    _ovr=Ov;
    _op=OpTree;

    const OperatorTree* ovTree=dynamic_cast<const OperatorTree*>(Ov);
    if(ovTree!=0 and OpTree->isBlockDiagonal() and ovTree->isBlockDiagonal()){
        for(int k=0;k<OpTree->childSize();k++){

            for (int l=0;l<k;l++){
                if(OpTree->child(k)->iIndex==OpTree->child(l)->iIndex)
                    DEVABORT("not implemented for equal index blocks");
            }

            childAdd(new EigenSolver(_eMin,_eMax,_numberEigenv,_computeRightVectors,_computeLeftVectors,_excludeRange,_method));
            childBack()->_computeBlock(OpTree->child(k),OpTree->child(k)->iIndex->overlap());
        }
    }
    else {
        if(parent()==0)
            PrintOutput::DEVmessage(Str("Eigensolver for problem size"," ")+OpTree->iIndex->sizeStored()+"("+SEP("")+autoMethod(_method)+")");
        else if(nSibling()==0){
            vector<int>siz;
            for(const OperatorTree*o=OpTree;o!=0;o=o->nodeRight())siz.push_back(o->iIndex->sizeStored());
        }

        int host = ParallelOperator::getHost(OpTree);

        if(host==MPIwrapper::Rank() or host==ParallelOperator::all)
        {
            if(OpTree->iIndex->sizeStored()>1 and autoMethod(_method)=="Arpack"){

                //HACK preliminary, for use in Benchmark, needs cleanup
                string which="SmallReal";
                int nVec=min(_numberEigenv,OpTree->iIndex->sizeStored()-1);
                if(_eMin>0. and _eMax>=DBL_MAX/4.)which="LargeAbs";

                Arp a(OpTree);
                vector<complex<double> > eval;
                vector<Coefficients*> evec;

                bool crit=Coefficients::timeCritical;
                Coefficients::timeCritical=false;
                a.eigen(eval,evec,nVec,which,false);
                Coefficients::timeCritical=crit;

                // select desired energy range
                for(int k=0;k<eval.size();k++){
                    if(_eMin<=eval[k].real() and eval[k].real()<=_eMax){
                        _eigenvalues.push_back(eval[k]);
                        if(evec.size()>k)_rightVectors.push_back(evec[k]);
                    }
                    else
                        if(evec.size()>k)delete evec[k];
                }

                // determine dual basis
                // I am not too experienced with the interface, maybe this can be done in a much better way
                if(_rightVectors.size() > 0) {
                    // R = matrix with right eigenvectors in columns
                    Eigen::MatrixXcd R(OpTree->iIndex->sizeCompute(), _rightVectors.size());
                    for(int k=0;k<_rightVectors.size();k++){
                        auto leaf = _rightVectors[k]->firstLeaf();
                        std::size_t row = 0;
                        while(leaf != nullptr) {
                            for(std::size_t n = 0; n < leaf->size(); ++n) {
                                R(row, k) = leaf->data()[n];
                                ++row;
                            }
                            leaf = leaf->nextLeaf();
                        }
                    }
                    Eigen::MatrixXcd L = R.adjoint(); // L = matrix of dual eigenvectors in rows
                    Eigen::MatrixXcd D = L * R; // D has (N_eig)^2 dimensions, so matrix operation should be cheap
                    // for symmetric eigenproblem, we should be done here
                    if(abs((D - Eigen::MatrixXcd::Identity(D.rows(), D.cols())).norm()) > 1.e-12) {
                        // if not symmetric, calculate D^-1 and obtain dual eigenvectors as rows from D^-1 L, as
                        // D^-1 * L * R = D^-1 D = 1
                        L = D.inverse() * L; // assumes that matrix is diagonalizable
                    }

                    for(int k = 0; k < _rightVectors.size(); ++k) {
                        _dualVectors.push_back(new Coefficients(OpTree->iIndex));
                        auto leaf = _dualVectors.back()->firstLeaf();
                        std::size_t count = 0;
                        while(leaf != nullptr) {
                            for(std::size_t n = 0; n < leaf->size(); ++n) {
                                leaf->data()[n] = L(k, count);
                                ++count;
                            }
                            leaf = leaf->nextLeaf();
                        }
                    }

                }
            }
            else if (OpTree->iIndex->sizeStored()<=1 or autoMethod(_method)=="Lapack"){

                if(OpTree->iIndex->size()>1000){
                    PrintOutput::warning(Sstr+"Large block"+OpTree->iIndex->size()+"in eigenproblem: "+OpTree->name);
                    if(OpTree->iIndex->size()>4000)ABORT("too large for full diagonlization");
                }
                UseMatrix mat,ovr,val,rvec,lvec;
                OpTree->matrixAdd(1.,mat);
                Ov->matrixAdd(1.,ovr);

                if(OpTree->iIndex->sizeStored()<1000)PrintOutput::outputLevel("low");
                if(not (_computeLeftVectors or _computeRightVectors))
                {
                    mat.eigenBlock(val,rvec,ovr,false);
                    for(int k=0;k<val.size();k++)
                        if(_eMin<=val.data()[k].real() and val.data()[k].real()<=_eMax)
                            _eigenvalues.push_back(val.data()[k]);

                }
                else
                {   // do not assume simple relation between left and right eigenvectors
                    mat.eigenBlock(val,ovr,true,rvec,true,lvec);
                    expandInto(false,_rightVectors,_eigenvalues,val.data(),rvec,_eMin,_eMax,_excludeRange);
                    expandInto(true, _dualVectors, _eigenvalues,val.data(),lvec,_eMin,_eMax,_excludeRange);

                }
                PrintOutput::outputLevel("full");
            }
            else ABORT("unknown EigenSolver method: "+_method);

            if(_computeRightVectors){
                orthonormalizeDegenerate(1.e-2,_eigenvalues,_dualVectors,_rightVectors);
                if(not verify())PrintOutput::DEVwarning("eigensolving failed");
            }
        }

    }
    MPIwrapper::Barrier();
}

void EigenSolver::collect(){
    if(_eigenvalues.size()>0)return; // already available on present level

    int dep=_op->iIndex->depth(); // determines level on which to collect (usually top)
    for(EigenSolver * leaf=firstLeaf();leaf!=0;leaf=leaf->nodeNext(this)){
        for(int k=0;k<leaf->_eigenvalues.size();k++){
            _eigenvalues.push_back(leaf->_eigenvalues[k]);

            if(_computeRightVectors){
                vector<unsigned int>lidx(leaf->_op->iIndex->index()); // multi-index of compute block
                lidx.erase(lidx.begin(),lidx.begin()+dep);            // remove top part of multi-index

                _rightVectors.push_back(new Coefficients(_op->iIndex));
                *_rightVectors.back()->nodeAt(lidx)=*leaf->_rightVectors[k];
                _dualVectors.push_back(new Coefficients(_op->iIndex));
                *_dualVectors.back()->nodeAt(lidx)=*leaf->_dualVectors[k];
            }
        }
    }
    phaseFix();
}

std::vector<Coefficients*> EigenSolver::rightVectors() const {
    const_cast<EigenSolver*>(this)->collect();
    return _rightVectors;
}
std::vector<Coefficients*> EigenSolver::dualVectors() const {
    const_cast<EigenSolver*>(this)->collect();
    return  _dualVectors;
}


vector<complex<double> > EigenSolver::eigenvalues() const {

    if(_eigenvalues.size()!=0)return _eigenvalues;

    vector<complex<double> > res;
    for(int k=0;k<childSize();k++){
        vector<complex<double> > eval(child(k)->eigenvalues());
        res.insert(res.end(),eval.begin(),eval.end());
    }
    return res;

}

void EigenSolver::orthonormalize(const vector<Coefficients*> &Dual,const vector<Coefficients*> &Rvec){
    if(Dual.size()!=Rvec.size())ABORT("numbers dual eigenvectors not equal right eigenvectors");
    for(int i=0;i<Dual.size();i++){
        for(int j=0;j<i;j++){
            complex<double> norm = Dual[j]->innerProduct(Rvec[j], true);
            if(abs(norm)<1.e-12) ABORT("singular");
            complex<double>oij=Dual[j]->innerProduct(Rvec[i],true) / norm;
            Rvec[i]->axpy(-oij,Rvec[j]);
        }
        complex<double>nrm=sqrt(Dual[i]->innerProduct(Rvec[i],true));
        if(abs(nrm)<1.e-12)ABORT("singular");
        std::complex<double> phaseAtMax=Rvec[i]->cMaxNorm();
        phaseAtMax/=std::abs(phaseAtMax);
        Rvec[i]->scale(std::conj(phaseAtMax)/std::abs(nrm));
        nrm=Dual[i]->innerProduct(Rvec[i],true);
        Dual[i]->scale(1./nrm);
    }
}

void EigenSolver::orthonormalizeDegenerate(double Eps,const vector<complex<double> > & Eval,
                                           const vector<Coefficients*> & Dual,const vector<Coefficients*> & Rvec)
{
    // orthogonalize on near-degenerate subspaces, generate duals in the process
    vector<bool> done(Eval.size(),false);
    for(int k=0;k<Eval.size();k++){
        if(done[k])continue;
        vector<Coefficients*> dual,rvec;
        for(int l=k;l<Eval.size();l++){
            if(done[l])continue;
            if(abs(Eval[k]-Eval[l])<Eps){
                done[l]=true;
                rvec.push_back(Rvec[l]);
                dual.push_back(Dual[l]);
            }
        }
        orthonormalize(dual,rvec);
    }
}

void EigenSolver::expandInto(bool Dual, std::vector<Coefficients *> &CVec, std::vector<std::complex<double> > &Val,
                             const std::complex<double> *PVal, UseMatrix & Vec, double Emin, double Emax,
                             bool ExcludeRange)
{
    // list of selected eigenvalues
    Val.clear();
    vector<int> cols;
    for(int k=0;k<Vec.cols();k++)
        if((Emin<=PVal[k].real() and PVal[k].real()<=Emax) != ExcludeRange){
            cols.push_back(k);
            Val.push_back(PVal[k]);
        }

    // expand selected values
    _op->iIndex->unGlobal(Vec,Dual,CVec,cols);

}

complex<double> EigenSolver::Arp::shift=0;

EigenSolver::Arp::Arp(const OperatorAbstract *O)
    :Arpack(1.e-9,5000,true),o(O),x(Coefficients(O->jIndex)),y(Coefficients(O->iIndex)),tmp(Coefficients(O->iIndex)){
    x.pointerToC(pX);
    y.pointerToC(pY);
    tmp.setToZero(); // make sure =0
    y.setToZero(); // make sure Y=0
    lvec=y.size();
}

void EigenSolver::Arp::eigen(vector<std::complex<double> > &Eval, vector<Coefficients *> &Rvec,
                             unsigned int Nvec, const string &Which, bool Restart){

    complex<double> defaultShift=shift;
    if(Restart){
        // initialize with starting vector
        if(Rvec.size()!=1)ABORT("for restarting, supply exactly one Rvec as starting vector");
        y=*Rvec[0];
        //delete Rvec[0];
        Rvec.clear();
        rvec.resize(lvec);
        for(unsigned int i=0;i<lvec;i++)rvec[i]=*(pY[i]);
    }
    else if(Rvec.size()>0)
        ABORT("must enter eigen with empty Rvec, unless Restart");

    if(Which=="SmallAbs"){
        if(Eval.size()!=1)ABORT("for Which=SmallAbs provide guess energy value in Eval");
        shift=Eval[0];
    }
    else if(Which=="SmallReal"){
        // in FEM, there may be 0-values due to projection
        shift=100.; //HACK
    }

    // let ArpackFunction compute the vectors
    Arpack::eigen(Nvec,Which,Restart,true);

    // move into Rvec
    Eval.resize(Nvec);
    for(unsigned int k=0;k<Nvec;k++){
        Eval[k]=eval[k]+EigenSolver::Arp::shift;

        for(unsigned int i=0;i<lvec;i++)*(pY[i])=rvec[i+k*lvec];
        Rvec.push_back(new Coefficients(y));
    }

    // reset in case is had been changed
    shift=defaultShift;
}


TIMER(eigen0,)
TIMER(eigen1,)
TIMER(eigen2,)
TIMER(eigen3,)
TIMER(eigen4,)
void EigenSolver::Arp::apply(const std::complex<double> *X, std::complex<double> *Y) {

    // copy to Coefficients
    START(eigen0);
    for (unsigned int i=0;i<pX.size();i++)*(pX[i])=*(X+i);
    STOP(eigen0);
    START(eigen1);

    // y=S^-1 Op x - shift*x

    x.makeContinuous();
    STOP(eigen1);
    START(eigen2);
    o->apply(1.,x,0.,tmp);
    STOP(eigen2);
    START(eigen3);
    tmp.makeContinuous();
    STOP(eigen3);
    START(eigen4);
    o->iIndex->inverseOverlap()->apply(1.,tmp,0.,y);
    STOP(eigen4);
    y.axpy(-shift,&x);

    // copy back and reset y to zero
    for (unsigned int i=0;i<pX.size();i++)*(Y+i)=*(pY[i]);
}

