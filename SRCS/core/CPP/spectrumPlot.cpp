#include "spectrumPlot.h"

#include "mpiWrapper.h"
#include "asciiFile.h"
#include "readInput.h"
#include "printOutput.h"
#include "index.h"
#include "coefficients.h"
#include "operatorMap.h"
#include "plot.h"

#include "spectrumPz.h"
#include "plotKind.h"
#include "vinayData.h"
#include "integrateTree.h"
#include "algebra.h"
#include "spectrumLinCom.h"
#include "tsurffSource.h"
#include "spectrumAngle.h"
using namespace std;
//using namespace tSurffTools;

SpectrumPlot::~SpectrumPlot() {
    delete coefs, toBas, basIdx;
}

SpectrumPlot::SpectrumPlot()
    : runPropagationTime(0.), integrationTime(0.), gridIdx(nullptr), basIdx(0), coefs(0),
      outputDir(""), plotWhat(""), overwrite(true), specFileHeader({""}) {}

SpectrumPlot::SpectrumPlot(string OutputDir, string PlotWhat, bool Overwrite, std::vector<string> InputFlags)
    : runPropagationTime(0.), integrationTime(0.), gridIdx(0), basIdx(0), coefs(0),
      outputDir(OutputDir), plotWhat(PlotWhat), overwrite(Overwrite), specFileHeader(InputFlags) {}


void SpectrumPlot::read(ReadInput &Inp, std::string &What, bool &Overwrite, std::string &AmplFiles) {

    Inp.read("Spectrum", "plot", What, "",
             "(mutable) read from ampl-file and plot, default depends on coordinates:"
             + string(", grid[file]...draw the plot info from file")
             + string(", total, partial, partialIon")
             + string(", cone...integrate cone[phiC,etaC,gamC] axis phiC,etaC, opening gamC")
             + string(", zone...integrate zone[the0,the1] and phi in [0,2pi]")
             + string(", intPhi...2d integrated over Phi")
             + string(", cutPhi[n]...n cuts at phi angles")
             + string(", cutEta[n]...n cuts at eta=cos(theta)")
             + string(", sumZ...sum over Z (for XZ or YZ)")
             + string(", sumY...sum over Y (for XY or YZ)")
             + string(", sumX...sum over X (for XY or XZ)")
             + string(", partialRn1...sum over Rn2, partial waves")
             + string(", sumRn1...sum over Rn1")
             + string(", sumRn2...sum over Rn2")
             + string(", kZ...z-momentum")
             + string(", lPartial...partial waves summed over m")
             + string(", JAD[eMin:eMax{:nE}]...joint angular distribution")
             + string(", intK...intK")

             + string(", user...read plot from Plot_user in input")
             + string(", default (coordinate-dependent)")
             , 1, "plot");

    PlotKind kind("user", Inp);
    if (What == "user") {
        if (kind.axes().size() == 0)
            ABORT("Spectrum:plot=user, need Plot_user: axes,usage,points,lowerBound,upperBound");
    }


    Inp.read("SpectrumPlot", "noOverwrite", Overwrite, ReadInput::flagOnly, "suppress overwriting of spectrum", 1, "noOvr");
    Inp.read("Spectrum", "amplitude", AmplFiles, "all", "which amplituded file to sum, specify as Rn1.Rn2,Rn2.Rn1 etc.", 1, "ampFiles");
    Overwrite = not Overwrite;

}

void SpectrumPlot::allRegions(string OutputDir, ReadInput &Inp) {
    // simplified version for at most double ionization
    std::vector<std::string> ax = TsurffSource::unboundAxes(Inp);
    for (std::string a : ax) {
        std::string dir = OutputDir + "S_" + a + "/";
        if (folder::exists(dir + "ampl")) {
            SpectrumPlot plt(dir, Inp);
            if (plt.what() == "total") {
                plt.amplFromFile(dir + "ampl");
                plt.plot();
            } else
                PrintOutput::warning("for now, no single-particle spectra for Spectrum:plot=" + plt.what());
        }
    }
    SpectrumPlot plt(OutputDir, Inp);
    plt.plot(Inp);
}

SpectrumPlot::SpectrumPlot(string OutputDir, ReadInput &Inp)
    : SpectrumPlot() {
    outputDir = OutputDir;
    // strip ampl, if present
    if (OutputDir.find("/ampl_") != string::npos or
            OutputDir.rfind("/ampl") + 5 == OutputDir.length())
        outputDir = OutputDir.substr(0, OutputDir.rfind("/ampl"));
    std::string files;
    read(Inp, plotWhat, overwrite, files);
}
void SpectrumPlot::addIntegratedPlot(string Definition, const Coefficients *Ampl, string SpecFile, double Scale, std::string Tag) {
    if (Ampl == 0)ABORT("must specify Ampl-file");
    basFromGrid(*Ampl);
    Coefficients basAmp(toBas->iIndex);
    toBas->apply(1., *Ampl, 0., basAmp);

    // get differential spectrum, integrate over subregion, add to plot
    const PlotKind *p;
    if (Definition.find("cone") == 0)
        p = PlotKind::definePlot("partial", basAmp.idx()->hierarchy());
    else if (Definition.find("zone") == 0)
        p = PlotKind::definePlot("lPartial", basAmp.idx()->hierarchy());
    else
        ABORT("not defined for " + Definition);

    Plot partial(basAmp.idx(), p);
    partial.sum(basAmp, SpecFile);

    IntegrateTree intRegion(Definition, partial.plot(), true);
    if (plt.isEmpty()) {
        plotIdx.reset(new Index(*intRegion.integralIdx()));
        plt = Plot(plotIdx.get(), PlotKind::definePlot("total", plotIdx->hierarchy()));
    }
    Coefficients region(plotIdx.get());
    region.treeOrderStorage();
    region = intRegion.integrate(&partial.plot());
    for (int k = 0; k < region.size(); k++)region.orderedData()[k] = sqrt(region.anyData()[k]); // will be squared again

    plt.append(Tag != ""); // if a tag is specified, append to plot file
    plt.sum(region, SpecFile, Scale, Tag);
}


// should largely got into Plot
void SpectrumPlot::addPlot(const Coefficients *Ampl, std::string &SpecFile, double Scale, std::string Tag) {
    if (plotWhat.find("cone") == 0 or plotWhat.find("zone") == 0) {
        addIntegratedPlot(plotWhat, Ampl, SpecFile, Scale);
        return;
    }

    LOG_PUSH("addPlot");
    if (Ampl == 0)ABORT("must specify Ampl-file");
    basFromGrid(*Ampl);
    Coefficients basAmp(toBas->iIndex);
    toBas->apply(1., *Ampl, 0., basAmp);
    // try to get rid of the cases...
    if (plotWhat.find("kZ") == 0) {
        // from polar coordinates, this requires an extra transformation
        SpectrumPz specPz(basAmp.idx());
        Coefficients specKz(specPz.compute(&basAmp));
        if (plt.isEmpty()) {
            plt = Plot(specKz.idx(), PlotKind::definePlot("kZ", specKz.idx()->hierarchy()));
            plt.append(Tag != "");
        }
        plt.sum(specKz, SpecFile, Scale, Tag);
    }  else {
        // NOTE: evenutally, all plot options should included into this basic format
        // except those that involve coordinate transformations (e.g. kZ for polar to cartesian)
        if (plotWhat == "" or plotWhat == "DEFAULT")plotWhat = PlotKind::defaultKind(basAmp.idx()->coordinates());
        const PlotKind *p = PlotKind::definePlot(plotWhat, basAmp.idx()->hierarchy());
        if (plt.isEmpty()) {
            plt = Plot(basAmp.idx(), p);
            plt.append(Tag != "");
        }
        plt.sum(basAmp, SpecFile, Scale, Tag);
    }
    PrintOutput::lineItem("Integral of Spectrum", plt.integral());
    PrintOutput::newLine();
    LOG_POP();
}

string SpectrumPlot::strPlotKind() const {
    if (plotWhat.find("[") == string::npos)return plotWhat;
    string kind = plotWhat.substr(0, plotWhat.find("["));
    if (kind == "cone" or kind == "zone") {
        vector<string>ang = tools::splitString(tools::stringInBetween(plotWhat, "[", "]"), ',');
        for (string a : ang)kind += "_" + tools::cropString(SpectrumLinCom::strFractionOfPi(Algebra::constantValue(a)));
        return kind;
    } else if (kind == "JAD")
        return PlotJAD(plotWhat).strDef();
    else
        return plotWhat.substr(0, plotWhat.find("["));
}

static bool lessIndex(const std::vector<unsigned int> &Idx, const std::vector<unsigned int> &Jdx) {
    if (Idx.size() != Jdx.size())DEVABORT("only for equal length indices");
    for (int k = 0; k < Idx.size(); k++) {
        if (Idx[k] < Jdx[k])return true;
        if (Idx[k] > Jdx[k])return false;
    }
    return false;
}

void SpectrumPlot::write(string ExtName) {

    if (outputDir.rfind("/") != outputDir.length() - 1)outputDir += "/";
    string specFile = outputDir + "spec_";
    if (ExtName != "")specFile += "_" + ExtName;
    else           specFile += strPlotKind();

    if (abs(integrationTime - runPropagationTime) > runPropagationTime * 1.e-2)
        specFile += "_tMax=" + tools::str(integrationTime, 4);
    if (not overwrite)specFile = tools::newFile(specFile);

    if (MPIwrapper::isMaster())plt.write(specFile, specFileHeader, overwrite);
    plt.clear();
    PrintOutput::message("   spectra file " + specFile);
}

/// 1<->2 symmetrize Coefficients
/// assuming hiearchy containing Eta1,Eta2,Phi1,Phi2 with k1/k2 floors
#include "basisGrid.h"
static void symmetrizeHelium(Coefficients &C) {
    std::vector<std::string> hier = tools::splitString(C.root()->idx()->hierarchy(), '.');

    std::vector<int> x12;
    for (int k = 0; k < hier.size(); k++)x12.push_back(k);
    // exchange postions of angular qn in hierarchy
    std::swap(x12[std::find(hier.begin(), hier.end(), "Eta1") - hier.begin()]
              , x12[std::find(hier.begin(), hier.end(), "Eta2") - hier.begin()]);
    std::swap(x12[std::find(hier.begin(), hier.end(), "Phi1") - hier.begin()]
              , x12[std::find(hier.begin(), hier.end(), "Phi2") - hier.begin()]);

    // loop through all patches
    for (Coefficients *f = C.firstLeaf(); f != 0; f = f->nextLeaf()) {
        // quantum numbers of present patch
        std::vector<double> qn;
        const Coefficients *p = f;
        while (p->parent() != 0) {
            qn.insert(qn.begin(), p->idx()->physical());
            p = p->parent();
        }

        // angular exchanged patch
        for (int k = 0; k < qn.size(); k++) {
            p = p->descend();
            // find matching physical parameter
            while (p and p->idx()->physical() != qn[x12[k]]) {
                p = p->rightSibling();
            }
            if (not p)DEVABORT("no matching exchanged branch");
        }

        // symmetrize (add the transposed)
        // as we add into the same memory location, we need to make sure to add only once
        if (not lessIndex(f->idx()->index(), p->idx()->index())) {
            bool equalIdx = f->index() == p->index();
            int nK = f->idx()->childSize();
            if (f->idx()->basisGrid()->mesh() != p->idx()->descend()->basisGrid()->mesh())DEVABORT("unequal k-grids");
            for (int k1 = 0; k1 < nK; k1++) {
                int k2Lim = equalIdx ? k1 + 1 : nK;
                for (int k2 = 0; k2 < k2Lim; k2++) {
                    int kk = k1 * nK + k2;
                    int kx = k2 * nK + k1;
                    p->floorData()[kx] = f->floorData()[kk] += p->floorData()[kx];
                }
            }

        }
    }
}

void SpectrumPlot::plot(ReadInput &Inp) {

    std::string what, amplFiles;
    bool over;
    read(Inp, what, over, amplFiles);

    // test for all amplitude region
    std::string region;
    region = TsurffSource::nextRegion(Inp, region);
    if (not region.find("unbound region"))ABORT("cannot plot, not all paths computed, region=" + region);
    std::vector<std::string> ax(tools::splitString(tools::cropString(region.substr(region.find(":") + 1)), ' '));
    Coefficients amplSum;
    do {
        std::string subD = Inp.outputTopDir() + "S_" + ax[0];
        for (int k = 1; k < ax.size(); k++)subD += "." + ax[k];
        if (amplFiles == "all" or amplFiles.find(subD.substr(subD.rfind("/S_") + 3)) != string::npos) {
            // replace with addAmplFromFile(...)
            amplFromFile(subD + "/ampl");
            if (not amplSum.idx())amplSum.reset(coefs->idx());

            if (not amplSum.idx()->treeEquivalent(coefs->idx()))
                ABORT("indices on ampl files differ - cannot add up");
            amplSum += *coefs;
            PrintOutput::message("added amplitude file " + subD + "/ampl");

        } else
            PrintOutput::message(" skip amplitude file " + subD + "/ampl");

    } while (not TsurffSource::readSymmetry12(Inp) and std::next_permutation(ax.begin(), ax.end()));
    if (TsurffSource::readSymmetry12(Inp))symmetrizeHelium(amplSum);
    if (amplSum.idx() == 0)ABORT("no amplitude file found of " + amplFiles);
    *coefs = amplSum;
    if(ReadInput::main.flag("DEBUGWriteAmpl", "DEBUGWriteAmpl")){
        std::ofstream amplTest((ReadInput::main.outputTopDir() + "ampl-test").c_str(),(ios_base::openmode) ios::beg | ios::binary);
        coefs->write(amplTest,false);
        return;
    }
    vector<string> specFileHeader;
    setInfo(specFileHeader);
    if (plotWhat == "JAD" or plotWhat.find("antiE") == 0 or plotWhat == "intK" or plotWhat == "B2BSBS") {
        SpectrumAngle *specAngle = new SpectrumAngle(*coefs, Inp, plotWhat, specFileHeader);
        specAngle->run();
        return;
    }
    plot();

}

/// if no reference amplitude set, use Ampl for reference
void SpectrumPlot::plot(const Coefficients *Ampl, string ExtName) {

    string specFile = outputDir + "/spec";
    if (not overwrite)specFile = tools::newFile(specFile);

    if (Ampl != 0)addPlot(Ampl, specFile);
    else       addPlot(coefs, specFile);
    write();

}

const Coefficients *SpectrumPlot::amplFromFile(string AmplFile) {
    if (AmplFile.find("VinayData") != string::npos) {
        VinayData vind(AmplFile);
        gridIdx = vind.idx();
        basFromGrid(*vind.ampl(), 1);
    } else
        basFromGrid(Coefficients(AmplFile, gridIdx));

    return coefs;
}

void SpectrumPlot::basFromGrid(const Coefficients &Coefs, int Deflate) {
    if (coefs != 0) {
        if (not gridIdx->treeEquivalent(Coefs.idx())) {
            DEVABORT(Sstr + "previously set gridIdx does not match Coefs.idx()" + Index::failureCompatible
                     + "\ngrid\n" + gridIdx->str() + "\ncoefs\n" + Coefs.idx()->str()
                     + gridIdx->hierarchy() + Coefs.idx()->hierarchy());
        }
    } else {
        gridIdx = Coefs.idx();
        if (Deflate != 1)PrintOutput::DEVwarning(Str("basis deflated from grid size by factor ") + Deflate);
        basIdx = gridIdx->toIndexBasis(vector<int>(gridIdx->height(), Deflate)); // grids have been bloated for integration
        toBas = new OperatorMap(basIdx, gridIdx);
        coefs = new Coefficients(gridIdx);
    }
    *coefs = Coefs;
}
