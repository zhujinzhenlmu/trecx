﻿// For dealing with the population projected to eigenstates, SI and DI, can be ingnored when merging

#include "population.h"
#include "readInput.h"
#include "timePropagatorOutput.h"
#include "coefficients.h"
#include "printOutput.h"
#include "derivativeFlat.h"
#include "wavefunction.h"
#include "index.h"
#include "indexProd.h"
#include "coefficientsPermute.h"
#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

using namespace std;
using namespace boost::filesystem;
using namespace boost;


Population::Population(ReadInput &Inpc) {
    string tensorStates, lshape;
    Inpc.read("Population", "statesNum", populationN, "0", "The number of states that are used for population"
             );
    Inpc.read("Population", "tensorStates", tensorStates, "", "The states be produced for the DI in SI"
             );
    Inpc.read("Population", "Lshape", lshape, "", "The Lshape produced for writting tensors"
             );
    Inpc.read("Population", "MConstrain", MConstrain, "0", "The MConstrain produced for writting tensors"
             );
    if (tensorStates != "") {
        vector<string> tensorCouples = tools::splitString(tensorStates, ';');
        for (unsigned int i = 0; i < tensorCouples.size(); i++) {
            vector<string> states = tools::splitString(tensorCouples[i], '-');
            if (states.size() == 2) {
                vector<int> state = {tools::string_to_int(states[0]), tools::string_to_int(states[1])};
                statesToWrite.push_back(state);
            } else {
                vector<int> state = {tools::string_to_int(states[0])};
                statesToWrite.push_back(state);
            }
        }
        if (lshape != "") {
            vector<string> shapes = tools::splitString(lshape, ';');
            Lshape.push_back(tools::string_to_int(shapes[0]) - 1);
            if (shapes.size() > 1)
                Lshape.push_back(tools::string_to_int(shapes[1]) + 1);
            else
                Lshape.push_back(3 * tools::string_to_int(shapes[0]) + 1);
        }
    }
}

void Population::run(TimePropagatorOutput &out, DerivativeFlat *propDer) {
    if (populationN == 0)return;
    vector<complex<double> > Eval;
    vector<Coefficients *> Evec;
    if (propDer->iIndex->hierarchy().find("Rn2") != string::npos)
        read(Eval, Evec, propDer->iIndex);
    else
        propDer->eigen(Eval, Evec, populationN);
//    out.setPouplationStates(Evec, Eval);
    string states;
    for (unsigned int i = 0; i < Eval.size(); i++)
        states += tools::str(Eval[i]) + " ";
    if (statesToWrite.size() > 0) {
        PrintOutput::message("writting the generated wavefunctions");
        if (propDer->iIndex->hierarchy() != "Phi.Eta.Rn.Rn")ABORT("we only support Phi.Eta.Rn.Rn type hierachy of DI");
        Index *prod = new IndexProd(propDer->iIndex, propDer->iIndex);
        vector<unsigned int> perm = {0, 1, 4, 5, 2, 6, 3, 7};
        Coefficients *gen6D = new CoefficientsPermute(prod, perm);
        crop(gen6D);
        Coefficients *coef1, * coef2, * coefGen;
        for (unsigned int i = 0; i < statesToWrite.size(); i++) {
            coefGen = gen6D->firstLeaf(), coef1 = Evec[statesToWrite[i][0]];
            if (statesToWrite[i].size() > 1)
                coef2 = Evec[statesToWrite[i][1]];
            else {
                coef2 = new Coefficients(*coef1);
                coef2->setToConstant(1.);
            }
            std::ofstream writeStream((ReadInput::main.output() + "wfPopDI@" + tools::str(Eval[statesToWrite[i][0]].real() + (statesToWrite.size() > 1 ? Eval[statesToWrite[i][1]].real() : 0.), 3)).c_str(),
                                      (ios_base::openmode) ios::beg);
            while (coefGen) {
                vector<unsigned int> blockIdx = coefGen->index();
                for (unsigned int i = 0; i < coefGen->size(); i++)
                    *(coefGen->data() + i) = *(coef1->child(blockIdx[0])->child(blockIdx[1])->child(blockIdx[4])->data() + blockIdx[6]) *
                                             (*(coef2->child(blockIdx[2])->child(blockIdx[3])->child(blockIdx[5])->data() + i));
                coefGen = coefGen->nodeRight();
            }
            PrintOutput::message("we are writting the population at size" + tools::str(gen6D->size()));
            gen6D->write(writeStream, false);
        }
        PrintOutput::message("DONE");
    }
}

void Population::read(vector<complex<double> > &Eval, vector<Coefficients *> &Evec, const Index *readIdx) {
    path dir(ReadInput::main.outputTopDir().c_str());
    for (auto &fileName : boost::make_iterator_range(directory_iterator(dir), {})) {
        std::stringstream ss;
        ss << fileName;
        if (ss.str().find("wfPopDI") != string::npos) {
            Evec.push_back(new Coefficients(readIdx));
            PrintOutput::message("we are reading the population at size" + tools::str(Evec.back()->size()));
            // rm " in the begin and end
            std::ifstream fileCoef(ss.str().substr(1, ss.str().length() - 2), ios::binary );
            Evec.back()->read(fileCoef, false);
            vector<string> items;
            items = tools::splitString(ss.str(), '@');
            Eval.push_back(tools::string_to_double(items[1]));
        }
    }
}


void Population::crop(Coefficients *coef) {
    if (coef->isLeaf())return;
    if (coef->idx()->axisName() == "Phi" and coef->depth() == 2) { //to be done for phi
        int mIdxKeep = coef->index()[0] % 2 == 0 ? (coef->index()[0] - 1) : (coef->index()[0] + 1);
        mIdxKeep = max(mIdxKeep, 0);
        while (coef->childSize() > mIdxKeep + 1)
            coef->childPop();
        while (coef->childSize() > 1)
            coef->childErase(0);
    }
    if (coef->idx()->axisName() == "Eta") {
        int lUpper = coef->childSize();
        if (Lshape.size() > 0  and coef->depth() == 3) {
            int constrain = (Lshape.size() > 1 ? (Lshape[1] - coef->index()[1]) : (3 * Lshape[0] - coef->index()[1]));
            if (coef->index()[1] > Lshape[0]) lUpper = max(constrain, Lshape[0]);
        }
        if (coef->index()[0] > 0 and MConstrain != 0)lUpper = min(lUpper, MConstrain - 1);
        while (coef->childSize() > lUpper)
            coef->childPop();
    }
    for (unsigned int i = 0; i < coef->childSize(); i++)
        crop(coef->child(i));
}

