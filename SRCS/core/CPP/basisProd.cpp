// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "basisProd.h"

#include "index.h"
#include "vectorComplex.h"
#include "str.h"
#include "basisMat.h"
#include "coordinateTrans.h"
#include "operatorDefinition.h"

std::map<const Index*,BasisProd*> BasisProd::all;

using namespace std;

Eigen::MatrixXd BasisProd::jacobianToNdim(const std::vector<double>&CoorNdim) const {
    return Eigen::Map<Eigen::MatrixXd>(_jacToNdim(CoorNdim).data(),dim(),dim());
}

BasisProd::BasisProd(const Index *RootIdx, const BasisNdim *Ref)
{
     _ndimCoor=RootIdx->coordinates();
    _fromNdim= CoordinateTrans::toCartesian(_ndimCoor);
    _toNdim=   CoordinateTrans::fromCartesian(_ndimCoor);
    _jacToNdim=CoordinateTrans::jacCartesian(_ndimCoor);
    _intFactor=CoordinateTrans::integrationFactor(_ndimCoor);
    _nabFactor=CoordinateTrans::nablaFactor(_ndimCoor);

    _quadCoor=Ref->quadCoor();
    CoordinateMap fromMain=CoordinateTrans::toCartesian(_quadCoor);
    IntegrationFactor mainFactor=CoordinateTrans::integrationFactor(_quadCoor);

    _valDer.resize(Ref->quadGrid().size());
    for(int pt=0;pt<Ref->quadGrid().size();pt++){
        // Ref._quadGrid,Ref._quadWeig is wrt to main coors, transform to Ndim coors
        _quadGrid.push_back(_toNdim(fromMain(Ref->quadGrid()[pt])));
        _quadWeig.push_back(Ref->quadWeig()[pt]*pow(mainFactor(Ref->quadGrid()[pt])/_intFactor(_quadGrid[pt]),2));

        // evaluate all basis functions at given point
        valDerAll(RootIdx,_quadGrid[pt],_valDer[pt]);
    }
    _comSca.resize(RootIdx->height()); // for now, no complex scaling on product basis

    // transform back to mainCoor
    mainQuadValDer();
}

BasisProd::BasisProd(const Index *RootIdx, unsigned int MinQuad)
{
    _ndimCoor=RootIdx->hierarchy();
    _quadCoor=_ndimCoor;
    productGridWeig(RootIdx,_quadGrid,_quadWeig,MinQuad);

    _fromNdim= CoordinateTrans::toCartesian(_ndimCoor);
    _toNdim=   CoordinateTrans::fromCartesian(_ndimCoor);
    _jacToNdim=CoordinateTrans::jacCartesian(_ndimCoor);
    _intFactor=CoordinateTrans::integrationFactor(_ndimCoor);
    _nabFactor=CoordinateTrans::nablaFactor(_ndimCoor);

    _valDer.resize(_quadGrid.size());
    for(int pt=0;pt<_quadGrid.size();pt++){
        // evaluate all basis functions at given point
        bool OutPoint; // outpoint is not really needed in this constructor
        valDerAll(RootIdx,_quadGrid[pt],_valDer[pt]);
    }
    _comSca.resize(RootIdx->height()); // for now, no complex scaling on product basis
}

const BasisProd* BasisProd::factory(const Index *Idx, const Index *Ref){
    if(Ref->basisNdim()==0)ABORT("must have BasisNdim on Ref'erence Index");

    // note: this check should be improved
    Idx=Ref->basisNdim()->rootIndex(Idx);
    if(Idx==0)ABORT("Index does not match Ref");

    if(all.count(Idx)==0)all[Idx]=new BasisProd(Idx,Ref->basisNdim());
    return all[Idx];
}

vector<complex<double> > BasisProd::tensorMultiply(const vector<complex<double> > &LFac, const vector<complex<double> > &RFac) const {
    vector<complex<double> > res;
    for(int k=0;k<RFac.size();k++)
        for(int l=0;l<LFac.size();l++)
            res.push_back(LFac[l]*RFac[k]);
    return res;
}

void BasisProd::valDerAll(const Index *Idx, vector<double> Point, vector<vector<complex<double> > >&ValDer) const{
    if(Point.size()==0){
        ValDer.push_back(vector<complex<double> >(1,1.));
    }
    else {

        if(Idx->continuity()!=Index::npos){
            // for fem levels, insert index coordinate
            for(int k=0;k<Idx->childSize();k++){
                std::vector<std::vector<std::complex<double> > > valder;
                valDerAll(Idx->child(k),Point,valder);
                for(int l=0;l<valder.size();l++)ValDer.push_back(valder[l]);
            }
        }
        else {
            UseMatrix uPt=UseMatrix::Constant(1,1,Point[0]),val,der;
            Idx->basisAbstract()->valDer(uPt,val,der,true);
            //"zeroOutside" is broken - do by hand
            if(Idx->basisIntegrable()->lowBound()>Point[0] or Point[0]>Idx->basisIntegrable()->upBound()){
                val*=0;
                der*=0;
            }
            for(int k=0;k<Idx->childSize();k++){
                std::vector<std::vector<std::complex<double> > > valder;
                valDerAll(Idx->child(k),vector<double>(Point.begin()+1,Point.end()),valder);
                for(int l=0;l<valder.size();l++){
                    complex<double> tmp=valder[l][0];
                    for(int i=0;i<valder[l].size();i++)valder[l][i]*=val(k).complex();
                    // sort deriviatives such that highest level has lowest index (e.g. phi at valder[l][1]
                    valder[l].insert(valder[l].begin()+1,tmp*der(k).complex());
                    ValDer.push_back(valder[l]);
                }
            }
        }
    }
}
