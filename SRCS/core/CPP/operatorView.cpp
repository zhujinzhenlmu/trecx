// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "operatorView.h"

#include "operator.h"
#include "index.h"
#include "operatorFloor.h"
#include "operatorZG.h"
#include <complex>

using namespace std;

OperatorView::OperatorView(Operator &Op):data(0)
{
    if(Op.o.size()>0)ABORT("convert to OperatorFloor's before creating OperatorView");
    if(Op.tensor!=0 or Op.preTensor!=0)ABORT("not for tensor products");

    bl=&Op;

    if(Op.oFloor!=0){
        Op.oFloor->uniqueData(); // make sure floor data is not shared by other floors
        // check for full block
        OperatorZG * op=dynamic_cast<OperatorZG*>(Op.oFloor);
        if(op==0)ABORT("only for full-block ZG floors, is: "+Op.oFloor->strInfo());

        // set data pointer to beginning of block, first column index
        data=op->dat->data();
    }

    else
        // descend through operator
        for(int n=0;n<Op.O.size();n++)childAdd(new OperatorView(*Op.O[n]));
}

void OperatorView::getColumn(int J, Coefficients &C){

    if(C.parent()==0){
        C.setToZero();
    }


    if(data!=0){
        if(C.idx()!=bl->iIndex)ABORT("Coefficient does not match");
        int colJ=(J-bl->jIndex->posIndex())*C.idx()->sizeStored();
        for(int i=0;i<C.idx()->sizeStored();i++)C.data()[i]+=data[colJ+i];
    }

    else {
        // descend in operator
        for(int n=0;n<childSize();n++){
            // locate block containing column J
            int col0=child(n)->bl->jIndex->posIndex();
            int col1=col0+child(n)->bl->jIndex->sizeStored();
            if(col0<=J and J<col1){
                if(C.idx()==child(n)->bl->iIndex)
                    child(n)->getColumn(J,C);
                else {
                    // find row range
                    for(int nC=0;nC<C.childSize();nC++)
                        if(C.child(nC)->idx()==child(n)->bl->iIndex)
                            child(n)->getColumn(J,*C.child(nC));
                }
            }
        }
    }
}

void OperatorView::getRow(int I, Coefficients &C){

    if(C.parent()==0)C.setToZero();

    if(data!=0){
        if(C.idx()!=bl->jIndex)ABORT("Coefficient does not match");
        int rowI=I-bl->iIndex->posIndex();
        for(int k=0;k<C.idx()->sizeStored();k++)C.data()[k]+=data[rowI+k*bl->iIndex->sizeStored()];
    }

    else {
        // descend in operator
        for(int n=0;n<childSize();n++){
            // locate block containing row I
            int row0=child(n)->bl->iIndex->posIndex();
            int row1=row0+child(n)->bl->iIndex->sizeStored();
            if(row0<=I and I<row1)
                if(C.idx()==child(n)->bl->jIndex)
                    child(n)->getRow(I,C);
                else {
                    for(int nC=0;nC<C.childSize();nC++){
                        // find col range
                        if(C.child(nC)->idx()==child(n)->bl->jIndex)
                            child(n)->getRow(I,*C.child(nC));
                    }
                }
        }
    }
}

void OperatorView::insertColumn(int J, const Coefficients &C){

    if(data!=0){
        if(C.idx()!=bl->iIndex)ABORT("Coefficient does not match");
        int colJ=(J-bl->jIndex->posIndex())*C.idx()->sizeStored();
        for(int i=0;i<C.idx()->sizeStored();i++)
            data[colJ+i]=const_cast<Coefficients*>(&C)->data()[i];
    }
    else if (C.isLeaf() and not C.isZero(1.e-12))
        ABORT(C.str()+"cannot insert into zero floor");

    else {
        // descend in operator
        for(int n=0;n<childSize();n++){
            // locate block containing column J
            int col0=child(n)->bl->jIndex->posIndex();
            int col1=col0+child(n)->bl->jIndex->sizeStored();
            if(col0<=J and J<col1){
                if(C.idx()==child(n)->bl->iIndex)
                    child(n)->insertColumn(J,C);
                else {
                    // find row range
                    for(int nC=0;nC<C.childSize();nC++){
                        if(C.child(nC)->idx()==child(n)->bl->iIndex){
                            child(n)->insertColumn(J,*C.child(nC));
                        }
                    }
                }
            }
        }
    }
}

std::string OperatorView::strData() const{
    //    std::cout<<"data: "<<data<<" at "<<row0+" x "<<col0
    //           <<std::endl;
    //    std::string s=tools::str(data)+" at "+tools::str(row0)+" x "+tools::str(col0);
    //    return tools::str(data)+" at "+tools::str(row0)+" x "+tools::str(col0);
    //string s(tools::str(data));
    string s("bla");
    return s;
}
