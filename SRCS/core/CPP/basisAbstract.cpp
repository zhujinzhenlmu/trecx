// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "basisAbstract.h"

#include "printOutput.h"

#include "basisSetDef.h"
#include "basisSet.h"
#include "basisNdim.h"
#include "basisDvr.h"
#include "basisGridQuad.h"
#include "basisSet.h"
#include "basisBesselCoulomb.h"
#include "basisEigen.h"
#include "basisExpIm.h"
#include "basisVector.h"
#include "basisOrbitalNumerical.h"
#include "basisChannel.h"
#include "basisSub.h"
#include "threads.h"
#include "basisAssocLeg.h"

static std::map<std::string,const BasisAbstract*> _listOfBases;

const BasisAbstract* BasisAbstract::factory(const BasisSetDef & Def){
    const BasisAbstract* bas;
    if(0!=(bas=BasisNdim::factory(Def.funcs)));
    else if(Def.funcs=="polynomial")bas=new BasisDVR(Def);
    else if(Def.funcs.find("polExp")==0)bas=new BasisDVR(Def);
    else if(Def.funcs.find("besselCoulomb")==0)bas=new BasisBesselCoulomb(Def);
    else if(Def.funcs.find("Eigenbasis")==0)bas=new BasisEigen(Def);
    else if(Def.funcs.find("expIm")==0)bas=new BasisExpIm(Def);
    else if(Def.funcs.find("cosSin")==0)bas=new BasisCosSin(Def);
    else if(Def.funcs.find("vector")==0)bas=new BasisVector(Def.order);
    else if(Def.funcs.find("Orbital")==0)bas=new BasisOrbitalNumerical(Def);
    else if(Def.funcs.find("Channel")==0)bas=new BasisChannel(Def.funcs,Def.order,int(Def.lowBound()));
    else bas=new BasisSet(Def);

    std::string strDef=bas->strDefinition();
    if(strDef.find("not implemented")!=std::string::npos){
        PrintOutput::DEVwarning("eliminate eventually: "+strDef,50);
        return bas;
    }
    delete bas;
    return factory(strDef);
}


const BasisAbstract* BasisAbstract::factory(const std::string & Def){
    //NOTE: the specialized factories should all be eliminated
    std::string def=tools::cropString(Def);
    if(_listOfBases.count(def))return _listOfBases[def];
    const BasisAbstract* bas(0);
    if(0==def.find("Thread:")) bas=new BasisThread(def);
    if(0==def.find("BasisSub:"))    bas=new BasisSub(def);
    if(0==def.find("ExpIm:"))       bas=new BasisExpIm(def);
    if(0==def.find("CosSin:"))      bas=new BasisCosSin(def);
    if(0==def.find("AssociatedLegendre:"))bas=new BasisAssocLeg(def);
    if(0==def.find("Grid:"))        bas=BasisGrid::factory(def);
    if(0==def.find("GridQuad:"))    bas=BasisGridQuad::factory(def);
    if(0==def.find("DVR:"))         bas=new BasisDVR(def);
    if(0==def.find("NONE:"))        bas=BasisSet::getDummy(1);
    if(0==def.find("Coef:"))        bas=BasisSet::getDummy(tools::string_to_int(def.substr(5)));
    if(0==def.find("Vector:"))      bas=BasisVector::factory(def);

    if(bas==0)DEVABORT("construction from StrDefinition not implemented: "+def.substr(0,40)+"...");
    return _listOfBases[def]=bas;
}

bool BasisAbstract::isAbsorptive() const{
    const BasisIntegrable* bas=dynamic_cast<const BasisIntegrable*>(this);
    if(bas==0)return false;
    return bas->isAbsorptive();
}

const BasisAbstract * BasisAbstract::remove(const std::vector<int> &RemoveK) const{
    std::vector<int> subset;
    for(int k=0;k<size();k++)
        if(std::find(RemoveK.begin(),RemoveK.end(),k)==RemoveK.end())
            subset.push_back(k);
    return BasisAbstract::factory(BasisSub::strDefinition(this,subset));
}


const BasisNdim* BasisAbstract::basisNdim() const {return dynamic_cast<const BasisNdim*>(this);}


bool BasisAbstract::operator==(const BasisAbstract &Other) const{
    if(this==&Other)return true;
    if(size()!=Other.size())return false;
    if(isGrid())return BasisGrid::factory(this)->operator==(Other);
    if(dynamic_cast<const BasisTrigon*>(this))return dynamic_cast<const BasisTrigon*>(this)->operator==(Other);
    if(dynamic_cast<const BasisSet*>(this)!=0)return dynamic_cast<const BasisSet*>(this)->operator==(Other);
    if(dynamic_cast<const BasisOrbitalNumerical*>(this)!=0)return dynamic_cast<const BasisOrbitalNumerical*>(this)->operator==(Other);
    if(_name=="NONE" and Other._name=="NONE")return true;
    if((dynamic_cast<const BasisThread*>(this)==0)!=(dynamic_cast<const BasisThread*>(&Other)==0))return false;
    if(dynamic_cast<const BasisSub*>(this))return dynamic_cast<const BasisSub*>(this)->operator==(Other);
    DEVABORT(Sstr+"comparison not implemented:"+str()+"??"+Other.str());
}

std::string BasisAbstract::str(int Level) const{
    return _name;
    //    if(dynamic_cast<const BasisSet*>(this)!=0)return dynamic_cast<const BasisSet*>(this)->str(Level);
    if(dynamic_cast<const BasisNdim*>(this)!=0)return dynamic_cast<const BasisNdim*>(this)->str(Level);
    if(dynamic_cast<const BasisDVR*>(this)!=0)return dynamic_cast<const BasisDVR*>(this)->str(Level);
    return Str("","")+_name+size();
}
std::string BasisAbstract::strDefinition() const{
    if(name()=="NONE")return "NONE:";
    return "not implemented for "+_name+" ("+str()+")";
}
