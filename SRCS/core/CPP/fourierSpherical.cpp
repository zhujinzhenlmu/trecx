#include "readInput.h"
#include "fourierSpherical.h"
#include "discretization.h"
#include "discretizationSurface.h"
#include "discretizationtsurffspectra.h"
#include "coefficients.h"
#include "index.h"
#include "discretizationGrid.h"
#include "algebra.h"
#include "plot.h"

using namespace std;

FourierSpherical::FourierSpherical(ReadInput inp, const Discretization *Din) {
    if (Din->idx()->hierarchy().find("Rn") == string::npos)ABORT("only support spherical coordinates in FourierSpherical, not suitable for " + Din->idx()->hierarchy());

    // get basis parameters
    vector<string> gridAxis = (Din->idx()->firstLeaf()->axisName() == "Rn") ? vector<string> {"Rn"}:
                              vector<string> {"Rn1", "Rn2"};
    const Index *idx = Din->idx()->root();
    for (; not idx->hasFloor(); idx = idx->descend()) {
        if (idx->axisName().find("Phi") != string::npos) {
            phiDepth.push_back(idx->depth());
            mmax = max(mmax, idx->childSize());
        } else if (idx->axisName().find("Eta") != string::npos) {
            etaDepth.push_back(idx->depth());
            const Index *idxEta = idx;
            for (; idxEta; idxEta = idxEta->nodeRight())lmax = max(lmax, idxEta->childSize()) > 1000? lmax:max(lmax, idxEta->childSize());
        } else if (idx->axisName().find("Rn") != string::npos and not idx->hasFloor())rDepth.push_back(idx->depth());
    }

    // create the momenta index and coefs
    vector<double> radii;
    inp.read("Surface", "points", radii, "", "which radii to use");
    PrintOutput::message("start creating k grid index");
    D = new DiscretizationTsurffSpectra(new DiscretizationSurface(new DiscretizationTsurffSpectra(new DiscretizationSurface(Din, radii, 0), inp), radii, 0), inp) ;
    cout << "The created discretization hierachy is " << D->idx()->hierarchy() << endl;
    coefMom = new Coefficients(D->idx());
    for (unsigned int i = 0; i < coefMom->firstLeaf()->idx()->basisAbstract()->Points().rows(); i++)
        kGrids.push_back(coefMom->firstLeaf()->idx()->basisAbstract()->Points()(i, 0).real());


    // create the gird index and coefs
    PrintOutput::message("start creating r grid index");
    vector<vector<double>> limits(2, {0., radii[0]});
    Dg = new DiscretizationGrid(Din->idx(), gridAxis, vector<unsigned int>(), limits);
    coefGrid = new Coefficients(Dg->idx());
    cout << tools::str(rDepth) << endl;
    cout << "The childSize is " << coefGrid->descend(rDepth[0])->childSize() << endl;
    rGrids.resize(coefGrid->descend(rDepth[0])->childSize() - 1);
    rWeights.resize(coefGrid->descend(rDepth[0])->childSize() - 1);
    for (unsigned int i = 0; i < coefGrid->descend(rDepth[0])->childSize() - 1; i++)
        for (unsigned int j = 0; j < coefGrid->descend(rDepth[0])->child(i)->firstLeaf()->idx()->basisAbstract()->Points().rows(); j++) {
            rGrids[i].push_back(coefGrid->descend(rDepth[0])->child(i)->firstLeaf()->idx()->basisAbstract()->Points()(j, 0).real());
            rWeights[i].push_back(coefGrid->descend(rDepth[0])->child(i)->firstLeaf()->idx()->basisAbstract()->Weights()(j, 0).real());
        }

    //create bessel functions based on r and k
    PrintOutput::message("start bessel functions with lmax " + tools::str(lmax));
    besselFuncs.resize(lmax);
    for (unsigned int l = 0; l < besselFuncs.size(); l++) {
        besselFuncs[l].resize(rGrids.size());
        for (unsigned int i = 0; i < rGrids.size(); i++) {
            besselFuncs[l][i].resize(rGrids[i].size());
            for (unsigned int j = 0; j < rGrids[i].size(); j++) {
                Algebra *bessel = new AlgebraSpherBessel(("[0," + tools::str(l) + "," + tools::str(rGrids[i]) + "]" ));
                besselFuncs[l][i][j].resize(kGrids.size());
                for (unsigned int k = 0; k < kGrids.size(); k++)
                    besselFuncs[l][i][j][k] = bessel->val(kGrids[k]);
            }

        }
    }
    cout << "The points are " << tools::str(rGrids[0]) << " and weights " << tools::str(rWeights[0]) << endl;

}

void FourierSpherical::genPlot6D(unsigned int NEta, unsigned int diffRange) {
    // generate plots
    vector<unsigned int> points = {(unsigned int)kGrids.size(), (unsigned int)kGrids.size(), NEta, NEta};
    vector<string> axes = {"kRn1", "kRn2", "Eta1", "Eta2"}, uses = {"g", "g", "", ""};
    vector<double> boundUpEta = {0., 1.}, boundDnEta = {-1., 0.};
    vector<vector<double> > UpWfBounds = {{0., 0.}, {0., 0.}, boundUpEta, boundUpEta}, DnWfBounds = {{0., 0.}, {0., 0.}, boundDnEta, boundDnEta};
    if (diffRange != 0) {
        UpWfBounds = {{0., 0.}, {0., 0.}, boundUpEta, boundDnEta};
        DnWfBounds = {{0., 0.}, {0., 0.}, boundUpEta, boundDnEta};
    }
    plotUp = new Plot(dynamic_cast<const Discretization *>(D)->idx(), axes, uses, points, UpWfBounds);
    plotDn = new Plot(dynamic_cast<const Discretization *>(D)->idx(), axes, uses, points, DnWfBounds);
}

void FourierSpherical::plotTransform(string fileName, string side, const Coefficients coefIn) {
    if (side == "Up")plotUp->plot(tranform(coefIn), fileName);
    else plotDn->plot(tranform(coefIn), fileName);
}

Coefficients FourierSpherical::tranform(const Coefficients coeff) {
    coefGrid->setToZero();
    coefMom->setToZero();
    Dg->mapFromParent()->apply(1, coeff, 0, *coefGrid);
    transCoef(coefGrid, coefMom);
    return *coefMom;
}

void FourierSpherical::transCoef(Coefficients *coefR, Coefficients *coefK) {
    if (coefR->isLeaf()) {
        vector<unsigned int> ls, intervals;
        for (unsigned int i = 0; i < etaDepth.size(); i++) {
            ls.push_back(coefR->index()[etaDepth[i]]);
            intervals.push_back(coefR->index()[rDepth[i]]);
        }
        transFloor(coefR->data(), coefK->data(), ls, intervals);
    } else {
        if (coefR->idx()->axisName().find("Rn") != string::npos and coefR->childSize() > 1)
            for (unsigned int i = 0; i < coefR->childSize() - 1; i++)
                transCoef(coefR->child(i), coefK->child(0));
        else
            for (unsigned int i = 0; i < coefR->childSize(); i++)
                transCoef(coefR->child(i), coefK->child(i));

    }
}

void FourierSpherical::transFloor(complex<double> *fData, complex<double> *fDatasMom,  vector<unsigned int> ls, vector<unsigned int> intervals) {
    // put the R data in a vector for transformation
    vector<complex<double>> fDataVec(pow(rGrids[0].size(), ls.size()), 0.);

// do the loop from all k grids
    for (unsigned int K = 0; K < pow(kGrids.size(), ls.size()); K++) {
        // initialize the floor data
        for (unsigned int i = 0; i < fDataVec.size(); i++)fDataVec[i] = *(fData + i);

        // get the k indexes
        vector<unsigned int> ks(ls.size(), 0);
        unsigned int totalK = K;
        for (unsigned int i = 0; i < ks.size(); i++) {
            ks[i] = totalK / pow(kGrids.size(), ks.size() - i - 1);
            totalK = totalK - ks[i] * pow(kGrids.size(), ks.size() - i - 1);
        }

        // loop the coefficients values of R
        for (unsigned int R = 0; R < fDataVec.size(); R++) {
            // get the r indexes
            vector<unsigned int> rs(ls.size(), 0);
            unsigned totalR = R;
            for (unsigned int i = 0; i < rs.size(); i++) {
                rs[i] = totalR / pow(rGrids[0].size(), rs.size() - i - 1);
                totalR = totalR - rs[i] * pow(rGrids[0].size(), rs.size() - i - 1);
            }
            // do the integration elements, with indexes by the dimension index, say R1, R2, R3
            for (unsigned int i = 0; i < rs.size(); i++)
                fDataVec[R] *= pow(complex<double>(0., 1.), ls[i]) * sqrt(2. / math::pi) * besselFuncs[ls[i]][intervals[i]][rs[i]][ks[i]] * pow(rGrids[intervals[i]][rs[i]], 2) * rWeights[intervals[i]][rs[i]];
            *(fDatasMom + K) = *(fDatasMom + K) + fDataVec[R];
        }
    }
}

