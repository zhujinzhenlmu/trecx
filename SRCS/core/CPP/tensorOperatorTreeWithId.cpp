#include <iostream>
#include <vector>
#include <set>

#include "str.h"
#include "tensorOperatorTreeWithId.h"
#include "index.h"
#include "indexProd.h"
#include "indexConstraint.h"
#include "operatorFloor.h"
#include "printOutput.h"
#include "parallelOperator.h"
#include "parallel.h"

#include "operatorTreeExplorer.h"
#include "eigenTools.h"
#include "log.h"


template<typename T>
static std::vector<T> extract(std::vector<bool> idx, std::vector<T> data, bool inv=false){
    std::vector<T> result;

    if(idx.size() < data.size()) ABORT("Sizes mismatch");

    for(int i=0; i<data.size(); i++){
        if((inv && !idx[i]) || (!inv && idx[i])) result.push_back(data[i]);
    }
    return result;
}

template<typename T>
static bool match(
        std::vector<T> first,
        std::vector<T> second,
        std::function<bool(const T&, const T&)> equals=[](const T& a, const T&b){ return a==b; }){
    for(int i=0; i<first.size() && i<second.size(); i++){
        if(!equals(first[i], second[i])) return false;
    }
    return true;
}

const OperatorTree* TensorOperatorTreeWithId::InternalOperatorTree::find(
        TensorOperatorTreeWithId* Parent,
        std::vector<double> IPath,
        std::vector<double> JPath,
        const OperatorTree* Cur){

    if(Cur==0) Cur=Parent->base;
    if(IPath.size()==0 && JPath.size()==0) return Cur;

    // Return if we cannot descend further, i.e. path specifies location within floor
    if(Cur->childSize()==0) return Cur;

    for(int i=0; i<Cur->childSize(); i++){
        bool iDescend = (Cur->child(i)->iIndex != Cur->iIndex);
        bool jDescend = (Cur->child(i)->jIndex != Cur->jIndex);

        if(iDescend && std::abs(Cur->child(i)->iIndex->physical() - IPath[0]) > 1.e-12) continue;
        if(jDescend && std::abs(Cur->child(i)->jIndex->physical() - JPath[0]) > 1.e-12) continue;

        std::vector<double> iPath = IPath;
        std::vector<double> jPath = JPath;

        if(iDescend) iPath.erase(iPath.begin());
        if(jDescend) jPath.erase(jPath.begin());

        const OperatorTree* result = find(Parent, std::move(iPath), std::move(jPath), Cur->child(i));
        if(result != 0) return result;
    }

    return 0;
}

TensorOperatorTreeWithId::InternalOperatorTree::InternalOperatorTree(
        TensorOperatorTreeWithId* Parent,
        const Index* IIndex,
        const Index* JIndex):
    OperatorTree(Parent->base->name, IIndex, JIndex){

    std::vector<double> iPath;
    for(const Index* idx=iIndex; idx->parent()!=0; idx=idx->parent()){
        iPath.insert(iPath.begin(), idx->physical());
    }

    std::vector<double> jPath;
    for(const Index* idx=jIndex; idx->parent()!=0; idx=idx->parent()){
        jPath.insert(jPath.begin(), idx->physical());
    }

    // Quite inefficient as for diagonal levels every child is created and immediately deleted
    if(!match(extract(Parent->iIdentities, iPath), extract(Parent->jIdentities, jPath))) return;


    /*
     * Construct floor
     */
    if(iIndex->hasFloor() && jIndex->hasFloor()){
        std::vector<double> iBase = extract(Parent->iIdentities, iPath, true);
        std::vector<double> jBase = extract(Parent->jIdentities, jPath, true);
        const OperatorTree* base = find(Parent, iBase, jBase);

        // No Operator = Zero Operator
        if(base == 0) return;

        // Possibly the matrix we want is a submatrix
        if(base->iIndex->index().size() != iBase.size() || base->jIndex->index().size() != jBase.size())
            DEVABORT("Not implemented");

        int idFactorSize = iIndex->sizeStored() / base->iIndex->sizeStored();
        if(base->iIndex->sizeStored() * idFactorSize != iIndex->sizeStored())
            DEVABORT("lhs descent went wrong");
        if(base->jIndex->sizeStored() * idFactorSize != jIndex->sizeStored())
            DEVABORT(Sstr+"rhs descent went wrong:"+iIndex->sizeStored()+base->jIndex->sizeStored()+idFactorSize+jIndex->sizeStored()
                     +"\n"+base->jIndex->str()+"\n"+iIndex->str());

        UseMatrix floorMatrix;
        base->matrix(floorMatrix);

        std::string hash = name + base->iIndex->hash() + base->jIndex->hash() + "i" + tools::str(idFactorSize);

        if(idFactorSize == 1){
            PrintOutput::DEVwarning(
                        "TensorOperatorTreeWithId: Not tensor product structure at floor level. Possibly inefficient");
            floor() = Parallel::operatorFloor(iIndex, jIndex,
                                              [&](){ return OperatorFloor::factory(std::vector<const UseMatrix*>{ &floorMatrix }, hash); }
            );
        }else{
            UseMatrix id = UseMatrix::Zero(idFactorSize, idFactorSize);
            for(int i=0; i<idFactorSize; i++) id(i, i)=1.;

            if(Parent->floorZGtimesId)
                floor() = Parallel::operatorFloor(iIndex, jIndex,
                                                  [&](){ return OperatorFloor::factory(std::vector<const UseMatrix*>{ &floorMatrix, &id }, hash); }
                );
            else
                floor() = Parallel::operatorFloor(iIndex, jIndex,
                                                  [&](){ return OperatorFloor::factory(std::vector<const UseMatrix*>{ &id, &floorMatrix }, hash); }
                );
        }

    }else

        /*
     * Descend
     */
        if(!iIndex->hasFloor() && !jIndex->hasFloor()){
            for(int i=0; i<iIndex->childSize(); i++){
                for(int j=0; j<jIndex->childSize(); j++){
                    InternalOperatorTree* child = new InternalOperatorTree(Parent, iIndex->child(i), jIndex->child(j));
                    if(child->childSize() == 0 && child->floor() == 0){
                        delete child;
                    }else{
                        childAdd(child);
                    }
                }
            }

        }else if(!iIndex->hasFloor()){
            for(int i=0; i<iIndex->childSize(); i++){
                InternalOperatorTree* child = new InternalOperatorTree(Parent, iIndex->child(i), jIndex);
                if(child->childSize() == 0 && child->floor() == 0){
                    delete child;
                }else{
                    childAdd(child);
                }
            }

        }else if(!jIndex->hasFloor()){ // Actually the only remaining case
            for(int j=0; j<jIndex->childSize(); j++){
                InternalOperatorTree* child = new InternalOperatorTree(Parent, iIndex, jIndex->child(j));
                if(child->childSize() == 0 && child->floor() == 0){
                    delete child;
                }else{
                    childAdd(child);
                }
            }
        }
}

TensorOperatorTreeWithId::TensorOperatorTreeWithId(const OperatorTree* Base):
    base(Base), iIndex(0), jIndex(0), quotIndex(0), hierarchy(""), floorDepth(-1){}

TensorOperatorTreeWithId& TensorOperatorTreeWithId::withQuotIndex(const Index* QuotIndex){
    quotIndex = QuotIndex;
    return *this;
}

TensorOperatorTreeWithId& TensorOperatorTreeWithId::withGeneratedIndex(std::string Hierarchy, int FloorDepth){
    hierarchy = Hierarchy;
    floorDepth = FloorDepth;
    return *this;
}

TensorOperatorTreeWithId& TensorOperatorTreeWithId::withIIndex(const Index* IIndex){
    iIndex = IIndex;
    return *this;
}

TensorOperatorTreeWithId& TensorOperatorTreeWithId::withJIndex(const Index* JIndex){
    jIndex = JIndex;
    return *this;
}

std::vector<unsigned int> TensorOperatorTreeWithId::findPermutation(
        std::string Hierarchy_a,
        std::string Hierarchy_A,
        std::string Hierarchy_b,
        std::string Hierarchy_B,
        std::string Hierarchy_I){

    std::vector<std::string> hierarchy_a, hierarchy_A, hierarchy_b, hierarchy_B, hierarchy_I;
    hierarchy_a=tools::splitString(Hierarchy_a, '.');
    hierarchy_A=tools::splitString(Hierarchy_A, '.');
    hierarchy_b=tools::splitString(Hierarchy_b, '.');
    hierarchy_B=tools::splitString(Hierarchy_B, '.');
    hierarchy_I=tools::splitString(Hierarchy_I, '.');


    /*
     * Clumsy algorithm. If in doubt supply hierarchy using withGeneratedIndex("Phi1.Eta1.Phi2.Eta2....")
     */
    if(hierarchy == ""){
        /*
         * Align target hierarchy for equal identity levels
         */
        std::vector<int> identityLevels;
        for(int i=0,j=0; i<hierarchy_I.size(); i++){
            while(j<hierarchy_A.size() && hierarchy_A[j]!=hierarchy_I[i]) j++;
            if(j>=hierarchy_A.size()) ABORT("Could not find "+hierarchy_I[i]);
            identityLevels.push_back(j);
            j++;
        }

        std::vector<std::string> targetHierarchy;
        for(int i=0, j=0, k=0; i<hierarchy_B.size(); i++){
            if(j<hierarchy_b.size() and (k>=identityLevels.size() or identityLevels[k]>i)){
                targetHierarchy.push_back(hierarchy_b[j]);
                j++;
            }else{
                targetHierarchy.push_back(hierarchy_I[k]);
                k++;
            }
        }

        // Small utility
        auto move = [](std::vector<std::string>& vec, int from, int to){
            std::string f = vec[from];
            if(from>to){
                for(int i=from; i>to; i--) vec[i] = vec[i-1];
            }else{
                for(int i=from; i<to; i++) vec[i] = vec[i+1];
            }
            vec[to]=f;
        };

        /*
         * Ensure Phi.Eta pairs remain together.
         * TODO: Needed?
         */
        for(int i=targetHierarchy.size()-1; i>=0; i--){
            if(targetHierarchy[i].find("Eta") != std::string::npos){
                std::string correspondingPhi = "Phi" + targetHierarchy[i].substr(3);
                int k = -1;
                for(int j=i-1; j>=0; j--){
                    if(targetHierarchy[j] == correspondingPhi){
                        k = j;
                        break;
                    }
                }

                if(k == -1) ABORT("Could not find "+correspondingPhi);
                if(k != i-1) move(targetHierarchy, i, k+1);
            }
        }

        /*
         * Match identity/factor levels inside floor
         */
        for(int i=0; i<floorDepth; i++){
            bool shouldBeFactor=false;
            for(auto v: hierarchy_a){
                if(hierarchy_A[hierarchy_A.size() - 1 - i]==v){
                    shouldBeFactor=true;
                    break;
                }
            }

            // Move upwards to find first matching level
            int j=i;
            for(;;j++){
                bool isFactor=false;
                for(auto v: hierarchy_b){
                    if(targetHierarchy[targetHierarchy.size() - 1 - j]==v){
                        isFactor=true;
                        break;
                    }
                }
                if(shouldBeFactor==isFactor) break;
            }

            move(targetHierarchy, targetHierarchy.size() - 1 - j, targetHierarchy.size() - 1 - i);
        }

        /*
         * Store
         */
        hierarchy = "";
        for(int i=0; i<targetHierarchy.size(); i++){
            hierarchy += targetHierarchy[i];
            if(i<targetHierarchy.size()-1) hierarchy+=".";
        }
    }

    /*
     * Find the permutation between Hierarchy_B and hierarchy
     */
    std::vector<std::string> targetHierarchy;
    targetHierarchy=tools::splitString(hierarchy, '.');
    if(hierarchy_B.size() != targetHierarchy.size()) ABORT("Incompatible hierarchies "+hierarchy+" and "+Hierarchy_B);

    // Utitity to replace Rn1.Rn1 by Rn1.Rn1'
    auto uniquify = [](std::vector<std::string>& v){
        for(auto it = v.begin(); it<v.end(); it++){
            for(auto it2 = v.begin(); it2<it; it2++){
                if(*it2 == *it){
                    *it = *it + "'";
                }
            }
        }
    };

    uniquify(targetHierarchy);
    uniquify(hierarchy_B);

    std::vector<unsigned int> permutation;
    for(int j=0; j<targetHierarchy.size(); j++){
        for(int i=0; i<hierarchy_B.size(); i++){
            if(hierarchy_B[i] == targetHierarchy[j]){
                permutation.push_back(i);
            }
        }

        if(permutation.size() != j+1) ABORT("Could not find "+hierarchy_B[j]);
    }

    return permutation;
}

#ifndef _GENERATE_INDEX_OLD_
TensorOperatorTreeWithId::IndexFactory::IndexFactory(const Index* first, const Index* second, std::vector<bool> choose_first):
    first(first), second(second), choose_first(choose_first){

    if((first->heightAboveBottom()+second->heightAboveBottom()+2) != choose_first.size()) ABORT("Size mismatch");
}

TensorOperatorTreeWithId::IndexFactory& TensorOperatorTreeWithId::IndexFactory::withConstraint(const IndexConstraint* constraint){
    this->constraint = constraint;
    return *this;
}

bool TensorOperatorTreeWithId::IndexFactory::buildRec(Index* at,
                                                      const std::vector<unsigned int>& path_first, const std::vector<unsigned int>& path_second,
                                                      const std::vector<const Index*>& path_idx){
    //AS: this was the status, without knowing the exact procedures, I hacked that to work;
    //    bool at_first = choose_first[path_idx.size()];
    bool at_first = false;
    if(choose_first.size()>path_idx.size())at_first=choose_first[path_idx.size()];

    const Index* ref = at_first ? first->nodeAt(path_first) : second->nodeAt(path_second);
    at->nodeCopy(ref, false);

    std::vector<const Index*> path_idx_new(path_idx);
    path_idx_new.push_back(at);

    std::vector<unsigned int> path(at_first ? path_first : path_second);
    path.push_back(0);

    /*
     * This is basically a copy of the Index constructor... Not really nice to
     * duplicate this code.
     */
//    for(const Index* ix=ref->firstLeaf();ix!=0 and ix->isBottom();ix=ix->nodeRight())ix->bottomExpand();
    ref->bottomExpandAll();

    std::vector<int> removed;
    for(int i=0; i<ref->childSize(); i++){
        path.back() = i;
        if(constraint != 0){
            std::vector<unsigned int> p;
            for(int j=0, iFirst=0, iSecond=0; j<path_first.size()+path_second.size(); j++){
                p.push_back(choose_first[j] ? path_first[iFirst++] : path_second[iSecond++]);
            }
            p.push_back(i);
            if(not constraint->includes(path_idx_new, p)){
                removed.push_back(i);
                continue;
            }
        }

        Index* child = new Index();
        if(buildRec(child,
                    at_first ? path : path_first,
                    at_first ? path_second : path,
                    path_idx_new)){
            at->childAdd(child);
        }else{
            delete child;
            child = 0;
            removed.push_back(i);
        }
    }

    if(removed.size() == ref->childSize() and removed.size() != 0) return false;
    if(removed.size() > 0){
        at->setBasis(at->basisAbstract()->remove(removed));
    }
    ref->bottomUnexpandAll();
    return true;
}

Index* TensorOperatorTreeWithId::IndexFactory::build(){
    Index* result = new Index();
    buildRec(result, {}, {}, {});
    return result;
}

#endif

Index* TensorOperatorTreeWithId::generateIndex(const Index* Idx_a, const Index* Idx_A, const Index* Idx_b){
    LOG_PUSH("generateIndex");

#ifdef _GENERATE_INDEX_OLD_
    std::shared_ptr<const Index> rawIdx_B;
    if(Idx_A->hierarchy() != Idx_a->hierarchy()){
        if(quotIndex == 0) ABORT("Must supply quotIndex");

        rawIdx_B = std::make_shared<IndexProd>(Idx_b, quotIndex);
        quotHierarchy = quotIndex->hierarchy();
    }else{
        // Suppress ownership
        rawIdx_B = std::shared_ptr<const Index>(Idx_b, [](const Index*){});
        quotHierarchy = "";
    }

    // Need to set floorDepth for findPermutation
    if(floorDepth == -1){
        floorDepth = Idx_A->heightAboveBottom()+1 - Idx_A->firstFloor()->depth();
    }

    std::vector<unsigned int> permutation = findPermutation(
                Idx_a->hierarchy(),
                Idx_A->hierarchy(),
                Idx_b->hierarchy(),
                rawIdx_B->hierarchy(),
                quotHierarchy);


    Index* result = new Index();
    rawIdx_B->permute(permutation, *result);

    result->sizeCompute();
    result->resetFloor(result->heightAboveBottom()+1 - floorDepth);

    LOG_POP();
    return result;

#else
    if(quotIndex == 0) ABORT("Not supported in new implementation.");
    std::string rawIdx_B_hierarchy =  Idx_b->hierarchy() + "." + quotIndex->hierarchy();
//    for(const Index* ix=quotIndex->firstLeaf();ix!=0 and ix->isBottom();ix=ix->nodeRight())ix->bottomExpand();
    quotIndex->bottomExpandAll();

    quotHierarchy = quotIndex->hierarchy();

    // Need to set floorDepth for findPermutation
    if(floorDepth == -1){
        floorDepth = Idx_A->heightAboveBottom()+1 - Idx_A->firstFloor()->depth();
    }

    std::vector<unsigned int> permutation = findPermutation(
                Idx_a->hierarchy(),
                Idx_A->hierarchy(),
                Idx_b->hierarchy(),
                rawIdx_B_hierarchy,
                quotHierarchy);

    std::vector<bool> isIdx_b(permutation.size(),false);
    std::vector<unsigned int> permutation_Idx_b;
    std::vector<unsigned int> permutation_quotIndex;

    for(int i=0; i<permutation.size(); i++){
        int j=0;
        while(permutation[j] != i) j++;
        isIdx_b[j] = i < Idx_b->heightAboveBottom()+1;

        if(isIdx_b[j]){
            permutation_Idx_b.push_back(j);
        }else{
            permutation_quotIndex.push_back(j);
        }
    }

    for(int i=1; i<permutation_Idx_b.size(); i++){
        if(permutation_Idx_b[i] < permutation_Idx_b[i-1]) ABORT("Commented out");
    }
    for(int i=1; i<permutation_quotIndex.size(); i++){
        if(permutation_quotIndex[i] < permutation_quotIndex[i-1]) ABORT("Commented out");
    }
    Index* result = IndexFactory(Idx_b, quotIndex, isIdx_b)
            .withConstraint(&IndexConstraint::main)
            .build();

    result->sizeCompute();
    result->resetFloor(result->heightAboveBottom()+1 - floorDepth);

    LOG_POP();
    return result;

#endif
}


OperatorTree* TensorOperatorTreeWithId::build(bool check){
    bool iIsGenerated = false;
    bool jIsGenerated = false;

    if(iIndex && jIndex){
        iIndex->bottomExpandAll();
        jIndex->bottomExpandAll();
        if(iIndex->hierarchy() != base->iIndex->hierarchy() or jIndex->hierarchy() != base->jIndex->hierarchy()){
            if(quotIndex == 0) ABORT("Must supply quotIndex");
            quotIndex->bottomExpandAll();
            quotHierarchy = quotIndex->hierarchy();
        }else{
            quotHierarchy = "";
        }
        
        floorDepth = iIndex->heightAboveBottom()+1 - iIndex->firstFloor()->depth();
        if(jIndex->heightAboveBottom()+1 - jIndex->firstFloor()->depth() != floorDepth){
            ABORT("Not accounted for. Should not be a big change however");
        }

    }else if(iIndex && !jIndex){
        jIndex->bottomExpandAll();

        jIndex = generateIndex(base->iIndex, iIndex, base->jIndex);
        jIsGenerated = true;

    }else if(!iIndex && jIndex){
        jIndex->bottomExpandAll();

        iIndex = generateIndex(base->jIndex, jIndex, base->iIndex);
        iIsGenerated = true;

    }else{
        ABORT("Need to supply at least one index");
    }
    
    // **** DEBUG OUTPUT *****
    LOG_D("---- Index setup ---");
    LOG_D("Product: "<<iIndex->hierarchy()<<" <- "<<jIndex->hierarchy());
    LOG_D("Factor:  "<<base->iIndex->hierarchy()<<" <- "<<base->jIndex->hierarchy());

    /*
     * Store whether each level corresponds to the identity factor or to
     * the operator factor
     */

    std::vector<std::string> iHierarchy, jHierarchy, qHierarchy;
    iHierarchy=tools::splitString(iIndex->hierarchy(), '.');
    jHierarchy=tools::splitString(jIndex->hierarchy(), '.');
    qHierarchy=tools::splitString(quotHierarchy, '.');
    for(int i=0; i<iHierarchy.size(); i++){
        int found=0;
        for(int j=0; j<qHierarchy.size(); j++){
            if(qHierarchy[j] == iHierarchy[i]){
                found++;
            }
        }

        int idx=0;
        for(int j=0; j<i; j++){
            if(iHierarchy[j] == iHierarchy[i]){
                idx++;
            }
        }

        iIdentities.push_back(idx<found);
    }
    for(int i=0; i<jHierarchy.size(); i++){
        int found=0;
        for(int j=0; j<qHierarchy.size(); j++){
            if(qHierarchy[j] == jHierarchy[i]){
                found++;
            }
        }

        int idx=0;
        for(int j=0; j<i; j++){
            if(jHierarchy[j] == jHierarchy[i]){
                idx++;
            }
        }

        jIdentities.push_back(idx<found);
    }

    // **** DEBUG OUTPUT *****
    LOG_D("---- Identities ---");
    LOG_D("I: ");
    for(auto v: iIdentities) LOG(v<<" ");
    LOG_D("J: ");
    for(auto v: jIdentities) LOG(v<<" ");

    /*
     * Ensure either ZGtimesId or IdtimesZG structure in floor
     * and store which one in floorZGtimesId
     */

    for(int i=0; i<floorDepth; i++){
        if(iIdentities[iIdentities.size()-i-1] != jIdentities[jIdentities.size()-i-1])
            ABORT("Not Kronecker product structure in floor, choose different permutation or decrease floor depth");
    }
    
    bool gradientFound=false;
    for(int i=iIdentities.size()-floorDepth+1; i<iIdentities.size(); i++){
        if(gradientFound && (iIdentities[i]!=iIdentities[i-1]))
            ABORT("Not Kronecker product structure in floor, choose different permutation or decrease floor depth");

        if(iIdentities[i] && !iIdentities[i-1]){
            floorZGtimesId=true;
            gradientFound=true;
        }else if(!iIdentities[i] && iIdentities[i-1]){
            floorZGtimesId=false;
            gradientFound=true;
        }
    }

    // Just to be sure
    iIndex->sizeCompute();
    jIndex->sizeCompute();
    base->iIndex->sizeCompute();
    base->jIndex->sizeCompute();

    InternalOperatorTree* result = new InternalOperatorTree(this, iIndex, jIndex);

    // Set parallel distribution (see OperatorTree::OperatorTree)
    ParallelOperator par(result);
    par.syncNormCost();
    par.setDistribution();

    if(check){
        if((iIndex->size()/base->iIndex->size())!=(jIndex->size()/base->jIndex->size()))
            PrintOutput::DEVmessage(result->name+" is not strict tensor product, test inapplicable");
        else {
            Coefficients c(base->jIndex);
            Coefficients c1(base->iIndex);
            Coefficients cCheck2(iIndex);

            c.setToRandom();
            for(int k=0;k<c.size();k++)c.data()[k]=double(k);
            Coefficients* cFull = tensorCoefficients(c, jIndex);

            base->apply(1., c, 0., c1);
            Coefficients* cCheck = tensorCoefficients(c1, iIndex);

            result->apply(1., *cFull, 0., cCheck2);

            *cCheck-=cCheck2;
            if(cCheck->isZero(1.e-9)){
                PrintOutput::DEVmessage("OK tensor "+base->name+" with Id");
            }else{
                PrintOutput::warning("FAILED tensor "+base->name+" with Id");
            }
            delete cFull;
            delete cCheck;
        }
        
    }

    if(quotIndex)quotIndex->bottomUnexpandAll();
    if(iIndex)iIndex->bottomUnexpandAll();
    if(jIndex)jIndex->bottomUnexpandAll();
    return result;
}


Coefficients* TensorOperatorTreeWithId::tensorCoefficients(const Coefficients& cFactor, const Index* iFull){
    Coefficients* cFull = new Coefficients(iFull);
    std::vector<std::complex<double>*> values;
    std::vector<std::complex<double>*> valuesFactor;
    cFull->pointerToC(values);
    const_cast<Coefficients&>(cFactor).pointerToC(valuesFactor);

    std::vector<std::string> factorHierarchy = tools::splitString(cFactor.idx()->hierarchy(), '.');
    std::vector<std::string> fullHierarchy = tools::splitString(cFull->idx()->hierarchy(), '.');
    std::vector<bool> factorLevels;
    for(int i=0; i<fullHierarchy.size(); i++){
        bool factor=false;
        for(auto& v: factorHierarchy){
            if(v==fullHierarchy[i]){
                factor=true;
                break;
            }
        }
        factorLevels.push_back(factor);
    }

//    for(const Index* ix= cFull->idx()->firstLeaf();ix!=0 and ix->isBottom();ix=ix->nodeRight())ix->bottomExpand();
//    for(const Index* ix=cFactor.idx()->firstLeaf();ix!=0 and ix->isBottom();ix=ix->nodeRight())ix->bottomExpand();
    cFull->idx()->bottomExpandAll();
    cFactor.idx()->bottomExpandAll();

    int rank=0;
    for(const Index* l=cFull->idx()->firstLeaf(); l!=0; l=l->nextLeaf()){
        std::vector<const Index*> _path = l->path();
        _path.push_back(l);
        _path.erase(_path.begin());

        std::vector<double> path;
        int depth=0;
        for(const Index* i: _path){
            if(factorLevels[depth]){
                path.push_back(i->physical());
            }
            depth++;
        }

        const Index* corresponding = cFactor.idx();
        int posInBottom;
        for(double phys: path){
            bool found=false;
            for(int k=0; k<corresponding->childSize(); k++){
                if(std::abs(corresponding->basisAbstract()->physical(k) - phys) < 1.e-12){
                    corresponding = corresponding->child(k);
                    found=true;
                    posInBottom=k;
                    break;
                }
            }
            if(not found) ABORT("Could not find path");
        }
        if(corresponding->childSize() != 0) ABORT("Expected leaf");

        *values[rank++] = *valuesFactor[corresponding->levelRank()];
    }

    cFull->idx()->bottomUnexpandAll();
    cFactor.idx()->bottomUnexpandAll();
    return cFull;
}
