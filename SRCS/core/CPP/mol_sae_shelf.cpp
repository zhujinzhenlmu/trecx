// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "mol_sae_shelf.h"
#include "time.h"
#include "timer.h"

using namespace std;
#include "eigenNames.h"


mol_sae_shelf mol_sae_shelf::main;

void mol_sae_shelf::xch_data_clear()
{
  for(unsigned int i=0;i<xch_data.size();i++)
    for(unsigned int j=0;j<xch_data[i].size();j++)
      if(xch_data[i][j]!=NULL)
        delete xch_data[i][j];
  xch_data.clear();
}

void mol_sae_shelf::clear_rho2(){

}

mol_sae_shelf::mol_sae_shelf(){

  //default value
  sinv_help = MatrixXcd::Zero(0,0);

  ylm_0.resize(0);
}

mol_sae_shelf::~mol_sae_shelf()
{
  for(unsigned int i=0;i<zero_vec_proj.size();i++)
    delete zero_vec_proj[i];
  for(unsigned int i=0;i<s0_zero_vec_proj.size();i++)
    delete s0_zero_vec_proj[i];
  for(unsigned int i=0;i<hev_proj.size();i++)
    delete hev_proj[i];
  for(unsigned int i=0;i<sX_hev_proj.size();i++)
    delete sX_hev_proj[i];

  xch_data_clear();
  clear_rho2();
}


mo_store_v1::mo_store_v1(){

  ABORT("Error");
//  this->ne = import_data::main.numberOfIntervals;
//  this->mmax = import_data::main.mmax;
//  this->lmax = import_data::main.lmax;
  op.clear();
  s.clear();
}

mo_store_v1::~mo_store_v1(){
  this->clear();
}

TIMER(shelf,)
void mo_store_v1::add(string name, Vector3i rfn, MatrixXcd a){

  START(shelf)
  if(std::find(op.begin(),op.end(),name)==op.end()){  //operator doesn't exist need to find it
      s.push_back(vector<vector<vector<MatrixXcd* > > >());
      s[s.size()-1].reserve(ne+1);
      for(int n=0;n<ne;n++){
          s[s.size()-1].push_back(vector<vector<MatrixXcd* > >());
          s[s.size()-1][n].reserve(lmax+1);
          for(int l=0;l<=lmax;l++){
              s[s.size()-1][n].push_back(vector<MatrixXcd* >());
              s[s.size()-1][n][l].reserve(2*min(l,mmax)+1);
              for(int m=-min(l,mmax);m<=min(l,mmax);m++){
                  MatrixXcd *aa = NULL;
                  s[s.size()-1][n][l].push_back(aa);
                }
            }
        }
      op.push_back(name);
    }

  // Find the first index
  int ind;
  for(unsigned int i=0;i<op.size();i++)
    if(op[i]==name)
      ind = i;

  s[ind][rfn(0)][rfn(1)][rfn(2)+min(rfn(1),mmax)] = new MatrixXcd;
  *s[ind][rfn(0)][rfn(1)][rfn(2)+min(rfn(1),mmax)] = a;

  STOP(shelf)
}

MatrixXcd* mo_store_v1::retrieve(string name, Vector3i rfn){
  START(shelf)
  if(std::find(op.begin(),op.end(),name)==op.end()) {STOP(shelf); return NULL;}
  int ind;
  for(unsigned int i=0;i<op.size();i++)
    if(op[i]==name)
      ind = i;
  STOP(shelf)
  return s[ind][rfn(0)][rfn(1)][rfn(2)+min(rfn(1),mmax)];
}

void mo_store_v1::clear()
{
  for(int i=0;i<op.size();i++)
    for(int n=0;n<ne;n++)
      for(int l=0;l<=lmax;l++)
        for(int m=-min(l,mmax);m<=min(l,mmax);m++){
            if(s[i][n][l][m+min(l,mmax)]!=NULL){
                delete s[i][n][l][m+min(l,mmax)];
                s[i][n][l][m+min(l,mmax)] = NULL;
              }
          }
  s.clear();
  op.clear();
}

