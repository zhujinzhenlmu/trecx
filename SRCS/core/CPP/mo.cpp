// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "mo.h"

#include "mpiWrapper.h"
#include "printOutput.h"
#include "gaunt.h"
#include <fstream>
#include <iomanip>
//#include <boost/math/special_functions/spherical_harmonic.hpp>
#include "timer.h"
#include "gaunt.h"
#include "printOutput.h"
#include "axis.h"
#include "sphericalHarmonicReal.h"
#include "parameters.h"
#include "algebra.h"

#include "basisMolecularOrbital.h"
#include "basisDvr.h"

using namespace std;
#include "eigenNames.h"
using Eigen::Dynamic;


const double mo::rho_eps  = 1e-14;
mo* mo::main=0;

mo::mo(columbus_data *Col_data, std::vector<Axis> *ax, bool compute_sce): S(Col_data),axis(ax){

    col_data=new QuantumChemicalData();
    *col_data = *Col_data;

    int no_sao = S.number_saos();
    Col_data->read_mo_coefficients(coef,no_sao);      //Returns a matrix with size:  num. of molecular orbitals x num. of atomic orbitals

    if(ax!=0) info_check();
    MPIwrapper::Barrier_slowButHard();

    vlqq.resize(0);
    R_dir.resize(0);
    R_xch.resize(0);
    R_O.resize(0);
    poly_val.resize(0);
    poly_der.resize(0);
    poly_val_nuclear.resize(0);
    cl_helper.resize(0);

    if(compute_sce){
        find_angular_componenets();

        //set the sce expansions after setting lmax_g and mmax_g
        setup_sce_expansions();
    }

    mat_tol = 1e-10;
}

mo::mo(QuantumChemicalInput & ChemDat, std::vector<Axis> *ax, bool compute_sce): S(ChemDat),axis(ax){

    col_data=new QuantumChemicalData();
    *col_data=ChemDat;

    //    int no_sao = S.number_saos();
    //    col_data->read_mo_coefficients(coef,no_sao);      //Returns a matrix with size:  num. of molecular orbitals x num. of atomic orbitals
    coef=ChemDat.moCoef;

    if(ax!=0) info_check();
    MPIwrapper::Barrier_slowButHard();

    vlqq.resize(0);
    R_dir.resize(0);
    R_xch.resize(0);
    R_O.resize(0);
    poly_val.resize(0);
    poly_der.resize(0);
    poly_val_nuclear.resize(0);
    cl_helper.resize(0);

    if(compute_sce){
        find_angular_componenets();

        //set the sce expansions after setting lmax_g and mmax_g
        setup_sce_expansions();
    }

    mat_tol = 1e-10;

//    if(axis!=0){
//        cout<<"set up MOs, axis "<<axis<<endl;
//        for(int n=0;n<radialSize();n++){
//            if(radialDef(n).upBound()>radialBox()+1.e-12)break;
//            const BasisSet bas(radialDef(n));
//            _moNdim.push_back(new BasisMolecularOrbital(*this,bas,3,2));
//        }
//    }
}

mo::~mo(){
    for(int i=0;i<gaunt_xch.size();i++)  delete gaunt_xch[i];
    gaunt_xch.clear();
    delete col_data;
    for(int k=0;k<_moNdim.size();k++)delete _moNdim[k];
}

Axis *mo::AxisForString(string s){
    if(newCode() and s=="Rn")ABORT("do not use");
    for(unsigned int k=0;k<(*axis).size();k++)
        if((*axis)[k].basDef[0].coor.name()==s){
            return &((*axis)[k]);
        }
    ABORT("Couldn't find axis "+s);
}

Axis* mo::radialAxis(){
    for(unsigned int k=0;k<(*axis).size();k++)
        if((*axis)[k].basDef[0].coor.name()=="Rn"){
            return &((*axis)[k]);
        }
    ABORT("Couldn't find radial axis");

}
BasisSetDef mo::radialDef(int N){
    BasisSetDef def=radialAxis()->basDef[N];
    //    if(abs(def.upBound()-Algebra("Rg").val(0.))<1.e-10){
    //        def.last=true;
    //    }
    return def;
}
double mo::radialBox(){
    return radialAxis()->boxsize();
}
double mo::radialSize(){
    return radialAxis()->basDef.size();
}


int mo::numberOfelementBeforecomplexscaling()
{
    Axis *Rn= radialAxis();

    double boxsize = Rn->boxsize();
    for(unsigned int k=0;k<Rn->basDef.size();k++){
        if(abs(boxsize-Rn->basDef[k].upBound())<1e-14) return k+1;
    }
    ABORT("Couldn't find number of elements");
}

int mo::orderonElement(int n)
{
    return radialDef(n).order;
}

void mo::check_box_size(string Mess,double Boxsize, double Eps){

    ///< Check if radial box size is sufficient to fit the molecular orbitals; It can effect zero projectors

    Matrix<double, Dynamic, 3> xyz = Matrix<double, Dynamic, 3>::Zero(3,3);
    xyz.row(0) << Boxsize, 0, 0;
    xyz.row(1) << 0, Boxsize, 0;
    xyz.row(2) << 0, 0, Boxsize;

    MatrixXd val;
    values(val,xyz);

    if(val.lpNorm<Eigen::Infinity>()>Eps){
        PrintOutput::paragraph();
        PrintOutput::warning(Str("Orbitals exceed ")+Mess+"[0,"+Boxsize+"]\n may cause erroneous results or breakdown");
        PrintOutput::newLine();
        PrintOutput::newRow();
        PrintOutput::rowItem("Orbital index");
        PrintOutput::rowItem("Coord");
        PrintOutput::rowItem("Value");
        PrintOutput::newRow();

        for(int i=0;i<val.rows();i++)
            for(int j=0;j<val.cols();j++)
                if(abs(val(i,j))>Eps){
                    PrintOutput::rowItem(i);
                    PrintOutput::rowItem(j);
                    PrintOutput::rowItem(val(i,j));
                    PrintOutput::newRow();
                }
        PrintOutput::newLine();
        PrintOutput::flush();
    }

    gauge_boundary=-1;
    if(gauge_boundary==-1)gauge_boundary=numberOfelementBeforecomplexscaling();

    MPIwrapper::Barrier();
}

void mo::info_check(){

    ///< computes overlap and kinetic energy integrals and compares with columbus output; Check for orbitals

    MatrixXcd M;
    matrix_1e(M,"1");

    if(M.rows() != col_data->Overlap.rows() or M.cols()!=col_data->Overlap.cols())
        ABORT("Sizes of overlap matrices did not match, Error in reading MO's");

    for(int i=0;i<M.rows();i++)
        for(int j=0;j<M.cols();j++)
            if(abs(M(i,j)-col_data->Overlap(i,j))>1e-9)
                ABORT("Overlap Matrix Elements did not match, could be some error in reading MO's ");

    matrix_1e(M,"dx|dx");

    if(M.rows() != col_data->KineticEnergy.rows() or M.cols()!=col_data->KineticEnergy.cols())
        ABORT("Sizes of Kinetic Energy matrices did not match, Error in reading MO's");

    for(int i=0;i<M.rows();i++)
        for(int j=0;j<M.cols();j++)
            if(abs(M(i,j)-col_data->KineticEnergy(i,j))>1e-9)
                ABORT("Kinetic Energy Matrix Elements did not match, could be some error in reading MO's ");

    PrintOutput::message("MO's read correctly - Overlap and Kinetic energy verified");

    // warn if orbitals exceed unscaled or length gauge radii
    check_box_size("unscaled region",radialAxis()->boxsize(),1.e-10);
    if(Algebra::isAlgebra("Rg") and Algebra("Rg").val(0.)!=0.)check_box_size("length gauge region",Algebra("Rg").val(0).real(),1.e-8);
}

void mo::values(MatrixXd& val, Matrix<double, Dynamic, 3>& xyz){
    S.values(val,xyz);
    val = coef*val;
}

void mo::values(VectorXd& val, Matrix<double, Dynamic, 3>& xyz, int fun_index){

    MatrixXd v;
    S.values(v,xyz);
    val = (coef*v).row(fun_index);

}

void mo::derivatives(MatrixXd& val, Matrix<double, Dynamic, 3>& xyz, string wrt){

    MatrixXd v;
    S.derivatives(v,xyz,wrt);
    val = coef*v;

}

void mo::derivatives(VectorXd& val, Matrix<double, Dynamic, 3>& xyz, int fun_index, string wrt){

    MatrixXd v;
    S.derivatives(v,xyz,wrt);
    val = (coef*v).row(fun_index);

}

void mo::matrix_1e(MatrixXcd &result, string op)
{
    matrix_1e(result,op,NULL);
}

double mo::vEE(int I1, int I2, int J1, int J2) const{
    return col_data->vee->val_at(I1,I2,J1,J2);
}

void mo::matrix_2e(My4dArray &result)
{
    result.resize(coef.rows(),coef.rows(),coef.rows(),coef.rows());
    Eigen::Map<Eigen::ArrayXd>(result.A.data(),result.A.size()) = Eigen::Map<Eigen::ArrayXd>(col_data->vee->data(),col_data->vee->size());
}

int mo::NumberOfOrbitals()
{
    return coef.rows();
}

void mo::matrix_1e(MatrixXcd &result, string op, mo *b){

    ///< computes matrices of type <\Phi_i | \hat{O} | |Phi_j>

    if(b==this or b==NULL){
        MatrixXcd* r_temp = mo_X_mo.retrieve(op,this);
        if(r_temp!=NULL){
            result = *r_temp;
            return;
        }
    }
    MatrixXcd res;
    if(b==NULL){
        if(op=="kin" or op=="Kinetic")  result = this->col_data->KineticEnergy.cast<std::complex<double > >();
        else if(op=="Laplacian")result = 2*this->col_data->KineticEnergy.cast<std::complex<double > >();
        else if(op=="pot" or op=="NucCoulomb" or op=="Coulomb")  result = this->col_data->Potential.cast<std::complex<double > >();
        else if(op=="h")result = (this->col_data->KineticEnergy+this->col_data->Potential).cast<std::complex<double > >();
        else{
            S.sao_matrix_1e(res,op);
            result = coef*res*coef.adjoint();
        }

    }
    else{
        S.sao_matrix_1e(res,op,&b->S);
        result = coef*res*b->coef.adjoint();
    }
    if(b==NULL)
        b=this;

    std::ofstream ofs{ op + "_mol.txt" };
    ofs << result << "\n\n\n";
    mo_X_mo.add(op,b,result);
}

void mo::find_angular_componenets(){

    MatrixXd ov = MatrixXd::Zero(NumberOfOrbitals(),NumberOfOrbitals());

    cout.precision(5);
    //  for(int n=0;n<numberOfelementBeforecomplexscaling();n++){
    for(int n=0;n<radialSize();n++){

        //        PrintOutput::rowItem(n);
        double errortolerance = this->col_data->cif.sce_cutoff;

        int m_quad = S.A.pg.mmax;
        int l_quad = S.A.pg.lmax;
        int g_quad = orderonElement(n)+3;

        double error_r = 2*errortolerance;
        MatrixXd test_r = MatrixXd::Random(NumberOfOrbitals(),NumberOfOrbitals());
        while(error_r>errortolerance){

            VectorXd r_quad(g_quad),r_weig(g_quad);
            quadRuleRn(n,g_quad,r_quad,r_weig);

            double error_m = 2*errortolerance;
            MatrixXd test_m = MatrixXd::Random(NumberOfOrbitals(),NumberOfOrbitals());
            while(error_m>errortolerance){

                double error_l = 2*errortolerance;

                MatrixXd test_l = MatrixXd::Random(NumberOfOrbitals(),NumberOfOrbitals());
                while(error_l>errortolerance){

                    MatrixXd res = MatrixXd::Zero(NumberOfOrbitals(),NumberOfOrbitals());

                    Matrix<double, Dynamic, 3> xyz;
                    VectorXd t_quad,t_weig,phi_quad,phi_weig;
                    setup_3dquadrature(xyz,t_quad,t_weig,phi_quad,phi_weig,r_quad,l_quad,m_quad);

                    MatrixXd val;
                    values(val,xyz);

                    int count=0;
                    for(int r=0;r<r_quad.size();r++)
                        for(int t=0; t<t_quad.size(); t++)
                            for(int p=0; p<phi_quad.size(); p++){
                                res += t_weig(t)*phi_weig(p)*r_quad(r)*r_quad(r)*r_weig(r)*val.col(count)*val.col(count).adjoint();
                                count++;
                            }

                    error_l = (res-test_l).array().abs().maxCoeff();
                    test_l  = res;

                    if(error_l>errortolerance){
                        if(col_data->no_atoms==1)  l_quad+=1;
                        else                       l_quad+=1;
                    }
                    if(error_l<errortolerance){
                        //since its converged - set m_quad to previous value
                        if(col_data->no_atoms==1)  l_quad-=1;
                        else                       l_quad-=1;
                    }
                    // cout<<"  error: "<<error_l<<endl;
                }

                error_m = (test_m-test_l).array().abs().maxCoeff();
                test_m  = test_l;

                if(error_m>errortolerance){
                    if(col_data->no_atoms==1)  m_quad+=1;
                    else                       m_quad+=1;
                }
                if(error_m<errortolerance){
                    //since its converged - set m_quad to previous value
                    if(col_data->no_atoms==1)  m_quad-=1;
                    else                       m_quad-=1;
                }
            }
            error_r = (test_r-test_m).array().abs().maxCoeff();
            test_r  = test_m;

            if(error_r>errortolerance){
                g_quad+=3;
            }
            if(error_r<errortolerance){
                //since its converged - set m_quad to previous value
                g_quad-=3;
            }
        }
        mo_l.push_back(l_quad);
        mo_m.push_back(m_quad);
        g_o.push_back(g_quad);


        ov += test_r;
    }
}

void mo::quadRuleRn(unsigned int Elem, unsigned int N, VectorXd &Pts, VectorXd &Wgs){
    BasisSetDef def=radialDef(Elem);
    std::unique_ptr<BasisIntegrable> bas(new BasisDVR(def));
    //    if(def.upBound()-1.e-12<radialBox())return;

    UseMatrix pts,wgs;
    bas->quadRule(N,pts,wgs);
    Pts=VectorXd(pts.size());
    Wgs=VectorXd(wgs.size());
    double box=radialBox();
    for(int k=0;k<pts.size();k++){
        Pts(k)=pts(k).real();
        Wgs(k)=wgs(k).real();
        if(abs(Pts(k)-box)<1.e-12)Wgs(k)=0.; // force =0 at end of box
    }

}

void mo::quadRuleRnNew(const BasisIntegrable * B, unsigned int N, VectorXd &Pts, VectorXd &Wgs){
    UseMatrix pts,wgs;
    B->quadRule(N,pts,wgs);
    Pts=VectorXd(pts.size());
    Wgs=VectorXd(wgs.size());
    double box=radialBox();
    for(int k=0;k<pts.size();k++){
        Pts(k)=pts(k).real();
        Wgs(k)=wgs(k).real();
        if(abs(Pts(k)-box)<1.e-12)Wgs(k)=0.; // force =0 at end of box
    }
}


TIMERSAMPLE(mo_matrix_1e,)
void mo::matrix_1e(MatrixXcd& result, string op, int n, int l1, int m1){

    ///<  computes matrices of type <\Phi_i | \hat{O} | |chi_{nk} Y_{lm}>; matrices between molecular orbitals and polynomial times spherical harmonics

    START(mo_matrix_1e);
    if(poly_val.size()==0)setup_pol_val();

    MatrixXd *poly_rad_val,*poly_rad_der;
    poly_rad_val = &poly_val[n];
    poly_rad_der = &poly_der[n];

    // Extra conditions to improve speed
    result = MatrixXcd::Zero(coef.rows(),poly_rad_val->rows());
    if(radialBox()<radialDef(n).lowBound()+1.e-12){ STOP(mo_matrix_1e);return;}
    if(op=="1" and (l1>mo_l[n] or abs(m1)>mo_m[n])) {STOP(mo_matrix_1e); return;}
    if(op=="z_vg" or op=="z" and (abs(m1)>mo_m[n] or l1>mo_l[n]+1)) {STOP(mo_matrix_1e); return;}
    if(op=="x_vg" or op=="x" and (abs(m1)>mo_m[n]+1 or l1>mo_l[n]+1)) {STOP(mo_matrix_1e); return;}

    //storage
    Vector3i aaa; aaa << n,l1,m1;
    MatrixXcd* r_temp = mo_poly.retrieve(op,aaa);
    if(r_temp!=NULL){
        if(r_temp->rows()==0 and r_temp->cols()==0) {
            STOP(mo_matrix_1e);
            return;
        }
        else{
            result = *r_temp;
            STOP(mo_matrix_1e);
            return;
        }
    }

    double ub = radialDef(n).upBound();

    //setting up the r quadrature.
    VectorXd r_quad,r_weig;
    quadRuleRn(n,g_o[n],r_quad,r_weig);


    if(n>sce.size())ABORT("outside sce");
    MatrixXcd* trans_gauss = &sce[n];    // Has a factor "r" multiplied with it
    MatrixXcd* gauss_der_r;
    if(op=="Laplacian")gauss_der_r = &sce_der[n];

    result = MatrixXcd::Zero(NumberOfOrbitals(),poly_rad_val->rows());
    if(op=="Coulomb")
        coulomb_gauss_poly(result,n,l1,m1);
    else{
        int count = 0;
        for(int mi=-mo_m[n]; mi<=mo_m[n]; mi++)
            for(int li=abs(mi); li<=mo_l[n]; li++){

                if(op=="1"){
                    if(l1==li and m1==mi)
                        result += trans_gauss->block(0,count,coef.rows(),g_o[n]).conjugate()*r_weig.asDiagonal()*poly_rad_val->transpose();
                }
                else if(op=="z"){
                    double gf = Gaunt::main.coeff(li,1,l1,mi,0,m1);
                    if(ub>500)ABORT("cannot handle radii beyond r=500");
                    if(gf!=0)
                        result += trans_gauss->block(0,count,coef.rows(),g_o[n]).conjugate()*r_weig.asDiagonal()*r_quad.asDiagonal()*(*poly_rad_val).adjoint()*gf*sqrt(4.0*math::pi/3.0);
                }
                else if(op=="x"){
                    double gf = Gaunt::main.coeff(li,1,l1,mi,-1,m1)-Gaunt::main.coeff(li,1,l1,mi,1,m1);
                    if(gf!=0)
                        result += trans_gauss->block(0,count,coef.rows(),g_o[n]).conjugate()*r_weig.asDiagonal()*r_quad.asDiagonal()*(*poly_rad_val).adjoint()*gf*sqrt(2.0*math::pi/3.0);
                }
                else if(op=="y"){
                    double gf = Gaunt::main.coeff(li,1,l1,mi,-1,m1)+Gaunt::main.coeff(li,1,l1,mi,1,m1);
                    if(gf!=0)
                        result += trans_gauss->block(0,count,coef.rows(),g_o[n]).conjugate()*r_weig.asDiagonal()*r_quad.asDiagonal()*(*poly_rad_val).adjoint()*gf*sqrt(2.0*math::pi/3.0)*complex<double>(0,1);
                }
                else if(op=="z_vg"){
                    if(mi==m1 and (l1==li-1 or l1==li+1)){
                        double aa = Gaunt::main.twoYintegrals(li,l1,mi,m1,"q");
                        double bb = Gaunt::main.twoYintegrals(li,l1,mi,m1,"(1-q^2)d");
                        result += 2*math::pi*complex<double>(0,1)*trans_gauss->block(0,count,coef.rows(),g_o[n]).conjugate()*r_weig.asDiagonal()*(poly_rad_der->transpose()*aa
                                                                                                                                                  + r_quad.array().inverse().matrix().asDiagonal()*poly_rad_val->transpose()*(bb-aa));
                    }
                }
                else if(op=="x_vg"){
                    if(mi==m1+1 or mi==m1-1){
                        result += complex<double>(0,1)*trans_gauss->block(0,count,coef.rows(),g_o[n]).conjugate()*r_weig.asDiagonal()*(*poly_rad_der).adjoint()*Gaunt::main.twoYintegrals(li,l1,mi,m1,"sqrt(1-q^2)")*math::pi;
                        result -= complex<double>(0,1)*trans_gauss->block(0,count,coef.rows(),g_o[n]).conjugate()*r_weig.asDiagonal()*r_quad.array().inverse().matrix().asDiagonal()*(*poly_rad_val).adjoint()
                                *(Gaunt::main.twoYintegrals(li,l1,mi,m1,"sqrt(1-q^2)")+Gaunt::main.twoYintegrals(li,l1,mi,m1,"sqrt(1-q^2)qd"))*math::pi;
                    }
                    if(mi==m1+1 and m1!=0)
                        result -= complex<double>(0,m1)*trans_gauss->block(0,count,coef.rows(),g_o[n]).conjugate()*r_weig.asDiagonal()*r_quad.array().inverse().matrix().asDiagonal()*(*poly_rad_val).adjoint()*Gaunt::main.twoYintegrals(li,l1,mi,m1,"1/sqrt(1-q^2)")*math::pi;
                    if(mi==m1-1 and m1!=0)
                        result += complex<double>(0,m1)*trans_gauss->block(0,count,coef.rows(),g_o[n]).conjugate()*r_weig.asDiagonal()*r_quad.array().inverse().matrix().asDiagonal()*(*poly_rad_val).adjoint()*Gaunt::main.twoYintegrals(li,l1,mi,m1,"1/sqrt(1-q^2)")*math::pi;
                }
                else if(op=="Laplacian"){
                    if(li==l1 and mi==m1){
                        result += gauss_der_r->block(0,count,coef.rows(),g_o[n]).conjugate()*r_weig.asDiagonal()*(poly_rad_der->transpose());
                        result += l1*(l1+1)*trans_gauss->block(0,count,coef.rows(),g_o[n]).conjugate()
                                *r_weig.asDiagonal()
                                *r_quad.array().inverse().matrix().asDiagonal()
                                *r_quad.array().inverse().matrix().asDiagonal()
                                *(*poly_rad_val).adjoint();
                    }
                }
                else if(op=="Lz"){
                    if(li==l1 and mi==m1)
                        result += mi*trans_gauss->block(0,count,coef.rows(),g_o[n]).conjugate()*r_weig.asDiagonal()*poly_rad_val->transpose();
                }
                else if(op=="L^2"){
                    ABORT("here - trying to use L^2 - abort for debugging purposes only");
                    if(li==l1 and mi==m1)
                        result += li*(li+1)*trans_gauss->block(0,count,coef.rows(),g_o[n]).conjugate()*r_weig.asDiagonal()*poly_rad_val->transpose();
                }
                else{
                    ABORT("Undefined operator "+op);
                }
                count+=g_o[n];
            }
    }

    // forbid any amplitude on last element
    if(abs(radialBox()-radialDef(n).upBound())<1.e-12)
        result.col(result.cols()-1)*=0.;

    if(result.isZero(1e-14))mo_poly.add(op,aaa,MatrixXcd::Zero(0,0));
    else                    mo_poly.add(op,aaa,result);

    STOP(mo_matrix_1e);
}

void mo::setup_cl_helpers(){

    ///< helper function for coloumb matrix elements between polynomial times spherical harmonics

    int lmax = AxisForString("Eta")->basDef[0].order;

    cl_helper.resize(col_data->no_atoms);

    for(int na=0;na<col_data->no_atoms;na++){

        cl_helper[na].resize(numberOfelementBeforecomplexscaling());
        for(int n=0;n<numberOfelementBeforecomplexscaling();n++){          //start from first interval
            cl_helper[na][n].resize(2*lmax+1);
            for(int L=0;L<=2*lmax;L++)
                cl_helper_helper(cl_helper[na][n][L],na,n,0,0,L);
        }
    }

}

void mo::cl_helper_helper(MatrixXcd& res, int na, int n, int l1, int l2, int L){

    ///< helper function for coloumb matrix elements between polynomial times spherical harmonics; computes matrices for operator r_<^L / r_>^{L+1}

    ABORT("Not implemented");

}

TIMER(mo_coloumb_sob,)
void mo::coulomb_setofbf(MatrixXcd &res, int n, int l1, int m1, int l2, int m2){

    ///<  Puts together pieces from cl_helper to compute the coloumb integrals

    START(mo_coloumb_sob);
    if(cl_helper.size()==0)
        setup_cl_helpers();

    res = MatrixXcd::Zero(cl_helper[0][n][0].rows(),cl_helper[0][n][0].cols());

    for(int na=0;na<col_data->no_atoms;na++){
        double r_0=sqrt(std::pow(col_data->cord[na](0),2)+std::pow(col_data->cord[na](1),2)+std::pow(col_data->cord[na](2),2));
                                                                   double theta0;
                                 if(r_0!=0)theta0 = acos(col_data->cord[na](2)/r_0);
                else theta0 = 0.0;
                        double phi0;
                if(abs(col_data->cord[na](0))<1e-14 and abs(col_data->cord[na](1))<1e-14)
                                                            phi0 = 0.0;
                       else if(abs(col_data->cord[na](0))<1e-14 and col_data->cord[na](1)>1e-14)
                                   phi0 = math::pi/2.0;
                               else if(abs(col_data->cord[na](0))<1e-14 and col_data->cord[na](1)<1e-14)
                                           phi0 = 3.0*math::pi/2.0;
                                       else
                                       phi0 = atan(col_data->cord[na](1)/col_data->cord[na](0));

                int Lmax = 2*max(l1,l2);

                               MatrixXcd res1 = MatrixXcd::Zero(res.rows(),res.cols());
                       for(int L=0;L<=Lmax;L++){
                MatrixXcd *rmat_L = &(cl_helper[na][n][L]);
                complex<double > ylm_gf = 0;
                for(int M=-L;M<=L;M++){
            double gf = Gaunt::main.coeff(l2,L,l1,m2,M,m1);
            if(abs(gf)<1e-14) continue;
            //            ylm_gf += gf*boost::math::spherical_harmonic(L, M, theta0, phi0);
            ylm_gf += gf*SphericalHarmonicReal()(L, M, theta0, phi0);
        }
        res1 += (*rmat_L)*ylm_gf;
    }
    res += res1*(-col_data->charge[na]);
}
STOP(mo_coloumb_sob)
}

void mo::setup_pol_val_nuclear(){

    if(poly_val_nuclear.size()!=0) {ABORT("mo::setup_pol_val_nuclear() - values seems to be already set - wrong call");}

    for(int n=0;n<numberOfelementBeforecomplexscaling();n++){

        std::unique_ptr<BasisIntegrable> B(new BasisDVR(radialDef(n)));

        VectorXd r_quad,r_weig;
        double lb = radialDef(n).lowBound();
        double ub = radialDef(n).upBound();
        int qmax = g_o[n];

        vector<double > pts;
        pts.push_back(lb);
        for(int na=0;na<col_data->no_atoms;na++){
            double r_0 = sqrt(std::pow(col_data->cord[na](0),2)+std::pow(col_data->cord[na](1),2)+std::pow(col_data->cord[na](2),2));
                                                                         if(r_0>lb and r_0<ub)
                                                                         pts.push_back(r_0);
        }
        pts.push_back(ub);
        if(pts.size()==2)
            p_roots(qmax,r_quad,r_weig,lb,ub);
        else if(pts.size()>2){
            r_quad = VectorXd::Zero(qmax*(pts.size()-1));
            r_weig = VectorXd::Zero(qmax*(pts.size()-1));
            for(unsigned int p=0;p<pts.size()-1;p++){
                VectorXd tr,tw;
                p_roots(qmax,tr,tw,pts[p],pts[p+1]);
                r_quad.segment(p*qmax,qmax) = tr;
                r_weig.segment(p*qmax,qmax) = tw;
            }
            qmax = r_quad.size();                         // modify qmax
        }
        else
            ABORT("mo::coulomb_gauss_poly: impossible case encountered");

        MatrixXd val(B->size(),qmax);

        UseMatrix X=UseMatrix::Zero(r_quad.rows(),r_quad.cols());
        Map<VectorXcd>(X.data(),X.size()) = r_quad.cast<complex<double> >();
        UseMatrix valUm = B->val(X);
        for(int i=0;i<B->size();i++)
            for(int k=0;k<qmax;k++)
                val(i,k) = valUm(k,i).real();
        poly_val_nuclear.push_back(val);
    }
}

std::pair<std::unique_ptr<AlgebraTrunc>, Eigen::VectorXd> get_smoothener(mo* mo_ptr, int na, int qmax = -1) {
    Eigen::VectorXd r_quad,r_weig;                                                                                                                                                                               
    std::unique_ptr<AlgebraTrunc> smooth_ptr{ };
    if(Algebra::isSpecialConstant("Rtrunc")) {
        if(!Algebra::isSpecialConstant("Rsmooth"))
            ABORT("Truncation for potentials defined, but no smoothing interval!");
        auto Rn = mo_ptr->radialDef(na);
        std::string param = "trunc[Rsmooth,Rtrunc]";
        auto low = Algebra::getParameter(0, param);
        auto up = Algebra::getParameter(1, param);
        if(Rn.upBound() >= low) {
            smooth_ptr.reset(new AlgebraTrunc(param));
            if(qmax == -1) qmax = mo_ptr->g_o[na];
            p_roots(qmax,r_quad,r_weig,Rn.lowBound(),Rn.upBound());
        }
    }
    else PrintOutput::warning("No potential cutoff defined: single active electron may not evolve freely at tSurff-radius!", 1);
    return std::make_pair(std::move(smooth_ptr), r_quad);
}

void mo::coulomb_gauss_poly(MatrixXcd &res, int n, int l, int m)
{
#ifdef __ACTIVATE_SIMPLE_PROFILER__
    clock_t start = clock();
#endif

    double lb = radialDef(n).lowBound();
    double ub = radialDef(n).upBound();
    int qmax = g_o[n];

    MatrixXcd trans_gauss = sce_nuc[n];
    VectorXd r_quad,r_weig;

    vector<double > pts;
    pts.push_back(lb);
    for(int na=0;na<col_data->no_atoms;na++){
        double r_0 = sqrt(std::pow(col_data->cord[na](0),2)+std::pow(col_data->cord[na](1),2)+std::pow(col_data->cord[na](2),2));
                                                                     if(r_0>lb and r_0<ub)
                                                                     pts.push_back(r_0);
    }
    pts.push_back(ub);
    if(pts.size()==2)
        p_roots(qmax,r_quad,r_weig,lb,ub);
    else if(pts.size()>2){
        r_quad = VectorXd::Zero(qmax*(pts.size()-1));
        r_weig = VectorXd::Zero(qmax*(pts.size()-1));
        for(unsigned int p=0;p<pts.size()-1;p++){
            VectorXd tr,tw;
            p_roots(qmax,tr,tw,pts[p],pts[p+1]);
            r_quad.segment(p*qmax,qmax) = tr;
            r_weig.segment(p*qmax,qmax) = tw;
        }
        qmax = r_quad.size();                         // modify qmax
    }
    else
        ABORT("mo::coulomb_gauss_poly: impossible case encountered");

    if(poly_val_nuclear.size()==0) setup_pol_val_nuclear();
    MatrixXd *val = &poly_val_nuclear[n];
    res = MatrixXcd::Zero(NumberOfOrbitals(),val->rows());

    if(mol_sae_shelf::main.ylm_0.size()==0) setup_ylm_0();

    for(int na=0;na<col_data->no_atoms;na++){

        double r_0 = sqrt(std::pow(col_data->cord[na](0),2)+std::pow(col_data->cord[na](1),2)+std::pow(col_data->cord[na](2),2));

                                                                     int Lmax = l+mo_l[n];//2*max(l,mo_l[n]);
                                   int Lmin=0;
                          if(l-mo_l[n] > 0)
                          Lmin = abs(l-mo_l[n]);

                MatrixXcd temp = MatrixXcd::Zero(NumberOfOrbitals(),qmax);

        //      int L_zero = 0;
        for(int L=0;L<=( AxisForString("Eta")->basDef[0].order + *max_element(mo_l.begin(),mo_l.end()));L++){
            if(L<Lmin or L>Lmax) continue;

            VectorXcd vlq(qmax);
            for(int q=0;q<qmax;q++)
                vlq(q) = std::pow(min(r_0,r_quad(q)),L)/std::pow(max(r_0,r_quad(q)),L+1);
            if(vlq.isZero(col_data->cif.sce_cutoff))  continue;

            MatrixXcd temp1 = MatrixXcd::Zero(NumberOfOrbitals(),qmax);
            int count=0;
            for(int mi=-mo_m[n]; mi<=mo_m[n]; mi++)
                for(int li=abs(mi); li<=mo_l[n]; li++){
                    complex<double > gf = 0.0;
                    for(int M=-L;M<=L;M++){
                        gf += Gaunt::main.coeff(l,L,li,m,M,mi)*mol_sae_shelf::main.ylm_0[na][L][M+L];
                    }
                    if(abs(gf)<1e-14) {count+=qmax; continue;}
                    temp1 += trans_gauss.block(0,count,NumberOfOrbitals(),qmax)*gf;
                    count+=qmax;
                }

            if(temp1.isZero(1e-14))  continue;
            MatrixXcd temp2 = MatrixXcd::Zero(NumberOfOrbitals(),qmax);
            for(int q=0;q<qmax;q++)
                temp2.col(q) += temp1.col(q) * vlq(q);

            std::pair<std::unique_ptr<AlgebraTrunc>, Eigen::VectorXd> smoothener = get_smoothener(this, n, qmax);
            if(smoothener.first != nullptr) {
                for(int q = 0; q < qmax; ++q)
                    temp2.col(q) *= smoothener.first->val(smoothener.second(q));
            }

            temp += temp2;
        }
        res += temp.conjugate() *r_weig.asDiagonal() * (*val).adjoint() * (-col_data->charge[na]) *4*math::pi;  // partner /(2L+1) is above with Y_LM_0
    }

#ifdef __ACTIVATE_SIMPLE_PROFILER__
    timer_pot += ((double)(clock()-start));
#endif

}

void mo::setup_ylm_0(){

    ///< helper function to store the values of spherical harmonics at given set of points; saves times evaluating this function many times

    for(int na=0;na<col_data->no_atoms;na++){

        mol_sae_shelf::main.ylm_0.push_back(vector<vector<complex<double > > >());

        double r_0 = sqrt(std::pow(col_data->cord[na](0),2)+std::pow(col_data->cord[na](1),2)+std::pow(col_data->cord[na](2),2));
                                                                     double theta0;
                                   if(r_0!=0)
                                   theta0 = acos(col_data->cord[na](2)/r_0);
                else
                theta0 = 0.0;
                          double phi0;
                if(abs(col_data->cord[na](0))<1e-14 and abs(col_data->cord[na](1))<1e-14)
                                                            phi0 = 0.0;
                       else if(abs(col_data->cord[na](0))<1e-14 and col_data->cord[na](1)>1e-14)
                                   phi0 = math::pi/2.0;
                               else if(abs(col_data->cord[na](0))<1e-14 and col_data->cord[na](1)<1e-14)
                                           phi0 = 3.0*math::pi/2.0;
                                       else
                                       phi0 = atan(col_data->cord[na](1)/col_data->cord[na](0));

                int Lmax = AxisForString("Eta")->basDef[0].order + *max_element(mo_l.begin(),mo_l.end());

                               for(int L=0;L<=Lmax;L++){
            mol_sae_shelf::main.ylm_0[na].push_back(vector<complex<double > >());
            for(int M=-L;M<=L;M++){
                //                mol_sae_shelf::main.ylm_0[na][L].push_back(boost::math::spherical_harmonic(L, M, theta0, phi0)/double(2*L+1));
                mol_sae_shelf::main.ylm_0[na][L].push_back(SphericalHarmonicReal()(L, M, theta0, phi0)/double(2*L+1));
            }
        }
    }
}

void mo::setup_sce_expansions(){


    // set up single centered expansions at the start up - sce of values, derivatives, and at more points for coloumb integrals
    for(int n=0;n<radialSize();n++){
        sce.push_back(MatrixXcd());
        expansion_dilmq_x_ylm(sce.back(),n);
    }
    for(int n=0;n<numberOfelementBeforecomplexscaling();n++){
        sce_der.push_back(MatrixXcd());
        expansion_dilmq_x_ylm(sce_der.back(),n,true);
    }
    for(int n=0;n<numberOfelementBeforecomplexscaling();n++){
        sce_nuc.push_back(MatrixXcd());
        expansion_dilmq_x_ylm(sce_nuc.back(),n,false,true);
    }

}
