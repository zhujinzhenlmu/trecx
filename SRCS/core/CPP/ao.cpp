// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "ao.h"

using namespace std;
#include "eigenNames.h"


ao::ao(columbus_data* col_data): pg(col_data){ 
    if(not oldCode())ABORT("not Old");
    col_data->read_contraction_coefficients(coef);                   //Returns a matrix with size:  num. of Atomic orbitals x num. of primitive gaussians
    MatrixXcd pg_mat;
    pg.matrix_1e(pg_mat,"1");                              //normalising each gaussian
    for(int i=0;i<coef.cols();i++)
        coef.col(i)/=sqrt(real(pg_mat(i,i)));

    normalize();                                           //normalising ao function
}

ao::ao(QuantumChemicalInput & ChemInp): pg(ChemInp){
    //  col_data->read_contraction_coefficients(coef);  //Returns a matrix with size:  num. of Atomic orbitals x num. of primitive gaussians

    coef=ChemInp.aoCoef;

    MatrixXcd pg_mat;
    pg.matrix_1e(pg_mat,"1");   //normalising each gaussian
    for(int i=0;i<coef.cols();i++)
        coef.col(i)/=sqrt(real(pg_mat(i,i)));

    normalize();       //normalising ao function
}


int ao::number_aos(){
    return coef.rows();
}

void ao::normalize(){

    MatrixXcd m;
    ao_matrix_1e(m,"1");
    for(int i=0;i<coef.rows();i++){
        int j;
        for(j=0;j<coef.cols();j++){
            if (coef(i,j)!=0.0)
                break;
        }
        //Different normalization factors- columbus convention
        if(pg.pow.row(j) == RowVector3i(0,0,0) or pg.pow.row(j) == RowVector3i(1,0,0) or pg.pow.row(j) == RowVector3i(0,1,0) or pg.pow.row(j) == RowVector3i(0,0,1) or pg.pow.row(j) == RowVector3i(1,1,0) or pg.pow.row(j) == RowVector3i(1,0,1) or pg.pow.row(j) == RowVector3i(0,1,1) or pg.pow.row(j) == RowVector3i(1,1,1))
            coef.row(i)*=sqrt(1.0/real(m(i,i)));
        else if(pg.pow.row(j) == RowVector3i(2,0,0) or pg.pow.row(j)==RowVector3i(0,2,0) or pg.pow.row(j)==RowVector3i(0,0,2) or pg.pow.row(j)==RowVector3i(2,1,0) or pg.pow.row(j)==RowVector3i(2,0,1) or pg.pow.row(j)==RowVector3i(1,2,0)
                or pg.pow.row(j)==RowVector3i(1,0,2) or pg.pow.row(j)==RowVector3i(0,2,1) or pg.pow.row(j)==RowVector3i(0,1,2) or pg.pow.row(j)==RowVector3i(2,1,1) or pg.pow.row(j) == RowVector3i(1,2,1) or pg.pow.row(j) == RowVector3i(1,1,2))
            coef.row(i)*=sqrt(3.0/real(m(i,i)));
        else if (pg.pow.row(j) == RowVector3i(3,0,0) or pg.pow.row(j) == RowVector3i(0,3,0) or pg.pow.row(j) == RowVector3i(0,0,3) or  pg.pow.row(j) == RowVector3i(3,1,0) or  pg.pow.row(j) == RowVector3i(3,0,1)
                 or pg.pow.row(j) == RowVector3i(1,3,0) or pg.pow.row(j) == RowVector3i(1,0,3) or pg.pow.row(j) == RowVector3i(0,3,1) or pg.pow.row(j) == RowVector3i(0,1,3) or pg.pow.row(j) == RowVector3i(3,1,1)
                 or pg.pow.row(j) == RowVector3i(1,3,1) or pg.pow.row(j) == RowVector3i(1,1,3))
            coef.row(i)*=sqrt(15.0/real(m(i,i)));
        else if(pg.pow.row(j) == RowVector3i(4,0,0) or pg.pow.row(j) == RowVector3i(0,4,0) or pg.pow.row(j) == RowVector3i(0,0,4) or pg.pow.row(j) == RowVector3i(4,0,1) or pg.pow.row(j) == RowVector3i(4,1,0) or pg.pow.row(j) == RowVector3i(1,4,0)
                or pg.pow.row(j) == RowVector3i(1,0,4) or pg.pow.row(j) == RowVector3i(0,4,1) or pg.pow.row(j) == RowVector3i(0,1,4))
            coef.row(i)*=sqrt(105.0/real(m(i,i)));
        else if(pg.pow.row(j) == RowVector3i(2,2,0) or pg.pow.row(j)==RowVector3i(2,0,2) or pg.pow.row(j)==RowVector3i(0,2,2)
                or pg.pow.row(j) == RowVector3i(2,2,1) or pg.pow.row(j) == RowVector3i(2,1,2) or pg.pow.row(j) == RowVector3i(1,2,2))
            coef.row(i)*=sqrt(9.0/real(m(i,i)));
        else if(pg.pow.row(j) == RowVector3i(5,0,0) or pg.pow.row(j) == RowVector3i(0,5,0) or pg.pow.row(j) == RowVector3i(0,0,5))
            coef.row(i)*=sqrt(945.0/real(m(i,i)));
        else if(pg.pow.row(j) == RowVector3i(3,2,0) or pg.pow.row(j) == RowVector3i(3,0,2) or pg.pow.row(j) == RowVector3i(2,3,0) or pg.pow.row(j) == RowVector3i(2,0,3) or pg.pow.row(j) == RowVector3i(0,3,2) or pg.pow.row(j) == RowVector3i(0,2,3))
            coef.row(i)*=sqrt(45.0/real(m(i,i)));
    }

}

void ao::ao_matrix_1e(MatrixXcd &result, string op, ao *b){

    MatrixXcd res;
    if(b==NULL){
        pg.matrix_1e(res,op);
        result = coef*res*coef.transpose();
    }
    else{
        pg.matrix_1e(res,op,&b->pg);
        result = coef*res*b->coef.transpose();
    }

}

void ao::values(MatrixXd& val, Matrix<double, Dynamic, 3>& xyz){

    pg.values(val,xyz);
    val = coef*val;

}

void ao::values(VectorXd& val, Matrix<double, Dynamic, 3>& xyz, int fun_index){

    MatrixXd v;
    pg.values(v,xyz);
    val = (coef*v).row(fun_index);

}

void ao::derivatives(MatrixXd& val, Matrix<double, Dynamic, 3>& xyz, string wrt){

    MatrixXd v;
    pg.derivatives(v,xyz,wrt);
    val = coef*v;

}

void ao::derivatives(VectorXd& val, Matrix<double, Dynamic, 3>& xyz, int fun_index, string wrt){

    MatrixXd v;
    pg.derivatives(v,xyz,wrt);
    val = (coef*v).row(fun_index);

}
