#include "operatorSubspace.h"

#include "projectSubspace.h"
#include "operatorTree.h"
#include "basisOrbital.h"
#include "eigenSolver.h"

OperatorSubspace::OperatorSubspace(const OperatorAbstract* Op,std::shared_ptr<const ProjectSubspace> Proj)
    :OperatorTree(Op->name,Op->iIndex,Op->jIndex),_proj(Proj),_op(Op){
    if(Proj!=0 and (Proj->iIndex!=Op->iIndex or Proj->iIndex!=Op->jIndex))
        ABORT("Operator and projector are not on the same space");
    definition=Op->def();
    _C.reset(new Coefficients(iIndex));
    _D.reset(new Coefficients(iIndex));
}

OperatorSubspace::OperatorSubspace(const OperatorAbstract *Op):OperatorSubspace(Op,0)
{
    if(iIndex->axisName()!="Hybrid")
        ABORT("cannot infer a projection for index:\n"+iIndex->str());

    const BasisOrbital * b=dynamic_cast<const BasisOrbital*>(iIndex->child(0)->basisAbstract());
    if(b==0)ABORT("Subspace does not seem to have orbital basis: "+iIndex->child(0)->strData());

    std::vector<Coefficients*>vecs,dual;
    Coefficients v(iIndex->child(1));
    for(int k=0;k<b->size();k++){
        vecs.push_back(new Coefficients(iIndex));
        dual.push_back(new Coefficients(iIndex));
        *vecs.back()->child(1)=*b->orbital(k);
        v=*b->orbital(k);
        v.conjugate(); // do we need this or not?
        iIndex->child(1)->overlap()->apply(1.,v,0.,*dual.back()->child(1));
    }
    _proj.reset(new ProjectSubspace(vecs,dual));
}

void OperatorSubspace::apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    if(_proj==0)DEVABORT("incomplete setup - no projector defined");
    *_C=Vec;
    _proj->apply(-1.,Vec,1.,*_C);
    _op->apply(A,*_C,0.,*_D);
    *_C=*_D;
    _proj->apply(-1.,*_D,1.,*_C);
    Y.scale(B);
    Y+=*_C;
}

