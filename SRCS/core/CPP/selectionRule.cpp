// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "selectionRule.h"

#include "discretization.h"
#include "index.h"
#include "axis.h"

using namespace std;

// a rule name for coordinates a,b,c has format name:a.b.c
//map<string,string> SelectionRule::operList["XY:Phi.Eta"]="<sin(Q)*cos(Q)><1-Q*Q>";

void SelectionRule::add(const std::vector<Axis> &Ax, std::string Rule)
{
}


