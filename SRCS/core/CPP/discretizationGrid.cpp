// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "discretizationDerived.h"
#include "discretizationGrid.h"
#include "indexDerived.h"
#include "operatorFloor.h"
#include "operatorMap.h"
#include "coefficientsFloor.h"
#include "qtEigenDense.h"
#include "printOutput.h"
#include "log.h"

#include "operator.h" // needed for Old path of discretization
#include "inverse.h"
#include "basisDvr.h"
#include "basisBesselCoulomb.h"
#include "basisSub.h"
#include "basisGridQuad.h"
#include "basisMat1D.h"
#include "operatorWithInverse.h"
#include "timeCritical.h"
#include "mpiWrapper.h"

using namespace std;

DiscretizationGrid::~DiscretizationGrid(){
    for(int k=0;k<mapDerivative.size();k++)delete mapDerivative[k];
}

DiscretizationGrid::DiscretizationGrid(const Index *Parent, const std::vector<string> &Axes, std::vector<std::vector<double> > Grid, std::vector<std::vector<double> > Weig, bool Gradient)
    :_parentIndex(Parent){
    _construct(Parent,Axes,Grid,Weig,Gradient);
}


/// convert to grid
///
/// Rules:
///<br> the lowest level with axisName==Axis[k] will be converted to grid
///<br> on each Axis[k], Point[k] equidistant points will be used, if Point[k]==0 default number of coefficients on axis
///<br> Limit[k][l/u=0/1] gives lower an upper bounds for the grid (end-points included), for lb<ub Points[k]>1 needed
///<br> if lb=ub and Points[k] must be > 0, if Points[k]>1, a default interval (axis length) will be used
///<br> Limit.size()==0 and Point.size(), a default quadrature grid will be generated
///<br> Gradient=true will compute gradients wrt to all grid axes
DiscretizationGrid::DiscretizationGrid(const Index *Parent, std::vector<string> Axes, std::vector<unsigned int> Point,
                                       std::vector< std::vector<double> > Boundary, bool Gradient)
    :_parentIndex(Parent)
{
    std::vector<std::vector<double>>grid(Axes.size()),weig(Axes.size());
    gridWeight(grid,weig,Axes,Point,Boundary); //this should be owned by DiscretizationGrid
    //        idx()=new IndexG(Parent,Axes,grid,weig);
    _construct(Parent,Axes,grid,weig,Gradient);
}

void DiscretizationGrid::_construct(const Index *Parent, const std::vector<string> &Axes, std::vector<std::vector<double> > Grid, std::vector<std::vector<double> > Weig, bool Gradient){
    parent=0;
    name=Parent->hierarchy()+"_grid";

    LOG_PUSH("IndexG");
    idx()=new IndexG(Parent,Axes,Grid,Weig);
    LOG_POP();
    idx()->resetFloor(Parent->firstFloor()->depth()-Parent->depth());

    // purge index
    idx()->purge(Parent->height());
    idx()->sizeCompute();
    // grid maps should always use tensor (not really elegant)
    bool oldTensor=Operator::useTensor,oldFuse=Operator::fuseOp;
    Operator::useTensor=true;
    Operator::fuseOp=false;

    _mapFromParent=0;
    _mapToParent=0;
    if(Gradient){
        for(unsigned int k=0;k<Axes.size();k++)
            mapDerivative.push_back(new OperatorMap(idx(),Parent,Axes[k]));
    }
    //    // test========================
    //    if(Boundary.size()==0)
    //    {
    //        Coefficients inp(mapFromParent()->jIndex);
    //        Coefficients out0(idx());
    //        Coefficients out1(idx());
    //        inp.treeOrderStorage();
    //        out0.treeOrderStorage();
    //        out1.treeOrderStorage();
    //        inp.setToRandom();

    //        PrintOutput::DEVmessage("testing quadrature grid");
    //        inp.setToRandom();
    //        inp.makeContinuous();
    //        Coefficients res(inp),tmp(inp);
    //        if(MPIwrapper::Size() > 1)
    //            PrintOutput::DEVwarning("for parallel, skip the test for some reason");
    //        else if(_parentIndex->inverseOverlap()==0){
    //            PrintOutput::DEVwarning("no quadrature grid test as inverseOverlap()==0 for "+_parentIndex->hierarchy());
    //        }
    //        else{
    //            mapFromParent()->apply(1.,inp,0.,out0);
    //            mapToParent()->apply(1.,out0,0.,tmp);
    //            Coefficients diff(res);
    //            if(not (diff-=inp).isZero(1.e-12)){
    //                PrintOutput::warning(Str("to/from grid failed by ","")+diff.norm()/inp.norm()+"  "+mapFromParent()->iIndex->hierarchy()
    //                                     +" <--//--> "+mapFromParent()->jIndex->hierarchy()+", converted: "+Ax);
    //            }
    //            else
    //                PrintOutput::DEVmessage("to/from quadrature grid OK");
    //        }

    //    }

    // switch back to previous settings
    Operator::useTensor=oldTensor;
    Operator::fuseOp=oldFuse;
    if(Gradient){
        for(unsigned int k=0;k<Axes.size();k++)
            mapDerivative.push_back(new OperatorMap(idx(),Parent,Axes[k]));
    }

}

const OperatorAbstract *DiscretizationGrid::mapFromParent() const {
    if(_mapFromParent==0){
        timeCritical::suspend();
        _mapFromParent.reset(new OperatorMap(idx(),_parentIndex));
        timeCritical::resume();
    }
    return _mapFromParent.get();
}

bool isMapToParent(const Index* Grid, const Index* Parent){
    if(not (Parent->basisAbstract()==Grid->basisAbstract())){
        if(Parent->basisIntegrable() and
                not BasisMat1D("1",Parent->basisIntegrable(),Parent->basisIntegrable()).mat().isIdentity(1.e-12))return false;
        if(not isMapToParent(Grid->child(0),Parent->child(0)))return false;
    }
    else
        for(int k=0;k<Grid->childSize();k++)
            if(not isMapToParent(Grid->child(k),Parent->child(k)))return false;

    return true;
}

const OperatorAbstract* DiscretizationGrid::mapToParent() const {
    if(_mapToParent==0){
        timeCritical::suspend();
        if(isMapToParent(idx(),_parentIndex))
            _mapToParent.reset(new OperatorMap(_parentIndex,idx()));
        else {
            _mapToDual.reset(new  OperatorMap(_parentIndex,idx()));
            _mapToParent.reset(new OperatorWithInverse(_mapToDual));
        }
        timeCritical::resume();
    }
    return _mapToParent.get();
}


// convert selected levels to grid
DiscretizationGrid::IndexG::IndexG(const Index *I, std::vector<unsigned int> Level, std::vector<unsigned int> Point, std::vector<double> Limit)
{
    setAxisName(I->axisName());
    setBasis(I->basisAbstract());

    setKind(I->indexKind());
    fromIndex.push_back(I);

    unsigned int kTrans=tools::locateElement(Level,I->depth()); // locate level
    if(kTrans>=Level.size() or I->basisAbstract()->isGrid()){
        // not in conversion list or is already grid
        for(unsigned int k=0;k<I->childSize();k++)
            Tree<Index>::childAdd(new IndexG(I->child(k),Level,Point,Limit));
    }
    else {
        // number of points can be specified (default = order underlying the basis)
        int nPoint=I->basisAbstract()->order();
        const BasisIntegrable* superB=dynamic_cast<const BasisIntegrable*>(BasisSub::superBas(I->basisAbstract()));
        //HACK
        if(I->axisName().find("Eta")==0){
            //Eta axes usually vary for different m's, need grid suitable for all m's
            std::string depAx=tools::stringInBetween(superB->name(),"{","}");
            depAx=depAx.substr(0,depAx.find(".")); //remove possible constraint specfication
            // now we should check whether that axis is to be converted

            nPoint=0;
            for(const Index* idx=I;idx!=0;idx=idx->nodeRight())
                nPoint=std::max(nPoint,int(idx->basisAbstract()->order()));
        }
        if(kTrans<Point.size())nPoint=Point[kTrans];



        UseMatrix points,weights;
        if(2*kTrans+1>Limit.size()){
            // no Limit specified, use quadrature grid
            if(superB->isDVR() and nPoint==superB->order())
                dynamic_cast<const BasisDVR*>(superB)->dvrRule(points,weights);
            else
                superB->quadRule(nPoint,points,weights);
        }
        else {
            // equidistant points between limits
            double dx=0;
            if(nPoint>1){
                if(Limit[2*kTrans]>=Limit[2*kTrans+1])
                    ABORT(Str("need lowerLimit<upperLimit for multiple grid points, axis and interval:")
                          +I->axisName()+Limit[2*kTrans]+Limit[2*kTrans+1]);
                dx=(Limit[2*kTrans+1]-Limit[2*kTrans])/(nPoint-1);
            }

            // determine points falling into [low,upp), [low,upp] for last element;
            double upp=superB->upBound();
            double low=superB->lowBound();
            double eps=(upp-low)*1.e-10;
            const Index* uppN=I->upperNeighbor();
            if(uppN==0)upp+=eps;   // raise by epsilon for last element

            const Index* lowN=I->lowerNeighbor();
            if(lowN==0)low-=eps;   // lower by epsilon for first element
            else       low=lowN->basisAbstract()->upBound(); // use upper boundary of lower neighbor

            vector<double> pts;
            for(unsigned int k=0;k<nPoint;k++){
                if(upp<=Limit[2*kTrans]+k*dx)break;
                if(low<=Limit[2*kTrans]+k*dx)pts.push_back(Limit[2*kTrans]+k*dx);
            }

            if(pts.size()!=0){
                points=UseMatrix(pts.size(),1),weights=UseMatrix(pts.size(),1);
                for (unsigned int n=0;n<pts.size();n++){
                    points(n)=pts[n];
                    weights(n)=dx;
                }
            }
        }

        if(points.size()>0){
            std::vector<double> pts,wgs;
            for(int k=0;k<points.size();k++){
                pts.push_back(points(k).real());
                wgs.push_back(weights(k).real());
            }
            setBasis(BasisGridQuad::factory(pts,wgs));
        }
        if(not I->isLeaf())
            for(unsigned int k=0;k<points.size();k++)
                Tree<Index>::childAdd(new IndexG(I->child(0),Level,Point,Limit));


        // converted lower indices must be equivalent
        for(int k=1;k<I->childSize() and childSize()>0;k++)
            if(not IndexG(I->child(k),Level,Point,Limit).treeEquivalent(child(0))){
                Sstr+I->child(0)->str()+Sendl;
                Sstr+I->child(k)->str()+Sendl;
                DEVABORT("branches are not equivalent after conversion - cannot convert to grid on present level "+I->axisName()+"\n"
                         +IndexG(I->child(k),Level,Point,Limit).str()
                         +"\n"+child(0)->str());
            }
    }
}
/// grid index from Index
///
/// Rules:
///<br> axisName==Axis[k] will be converted to grid given in Grid[k] with quadrature weights Weig[k]
///<br> if Grid[k].size()==0, use Weigh[k].size() quadrature points, if Weigh[k].size()==0, use Basis order() points
///<br> BasisIntegrable is evaluated, BasisGrid interpolated for Grid points, other bases cannot be converted
///<br> on finite element axes, only Grid[k] points falling into given element will be converted (-> piece-wise grid)
DiscretizationGrid::IndexG::IndexG(const Index *I, const std::vector<string> & Ax, std::vector<std::vector<double> > Grid, std::vector<std::vector<double> >  Weig)
{
    if(Weig.size()==0)Weig.resize(Ax.size());
    if(Grid.size()!=Ax.size() or Weig.size()!=Ax.size())DEVABORT("must specify grid points and weights for each axis");


    setAxisName(I->axisName());
    setBasis(I->basisAbstract());
    setKind(I->indexKind());
    fromIndex.push_back(I);

    int nAx=(std::find(Ax.begin(),Ax.end(),axisName())-Ax.begin());
    const BasisAbstract* supBas=BasisSub::superBas(I->basisAbstract());
    // not in list or FE index or identical grid
    if(nAx==Ax.size() or supBas->isIndex() or (supBas->isGrid() and Grid[nAx].size()==0)){
        if(nAx<Ax.size() and (supBas->isGrid()!=(Grid[nAx].size()==0)))DEVABORT("must give Grid points if on FE axis: "+axisName());
        // not in conversion list or not integrable
        for(unsigned int k=0;k<I->childSize();k++)
            Tree<Index>::childAdd(new IndexG(I->child(k),Ax,Grid,Weig));
    }
    // interpolation from grid
    else if (supBas->isGrid()) {
        const BasisGrid* grdBas=dynamic_cast<const BasisGrid*>(supBas);
        for(double g: Grid[nAx])
            if(g<grdBas->mesh()[0] or g>grdBas->mesh().back())
                ABORT(Sstr+"cannot interpolate"+g+"is outside grid ["+grdBas->mesh()[0]+","+grdBas->mesh().back()+"]");
        setBasis(BasisGrid::factory(Grid[nAx]));
        if(not I->isLeaf())
            for(unsigned int k=0;k<basisAbstract()->size();k++)
                Tree<Index>::childAdd(new IndexG(I->child(0),Ax,Grid,Weig));
    }
    else if (dynamic_cast<const BasisIntegrable*>(supBas)){
        const BasisIntegrable* intBas=dynamic_cast<const BasisIntegrable*>(supBas);
        if(Grid[nAx].size()==0){
            if(Weig[nAx].size()>0)intBas->quadRule(Weig[nAx].size(),Grid[nAx],Weig[nAx]);
            else                  intBas->quadRule(  intBas->size(),Grid[nAx],Weig[nAx]);
        }
        if(Weig[nAx].size()==0)Weig[nAx].resize(Grid[nAx].size(),1.);
        if(Weig[nAx].size()!=Grid[nAx].size())DEVABORT("grid points and weights do not match");


        // determine points falling into [low,upp), [low,upp] for last element;
        double upp=intBas->upBound();
        double low=intBas->lowBound();
        double eps=(upp-low)*1.e-10;
        const Index* uppN=I->upperNeighbor();
        if(uppN==0)upp+=eps;   // raise by epsilon for last element

        const Index* lowN=I->lowerNeighbor();
        if(lowN==0)low-=eps;   // lower by epsilon for first element
        else       low=lowN->basisIntegrable()->upBound(); // use upper boundary of lower neighbor

        // select grid points and weights falling into present basis
        vector<double> pts,wgs;
        for(double g: Grid[nAx]){
            if(upp<=g)break;
            if(low<=g){
                pts.push_back(g);
                wgs.push_back(Weig[nAx][std::find(Grid[nAx].begin(),Grid[nAx].end(),g)-Grid[nAx].begin()]);
            }
        }
        if(pts.size()>0){
            setBasis(BasisGridQuad::factory(pts,wgs));
            if(not I->isLeaf())
                for(unsigned int k=0;k<pts.size();k++)
                    Tree<Index>::childAdd(new IndexG(I->child(0),Ax,Grid,Weig));

            // converted lower indices must be equivalent
            for(int k=1;k<I->childSize() and childSize()>0;k++)
                if(not IndexG(I->child(k),Ax,Grid,Weig).treeEquivalent(child(0))){
                    Sstr+I->child(0)->strData()+Sendl;
                    Sstr+I->child(k)->strData()+Sendl;
                    DEVABORT("branches are not equivalent after conversion - cannot convert to grid on present level "+I->axisName()+"\n"
                             +IndexG(I->child(k),Ax,Grid,Weig).str()
                             +"\n"+child(0)->str());
                }
        }
        else
            _indexBas=0; // no basis
    }
    else
        PrintOutput::warning("basis on axis is not integrable - will not convert: "+basisAbstract()->str(),1);
    // remove empty branches
    for(int k=childSize()-1;k>=0;k--)
        if(not child(k)->basisAbstract())childErase(k);
}

void DiscretizationGrid::gridWeight(std::vector<std::vector<double>> &Grid, std::vector<std::vector<double>> &Weig,
                                    std::vector<string> Axes, std::vector<unsigned int> Points, std::vector<std::vector<double> > Bounds) {

    if(Points.size()==0)Points.assign(Axes.size(),0);
    if(Bounds.size()==0)Bounds.assign(Axes.size(),{0.,-1.});
    if(Points.size()!=Axes.size())DEVABORT(Sstr+"must supply Points for all Axes"+Axes+"is:"+Points);
    if(Bounds.size()!=Axes.size())DEVABORT(Sstr+"must supply Bounds for all Axes"+Axes+"got only"+Bounds.size());

    Grid.assign(Axes.size(),{});
    Weig.assign(Axes.size(),{});
    for(int k=0;k<Axes.size();k++){
        if(Points[k]==1){
            if(Bounds[k][0]!=Bounds[k][1])ABORT(Sstr+"for single point, lower and upper boundary must be equal, is:"+Bounds[k]);
            Grid[k]={Bounds[k][0]};
        }
        else if(Points[k]>1) {
            if(Bounds[k][0]<Bounds[k][1]){
                for(int l=0;l<Points[k];l++)
                    Grid[k].push_back(Bounds[k][0]+l*(Bounds[k][1]-Bounds[k][0])/(Points[k]-1));
            } else {
                Weig[k].assign(Points[k],0.);
            }
        }
    }
}













