// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "test_basisMatFunc.h"

//#include <boost/test/unit_test.hpp>
#include <complex>

#include "basisMat.h"

using namespace std;
using namespace tests;
//using namespace boost::unit_test;

void tests::test_basisMatExpI()
{
    complex<double> I(0.,1.);

    basisMatFunc* expI = new basisMatExpI();
    basisMatFuncSet(expI);
    BasisMat::BasisMatFuncs["ExpI"]->set("0,1.");
    DEVABORT("get non-boost");//BOOST_CHECK(norm(BasisMat::BasisMatFuncs["ExpI"]->operator()(1.)-exp(I))<1.e-7);
    BasisMat::BasisMatFuncs["ExpI"]->set("1,1.");
    DEVABORT("get non-boost");//BOOST_CHECK(norm(BasisMat::BasisMatFuncs["ExpI"]->operator()(2.)-I*2.*exp(2.*I))<1.e-7);
}
