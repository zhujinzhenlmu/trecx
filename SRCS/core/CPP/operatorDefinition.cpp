// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply.
//
// See terms of use in the LICENSE file included with the source distribution
//
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "operatorDefinition.h"
#include "tools.h"
#include "readInput.h"
#include "algebra.h"
#include "index.h"
#include "operatorData.h"
#include "str.h"
#include "parameters.h"

#include "operatorFloorEE.h"
using namespace std;

std::map<std::string,std::map<std::string,std::string> > OperatorDefinition::standardOperators;

OperatorDefinition::OperatorDefinition(string Def, string Hierarchy){
    if(Def.find("<<1>>")!=string::npos)
        ABORT("OBSOLETE operator defintion <<1>>, replace by <<Overlap>>");

    string coors="",def="undefined";
    vector<string> defs=OperatorData::terms(Def);
    if(tools::subStringCount(Def,"<<")>1){
        string s;
        for(int k=0;k<defs.size();k++){
            if(defs[k].find("<<")==string::npos)s+=defs[k];
            else s+=(OperatorDefinition(defs[k],Hierarchy));
        }
        assign(s);
        return;
    }

    // try to resolve pre-defined operator
    if(Hierarchy!=""){
        coors=Index::coordinates(Hierarchy);
        def=OperatorDefinition::get(Def,coors); 
        //HACK - remove constraints
        Def=Def.substr(0,Def.find("$"));
    }

    // take literally
    if(def.find("undefined")==string::npos){
        assign(def);
        return;
    }

    // if Hierachy contains FE-axis, move factors properly
    if(Hierarchy!="" and coors!=Hierarchy){
        vector<string> term=OperatorData::terms(Def);
        string s;
        for(int k=0;k<term.size();k++){
            s+=OperatorDefinition(term[k]).axisToCoor(Hierarchy);
        }
        assign(s);
    }
    else
        assign(Def);

}

OperatorDefinition & OperatorDefinition::tsurffDropTerms(const Index *IIndex, const Index *JIndex){
    vector<string> terms=OperatorData::terms(*this);
    if(IIndex->hierarchy().find("spec")!=string::npos or JIndex->hierarchy().find("spec")!=string::npos){
        dropTerms(terms,IIndex->hierarchy());
    }
    clear();
    for(unsigned int k=0;k<terms.size();k++)*this+=terms[k];
    return *this;
}

void OperatorDefinition::dropTerms(std::vector<string> &terms, const Index* IIndex, const Index *JIndex)
{
    if(IIndex==0)return;
    if(IIndex!=JIndex)DEVABORT("dropTerms only for operators maping space onto itself");
    vector<string> terms1(0);

    for(unsigned int k=0;k<terms.size();k++){
        // Go through all terms
        bool drop = false;
        string temp = terms[k];

        const Index *iI=IIndex,*jI=JIndex;
        while(not drop and not iI->isLeaf() and not jI->isLeaf()){
            if(iI->continuity()!=Index::npos){
                // Continuity level - do nothing as there is not string associated.
                if(iI->continuity()!=jI->continuity())ABORT("continuity levels do not match");
            }
            else if((iI->axisName().substr(0,4)=="surf" and jI->axisName().substr(0,4)=="surf") or
                    (iI->axisName().substr(0,4)=="spec" and jI->axisName().substr(0,4)=="spec")){
                // Surface/Spec level - do nothing as there is not string associated.
            }
            else {
                string a = OperatorData::first(terms[k],true);
                terms[k]=OperatorData::remainder(terms[k]);
                if(iI->axisName().substr(0,1)=="k" and jI->axisName().substr(0,1)=="k"){ // Momentum index
                    if(a.find("<1>")==string::npos) {drop=true; break;}
                }
            }
            iI=iI->child(0);
            jI=jI->child(0);
        }

        if(not drop)terms1.push_back(temp);
    }

    terms = terms1;
}
void OperatorDefinition::dropTerms(std::vector<string> &terms, std::string Hierarchy)
{
    vector<string> terms1;
    for(std::string termK: terms){
        std::string coor=Index::coordinates(Hierarchy)+"."; // reduce hierarchy to actual coordinates
        if(std::count(coor.begin(),coor.end(),'.')!=std::count(termK.begin(),termK.end(),'<') and termK.find("[[")==string::npos)
            DEVABORT("number of factors in "+termK+" does not match coordinates: "+coor);
        // if k-factor need <1> or <Id>
        for(size_t pos0=termK.find("<");coor!="";coor=coor.substr(coor.find(".")+1),pos0=termK.find("<",pos0+2)){
            if(coor[0]=='k' and pos0==string::npos)break; // not tensor product structure - cannot select
            if((coor[0]=='k' or coor.substr(0,4)=="spec") and not(termK.find("<1>",pos0)==pos0  or termK.find("<Id>" ,pos0)==pos0))break;
        }
        if(coor=="")terms1.push_back(termK); // all factors passed, keep term
    }
    terms=terms1;
}

std::string OperatorDefinition::parentHierarchy(std::string Hierarchy){
    // given a hierarchy, convert special names to where they derived from
    // note: historically, there is some inconsistency in the use of surf/ValDer and spec/k
    //       this is fixed here.
    Hierarchy+=".";
    string parent;
    for(size_t i=0,j=Hierarchy.find('.');j!=string::npos;i=j+1,j=Hierarchy.find('.',i)){
        if     (Hierarchy.find("spec",i)==i)parent+=Hierarchy.substr(i+4,j-i-3);
        else if(Hierarchy.find("surf",i)==i){
            string ax=Hierarchy.substr(i+4,j-i-3);
            if(Hierarchy.find("ValDer"+ax)!=string::npos)parent+="ValDer"+ax;
            else                                         parent+=ax;
        }
        else if(Hierarchy.find("k",i)==i)parent+=Hierarchy.substr(i+1,j-i);
        else parent+=Hierarchy.substr(i,j-i+1);
    }
    return parent.substr(0,parent.length()-1);
}

// this is a rather shaky un-bracketing function
std::string OperatorDefinition::unBracket(string Term,string Front,string Back){
    if(Term=="")return "";

    // decompose into terms
    vector<std::string> terms,signs,fac,sFac;
    tools::splitString(Term,"+-",terms,signs,"<(",">)");

    tools::splitString(Front,"+-",fac,sFac,"<(",">)");
    if(signs[0]==" ")signs[0]="+";
    if(sFac.size()==0){
        fac.push_back("");
        sFac.push_back("");
    }
    if(sFac[0]=="")sFac[0]="+";

    // remove outermost bracket (if any) from each term
    string res;
    for(unsigned int k=0;k<terms.size();k++){
        // distribute qualifiers
        string back(Back);
        size_t qPos=tools::findLastOutsideBrackets(terms[k],"$","(",")");
        if(qPos!=string::npos){
            if(Back!="")ABORT("multiple qualifiers in Term:\n"+Term);
            back=terms[k].substr(qPos);
            terms[k]=terms[k].substr(0,qPos);
        }
        if(signs[k]==sFac[0])signs[k]="+";
        else                 signs[k]="-";

        size_t i0=tools::findFirstOutsideBrackets(terms[k],"(","<",">");
        if(i0==string::npos)
            res+=signs[k]+fac[0]+terms[k]+back;
        else{
            size_t i1=tools::findLastOutsideBrackets(terms[k],")","<",">");
            if(i1+1<qPos)back=terms[k].substr(i1+1,qPos-i1-1); // new
            string fact=signs[k]+tools::cropString(fac[0])+tools::cropString(terms[k].substr(0,i0));
            string termk=terms[k].substr(i0+1,i1-i0-1);
            res+=unBracket(termk,fact,back);
        }
    }
    return res;
}

/// return definition with factors at FE levels moved to end
OperatorDefinition OperatorDefinition::axisToCoor(const std::string Hierarchy){
    //HACK - remove the constraints for now
//    PrintOutput::DEVwarning("operator constraint disabled",5);
    *this=substr(0,find("$"));
    if(not OperatorData::isStandard(*this))return *this; // do not modify special operators
    size_t i0=find("<"),d0=find(".",find("$"))+1;
    size_t inext=find("<",i0+1),d1=find(".",d0+1);
    if(inext==string::npos)return *this; // single factor, no rearangement
    if(inext==i0+1)return *this; // predefined operator, no rearrangement

    size_t i1=find(">");
    size_t first=Hierarchy.find(".");
    if(first==string::npos)DEVABORT("hierarchy shorter than operator factors?");

    if(Hierarchy.find(Hierarchy.substr(0,first),first)==string::npos){
        // not FE level, continue to next factor
        return substr(0,i1+1)+OperatorDefinition(substr(i1+1)).axisToCoor(Hierarchy.substr(first+1)); // not fem axis
    }
    else {
        // FE level: move current factor to end and continue to next
        OperatorDefinition s(*this);
        int lenDef=min(s.length(),s.find("$"));
        s.insert(s.begin()+lenDef,s.begin()+i0,s.begin()+i1+1); // copy to end
        if(i1!=find("><") and i1!=find(">iLaserA")){
            // move factor to front
            string par0=substr(0,find("<"));
            string par1=substr(i1+1,find("<",i1+1)-i1-1);
            ABORT("multiple factors in "+*this+" "+par0+" "+par1);
        }
        s.replace(s.begin()+i0,s.begin()+i1+1,""); // remove from original location
        string constraint;
        return s.substr(0,i0)+OperatorDefinition(s.substr(i0)).axisToCoor(Hierarchy.substr(first+1))+constraint;
    }

}

bool OperatorDefinition::isLocal(const Index *IIndex, const Index *JIndex) const {
    if(not isStandard(IIndex, JIndex)){
        if(*this == "[[eeInt6DHelium]]") return true;
        return false;
    }
    if(find("NONORTH")!=string::npos)return false;
    return true;
}


bool OperatorDefinition::isStandard(const Index *IIndex, const Index *JIndex) const {
    if(IIndex->basisNdim())return false;
    if(JIndex->basisNdim())return false;
    return OperatorData::isStandard(*this);
}

void OperatorDefinition::specialConstrain(UseMatrix& Mult, const Index *IIndex, const Index *JIndex) const{
    if(*this == "[[eeInt6DHelium]]"){
        OperatorFloorEE::constrain(Mult, IIndex, JIndex);
    }
}

bool OperatorDefinition::isSeparable() const {
    return (tools::subStringCount(*this,":","<([",">)]")!=0);
}

OperatorDefinition OperatorDefinition::constrain(UseMatrix &Mult, const Index *IIndex, const Index *JIndex) const {
    OperatorDefinition remainder(*this);
    if(IIndex->axisName()!=JIndex->axisName())return remainder;

    size_t itype=find("$");
    int band=INT_MAX;
    if(itype!=string::npos){

        // sanity checks for constraints
        size_t idot0=find(".",itype);
        if(substr(itype,idot0-itype)!="$BAND")ABORT("Constraint not implemented, need \"$BAND\": "+*this);

        // band-width of constraint
        size_t idot1=min(length(),find(".",idot0+1));
        if(idot0<idot1){
            if(substr(idot0+1,idot1-idot0-1)=="*")
                band=INT_MAX;
            else
                band=tools::string_to_int(substr(idot0+1,idot1-idot0-1));
        }

        // remove present constraint from definintion
        remainder.erase(remainder.begin()+idot0,remainder.begin()+idot1);

        // sanity checks for constraints
        string inam=IIndex->basisAbstract()->name(),jnam=JIndex->basisAbstract()->name();
        if(IIndex->axisName()=="Phi"){
            if((inam.find("PlaneWave")==string::npos and inam.find("CosSin")==string::npos and inam.find("expIm")==string::npos) or
                    (jnam.find("PlaneWave")==string::npos and jnam.find("CosSin")==string::npos and jnam.find("expIm")==string::npos))
                  {
                PrintOutput::warning("Phi-constraint not applied, not trigonometric basis: "+inam+" | "+jnam,5);
                band=INT_MAX;
            }
        }
        if(IIndex->axisName()=="Eta"){
            if(inam.find("assocLegendre")==string::npos or jnam.find("assocLegendre")==string::npos){
                PrintOutput::warning("Eta-constraint not applied, not associatedLegendre basis: "+inam+" | "+jnam,5);
                band=INT_MAX;
            }
        }
    }

    if(band<INT_MAX)
        for(int c=0;c<Mult.cols();c++)
            for(int r=0;r<Mult.rows();r++)
                if(abs(r-c)>band)Mult(r,c)=0.;

    if(not isStandard(IIndex, JIndex)){
        specialConstrain(Mult, IIndex, JIndex);
    }

    return OperatorData::remainder(remainder);
}

static string operatorBrackets("([<");
string OperatorDefinition::parameter(OperatorDefinition &Remainder) const{
    size_t bop=find_first_of(operatorBrackets),bra=0;

    while(bra<bop){
        if(bop==string::npos)ABORT("not an operator: "+*this);
        bra=bop;
        switch (string(*this)[bra]){
        case '<': break; // anything that starts with '<' is an operator
        case '[':
            if(find("[[")!=bop)bop=find_first_of(operatorBrackets,bra+1); // only [[ begins an opererator
            break;
        case '(':
            size_t clos=find(')');
            if(clos==string::npos)ABORT("unmatched ')' in "+*this);
            if(min(find('<',bra+1),find("[[",bra+1))>clos)
                bop=find_first_of(operatorBrackets,bra+1);
            break;
        }

        // make sure that we can alias &Remainder with this
        string fac=substr(0,bop);
        Remainder=substr(bop);
        return fac;
    }
}

void OperatorDefinition::checkVariable(std::string Message,std::string Name) const {
    vector<string> nonConst,timeDep;
    for(unsigned int k=0;k<Parameters::table.size();k++)
        if(Parameters::isFunction(Parameters::table[k].name) and find(Parameters::table[k].name)!=string::npos){
            if(Parameters::table[k].name.find("[t]")!=string::npos)timeDep.push_back(Parameters::table[k].name);
            else                                                  nonConst.push_back(Parameters::table[k].name);
        }
    if(nonConst.size()+timeDep.size()>0){
        PrintOutput::newLine();
        PrintOutput::message("non-constant parameters in operator \""+Name+"\": "+tools::str(timeDep,", ")+tools::str(nonConst,", "));

        if(nonConst.size()>0 and Message!="")PrintOutput::warning(Message);
    }
}

void OperatorDefinition::setup(){
    if(standardOperators.size()>0)return; // already set up
    standardOperators["GridWeights"];

    standardOperators["Overlap"]["Phi.PXi.PEta"]="0.25<1>(<Q><1>+<1><Q>)";
    standardOperators["Laplacian"]["Phi.PXi.PEta"]="<1>(<d_Q_d><1>+<1><d_Q_d>)+0.25<d_1_d>(<1/Q><1>+<1><1/Q>)";
    standardOperators["Coulomb"]["Phi.PXi.PEta"]="-0.5<1><1><1>";
    
    standardOperators["Id"]["Rn.Phi.Eta"]="<1><1><1>$BAND.0.0.0";
    standardOperators["Id"]["Vec.Ion"]="<1><1>";
    standardOperators["Id"]["Ion"]="<1>";
    standardOperators["Id"]["Rn1.Phi1.Eta1.Rn2.Phi2.Eta2"]="<1><1><1><1><1><1>+<1><1><1><1><1><1>";

    standardOperators["FullOverlap"]["Ion.Rn.Phi.Eta"]="<1><1><1><1>+<allOnes><NONORTH1><allOnes><allOnes>";
    standardOperators["FullOverlap"]["Neutral"]="0.5(<1>+<1>)";
    standardOperators["EEInteraction"]["Ion.Rn.Phi.Eta"]="+<EEInt><1><1><1>$BAND.0.0.0.0+<allOnes><NONORTHEEInt><allOnes><allOnes>";
    standardOperators["TestOp"]["Ion.Rn.Phi.Eta"]="<allOnes><NONORTHLaplacian><allOnes><allOnes>";

    standardOperators["Laplacian"]["Ion.Rn.Phi.Eta"]="<1><d_1_d><1><1>$BAND.0.0.0.0+<1><1/(Q*Q)><1><d_(1-Q*Q)_d>$BAND.0.0.0.0+<1><1/(Q*Q)><d_1_d><1/(1-Q*Q)>$BAND.0.0.0.0"
            +string("+<Laplacian><1><1><1>$BAND.0.0.0.0+<allOnes><NONORTHLaplacian><allOnes><allOnes>");
    standardOperators["Laplacian"]["Ion"]=standardOperators["Laplacian"]["Neutral"]="<Laplacian>";
    standardOperators["Laplacian"]["Vec.Ion"]="<1><Laplacian>";
    standardOperators["Laplacian"]["X"]=standardOperators["Laplacian"]["Y"]=standardOperators["Laplacian"]["Z"]="<d_1_d>";
    standardOperators["Laplacian"]["X.Y"]="<1><d_1_d>+<d_1_d><1>";
    standardOperators["Laplacian"]["X.Y.Z"]="<1><d_1_d><1>+<d_1_d><1><1>+<1><1><d_1_d>";

    standardOperators["EEInteraction"]["Ion"]=standardOperators["EEInteraction"]["Neutral"]="<EEInt>";
    standardOperators["EEInteraction"]["Vec.Ion"]="<1><EEInt>";

    standardOperators["AngularMomentumSquared"]["Rn.Phi.Eta"]="<1><1><d_(1-Q*Q)_d>$BAND.0.0.0+<1><d_1_d><1/(1-Q*Q)>$BAND.0.0.0";
    standardOperators["AngularMomentumZsquared"]["Rn.Phi.Eta"]="<1><d_1_d><1>$BAND.0.0.0";
    standardOperators["Parity"]["Rn.Phi.Eta"]="<1><Parity><Parity>$BAND.0.0.0";

    standardOperators["Laplacian"]["Phi.Rho"]="<1><d_1_d>$BAND.0.*+<d_1_d><1/(Q*Q)>";

    standardOperators["Laplacian"]["Rn.Phi.Eta"]="<d_1_d><1><1>$BAND.0.0.0+<1/(Q*Q)><1><d_(1-Q*Q)_d>$BAND.0.0.0+<1/(Q*Q)><d_1_d><1/(1-Q*Q)>$BAND.0.0.0";
    standardOperators["Laplacian"]["Rn1.Phi1.Eta1"]=standardOperators["Laplacian"]["Rn2.Phi2.Eta2"]=standardOperators["Laplacian"]["Rn.Phi.Eta"];
    standardOperators["Laplacian"]["Vec.Rn2.Phi2.Eta2"]="<1><d_1_d><1><1>$BAND.0.0.0.0+<1><1/(Q*Q)><1><d_(1-Q*Q)_d>$BAND.0.0.0.0+<1><1/(Q*Q)><d_1_d><1/(1-Q*Q)>$BAND.0.0.0.0";

    standardOperators["Laplacian"]["Rn1.Phi1.Eta1.Rn2.Phi2.Eta2"]=  "<d_1_d><1><1><1><1><1>+<1/(Q*Q)><1><d_(1-Q*Q)_d><1><1><1>+<1/(Q*Q)><d_1_d><1/(1-Q*Q)><1><1><1>";
    standardOperators["Laplacian"]["Rn1.Phi1.Eta1.Rn2.Phi2.Eta2"]+="+<1><1><1><d_1_d><1><1>+<1><1><1><1/(Q*Q)><1><d_(1-Q*Q)_d>+<1><1><1><1/(Q*Q)><d_1_d><1/(1-Q*Q)>";
    standardOperators["Laplacian"]["Rn1.Phi1.Eta1.nSurface.Phi2.Eta2"]=  "<d_1_d><1><1><1><1><1>+<1/(Q*Q)><1><d_(1-Q*Q)_d><1><1><1>+<1/(Q*Q)><d_1_d><1/(1-Q*Q)><1><1><1>";
    standardOperators["Laplacian"]["nSurface.Phi1.Eta1.Rn2.Phi2.Eta2"]=  "<1><1><1><d_1_d><1><1>+<1><1><1><1/(Q*Q)><1><d_(1-Q*Q)_d>+<1><1><1><1/(Q*Q)><d_1_d><1/(1-Q*Q)>";

    standardOperators["Parabolic"]["Rn.Phi.Eta"]="<Q*Q><1><1>";
    standardOperators["Parabolic"]["Rn1.Phi1.Eta1.Rn2.Phi2.Eta2"]="<Q*Q><1><1><1><1><1>+<1><1><1><Q*Q><1><1>";
    standardOperators["Parabolic"]["X"]=standardOperators["Parabolic"]["Y"]=standardOperators["Parabolic"]["Z"]="<Q*Q>";
    standardOperators["Parabolic"]["X.Y"]="<Q*Q><1>+<1><Q*Q>";
    standardOperators["Parabolic"]["X.Y.Z"]="<Q*Q><1><1>+<1><Q*Q><1>+<1><1><Q*Q>";

    string smoothFunction("1"),smoothDerivative("0");
    if(Algebra::isSpecialConstant("Rtrunc")){
        if(not Algebra::isSpecialConstant("Rsmooth"))ABORT("for using potential cutoff radius Rcutoff, must also define Rsmooth");
        smoothFunction  ="trunc[Rsmooth,Rtrunc]";
        smoothDerivative="trunc[Rsmooth,Rtrunc,1]";
    }

    standardOperators["Coulomb"]["Rn1.Phi1.Eta1"]= standardOperators["Coulomb"]["Rn2.Phi2.Eta2"] =standardOperators["Coulomb"]["Rn.Phi.Eta"]="<"+smoothFunction+"/Q><1><1>$BAND.0.0.0";
    standardOperators["Coulomb"]["Vec.Rn1.Phi1.Eta1"]=standardOperators["Coulomb"]["Vec.Rn2.Phi2.Eta2"]="<1><"+smoothFunction+"/Q><1><1>$BAND.0.0.0.0";
    standardOperators["Coulomb"]["Ion.Rn.Phi.Eta"]="-2.<1><"+smoothFunction+"/Q><1><1>$BAND.0.0.0.0+<NucCoulomb><1><1><1>$BAND.0.0.0.0+<allOnes><NONORTHCoulomb><allOnes><allOnes>";
    standardOperators["Coulomb"]["Ion"]=standardOperators["Coulomb"]["Neutral"]="<NucCoulomb>";
    standardOperators["Coulomb"]["Vec.Ion"]="<1><NucCoulomb>";


    standardOperators["Coulomb"]["Rn1.Phi1.Eta1.Rn2.Phi2.Eta2"]="<"+smoothFunction+"/Q><1><1><1><1><1>+<1><1><1><"+smoothFunction+"/Q><1><1>";
    standardOperators["Coulomb"]["Rn1.Phi1.Eta1.nSurface.Phi2.Eta2"]="<"+smoothFunction+"/Q><1><1><1><1><1>";
    standardOperators["Coulomb"]["nSurface.Phi1.Eta1.Rn2.Phi2.Eta2"]="<1><1><1><"+smoothFunction+"/Q><1><1>";

    standardOperators["X"]["Rn.Phi.Eta"]="<Q><cos(Q)><sqrt(1-Q*Q)>";
    standardOperators["Y"]["Rn.Phi.Eta"]="<Q><sin(Q)><sqrt(1-Q*Q)>";
    standardOperators["Z"]["Rn.Phi.Eta"]="<Q><1><Q>";

    // derivative operators in explicitly anti-symmetric form
    string ddxPolar="(<0.5_d><cos(Q)><sqrt(1-Q*Q)>-<0.5/Q><cos(Q)><Q*sqrt(1-Q*Q)_d>-<0.5/Q><sin(Q)_d><1/sqrt(1-Q*Q)>-<d_0.5><cos(Q)><sqrt(1-Q*Q)>+<0.5/Q><cos(Q)><d_Q*sqrt(1-Q*Q)>+<0.5/Q><d_sin(Q)><1/sqrt(1-Q*Q)>)";
    string ddyPolar="(<0.5_d><sin(Q)><sqrt(1-Q*Q)>-<0.5/Q><sin(Q)><Q*sqrt(1-Q*Q)_d>+<0.5/Q><cos(Q)_d><1/sqrt(1-Q*Q)>-<d_0.5><sin(Q)><sqrt(1-Q*Q)>+<0.5/Q><sin(Q)><d_Q*sqrt(1-Q*Q)>-<0.5/Q><d_cos(Q)><1/sqrt(1-Q*Q)>)";
    standardOperators["D/DX"]["Rn1.Phi1.Eta1.Rn2.Phi2.Eta2"]="(<1><1><1>"+ddxPolar+")$BAND.0.0.0.0.4.2 +("+ddxPolar+"<1><1><1>)$BAND.0.4.2.0.0.0";
    standardOperators["D/DY"]["Rn1.Phi1.Eta1.Rn2.Phi2.Eta2"]="(<1><1><1>"+ddyPolar+")$BAND.0.0.0.0.4.2 +("+ddyPolar+"<1><1><1>)$BAND.0.4.2.0.0.0";

    standardOperators["D/DX"]["Rn1.Phi1.Eta1"] = standardOperators["D/DX"]["Rn2.Phi2.Eta2"] =
            standardOperators["D/DX"]["Rn.Phi.Eta"]="(<0.5_d><cos(Q)><sqrt(1-Q*Q)>-<0.5/Q><cos(Q)><Q*sqrt(1-Q*Q)_d>-<0.5/Q><sin(Q)_d><1/sqrt(1-Q*Q)>"
            +string(                        "-<d_0.5><cos(Q)><sqrt(1-Q*Q)>+<0.5/Q><cos(Q)><d_Q*sqrt(1-Q*Q)>+<0.5/Q><d_sin(Q)><1/sqrt(1-Q*Q)>)$BAND.0.4.2");
    standardOperators["D/DY"]["Rn1.Phi1.Eta1"] = standardOperators["D/DY"]["Rn2.Phi2.Eta2"] =
            standardOperators["D/DY"]["Rn.Phi.Eta"]
            =       "(<0.5_d><sin(Q)><sqrt(1-Q*Q)>-<0.5/Q><sin(Q)><Q*sqrt(1-Q*Q)_d>+<0.5/Q><cos(Q)_d><1/sqrt(1-Q*Q)>"
            +string("-<d_0.5><sin(Q)><sqrt(1-Q*Q)>+<0.5/Q><sin(Q)><d_Q*sqrt(1-Q*Q)>-<0.5/Q><d_cos(Q)><1/sqrt(1-Q*Q)>)$BAND.0.4.2");
    standardOperators["D/DZ"]["Rn1.Phi1.Eta1"]=
            standardOperators["D/DZ"]["Rn2.Phi2.Eta2"] =
            standardOperators["D/DZ"]["Rn.Phi.Eta"]="(<0.5_d><1><Q>+<0.5/Q><1><(1-Q*Q)_d>"
            +string(                                "-<d_0.5><1><Q>-<0.5/Q><1><d_(1-Q*Q)>)$BAND.0.0.2");
    standardOperators["D/DX"]["Vec.Rn1.Phi1.Eta1"] =
            standardOperators["D/DX"]["Vec.Rn2.Phi2.Eta2"]
            =       "(<1><0.5_d><cos(Q)><sqrt(1-Q*Q)>-<1><0.5/Q><cos(Q)><Q*sqrt(1-Q*Q)_d>-<1><0.5/Q><sin(Q)_d><1/sqrt(1-Q*Q)>"
            +string("-<1><d_0.5><cos(Q)><sqrt(1-Q*Q)>+<1><0.5/Q><cos(Q)><d_Q*sqrt(1-Q*Q)>+<1><0.5/Q><d_sin(Q)><1/sqrt(1-Q*Q)>)$BAND.0.0.4.2");
    standardOperators["D/DY"]["Vec.Rn1.Phi1.Eta1"] =
            standardOperators["D/DY"]["Vec.Rn2.Phi2.Eta2"]
            =       "(<1><0.5_d><sin(Q)><sqrt(1-Q*Q)>-<1><0.5/Q><sin(Q)><Q*sqrt(1-Q*Q)_d>+<1><0.5/Q><cos(Q)_d><1/sqrt(1-Q*Q)>"
            +string("-<1><d_0.5><sin(Q)><sqrt(1-Q*Q)>+<1><0.5/Q><sin(Q)><d_Q*sqrt(1-Q*Q)>-<1><0.5/Q><d_cos(Q)><1/sqrt(1-Q*Q)>)$BAND.0.0.4.2");
    standardOperators["D/DZ"]["Vec.Rn1.Phi1.Eta1"] =
            standardOperators["D/DZ"]["Vec.Rn2.Phi2.Eta2"]="(<1><0.5_d><1><Q>+<1><0.5/Q><1><(1-Q*Q)_d>"
            +string(                       " -<1><d_0.5><1><Q>-<1><0.5/Q><1><d_(1-Q*Q)>)$BAND.0.0.0.2");

    standardOperators["D/DZ"]["Ion.Rn.Phi.Eta"]="(<1><0.5_d><1><Q>+<1><0.5/Q><1><(1-Q*Q)_d>"
            +string(                       " -<1><d_0.5><1><Q>-<1><0.5/Q><1><d_(1-Q*Q)>)$BAND.0.0.0.2+<D/Dz><1><1><1>")
            +string(                       "+<allOnes><NONORTHz_vg><allOnes><allOnes>");
    standardOperators["D/DZ"]["Vec.Ion"]="<1><D/Dz>";

    standardOperators["D/DZ"]["Rn1.Phi1.Eta1.Rn2.Phi2.Eta2"]="(<0.5_d><1><Q><1><1><1>+<0.5/Q><1><(1-Q*Q)_d><1><1><1>"
            +string(                       " -<d_0.5><1><Q><1><1><1>-<0.5/Q><1><d_(1-Q*Q)><1><1><1>)$BAND.0.0.2.0.0.0")
            +string(                           " +(<1><1><1><0.5_d><1><Q>+<1><1><1><0.5/Q><1><(1-Q*Q)_d>" )
            +string(                       " -<1><1><1><d_0.5><1><Q>-<1><1><1><0.5/Q><1><d_(1-Q*Q)>)$BAND.0.0.0.0.0.2");
    standardOperators["D/DZ"]["nSurface.Phi1.Eta1.Rn2.Phi2.Eta2"]="(<1><1><1><0.5_d><1><Q>+<1><1><1><0.5/Q><1><(1-Q*Q)_d>"
            +string(                       " -<1><1><1><d_0.5><1><Q>-<1><1><1><0.5/Q><1><d_(1-Q*Q)>)$BAND.0.0.0.0.0.2");
    standardOperators["D/DZ"]["Rn1.Phi1.Eta1.nSurface.Phi2.Eta2"]="(<0.5_d><1><Q><1><1><1>+<0.5/Q><1><(1-Q*Q)_d><1><1><1>"
            +string(                       " -<d_0.5><1><Q><1><1><1>-<0.5/Q><1><d_(1-Q*Q)><1><1><1>)$BAND.0.0.2.0.0.0");

    // dipole, dipole velocity, dipole acceleration
    standardOperators["DipLenX"]["Rn.Phi.Eta"]=standardOperators["X"]["Rn.Phi.Eta"];
    standardOperators["DipLenY"]["Rn.Phi.Eta"]=standardOperators["Y"]["Rn.Phi.Eta"];
    standardOperators["DipLenZ"]["Rn.Phi.Eta"]=standardOperators["Z"]["Rn.Phi.Eta"];
    standardOperators["DipVelX"]["Rn.Phi.Eta"]="i"+standardOperators["D/DX"]["Rn.Phi.Eta"];
    standardOperators["DipVelY"]["Rn.Phi.Eta"]="i"+standardOperators["D/DY"]["Rn.Phi.Eta"];
    standardOperators["DipVelZ"]["Rn.Phi.Eta"]="i"+standardOperators["D/DZ"]["Rn.Phi.Eta"];
    string potDer("<1/(Q*Q)>");
    if(smoothFunction!="1")potDer="<"+smoothFunction+"/(Q*Q)-"+smoothDerivative+"/Q>";
    standardOperators["DipAccX"]["Rn.Phi.Eta"]=potDer+"<cos(Q)><sqrt(1-Q*Q)>";
    standardOperators["DipAccY"]["Rn.Phi.Eta"]=potDer+"<sin(Q)><sqrt(1-Q*Q)>";
    standardOperators["DipAccZ"]["Rn.Phi.Eta"]=potDer+"<1><Q>";

    standardOperators["PotSolid"]["Z"]="<PotSolid>";
    standardOperators["PotSolid"]["Vec.Z"]="<1><PotSolid>";

    standardOperators["DipLenX"]["X"]=standardOperators["DipLenY"]["Y"]=standardOperators["DipLenZ"]["Z"]="<Q>";
    standardOperators["DipVelX"]["X"]=standardOperators["DipVelY"]["Y"]=standardOperators["DipVelZ"]["Z"]="(i<d_0.5>-i<0.5_d>)";

    standardOperators["DipLenX"]["Vec.X"]=standardOperators["DipLenY"]["Vec.Y"]=standardOperators["DipLenZ"]["Vec.Z"]="<1><Q>";
    standardOperators["DipVelX"]["Vec.X"]=standardOperators["DipVelY"]["Vec.Y"]=standardOperators["DipVelZ"]["Vec.Z"]="i<1>(0.5<d_1>-0.5<1_d>)";
    standardOperators["DipAccX"]["Vec.X"]=standardOperators["DipAccY"]["Vec.Y"]=standardOperators["DipAccZ"]["Vec.Z"]="<-1><Derivative_PotSolid>";
    standardOperators["DipAccX"]["X"]=standardOperators["DipAccY"]["Y"]=standardOperators["DipAccZ"]["Z"]="-<Derivative_PotSolid>";

    // Coriolis term for rotating frame of reference
    standardOperators["CoriolisTheta"]["Rn.Phi.Eta"]="(<1><cos(Q)><sqrt(1-Q*Q)_d>+<1><sin(Q)_d><Q/sqrt(1-Q*Q)>)$BAND.0.4.2";

    // the mixed gauge dipole term
    string r0("chi[Rg,infty]*0.5");                   // factor 1/2 for anti-symmetrization included here
    string r1("chi[Rg,infty]*((Q-Rg)/(Q*Q))*0.5");    // factor 1/2 for anti-symmetrization included here
    string r2("chi[Rg,infty]*Rg*(2*Q-Rg)/(Q*Q)*0.5"); // factor 1/2 from kinetic energy
    string r3("chi[0,Rg]*Q+chi[Rg,infty]*Rg");
    string eta1("(1-Q*Q)");
    string eta2("sqrt"+eta1);

    string mixDip;
    // field-terms
    mixDip+= "LaserFz[t]<"+r3+"><1><Q>$BAND.0.0.2";
    mixDip+="+LaserFx[t]<"+r3+"><cos(Q)><"+eta2+">$BAND.0.2.2";
    mixDip+="+LaserFy[t]<"+r3+"><sin(Q)><"+eta2+">$BAND.0.4.2";

    // linear A-terms (anti-symmetrized)
    mixDip+="+iLaserAx[t](<"  +r0+"_d>< cos(Q)><"+eta2+">+<"+r1+">(<cos(Q)>< -Q*"+eta2+"_d>+<  sin(Q)_d><-1/"+eta2+">)"
            +           "+<d_"+r0+  "><-cos(Q)><"+eta2+">+<"+r1+">(<cos(Q)><d_Q*"+eta2+  ">+<d_sin(Q)  >< 1/"+eta2+">))$BAND.0.4.2";

    mixDip+="+iLaserAy[t](<"  +r0+"_d>< sin(Q)><"+eta2+">+<"+r1+">(<sin(Q)>< -Q*"+eta2+"_d>+<  cos(Q)_d>< 1/"+eta2+">)"
            +           "+<d_"+r0+  "><-sin(Q)><"+eta2+">+<"+r1+">(<sin(Q)><d_Q*"+eta2+  ">+<d_cos(Q)  ><-1/"+eta2+">))$BAND.0.4.2";

    mixDip+="+iLaserAz[t](<"       +r0+"_d><1><Q>+< "+r1+"><1><"       +eta1+"_d>"
            +           "+<d_(-1)*"+r0+"><1><Q>+<"   +r1+"><1><d_(-1)*"+eta1+">)$BAND.0.0.2";

    // quadratic A-terms...
    mixDip+="+LaserAsq[t]<-1+chi[Rg,infty]*pow[2]((Q-Rg)/Q)><0.5><1>$BAND.0.2.2"; // note: for Rg=0 no Asq-term

    mixDip+="+LaserAxx[t]<"+r2+"><cos(Q)*cos(Q)><"+eta1+">$BAND.0.4.4";
    mixDip+="+LaserAyy[t]<"+r2+"><sin(Q)*sin(Q)><"+eta1+">$BAND.0.4.4";
    mixDip+="+LaserAzz[t]<"+r2+"><1><Q*Q>$BAND.0.0.4";

    mixDip+="+LaserAxy[t]<"+r2+"><2*sin(Q)*cos(Q)><"+eta1+">$BAND.0.6.4";
    mixDip+="+LaserAxz[t]<"+r2+"><2*cos(Q)><Q*"+eta2+">$BAND.0.4.4";
    mixDip+="+LaserAyz[t]<"+r2+"><2*sin(Q)><Q*"+eta2+">$BAND.0.4.4";

    standardOperators["MixedGaugeDipole"]["Rn.Phi.Eta"]=unBracket(mixDip,"");

    // NOTE: a constraint for the first factor must be inserted
    standardOperators["MixedGaugeDipole"]["Ion.Rn.Phi.Eta"]=addConstraint("<1>("+mixDip+")","BAND",0,"0");
    standardOperators["MixedGaugeDipole"]["Ion.Rn.Phi.Eta"]+="+(LaserFx[t]<x>+LaserFy[t]<y>+LaserFz[t]<z>)<1><1><1>";
    standardOperators["MixedGaugeDipole"]["Ion.Rn.Phi.Eta"]+="+LaserFx[t]<allOnes><NONORTHx><allOnes><allOnes>";
    standardOperators["MixedGaugeDipole"]["Ion.Rn.Phi.Eta"]+="+LaserFy[t]<allOnes><NONORTHy><allOnes><allOnes>";
    standardOperators["MixedGaugeDipole"]["Ion.Rn.Phi.Eta"]+="+LaserFz[t]<allOnes><NONORTHz><allOnes><allOnes>";

    // quadratic antisym term
    standardOperators["MixedGaugeDipole"]["Ion.Rn.Phi.Eta"]+="-LaserAsqHalf[t]<allOnes><NONORTH1><allOnes><allOnes>";

    // pure length gauge coupling of ionic and states
    standardOperators["MixedGaugeDipole"]["Neutral"]="(LaserFx[t]<x>+LaserFy[t]<y>+LaserFz[t]<z>)";
    standardOperators["MixedGaugeDipole"]["Vec.Ion"]="<1>(LaserFx[t]<x>+LaserFy[t]<y>+LaserFz[t]<z>)";

    // remove Asq terms from Neutral
    standardOperators["MixedGaugeDipole"]["Neutral"]+="-LaserAsqHalf[t]<1>";
    //standardOperators["MixedGaugeDipole"]["Vec.Ion"]="<1>(-<1>)";

    // commutators - compose of basic 1d and polar
    standardOperators        ["Commutator"]["ValDerX"]=
            standardOperators["Commutator"]["ValDerY"]=
            standardOperators["Commutator"]["ValDerZ"]=
            "-1/2<0,1>+1/2<1,0>+iLaserAz[t]<0,0>";

    standardOperators         ["Commutator"]["ValDerX.Y"]
            =standardOperators["Commutator"]["ValDerX.kY"]
            =standardOperators["Commutator"]["ValDerY.X"]
            =standardOperators["Commutator"]["ValDerY.kX"]
            ="("+standardOperators["Commutator"]["ValDerX"]+")<Id>";

    standardOperators         ["Commutator"]["ValDerX.Z"]
            =standardOperators["Commutator"]["ValDerX.kZ"]
            =standardOperators["Commutator"]["ValDerZ.X"]
            =standardOperators["Commutator"]["ValDerZ.kX"]
            ="("+standardOperators["Commutator"]["ValDerX"]+")<Id>";

    standardOperators         ["Commutator"]["ValDerX.Y.Z"]
            =standardOperators["Commutator"]["ValDerX.kY.Z"]
            =standardOperators["Commutator"]["ValDerX.Y.kZ"]
            =standardOperators["Commutator"]["ValDerX.kY.kZ"]
            =standardOperators["Commutator"]["ValDerX.Z.Y"]
            =standardOperators["Commutator"]["ValDerX.kZ.Y"]
            =standardOperators["Commutator"]["ValDerX.Z.kY"]
            =standardOperators["Commutator"]["ValDerX.kZ.kY"]
            =standardOperators["Commutator"]["ValDerY.X.Z"]
            =standardOperators["Commutator"]["ValDerY.kX.Z"]
            =standardOperators["Commutator"]["ValDerY.X.kZ"]
            =standardOperators["Commutator"]["ValDerY.kX.kZ"]
            =standardOperators["Commutator"]["ValDerY.Z.X"]
            =standardOperators["Commutator"]["ValDerY.kZ.X"]
            =standardOperators["Commutator"]["ValDerY.Z.kX"]
            =standardOperators["Commutator"]["ValDerY.kZ.kX"]
            =standardOperators["Commutator"]["ValDerZ.X.Y"]
            =standardOperators["Commutator"]["ValDerZ.kX.Y"]
            =standardOperators["Commutator"]["ValDerZ.X.kY"]
            =standardOperators["Commutator"]["ValDerZ.kX.kY"]
            =standardOperators["Commutator"]["ValDerZ.Y.X"]
            =standardOperators["Commutator"]["ValDerZ.kY.X"]
            =standardOperators["Commutator"]["ValDerZ.Y.kX"]
            =standardOperators["Commutator"]["ValDerZ.kY.kX"]
            ="("+standardOperators["Commutator"]["ValDerX"]+")<Id><Id>";

    standardOperators["Commutator"]["Phi.Eta.ValDerRn"]=
            "iLaserAz[t]<1><Q><0,0>+iLaserAx[t]<cos(Q)><sqrt(1-Q*Q)><0,0>+iLaserAy[t]<sin(Q)><sqrt(1-Q*Q)><0,0>+1/2<1><1><1,0>-1/2<1><1><0,1>";

    standardOperators["Commutator"]["Phi1.Eta1.Phi2.Eta2.ValDerRn1.Rn2"]="iLaserAz[t]<1><Q><1><1><0,0><1>+iLaserAx[t]<cos(Q)><sqrt(1-Q*Q)><1><1><0,0><1>+iLaserAy[t]<sin(Q)><sqrt(1-Q*Q)><1><1><0,0><1>+1/2<1><1><1><1><1,0><1>-1/2<1><1><1><1><0,1><1>";
    standardOperators["Commutator"]["Phi1.Eta1.Phi2.Eta2.Rn1.ValDerRn2"]="<1><1>(iLaserAz[t]<1><Q><1><0,0>+iLaserAx[t]<cos(Q)><sqrt(1-Q*Q)><1><0,0>+iLaserAy[t]<sin(Q)><sqrt(1-Q*Q)><1><0,0>+1/2<1><1><1><1,0>-1/2<1><1><1><0,1>)";

    standardOperators["Commutator"]["Phi.Eta.ValDerRn"]=
            "<1><Q>iLaserAz[t]<0,0>+<cos(Q)><sqrt(1-Q*Q)>iLaserAx[t]<0,0>+<sin(Q)><sqrt(1-Q*Q)>iLaserAy[t]<0,0>+<1><1>1/2<1,0>-<1><1>1/2<0,1>";

    standardOperators         ["Commutator"]["Phi.Eta.ValDerRn.Vec"]
            =standardOperators["Commutator"]["Phi1.Eta1.ValDerRn1.Vec"]
            =standardOperators["Commutator"]["Phi2.Eta2.ValDerRn2.Vec"]
            ="("+standardOperators["Commutator"]["Phi.Eta.ValDerRn"]+")<Id>";

    standardOperators         ["Commutator"]["Phi1.Eta1.ValDerRn1.Phi2.Eta2.Rn2"]
            =standardOperators["Commutator"]["Phi1.Eta1.ValDerRn1.Phi2.Eta2.kRn2"]
            =standardOperators["Commutator"]["Phi2.Eta2.ValDerRn2.Phi1.Eta1.Rn1"]
            =standardOperators["Commutator"]["Phi2.Eta2.ValDerRn2.Phi1.Eta1.kRn1"]
            ="("+standardOperators["Commutator"]["Phi.Eta.ValDerRn"]+")<Id><Id><Id>";

    standardOperators["Commutator"]["Phi1.Eta1.Phi2.Eta2.ValDerRn1.Rn2"]="iLaserAz[t]<1><Q><1><1><0,0><1>+iLaserAx[t]<cos(Q)><sqrt(1-Q*Q)><1><1><0,0><1>+iLaserAy[t]<sin(Q)><sqrt(1-Q*Q)><1><1><0,0><1>+1/2<1><1><1><1><1,0><1>-1/2<1><1><1><1><0,1><1>";
    standardOperators["Commutator"]["Phi1.Eta1.Phi2.Eta2.Rn1.ValDerRn2"]="iLaserAz[t]<1><1><1><Q><1><0,0>+iLaserAx[t]<1><1><cos(Q)><sqrt(1-Q*Q)><1><0,0>+iLaserAy[t]<1><1><sin(Q)><sqrt(1-Q*Q)><1><0,0>+1/2<1><1><1><1><1><1,0>-1/2<1><1><1><1><1><0,1>";
    standardOperators["Commutator"]["Phi1.Eta1.Phi2.Eta2.kRn1.ValDerRn2"]="iLaserAz[t]<1><1><1><Q><1><0,0>+iLaserAx[t]<1><1><cos(Q)><sqrt(1-Q*Q)><1><0,0>+iLaserAy[t]<1><1><sin(Q)><sqrt(1-Q*Q)><1><0,0>+1/2<1><1><1><1><1><1,0>-1/2<1><1><1><1><1><0,1>";;
    standardOperators["Commutator"]["Phi1.Eta1.Phi2.Eta2.ValDerRn1.kRn2"]="iLaserAz[t]<1><Q><1><1><0,0><1>+iLaserAx[t]<cos(Q)><sqrt(1-Q*Q)><1><1><0,0><1>+iLaserAy[t]<sin(Q)><sqrt(1-Q*Q)><1><1><0,0><1>+1/2<1><1><1><1><1,0><1>-1/2<1><1><1><1><0,1><1>";

}


//string OperatorDefinition::undefined="UNDEFINED";

void OperatorDefinition::setParameters(const string Definition){
    size_t pos=Definition.find("<<",0);
    while(pos!=string::npos){
        extractParameters(Definition.substr(pos,Definition.find(">>",pos)+2-pos));
        pos=Definition.find("<<",pos+2);
    }
}

void OperatorDefinition::truncationRadius(double Rsmooth, double Rtrunc){
    Algebra::addSpecialConstant("Rsmooth",Rsmooth);
    Algebra::addSpecialConstant("Rtrunc",Rtrunc);
}

string OperatorDefinition::extractParameters(const string Name)
{
    if(tools::subStringCount(Name,"<<")>1)ABORT("only for single standard operator in definition");
    string name=tools::stringInBetween(Name,"<<",">>",true);
    size_t pos=name.find(":");
    if(pos==string::npos)return Name;

    vector<string>pars=tools::splitString(name.substr(pos+1),',');
    for(vector<string>::iterator par=pars.begin();par!=pars.end();par++){
        size_t sep=par->find("=");
        if(sep==string::npos)ABORT("invalid parameter specification in "+Name+"\n specify as in Name(par=123)");
        Algebra::addSpecialConstant(par->substr(0,sep),tools::string_to_double(par->substr(sep+1)));
    }

    return name.substr(0,pos);
}

std::string OperatorDefinition::get(std::string Name, std::string Hierarchy){
    string coor=Index::coordinates(Hierarchy);
    Name=extractParameters(Name); // e.g. for <<MixedGaugeDipole:Rg=xx>>: add value Rg to parameter list with value xx

//    //HACK really bad
//    int unboundDOF;
//    ReadInput::main.read("Source","UnboundDOF",unboundDOF,"0","Unbound degrees of freedom",1,"unbound");
//    if(unboundDOF>0)Name=tools::stringInBetween(Name,"<<",">>",true);
    ReadInput::main.obsolete("Source","undoundDOF","do not use, instead specify region as Rn1, Rn2, Rn1.Rn2, etc.");

    if(not tools::hasKey(standardOperators,Name)){
        return "undefined <<"+Name
            +">>\n available operators:\n"+tools::listMapKeys(standardOperators,"\n");
    }

    std::vector<string> keys=tools::vectorMapKeys(standardOperators[Name]);
    std::vector<unsigned int> perm;
    string opDef="";
    for(unsigned int k=0;k<keys.size();k++){
        perm=tools::permutation(tools::splitString(keys[k],'.'),tools::splitString(coor,'.'));
        if(perm.size()!=0){
            opDef=unBracket(standardOperators[Name][keys[k]]);
            break;
        }
    }
    
    if(opDef.length()==0 and (Name=="Overlap" or Name=="1")){  // use default Overlap: <1><1>...
        standardOperators["Overlap"][coor]="<1>";
        for(int k=0;k<std::count(coor.begin(),coor.end(),'.');k++)standardOperators["Overlap"][coor]+="<1>";
        opDef=unBracket(standardOperators["Overlap"][coor]);
    }

    if(opDef.length()==0 and (Name=="GridWeights")){  // use default Overlap: <1><1>...
        standardOperators[Name][coor]="<GridWeight>";
        for(int k=0;k<std::count(coor.begin(),coor.end(),'.');k++){
            standardOperators[Name][coor]+="<GridWeight>";
        }
        opDef=unBracket(standardOperators[Name][coor]);
    }

    if(opDef.length()==0)return "undefined <<"+Name+">> for "+coor
            +"\n available for permutations of\n"+tools::listMapKeys(standardOperators[Name],"\n");

    bool permuted=false;
    for(unsigned int i=0;i<perm.size();i++) if(perm[i]!=i) {permuted=true; break;}
    if(not permuted) return opDef;

    // for now, we cannot handle brackets outside factors
    if(tools::findFirstOutsideBrackets(opDef,"(","<",">")!=string::npos)
        ABORT("cannot handle brackets ouside factors\nDef: "+opDef);

    return opPermuted(opDef,perm);
}

string OperatorDefinition::addConstraint(string Def, string ConstType, int Pos, string C){
    size_t cpos=Def.find(ConstType);
    while(cpos!=string::npos){
        for(int k=0;k<Pos+1 and cpos!=string::npos;k++)cpos=Def.find(".",cpos);
        if(cpos==string::npos)
            ABORT(Str("Def="+Def+": no position=")+Pos+("in constraint $"+ConstType));
        Def.insert(cpos,"."+C);
        cpos=Def.find(ConstType,cpos);
    }
    return Def;
}

string OperatorDefinition::opPermuted(const string & Op, const std::vector<unsigned int> Perm){
    string oPerm;
    string op(Op);
    if(tools::subStringCount(op,"<")%Perm.size()!=0)
        ABORT("incorrect number of factors in operator definition\n"
              +Op+": factors="+tools::str(tools::subStringCount(op,"<"))+", dimension="+tools::str(Perm.size()));

    vector<string> term,sign;
    tools::splitString(Op,"+-",term,sign,"<|",">>");
    for(unsigned int k=0;k<term.size();k++){
        string front=sign[k]+term[k].substr(0,term[k].find('<'));
        string back=term[k].substr(term[k].find('<'));
        vector<string>fac=tools::splitString(back,'>');
        while(fac.back()=="")fac.pop_back(); // remove trailing empty strings
        oPerm+=front;
        // permute into oPerm
        for(unsigned int l=0;l<Perm.size();l++){
            oPerm+=fac[Perm[l]]+">";
        }
        // permute the constraints
        if(fac.size()>Perm.size()){
            vector<string>lCons=tools::splitString(fac.back(),'.');
            if(lCons.size()<Perm.size()+1)
                ABORT(Str("number of constraints in ")+term[k]+"smaller than permutation "+Perm);
            if(tools::cropString(lCons[0])!="$BAND")ABORT("only band qualifiers implemented, is: '"+lCons[0]+"'\n"+Op);
            oPerm+=tools::cropString(lCons[0]);
            for(unsigned int l=0;l<Perm.size();l++)oPerm+="."+lCons[1+Perm[l]];
        }
    }

    return oPerm;
}

void OperatorDefinition::syntax() const {
    if(find("<d_-")!=string::npos or find("-_d>")!=string::npos or find("<d_+")!=string::npos or find("+_d>")!=string::npos){
        //note: tests is too crude...
        ABORT("malformed operator: "+*this+"\ndo not use signes next to _d> or <d_, enclose in brackets, e.g. <d_(-0.5)> etc.");
    }
}

void OperatorDefinition::Test(){

    OperatorDefinition d;
    cout<<"Operators and some permutations"<<endl;
    cout<<"Laplacian(Rn.Phi.Eta): "<<get("Laplacian","Rn.Phi.Eta")<<endl;
    cout<<"Laplacian(Phi.Rn.Eta): "<<get("Laplacian","Phi.Rn.Eta")<<endl;
    cout<<"Laplacian(Phi.Rn.bad): "<<get("Laplacian","Phi.Eta.bad")<<endl;
    cout<<"Laplacian(Phi.Eta.Rn): "<<get("Laplacian","Phi.Eta.Rn")<<endl;
    cout<<"Laplacian(Rn1.Rn2.Phi1.Phi2.Eta1.Eta2): "<<get("Laplacian","Rn1.Rn2.Phi1.Phi2.Eta1.Eta2")<<endl;
    cout<<"D/DX(Rn.Phi.Eta): "<<get("D/DX","Rn.Phi.Eta")<<endl;
    cout<<"D/DX(Phi.Rn.Eta): "<<get("D/DX","Phi.Rn.Eta")<<endl;
    cout<<"D/DX(Eta.Phi.Rn): "<<get("D/DX","Eta.Phi.Rn")<<endl;
}
