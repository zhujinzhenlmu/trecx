// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "basisMat1D.h"

#include "basisAbstract.h"
#include "algebra.h"

#include <vector>
using namespace std;

#include "basisIntegrable.h"
#include "basisDvr.h"
#include "basisSub.h"
#include "basisGridQuad.h"

static const double ErrorDouble=DBL_MAX;

/// Mat = <IBas|Op|JBas>, IBas and JBas are the bases at IIndex and JIndex
/// <br> Op=factor_string<operator_string>
/// <br> factor_string must be a valid string for class Algebra
/// <br> operator_string any of <alg>,<d_alg>,<alg_d> and <d_alg_d>
/// with alg a valid string for class Algebra
BasisMat1D::BasisMat1D(std::string Op, const Index * IIndex, const Index* JIndex){
    if(not IIndex->isBottom())return;

    BasisMat1D bm(Op,IIndex->basisAbstract(),JIndex->basisAbstract());
    _mat=bm._mat;
}

BasisMat1D::BasisMat1D(std::string Op, int Idim, int Jdim){
    if(Op.find("<Id>")==1){
        if(Idim!=Jdim)return;
        _mat=Eigen::MatrixXcd::Identity(Idim,Jdim);
    }
    else if (Op.find(",")<Op.find_first_of(">[({")){
        int i=std::stoi(Op.substr(Op.find('<')+1,Op.find(',')));
        if(i<0 or i>=Idim)return;
        int j=std::stoi(Op.substr(Op.find(',')+1,Op.find('>')));
        if(j<0 or j>=Jdim)return;
        _mat=Eigen::MatrixXcd::Zero(Idim,Jdim);
        _mat(i,j)=1;
    }
}

BasisMat1D::BasisMat1D(string Op, const BasisAbstract *IBas, const BasisAbstract *JBas){

    //HACK until better solution======================================================

    if(Op=="<Id>" and *IBas==*JBas){
        _mat=Eigen::MatrixXcd::Identity(IBas->size(),JBas->size());
        return;
    }

    const BasisAbstract * iBas=BasisSub::superBas(IBas);
    const BasisAbstract * jBas=BasisSub::superBas(JBas);

    if(Op=="<GridWeight>"){
        if(not(*iBas==*jBas))DEVABORT("GridWeight "+jBas->str()+" != "+JBas->str());
        if(dynamic_cast<const BasisGridQuad*>(iBas)){
            const BasisGridQuad* g=dynamic_cast<const BasisGridQuad*>(iBas);
            _mat=Eigen::MatrixXcd::Zero(g->size(),g->size());
            for(int k=0;k<g->size();k++)_mat(k,k)=g->weights()[k];
        }
        else if(dynamic_cast<const BasisGrid*>(iBas)){
            const BasisGrid* g=dynamic_cast<const BasisGrid*>(iBas);
            _mat=Eigen::MatrixXcd::Zero(g->size(),g->size());
            if(g->size()==1)_mat(0,0)=1.;
            if(g->size()==2){
                _mat(0,0)=g->mesh()[1]-g->mesh()[0];
                _mat(1,1)=_mat(0,0);
            }
            else {
                for(int k=1;k<g->size()-1;k++)_mat(k,k)=0.5*(g->mesh()[k+1]-g->mesh()[k-1]);
                _mat(0,0)=_mat(1,1);
                _mat(g->size()-1,g->size()-1)=_mat(g->size()-2,g->size()-2);
            }
        }
        else
            DEVABORT("cannot do "+Op+" for basis"+IBas->str());
        _mat=BasisSub::subMatrix(_mat,BasisSub::subset(IBas),BasisSub::subset(JBas));
        return;
    }

    if(iBas->isGrid() or jBas->isGrid()
            or iBas->isIndex() or jBas->isIndex() or iBas->name()=="CIion" or jBas->name()=="CIion"){
        //HACK
        if(Op=="<1>"){
            _mat=BasisSub::subMatrix(Eigen::MatrixXcd::Identity(iBas->size(),jBas->size()),
                                     BasisSub::subset(IBas),BasisSub::subset(JBas));
            return;
        }
        if(tools::findFirstOutsideBrackets(Op,",","[","]")!=string::npos){
            vector<string> ij=tools::splitString(Op,',');
            if(ij.size()==2){
                _mat=Eigen::MatrixXcd::Zero(iBas->size(),iBas->size());
                _mat(std::stoi(ij[0].substr(1)),std::stoi(ij[1].substr(0,ij[1].find('>'))))=1.;
                return;
            }
        }
    }
    //==================================================================================


    // get exact or dvr quadrature
    const BasisIntegrable * bi,*bj;
    UseMatrix quadX,quadW;
    auto testPtr = dynamic_cast<const BasisSet*>(iBas);
    if(testPtr != nullptr && testPtr->isCIstate()) return;
    if(iBas->isDVR() and jBas->isDVR()){
        const BasisSet* bs;
        const BasisDVR* dv;
        if     (0!=(bi=bs=dynamic_cast<const BasisSet*>(iBas))){
            bs->dvrRule(quadX,quadW,true);
        }
        else if(0!=(bi=dv=dynamic_cast<const BasisDVR*>(iBas))){
            dv->dvrRule(quadX,quadW);
        }
        else DEVABORT("isDVR, but neither BasisSet nor BasisDVR");
    }
    else if (0!=(bi=dynamic_cast<const BasisIntegrable*>(iBas)) and 0!=(bj=dynamic_cast<const BasisIntegrable*>(jBas))){
        bi->quadRule(max(bi->order()+2,bj->order()+2),quadX,quadW);
    }
    else return;
    bj=dynamic_cast<const BasisIntegrable*>(jBas);
    bi->jacobian()->operator ()(quadX,quadW);

    // functions may have discontinuities or  singularities at the interval boundaries
    // --- evaluate slightly away from boundaries ---
    // epsilon for moving away from boundary
    double epsLo=1.e-14,epsUp=1.e-14;
    if(bi->lowBound()>-BasisIntegrable::infty)epsLo=epsLo+1.e-14*abs(bi->lowBound());
    if(bi->upBound() < BasisIntegrable::infty)epsUp=epsUp+1.e-14*abs(bi->upBound());

    // this should go to basis integrable, maybe into the complexScaling() function
    // make sure complex scaling radius is at boundary
    double eps=(bi->upBound()-bi->lowBound())*1.e-12;
    if(bi->complexScaling()->r0up()>bi->lowBound()+eps and bi->complexScaling()->r0up()<bi->upBound()-eps)
        ABORT(Str("complex scaling radius must be on element boundary, is inside: lb=")+
              bi->lowBound()+"< r0="+bi->complexScaling()->r0up()+"< ub="+bi->upBound());

    UseMatrix funcArg;
    funcArg=quadX;
    for(int k=0;k<quadX.size();k++){
        funcArg(k)=bi->complexScaling()->xScaled(quadX(k).real());
        quadW(k)*=bi->complexScaling()->etaX(0.5*(bi->lowBound()+bi->upBound()));
        if(abs(funcArg(k).real()-bi->lowBound())<=epsLo)funcArg(k)=complex<double>(bi->lowBound()+epsLo,funcArg(k).imag());
        if(abs(funcArg(k).real()-bi->upBound()) <=epsUp)funcArg(k)=complex<double>(bi->upBound() -epsUp,funcArg(k).imag());
    }

    complex<double> preFac;
    string fac=Op.substr(0,Op.find("<"));
    if(fac=="")fac="1";
    Algebra facAlg(fac);
    if(not facAlg.isAlgebra())return;
    preFac=facAlg.val(0.);

    string opFunc=tools::stringInBetween(Op,"<",">");
    if(opFunc.find("d_")==0)opFunc=opFunc.substr(2);
    if(opFunc.find("_d")==opFunc.length()-2)opFunc=opFunc.substr(0,opFunc.length()-2);
    Algebra funcAlg(opFunc);
    if(not funcAlg.isAlgebra())return; // not a legal algebra string

    // we should check compatibility
    Eigen::MatrixXcd iVals(valDer(quadX,bi,Op.find("<d_")!=string::npos));
    Eigen::MatrixXcd jVals(valDer(quadX,bj,Op.find("_d>")!=string::npos));

    Eigen::VectorXcd funcWeig=Eigen::VectorXcd::Zero(quadX.size());
    for(int k=0;k<quadX.size();k++){
        if(iVals.row(k).isZero() or jVals.row(k).isZero())continue; // skip zero entries (mostly for avoiding singulariets at r=0)
        funcWeig(k)=funcAlg.val(funcArg(k).complex())*quadW(k).complex();
    }

    _mat=iVals.adjoint()*funcWeig.asDiagonal()*jVals*preFac;
    _mat.purge();

    // complex scaling
    _mat/=std::pow(bi->complexScaling()->etaX(0.5*(bi->lowBound()+bi->upBound())),tools::subStringCount(Op,"_d>")+tools::subStringCount(Op,"<d_"));
    _mat=BasisSub::subMatrix(_mat,BasisSub::subset(IBas),BasisSub::subset(JBas));
}

Eigen::MatrixXcd BasisMat1D::valDer(const UseMatrix &X, const BasisIntegrable *Bas, int Derivative) const
{
    vector<complex<double> > x,val,dum;
    for(int k=0;k<X.size();k++)x.push_back(X.data()[k]);
    if(     Derivative==0)Bas->valDer(x,val,dum);
    else if(Derivative==1)Bas->valDer(x,dum,val);
    vector<int> allIdx;
    for(int k=0;k<X.size();k++)allIdx.push_back(k);

    return BasisSub::subMatrix(Eigen::Map<Eigen::MatrixXcd>(val.data(),x.size(),val.size()/x.size()),
                               allIdx,BasisSub::subset(Bas));
}
