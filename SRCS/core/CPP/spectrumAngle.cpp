#include "spectrumAngle.h"
#include "polylagrange.h"
#include "basisGrid.h"
#include "coefficients.h"
#include "coefficientsPermute.h"
#include "index.h"
#include "plot.h"
#include "plotKind.h"
#include "evaluator6D.h"
#include "discretizationSurface.h"
#include "discretizationtsurffspectra.h"
#include "printOutput.h"
#include "coefficientsFloor.h"
#include "mpiWrapper.h"
using namespace std;


static void getEIndex(Coefficients *C, double E1, double E2, unsigned int &k1Idx, unsigned int &k2Idx) {
    k1Idx = 0, k2Idx = 0;
    for (unsigned int i = 0; i < C->firstFloor()->idx()->basisGrid()->Points().rows(); i++) {
        if (C->firstFloor()->idx()->basisGrid()->Points()(i, 0).real() < sqrt(2 * E1))k1Idx = i;
        if (C->firstFloor()->idx()->basisGrid()->Points()(i, 0).real() < sqrt(2 * E2))k2Idx = i;
    }
    k1Idx ++;
    k2Idx ++;
}

void SpectrumAngle::getKIdx(double E1, double E2, unsigned int &k1Idx, unsigned int &k2Idx, std::vector<double> KGrids) {
    k1Idx = 0, k2Idx = 0;
    for (unsigned int i = 0; i < KGrids.size(); i++) {
        if (KGrids[i] < sqrt(2 * E1))k1Idx = i;
        if (KGrids[i] < sqrt(2 * E2))k2Idx = i;
    }
    k1Idx ++;
    k2Idx ++;
}


Discretization *SpectrumAngle::getJADDisc(ReadInput dataInp) {
    double surfRad;
    dataInp.read("Surface", "points", surfRad, "", "save values and derivatives at surface points (blank-separated list)");
    Discretization *D = Discretization::factory(dataInp);
    DiscretizationSurface *S1 = new DiscretizationSurface(D, vector<double>(1, surfRad), 1);
    Discretization *Dtf1 = DiscretizationTsurffSpectra::factoryTsurff(S1, dataInp);
    delete S1;
    DiscretizationSurface *S2 = new DiscretizationSurface(Dtf1, vector<double>(1, surfRad), 0);
    delete Dtf1;
    Discretization *Dtf2 = DiscretizationTsurffSpectra::factoryTsurff(S2, dataInp);
    const Index *dt = Dtf2->idx()->firstLeaf()->parent();
    while (dt != 0) {
        while(dt->childSize()>0)
            const_cast<Index *>(dt)->childPop();
        std::vector<double> grids = {0.,0.};//using {0.} leads to memory corruption, the reason need furthre investigation
        const_cast<Index *>(dt)->childAdd(new Index({BasisGrid::factory(grids)},{"kRn2"}));
        dt = dt->nodeRight();
    }
    Dtf2->idx()->sizeCompute();
    return Dtf2;
}

void SpectrumAngle::initialize(ReadInput &dataInp) {
    // the purpose is to store each possible flags into the readInput main table
    double thetaNum, phiNum, EAverage;
    dataInp.read("JAD", "thetaNum", thetaNum, "200", "The number of thetas");
    dataInp.read("JAD", "phiNum", phiNum, "5", "The number of phis");
    dataInp.read("JAD", "EAverage", EAverage, "0.", "The averaging JAD range");
    unsigned int EIndex = 0, startKIdx = 0, endKIdx = 0;
    double E1 = 0., E2 = 0.;
    while (true) {
        dataInp.read("JAD", "E1" + tools::str(EIndex), E1, "0.", "The selected E1 energy");
        dataInp.read("JAD", "E2" + tools::str(EIndex), E2, "0.", "The selected E2 energy");
        EIndex++;
        if (E1 == 0. and E2 == 0.)break;
    }
    dataInp.read("JAD", "startKIdx", startKIdx, "0", "The selected E1 energy");
    dataInp.read("JAD", "endKIdx", endKIdx, "0", "The selected E2 energy");
}

SpectrumAngle::SpectrumAngle(const Coefficients amplitutes, ReadInput inpc, string plotWhat, std::vector<string> head, Discretization *D) {
    _head = head;
    _plotWhat = plotWhat;
    _ampl = new Coefficients(amplitutes);
    _specFile = inpc.outputTopDir() + "/spec_" + plotWhat;
    kGrids = _ampl->firstFloor()->idx()->basisGrid()->mesh();
    Nk = kGrids.size();
    Inp = inpc;
    Inp.read("JAD", "thetaNum", _thetaNum, "200", "The number of thetas");
    Inp.read("JAD", "phiNum", _phiNum, "5", "The number of phis");
    Inp.read("JAD", "EAverage", _EAverage, "0.", "The averaging JAD range");
//    ABORT("111111" +tools::str(_thetaNum));
    PrintOutput::message("The parameters are " + tools::str(_thetaNum) + " " + tools::str(_phiNum) + " " + tools::str(_EAverage));
    DPlot = D != 0 ? D : getJADDisc(Inp);
    if (plotWhat == "JAD") {
        // Here
        vector<string> axisJAD = {"Eta1", "Eta2"}, useJAD(2, "g");
        vector<unsigned int> pointsJAD = {_thetaNum, _thetaNum};
        vector<double> boundEta = {-1, 1};
        vector<vector<double> > boundsJADs(2, boundEta);
        // selecting the 0, \pi for phi values
        boundsJADs.push_back({0, 2 * math::pi});
        boundsJADs.push_back({0, 2 * math::pi});
        useJAD = {"g", "g", "g", "g"};
        axisJAD = {"Eta1", "Eta2", "Phi1", "Phi2"};
        pointsJAD = {_thetaNum * 2, _thetaNum * 2, _phiNum, _phiNum};
        _plotJAD = new Plot(DPlot->idx(), axisJAD, useJAD, pointsJAD, boundsJADs);
    } else if (plotWhat == "antiE" or plotWhat == "intK") {
        if (amplitutes.idx()->hierarchy().find("Phi1") != string::npos) {
            vector<string> axisJAD = {"kRn1", "kRn2", "Eta1", "Eta2"}, useJAD(4, "g");
            vector<unsigned int> pointsJAD = {Nk, Nk, _thetaNum, _thetaNum};
            vector<double> boundJAD = {-1, 1}, boundK = {0., 0.};
            vector<vector<double> > boundsJAD(2, boundK);
            boundsJAD.push_back(boundJAD);
            boundsJAD.push_back(boundJAD);
            _plotJAD = new Plot(DPlot->idx(), axisJAD, useJAD, pointsJAD, boundsJAD);
        } else {
            vector<string> axisJAD = {"kRn", "Eta"}, useJAD({"g", "g"});
            vector<unsigned int> pointsJAD = {0, _thetaNum};
            vector<double> boundJAD = {-1, 1};
            vector<vector<double> > boundsJAD(1, vector<double>({2, 0.}));
            boundsJAD.push_back(boundJAD);
            _plotJAD = new Plot(DPlot->idx(), axisJAD, useJAD, pointsJAD, boundsJAD);
        }

    }
    else
        ABORT("The flag " + _plotWhat + "is not acceptable");

}

void SpectrumAngle::run() {
    if (_plotWhat == "JAD")
        JAD();
    else if (_plotWhat == "intK") {
        if (_ampl->idx()->hierarchy().find("Phi1") != string::npos)
            intK();
        else
            intK3D();
    } else if (_plotWhat == "antiE")
        antiCorrYield();
    else
        ABORT("The flag " + _plotWhat + "is not acceptable");
}

std::vector<std::vector<double>> SpectrumAngle::getJADPlot(unsigned int k1Idx, unsigned int k2Idx, bool toIntegrate) {
    Coefficients *coefJAD = new Coefficients(DPlot->idx());
    coefJAD->setToZero();
    Coefficients *C1 = coefJAD->firstFloor();
    Coefficients *C2 = _ampl->firstFloor();
    while (C1) {
        *(C1->floorData()) = *(C2->floorData() + k1Idx * Nk + k2Idx);
        C1 = C1->nodeRight();
        C2 = C2->nodeRight();
    }
    std::vector<std::vector<double>> tem = _plotJAD->sum(*coefJAD, _specFile, true);
    if (toIntegrate)
        for (unsigned int i = 0; i < tem.back().size(); i++) {
            double intElem = pow(kGrids[k1Idx], 2) * (k1Idx > 0 ? kGrids[k1Idx] - kGrids[k1Idx - 1] : kGrids[0]);
            intElem *= pow(kGrids[k2Idx], 2) * (k2Idx > 0 ? kGrids[k2Idx] - kGrids[k2Idx - 1] : kGrids[0]);
            tem.back()[i] *= intElem;
        }
    delete coefJAD, C1;
    return tem;
}

void SpectrumAngle::plotByData(std::vector<std::vector<double>> result, std::string fileName, int blockRows) {
    ofstream fileToWrite(Inp.outputTopDir() + "/" + fileName);
    for (unsigned int i = 0; i < result[0].size(); i++) {
        for (unsigned int j = 0; j < result.size() - 1; j++)
            fileToWrite << setprecision(7) << result[j][i] << ", ";
        fileToWrite << setprecision(7) << result.back()[i] << endl;
        if ((i + 1) % blockRows == 0 and i != result[0].size() - 1)fileToWrite << endl;
    }
    fileToWrite.close();
}

void SpectrumAngle::JAD() {
    PrintOutput::DEVwarning("Plot the JAD");
    unsigned int k1Idx, k2Idx, EIndex = 0;
    double E1 = 0., E2 = 0.;
    while (true) {
        Inp.read("JAD", "E1" + tools::str(EIndex), E1, "0.", "The selected E1 energy");
        Inp.read("JAD", "E2" + tools::str(EIndex), E2, "0.", "The selected E1 energy");
        PrintOutput::message("The E1, E2 selected at EIndex " + tools::str(EIndex) + " is " + tools::str(E1) + ", " + tools::str(E2));
        if (E1 == 0. or E2 == 0.) {
            PrintOutput::message("No E1, E2 selected at " + tools::str(EIndex));
            break;
        }
        getKIdx(E1, E2, k1Idx, k2Idx,kGrids);
        unsigned int k1LB = 0, k1UB = 0, k2LB = 0, k2UB = 0;
        if (abs(_EAverage) > 1e-5) {
            getKIdx(E1 - _EAverage, E2 - _EAverage, k1LB, k2LB,kGrids);
            getKIdx(E1 + _EAverage, E2 + _EAverage, k1UB, k2UB,kGrids);
            PrintOutput::message("The kIndex is " + tools::str(k1LB) + "-" + tools::str(k1UB) + ", " + tools::str(k2LB) + "-" + tools::str(k2UB));
        } else
            PrintOutput::message("The kIndex is " + tools::str(k1Idx) + ", " + tools::str(k2Idx));
        if (k1LB * k1UB * k2LB * k2UB == 0) {
            k1LB = k1Idx;
            k1UB = k1Idx + 1;
            k2LB = k2Idx;
            k2UB = k2Idx + 1;
        }
        std::vector<std::vector<double>> result;
        for (unsigned int k1IdxTem = k1LB; k1IdxTem < k1UB; k1IdxTem++) {
            for (unsigned int k2IdxTem = k2LB; k2IdxTem < k2UB; k2IdxTem++) {
                std::vector<std::vector<double>> tem = getJADPlot(k1IdxTem, k2IdxTem, (k1UB - k1UB) * (k2UB - k2LB) > 1);
                if (k1IdxTem == k1LB and k2IdxTem == k2LB) {
                    result = tem;
                } else {
                    for (unsigned int i = 0; i < tem[4].size(); i++)
                        result[4][i] += tem[4][i];
                }
            }
        }
        if (MPIwrapper::isMaster()) {
            plotByData(result, "/spec_JAD_" + tools::str(E1) + "," + tools::str(E2), _phiNum);
        }
        EIndex++;
    }
}

void SpectrumAngle::intK3D() {
    // do the loop for each k grid
    vector<vector<double> > results;
    vector<vector<double> > tem = _plotJAD->sum(*_ampl, _specFile, true);
    results.resize(2);
    results[0].resize(_thetaNum);
    results[1].resize(_thetaNum);
    for (unsigned int k1Idx = 0; k1Idx < Nk; k1Idx++) {
        double intElem = pow(kGrids[k1Idx], 2) * (k1Idx > 0 ? (kGrids[k1Idx] - kGrids[k1Idx - 1]) : kGrids[0]);
        for (unsigned int i = k1Idx * _thetaNum; i < (k1Idx + 1)*_thetaNum; i++) {
            if (k1Idx == 0)
                results[0][i] = tem[1][i];
            results[1][i - k1Idx * _thetaNum] += tem[2][i] * intElem;
        }
    }
    plotByData(results, "spec_intK", _thetaNum);
}

void SpectrumAngle::intK() {
    PrintOutput::DEVwarning("Plot the intK");
    // do the Eta grid for each K grid, then sum them up. sumK does the same thing as intK, but somethines much faster
    unsigned int startKIdx = 0, endKIdx = 0;
    Inp.read("JAD", "startKIdx", startKIdx, "0", "The selected E1 energy");
    Inp.read("JAD", "endKIdx", endKIdx, "0", "The selected E1 energy");
    int kIdx = 0;
    double startK = -1., endK = -1.;
    while (kIdx < 100) {
        if (ReadInput::main.flag("DEBUGStartK" + tools::str(double(kIdx / 100.), 2), "DEBUGStartK" + tools::str(double(kIdx / 100))))
            startK = double(kIdx / 100.);
        if (ReadInput::main.flag("DEBUGEndK" + tools::str(0.5 + double(kIdx / 100.), 3), "DEBUGEndK" + tools::str(0.5 + double(kIdx / 100.), 3)))
            endK = double(0.5 + double(kIdx / 100.));
        kIdx++;
    }
    // read the input startK and endK in parameters
    if (startK >= 0 and endK >= 0) {
        getKIdx(pow(startK, 2) / 2, pow(endK, 2) / 2, startKIdx, endKIdx,kGrids);
// to preserve a consistency of previous code
        startKIdx--;
        endKIdx--;
    }
    std::cout << "The startK and endK index " << startKIdx << ", " << endKIdx << std::endl;
    endKIdx = endKIdx == 0 ? Nk : endKIdx;
    // do the loop for each k grid
    vector<vector<double> > results;
    results.resize(3);
    for (unsigned int i = 0; i < results.size(); i++)
        results[i].resize(_thetaNum * _thetaNum);
    for (unsigned int k1Idx = startKIdx; k1Idx < endKIdx; k1Idx++) {
        for (unsigned int k2Idx = startKIdx; k2Idx < endKIdx; k2Idx++) {
            vector<vector<double> > tem = getJADPlot(k1Idx, k2Idx, (endKIdx - startKIdx) > 1);
            for (unsigned int etaIdx1 = 0; etaIdx1 < _thetaNum; etaIdx1++) {
                for (unsigned int etaIdx2 = 0; etaIdx2 < _thetaNum; etaIdx2++) {
                    if (k1Idx == startKIdx and k2Idx == startKIdx) {
                        results[0][etaIdx1 * _thetaNum + etaIdx2] = tem[2][etaIdx1 * _thetaNum + etaIdx2];
                        results[1][etaIdx1 * _thetaNum + etaIdx2] = tem[3][etaIdx1 * _thetaNum + etaIdx2];
                    }
                    results[2][etaIdx1 * _thetaNum + etaIdx2] += tem[4][etaIdx1 * _thetaNum + etaIdx2];
                }
            }
        }
    }
    if (MPIwrapper::isMaster()) {
        plotByData(results, "/spec_intK", _thetaNum);
    }
}

void SpectrumAngle::antiCorrYield() {
    PrintOutput::DEVwarning("Plot the antiE");
    bool b2b = _plotWhat.find("B2B") != std::string::npos;
    unsigned int startKIdx, endKIdx;
    Inp.read("JAD", "startKIdx", startKIdx, "0", "The selected E1 energy");
    Inp.read("JAD", "endKIdx", endKIdx, "0", "The selected E1 energy");
    endKIdx = endKIdx == 0 ? Nk : endKIdx;
    // do the loop for each k grid
    vector<vector<double> > antiResults, corrResults;
    antiResults.resize(3);
    corrResults.resize(3);
    for (unsigned int i = 0; i < antiResults.size(); i++) {
        corrResults[i].resize(Nk * Nk);
        antiResults[i].resize(Nk * Nk);
    }
    for (unsigned int k1Idx = startKIdx; k1Idx < endKIdx; k1Idx++) {
        for (unsigned int k2Idx = startKIdx; k2Idx < endKIdx; k2Idx++) {
            antiResults[0][k1Idx * Nk + k2Idx] = kGrids[k1Idx];
            antiResults[1][k1Idx * Nk + k2Idx] = kGrids[k2Idx];
            corrResults[0][k1Idx * Nk + k2Idx] = kGrids[k1Idx];
            corrResults[1][k1Idx * Nk + k2Idx] = kGrids[k2Idx];
            vector<vector<double> > tem = getJADPlot(k1Idx, k2Idx);
            antiResults[2][k1Idx * Nk + k2Idx] = 0;
            corrResults[2][k1Idx * Nk + k2Idx] = 0;
            if(b2b){
                for (unsigned int etaIdx1 = 0; etaIdx1 < _thetaNum; etaIdx1++) {
                    for (unsigned int etaIdx2 = 0; etaIdx2 < _thetaNum; etaIdx2++) {
                        if ((etaIdx1 > _thetaNum / 2 and etaIdx2 < _thetaNum / 2) or (etaIdx1 < _thetaNum / 2 and etaIdx2 > _thetaNum / 2))
                            antiResults[2][k1Idx * Nk + k2Idx] += tem[4][etaIdx1 * _thetaNum + etaIdx2] * pow(2.0 / _thetaNum, 2);
                        else if ((etaIdx1 > _thetaNum / 2 and etaIdx2 > _thetaNum / 2) or (etaIdx1 < _thetaNum / 2 and etaIdx2 < _thetaNum / 2))
                            corrResults[2][k1Idx * Nk + k2Idx] += tem[4][etaIdx1 * _thetaNum + etaIdx2] * pow(2.0 / _thetaNum, 2);
                    }
                }
            }else{
                for (unsigned int etaIdx1 = 0; etaIdx1 < _thetaNum; etaIdx1++) {
                    for (unsigned int etaIdx2 = 0; etaIdx2 < _thetaNum; etaIdx2++) {
                        if ((etaIdx1 > _thetaNum / 2 and etaIdx2 < _thetaNum / 2) or (etaIdx1 < _thetaNum / 2 and etaIdx2 > _thetaNum / 2))
                            antiResults[2][k1Idx * Nk + k2Idx] += tem[4][etaIdx1 * _thetaNum + etaIdx2] * pow(2.0 / _thetaNum, 2);
                        else if ((etaIdx1 > _thetaNum / 2 and etaIdx2 > _thetaNum / 2) or (etaIdx1 < _thetaNum / 2 and etaIdx2 < _thetaNum / 2))
                            corrResults[2][k1Idx * Nk + k2Idx] += tem[4][etaIdx1 * _thetaNum + etaIdx2] * pow(2.0 / _thetaNum, 2);
                    }
                }
            }
        }
    }
    if (MPIwrapper::isMaster()) {
        plotByData(antiResults, "/spec_antiE", Nk);
        plotByData(corrResults, "/spec_corrE", Nk);
    }
}
