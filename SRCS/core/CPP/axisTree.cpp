#include "axisTree.h"
#include "readInput.h"

bool nextLower(std::string Current,std::string Next){
    return (Current=="" and Next!="" and Next.find("@")==std::string::npos)  // first level below root
            or Current!="" and std::count(Current.begin(),Current.end(),'@')+1==std::count(Next.begin(),Next.end(),'@'); // next lower hierarchy
}

// nonsensical input with complicated hierarchy
static std::vector<std::string> inputExample=
{
    "Axis: subset,name,functions,nCoefficients,lower end, upper end,order,exactIntegral",
    "        , Vec,,2",
    "        , Vec1,,2",
    "Subspace, Orbital,Eigenbasis[0.5<<Laplacian>>-<<Coulomb>>:Complement],1,2,,1",
    "        , Orbital1,Eigenbasis[0.5<<Laplacian>>-<<Coulomb>>:Complement],1,2,,1",
    "  @sub1 , Vec2,,2",
    "  @sub2 , Vec3,,3",
    " @@sub2 , Vec6,,3",
    " @@sub3 , Vec7,,3",
    "Complement,Phi,,1",
    "        , Eta,assocLegendre{Phi},4,-1,1",
    "        , Rn,polynomial,        40, 0.,40.,20",
    "        @sub3 , Vec4,,2",
    "        @sub4 , Vec5,,3",
    "       @@sub3 , Vec8,,3",
    "       @@sub4 , Vec9,,3",
    "Nonsense,Phi,,1",
    "        , Eta,assocLegendre{Phi},4,-1,1",
    "        , Rn,polynomial,        40, 0.,40.,20"
};

std::vector<Axis> AxisTree::toVector() const {
    std::vector<Axis> ax;
    for(const AxisTree* axt=this;axt!=0;axt=axt->nodeNext()){
        if(axt->childSize()>1)DEVABORT("conversion to vector only for trivial AxisTree, is\n"+Tree::str(1));
        ax.push_back(*axt);
    }
    return ax;
}

AxisTree::AxisTree(ReadInput &Inp, int &Line, std::string Subset):
    Axis(Inp,++Line)
{
    if(basDef.size()==0)DEVABORT(Str("emtpy basis, but non-empty Axis line")+Line);

    Inp.read("Axis","subset",_subset,Subset,"axis will be read into named subset",Line);

    if(Line==1 and _subset!=Subset){
        // list starts with subset, add root
        childAdd(new AxisTree(Inp,--Line,_subset));
        name="root";
        _subset="";
    }

    // concatenate multi-part axes
    while(not Inp.endCategory("Axis",Line+1) and name==Axis::readName(Inp,Line+1,name)){
        Axis xx(Inp,++Line);
        appendInPlace(xx);
    }

    while(not Inp.endCategory("Axis",Line+1)){
        std::string subCur,subNext;
        Inp.read("Axis","subset",subCur,Subset,"axis will be read into named subset",Line);
        Inp.read("Axis","subset",subNext,subCur,"axis will be read into named subset",Line+1);

        if(subCur==subNext or nextLower(subCur,subNext)){
            // stay in subset or attach next lower level
            _subset=subCur;
            childAdd(new AxisTree(Inp,Line,_subset));
            Inp.read("Axis","subset",subNext,subCur,"axis will be read into named subset",Line+1);
        }
        if(not nextLower(_subset,subNext)
                or Inp.endCategory("Axis",Line+1))return; // next cannot be attached - go up

        childAdd(new AxisTree(Inp,Line,_subset));
    }

}

AxisTree::AxisTree(const std::vector<Axis> &Axes):Axis(Axes[0]){
    if(Axes.size()==0)ABORT("cannot construct AxisTree from empty Axis vector");
    if(Axes.size()>1)childAdd(new AxisTree(std::vector<Axis>(Axes.begin()+1,Axes.end())));
}

void AxisTree::nodeCopy(const AxisTree *Node, bool View){
    _subset=Node->_subset;
    Axis::operator=(*Node);
}
std::string AxisTree::strData(int Level) const{
    return Axis::str();
    if(_subset!="")return name+"("+_subset+")";return name;
}

void AxisTree::print() const{
    std::vector<Axis> ax;
    for(const AxisTree* a=this;a!=0;a=a->nodeNext(this)){
        ax.push_back(*a);
        if(a->subset()!="")ax.back().name+="("+a->subset()+")";
    }

    Axis::print(ax);
}


static std::vector<std::string> factorAxes;
static bool isFactor(const AxisTree* Ax){
    return std::find(factorAxes.begin(),factorAxes.end(),Ax->name)!=factorAxes.end();
}
static bool isComplement(const AxisTree* Ax){return not isFactor(Ax);}

static AxisTree* newSubTree(const AxisTree* Ax,bool (*Select)(const AxisTree*)){
    AxisTree * sub;
    if(Select(Ax)){
        sub=new AxisTree();
        Ax->subTree(Select,sub);
    }
    else {
        int cnt=0;
        for(int k=0;k<Ax->childSize();k++)cnt+=Select(Ax->child(k));
        if(cnt==1)
            sub=newSubTree(Ax->child(0),Select);
        else {
            sub=new AxisTree(Axis("Hybrid",cnt,0.,double(cnt-1),"automatic",cnt));
            for(int k=0;k<Ax->childSize();k++)sub->childAdd(newSubTree(Ax->child(k),Select));
        }
    }
    return sub;
}

AxisTree * AxisTree::factor(std::vector<std::string> Factor) const {
    factorAxes=Factor;
    AxisTree * fac=new AxisTree();
    subTree(isFactor,fac);
    return newSubTree(this,isFactor);
}
AxisTree * AxisTree::complement(std::vector<std::string> Factor) const {
    factorAxes=Factor;
    AxisTree * fac=new AxisTree();
    subTree(isComplement,fac);
    return newSubTree(this,isComplement);
}


