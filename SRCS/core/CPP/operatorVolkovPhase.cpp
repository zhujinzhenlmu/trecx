#include "operatorVolkovPhase.h"
#include "discretizationDerived.h"
#include "coefficientsFunction.h"
#include "parameters.h"

OperatorVolkovPhase::OperatorVolkovPhase(const DiscretizationDerived* GridSpecDisc, const std::string KLevel):OperatorAbstract("OperatorVolkovPhase",GridSpecDisc->idx(),GridSpecDisc->idx()){
    volkov = new VolkovPhase(GridSpecDisc,KLevel);
}

void OperatorVolkovPhase::apply(const std::complex<double> A, const Coefficients& X, const std::complex<double> B, Coefficients& Y) const {
    volkov->multiply(&X,&Y,time);
}

void OperatorVolkovPhase::update(double Time, const Coefficients* CurrentVec){
    time=Time;
    Parameters::update(Time);
}
