// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "timePropagator.h"

#include "timeCritical.h"
#include "coefficientsGlobal.h"
#include "index.h"
#include "discretization.h"

//resolve forward declarations
#include "wavefunction.h"
#include "printOutput.h"
#include "plot.h"
#include "parameters.h"
#include "readInput.h"
#include "odeStep.h"
#include "derivativeFlat.h"
#include "parallel.h"
#include "mpiWrapper.h"
#include "pulse.h"
#include "log.h"

using namespace std;
using namespace tools;

TimePropagator::TimePropagator(OdeStep<LinSpaceMap<Coefficients>, Coefficients> *Ode, TimePropagatorOutput *Out, double Precision, double FixStep)
    : out(Out),ode(Ode),odeLocal(0),desiredPrecision(Precision),h_fix(FixStep),h_max(100.),successfulTimeSteps(0),failedTimeSteps(0),h(0.01),hSum(0.),hSquareSum(0.)
{}

TimePropagator::TimePropagator(OdeStep<LinSpaceMap<CoefficientsLocal>, CoefficientsLocal> *Ode, TimePropagatorOutput *Out, double Precision, double FixStep)
    : out(Out),ode(0),odeLocal(Ode),desiredPrecision(Precision),h_fix(FixStep),h_max(100.),successfulTimeSteps(0),failedTimeSteps(0),h(0.01),hSum(0.),hSquareSum(0.)
{}


TIMER(propAll,)
TIMER(propStep,)
TIMER(propGather,)
void TimePropagator::propagate_intern(Coefficients *C, double & Time, double TEnd) {
    timeCritical::suspend();
    // set flag to indicate time-critical phase
    Coefficients errVec(C->idx());
    Coefficients vecT0(C->idx());

    CoefficientsLocal localE(C->idx());
    CoefficientsLocal localT0(C->idx());
    CoefficientsGlobal writeC(C->idx());
    CoefficientsGlobal * globalC=CoefficientsGlobal::view(C);
    CoefficientsLocal * localC  =CoefficientsLocal::view(globalC);
    timeCritical::setOn();

    //propagate from tStart to tEnd
    if(h_fix>h_max)ABORT("cannot fix time step at values > max time step");
    if(h_fix!=0)h=h_fix;
    if(odeLocal!=0)MPIwrapper::Bcast(globalC->storageData(),globalC->size(),MPIwrapper::master());
    vector<double> steps,errs;
    out->timer->start();
    timeCritical::setOn();
    while(Time<TEnd) {
        STARTDEBUG(propAll);
        h=min(h,h_max);

        if(out->timer)out->timer->monitor(Time, "h="+std::to_string(h));

        // we want to hit the final time exactly
        double hTemp=h;
        if(Time+h>TEnd-h*1.e-2)hTemp=TEnd-Time;
        h=min(h,h_max);
        if(h_fix!=0.){
            STARTDEBUG(propStep);
            if(ode!=0)ode->step(*C,Time,hTemp);
            else odeLocal->step(*localC,Time,hTemp);
            STOPDEBUG(propStep);
            successfulTimeSteps++;
            Time+=hTemp;
            Time=Threads::max(Time);
		STARTDEBUG(propGather);
            if(ode!=0)writeC=*C;
            else Parallel::allGather(&writeC,localC);
		STOPDEBUG(propGather);
            out->write(&writeC,Time);
            hSum+=h;
            hSquareSum+=h*h;
        }
        else {
            if(ode!=0)vecT0=*C;
            else localT0=*localC;

            STARTDEBUG(propStep);
            if(ode!=0)ode->stepError(*C,Time, hTemp,errVec);
            else odeLocal->stepError(*localC,Time,hTemp,localE);
            STOPDEBUG(propStep);

            double maxNrm;
            bool accept;
            if(ode!=0)accept=ode->acceptStep(desiredPrecision,h,h,maxNrm=Threads::max(errVec.norm()));
            else accept=odeLocal->acceptStep(desiredPrecision,h,h,maxNrm=Threads::max(localE.norm()));

            if(accept){
                successfulTimeSteps+=1;
                Time+=hTemp;
		STARTDEBUG(propGather);
                if(abs(Time-TEnd)<abs(h*1.e-10))Time=TEnd;
                if(ode!=0)writeC=*C;
                else Parallel::allGather(&writeC,localC);
		STOPDEBUG(propGather);
                out->write(&writeC,Time);
                hSum+=h;
                hSquareSum+=h*h;
            } else {
                failedTimeSteps++;
                if(ode!=0)*C=vecT0;
                else *localC=localT0;
            }


            if(h<1e-9 and TEnd-Time>1.e-8){
                // several tiny steps - terminate
                steps.push_back(h);
                errs.push_back(maxNrm);
                if(steps.size()>5){
                    string mess;
                    mess="step size underflow:\nSteps"+tools::str(steps)
                            +"\nError"+tools::str(errs)+"\nTimes "+tools::str(Time)+" "+tools::str(TEnd);
                    ABORT(mess);
                }
            }
            else {
                steps.clear();
                errs.clear();
            }
        }
        STOPDEBUG(propAll);
    }
    timeCritical::suspend();
    if(ode==0)*C=writeC;
    out->timer->stopTimer();

    // force write at tEnd
    if(out->nextWriteTime()<TEnd+(1.-1.e12)*out->writeInterval){
        out->lastTimeWritten=TEnd-out->writeInterval;
        out->write(C,Time,true);
    }

    timeCritical::resume();
}

string TimePropagator::info() const {
    string s;
    if(ode!=0)s+=ode->name()+": ";
    else s+=odeLocal->name()+": ";
    s+=" accept/reject "+tools::str(successfulTimeSteps)+"/"+tools::str(failedTimeSteps);
    if(ode!=0)s+=", func.calls="+tools::str(ode->nApplyFunction());
    else s+=", func.calls="+tools::str(odeLocal->nApplyFunction());
    s+=", <h>="+tools::str(hSum/successfulTimeSteps,2);
    s+=", Delta h="+tools::str(sqrt(hSquareSum/successfulTimeSteps-pow(hSum/successfulTimeSteps,2)));
    return s;
}
void TimePropagator::print(double tBeg, double tEnd, double tPrint, double tStore, double accuracy, double cutE, double  FixStep,
                           double ApplyThreshold)
{
    if(tBeg==0. and tEnd==0.)return;

    PrintOutput::title("PROPAGATION PARAMETERS");
    PrintOutput::paragraph();
    PrintOutput::newRow();
    PrintOutput::rowItem("begin");
    PrintOutput::rowItem("end");
    PrintOutput::rowItem("print");
    PrintOutput::rowItem("store");
    PrintOutput::rowItem("accuracy");
    PrintOutput::rowItem("cutEnergy");
    if(FixStep>0.)PrintOutput::rowItem("fixStep");
    PrintOutput::newRow();
    PrintOutput::rowItem(tBeg);
    PrintOutput::rowItem(tEnd);
    PrintOutput::rowItem(tPrint);
    PrintOutput::rowItem(tStore);
    PrintOutput::rowItem(accuracy);
    PrintOutput::rowItem(cutE);
    if(FixStep>0.)PrintOutput::rowItem(FixStep);
    PrintOutput::paragraph();

    if(accuracy<1.e-11)PrintOutput::warning(Str("Very high accuracy of")+accuracy+"requested, recommended values >= 1e-11");
    if(accuracy>1.e-6) PrintOutput::warning(Str("Very low accuracy of")+accuracy+"requested, recommended values <= 1e-6");

    if(ApplyThreshold>0.){
        PrintOutput::lineItem("Threshold for operator application",ApplyThreshold);
        PrintOutput::paragraph();
        if(accuracy<ApplyThreshold*0.09)
            PrintOutput::warning("High threshold for operator application - recommended: threshold < 10*accuracy");
    }

}

void TimePropagator::read(ReadInput &Inp, double &tBeg, double &tEnd, double &tPrint, double &tStore, double &accuracy, double &cutE, double & FixStep,
                          double & ApplyThreshold,std::string &Method)
{

    double tPulseEnd=-DBL_MAX,tPulseBeg=DBL_MAX;
    if(Pulse::current.apotMax()!=0.){
        tPulseBeg=Pulse::gettBegin();
        tPulseEnd=Pulse::gettEnd();
        tBeg=tPulseBeg;
        tEnd=tPulseEnd;
    }

    Inp.read("TimePropagation","begin",tBeg,tools::str(tBeg,12),"begin time of propagation");
    Inp.read("TimePropagation","end",  tEnd  ,tools::str(tEnd,12),"end time of propagation");
    if(tBeg<-DBL_MAX*1.e-2 or (tEnd>DBL_MAX*1.e-2)){
        PrintOutput::warning(Str("very large time-integration range: [")+tBeg+","+tEnd+"]");
        if(tBeg<-DBL_MAX/2 or (tEnd>DBL_MAX/2))ABORT("near-infinite time-interval");
    }
    Inp.read("TimePropagation","print",tPrint,tools::str((tEnd-tBeg)/20,4),"printout intervals");
    Inp.read("TimePropagation","store",tStore,tools::str(tPrint,14),"minimal interval for file output (default=print,0=every valid step");
    Inp.read("TimePropagation","accuracy",accuracy,"1.e-8","accuracy control");
    Inp.read("TimePropagation","cutEnergy",cutE,tools::str(DBL_MAX),"remove energies above this");
    Inp.read("TimePropagation","fixStep",FixStep,"0.","fixed step size (0 - automatic control)");
    ApplyThreshold=-1.;
    Inp.read("TimePropagation","operatorThreshold",ApplyThreshold,"-1","consider block=0, if result remains below this treshold");
    //    Inp.obsolete("TimePropagation","operatorThreshold","do not use - temporarily not supported");
    Inp.read("TimePropagation","method",Method,"RK4","choices: RK4...classical RK, Butcher67...order 6 Butcher");

    if(Pulse::current.apotMax()!=0.){
        double propEps=0.00001*Units::convert(1.,"OptCyc");
        if(tPulseBeg<tBeg-propEps or tEnd+propEps<tPulseEnd)
            PrintOutput::warning("time propagation starts or ends during pulse: prop=["
                                 +tools::str(tBeg)+","+tools::str(tEnd)+"], pulse=["
                                 +tools::str(tPulseBeg)+","+tools::str(tPulseEnd)+"]");
    }

}

void TimePropagator::propagate(Wavefunction* Wf, double tEnd, const std::string Mode) {
    timeCritical::setOff();

    // initial wave function may have been set up only on master
    // VERY SUBTLE TRAP: if view of coefs has already been created, must not re-arrange storage
    if(Wf->coefs->storageData()==0)Wf->coefs->treeOrderStorage();
    MPIwrapper::Bcast(Wf->coefs->data(),Wf->coefs->size(),MPIwrapper::master());

    // propagation backwards in time
    if(Wf->time>tEnd){
        DEVABORT(Sstr+"backward propagation disabled, end:"+tEnd+"wf:"+Wf->time);
        //        backProp = true;
        //        Wf->time = -Wf->time;
        //        tEnd = -tEnd;
    }

    // if output has no interval, set it here
    if(out->tStart==DBL_MAX)out->tStart=Wf->time;
    if(out->tEnd==-DBL_MAX)out->tEnd=tEnd;

    // assign internal pointers and intermediate storage
    double nextTime=Wf->time,printStep=out->printInterval;
    if(printStep==0.)printStep=(tEnd-out->tStart)/10.;
    if(printStep<0)ABORT("cannot do backward time step: h="+str(printStep));
    if(h_fix!=0. and
            (remainder(tEnd-Wf->time,h_fix)>(tEnd-Wf->time)*1.e-10 or
             remainder(printStep,h_fix)>(tEnd-Wf->time)*1.e-10))PrintOutput::DEVwarning
            ("propagation time or print interval are not integer multiple of fixed time step - saved times will not be equidistant",1);

    if(Mode.find("Start")!=string::npos){
        out->startingCPU = clock();
        out->print(Wf->coefs,Wf->time,0.);
    }
    timeCritical::setOn();
    while(nextTime<tEnd) {
        nextTime=min(nextTime+printStep,tEnd);
        if(abs(nextTime-tEnd)<printStep*1.e-3)nextTime=tEnd;

        propagate_intern(Wf->coefs,Wf->time,nextTime);
        LOG_MEM("propagation");

        //print some simple output to screen and file
        out->print(Wf->coefs,nextTime,out->timer->secs());
    }
    timeCritical::setOff();

    if(Mode.find("Stop")!=string::npos){
        out->timer->monitorDone(Wf->time);

        //HACK
        out->coefsFFT();
        if(MPIwrapper::isMaster())
            out->close();
    }
    timeCritical::suspend();
}

string TimePropagator::str(unsigned int Brief) const{
    string s,b;
    s="tolerance=";
    b=tools::str(desiredPrecision,1);
    s+=b;
    if(Brief>0)return b;
    return s;
}
