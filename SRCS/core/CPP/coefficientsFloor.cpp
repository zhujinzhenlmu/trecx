// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "coefficientsFloor.h"

// resolve forward declarations
//#include "discretization.h"
#include "coefficients.h"
#include "index.h"

#include "qtEigenDense.h"

using namespace std;
#include "eigenNames.h"
#include "timer.h"

//CoefficientsFloor::CoefficientsFloor(const Index *Idx, complex<double>*CData){
//}

//CoefficientsFloor::CoefficientsFloor(const CoefficientsFloor *Floor, complex<double>*CData){
//}
