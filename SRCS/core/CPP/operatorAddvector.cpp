// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "operatorAddvector.h"
#include <typeinfo>

//resolve forward declarations
#include "discretization.h"
#include "qtEigenDense.h"
#include <Core>
#include "coefficients.h"
#include "coefficientsFloor.h"
#include "index.h"
#include "parameters.h"
#include "index.h"
#include "useMatrix.h"
#include "timer.h"
#include "operatorData.h"

using namespace std;
using namespace tools;
#include "eigenNames.h"


map<string,OperatorAddvector::UpdateC> OperatorAddvector::updateTable;

/// construct from vector of tensor factorsconst
OperatorAddvector::OperatorAddvector(const string & Definition, const Discretization *IDisc, const Discretization *JDisc,
                                     const Index *IIndex, const Index *JIndex,
                                     complex<double> Multiplier) :
    OperatorSingle("no name",Definition,IIndex,JIndex){

    if(IIndex!=JIndex)ABORT("OperatorAddvector can only be constructed within the same floor");

    // factor is 1.0 by default
    mats.clear();
//CHANGE    factor=Parameters::pointer("1");
    vector<UseMatrix> matrices;
    IDisc->operatorData(Definition,IIndex,JIndex,matrices);

    // check for zero matrices, if non-zero add to the matrix-repository
    string hash=Definition;
//    hash+=tools::str(IIndex)+tools::str(JIndex)+tools::str(Multiplier);
    hash+=IIndex->hash()+JIndex->hash()+tools::str(Multiplier);
    for (unsigned int k=0;k<matrices.size();k++){
        if(k==0)matrices[0]*=Multiplier;
        if(matrices[k].isZero(1e-14)){
            mats.resize(0);
            break;
        }
        const UseMatrix* temp = OperatorSingle::matsAdd(matrices[k],hash+tools::str(k));
        mats.push_back(temp);
    }
    if(mats.size()>0)fillC();

    // set update function, if any
    setUpdate(Definition);
}

void OperatorAddvector::addUpdate(UpdateC UpdateFunction, string Name){
    if(tools::hasKey(updateTable,Name) and updateTable[Name]!=UpdateFunction)
        ABORT("multiple ambigous definition of updateFunction "+Name);
    if(Name.find("[t]")!=Name.length()-3)ABORT("updateFunction name must end in \"[t]\", is: "+Name);
//    cout<<" update function "<<Name<<" "<<UpdateFunction<<" "<<&UpdateFunction<<endl;
//    ABORT("bad");
    updateTable[Name]=UpdateFunction;
}

bool OperatorAddvector::isZero(double Eps) const {
    for(unsigned int k=0;k<C.size();k++)if(abs(C[k])>Eps)return false;
    return false;
}

void OperatorAddvector::setUpdate(string Definition){
    updateFunction=0;
    if(Definition.find("[t]")==string::npos)return; // no time-dependence indicated
    if(OperatorData::terms(Definition).size()>1)ABORT("cannot have sum of time-dependent terms: "+Definition);
    string term=tools::stringInBetween(Definition,"|",">");
    if(term.find("[t]")==string::npos)return;
    if(not tools::hasKey(updateTable,term))
        ABORT("cannot find "+term+" in updateTable\nuse OperatorAddvector::addUpdate(...) to add\nAvailable:\n"+tools::listMapKeys(updateTable,"\n"));
    updateFunction=updateTable[term];
    Parameters::updatables.push_back(this);
}

void OperatorAddvector::fillC(){
    // fill the coefficients
    if(mats.size()==1){
        for (unsigned int i=0;i<mats[0]->rows();i++)C.push_back(mats[0]->operator()(i,i).complex());
    }
    else if (mats.size()==2){
        for (unsigned int i=0;i<mats[0]->rows();i++)
            for (unsigned int j=0;j<mats[1]->rows();j++)
                C.push_back(mats[0]->operator()(i,i).complex()*mats[1]->operator()(j,j).complex());
    }
    else ABORT("OperatorAddvector only implemented up to 2 dimensions");
}

/// Y<- Beta*Y + factor*C
void OperatorAddvector::apply(CoefficientsFloor *X, CoefficientsFloor *Y, std::complex<double> Alfa, std::complex<double> Beta) const{
    axpy(Alfa,*X,Beta,*Y);
}

/// Y <- Y + factor*C
void OperatorAddvector::axpy(std::complex<double> Alfa, CoefficientsFloor & X, std::complex<double> Beta, CoefficientsFloor & Y, bool transpose) const {
    if(transpose)ABORT("cannot transpose OperatorAddvector");
    /// y<- y + C
    DEVABORT("OperatorAddvector no longer supported");
    complex<double>alFac=Alfa;
//    if(Beta!=1.)
//        for(unsigned int i=0;i<C.size();i++)Y.xData()[i]=alFac*C[i]+Beta*Y.xData()[i];
//    else
//        for(unsigned int i=0;i<C.size();i++)Y.xData()[i]+=alFac*C[i];
}

/// try merge Other into present, return true if successful
bool OperatorAddvector::absorb(OperatorSingle *&Other){
    // only operators with equal indices and equal tensor can be absorbed
    if(iIndex!=Other->iIndex)return false;
    if(jIndex!=Other->jIndex)return false;
//CHANGE    if(factor!=Other->factor)return false;
    if(typeid(*Other)!=typeid(*this))return false; // can only absorb equal types

    if(mats.size()!=Other->mats.size())return false;
    if(mats.size()>1)return false;

    if(name.find("(fused)")!=string::npos)name+="(fused)";
    definition+=Other->definition;

    UseMatrix* mat=new UseMatrix(mats[0]->rows(),mats[0]->cols());
    *mat=*(mats[0])+*(Other->mats[0]);
    mats[0]=mat;

    // create new C
    C.clear();
    fillC();

    return true;
}

string OperatorAddvector::str() const {return name+": "+tools::str(mats[0]);}
