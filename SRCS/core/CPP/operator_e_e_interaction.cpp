// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "operator_e_e_interaction.h"
#include "operator.h"
#include "basisSet.h"
#include "gaunt.h"
#include "coefficientsFloor.h"
#include "index.h"
#include "qtAlglib.h"
#include "operatorFloorEE.h"
#include "basisMat.h"

using namespace std;
#include "eigenNames.h"


std::vector<std::vector<std::vector<Eigen::MatrixXcd > > > Operator_e_e_interaction::Helpers_e_e::TwoElecOverlap(0);

Operator_e_e_interaction::BasicDisc_remove1particle::BasicDisc_remove1particle(ReadInput &In, int remove)
{
    In.obsolete("Integration","DVR","DVR is enforced - cannot choose externally");
//    In.read("Integration","DVR",BasisMat::femDVR,"true","use FEM-DVR method or not",0);
    setAxis(In);

    lmax = 0;

    vector<Axis> ax;
    std::vector<std::string> hier;

    // Remove particle defined by remove
    for(unsigned int i=0;i<axis.size();i++){
        if(axis[i].name.find("Eta")!=std::string::npos){
            if(axis[i].basDef.size()!=1) ABORT("Size not 1");
            lmax = max(lmax,(int)(axis[i].basDef[0].order-1));
        }
        if(axis[i].name.substr(axis[i].name.size()-1,axis[i].name.size())==tools::str(remove)
                or axis[i].name.find("Eta")!=std::string::npos or axis[i].name.find("Phi")!=std::string::npos){
        }
        else{
            ax.push_back(axis[i]);
            hier.push_back(hierarchy[i]);
        }
    }
    axis = ax;
    hierarchy = hier;
    if(axis.size()!=1) ABORT("Axis size is not 1: "+tools::str(axis.size()));
    continuityLevel.clear();
    continuityLevel.push_back(0);

    if(axis.size()>0)construct();
}

void Operator_e_e_interaction::Helpers_e_e::lobatto_quadrature(BasisSet *B, VectorXd &quadraturePoints, VectorXd &weights)
{
    int N= B->order();
    double lb = B->lowBound();
    double ub = B->upBound();

    quadraturePoints = VectorXd::Zero(N);
    weights = VectorXd::Zero(N);
    alglib::real_1d_array alpha;
    alglib::real_1d_array beta;
    alglib::real_1d_array x1;
    alglib::real_1d_array w1;
    alpha.setlength(N-1);
    beta.setlength(N-1);
    x1.setlength(N);
    w1.setlength(N);
    alglib::ae_int_t info;

    for(int i=0;i<N-1;i++)
        alpha(i)=0;

    for(int i=0;i<N-1;i++)
        beta(i)=double(i*i)/double(4*i*i-1);

    alglib::gqgenerategausslobattorec(alpha, beta, (double)2.0, (double)(-1.0) , (double)1, N, info, x1, w1);

    if (info!=1)
        cout<<"Error in Gauss-Lobatto quadrature. Error code "<<info<<endl;

    double temp = (ub-lb)/2.0;
    for(int i=0;i<N;i++){
        quadraturePoints(i)=lb+(x1[i]+1.0)*temp;
        weights(i)=w1[i]*temp;
    }
}

void Operator_e_e_interaction::Helpers_e_e::initialize()
{
    if(TwoElecOverlap.size()!=0)  return;      // Already initialized, should only be done once

    BasicDisc_remove1particle *D = new BasicDisc_remove1particle(ReadInput::main);

    int lmax =D->lmax;
    Gaunt::main.initialize(2*lmax+1);          // Initialize gaunt

    vector<UseMatrix > Kin_mat,Q2_mat;
    vector<vector<bool> > WgtIncludedInFunction;
    vector<VectorXd> points,weig;

    int ne = -1;  // Number of unscaled elements set below
    double r_max=-1;

    for(int n=0;n<D->getAxis()[0].basDef.size();n++){

        const BasisSetDef* Bdef = &(D->getAxis()[0].basDef[n]);
        BasisSet *B = BasisSet::get(*Bdef);

        // if complex scaled or not polynomial basis stop
        if(abs(real(Bdef->comSca.etaX((B->lowBound()+B->upBound())*0.5))-1.0)>1e-12
                or abs(imag(Bdef->comSca.etaX((B->lowBound()+B->upBound())*0.5)))>1e-12
                or B->str().find("polynomial")==string::npos){
            //eta is not 1 any more complex scaling starts
            ne = n;
            r_max=B->lowBound();
            break;
        }

//        cout<<B->str()<<endl;

        UseMatrix kin,Q2,pts_mat,wgs_mat;
        BasisMat::get(vector<string>(1,"d_1_d"),vector<const BasisSet*>(1,B),vector<const BasisSet*>(1,B),kin,vector<string>(1,""));
        BasisMat::get(vector<string>(1,"1/Q"),vector<const BasisSet*>(1,B),vector<const BasisSet*>(1,B),Q2,vector<string>(1,""));
        for(int i=0;i<Q2.rows();i++) Q2(i,i) = std::pow(Q2(i,i).real(),2);
        BasisMat::get(vector<string>(1,"1"),vector<const BasisSet*>(1,B),vector<const BasisSet*>(1,B),wgs_mat,vector<string>(1,""));
        BasisMat::get(vector<string>(1,"Q"),vector<const BasisSet*>(1,B),vector<const BasisSet*>(1,B),pts_mat,vector<string>(1,""));

        vector<bool> wgs_inc;  // Does the function include weight
        for(int i=0;i<wgs_mat.rows();i++){
            if(abs(wgs_mat(i,i).real()-1.0)<1e-14) wgs_inc.push_back(true);
            else wgs_inc.push_back(false);
        }

        // Cannot get lobatto points from quad rule??? So using own routine
        VectorXd pts,wgs_temp;
        lobatto_quadrature(B,pts,wgs_temp);
        if(n==0){
            VectorXd temp = pts.segment(1,pts.size()-1);
            pts = temp;
            temp = wgs_temp.segment(1,wgs_temp.size()-1);
            wgs_temp = temp;
        }

        //      kin.print();
        //      Q2.print();
        //      pts_mat.print();
        //      wgs_mat.print();

        // For som reason the points are not in ascending order, so rearrange according to pts_mat
        VectorXd wgs = VectorXd::Zero(wgs_temp.size());
        for(int i=0;i<pts_mat.rows();i++){
            int index=-1;
            for(int j=0;j<pts_mat.rows();j++){
                if(abs(pts(j)-pts_mat(i,i).real())<1e-9) {index=j; break;}
                if((i==0 or i==pts_mat.rows()-1) and abs(pts(j)*wgs_temp(i)-pts_mat(i,i).real())<1e-9) {index=j; break;}
            }
            if(index==-1) ABORT("Could not find the point. Why??? "+tools::str(pts_mat(i,i).real()));
            wgs[i] = wgs_temp[index];
        }
        for(int i=0;i<pts_mat.rows();i++) pts(i) = pts_mat(i,i).real();

        points.push_back(pts);
        weig.push_back(wgs);

        //      cout<<points.back().transpose()<<endl<<weig.back().transpose()<<endl;
        //      cin.get();

        Kin_mat.push_back(kin);
        Q2_mat.push_back(Q2);
        WgtIncludedInFunction.push_back(wgs_inc);
    }

    // Set size
    TwoElecOverlap.resize(ne);
    for(int n=0;n<ne;n++){
        TwoElecOverlap[n].resize(ne);
        for(int n2=0;n2<ne;n2++){
            TwoElecOverlap[n][n2].resize(2*lmax+1);
        }
    }

    //  bool smoothlyTruncateEEintetaction = false;
    //  if(MPIwrapper::getMPI_ID()==0) cout << "smoothlyTruncateEEintetaction: " << smoothlyTruncateEEintetaction << endl;
    //  ScaledLegendre helperForSmoothingMask(5,0,1); // order and so on are never used for the smoothing stuff

    //Setting up the overall T_Vee matrix
    for(int lambda=0;lambda<=2*lmax;lambda++){

        int size=0;
        for(int n=0;n<ne;n++)
            size+=Q2_mat[n].rows();
        size=size-ne+1;
        MatrixXcd T_Vee = MatrixXcd::Zero(size,size);

        int start=0;
        for(int n=0;n<ne;n++){
            MatrixXcd tp = Map<MatrixXcd>(Kin_mat[n].data(),Kin_mat[n].rows(),Kin_mat[n].cols())
                    +(double)lambda*((double)lambda+1)*Map<MatrixXcd>(Q2_mat[n].data(),Q2_mat[n].rows(),Q2_mat[n].cols());

            T_Vee.block(start,start,tp.rows(),tp.cols())+=tp;
            start=start+tp.rows()-1;
        }

        MatrixXcd temp = T_Vee.block(0,0,T_Vee.rows()-1,T_Vee.cols()-1);   //Removing the last function; boundary conditions
        T_Vee = MatrixXcd::Zero(T_Vee.rows(), T_Vee.cols());
        T_Vee.block(0,0,T_Vee.rows()-1,T_Vee.cols()-1) = temp.inverse();

        //distrubute it back to each of the voxels
        int start1 =0;
        for(int n1=0;n1<ne;n1++){
            int start2=0;
            for(int n2=0;n2<ne;n2++){
                MatrixXcd T_Vee_all_parts = T_Vee.block(start1,start2,Q2_mat[n1].rows(),Q2_mat[n2].cols());
                TwoElecOverlap[n1][n2][lambda] = VectorXcd::Zero(T_Vee_all_parts.rows()*T_Vee_all_parts.cols());
                MatrixXcd temp2e = MatrixXcd::Zero(T_Vee_all_parts.rows(),T_Vee_all_parts.cols());

                //              VectorXcd points1(points[n1].size());
                //              VectorXcd points2(points[n2].size());
                //              for(int i1=0;i1<points1.size();i1++)
                //                points1(i1) = points[n1](i1);
                //              for(int i2=0;i2<points2.size();i2++)
                //                points2(i2) = points[n2](i2);
                //              VectorXcd smoothMask1(points1.size());
                //              VectorXcd smoothMask2(points2.size());
                //              VectorXcd smoothMaskDeriv1(points1.size());
                //              VectorXcd smoothMaskDeriv2(points2.size());
                //              helperForSmoothingMask.getSmoothingMask(points1,smoothMask1,smoothMaskDeriv1);
                //              helperForSmoothingMask.getSmoothingMask(points2,smoothMask2,smoothMaskDeriv2);

                //              cout<<n1<<" "<<n2<<endl;
                //              cout<<T_Vee_all_parts.rows()<<" "<<T_Vee_all_parts.cols()<<" "<<points[n1].size()<<" "<<points[n2].size()<<endl;
                for(int k1=0;k1<T_Vee_all_parts.rows();k1++){
                    for(int k2=0;k2<T_Vee_all_parts.cols();k2++){
                        double r_j = points[n2](k2);
                        double r_i = points[n1](k1);
                        double w_j = weig[n2](k2);
                        double w_i = weig[n1](k1);

                        //                      if(n1==0 and k1==0) continue;
                        //                      if(n2==0 and k2==0) continue;
                        if(n1==ne-1 and k1==T_Vee_all_parts.rows()-1) continue;
                        if(n2==ne-1 and k2==T_Vee_all_parts.cols()-1) continue;

                        if(WgtIncludedInFunction[n1][k1] and WgtIncludedInFunction[n2][k2]){
                            temp2e(k1,k2) = (pow(r_j*r_i,lambda)/pow(r_max,2*lambda+1))
                                    + (2*(double)lambda+1)*T_Vee_all_parts(k1,k2)/(r_j*r_i*sqrt(w_i*w_j));
                        }
                        else if(WgtIncludedInFunction[n1][k1] and not WgtIncludedInFunction[n2][k2]){
                            temp2e(k1,k2) = ((pow(r_j*r_i,lambda)/pow(r_max,2*lambda+1))
                                             + (2*(double)lambda+1)*T_Vee_all_parts(k1,k2)/(r_j*r_i*sqrt(w_i))) *w_j;

                        }
                        else if(not WgtIncludedInFunction[n1][k1] and WgtIncludedInFunction[n2][k2]){
                            temp2e(k1,k2) = (pow(r_j*r_i,lambda)/pow(r_max,2*lambda+1))
                                    + (2*(double)lambda+1)*T_Vee_all_parts(k1,k2)/(r_j*r_i*sqrt(w_j)) *w_i;

                        }
                        else{
                            temp2e(k1,k2) = ( pow(r_j*r_i,lambda)/pow(r_max,2*lambda+1)
                                              + (2*(double)lambda+1)*T_Vee_all_parts(k1,k2)/(r_j*r_i)) * w_i*w_j;
                        }

                        //                      //smoothly turn it off. CHECK IF THIS IS THE CORRECT WAY TO DO THAT
                        //                      if(smoothlyTruncateEEintetaction){
                        //                          temp2e(k1,k2) *= smoothMask1(k1)*smoothMask2(k2);
                        //                        }
                        TwoElecOverlap[n1][n2][lambda](k1*T_Vee_all_parts.cols()+k2) = temp2e(k1,k2);
                    }
                }
                start2 = start2+Q2_mat[n2].cols()-1;
            }
            start1 = start1+Q2_mat[n1].rows()-1;
        }
    }
}

Operator_e_e_interaction::Operator_e_e_interaction(const std::string Name, const std::string Def, const Index *IIndex, const Index *JIndex):
    OperatorSingle(Name,Def,IIndex,JIndex),epsGaunt(1e-15),oFloor(0)
{
    // Find the indices n1,l1,m1,n2,l2,m2
    construct_index(IIndex,Idx);
    construct_index(JIndex,Jdx);

    Helpers_e_e::initialize();
    lambda_upper_limit = Helpers_e_e::TwoElecOverlap[0][0].size()-1;
    if(not isZero(1.e-12))oFloor=new OperatorFloorEE(Name,Def,IIndex,JIndex);
}

bool Operator_e_e_interaction::isZero(double Eps) const
{
    if(Idx[0]!=Jdx[0] or Idx[1]!=Jdx[1]) return true;
    int unscaledElems = Helpers_e_e::TwoElecOverlap.size();
    if(Idx[0]>=unscaledElems or Idx[1]>=unscaledElems or Jdx[0]>=unscaledElems or Jdx[1]>=unscaledElems) return true; // 0 in complex scaled region
    //  cout<<"Checking is Zero \n";

    for(int lambda=0;lambda<=lambda_upper_limit;lambda++){
        if(Jdx[4]-Idx[4]!=Idx[5]-Jdx[5]) continue;
        int M = Jdx[4]-Idx[4];
        if(Gaunt::main.coeff_isZero(Jdx[2],lambda,Idx[2],Jdx[4],M,Idx[4]) or Gaunt::main.coeff_isZero(Idx[3],lambda,Jdx[3],Idx[5],M,Jdx[5])) continue;

        double gf1,gf2;    // gaunt coefficients
        gf1 = Gaunt::main.coeff(Jdx[2],lambda,Idx[2],Jdx[4],M,Idx[4]);
        if(abs(gf1) < epsGaunt) continue;
        gf2 = Gaunt::main.coeff(Idx[3],lambda,Jdx[3],Idx[5],M,Jdx[5]);

        double gaunt_factor=gf1*gf2*(4.0*math::pi/(2.0*double(lambda)+1.0));
        if((abs(gaunt_factor)<epsGaunt)==false) return false;
    }
    return true;
}

void Operator_e_e_interaction::apply(std::vector<std::complex<double> > &InOut) const
{
    ABORT("Operator_e_e_interaction::apply: not implemented yet");
}

void Operator_e_e_interaction::inverse()
{
    ABORT("Operator_e_e_interaction::inverse() should never be called");
}

void Operator_e_e_interaction::construct_index(const Index *I, std::vector<int> &Idx)
{// order n1,n2,m1,m2,l1,l2

    if(not I->hasFloor()) ABORT("Not floor ?? ");

    Idx.resize(6);
    for(const Index* s=I; ; s=s->parent()){
        if(s->parent()==0) break;

        if(s->parent()->axisName()=="Rn1") Idx[0]=s->nSibling();
        else if(s->parent()->axisName()=="Rn2") Idx[1]=s->nSibling();
        else if(s->parent()->axisName()=="Eta1") Idx[2]=s->nSibling()+s->parent()->basisSet()->param(0);
        else if(s->parent()->axisName()=="Eta2") Idx[3]=s->nSibling()+s->parent()->basisSet()->param(0);
        else if(s->parent()->axisName()=="Phi1") Idx[4]=s->parent()->basisSet()->parameters()[s->nSibling()];
        else if(s->parent()->axisName()=="Phi2") Idx[5]=s->parent()->basisSet()->parameters()[s->nSibling()];
        else ABORT("Unknown axis name: "+s->parent()->axisName());
    }
}

Operator_e_e_interaction::~Operator_e_e_interaction(){
}
