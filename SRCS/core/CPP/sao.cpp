// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "sao.h"

using namespace std;
#include "eigenNames.h"


sao::sao(columbus_data* col_data): A(col_data){
    if(not oldCode())ABORT("not old");
    col_data->read_sao_coefficients(coef,A.coef.rows());  //Returns a matrix with size:  num. of symmetry Atomic orbitals x Atomic Orbitals

}
sao::sao(QuantumChemicalInput & ChemInp): A(ChemInp){
    coef=ChemInp.saoCoef;
}

int sao::number_saos(){

    return coef.rows();

}

void sao::sao_matrix_1e(MatrixXcd &result, string op, sao *b){

    MatrixXcd res;
    if(b==NULL){
        A.ao_matrix_1e(res,op);
       result = coef*res*coef.transpose();
    }
    else{
        A.ao_matrix_1e(res,op,&b->A);
        result = coef*res*b->coef.transpose();
    }

}

void sao::values(MatrixXd& val, Matrix<double, Dynamic, 3>& xyz){

    A.values(val,xyz);
    val = coef*val;

}

void sao::values(VectorXd& val, Matrix<double, Dynamic, 3>& xyz, int fun_index){

    MatrixXd v;
    A.values(v,xyz);
    val = (coef*v).row(fun_index);

}

void sao::derivatives(MatrixXd& val, Matrix<double, Dynamic, 3>& xyz, string wrt){

    MatrixXd v;
    A.derivatives(v,xyz,wrt);
    val = coef*v;

}

void sao::derivatives(VectorXd& val, Matrix<double, Dynamic, 3>& xyz, int fun_index, string wrt){

    MatrixXd v;
    A.derivatives(v,xyz,wrt);
    val = (coef*v).row(fun_index);

}
