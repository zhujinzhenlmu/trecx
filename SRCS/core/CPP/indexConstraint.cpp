#include "indexConstraint.h"
#include "readInput.h"
#include "index.h"
#include "tools.h"
#include "basisSub.h"

#include <map>

IndexConstraint IndexConstraint::main;

std::string IndexConstraint::str() const {
    std::string s;
    for(int k=0;k<_constraints.size();k++)s+=_constraints[k]->str()+" ";
    return s;
}

void IndexConstraint::readAll(ReadInput &Inp, std::string &Kind, std::string &Axes, int Line){
    Inp.read("BasisConstraint", "kind", Kind, "None",
             "Kinds: None...(default), L-M[c1;c2]...restrict to l-m<c1 except l<c2, M=0...total M, Lshape[c1;c2]...exclude (l1>=c1 && l2>=c1) except l1+l2<c2, MLmax[m1max]", Line);
    Inp.read("BasisConstraint", "axes", Axes, "BLANK", "Axes used in constraint, ordering is important", Line);
}

void IndexConstraint::read(ReadInput& Inp){

    Inp.obsolete("IndexConstraint","name","use BasisContraint: kind instead");
    Inp.obsolete("IndexConstraint","axes","use BasisContraint: axes instead");

    _constraints.clear();
    for(int line=1;;line++){
        std::string kind, axes;
        readAll(Inp,kind,axes,line);
        if(kind=="None") break;

        if(kind.find("L-M") != std::string::npos){
            int threshold, excluded_l;
            std::string pars = tools::splitString(kind, '[')[1];
            pars = tools::splitString(pars,']')[0];

            if(pars.find(";") == std::string::npos){
                threshold = tools::string_to_int(pars);
                excluded_l = 1.66*threshold;
            }else{
                threshold = tools::string_to_int(tools::splitString(pars, ';')[0]);
                excluded_l = tools::string_to_int(tools::splitString(pars, ';')[1]);
            }

            if(axes == "BLANK"){
                _constraints.push_back(std::unique_ptr<Constraint>(new LmMConstraint("Eta", "Phi", threshold, excluded_l)));
                _constraints.push_back(std::unique_ptr<Constraint>(new LmMConstraint("Eta1", "Phi1", threshold, excluded_l)));
                _constraints.push_back(std::unique_ptr<Constraint>(new LmMConstraint("Eta2", "Phi2", threshold, excluded_l)));
            }else{
                std::vector<std::string> ax = tools::splitString(axes, '.');
                if(ax.size() != 2) ABORT("Specify exactly two axes, like: Eta.Phi1");
                _constraints.push_back(std::unique_ptr<Constraint>(new LmMConstraint(ax[0], ax[1], threshold, excluded_l)));
            }

        }else if(kind.find("M=0") != std::string::npos){
            if(axes == "BLANK"){
                _constraints.push_back(std::unique_ptr<Constraint>(new MZeroConstraint("Phi1", "Phi2")));
            }else{
                std::vector<std::string> ax = tools::splitString(axes, '.');
                if(ax.size() != 2) ABORT("Specify exactly two axes, like: Phi1.Phi2");
                _constraints.push_back(std::unique_ptr<Constraint>(new MZeroConstraint(ax[0], ax[1])));
            }

        }else if(kind.find("Lshape") != std::string::npos){
            int threshold, sumL;
            std::string pars = tools::splitString(kind, '[')[1];
            pars = tools::splitString(pars,']')[0];
            if(pars.find(";") == std::string::npos){
                threshold = tools::string_to_int(pars);
                sumL = 3*threshold;
            }else{
                threshold = tools::string_to_int(tools::splitString(pars, ';')[0]);
                sumL = tools::string_to_int(tools::splitString(pars, ';')[1]);
            }

            if(axes == "BLANK"){
                _constraints.push_back(std::unique_ptr<Constraint>(new LShapeConstraint("Eta1", "Eta2", threshold, sumL)));
            }else{
                std::vector<std::string> ax = tools::splitString(axes, '.');
                if(ax.size() != 2) ABORT("Specify exactly two axes, like: Eta1.Eta2");
                _constraints.push_back(std::unique_ptr<Constraint>(new LShapeConstraint(ax[0], ax[1], threshold, sumL)));
            }

        }else if (kind.find("MLmax") != std::string::npos) {
            int MLmax;
            std::string pars = tools::splitString(kind, '[')[1];
            pars = tools::splitString(pars, ']')[0];
            MLmax = tools::string_to_int(pars);

            if (axes == "BLANK") {
                _constraints.push_back(std::unique_ptr<Constraint>(new LM1Constraint("Phi1", "Eta1", MLmax)));
            } else {
                std::vector<std::string> ax = tools::splitString(axes, '.');
                if (ax.size() != 2) ABORT("Specify exactly two axes, like: Eta1.Eta2");
                _constraints.push_back(std::unique_ptr<Constraint>(new LM1Constraint(ax[0], ax[1], MLmax)));
            }

        }
        else ABORT("Unknown constraint: "+kind);
    }
}

bool IndexConstraint::includes(std::vector<const Index*> Path, std::vector<unsigned int> Pos) const{
    //not very beautiful...
    std::map<std::string, double> physicals;
    for(int i=0; i<Pos.size(); i++){
        const BasisAbstract* bas = Path[i]->basisAbstract();
        if(bas!=0){
            std::string ax=Path[i]->axisName();

            // set correct physical value where needed
            physicals[ax]=DBL_MAX;
            for(auto& c: _constraints)
                if(std::find(c->required_physicals.begin(),c->required_physicals.end(),ax)!=c->required_physicals.end()){
                    physicals[ax] = bas->physical(Pos[i]);
                    break;
                }
        }
    }

    for(auto& c: _constraints){
        for(auto v: c->required_physicals){
            if(physicals.find(v) == physicals.end()){
                goto Done;
            }
        }
        if(!c->includes(physicals)) return false;
Done:;
    }

    return true;
}



void IndexConstraint::apply(Index *Idx, std::vector<const Index*> Path, std::vector<unsigned int> Pos) const {
    if(_constraints.size()==0)return; // actually no constraints
    Path.push_back(0);
    Pos.push_back(-1);
    std::vector<int> subset;
    for(int k=Idx->childSize()-1;k>=0;k--){
        Path.back()=Idx->child(k);
        Pos.back()=k;
        apply(Idx->child(k),Path,Pos);
        if(Idx->child(k)->sizeCompute()==0 or not includes(Path,Pos))Idx->childErase(k);
        else                                                         subset.insert(subset.begin(),k);
    }
    Path.pop_back();
    Pos.pop_back();
    if(subset.size()!=Idx->childSize()){
        Idx->setBasis(BasisAbstract::factory(BasisSub::strDefinition(Idx->basisAbstract(),subset)));
    }
}

