// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "discretizationSpectral.h"

#include <vector>
#include <complex>


#include "discretizationDerived.h"
#include "index.h"
#include "operatorDiagonal.h"
#include "parallel.h"
#include "operatorFloor.h"

#include "printOutput.h"
#include "readInput.h"
#include "coefficients.h"
#include "operatorAbstract.h"
#include "operatorTree.h"
#include "operatorDefinition.h"
#include "eigenSolver.h"
#include "discretizationHaCC.h"
#include "inverse.h"
#include "tRecXchecks.h"
#include "indexProd.h"
#include "basisSub.h"

#include "parallelOperator.h"

#include "plot.h"
#include "projectSubspace.h"
#include "eigenTools.h"

using namespace std;

DiscretizationSpectral::~DiscretizationSpectral(){
    delete spectralValues;
    spectralValues=0;
    delete idx();
    idx()=0;
}

DiscretizationSpectral::DiscretizationSpectral(const Discretization *D, std::string Criterion)
    :_selectionCriterion(Criterion), spectralValues(0){parent=D;}




DiscretizationSpectral::DiscretizationSpectral(const Discretization *D, const OperatorAbstract *Op,
                                               double Emin, double Emax, bool excludeEnergyRange)
    :DiscretizationSpectral(Op,Emin,Emax,excludeEnergyRange)
{
    parent=D;
    name=D->name+"_"+Op->name+_selectionCriterion;
    name+="_spectral";
}

DiscretizationSpectral::DiscretizationSpectral(const OperatorAbstract *Op,
                                               double Emin, double Emax, bool excludeEnergyRange)
    :DiscretizationSpectral(0,"["+tools::str(Emin,3,DBL_MAX/2.)+","+tools::str(Emax,3,DBL_MAX/2.)+"]")
{
    _selectionCriterion="["+tools::str(Emin,3,DBL_MAX/2.)+","+tools::str(Emax,3,DBL_MAX/2.)+"]";
    name=Op->name+"_"+Op->iIndex->hierarchy()+_selectionCriterion+"_spectral";
    idx()=0;

    // solve eigenproblem
    EigenSolver slv(Emin,Emax,true,true,excludeEnergyRange,"Lapack");
    slv.parallel(true);
    slv.compute(Op);

    if(slv.eigenvalues().size()==0)return;

    slv.rightVectors();
    slv.normalize();

    for(auto v: slv.rightVectors())eigenVectors.push_back(std::shared_ptr<Coefficients>(v));

    for(auto v: slv.eigenvalues())eigenValues.push_back(v);

    if(not slv.verify()){
        UseMatrix mat,ovr;
        Op->matrix(mat);
        mat.print("Op",0);

        Op->iIndex->overlap()->matrix(ovr);
        ovr.print("ovr",0);
        ABORT("Eigensolving failed for "+name);
    }

    _project.reset(new ProjectSubspace(slv.rightVectors(),slv.dualVectors()));

    // create index and maps
    OperatorTree * mapFrom;
    OperatorTree * mapTo;
    mapFrom=const_cast<OperatorTree*>(_project->mapFrom());
    mapTo=  const_cast<OperatorTree*>(_project->mapTo());
    idx()=const_cast<Index*>(_project->subspaceIndex());
    spectralValues=new OperatorDiagonal("eval("+Op->name+")",idx());
    // ProjectSubspace internally sorts the eigenvectors - adjust
    std::vector<std::complex<double> > eSort;
    for(int k: _project->sorting())eSort.push_back(slv.eigenvalues()[k]);
    spectralValues->add(eSort,idx());

    //HACK: Prevent hierarchies Phi1.Phi1.Eta1.Eigen
    // TensorOperatorTreeWithId seems to be able to deal with it, UNLESS constraint M=0 is imposed
    if(idx()->childSize() == 1 && idx()->child(0)->axisName() == idx()->axisName()){
        if(mapFrom->childSize() != 1) ABORT("Unexpected");
        if(mapTo->childSize() != 1) ABORT("Unexpected");
        if(mapFrom->child(0)->iIndex != idx()->child(0)) ABORT("Unexpected");
        if(mapTo->child(0)->jIndex != idx()->child(0)) ABORT("Unexpected");

        idx() = idx()->child(0);
        mapFrom = mapFrom->child(0);
        mapTo = mapTo->child(0);

        spectralValues = spectralValues->child(0);

        // TODO: Leaks
        idx()->parentRef() = 0;
        mapFrom->parentRef() = 0;
        mapTo->parentRef() = 0;
        spectralValues->parentRef() = 0;
    }

    _mapFromParent.reset(mapFrom);
    _mapToParent.reset(mapTo);

    check(Op);

    Parallel::setSort(this);
}

int DiscretizationSpectral::selectNmax(const Index *Idx) const{
    if(_selectionCriterion=="" or _selectionCriterion[0]=='[')return INT_MAX;

    if(_selectionCriterion.find("Rn<=")!=string::npos){
        // special case for smooth extrapolation
        if(Idx->axisName()!="Rn")ABORT("axis does not match slection "+Idx->axisName());
        double rmax=tools::string_to_double(_selectionCriterion.substr(_selectionCriterion.find("Rn<=")+4));
        int nmax=0;
        for(int k=0;k<Idx->childSize() and Idx->child(k)->basisSet()->upBound()*1.0000001<rmax;k++)
            nmax+=Idx->child(k)->basisSet()->size();
        return nmax;
    }
    else ABORT("undefined eigenvalue selection: "+_selectionCriterion);
}

OperatorTree * DiscretizationSpectral::_mapConstructor(bool ToSpectral, const Discretization* Disc, const Index *SIndex, const Index *EIndex,
                                                       const std::vector<Coefficients*> & Evec)
{
    OperatorTree * mapT;
    if(ToSpectral)mapT=new OperatorTree(Disc->name+" <-- "+Disc->parent->name,SIndex,EIndex);
    else          mapT=new OperatorTree(Disc->parent->name+" <-- "+Disc->name,EIndex,SIndex);

    if(Evec.size()==0)return mapT;

    if(EIndex!=Evec[0]->idx())ABORT("EIndex must match Evec.idx(), is\n"+EIndex->str()+"\n"+Evec[0]->idx()->str());

    // descend to coefficient floor level
    vector<Coefficients*>evec(Evec.size());
    for(unsigned int k=0;k<Evec[0]->childSize();k++){
        for(unsigned int l=0;l<Evec.size();l++)evec[l]=Evec[l]->child(k);
        mapT->childAdd(_mapConstructor(ToSpectral,Disc,SIndex,evec[0]->idx(),evec));
    }

    if(Evec[0]->isLeaf()){
        string hash=name+tools::str(Evec[0]->levelRank());
        vector<complex<double> > mat;
        for(unsigned int k=0;k<Evec.size();k++)
            for(unsigned int l=0;l<Evec[0]->size();l++)
                mat.push_back(Evec[k]->floorData()[l]);

        UseMatrix fmat;
        if(ToSpectral) {
            // to spectral from original
            fmat=UseMatrix::UseMap(mat.data(),Evec[0]->size(),Evec.size()).transpose();
            fmat.purge(1.e-14,1.e-15);
            mapT->floor()=OperatorFloor::factory(vector<const UseMatrix*>(1,&fmat),hash+SIndex->hash()+Evec[0]->idx()->hash());
        }

        else {
            // from spectral to original
            fmat=UseMatrix::UseMap(mat.data(),Evec[0]->size(),Evec.size());
            fmat.purge(1.e-14,1.e-15);
            mapT->floor()=OperatorFloor::factory(vector<const UseMatrix*>(1,&fmat),hash+Evec[0]->idx()->hash()+SIndex->hash());
        }
    }
    return mapT;
}

void DiscretizationSpectral::addSpectralValues(const EigenSolver *Slv, Index *Idx, OperatorDiagonal *SpectralValues){

    const Index * idx=Idx->firstFloor();
    for(const EigenSolver* slv=Slv->firstLeaf();slv!=0;slv=slv->nextLeaf(Slv)){
        if(slv->eigenvalues().size()!=0){
            // spectral indices only for non-zero blocks
            spectralValues->add(slv->eigenvalues(),idx);
            idx=idx->nodeRight();
        }
    }
}

void DiscretizationSpectral::indexAndMaps(const EigenSolver *Slv, Index * &Idx, OperatorTree * &FromParent, OperatorTree * &ToParent){

    // bottom up build of spectral index and maps

    const Index* opIdx=Slv->oper()->iIndex;

    if(Slv->isLeaf()){

        FromParent=0;
        ToParent=0;
        Idx=0;
        if(Slv->eigenvalues().size()==0) return;

        Idx=new Index();
        Idx->setFloor(0);
        Idx->setBasis(BasisSet::getDummy(Slv->eigenvalues().size()));
        Idx->setAxisName("Eigen");

        for(unsigned int k=0;k<Idx->basisSet()->size();k++){
            Idx->childAdd(new Index());
            Idx->childBack()->setBasis(BasisSet::getDummy(1));
        }
        ToParent  =_mapConstructor(false,this,Idx,opIdx,Slv->rightVectors());
        FromParent=_mapConstructor(true, this,Idx,opIdx,Slv->dualVectors());
    }
    else {
        // diagonal level, operator acts as identity
        Idx=new Index();
        Idx->nodeCopy(opIdx,false);
        FromParent=new OperatorTree(  "toSpectral",Idx,opIdx);
        ToParent  =new OperatorTree("fromSpectral",opIdx,Idx);

        std::vector<int> removed;

        for(int k=0;k<Slv->childSize();k++){
            Index * idx;
            OperatorTree* fromParent,*toParent;
            indexAndMaps(Slv->child(k),idx,fromParent,toParent);
            if(idx==0){
                removed.push_back(k);
                continue; // no block here
            }
            if(Idx!=0)Idx->childAdd(idx);
            if(fromParent!=0)FromParent->childAdd(fromParent);
            if(toParent!=0)ToParent->childAdd(toParent);
        }

        if(removed.size() == Slv->childSize()){
            delete Idx;
            delete FromParent;
            delete ToParent;

            Idx=0;
            FromParent=0;
            ToParent=0;
        }else if(removed.size() > 0){
            const BasisAbstract* basis = Idx->basisAbstract();
            if(basis){
                Idx->setBasis(basis->remove(removed));
            }
        }
    }
    Idx->sizeCompute(); // this is essentially an Index-constructor - need size

    // remove near-zero blocks
    if(FromParent!=0)FromParent->purge(FromParent->norm()*1.e-15);
    if(ToParent!=0)ToParent->purge(ToParent->norm()*1.e-15);

    // broadcast the maps
    if(FromParent!=0)ParallelOperator::bcast(FromParent);
    if(ToParent!=0)ParallelOperator::bcast(ToParent);

    // check block projection property
    if(Slv->isLeaf() and not tRecX::off("spectralMap")){
        Coefficients ran(opIdx);
        Coefficients pro(opIdx);
        Coefficients spec(Idx);
        ran.setToRandom();
        for(unsigned int k=0;k<2;k++){
            FromParent->apply(1.,ran,0.,spec);
            ToParent->apply(1.,spec,0.,pro);
            if(k==0)ran=pro;
        }
        if(not (pro-=ran).isZero(1.e-10))PrintOutput::warning(Str("FAILED spectral projection on level (")+opIdx->index()+")"+", err="+tools::str(pro.maxCoeff()));
        else                             PrintOutput::DEVmessage("OK spectral projection");
    }
}

void DiscretizationSpectral::check(const OperatorAbstract* Op) const {
    if(tRecX::off("spectralMap"))return;
    if(idx()==0 or idx()->size()==0){
        PrintOutput::DEVwarning("cannot check empty discretization: "+name);
        return;
    }

    Str mess(""," ");
    Coefficients cspec(idx());
    cspec.setToRandom();
    cspec*=1.;
    Coefficients cinit(cspec);
    Coefficients cparent(Op->iIndex);
    mapToParent()->apply(1.,cspec,0.,cparent);
    mapFromParent()->apply(-1.,cparent,1.,cinit);
    if(not cinit.isZero(1.e-9)){
        mess=mess+"\nnot identity on spectral space:\n"+cinit.str(2);
    }
    Coefficients caux(cparent);
    mapFromParent()->apply(1.,cparent,0.,cinit);
    mapToParent()->apply(-1.,cinit,1.,caux);
    if(not caux.isZero(1.e-9)){
        mess=mess+"\nnot projector on parent space:\n"+caux.str();
    }

    double errMax=0.;
    if(Op != 0){
        Coefficients c1(Op->jIndex);
        Coefficients c1Proj(Op->jIndex);
        Coefficients c2(Op->iIndex);
        Coefficients cSpec(idx());
        Coefficients cSpec2(idx());
        Coefficients c2Check(Op->iIndex);

        c1.setToRandom();
        c1.makeContinuous();

        mapFromParent()->apply(1., c1, 0., cSpec);
        mapToParent()->apply(1., cSpec, 0., c1Proj);

        Op->apply(1., c1Proj, 0., c1);
        Op->iIndex->inverseOverlap()->apply(1., c1, 0., c2);
        c2.makeContinuous();

        spectralValues->updateFunction(0., OperatorDiagonal::identityFunction);
        spectralValues->apply(1., cSpec, 0., cSpec2);
        mapToParent()->apply(1., cSpec2, 0., c2Check);

        c2-=c2Check;
        if(not c2.isZero(1.e-9)){
            mess=mess+"HP=U^\\dagger d U not satisfied, error=";
            mess=mess+c2.norm();
        }
        errMax=std::max(c2.norm(),errMax);
    }

    if(mess=="")PrintOutput::DEVmessage(Str("OK spectral maps for")+name+"(size"+cinit.size()+")");
    else        PrintOutput::warning(mess+"for"+name+"(size"+cinit.size()+")");
    if(errMax>1.e-6){
        Sstr+"Op\n"+Op->str()+Sendl;
        Sstr+"Op.idx\n"+Op->iIndex->str()+Sendl;
        Sstr+"eigenvalues\n"+spectralValues->str()+Sendl;
        DEVABORT("severe spectral error");
    }
}
























