#include "convertFromGrid.h"

#include "index.h"
#include "operatorMap.h"

using namespace std;

ConvertFromGrid::ConvertFromGrid(const Index *GridIdx, std::vector<int> Deflate)
    :gridIdx(GridIdx)
{
    if(Deflate.size()==0)Deflate.assign(GridIdx->height(),1);
    basIdx=gridIdx->toIndexBasis(Deflate);
    toBas=new OperatorMap(basIdx,gridIdx);
}
