#include "operatorHartree.h"
#include "basisOrbitalNumerical.h"
#include "multipolePotential.h"
#include "index.h"
#include "basisExpIm.h"
#include "densityMatrix1.h"
#include "operatorFloorXC.h"
#include "gaunts.h"
#include "patchRadial.h"

#include "timer.h"
#include "printOutput.h"
#include "my4darray_shared.h"
#include "eigenTools.h"
#include "basisChannel.h"
#include "gaunt.h"
#include "mo.h"
#include "basisMO.h"
#include "basisDvr.h"
#include "vectorValuedFunction.h"
#include "basisMatMatrix.h"
#include "algebra.h"

std::string potName(std::string OpDef) {
    std::string pot=OpDef.substr(OpDef.find("Hartree"));
    if(pot==OpDef)ABORT("no Hartree potential in "+OpDef);
    if(tools::findFirstOutsideBrackets(pot,"[","{(","})")==std::string::npos)return "CoulombEE";
    pot=tools::stringInBetween(OpDef,"[","]");
    if(pot.find("Hartree")!=string::npos)return "CoulombEE";
    return pot;
}

OperatorHartree::OperatorHartree(std::string Pot, const Index* IIndex, const Index* JIndex, std::complex<double> Multiplier)
{
    dat=0;
    oNorm=1.;
    if(IIndex->basisIntegrable()->isAbsorptive()
            or IIndex->basisIntegrable()->lowBound()!=JIndex->basisIntegrable()->lowBound()
            or IIndex->basisIntegrable()->upBound()!=JIndex->basisIntegrable()->upBound()
            )
        oNorm=0.;
}
static std::vector<const BasisIntegrable*> radialBases(const Index* Idx) {

    std::vector<const BasisIntegrable*> res;
    // find first Rn floor
    const Index * idx=Idx->firstLeaf();
    while(idx!=0 and idx->parent()->axisName()!="Rn")idx=idx->nextLeaf();
    if(idx==0)DEVABORT("no Rn basis found in Idx hierarchy "+Idx->root()->hierarchy());

    for(idx=idx->parent();idx!=0;idx=idx->nodeRight(Idx)){
        if(idx->axisName()!="Rn")ABORT("need Rn radial axis, got: "+idx->strData());
        int k;
        for(k=0;k<res.size() and not(*res[k]==*idx->basisIntegrable());k++);
        if(k==res.size())res.push_back(idx->basisIntegrable());
    }
    return res;
}
static std::vector<const BasisIntegrable*> radialBases(const Coefficients* C) {
    return radialBases(C->idx());
}

// in given patch, subtract from all (diagonal) channels the reference channel matrix
static void hartreeRelative(PatchRadial<OperatorHartree> & Patch,  const DensityMatrix1 & Dens){
    if(Patch.firstC().op->def().find("HartreeRelative{")==string::npos)return; // do not form difference

    int RefChan=tools::string_to_int(tools::stringInBetween(Patch.firstC().op->name,"{","}"));

    for(PatchRadial<OperatorHartree>::LeafM &leafM: Patch.leafs){
        for (PatchRadial<OperatorHartree>::LeafL & leafL: leafM.leafs){
            Eigen::MatrixXcd matRef=*leafL.leafs[Dens.cacb(RefChan,RefChan)].mat;
            for(int c=0;c<Dens.nChan();c++){
                if(leafL.leafs[Dens.cacb(c,c)].mat->size()==0)DEVABORT("matrix no set up");
                *leafL.leafs[Dens.cacb(c,c)].mat-=matRef;
            }
        }
    }
}

/// maximum angular momentum l found in Idx
static int lMax(const Index* Idx){
    //HACK somewhat unsafe
    const Index* idx=Idx;
    while(idx!=0 and idx->axisName()!="Eta"){
        if(idx->axisName().find("&")!=string::npos)idx=idx->child(1); //HACK for hybrid
        idx=idx->descend();
    }
    if(idx==0)DEVABORT("Index does not contain Eta-axis, hierarchty="+Idx->hierarchy());
    int l=0;
    for(int k=0;k<idx->childSize();k++)l=std::max(l,int(idx->basisAbstract()->physical(k)));
    return l;
}

/// basA-wise multipole potentials
///
///     Wml[basA][M][L](a,cc)...int[ds] V[L](a,s) sum[mi,mj,li,lj] gaunt(li,mi,L,P,lj,mj) rho{cc}[mi,mj,li,lj](s,s)
///
/// (see notes)
static void getPotWml(std::string Pot, const std::vector<std::vector<Eigen::MatrixXcd> > &RhoIJ,
                      const BasisOrbitalNumerical &IOrb, const Index* IdxA,
                      std::map<double,std::map<int,std::vector<Eigen::MatrixXcd>>> & Wml)
{
    Gaunts gaunts;
    std::vector<const BasisIntegrable*> radBasX(radialBases(IOrb.orbital(0)->idx()));
    std::vector<const BasisIntegrable*> radBasA(radialBases(IdxA));

    DensityMatrix1 rho(RhoIJ); // spherical rep of density matrix rho for present radial basis
    int lpMax=std::min(2*lMax(IdxA),2*lMax(IOrb.orbital(0)->idx())); // maximal possible lp

    // loop through radial basis
    for(const BasisIntegrable* basX: radBasX){
        rho.patch(*basX,*basX,IOrb,IOrb); // set density to present (diagonal) x-patch

        // mpPots[basA][lp](a,x) = V[lp](a,x)*basA(x)*basX(x)
        // radial multipole pots multiplied by basis values at DVR points for given basX
        std::map<double,std::vector<Eigen::MatrixXcd> > mpPots;
        const BasisDVR* xDvr=dynamic_cast<const BasisDVR*>(basX);
        for(const BasisIntegrable* basA: radBasA){
            // product of values ad DVR points
            const BasisDVR* aDvr=dynamic_cast<const BasisDVR*>(basA);
            if(aDvr==0 or xDvr==0)DEVABORT("for now, need dvr bases, got\n"+basA->str()+"\n"+basX->str());
            std::vector<double> aVal(aDvr->valNodes());
            std::vector<double> xVal(xDvr->valNodes());
            MultipolePotential mpPot(lpMax,Pot,basA,basX);
            Eigen::MatrixXd prodAX=Eigen::Map<Eigen::MatrixXd>(aVal.data(),aVal.size(),1)
                    * Eigen::Map<Eigen::MatrixXd>(xVal.data(),1,xVal.size());
            for(int lp=0;lp<=lpMax;lp++){
                mpPots[basA->lowBound()].push_back(mpPot.vals(lp).cwiseProduct(prodAX));
            }
        }

        // loop through points in basX
        for (int x=0;x<basX->size();x++){
            rho.point(x,x);
            // sig[M][L](1,cc)=sum[li,mi,mj,lj] gaunt(li,mi|L,mi-mj,lj,mj) rho{cc}[mi,mj,li,lj
            std::map<int,std::vector<Eigen::MatrixXcd> > sig;
            for(int mi=rho.imMin();mi<=rho.imMax();mi++)
                for(int mj=rho.jmMin();mj<=rho.jmMax();mj++){
                    DensityMatrix1::M* rhoM=rho(mi,mj);
                    std::vector<Eigen::MatrixXcd> & sigM=sig[mi-mj];
                    if(sigM.size()==0)
                        sigM.assign(rhoM->ilMax()+rhoM->jlMax()+1,Eigen::MatrixXcd::Zero(1,rho.cacbMax()+1));

                    for(int li: rhoM->listLi()){
                        DensityMatrix1::L* rhoML=(*rhoM)(li);
                        for(int lj: rhoM->listLj()){
                            int lpMin;
                            const std::vector<double> & gauntIJ=gaunts.vals(-mi,mj,li,lj,lpMin);
                            DensityMatrix1::LL* rhoMLL=(*rhoML)(lj); // rho[mi,mj,li](s,s;cc)
                            for(int gp=0;gp<gauntIJ.size();gp++){
                                sig[mi-mj][lpMin+2*gp]+=gauntIJ[gp]
                                        *Eigen::Map<Eigen::MatrixXcd>(rhoMLL->densC.data(),1,rhoMLL->densC.size());
                            }
                        }
                    }
                }

            for(const BasisIntegrable * basA: radBasA){
                for(auto & sigM: sig){
                    std::vector<Eigen::MatrixXcd> & wmlA=Wml[basA->lowBound()][sigM.first];
                    if(wmlA.size()==0)wmlA.assign(lpMax+1,Eigen::MatrixXcd::Zero(basA->size(),sigM.second[0].cols()));
                    for(int lp=0;lp<wmlA.size();lp++){
                        Wml[basA->lowBound()][sigM.first][lp]+=mpPots[basA->lowBound()][lp].col(x)*sigM.second[lp];
                    }
                }
            }
        }
    }
}

TIMER(Hartree,)
static bool firstPost=true;
void OperatorHartree::postProcess(OperatorTree *Op, const std::vector<std::vector<Eigen::MatrixXcd> > &RhoIJ, const BasisOrbitalNumerical &IOrb){

    START(Hartree);

    Gaunts gaunts;
    DensityMatrix1 rho(RhoIJ);
    PatchRadial<OperatorHartree> patch(Op,rho,"diagonal");
    if(patch.leafs.size()==0)return; // already done

    IOrb.orbitals();
    if(firstPost)IOrb.print("Setting up Hartree operator for "+IOrb.str());
    firstPost=false;

    // get mean field potentials in mp,lp-expansion
    std::map<double,std::map<int,std::vector<Eigen::MatrixXcd> > > wml;
    getPotWml(potName(Op->def()),RhoIJ,IOrb,Op->iIndex,wml);

    // compute M{cc}[basA;ma,la,a;mb,lb] as sum over multipoles
    for(;patch.leafs.size()>0;patch=PatchRadial<OperatorHartree>(Op,rho,"diagonal")){
         if(not (*patch.aBas()==*patch.bBas()))DEVABORT("non-diagonal radial term in Hartree");
        for(PatchRadial<OperatorHartree>::LeafM &leafM: patch.leafs){
            int mp=leafM.ma-leafM.mb;
            for (PatchRadial<OperatorHartree>::LeafL & leafL: leafM.leafs){
                int lpMin;
                std::vector<double> gAB=gaunts.vals(-leafM.ma,leafM.mb,leafL.la,leafL.lb,lpMin);
                // loop over channels cc
                for(PatchRadial<OperatorHartree>::LeafC & leafC: leafL.leafs){
                    // sum over multipole L
                    for(int gp=0;gp<gAB.size();gp++){
                        *leafC.mat+=wml[patch.aBas()->lowBound()][mp][lpMin+2*gp].col(leafC.cc)*gAB[gp];
                    }
                }
            }
        }
        hartreeRelative(patch,rho); // potential relative to one given channel (if desired)
    }

    // final setup of floors
    OperatorHartree* floor;
    for(OperatorTree* op=Op->firstLeaf();op!=0;op=op->nextLeaf()){
        if(0!=(floor=dynamic_cast<OperatorHartree*>(const_cast<OperatorFloor*>(op->floor())))
                and floor->dat==0)floor->finalize(op);
    }

    if(dynamic_cast<const BasisOrbitalNumerical*>(&IOrb))test(Op,IOrb);
    STOP(Hartree);
}
void OperatorHartree::finalize(const OperatorTree* OpLeaf)
{
    if(_mat.size()==0)DEVABORT("no matrix calculated");

    // post-multiply by function of r if desired
    std::string def=OpLeaf->def();
    if(def.find("*")!=std::string::npos){
        if(def.find("Hartree")>def.find("*"))
            ABORT("can only post-multiply, e.g. Hartree*exp(-Q), found: "+def);
        std::string algStr=tools::stringInBetween(OpLeaf->def(),"*",">");
        Algebra alg(algStr);
        if(not alg.isAlgebra())ABORT("error in algebra string "+def+"\n"+Algebra::failures);
        std::vector<double> nodes=dynamic_cast<const BasisDVR*>(OpLeaf->iIndex->basisAbstract())->nodes();
        for(int k=0;k<nodes.size();k++)_mat(k)*=alg.val(nodes[k]);
    }

    construct(_mat,"Hartree");
    _mat.resize(0,0);
}

// test against input MO integrals
void OperatorHartree::test(const OperatorTree* Op, const BasisOrbitalNumerical &IOrb) {


    const std::vector<int>& s = IOrb.select();

    for(const OperatorTree* ppCC=Op->descend();ppCC!=0;ppCC=ppCC->nodeNext()){
        if(ppCC->def().find("Hartree")!=string::npos
                and ppCC->parent()->iIndex->axisName()=="Channel"
                and ppCC->iIndex->parent()==ppCC->jIndex->parent()
                and ppCC->iIndex->axisName()=="Phi")
        {
            if(ppCC->def().find("HartreeRelative")!=string::npos)continue; // this cannot compare to full Hartree

            const BasisChannel * bChan=dynamic_cast<const BasisChannel*>(ppCC->parent()->iIndex->basisAbstract());
            // non-zero channel blocks
            int ci=ppCC->iIndex->nSibling();
            int cj=ppCC->jIndex->nSibling();
            Eigen::MatrixXcd rho(bChan->rho(ci,cj));
            Eigen::MatrixXcd potNum=Eigen::MatrixXcd::Zero(IOrb.size(),IOrb.size());
            Eigen::MatrixXcd potOrb=Eigen::MatrixXcd::Zero(IOrb.size(),IOrb.size());
            Coefficients tmp(ppCC->iIndex);
            for(int j=0;j<IOrb.size();j++){
                *ppCC->tempRHS()=*IOrb.orbital(j);
                ppCC->apply(1.,*ppCC->tempRHS(),0.,tmp);
                for(int i=0;i<IOrb.size();i++){
                    potNum(i,j)=IOrb.orbital(i)->innerProduct(&tmp);
                    for(int k=0;k<IOrb.size();k++){
                        for(int l=0;l<IOrb.size();l++){
                            double vIJKL=dynamic_cast<const BasisMO*>(IOrb.orbitalFunctions())->vinayMO()->vEE(s[i],s[j],s[k],s[l]);
                            potOrb(i,j)+=rho(k,l)*vIJKL;
                        }
                    }
                }
            }
            EigenTools::purge(potNum,1.e-10);
            EigenTools::purge(potOrb,1.e-10);
            double err=0.;
            double maxVal=potOrb.lpNorm<Eigen::Infinity>();
            for(int j=0;j<potNum.cols();j++)
                for(int i=0;i<potNum.rows();i++){
                    if(potNum(i,j)+potOrb(i,j)!=0.)err=max(err,abs(potNum(i,j)-potOrb(i,j)));
                }
            PrintOutput::message(Sstr+"verified Hartree for channel ("+ci+cj+") against original potentials with relative error"
                                 +(tools::str(100*err/maxVal,2)+"% (abs=")+err+")");
            if(err>maxVal*1.e-2){
                PrintOutput::warning("significant deviation -- may need larger basis");
            }
        }
    }
}


