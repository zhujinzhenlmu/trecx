// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "basisMolecularOrbital.h"

#include "mo.h"
#include "basisSet.h"
#include "coordinateTrans.h"
#include "index.h"

#include "eigenNames.h"
#include "basisPolarOff.h"

BasisMolecularOrbital::BasisMolecularOrbital(mo &Mo, const BasisSet & Radial, int Lmax, int Mmax)
    :_mo(&Mo)
{

    _polarIdx=BasisPolarOff::productIndex(&Radial,Lmax,Mmax);
    _moIdx=new Index(vector<const BasisAbstract*>(1,BasisSet::getDummy(Mo.coef.rows())),vector<string>(1,"MolOrb"));

    // transform grid and first derivatives to zero-center
    _quadCoor="Phi.Eta.Rn";
    _comSca.resize(3); // cannot complex scale off-center basis

    // get the quadrature grid and weights
    productGridWeig(_polarIdx,_quadGrid,_quadWeig,10);

    // cartesian suitable for mo
    Matrix<double,Dynamic,3> xyz(_quadGrid.size(),3);
    for(int k=0;k<_quadGrid.size();k++)
        xyz.row(k)=Map<MatrixXd>(CoordinateTrans::fromPolar3d(_quadGrid[k]).data(),1,3);

    // evaluate mo's values and derivatives
    MatrixXd val,derX,derY,derZ;
    Mo.values(val,xyz);
    Mo.derivatives(derX,xyz,"x");
    Mo.derivatives(derY,xyz,"x");//HACK - y not defined yet
    Mo.derivatives(derZ,xyz,"z");


    for(int pt=0;pt<_quadGrid.size();pt++){

        _valDer.push_back(vector<vector<complex<double> > >());
        _valDer[pt].resize(val.rows());

        // transformation from cartesian gradient to polar partial derivatives
        Eigen::Matrix3d trans=Eigen::Map<Eigen::Matrix3d>(CoordinateTrans::jacPolar3d(_quadGrid[pt]).data(),3,3);
        for(int ibas=0;ibas<_valDer[pt].size();ibas++){
            _valDer[pt][ibas].push_back( val(ibas,pt));
            _valDer[pt][ibas].push_back(derX(ibas,pt));
            _valDer[pt][ibas].push_back(derY(ibas,pt));
            _valDer[pt][ibas].push_back(derZ(ibas,pt));
             Eigen::Map<Eigen::Vector3cd>(_valDer[pt][ibas].data()+1)=trans*Eigen::Map<Eigen::Vector3cd>(_valDer[pt][ibas].data()+1);
        }
    }

}
