#include "spectrumPzSimple.h"

#include "polylagrange.h"
#include "basisGrid.h"
#include "coefficients.h"
#include "coefficientsPermute.h"
#include "index.h"
#include "plot.h"
#include "plotKind.h"
#include "evaluator6D.h"
#include "discretizationSurface.h"
#include "discretizationtsurffspectra.h"
#include "readInput.h"
#include "mpiWrapper.h"
#include "spectrumAngle.h"

using namespace std;


void SpectrumPzSimple::initialize(ReadInput &inpc) {
    double E00, E01, E10, E11;
    inpc.read("kZ", "E01", E01, "0.", "end E0");
    inpc.read("kZ", "E00", E00, "0.", "start E0");
    inpc.read("kZ", "E10", E10, "0.", "start E1");
    inpc.read("kZ", "E11", E11, "0.", "end E1");
}

SpectrumPzSimple::SpectrumPzSimple(const Coefficients amplitutes, ReadInput inpc, int version) {
    _version = version;
    _ampl = new Coefficients(amplitutes);
    _specFile = inpc.outputTopDir() + "/spec_kZ";
    Nk = _ampl->idx()->axisName() == "Phi1" ? int(sqrt(_ampl->firstLeaf()->size())) : _ampl->firstLeaf()->size();
    if (_ampl->idx()->axisName() != "Phi"
            and _ampl->idx()->hierarchy() != "Phi1.Eta1.Phi2.Eta2.specRn1.specRn2.kRn1.kRn2")ABORT("we now only accept the hierarchy of Phi1.Eta1.Phi2.Eta2.specRn1.specRn2.kRn1.kRn2 for 6D");
    inpc.read("JAD", "thetaNum", _thetaNum, "0", "number of thetas");
    inpc.read("JAD", "startKIdx", _startKIdx, "0", "start k index");
    kGrid = _ampl->firstFloor()->idx()->basisGrid()->mesh();
    deltaK = kGrid[1] - kGrid[0];
    for (int k = 1; k < kGrid.size(); k++)
        if (abs(kGrid[k] - kGrid[k - 1] - deltaK) > 1.e-5 * abs(deltaK))ABORT(Str("Pz-spectrum requires equidistant momentum grid, is\n") + kGrid);
    for (int k = kGrid.size() - 1; k >= 0; k--)zGrid.push_back(-kGrid[k]);
    zGrid.push_back(0.);
    for (int k = 0; k < kGrid.size(); k++)   zGrid.push_back( kGrid[k]);
    double E00, E01, E10, E11;
    inpc.read("kZ", "E01", E01, "0.", "end E0");
    if (E01 > 0.) {
        inpc.read("kZ", "E00", E00, "0.", "start E0");
        inpc.read("kZ", "E10", E10, "0.", "start E1");
        inpc.read("kZ", "E11", E11, "0.", "end E1");
        unsigned int k00Idx, k01Idx, k10Idx, k11Idx;
        SpectrumAngle::getKIdx(E00, E01, k00Idx, k01Idx, kGrid);
        SpectrumAngle::getKIdx(E10, E11, k10Idx, k11Idx, kGrid);
        cropCoef(k00Idx, k01Idx, k10Idx, k11Idx);
    }
    if (_version == 0)
        DPlot = SpectrumAngle::getJADDisc(inpc);
    getInterplate();
}

void SpectrumPzSimple::getInterplate() {
    interp.resize(zGrid.size());
    PolyLagrange<double> *lag;
    if (_version == 0) {
        _thetaNum = _thetaNum == 0 ? (2 * _ampl->child(0)->idx()->childSize() + 1) : _thetaNum;
        for (unsigned int i = 0; i < _thetaNum; i++)
            etaGrid.push_back(-1 + 2. / (_thetaNum - 1) * i);
        lag = new PolyLagrange<double>(etaGrid);
    }
    for (int z = 0; z < interp.size(); z++) {
        interp[z].resize(kGrid.size());
        for (int k = _startKIdx; k < interp[z].size(); k++) {
            if (-kGrid[k] <= zGrid[z] and zGrid[z] <= kGrid[k]) {
                // interpolation coefficients for given eta=z/k
                if (_version == 0) {
                    // interpolation coefficients for given eta=z/k
                    interp[z][k] = lag->values(zGrid[z] / kGrid[k]);
                    // absorb the integration weight
                    for (int e = 0; e < interp[z][k].size(); e++) {
                        //k^2*dk*sin\theta*d\theta
                        interp[z][k][e] *= kGrid[k] * deltaK;
                    }
                } else
                    getInterplateWf(zGrid[z] / kGrid[k], k, interp[z][k]);
            }
        }
    }
}

void SpectrumPzSimple::getInterplateWf(double cosTheta, int k, std::vector<double> &interItem) {
    UseMatrix mat = UseMatrix::Constant(1, 1, cosTheta);
    UseMatrix values = _ampl->child(0)->idx()->basisAbstract()->val(mat);
    // The polynomial at Phi is 1/sqrt(2pi) * exp(i*m*\phi)
    // The polynomial at Eta is sqrt((2*l+1)/2 * (l-m)! / (l+m)!)* P_l^m(\eta)
    for (unsigned int i = 0; i < values.cols(); i++) {
        interItem.push_back(values(0, i).real());
    }
}


void SpectrumPzSimple::computeNormal(vector<vector<double> > &specKZ, int startKIdx, int endkIdx) {
    vector<string> axisJAD = {"kRn1", "kRn2", "Eta1", "Eta2"}, useJAD(4, "g");
    vector<unsigned int> pointsJAD = {Nk, Nk, _thetaNum, _thetaNum};
    vector<double> boundJAD = {-1, 1}, boundK = {0., 0.};
    vector<vector<double> > boundsJAD(2, boundK);
    boundsJAD.push_back(boundJAD);
    boundsJAD.push_back(boundJAD);
    Plot plotJADPerKRn(DPlot->idx(), axisJAD, useJAD, pointsJAD, boundsJAD);
    Coefficients *ampPerKRn = new Coefficients(DPlot->idx());
    Coefficients *leafPerKRn, *leafOrigin;
    ampPerKRn->setToZero();
    int startK1Idx = Nk / MPIwrapper::Size() * MPIwrapper::Rank(), endK1Idx = Nk / MPIwrapper::Size() * (MPIwrapper::Rank() + 1);
    for (unsigned int k1Idx = startK1Idx; k1Idx < endK1Idx; k1Idx++) {
        for (unsigned int k2Idx = _startKIdx; k2Idx < Nk; k2Idx++) {
            leafPerKRn = ampPerKRn->firstLeaf();
            leafOrigin = _ampl->firstLeaf();
            while (leafOrigin) {
                *(leafPerKRn->data()) = *(leafOrigin->data() + k1Idx * Nk + k2Idx);
                leafOrigin = leafOrigin->nodeRight();
                leafPerKRn = leafPerKRn->nodeRight();
            }
            vector<vector<double> > tem = plotJADPerKRn.sum(*ampPerKRn, _specFile, 1, "");
            for (unsigned int z1 = 0; z1 < zGrid.size(); z1++)
                for (unsigned int z2 = 0; z2 < zGrid.size(); z2++)
                    for (unsigned int eta1 = 0; eta1 < interp[z1][k1Idx].size(); eta1++)
                        for (unsigned int eta2 = 0; eta2 < interp[z2][k2Idx].size(); eta2++)
                            specKZ[z1][z2] += interp[z1][k1Idx][eta1] * interp[z2][k2Idx][eta2] * tem.back()[eta1 * etaGrid.size() + eta2];
        }
    }
}

void SpectrumPzSimple::computeWf6D(vector<vector<double> > &specKZ) {
    int startK1Idx = Nk / MPIwrapper::Size() * MPIwrapper::Rank(), endK1Idx = Nk / MPIwrapper::Size() * (MPIwrapper::Rank() + 1);
    for (unsigned int k1Idx = startK1Idx; k1Idx < endK1Idx; k1Idx++) {
//    for (unsigned int k1Idx = _startKIdx; k1Idx < Nk; k1Idx++) {
        for (unsigned int k2Idx = _startKIdx; k2Idx < Nk; k2Idx++) {
            for (unsigned int z1 = 0; z1 < zGrid.size(); z1++)
                for (unsigned int z2 = 0; z2 < zGrid.size(); z2++) {
                    Coefficients *leafOrigin = _ampl->firstLeaf();
                    // In principle, the (-1)^m should be added in the values
                    // But We do not need to consider m at the moment
                    // as m1+m2=0 so the m part (-1)^(m1+m2) is always 1
                    std::complex<double> item(0.);
                    double from1 = 0;
                    while (leafOrigin) {
                        int eta1 = leafOrigin->index()[1], eta2 = leafOrigin->index()[3];
                        if (interp[z1][k1Idx].size() > eta1 and interp[z2][k2Idx].size() > eta2)
                            item += interp[z1][k1Idx][eta1] * interp[z2][k2Idx][eta2] * (*(leafOrigin->data() + k1Idx * Nk + k2Idx));
                        leafOrigin = leafOrigin->nodeRight();
                    }
                    from1 = pow(abs(item), 2) * kGrid[k1Idx] * kGrid[k2Idx] * deltaK * deltaK;
                    specKZ[z1][z2] += from1;
                }
        }
    }
}

void SpectrumPzSimple::computeWf3D(vector<double> &specKZ) {
    for (unsigned int k1Idx = _startKIdx; k1Idx < Nk; k1Idx++) {
        for (unsigned int z1 = 0; z1 < zGrid.size(); z1++) {
            Coefficients *leafOrigin = _ampl->firstLeaf();
            int eta1 = leafOrigin->index()[1];
            std::complex<double> item(0.);
            double from1 = 0, from2 = 0;
            while (leafOrigin) {
                if (interp[z1][k1Idx].size() > eta1)
                    item += interp[z1][k1Idx][eta1] * (*(leafOrigin->data() + k1Idx));
                leafOrigin = leafOrigin->nodeRight();
            }
            from1 = pow(abs(item), 2) * kGrid[k1Idx] * deltaK;
            specKZ[z1] += from1;
        }
    }
}

void SpectrumPzSimple::compute6D() {
    vector<vector<double> > specKZ;
    for (unsigned int i = 0; i < zGrid.size(); i++) {
        vector<double> kzItem(zGrid.size(), 0.);
        specKZ.push_back(kzItem);
    }
    if (_version == 0)
        computeNormal(specKZ);
    else
        computeWf6D(specKZ);
    if (MPIwrapper::Size() > 1) {
        vector< MPI_Request> sendReqs, recvReqs;
        sendReqs.resize(MPIwrapper::Size());
        recvReqs.resize(MPIwrapper::Size());
        vector<vector<complex<double>>> totalSpecKZ(MPIwrapper::Size());
        for (unsigned int i = 0; i < MPIwrapper::Size(); i++)
            totalSpecKZ[i].resize(specKZ.size()*specKZ.size());
        for (unsigned int i = 0; i < specKZ.size(); i++)
            for (unsigned int j = 0; j < specKZ[i].size(); j++)
                totalSpecKZ[MPIwrapper::Rank()][i * specKZ.size() + j] = specKZ[i][j];
        int rank = MPIwrapper::Rank();
        if (rank == 0)
            for (unsigned int i = 1; i < MPIwrapper::Size(); i++) {
                MPIwrapper::IRecv(totalSpecKZ[i].data(), totalSpecKZ[i].size(), i, recvReqs[i]);
                MPIwrapper::Wait(&recvReqs[i]);
                for (unsigned int j = 0; j < specKZ.size(); j++)
                    for (unsigned int k = 0; k < specKZ[j].size(); k++)
                        specKZ[j][k] += totalSpecKZ[i][j * specKZ.size() + k].real();
            } else {
            MPIwrapper::ISend(totalSpecKZ[MPIwrapper::Rank()].data(), totalSpecKZ[MPIwrapper::Rank()].size(), 0, sendReqs[MPIwrapper::Rank()]);
            MPIwrapper::Wait(&sendReqs[MPIwrapper::Rank()]);
        }
    }
    if (MPIwrapper::isMaster()) {
        ofstream specKZFile(_specFile);
        for (unsigned int i = 0; i < specKZ.size(); i++) {
            for (unsigned int j = 0; j < specKZ[i].size(); j++)
                specKZFile << setprecision(7) << zGrid[i] << ", " << setprecision(7) << zGrid[j] << ", " << specKZ[i][j] << endl;
            if (i != specKZ.size() - 1)
                specKZFile << endl;
        }
        specKZFile.close();
    }

}

void SpectrumPzSimple::compute3D() {
    vector<double> specKZ(zGrid.size(), 0.);
    computeWf3D(specKZ);
    ofstream specKZFile(_specFile);
    for (unsigned int i = 0; i < specKZ.size(); i++)
        specKZFile << setprecision(7) << zGrid[i] << ", " << specKZ[i] << endl;
    specKZFile.close();
}

void SpectrumPzSimple::compute() {
    if (_ampl->idx()->axisName() == "Phi1")
        compute6D();
    else if (_ampl->idx()->axisName() == "Phi")
        compute3D();
}

void SpectrumPzSimple::cropCoef(int k01Idx, int k02Idx, int k11Idx, int k12Idx) {
    Coefficients *coefFloor = _ampl->firstLeaf();
    while (coefFloor) {
        if (_ampl->idx()->axisName() == "Phi1") {
            for (unsigned int i = 0; i < Nk; i++)
                for (unsigned int j = 0; j < Nk; j++)
                    if (not (i > k01Idx and i < k02Idx and j > k11Idx and j < k12Idx))
                        *(coefFloor->data() + i * Nk + j) = 0;
        } else {
            for (unsigned int i = 0; i < Nk; i++)
                if (not (i > k01Idx and i < k02Idx))
                    *(coefFloor->data() + i) = 0;
        }
        coefFloor = coefFloor->nodeRight();
    }
}

