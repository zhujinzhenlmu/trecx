// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "eigenSolverAbstract.h"
#include "coefficients.h"
#include "operatorTree.h"

#include "tools.h"
#include "printOutput.h"
#include "tRecXchecks.h"
#include "algebra.h"
#include "readInput.h"

using namespace std;
std::string EigenSolverAbstract::method;
std::string EigenSolverAbstract::defaultMethod;

void EigenSolverAbstract::readControls(ReadInput &Inp){
    Inp.read("Eigensolver","method",defaultMethod,"auto","options: auto...(default) reasonable choice,Lapack,Arpack,NonLin",1,"eigenMethod");
    method=defaultMethod;

}
void EigenSolverAbstract::setMethod(std::string Method){
    if(method!=defaultMethod)PrintOutput::DEVwarning("setting new EigenSolver method="+Method+" but previous != default");
    method=Method;
}

std::string EigenSolverAbstract::autoMethod(const string Method){
    if(Method!="auto")return Method;
    if(_op==0)return Method;
    int size=_op->iIndex->sizeCompute();
    if(size<200 or _numberEigenv>_op->iIndex->sizeCompute()/5)return "Lapack";
    return "Arpack";
}

EigenSolverAbstract::EigenSolverAbstract(const OperatorAbstract *Op):
    _serial(true),
    _op(Op),
    _ovr(0),
    _computeLeftVectors(false),
    _computeRightVectors(true),
    _numberEigenv(INT_MAX),
    _sort("SmallReal"),
    _excludeRange(false),
    _eMin(-DBL_MAX),
    _eMax(DBL_MAX)
{
    if(Op!=0)_numberEigenv=min(_numberEigenv,Op->iIndex->sizeStored());
}

EigenSolverAbstract & EigenSolverAbstract::compute(const OperatorAbstract *Op, const OperatorAbstract *Ovr){
    //if(_op!=0)ABORT("Operator alread set - call copute w/o arguments");
    _op=Op;

    if(_ovr==0){
        if(Ovr==0)_ovr=Op->iIndex->overlap();
        else _ovr=Ovr;
    }
    else if(Ovr!=0)
        //ABORT("Overlap alread set - call compute w/o Ovr argument");

    if(_op->iIndex!=_ovr->iIndex)ABORT("bad");

    _rightVectors.clear();
    _leftVectors.clear();
    _dualVectors.clear();
    _eigenvalues.clear();
    _compute();
    normalize();
    return *this;
}

void EigenSolverAbstract::select(string Kind){
    if(Kind=="All"){
        sort("SmallReal");
    }

    else if(Kind.find("SmallReal[")==0){
        int nend=Algebra(tools::stringInBetween(Kind,"[","]")).val(0.).real();
        sort("SmallReal");
        if(nend>=_eigenvalues.size())return;
        for(int k=nend;k<_rightVectors.size();k++)delete _rightVectors[k];
        for(int k=nend;k<_leftVectors.size();k++)delete _leftVectors[k];
        for(int k=nend;k<_dualVectors.size();k++)delete _dualVectors[k];
        _rightVectors.resize(nend);
        _leftVectors.resize(nend);
        _dualVectors.resize(nend);
        _eigenvalues.resize(nend);
        PrintOutput::DEVmessage(Sstr+"selected"+nend+"eigenvalues out of"+_ovr->iIndex->sizeStored()+", max energy"+_eigenvalues.back());
    }

    else if(Kind.find("Rectangle")==0){
        vector<string> rect=tools::splitString(tools::stringInBetween(Kind,"[","]"),',');
        if(rect.size()!=4)ABORT("need Rectangle[realMin,realMax,imagMin,imagMax], got: "+Kind);
        vector<double>lim;
        for(int k=0;k<rect.size();k++)lim.push_back(tools::string_to_double(rect[k]));
        for(int k=_eigenvalues.size()-1;k>=0;k--){
            double eR=_eigenvalues[k].real(),eI=_eigenvalues[k].imag();
            if(lim[0]>eR or eR>lim[1] or lim[2]>eI or eI>lim[3]){
                if(_rightVectors.size()==_eigenvalues.size()){
                    delete _rightVectors[k];
                    _rightVectors.erase(_rightVectors.begin()+k);
                }
                if(_dualVectors.size()==_eigenvalues.size()){
                    delete _dualVectors[k];
                    _dualVectors.erase(_dualVectors.begin()+k);
                }
                if(_leftVectors.size()==_eigenvalues.size()){
                    delete _leftVectors[k];
                    _leftVectors.erase(_leftVectors.begin()+k);
                }
                _eigenvalues.erase(_eigenvalues.begin()+k);
            }
        }
    }

    else
        ABORT("selection kind not defined: "+Kind);
}

void EigenSolverAbstract::postSelect(){
    for(auto k: {6,3,1}){
        _rightVectors.erase(_rightVectors.begin()+k);
        _dualVectors.erase(_dualVectors.begin()+k);
        _eigenvalues.erase(_eigenvalues.begin()+k);
    }
}

void EigenSolverAbstract::sort(string Kind){
    // (really dumb)
    std::vector<std::complex<double> > tmp;
    if(_leftVectors.size()>0){
        tmp=_eigenvalues;
        tools::sortKey(Kind, tmp, _leftVectors);
    }
    if(_rightVectors.size()>0){
        tmp=_eigenvalues;
        tools::sortKey(Kind, tmp, _rightVectors);
    }
    if(_dualVectors.size()>0){
        tmp=_eigenvalues;
        tools::sortKey(Kind, tmp, _dualVectors);
    }
    tmp=_eigenvalues;
    tools::sortKey(Kind, tmp,_eigenvalues);


}

void EigenSolverAbstract::phaseFix(){
    for(int k=0;k<_rightVectors.size();k++){
        // phase at maximum coefficient
        std::complex<double> phaseAtMax=_rightVectors[k]->cMaxNorm();
        phaseAtMax/=std::abs(phaseAtMax);
        _rightVectors[k]->scale(std::conj(phaseAtMax));
        _dualVectors[k]->scale(phaseAtMax);
    }
}

void EigenSolverAbstract::normalize(){
    for(int k=0;k<_rightVectors.size();k++){
        // phase at maximum coefficient
        std::complex<double> phaseAtMax=_rightVectors[k]->cMaxNorm();
        phaseAtMax/=std::abs(phaseAtMax);
        // L2-normalize rhs
        double realNrm=std::abs(_ovr->matrixElement(*_rightVectors[k],*_rightVectors[k]));
        _rightVectors[k]->scale(std::conj(phaseAtMax)/sqrt(realNrm));
        _dualVectors[k]->scale(1./_dualVectors[k]->innerProduct(_rightVectors[k],true));
    }
}

bool EigenSolverAbstract::verify(double Epsilon) const {
    if(tRecX::off("EigenSolver"))return true;
    double maxErr=0.;
    vector<double> err;
    for(int k=0;k<_rightVectors.size();k++){
        _ovr->apply(1.,*_rightVectors[k],0.,*_ovr->tempLHS());
        _op->apply(1.,*_rightVectors[k],0.,*_op->tempLHS());
        _op->tempLHS()->axpy(-_eigenvalues[k],_ovr->tempLHS());
        _op->tempLHS()->makeContinuous();
        err.push_back(abs(_op->tempLHS()->norm()/(_rightVectors[k]->norm()*max(abs(_eigenvalues[k]),1.))));
        maxErr=max(maxErr,err.back());
    }
    if(maxErr>Epsilon){
        PrintOutput::DEVwarning(Str("largest eigenvector error"," ")+maxErr);
    }

    UseMatrix one(_dualVectors.size(),_rightVectors.size());
    for(int k=0;k<_dualVectors.size();k++)
        for(int l=0;l<_rightVectors.size();l++)
            one(k,l)=_dualVectors[k]->innerProduct(_rightVectors[l],true);
    if(not one.purge().isIdentity(Epsilon)){
        one-=UseMatrix::Identity(one.rows(),one.cols());
        PrintOutput::DEVwarning(Str("incorrect duals, max error = ")+one.maxAbsVal());
    }
    return maxErr<Epsilon;
}
