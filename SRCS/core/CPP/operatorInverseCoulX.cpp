#include "operatorInverseCoulX.h"
#include "index.h"
#include "operatorFloor.h"

OperatorInverseCoulX::OperatorInverseCoulX(const Index *Idx, unsigned int SubD, unsigned int SuperD, bool BandOvr):OperatorTree("Inv(CoulXOvr)",Idx,Idx){
    if(Idx->hasFloor()){
        oFloor = OperatorFloor::factoryInverse(Idx, SubD, SuperD, BandOvr);
    }
    else{
        for(int k=0;k<Idx->childSize();k++){
            childAdd(new OperatorInverseCoulX(Idx->child(k),SubD,SuperD,BandOvr));
        }
    }
}
