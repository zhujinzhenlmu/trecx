// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "operatorFactor.h"

#include "readInput.h"
#include "tools.h"

using namespace std;

map<string,UseMatrix> OperatorFactor::matrix;

void OperatorFactor::readMatrix(ReadInput & Inp)
{
    string Name;
    unsigned int line=0;
    while (line<1000) {
        line++;
        Inp.read("FactorMatrix","name",Name,"BLANK","name to be used in operator definition",line);
        if(Name=="BLANK")return;
        unsigned int nrow,ncol;
        Inp.read("FactorMatrix","nrow",nrow,ReadInput::noDefault,"number of rows",line);
        Inp.read("FactorMatrix","ncol",ncol,tools::str(nrow),"number of columns (default == number of rows)",line);
        matrix[Name]=UseMatrix(nrow,ncol);
        string rowString;
        for(unsigned int i=0;i<nrow;i++){
            line++;
            Inp.read("FactorMatrix","name",rowString,"","name to be used in operator definition",line);
            vector<string> entry=tools::splitString(rowString,' ');
            if(entry.size()!=ncol)ABORT("too few colums in "+Name+", need BLANK SEPARATED list of all entries, is: "+rowString);
            for(unsigned int j=0;j<entry.size();j++)matrix[Name](i,j)=tools::string_to_double(entry[j]);
        }
    }
    ABORT("FactorMatrix entry limited to below 1000 lines in total total");
}
