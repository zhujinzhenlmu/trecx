// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "basisfunctioncineutral.h"
#include "read_columbus_data.h"
#include "quantumChemicalInput.h"
#include "mo.h"
#include "ci.h"

#include "printOutput.h"

using namespace std;
#include "eigenNames.h"


BasisFunctionCINeutral::BasisFunctionCINeutral(int order):BasisFunction("CIneut",order)
{
    My4dArray_shared* vee;
        orbitals = new mo(*QuantumChemicalInput::neutrals,0);
        CI = new ci(*QuantumChemicalInput::neutrals,*orbitals,order,0);
        vee=QuantumChemicalInput::neutrals->vee;

    /// Setup required reduced density matrices
    CI->ci_setup_all_spindens(vee,neut_rho1,neut_rho2,neut_rho3,vector<double>(0),vector<int>(0),&neut_2e);
}

BasisFunctionCINeutral::~BasisFunctionCINeutral()
{
    if(CI!=0)       {delete CI; CI=0;}
    if(orbitals!=0) {delete orbitals; orbitals=0;}
}

UseMatrix BasisFunctionCINeutral::matrix_1e(string def) const
{
    // check if it is storage
    if(onepMat.find(def)!=onepMat.end()) return onepMat[def];

    // Else compute

    UseMatrix result = UseMatrix::Zero(neut_2e.rows(),neut_2e.rows());

    if(def!="EEInt"){

        MatrixXcd mores;
        orbitals->matrix_1e(mores,def);

//        if(def=="1") mores *= (1./orbitals->col_data->no_electrons);     // special case for overlap
        if(def=="1") mores *= (1./CI->nElectrons());     // special case for overlap

        int Nmos = orbitals->NumberOfOrbitals();
        for(int I_i=0;I_i<result.rows();I_i++)
            for(int I_j=0;I_j<result.cols();I_j++){

                MatrixXd rho = neut_rho1[I_i][I_j].block(0,0,Nmos,Nmos);
                rho += neut_rho1[I_i][I_j].block(Nmos,Nmos,Nmos,Nmos);

                result(I_i,I_j) = (mores.array()*rho.array()).sum();
            }
    }
    else{

        for(int I_i=0;I_i<result.rows();I_i++)
            for(int I_j=0;I_j<result.cols();I_j++){
//                if(orbitals->col_data->no_electrons>1){
                    if(CI->nElectrons()>1){
                    if(I_i>=I_j ) result(I_i,I_j) += 0.5*neut_2e(I_i,I_j);
                    else          result(I_i,I_j) += 0.5*neut_2e(I_j,I_i);
                }
            }
    }

    onepMat[def]=result;
    return result;
}
