#include "basisOrbitalNumerical.h"

#include "operatorDefinitionNew.h"
#include "index.h"
#include "indexNew.h"
#include "discretizationGrid.h"
#include "basisGrid.h"
#include "inverse.h"
#include "vectorValuedFunction.h"
#include "coordinateTrans.h"
#include "readInput.h"
#include "printOutput.h"
#include "coorSystem.h"
#include "plot.h"
#include "operatorMap.h"

#include "operatorHartree.h"
#include "basisDvr.h"
#include "inverseDvr.h"

void BasisOrbitalNumerical::setup(){
    //HACK for now
    if(VectorValuedFunction::get("CO2")!=0){
        std::string refIdx;
        if(BasisOrbital::referenceIndex.count("main"))refIdx=":main";
        if(BasisOrbital::referenceIndex.count("single"))refIdx=":single";
        // generate and plot MO's
        BasisOrbitalNumerical allOrbs("CO2"+refIdx);
        allOrbs.generateOrbitals();
        allOrbs.print("CO2 molecular orbitals");
        allOrbs.plot();
    }
}

BasisOrbitalNumerical::BasisOrbitalNumerical(const BasisSetDef &Def)
    :BasisOrbitalNumerical(tools::stringInBetween(Def.funcs,"[","]"),Def.order,Def.lowBound())
{}

BasisOrbitalNumerical::BasisOrbitalNumerical(std::string Def, int NumberOfOrbitals, int First)
    :BasisOrbitalNumerical(Def,std::vector<int>())
{
    std::string sel=tools::stringInBetween(Def,"{","}");
    if(sel!=Def){
        std::vector<std::string> vsel=tools::splitString(sel,',');
        for(std::string s: vsel)_select.push_back(tools::string_to_int(s));

        if(First+NumberOfOrbitals<=_select.size()){
            while(0<First--)_select.erase(_select.begin());
            _select.resize(NumberOfOrbitals);
        }
        else
            ABORT(Sstr+"too few functions, cannot select"+NumberOfOrbitals+"starting at"+First+Str::sep("")+"'th of "+Def);
    }
    else if(NumberOfOrbitals>0)
        for(int k=First;k<First+NumberOfOrbitals;k++)
            _select.push_back(k);
}

BasisOrbitalNumerical::BasisOrbitalNumerical(const std::string Def,std::vector<int> Select)
    :BasisOrbital("NumOrb"),_select(Select)
{
    if(Def.find(":")==std::string::npos)ABORT("need format FuncDef:RefIndex, got: "+Def);
    _funcDef=Def.substr(0,Def.rfind(":"));
    _refName=Def.substr(Def.rfind(":")+1);
    _orb.resize(_select.size());
    _name="NumOrb["+_funcDef+"]";
}

static std::vector<double> gridPoint(const Index* Idx){
    std::vector<double> pts;
    for(const Index * idx=Idx;idx->parent()!=0;idx=idx->parent()){
        const BasisGrid* b=idx->parent()->basisGrid();
        if(b!=0)pts.push_back(b->mesh()[idx->nSibling()]);
    }
    std::reverse(pts.begin(),pts.end());
    return pts;
}

std::string BasisOrbitalNumerical::str(int Level) const{
    Str s("NumOrb[","");
    s+=_funcDef+"] ";
    if(size()==1)     s=s+"no."+_select[0];
    else if(size()<10)s=s+"{"+_select+"}";
    else              s=s+_select.front()+",..,"+_select.back()+"} ["+size()+"]";
    if(orbitalFunctions()==0)return "function not found: "+_funcDef;
    return s+" out of "+orbitalFunctions()->info();
}

/// set CoefficientFloors with f.innerProduct(f)<Eps*C.innerProduct(C) to =0
static void purge(Coefficients & C, double Eps=1.e-20){
    double eps=Eps*abs(C.innerProduct(&C));
    for(Coefficients* f=C.firstLeaf();f!=0;f=f->nextLeaf())
        if(abs(f->innerProduct(f))<eps)f->setToZero();
}

void BasisOrbitalNumerical::generateOrbitals(const Index *Idx){
    const Index* idx=Idx;
    if(idx==0){
        if(referenceIndex.find(_refName)==referenceIndex.end())
            ABORT("reference index not defined: "+_refName+"\navailable: "+tools::listMapKeys(referenceIndex));
        idx=referenceIndex[_refName];
    }

    // get the function
    const VectorValuedFunction * orbs=orbitalFunctions();
    if(orbs==0)ABORT("not defined "+_funcDef+", do VectorValuedFunction::add(...) first\nAvailable:\n"
                     +VectorValuedFunction::list());

    // get discretization on a quadrature grid
    DiscretizationGrid gDisc(idx,tools::splitString(idx->coordinates(),'.'));

    CoordinateMap fromIdx=CoordinateTrans::toCartesian(idx->coordinates());
    IntegrationFactor factorIdx=CoordinateTrans::integrationFactor(idx->coordinates());
    CoordinateMap toFunctionArg=CoordinateTrans::fromCartesian(orbs->coordinates());
    IntegrationFactor factorFunc=CoordinateTrans::integrationFactor(orbs->coordinates());

    const Index * idxLeaf=gDisc.idx()->firstLeaf();
    std::vector<std::complex<double> > val;

    std::vector<Coefficients> gVal;
    for (int k=0;k<gDisc.idx()->size();k++,idxLeaf=idxLeaf->nodeNext()){
        std::vector<double>arg,point;
        point=gridPoint(idxLeaf);
        arg=toFunctionArg(fromIdx(point));
        if(not orbs->validArguments(arg))
            orbs->abortInvalidArguments(tools::str(arg)+" converted from "
                                        +tools::str(gridPoint(idxLeaf)));
        double ratio=factorFunc(arg);
        ratio=ratio==0.? 1. : factorIdx(point)/ratio; //quadratur point=0 may be included, but is not needed here...

        val=(*orbs)(arg);

        if(k==0){
            // at first point set up the vectors
            if(_select.size()==0)
                for(int k=0;k<val.size();k++)_select.push_back(k);
            for(int sel: _select)if(sel>=val.size())ABORT(Str("cannot select"," ")+_select+"only"+val.size()+"orbitals");
            _orb.resize(_select.size()); // no size was given, use full function
            gVal.resize(_orb.size());
            //CAUTION: the Coefficients copy constructor broken: does not copy the contiguous storage, assignemt does
            for(int k=0;k<_orb.size();k++)gVal[k]=Coefficients(gDisc.idx());

        }
        for(int l=0;l<gVal.size();l++)gVal[l].orderedData()[k]=val[_select[l]]*ratio;
    }

    if(not idx->inverseOverlap()){
        const_cast<Index*>(idx)->localOverlapAndInverse(0,0);
        Inverse::factory(idx);
    }
    for(int k=0;k<size();k++){
        _orb[k]=Coefficients(idx);
        gDisc.mapToParent()->apply(1.,gVal[k],0.,_orb[k]);
        _orb[k].makeContinuous();
        purge(_orb[k],1.e-20);
    }
    std::vector<double>nrm=norms();
    for(int k=nrm.size()-1;k>=0;k--){
        if(nrm[k]<0.99){
            if(nrm[k]<1.e-12){
                nrm.erase(nrm.begin()+k);
                _orb.erase(_orb.begin()+k);
                PrintOutput::warning(Str("erased empty orbital no."," ")+k+"- reference basis may be too small");
            } else
                PrintOutput::warning(Str("low norm"," ")+nrm[k]+"for orbital"+k+": expansion basis may be inaccurate");
        }
    }
    orthonormalize(true);

    // warn about complex scaled region
    for(int k=0;k<_orb.size();k++){
        double scaledExp=abs( idx->overlap()->matrixElement(_orb[k],_orb[k])
                              -idx->overlap()->matrixElementUnscaled(_orb[k],_orb[k]));
        if(scaledExp>1.e-9){
            PrintOutput::warning(Str("OrbitalNumerical"," ")+k+"has expectation value"+scaledExp+"in scaled region - may not be meaningful");
        }
    }

}

bool BasisOrbitalNumerical::operator==(const BasisAbstract & Other) const {
    const BasisOrbitalNumerical* other=dynamic_cast<const BasisOrbitalNumerical*>(&Other);
    if(not other)return false;
    return size()==other->size()
            and _refName==other->_refName
            and _funcDef==other->_funcDef
            and _select==other->_select;
}

void BasisOrbitalNumerical::print(std::string Title) const{

    std::vector<double>ekin=kineticEnergy();
    std::vector<double>lsqu=expectationValues("<<AngularMomentumSquared>>");
    std::vector<double>mmag=expectationValues("<<AngularMomentumZsquared>>");
    //    std::vector<double>pari=expectationValues("<<Parity>>");

    if(Title!="")PrintOutput::title(Title);

    PrintOutput::newRow();
    PrintOutput::rowItem("No.");
    PrintOutput::rowItem("{org}");
    PrintOutput::rowItem("<T>");
    PrintOutput::rowItem("<L(L+1)>");
    PrintOutput::rowItem("<Lz^2>");
    PrintOutput::rowItem("Parity");
    for(int k=0;k<_orb.size();k++){
        PrintOutput::newRow();
        PrintOutput::rowItem(k);
        PrintOutput::rowItem(_select[k]);
        PrintOutput::rowItem(ekin[k]);
        PrintOutput::rowItem(lsqu[k]);
        PrintOutput::rowItem(mmag[k]);
    }
    PrintOutput::paragraph();
}

const VectorValuedFunction* BasisOrbitalNumerical::orbitalFunctions() const {
    return VectorValuedFunction::get(_funcDef.substr(0,_funcDef.find("{")));
}

void BasisOrbitalNumerical::plot() const {
    if(size()==0)return;
    Plot plt(_orb[0].idx(),ReadInput::main);
    if(plt.isEmpty())return;
    std::string pltFile=ReadInput::main.output()+name();

    int wid= size()>100 ? 3 : 2;
    for(int k=0;k<size();k++)
        plt.plot(_orb[k],pltFile+tools::str(k,wid,'0'));
    PrintOutput::message("Orbital plots on "+pltFile+tools::str(0,wid,'0')+" through ..."+tools::str(size()-1,wid,'0'));
    PrintOutput::paragraph();

}


