// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "basisPolarOff.h"

#include "readInput.h"
#include "printOutput.h"
#include "axis.h"
#include "index.h"
#include "basisSet.h"
#include "coordinateTrans.h"
#include "basisProd.h"
#include "qtEigenDense.h"
#include "str.h"
#include "basisMat.h"
#include "axis.h"

using namespace std;

std::vector<double> BasisPolarOff::toCartesian(const std::vector<double>&CoorNdim) const{
    if(_quadCoor!="Phi.Eta.Rn")DEVABORT("only implemented for Phi.Eta.Rn");
    vector<double> vu=CoordinateTrans::fromPolar3d(CoorNdim);
    for(int k=0;k<dim();k++)vu[k]+=_origin[k];
    return vu;
}

Eigen::MatrixXd BasisPolarOff::jacobianToNdim(const std::vector<double>&CoorNdim) const {
    return Eigen::Map<Eigen::MatrixXd>(CoordinateTrans::jacPolar3d(CoorNdim).data(),3,3);
}

BasisPolarOff::BasisPolarOff(ReadInput &Inp)
{
    _name="PolarOff";
    _ndimCoor="Phi.Eta.Rn";
    double rad;
    int lmax,mmax,nrad,quad;
    Inp.read("PolarOffCenter","origin",_origin,"0 0 0","x y z, origin of the off-center polar basis");
    Inp.read("PolarOffCenter","radius",rad,"0","radius around origin");
    Inp.read("PolarOffCenter","Lmax",lmax,"2","maximal angular momentum");
    Inp.read("PolarOffCenter","Mmax",mmax,tools::str(lmax),"maximal |m|");
    Inp.read("PolarOffCenter","Nmax",nrad,"10","number of radial functions");
    Inp.read("PolarOffCenter","quad",quad,"10","minimal number of quadrature points");
    if(not Inp.found("PolarOffCenter"))return;
    if(rad==0.)ABORT("must specify radius>0");
    double offRad=sqrt(_origin[0]*_origin[0]+_origin[1]*_origin[1]+_origin[2]*_origin[2]);
    if(_origin!=vector<double>(3,0.) and rad>0.5*offRad){
        if(rad>=offRad*(1.-1.e-12))
            ABORT("off-center radius must be smaller than distance to origin, is: "+tools::str(rad)+" >= "+tools::str(offRad));
        PrintOutput::warning(
                    Str("off-center basis not well separated from origin, recommended distance > 2*radius, is:")+
                    rad+"vs"+offRad+"\nlarge number of quadrature points may help, is: "+quad);
    }

    // get the reference coordinates
    _quadCoor=mainCoor(Inp);
    _idx=idxConstruct(rad,lmax,mmax,nrad);

    // get native quadrature points and values and first derivatives there
    BasisProd off(_idx,quad);
    _valDer=off.valDer();
    _quadGrid=off.quadGrid();
    _quadWeig=off.quadWeig();

    // transform to main coordinates and cooresponding values and derivatives
    mainQuadValDer();

    test();
}

const Index* BasisPolarOff::idxConstruct(double RadMax, int Lmax, int Mmax, int Nrad){
    vector<int> marg(2,0);
    marg[1]=Nrad-1;
    BasisSetDef dR(Nrad,0.,RadMax,"polynomial",true,true,true,Coordinate("Rn"),marg);
    BasisSet *rBas=BasisSet::get(dR);
    return const_cast<const Index*>(productIndex(rBas,Lmax,Mmax));
}

Index * BasisPolarOff::productIndex(const BasisSet *Radial, int Lmax, int Mmax){
    vector<int> marg(2,0);
    marg[1]=2*Mmax;
    BasisSetDef dPhi(2*Mmax+1,0.,2*math::pi,"cosSin",true,true,true,Coordinate("Phi"),marg);

    Index * idx=new Index();
    idx->setAxisName("Phi");
    idx->setBasis(BasisSet::get(dPhi));
    for(int k=0;k<idx->basisSet()->size();k++){
        marg[1]=Lmax-abs(idx->basisSet()->parameter(k,"m"));
        BasisSetDef dEta(Lmax+1-abs(idx->basisSet()->parameter(k,"m")),-1.,2.,"assocLegendre",
                         true,true,true,Coordinate::fromString("Eta"),marg,
                         ComplexScaling(),false,vector<double>(1,idx->basisSet()->parameter(k,"m")));
        idx->childAdd(new Index());
        idx->childBack()->setAxisName("Eta");
        idx->childBack()->setBasis(BasisSet::get(dEta));
        for (int l=0;l<idx->childBack()->basisSet()->size();l++){
            idx->childBack()->childAdd(new Index());
            idx->childBack()->childBack()->setAxisName("Rn");
            idx->childBack()->childBack()->setBasis(Radial);
            for(int n=0;n<Radial->size();n++)idx->childBack()->childBack()->leafAdd();
        }
    }
    idx->setFloor(2);
    idx->sizeCompute();
    return idx;

}
