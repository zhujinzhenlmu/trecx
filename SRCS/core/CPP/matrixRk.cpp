#include "matrixRk.h"

#include "index.h"
#include "coefficients.h"

#include "qtEigenDense.h"
#include "lapacke.h"

static void svDecompose(std::complex<double>* A, unsigned int rows, unsigned int cols, double* s, std::complex<double>* U, std::complex<double>* Vadj){
	Eigen::JacobiSVD<Eigen::MatrixXcd> svd;
    
    svd.compute(Eigen::Map<Eigen::MatrixXcd>(A, rows, cols), Eigen::ComputeFullU | Eigen::ComputeFullV);
    
    Eigen::Map<Eigen::MatrixXcd>(U, rows, rows)          = svd.matrixU();
    Eigen::Map<Eigen::MatrixXcd>(Vadj, cols, cols)       = svd.matrixV().adjoint();
    Eigen::Map<Eigen::VectorXd> (s, std::min(rows,cols)) = svd.singularValues();
}

void MatrixRk::create(std::vector<std::complex<double> >& m){
    
    unsigned int rows = iIndex->sizeStored();
    unsigned int cols = jIndex->sizeStored();


    // SVD
    _v_compress = new std::vector<std::complex<double> >(cols*cols);
    _v_decompress = new std::vector<std::complex<double> >(rows*rows);
    std::vector<double> s(std::min(rows, cols));
    std::vector<std::complex<double> > compressAdjoint(cols*cols);    
 
    svDecompose(m.data(), rows, cols, s.data(), _v_decompress->data(), compressAdjoint.data());

    // Store into _compress; values stored are conjugates, so scalar product must be 
    // calculated without additional conjugate
    for(unsigned int i=0; i<cols; i++){
        for(unsigned int j=0; j<cols; j++) (*_v_compress)[i*cols+j] = compressAdjoint[j*cols+i];
    }

    // Absorb singular values into _decompress 
    for(unsigned int i=0; i<s.size(); i++){
        for(unsigned int j=0; j<rows; j++) (*_v_decompress)[j+i*rows] *= s[i];
    }

    _compress = _v_compress->data();
    _decompress = _v_decompress->data();


    rank(1);
}


MatrixRk::MatrixRk(const HMatrix::HMatrixIndex* iIndex, const HMatrix::HMatrixIndex* jIndex, HMatrix::TruncationStrategy* strategy, const UseMatrix& mat):
    HMatrix(iIndex, jIndex, strategy),
    _v_full(new std::vector<std::complex<double> >(mat.rows()*mat.cols())),
    _v_decompress(0),
    _v_compress(0),
    _full(0),
    _decompress(0),
    _compress(0),
    _use_full(false){

    unsigned int rows = iIndex->sizeStored();
    unsigned int cols = jIndex->sizeStored();

    if(rows != mat.rows() or cols != mat.cols()) ABORT("Size mismatch!");

    std::vector<std::complex<double> > m(rows*cols);
    for(unsigned int i=0; i<rows; i++){
        for(unsigned int j=0; j<cols; j++){ 
            m[j*rows+i]=mat(i,j);
            (*_v_full)[j*rows+i]=mat(i,j);
        }
    }
    _full = _v_full->data();

    create(m);

#ifdef _DEBUG_
    rank(std::min(iIndex->sizeStored(), jIndex->sizeStored()));
    UseMatrix matCheck;
    asOperatorFloor()->matrix(matCheck);
    
    for(unsigned int i=0; i<mat.rows(); i++){
        for(unsigned int j=0; j<mat.cols(); j++){
            if(std::abs(mat(i,j)-matCheck(i,j))>1.e-12) ABORT("SVD didnt work");
        }
    }
#endif
}

MatrixRk::~MatrixRk(){
    if(_v_full!=0) delete _v_full;
    if(_v_decompress!=0) delete _v_decompress;
    if(_v_compress!=0) delete _v_compress;
}

unsigned int MatrixRk::storageRequirement() const{
    /*
     * TODO: Could be optimized for storage, as for rank>=fullrank/2, _use_full will be set
     */

    return (iIndex->sizeStored()*jIndex->sizeStored()) + (iIndex->sizeStored()+jIndex->sizeStored())*std::min(iIndex->sizeStored(), jIndex->sizeStored());
}

void MatrixRk::storage(std::complex<double>* storage){
    
    // Move data, _full starts at 0, _decompress at rows*cols, _compress at rows*cols+rows*rows
    unsigned int c=0;
    for(unsigned int i=0; i<iIndex->sizeStored()*jIndex->sizeStored(); i++, c++) storage[c]=_full[i];
    for(unsigned int i=0; i<iIndex->sizeStored()*iIndex->sizeStored(); i++, c++) storage[c]=_decompress[i];
    for(unsigned int i=0; i<jIndex->sizeStored()*jIndex->sizeStored(); i++, c++) storage[c]=_compress[i];

    _full       = storage;
    _decompress = _full +       iIndex->sizeStored()*jIndex->sizeStored();
    _compress   = _decompress + iIndex->sizeStored()*iIndex->sizeStored();

    // If data was owned previously, free storage
    if(_v_full!=0) delete _v_full;
    if(_v_decompress!=0) delete _v_decompress;
    if(_v_compress!=0) delete _v_compress;
    _v_full=0;
    _v_decompress=0;
    _v_compress=0;
}

std::vector<MatrixRk*> MatrixRk::getLowrankMatrices(){
    std::vector<MatrixRk*> res;
    res.push_back(this);
    return res;
}

void MatrixRk::fullMatrix(UseMatrix& mat) const{
    mat = UseMatrix::UseMap(_full, iIndex->sizeStored(), jIndex->sizeStored());
}

void MatrixRk::rankForAccuracy(double rel, double abs, const UseMatrix* mat){
    UseMatrix Mat;
    if(mat==0){
        fullMatrix(Mat);
        mat = &Mat;
    }

    for(unsigned int k=0; k<=std::min(iIndex->sizeStored(), jIndex->sizeStored()); k++){
        rank(k);
        double _rel, _abs;
        accuracy(_rel, _abs, mat);
        if(abs<=0.){
            if(_rel<rel) break;
        }else{
            if(_abs<abs) break;
        }
    }
}

void MatrixRk::rank(unsigned int rank){
    _rank=rank;
    _use_full = _rank*(iIndex->sizeStored()+jIndex->sizeStored()) >= (iIndex->sizeStored()*jIndex->sizeStored());
}

int MatrixRk::rank() const{ return _rank; }

void MatrixRk::axpy(std::complex<double> A, const std::complex<double>* X, std::complex<double> B, std::complex<double>* Y) const{
    unsigned int rows = iIndex->sizeStored();
    unsigned int cols = jIndex->sizeStored();
    
    //TEMPORARILY NEEDED, can be dropped, if _use_full is reinstated
    //unsigned int _rank = std::min(this->_rank, std::min(rows, cols));

    if(B!=1.){
        if(B==0.){
            for(unsigned int i=0; i<rows; i++) Y[i]=0.;
        } else {
            for(unsigned int i=0; i<rows; i++) Y[i]*=B;
        }
        B=1.;
    }

    // Zero-matrix
    if(_rank==0) return;

    if(X!=Y){
        // Without aliasing

        if(A==1.){
            if(_use_full){
                Eigen::Map<Eigen::VectorXcd>(Y, rows).noalias()+= 
                    Eigen::Map<Eigen::MatrixXcd>(const_cast<std::complex<double>* >(_full), rows, cols) * 
                    Eigen::Map<Eigen::VectorXcd>(const_cast<std::complex<double>* >(X), cols);
            }else{

                Eigen::Map<Eigen::VectorXcd>(Y, rows).noalias()+=
                    Eigen::Map<Eigen::MatrixXcd>(const_cast<std::complex<double>* >(_decompress), rows, _rank) *
                    (Eigen::Map<Eigen::MatrixXcd>(const_cast<std::complex<double>* >(_compress), cols, _rank).transpose() *
                     Eigen::Map<Eigen::VectorXcd>(const_cast<std::complex<double>* >(X), cols));

            }
        }else{
            if(_use_full){
                Eigen::Map<Eigen::VectorXcd>(Y, rows).noalias()+= A* 
                    Eigen::Map<Eigen::MatrixXcd>(const_cast<std::complex<double>* >(_full), rows, cols) * 
                    Eigen::Map<Eigen::VectorXcd>(const_cast<std::complex<double>* >(X), cols);
            }else{
                Eigen::Map<Eigen::VectorXcd>(Y, rows).noalias()+= A*
                    Eigen::Map<Eigen::MatrixXcd>(const_cast<std::complex<double>* >(_decompress), rows, _rank) *
                    (Eigen::Map<Eigen::MatrixXcd>(const_cast<std::complex<double>* >(_compress), cols, _rank).transpose() *
                     Eigen::Map<Eigen::VectorXcd>(const_cast<std::complex<double>* >(X), cols));
            }
        }
    }else{
        // With aliasing

        if(A==1.){
            if(_use_full){
                Eigen::Map<Eigen::VectorXcd>(Y, rows)+=
                    Eigen::Map<Eigen::MatrixXcd>(const_cast<std::complex<double>* >(_full), rows, cols) * 
                    Eigen::Map<Eigen::VectorXcd>(const_cast<std::complex<double>* >(X), cols);
            }else{
                Eigen::Map<Eigen::VectorXcd>(Y, rows)+=
                    Eigen::Map<Eigen::MatrixXcd>(const_cast<std::complex<double>* >(_decompress), rows, _rank) *
                    (Eigen::Map<Eigen::MatrixXcd>(const_cast<std::complex<double>* >(_compress), cols, _rank).transpose() *
                     Eigen::Map<Eigen::VectorXcd>(const_cast<std::complex<double>* >(X), cols));
            }
        }else{
            if(_use_full){
                Eigen::Map<Eigen::VectorXcd>(Y, rows)+= A* 
                    Eigen::Map<Eigen::MatrixXcd>(const_cast<std::complex<double>* >(_full), rows, cols) * 
                    Eigen::Map<Eigen::VectorXcd>(const_cast<std::complex<double>* >(X), cols);
            }else{
                Eigen::Map<Eigen::VectorXcd>(Y, rows)+= A*
                    Eigen::Map<Eigen::MatrixXcd>(const_cast<std::complex<double>* >(_decompress), rows, _rank) *
                    (Eigen::Map<Eigen::MatrixXcd>(const_cast<std::complex<double>* >(_compress), cols, _rank).transpose() *
                     Eigen::Map<Eigen::VectorXcd>(const_cast<std::complex<double>* >(X), cols));
            }
        }
    }

    // if(_use_full){
    //     if(A==1.){
    //         for(unsigned int i=0; i<rows; i++){
    //             for(unsigned int j=0; j<cols; j++){
    //     	    Y[i]+=_full[j*rows+i]*X[j];
    //     	}
    //         }
    //     }else{
    //         for(unsigned int i=0; i<rows; i++){
    //             for(unsigned int j=0; j<cols; j++){
    //     	    Y[i]+=A*_full[j*rows+i]*X[j];
    //     	}
    //         }
    //     }
    // }else{
    //     for(unsigned int k=0; k<_rank; k++){
    //         std::complex<double> prod=0.;
    //         for(unsigned int i=0; i<cols; i++) prod+= _compress[k*cols+i]*X[i];
    //         prod*=A;
    //         for(unsigned int i=0; i<rows; i++) Y[i]+= _decompress[k*rows+i]*prod;
    //     }
    // 
    // }
}

long MatrixRk::applyCount() const{
    return std::min(_rank*(iIndex->sizeStored()+jIndex->sizeStored()), iIndex->sizeStored()*jIndex->sizeStored());
}





/*
 * TEMPORARY CODE
 */
void MatrixRk::structureTikZ(std::ostream* output) const{
    int rows=iIndex->sizeStored();
    int cols=jIndex->sizeStored();
    int perc;
    std::string text;
    if(_use_full){
        perc=100;
        text=std::to_string(rows)+"x"+std::to_string(cols);
    }else{
        perc = 100*_rank*(rows+cols)/(rows*cols);
        text=std::to_string(rows)+"x"+std::to_string(cols)+", k="+std::to_string(_rank);
    }

    *output<<"\\fill[red!"<<perc<<"!white] ("<<jIndex->posIndex()<<  ",-"<<iIndex->posIndex()<<  ") rectangle "
                 <<"+("<<jIndex->sizeStored()<<",-"<<iIndex->sizeStored()<<") node[pos=.5, color=black]{"<<text<<"};\n";
}











