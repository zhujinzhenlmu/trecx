#include "projectSubspace.h"

#include "qtEigenDense.h"
#include "eigenTools.h"
#include "coefficients.h"
#include "index.h"
#include "operatorFloor.h"
#include "printOutput.h"
#include "parallelOperator.h"

// duplicated in BasisOrbital!
static Eigen::MatrixXcd allCoefs( std::vector<const Coefficients*> ICoefs){
    if(ICoefs.size()==0)DEVABORT("emtpy vector of Coefficients");
    Eigen::MatrixXcd allMat(ICoefs[0]->size(),ICoefs.size());
    for(int k=0;k<ICoefs.size();k++)allMat.col(k)=Eigen::Map<Eigen::MatrixXcd>(ICoefs[k]->orderedData(),ICoefs[k]->size(),1);
    return allMat;
}


void buildMap(OperatorTree* Map, std::vector<const Coefficients*> C){

    // floor
    if(Map->iIndex->hasFloor() and Map->jIndex->hasFloor()){
        Eigen::MatrixXcd cMat=C[0]->idx()==Map->iIndex ?
                    allCoefs(C) :
                    allCoefs(C).transpose();
        Map->floor()=OperatorFloor::factory(std::vector<const Eigen::MatrixXcd*>(1,&cMat),
                                            "map:"+Map->iIndex->hash()+"|"+Map->jIndex->hash());
    }

    std::vector<const Coefficients*>c(C);

    // column indices
    if(not Map->jIndex->hasFloor())
        for(int n=0;n<Map->jIndex->childSize();n++){
            Map->childAdd(new OperatorTree("map",Map->iIndex,Map->jIndex->child(n)));
            if(C[0]->idx()==Map->jIndex)for(int l=0;l<c.size();l++)c[l]=C[l]->child(n); // descend in C, if C are columns
            buildMap(Map->childBack(),c);
        }

    // row indices
    if(not Map->iIndex->hasFloor())
        for(int n=0;n<Map->iIndex->childSize();n++){
            Map->childAdd(new OperatorTree("map",Map->iIndex->child(n),Map->jIndex));
            if(C[0]->idx()==Map->iIndex)for(int l=0;l<c.size();l++)c[l]=C[l]->child(n); // descend in C, if C are rows
            buildMap(Map->childBack(),c);
        }
    if(Map->root()==Map)Map->purge();
}

ProjectSubspace::ProjectSubspace(std::vector<Coefficients *> Vectors, std::vector<Coefficients *> Duals){
    std::vector<const Coefficients*> cV,cD;
    for(Coefficients* c: Vectors)cV.push_back(c);
    for(Coefficients* c: Duals)  cD.push_back(c);
    _construct(cV,cD);
}

bool ProjectSubspace::verify() const {
    Coefficients c(iIndex),d(iIndex);
    c.setToRandom();
    apply(1.,c,0.,d);
    c=d;
    apply(1.,c,-1.,d);
    if(not d.isZero(1.e-12))PrintOutput::DEVwarning("spectral projectors failed for "+name);
    return d.isZero(1.e-12);
}


std::vector<std::vector<std::vector<const Coefficients*> > > groupByNonzeros(std::vector<const Coefficients*> Vecs,std::vector<const Coefficients*> Duals,std::vector<int> &Sorting){
    // first nonzero and end of non-zero floors in Coefficient
    struct vStruc {
        int nOrig,beg,end;
        const Coefficients *c,*d;
        vStruc(int NOrig, const Coefficients* C,const Coefficients* D):nOrig(NOrig),c(C),d(D){
            beg=0;
            const Coefficients* f=c->firstLeaf();
            for(;f!=0 and f->isZero();f=f->nextLeaf())beg++;
            for(int k=beg;f!=0;k++,f=f->nextLeaf())
                if(not f->isZero())end=k+1;
        }
    };
    std::vector<vStruc> vS;
    for(int k=0;k<Vecs.size();k++)vS.push_back(vStruc(k,Vecs[k],Duals[k]));

    std::sort(vS.begin(),vS.end(),[](const vStruc & A,const vStruc & B){return A.beg<B.beg;});

    std::vector<std::vector<std::vector<const Coefficients*> > > gVecs(2);
    Sorting.clear();
    Sorting.push_back(vS[0].nOrig);
    gVecs[0]=std::vector<std::vector<const Coefficients*> >(1,std::vector<const Coefficients*>(1,vS[0].c));
    gVecs[1]=std::vector<std::vector<const Coefficients*> >(1,std::vector<const Coefficients*>(1,vS[0].d));
    for(int k=1;k<vS.size();k++){
        if(vS[k-1].beg<vS[k].beg){
            gVecs[0].push_back(std::vector<const Coefficients*>(0));
            gVecs[1].push_back(std::vector<const Coefficients*>(0));
        }
        Sorting.push_back(vS[k].nOrig);
        gVecs[0].back().push_back(vS[k].c);
        gVecs[1].back().push_back(vS[k].d);
    }
    return gVecs;
}

void ProjectSubspace::_construct(std::vector<const Coefficients *> Vectors, std::vector<const Coefficients *> Duals)
{
    if(Vectors.size()!=Duals.size())DEVABORT(Sstr+"unequal number of Vectors and Duals"+Vectors.size()+Duals.size());
    name="Project"+tools::str(Vectors.size());
    if(Vectors.size()==0)return;
    iIndex=Vectors[0]->idx();
    jIndex=iIndex;

    // check duals
    if(Vectors[0]->idx()!=Duals[0]->idx())DEVABORT(Sstr+"unequal Index on Vectors and Duals"+Vectors[0]->idx()+Duals[0]->idx());
    if(Duals.size()*Duals[0]->idx()->size()>1.e6)
        PrintOutput::warning("large spectral size - duals not checked for time reasons");
    else {
        Eigen::MatrixXcd ovr(Duals.size(),Vectors.size());
        for(int j=0;j<Vectors.size();j++)
            for(int i=0;i<Duals.size();i++)
                ovr(i,j)=Duals[i]->innerProduct(Vectors[j],true);
        if(not EigenTools::isIdentity(ovr,1.e-12)){
            PrintOutput::DEVwarning("Duals and Vectors do not match on level 1e-12");
            if(not EigenTools::isIdentity(ovr,1.e-9))ABORT(EigenTools::str(ovr,0)+"\nDuals and Vectors do not match");
        }
    }

    std::vector<std::vector<std::vector<const Coefficients*> > > gVecs=groupByNonzeros(Vectors,Duals,_sorting);
    // build a grouped subspace index
    Index* idx=new Index(std::vector<const BasisAbstract*>(1,BasisAbstract::factory("Vector:"+tools::str(gVecs[0].size()))),{"Subspace"});
    for(int k=0;k<idx->basisAbstract()->size();k++)
        idx->childReplace(k,new Index(std::vector<const BasisAbstract*>(1,BasisAbstract::factory("Vector:"+tools::str(gVecs[0][k].size()))),{"SubSubspace"}));
    std::vector<std::string> dum;
    idx->setFloorFE(dum);
    idx->sizeCompute();

    // for now, we consider projections as global (can be improved)
    _subspaceIndex.reset(idx);
    _subspaceC.reset(new Coefficients(_subspaceIndex.get()));

    _mapTo.reset  (new OperatorTree("mapToFullFromContracted",iIndex,_subspaceIndex.get()));
    _mapFrom.reset(new OperatorTree("mapFromFullToContracted",_subspaceIndex.get(),iIndex));

    for(int k=0;k<gVecs[0].size();k++){
        _mapTo->childAdd(new OperatorTree("block",iIndex,_subspaceIndex->child(k)));
        _mapFrom->childAdd(new OperatorTree("block",_subspaceIndex->child(k),iIndex));
        buildMap(_mapTo->childBack(),gVecs[0][k]);
        buildMap(_mapFrom->childBack(),gVecs[1][k]);
    }
    _mapTo->purge(1.e-12);
    _mapFrom->purge(1.e-12);

    ParallelOperator::setDistribution(_mapTo.get());
    ParallelOperator::setDistribution(_mapFrom.get());
    PrintOutput::message(Str("Set up projector, dim=")+Vectors.size());
}



void ProjectSubspace::apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    if(not _subspaceC)const_cast<ProjectSubspace*>(this)->_subspaceC.reset(new Coefficients(_subspaceIndex.get()));
    _mapFrom->apply(A,Vec,0.,*_subspaceC.get());
    _mapTo->apply(1.,*_subspaceC.get(),B,Y);
}

std::vector<Coefficients> ProjectSubspace::orbitals(std::vector<int> Select){
    std::vector<int>select(Select);
    if(select.size()==0)
        for(int k=0;k<dim();k++)select.push_back(k);

    std::vector<Coefficients>result;
    for(int k: select){
        _subspaceC->setToZero();
        _subspaceC->orderedData()[k]=1.;
        result.push_back(Coefficients(iIndex));
        _mapTo->apply(1.,*_subspaceC,0.,result.back());
    }
    return result;
}

ProjectSubspace::~ProjectSubspace(){
}
