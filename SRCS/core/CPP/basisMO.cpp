#include "basisMO.h"

#include "readInput.h"
#include "printOutput.h"
#include "quantumChemicalInput.h"
#include "mo.h"
#include "basisOrbitalNumerical.h"
#include "qtEigenDense.h"


void BasisMO::read(ReadInput & Inp){
    if(not Inp.found("MolecularOrbitals"))return;

    std::string Name;
    Inp.obsolete("MolecularOrbitals","name","access from Axis definition, e.g.  as ChannelHF[Chemical{10,9,8,7}:single] ");
    VectorValuedFunction::add(Name,std::shared_ptr<VectorValuedFunction>(new BasisMO(Inp)));
}

void BasisMO::print(std::string RefIdx) const{
    BasisOrbitalNumerical orbs(_name+":"+RefIdx,0);
    orbs.print("MolecularOrbitals");
}

BasisMO::BasisMO(ReadInput & Inp):BasisAbstract("MO")
{
    QuantumChemicalInput::read(Inp);
    Inp.obsolete("MolecularOrbitals","select","specify selection in basis for Axis, e.g. ChannelHF[Chemical{10,9,8,7}:single]");
    _vinayMO.reset(new mo(*QuantumChemicalInput::neutrals,0));
    _source=QuantumChemicalInput::neutrals->source();
    VectorValuedFunction::add(_name,std::shared_ptr<VectorValuedFunction>(this));
}

BasisMO::BasisMO(const QuantumChemicalInput * System):BasisAbstract("Chemical"){

    //HACK: not trying to fix old mo(...)
    _vinayMO.reset(new mo(*const_cast<QuantumChemicalInput*>(System),0));
    _source=System->source();
}

void BasisMO::add(const QuantumChemicalInput * System){
    std::string name=System->source();
    if(name.rfind("/")==name.length()-1)name=name.substr(0,name.length()-1);
    name=name.substr(name.rfind("/"));
    VectorValuedFunction::add(name,std::shared_ptr<VectorValuedFunction>(new BasisMO(System)));
}

unsigned int BasisMO::size() const {
    return _select.size();
}

std::string BasisMO::str(int Level) const {
    //    return Str("DVR(","")+_opol->name()+") ["+_lowBound+","+_upBound+"] ("+_shiftX+","+_scale+") "+_size+"["+_dvrX.size()+"]";

    Str strSel=_select.size()<10 ? Str(" {","")+_select+"} " : Str(" {")+_select.front()+"..."+_select.back()+"} ";
    return Str(VectorValuedFunction::name(),"")+strSel+"["+size()+"]"+info();
}
std::string BasisMO::info(int Number) const {
    Str strSel;
    if (Number>-1 or _select.size()==1)
        strSel=Str("{","")+_select[Number]+"}";
    else
        strSel=_select.size()<10 ? Str(" {","")+_select+"} " : Str(" {")+_select.front()+"..."+_select.back()+"} ["+size()+"]";
    return Str(VectorValuedFunction::name(),"")+strSel+" from "+_source;
}

std::vector<std::complex<double> > BasisMO::operator()(std::vector<double> X) const{
    Eigen::MatrixXd val;
    Eigen::Matrix<double, Eigen::Dynamic, 3> arg=Eigen::Map<Eigen::MatrixXd>(X.data(),1,3);
    _vinayMO->values(val,arg);
    if(_select.size()==0)
        for(int k=0;k<val.rows();k++)
            const_cast<BasisMO*>(this)->_select.push_back(k);

    std::vector<std::complex<double> > res(_select.size());
    for(int k=0;k<_select.size();k++)res[k]=val(_select[k],0);
    return res;
}

