#include "coulXVerification.h"
#include "recursiveIntegrator.h"
#include "index.h"
#include "orthopol.h"
#ifdef _USE_GSL_
#include <gsl/gsl_sf_bessel.h>

// static variables
int CoulXVerification::m;
int CoulXVerification::mPrime;
int CoulXVerification::lAngular;
int CoulXVerification::lAngularPrime;
double CoulXVerification::k;
double CoulXVerification::kPrime;
double CoulXVerification::normalizationConstPrime;
int CoulXVerification::kIndex;
int CoulXVerification::kPrimeIndex;

double besselR0Func(double x){
    gsl_sf_result besselLLHSVal,besselLRHSVal;
    int status1 = gsl_sf_bessel_jl_e(CoulXVerification::lAngular,CoulXVerification::kPrime*x,&besselLLHSVal);
    int status2 = gsl_sf_bessel_jl_e(CoulXVerification::lAngular,CoulXVerification::k*x,&besselLRHSVal);
    return x*x*besselLLHSVal.val*besselLRHSVal.val;
}

double besselR1Func(double x){
    gsl_sf_result besselLLHSVal,besselLLHSDer,besselLRHSVal,besselLRHSDer;
    int status1 = gsl_sf_bessel_jl_e(CoulXVerification::lAngularPrime,CoulXVerification::kPrime*x,&besselLLHSVal);
    int status2 = gsl_sf_bessel_jl_e(CoulXVerification::lAngular,CoulXVerification::k*x,&besselLRHSVal);
    int status3 = gsl_sf_bessel_jl_e(CoulXVerification::lAngular+1,CoulXVerification::k*x,&besselLRHSDer);
    return x*x*(besselLLHSVal.val*(CoulXVerification::lAngular*besselLRHSVal.val/x-CoulXVerification::k*besselLRHSDer.val));
}

double besselR2Func(double x){
    gsl_sf_result besselLLHSVal,besselLRHSVal;
    int status1 = gsl_sf_bessel_jl_e(CoulXVerification::lAngularPrime,CoulXVerification::kPrime*x,&besselLLHSVal);
    int status2 = gsl_sf_bessel_jl_e(CoulXVerification::lAngular,CoulXVerification::k*x,&besselLRHSVal);
    return x*besselLLHSVal.val*besselLRHSVal.val;
}

double legendreE4Func(double x){
    OrthogonalNassocLegendre legendre(CoulXVerification::m); // There is always "m'=m" for non-zero matrix elements!
    return legendre.val(CoulXVerification::lAngularPrime+1,x)[CoulXVerification::lAngularPrime-CoulXVerification::m]*x*legendre.val(CoulXVerification::lAngular+1,x)[CoulXVerification::lAngular-CoulXVerification::m];
}

double legendreE5Func(double x){
    OrthogonalNassocLegendre legendre(CoulXVerification::m); // There is always "m'=m" for non-zero matrix elements!
    return legendre.val(CoulXVerification::lAngularPrime+1,x)[CoulXVerification::lAngularPrime-CoulXVerification::m]*(1-x*x)*legendre.der(CoulXVerification::lAngular+1,x)[CoulXVerification::lAngular-CoulXVerification::m];
}

CoulXVerification::CoulXVerification(double RMax, std::vector<double> KVec){
    kVec = KVec;
    rRange.resize(0);
    rRange.push_back(0.);rRange.push_back(RMax);
    etaRange.resize(0);
    etaRange.push_back(-1.);etaRange.push_back(1.);
}

void CoulXVerification::calculateOverlap(const Index *Idx, UseMatrix &Mat){
    if(Idx->isRoot()) Mat = UseMatrix::Zero(Idx->sizeStored(),Idx->sizeStored());
    if(Idx->child(0)->childSize()!=0){
        for(unsigned int ip=0; ip<Idx->childSize(); ip++){
            if(not Idx->isRoot()){
                if(Idx->parent()->axisName()=="Phi") m = Idx->nSibling();
            }
            calculateOverlap(Idx->child(ip),Mat);
        }
    }
    else{
        lAngular = Idx->nSibling();
        std::cout << "l = " << lAngular << std::endl;
        for(unsigned int jp=0; jp<Idx->childSize(); jp++){
            kPrimeIndex = Idx->posIndex(Idx->root())+jp;
            kPrime = kVec[jp];
            for(unsigned int j=0; j<Idx->childSize(); j++){
                kIndex = Idx->posIndex(Idx->root())+j;
                k = kVec[j];
                tools::RecursiveIntegrator<double>r0Int(&besselR0Func,1.e-10);
                Mat(kPrimeIndex,kIndex) = r0Int.integrate(rRange);
            }
        }
    }
}

void CoulXVerification::calculateMatrix(const Index *IdxPrime, const Index *Idx, UseMatrix &Mat){
    if(IdxPrime->isRoot()) Mat = UseMatrix::Zero(Idx->sizeStored(),IdxPrime->sizeStored());
    if(IdxPrime->child(0)->childSize()!=0){
        for(unsigned int ip=0; ip<IdxPrime->childSize(); ip++){
            if(not IdxPrime->isRoot()){
                if(IdxPrime->parent()->axisName()=="Phi") mPrime = IdxPrime->nSibling();
            }
            calculateMatrix(IdxPrime->child(ip),Idx,Mat);
        }
    }
    else{
        lAngularPrime = IdxPrime->nSibling();
        std::cout << "l' = " << lAngularPrime << std::endl;
        for(unsigned int jp=0; jp<IdxPrime->childSize(); jp++){
            kPrimeIndex = IdxPrime->posIndex(IdxPrime->root())+jp;
            kPrime = kVec[jp];
            calculateRow(Idx,Mat);
        }
    }
}

void CoulXVerification::calculateRow(const Index *Idx,UseMatrix &Mat){
    if(Idx->child(0)->childSize()!=0){
        for(unsigned int i; i<Idx->childSize(); i++){
            if(not Idx->isRoot()){
                if(Idx->parent()->axisName()=="Phi") m = Idx->nSibling();
                if(m!=mPrime) return;
            }
            calculateRow(Idx->child(i),Mat);
        }
    }
    else{
        lAngular = Idx->nSibling();
        if(lAngular!=lAngularPrime+1 and lAngular!=lAngularPrime-1) return;
        tools::RecursiveIntegrator<double>r1Int(&besselR1Func,1.e-10);
        tools::RecursiveIntegrator<double>r2Int(&besselR2Func,1.e-10);
        tools::RecursiveIntegrator<double>e4Int(&legendreE4Func,1.e-10);
        tools::RecursiveIntegrator<double>e5Int(&legendreE5Func,1.e-10);
        double e4Result = e4Int.integrate(etaRange);
        double e5Result = e5Int.integrate(etaRange);

        for(unsigned int j=0; j<Idx->childSize(); j++){
            kIndex = Idx->posIndex(Idx->root())+j;
            k = kVec[j];
            double r1Result = r1Int.integrate(rRange);
            double r2Result = r2Int.integrate(rRange);
            Mat(kPrimeIndex,kIndex) = r1Result*e4Result+r2Result*e5Result;
        }
    }
}
#endif
