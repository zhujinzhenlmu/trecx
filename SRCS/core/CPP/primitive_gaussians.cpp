// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "primitive_gaussians.h"
#include "read_columbus_data.h"
#include "extra_functions.h"
#include <fstream>
#include <math.h>
#include <cmath>
#include "gaunt.h"
#include "constants.h"
#include <time.h>
//#include <mpi.h>

using namespace std;
#include "eigenNames.h"


void primitive_gaussians::values(MatrixXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz){

    val = MatrixXd::Zero(exponents.size(),xyz.rows());

    for(int i=0;i<exponents.size();i++)
        for(int j=0; j<xyz.rows(); j++)
            val(i,j) =std::pow(xyz(j,0)-coord(i,0),pow(i,0))
                    * std::pow(xyz(j,1)-coord(i,1),pow(i,1))
                    * std::pow(xyz(j,2)-coord(i,2),pow(i,2))
                    * std::exp(-exponents(i)*(std::pow(xyz(j,0)-coord(i,0),2)+std::pow(xyz(j,1)-coord(i,1),2)+std::pow(xyz(j,2)-coord(i,2),2)));
    
}

void primitive_gaussians::values(VectorXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, int fun_index){

    //Returns values for a particular gaussian function
    val = VectorXd::Zero(xyz.rows());
    int i = fun_index;

    for(int j=0; j<xyz.rows(); j++)
        val(j) = std::pow(xyz(j,0)-coord(i,0),pow(i,0))
                *std::pow(xyz(j,1)-coord(i,1),pow(i,1))
                *std::pow(xyz(j,2)-coord(i,2),pow(i,2))
                *std::exp(-exponents(i)*(std::pow(xyz(j,0)-coord(i,0),2)+std::pow(xyz(j,1)-coord(i,1),2)+std::pow(xyz(j,2)-coord(i,2),2)));

}

void primitive_gaussians::derivatives(VectorXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, int fun_index, string wrt){

    //Returns values of derivative of fun_index^th function with respect to wrt at points xyz
    val = VectorXd::Zero(xyz.rows());

    int i=fun_index;

    if(wrt=="r")
        for(int j=0;j<xyz.rows();j++){

            double dx,dy,dz;

            double common_term = std::exp(-exponents(i)*(std::pow(xyz(j,0)-coord(i,0),2)+std::pow(xyz(j,1)-coord(i,1),2)+std::pow(xyz(j,2)-coord(i,2),2)));

            if(pow(i,0)>0)
                dx =  pow(i,0)*std::pow(xyz(j,0)-coord(i,0),pow(i,0)-1)* std::pow(xyz(j,1)-coord(i,1),pow(i,1))* std::pow(xyz(j,2)-coord(i,2),pow(i,2))
                        - 2*exponents(i)*std::pow(xyz(j,0)-coord(i,0),pow(i,0)+1)* std::pow(xyz(j,1)-coord(i,1),pow(i,1))* std::pow(xyz(j,2)-coord(i,2),pow(i,2));
            else
                dx = - 2*exponents(i)*std::pow(xyz(j,0)-coord(i,0),pow(i,0)+1)* std::pow(xyz(j,1)-coord(i,1),pow(i,1))* std::pow(xyz(j,2)-coord(i,2),pow(i,2));

            if(pow(i,1)>0)
                dy =  pow(i,1)*std::pow(xyz(j,0)-coord(i,0),pow(i,0))* std::pow(xyz(j,1)-coord(i,1),pow(i,1)-1)* std::pow(xyz(j,2)-coord(i,2),pow(i,2))
                        - 2*exponents(i)*std::pow(xyz(j,0)-coord(i,0),pow(i,0))* std::pow(xyz(j,1)-coord(i,1),pow(i,1)+1)* std::pow(xyz(j,2)-coord(i,2),pow(i,2));
            else
                dy = - 2*exponents(i)*std::pow(xyz(j,0)-coord(i,0),pow(i,0))* std::pow(xyz(j,1)-coord(i,1),pow(i,1)+1)* std::pow(xyz(j,2)-coord(i,2),pow(i,2));

            if(pow(i,2)>0)
                dz = pow(i,2)*std::pow(xyz(j,0)-coord(i,0),pow(i,0))* std::pow(xyz(j,1)-coord(i,1),pow(i,1))* std::pow(xyz(j,2)-coord(i,2),pow(i,2)-1)
                        - 2*exponents(i)*std::pow(xyz(j,0)-coord(i,0),pow(i,0))* std::pow(xyz(j,1)-coord(i,1),pow(i,1))* std::pow(xyz(j,2)-coord(i,2),pow(i,2)+1);
            else
                dz = - 2*exponents(i)*std::pow(xyz(j,0)-coord(i,0),pow(i,0))* std::pow(xyz(j,1)-coord(i,1),pow(i,1))* std::pow(xyz(j,2)-coord(i,2),pow(i,2)+1);

            val(j) = (dx*xyz(j,0) + dy*xyz(j,1) + dz*xyz(j,2))*common_term/std::sqrt(std::pow(xyz(j,0),2)+std::pow(xyz(j,1),2)+std::pow(xyz(j,2),2));
        }

    else if(wrt=="z"){

        for(int j=0;j<xyz.rows();j++){
            double dz;
            double common_term = std::exp(-exponents(i)*(std::pow(xyz(j,0)-coord(i,0),2)+std::pow(xyz(j,1)-coord(i,1),2)+std::pow(xyz(j,2)-coord(i,2),2)));

            if(pow(i,2)>0)
                dz = pow(i,2)*std::pow(xyz(j,0)-coord(i,0),pow(i,0))* std::pow(xyz(j,1)-coord(i,1),pow(i,1))* std::pow(xyz(j,2)-coord(i,2),pow(i,2)-1)
                        - 2*exponents(i)*std::pow(xyz(j,0)-coord(i,0),pow(i,0))* std::pow(xyz(j,1)-coord(i,1),pow(i,1))* std::pow(xyz(j,2)-coord(i,2),pow(i,2)+1);
            else
                dz = - 2*exponents(i)*std::pow(xyz(j,0)-coord(i,0),pow(i,0))* std::pow(xyz(j,1)-coord(i,1),pow(i,1))* std::pow(xyz(j,2)-coord(i,2),pow(i,2)+1);

            val(j) = dz*common_term;
        }
    }

    else if(wrt=="x"){

        for(int j=0;j<xyz.rows();j++){

            double dx;
            double common_term = std::exp(-exponents(i)*(std::pow(xyz(j,0)-coord(i,0),2)+std::pow(xyz(j,1)-coord(i,1),2)+std::pow(xyz(j,2)-coord(i,2),2)));

            if(pow(i,0)>0)
                dx =  pow(i,0)*std::pow(xyz(j,0)-coord(i,0),pow(i,0)-1)* std::pow(xyz(j,1)-coord(i,1),pow(i,1))* std::pow(xyz(j,2)-coord(i,2),pow(i,2))
                        - 2*exponents(i)*std::pow(xyz(j,0)-coord(i,0),pow(i,0)+1)* std::pow(xyz(j,1)-coord(i,1),pow(i,1))* std::pow(xyz(j,2)-coord(i,2),pow(i,2));
            else
                dx = - 2*exponents(i)*std::pow(xyz(j,0)-coord(i,0),pow(i,0)+1)* std::pow(xyz(j,1)-coord(i,1),pow(i,1))* std::pow(xyz(j,2)-coord(i,2),pow(i,2));

            val(j) = dx*common_term;

        }

    }

    else{
        cout<<"Derivative with respect to "<<wrt<<" not yet defined"<<endl;
        exit(1);
    }


}

void primitive_gaussians::derivatives(MatrixXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, string wrt){

    val = MatrixXd::Zero(exponents.size(),xyz.rows());

    for(int i=0; i<exponents.size(); i++){
        VectorXd v;
        derivatives(v,xyz,i,wrt);
        val.row(i)=v;

    }

}

void primitive_gaussians::matrix_1e(MatrixXcd &result, string name, primitive_gaussians *b){

    if(b==NULL)
        b=this;

    string mat_elem_type = this->type;
    mat_elem_type+="_";
    mat_elem_type+=b->type;
    if(mat_elem_type.compare("gaussian_guassian")){
        gauss_gauss(result,name,b);
    }
}

void primitive_gaussians::gauss_gauss(MatrixXcd &result, string op, primitive_gaussians* b){

    result = MatrixXcd::Zero(exponents.size(),b->exponents.size());

    for(int i=0; i<result.rows(); i++)
        for(int j=0; j<result.cols(); j++){
            if(op=="1")
                result(i,j) = gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0)) * gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1)) * gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));
            else if(op=="x")
                result(i,j) = gauss_1(1,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0)) * gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1)) * gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));

            else if(op=="y")
                result(i,j) = gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0)) * gauss_1(1,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1)) * gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));

            else if(op=="z")
                result(i,j) = gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0)) * gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1)) * gauss_1(1,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));

            else if(op=="xx")
                result(i,j) = gauss_1(2,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0)) * gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1)) * gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));

            else if(op=="yy")
                result(i,j) = gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0)) * gauss_1(2,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1)) * gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));

            else if(op=="zz")
                result(i,j) = gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0)) * gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1)) * gauss_1(2,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));

            else if(op=="rr")
                result(i,j) = gauss_1(2,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0)) * gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1)) * gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2)) + gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0)) * gauss_1(2,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1)) * gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2)) + gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0)) * gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1)) * gauss_1(2,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2)) ;

            else if(op=="dx|dx" or op=="dy|dy" or op=="dz|dz"){
                complex<double > val = 0.0;

                for(int k=0; k<3; k++){
                    RowVector3cd temp = RowVector3cd(0,0,0);
                    for(int l=0; l<3; l++){
                        if(l==k){
                            if (b->pow(j,l)>1)
                                temp(l) = (complex<double >)(b->pow(j,l)*(b->pow(j,l)-1))*gauss_1(0,pow(i,l),exponents(i),coord(i,l),b->pow(j,l)-2,b->exponents(j),b->coord(j,l))-2*b->exponents(j)*(2*b->pow(j,l)+1)*gauss_1(0,pow(i,l),exponents(i),coord(i,l),b->pow(j,l),b->exponents(j),b->coord(j,l)) + 4*std::pow(b->exponents(j),2)*gauss_1(0,pow(i,l),exponents(i),coord(i,l),b->pow(j,l)+2,b->exponents(j),b->coord(j,l));
                            else if(b->pow(j,l)==1)
                                temp(l) = -6*b->exponents(j)*gauss_1(0,pow(i,l),exponents(i),coord(i,l),1,b->exponents(j),b->coord(j,l)) + 4*std::pow(b->exponents(j),2) *gauss_1(0,pow(i,l),exponents(i),coord(i,l),3,b->exponents(j),b->coord(j,l));
                            else if(b->pow(j,l)==0)
                                temp(l) = -2*b->exponents(j)*(gauss_1(0,pow(i,l),exponents(i),coord(i,l),0,b->exponents(j),b->coord(j,l))-2*b->exponents(j)*gauss_1(0,pow(i,l),exponents(i),coord(i,l),2,b->exponents(j),b->coord(j,l)));
                        }
                        else{
                            temp(l) = gauss_1(0,pow(i,l),exponents(i),coord(i,l),b->pow(j,l),b->exponents(j),b->coord(j,l));
                        }
                    }
                    val+=temp(0)*temp(1)*temp(2);
                }
                result(i,j) = -val/2.0;
            }
            else if(op=="D/Dz"){
                MatrixXcd res1;
                gauss_gauss(res1,"z_vg",b);
                result = res1*complex<double>(0,-1) ;
            }

            else if(op=="z_vg"){
                // Dipole operator z in velocity gauge:  i d/dz

                RowVector3cd temp = RowVector3cd(0,0,0);
                temp(0) = gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0));
                temp(1) = gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1));
                temp(2) = -2*b->exponents(j)*gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2)+1,b->exponents(j),b->coord(j,2));
                if(b->pow(j,2) != 0)
                    temp(2) += (complex<double>)b->pow(j,2)*gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2)-1,b->exponents(j),b->coord(j,2));
                result(i,j) = complex<double>(0,1)*temp.prod();
            }
            else if(op=="x_vg"){
                // Dipole operator x in velocity gauge:  i d/dx

                RowVector3cd temp = RowVector3cd(0,0,0);
                temp(0) = -2*b->exponents(j)*gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0)+1,b->exponents(j),b->coord(j,0));
                if(b->pow(j,0) != 0)
                    temp(0) += (complex<double>)b->pow(j,0)*gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0)-1,b->exponents(j),b->coord(j,0));
                temp(1) = gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1));
                temp(2) = gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));
                result(i,j) = complex<double>(0,1)*temp.prod();
            }
            else if(op=="y_vg"){
                // Dipole operator y in velocity gauge:  i d/dy

                RowVector3cd temp = RowVector3cd(0,0,0);
                temp(0) = gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0));
                temp(1) = -2*b->exponents(j)*gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1)+1,b->exponents(j),b->coord(j,1));
                if(b->pow(j,1) != 0)
                    temp(1) += (complex<double>)b->pow(j,1)*gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1)-1,b->exponents(j),b->coord(j,1));
                temp(2) = gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));
                result(i,j) = complex<double>(0,1)*temp.prod();
            }
            else if(op=="Lz"){
                // operator Lz = -i(x d/dy - y d/dx)

                // x d/dy
                RowVector3cd temp1 = RowVector3cd(0,0,0);
                temp1(0) = gauss_1(1,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0));
                temp1(1) = -2*b->exponents(j)*gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1)+1,b->exponents(j),b->coord(j,1));
                if(b->pow(j,1) != 0)
                    temp1(1) += (complex<double>)b->pow(j,1)*gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1)-1,b->exponents(j),b->coord(j,1));
                temp1(2) = gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));

                // y d/dx
                RowVector3cd temp2 = RowVector3cd(0,0,0);
                temp2(0) = -2*b->exponents(j)*gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0)+1,b->exponents(j),b->coord(j,0));
                if(b->pow(j,0) != 0)
                    temp2(0) += (complex<double>)b->pow(j,0)*gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0)-1,b->exponents(j),b->coord(j,0));
                temp2(1) = gauss_1(1,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1));
                temp2(2) = gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));

                result(i,j) = complex<double>(0,-1)*(temp1.prod()-temp2.prod());
            }
            else if(op=="Lx"){
                // operator Lx = -i(y d/dz - z d/dy)

                // y d/dz
                RowVector3cd temp1 = RowVector3cd(0,0,0);
                temp1(0) = gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0));
                temp1(1) = gauss_1(1,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1));
                temp1(2) = -2*b->exponents(j)*gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2)+1,b->exponents(j),b->coord(j,2));
                if(b->pow(j,2) != 0)
                    temp1(2) += (complex<double>)b->pow(j,2)*gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2)-1,b->exponents(j),b->coord(j,2));

                // z d/dy
                RowVector3cd temp2 = RowVector3cd(0,0,0);
                temp2(0) = gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0));
                temp2(1) = -2*b->exponents(j)*gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1)+1,b->exponents(j),b->coord(j,1));
                if(b->pow(j,1) != 0)
                    temp2(1) += (complex<double>)b->pow(j,1)*gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1)-1,b->exponents(j),b->coord(j,1));
                temp2(2) = gauss_1(1,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));

                result(i,j) = complex<double>(0,-1)*(temp1.prod()-temp2.prod());
            }
            else if(op=="Ly"){
                // operator Lx = -i(z d/dx - x d/dz)

                // z d/dx
                RowVector3cd temp1 = RowVector3cd(0,0,0);
                temp1(0) = -2*b->exponents(j)*gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0)+1,b->exponents(j),b->coord(j,0));
                if(b->pow(j,0) != 0)
                    temp1(0) += (complex<double>)b->pow(j,0)*gauss_1(0,pow(i,0),exponents(i),coord(i,0),b->pow(j,0)-1,b->exponents(j),b->coord(j,0));
                temp1(1) = gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1));
                temp1(2) = gauss_1(1,pow(i,2),exponents(i),coord(i,2),b->pow(j,2),b->exponents(j),b->coord(j,2));

                // x d/dz
                RowVector3cd temp2 = RowVector3cd(0,0,0);
                temp2(0) = gauss_1(1,pow(i,0),exponents(i),coord(i,0),b->pow(j,0),b->exponents(j),b->coord(j,0));
                temp2(1) = gauss_1(0,pow(i,1),exponents(i),coord(i,1),b->pow(j,1),b->exponents(j),b->coord(j,1));
                temp2(2) = -2*b->exponents(j)*gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2)+1,b->exponents(j),b->coord(j,2));
                if(b->pow(j,2) != 0)
                    temp2(2) += (complex<double>)b->pow(j,2)*gauss_1(0,pow(i,2),exponents(i),coord(i,2),b->pow(j,2)-1,b->exponents(j),b->coord(j,2));

                result(i,j) = complex<double>(0,-1)*(temp1.prod()-temp2.prod());
            }
            else
                ABORT("operator not defined: "+tools::str(op));
        }
}

complex<double > primitive_gaussians::gauss_1(int n,int ii,double ai,double ri,int jj,double aj,double rj){

    // int dx x^n (x-ri)^ii (x-rj)^jj exp(-ai(x-ri)^2) exp(-aj(x-rj)^2)
    complex<double > val;
    VectorXd y,w;
    double shift,ir_aiaj,x;
    int nquad;
    val=0;
    nquad=(n+ii+jj+1)+1;
    shift=(ai*ri+aj*rj)/(ai+aj);
    ir_aiaj=1.0/sqrt(ai+aj);
    h_roots(nquad,y,w);

    for(int i=0;i<nquad;i++){
        x=y(i)*ir_aiaj+shift;
        val+=std::pow(x,n)*std::pow((x-ri),ii)*std::pow((x-rj),jj)*w(i)*std::exp(-(ai*aj*std::pow((ri-rj),2)/(ai+aj)));
    }
    val=val*ir_aiaj;

    return val;
}

complex<double > primitive_gaussians::gauss_k(int n,int ii,double ai,double ri,double kj,double bj){

    // int dx x^n (x-ri)^ii exp(-ai(x-ri)^2 + i x*kj)
    // the box size bj, where the integrations are truncated, is checked for consistency
    // mctdhf.pdf has formulae

    // check size of the plane wave box
    if(bj-abs(ri) < 5/std::pow(ai,0.5)){
        cout<<"plane wave box too small for correct integrals with gaussian"<<endl;
        exit(1);
    }

    // sum up the binomails for (x+ri)^n
    complex<double > val=0;
    for(int l=0;l<n+1;l++)
        val+=binomial(n,l)*std::pow(ri,n-l)*alglib::hermitecalculate(ii+l,kj/sqrt(4*ai))*std::pow((complex<double >(0,1)/sqrt(4*ai)),(ii+l));

    val=(val*exp(-std::pow((double)kj,2)/(4*ai))*sqrt(math::pi/ai)*exp(complex<double >(0,1)*kj*ri));

    return val;

}

primitive_gaussians::primitive_gaussians(columbus_data *col_data){
    if(not oldCode())ABORT("not OLD");
    this->type = "gaussian";
    col_data->read_primitive_gaussian_info(this->exponents,this->pow, this->coord);

    this->lmax = pow.col(0).maxCoeff();
    this->mmax = abs(lmax);
}

primitive_gaussians::primitive_gaussians(QuantumChemicalInput & ChemInp){
    type = "gaussian";
    //  col_data->read_primitive_gaussian_info(exponents,pow,coord);
    exponents=ChemInp.exponents;
    pow=ChemInp.pow;
    coord=ChemInp.coord;

    lmax = pow.col(0).maxCoeff();
    mmax = abs(lmax);
}
