#include "basisEigen.h"
#include "operatorTree.h"
#include "operatorDefinitionNew.h"
#include "index.h"
#include "eigenSolver.h"


BasisEigen::BasisEigen(std::string OperRef, int Size, int NLow)
    :BasisOrbital("Eigen"),_nLow(NLow){
    std::vector<std::string> part=tools::splitString(OperRef,':');
    if(part.size()!=2)ABORT("need format OperDef:RefSubset, got: "+OperRef);
    _operDef=part[0];
    _refName=part[1];
    _orb.resize(Size);
    if(_nLow<0)ABORT("eigenvalue numbering starts at 0, but found "+tools::str(_nLow));
}

BasisEigen::BasisEigen(const BasisSetDef &Def)
    :BasisEigen(tools::stringInBetween(Def.funcs,"[","]"),Def.order,Def.lowBound()){}
//    :BasisOrbital("Eigen"){
//    std::vector<std::string> part=tools::splitString(tools::stringInBetween(Def.funcs,"[","]"),':');
//    if(part.size()!=2)ABORT("need format Eigen[OperDef:RefSubset], got: "+Def.funcs);
//    _operDef=part[0];
//    _refName=part[1];
//    _nLow=Def.lowBound();
//    _orb.resize(Def.order);
//}

void BasisEigen::generateOrbitals(const Index *Idx){

    if(Idx==0)Idx=BasisOrbital::referenceIndex[_refName];
    if(Idx==0)DEVABORT("no index for reference "+_refName+
                       ", set BasisOrbital::referenceIndex[...] before constructing BasisEigen");

    OperatorTree op("forBasisEigen",OperatorDefinitionNew(_operDef,Idx->hierarchy()),Idx,Idx);
    EigenSolver slv(-DBL_MAX,DBL_MAX,size());
    slv.compute(&op);
    slv.select("SmallReal["+tools::str(size()+_nLow)+"]");
    slv.orthonormalize(slv.dualVectors(),slv.rightVectors());

    _eigenValues.clear();
    for(int k=_nLow;k<slv.eigenvalues().size();k++){
        _eigenValues.push_back(slv.eigenvalues()[k]);
        _orb[k-_nLow]=*slv.rightVectors()[k];
    }
}
