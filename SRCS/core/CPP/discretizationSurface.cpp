// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply.
//
// See terms of use in the LICENSE file included with the source distribution
//
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "discretizationSurface.h"

#include "threads.h"
#include "folder.h"
#include "algebra.h"
#include "index.h"
#include "indexSurface.h"
#include "operatorTensor.h"
#include "pulse.h"
#include "mapGauge.h"
#include "discretizationHybrid.h"
#include "discretizationSurfaceHybrid.h"
#include "discretizationtsurffspectra.h"
#include "axisTree.h"
#include "operatorFloor.h"
#include "basisGrid.h"
#include "eigenTools.h"
#include "parallelOperator.h"

using namespace std;

string DiscretizationSurface::prefix="surface_";
string DiscretizationSurface::prefixAx="surf"; // NOTE: this is hard-coded in many places, keep "surf" for now

std::vector<std::string>  DiscretizationSurface::surfaceFiles(std::string RunDir, const Index * Idx, std::string Region){

    for(std::string coor: tools::splitString(Region,'.')){
        const Index* idx=Idx->axisIndex(coor);
        if(idx->axisIndex(coor)==0)
            ABORT("axis "+coor+" is not an unbound axis in "+Idx->hierarchy());
    }


std:string src=RunDir;
    if(Region.find('.')==std::string::npos)
        src+=DiscretizationSurface::prefix+Region;
    else
        src+="S_"+Region.substr(0,Region.find('.'))+"/"+DiscretizationSurface::prefix+Region.substr(Region.rfind('.')+1);
    if(not folder::exists(src))
        ABORT("Could not find surface file "+src+" needed as source for subregion "+Region
              +", first run with -region="+Region.substr(0,Region.find('.')));

    return std::vector<std::string>(1,src);
}


DiscretizationSurface * DiscretizationSurface::factory(const Discretization *Parent, const std::vector<double> &Rad, unsigned int Nsurf){
    const DiscretizationHybrid * hyb=dynamic_cast<const DiscretizationHybrid*>(Parent);
    if(hyb!=0)
        return new DiscretizationSurfaceHybrid(hyb,Rad,Nsurf);
    else
        return new DiscretizationSurface(Parent,Rad,Nsurf);
}

// return the NSurf'th continuity index
const Index* getContinuityLevel(const Index* Idx, int NSurf){

    const Index* idx=Idx;
    while(not idx->isLeaf()){
        //HACK for hybrid: second part contains continuity
        if(idx->axisName().find("&")!=string::npos)idx=idx->child(1);

        const Index* idxFloor=idx->descend()->axisIndex(idx->axisName()); // find matching axis name
        if(idx->basisAbstract()->isIndex() and idxFloor!=0 and idxFloor->basisIntegrable())
        {
            if(NSurf==0)return idx;
            NSurf--;
        }
        idx=idx->descend();
    }
    //    ABORT(Sstr+"no continuity level"+NSurf+"in index\n"+Idx->str());
    return 0; // to shut up some over-ambitious compilers (CLANG)
}

DiscretizationSurface::DiscretizationSurface(const Discretization *Parent, const std::vector<double> &Rad, int NSurf, string DerivativeSide)
{
    if(Rad.size()!=1)
        ABORT(Str("specify exactly 1 surface radius, found")+Rad.size()+"at"+Rad);

    // create new hierarchy and continuitLevel: add value/derivative index just above floor
    parent = dynamic_cast<const Discretization *>(Parent);
    name = Parent->name;
    // get the name of the NSurf'th continuity axis

    std::size_t found;
    const Index* surfI=getContinuityLevel(Parent->idx(),NSurf);
    found = name.find(surfI->axisName());
    name.insert(found+surfI->axisName().length(),"_surf");

    if(Rad.size()==0)ABORT("specify at least one surface radius");
    wIdx = 0;

    // construct global index
    idx() = new IndexS(Parent->idx(),Rad,NSurf,DerivativeSide);

    _mapFromParent.reset(new MapSurface(idx(),Parent->idx()));


    ParallelOperator::setDistribution(mapFromParent());

    // mixed gauge: apply back-gauge transform before write
    if(Algebra::isAlgebra("Rg") and Algebra("Rg").val(0.).real()!=0){
        std::shared_ptr<OperatorAbstract> m(new MapGauge(this));
        _mapFromParent=m;
    }
}

DiscretizationSurface::DiscretizationSurface(const Discretization *Parent, const std::vector<double> &Rad, const std::string SurfaceFile, string DerivativeSide)
{
    if(Rad.size()!=1)
        ABORT(Str("specify exactly 1 surface radius, found")+Rad.size()+"at"+Rad);

    // create new hierarchy and continuitLevel: add value/derivative index just above floor
    parent = dynamic_cast<const Discretization *>(Parent);
    name = Parent->name;
    std::string surfCoor=SurfaceFile.substr(SurfaceFile.rfind("_")+1);
    name.insert(name.find(surfCoor)+surfCoor.length(),"_surf");

    if(Rad.size()==0)ABORT("specify at least one surface radius");
    ifstream surfStream(SurfaceFile.c_str(), ios::in|ios::binary);
    idx() = new Index(surfStream);

    _mapFromParent.reset(new MapSurface(idx(),Parent->idx()));
    ParallelOperator::setDistribution(mapFromParent());

    // mixed gauge: apply back-gauge transform before write
    if(Algebra::isAlgebra("Rg") and Algebra("Rg").val(0.).real()!=0){
        std::shared_ptr<OperatorAbstract> m(new MapGauge(this));
        _mapFromParent=m;
    }
}

DiscretizationSurface::Map::Map(const Index *SurfI, const Index *FromI):viewI(0){

    name=SurfI->parent()!=0
            ? "mapToSurface"
            : prefix+SurfI->findAxisStarts(prefixAx)->axisName().substr(prefixAx.length());

    iIndex=SurfI;
    jIndex=FromI;
    if(FromI->hasFloor()){
        // set a view with floor level below ValDer.. level
        const Index*s = SurfI;
        for(; not s->isLeaf(); s=s->child(0))
            if(s->axisName().find("ValDer")!=string::npos) break;

        viewI=new Index(0,FromI);
        viewI->resetFloor(s->depthInFloor()+1);
        temp=new Coefficients(FromI);
        viewTemp=new Coefficients(viewI,temp);


        viewSurfI=new Index(0,SurfI);
        viewSurfI->resetFloor(s->depthInFloor()+1);
        tempSurf=new Coefficients(SurfI);
        viewTempSurf=new Coefficients(viewSurfI,tempSurf);
    }

    if(SurfI->axisName().find(prefixAx)==0){
        // descend further
        for(unsigned int k=0;k<SurfI->childSize();k++){
            elem.push_back((int)SurfI->basisGrid()->mesh()[k]);
            childAdd(new Map(SurfI->child(k),FromI->child(elem[k])));
        }
    }
    else if (SurfI->axisName().substr(0,6)=="ValDer"){
        FromI->basisIntegrable()->valDerD(SurfI->basisGrid()->mesh(),val,der);
        UseMatrix valder = UseMatrix::Zero(2,FromI->basisAbstract()->size());
        for(unsigned int k=0;k<val.size()/2;k++){

            //HACK true radial derivative here - this should be changed for consistency?
            if(FromI->axisName().find("Rn")!=string::npos)
                der[k]-=val[k]/SurfI->basisGrid()->mesh()[0];
            //HACK very ugly - surface part seems to be adapted to this?
            else if(FromI->axisName().find_first_of("XYZ")!=string::npos)
                der[k]-=val[k]/SurfI->basisGrid()->mesh()[0];

            valder(0,k) = val[k];
            valder(1,k) = der[k];
        }
        vector<UseMatrix> mats; mats.push_back(valder);
        o.push_back(new OperatorTensor("valder",SurfI,FromI,mats));
    }
    else {
        for(unsigned int k=0,kOrig=0;k<SurfI->childSize();k++,kOrig++){
            // hybrid branches may have been dropped
            while(SurfI->child(k)->axisName().find("surf")!=0
                  and SurfI->child(k)->axisName().find("ValDer")!=0
                  and FromI->child(kOrig)->axisName()!=SurfI->child(k)->axisName())kOrig++;
            O.push_back(new Map(SurfI->child(k),FromI->child(kOrig)));
        }
    }

}

DiscretizationSurface::MapSurface::MapSurface(const Index *SurfI, const Index *FromI)
    :OperatorTree("mapTo"+SurfI->hierarchy(),SurfI,FromI)
{
    // set top level name that is used as surface file name
    if(SurfI->parent()==0)
        name=prefix+SurfI->findAxisStarts(prefixAx)->axisName().substr(prefixAx.length());

    if(FromI->hasFloor()){
        // one of the coordinates of Floor is converted to surface
        std::vector<const Eigen::MatrixXcd*> pMats;
        Eigen::MatrixXcd * pmat;
        for(const Index*from=FromI,*surf=SurfI;;from=from->descend(),surf=surf->descend()){
            if(not from->subEquivalent())
                DEVABORT("surfaces only for floors with tensor product structure, is:\n"+FromI->str());

            // tensor product of identity operators and conversion to value and derivative
            if(surf->axisName().substr(0,6)=="ValDer"){
                std::vector<std::complex<double> > val,der;
                if(surf->basisGrid()->mesh().size()!=2)DEVABORT(Sstr+"not 2 grid points at ValDer:"+surf->basisGrid()->mesh());
                from->basisIntegrable()->valDerD({surf->basisGrid()->mesh()[0]},val,der);

                if(from->basisIntegrable()->eta()!=1.)
                    ABORT(Sstr+"Illegal: surface"+surf->basisGrid()->mesh()[0]+"in complex scaled region, basis"+from->basisAbstract()->str());
                pmat=new Eigen::MatrixXcd(2,val.size());
                for(int k=0;k<from->basisAbstract()->size();k++){
                    //HACK true radial derivative here - this should be changed for consistency, ugly HACK?? for XYZ
                    if(FromI->axisName().find("Rn")!=string::npos or FromI->axisName().find_first_of("XYZ")!=string::npos)
                        der[k]-=val[k]/surf->basisGrid()->mesh()[0];
                    pmat->operator()(0,k)=val[k];
                    pmat->operator()(1,k)=der[k];
                }
            } else {
                pmat=new Eigen::MatrixXcd(surf->basisAbstract()->size(),surf->basisAbstract()->size());
                *pmat=Eigen::MatrixXcd::Identity(surf->basisAbstract()->size(),surf->basisAbstract()->size());
            }
            pMats.push_back(pmat);

            if(from->isBottom())break;
        }
        floor()=OperatorFloor::factory(pMats,"toSurf"+FromI->hash());
        for(auto m: pMats)delete m;
    }
    else if(SurfI->axisName().find(prefixAx)==0){
        // FE level of surface and matching branch of SurfI
        for(int k=0;k<SurfI->childSize();k++)
            childAdd(new MapSurface(SurfI->child(k),FromI->child(int(SurfI->basisGrid()->mesh()[k]))));
    } else {
        // identity on all other axes, except for hybrid where terms will have been omitted
        for(unsigned int k=0,kOrig=0;k<SurfI->childSize();k++,kOrig++){
            //HACK for hybrid axes
            while(FromI->axisName().find("&")!=string::npos and FromI->child(kOrig)->axisName()!=SurfI->child(k)->axisName())kOrig++;
            childAdd(new MapSurface(SurfI->child(k),FromI->child(kOrig)));
        }
    }
}

void DiscretizationSurface::Map::apply(std::complex<double> Alfa, const Coefficients &X, std::complex<double> Beta, Coefficients &Y) const {

    Coefficients * viewX=const_cast<Coefficients*>(&X);
    Coefficients * viewY=const_cast<Coefficients*>(&Y);
    if(viewI!=0){
        // in most cases, a viewX into the floor of X will be needed
        viewX=const_cast<Map*>(this)->viewTemp;
        *temp=X;

        viewY=const_cast<Map*>(this)->viewTempSurf;
        *tempSurf=Y;
    }

    if(elem.size()>0){
        // different surfaces
        for(unsigned int k=0;k<viewY->childSize();k++)O[k]->apply(Alfa,*viewX->child(elem[k]),Beta,*viewY->child(k));
    }
    else if(val.size()>0){
        // below v/d just copy
        *viewY*=Beta;
        for(unsigned int k=0;k<viewX->childSize();k++){
            viewY->child(0)->axpy(Alfa*val[k],*(viewX->child(k)),1.);
            viewY->child(1)->axpy(Alfa*der[k],*(viewX->child(k)),1.);
        }
    }
    else {
        // descend further
        for(unsigned int k=0;k<viewY->childSize();k++)O[k]->apply(Alfa,*viewX->child(k),Beta,*viewY->child(k));
    }
    if(viewI!=0){  // copy back
        Y = *tempSurf;
    }
}

// Algorithm:
// duplicate structure in From index until the NSurf'th continuity level is met
// attach a copy of From branch that contains the surface
// replace the node at the continuity level for coordinate C with surfC (grid, at present limited to single point)
// replace the basis covering the surface coordinate C with ValeDerC consiting of value and derivative at the surface point
DiscretizationSurface::IndexS::IndexS(const Index *From, std::vector<double> Radius, unsigned int NSurf, string DerivativeSide)
{
    //    if(From->isLeaf())ABORT(From->root()->str()+"\n\nNo unbounded axis found in Index");

    Index::nodeCopy(From,false); // true copy, not view
    fromIndex.push_back(From);

    const Index* fIndex=getContinuityLevel(From,0);
    if(fIndex!=From or NSurf>0){
        // not the continuity level for desired surface - descend
        if(fIndex==From)NSurf--; // found continuity - reduce surface count

        //HACK hybrid axis
        int kMin=From->axisName().find("&")!=string::npos ? 1 : 0;

        for(unsigned int k=kMin;k<From->childSize();k++)
            childAdd(new IndexS(From->child(k),Radius,NSurf));
    }

    else
    {
        // indicate surface level
        setAxisName(prefixAx+From->axisName());
        fromIndex.push_back(From);
        vector<unsigned int> elemN;
        for(unsigned int k=0;k<Radius.size();k++)
        {
            // loop through sections below From (to locate Radius)
            const Index* elem=From->descend();
            for(;elem!=0;elem=elem->rightSibling()){
                Index* elemFunc=elem->axisIndex(From->axisName());
                double lB=elemFunc->basisIntegrable()->lowBound(),uB=elemFunc->basisIntegrable()->upBound();
                double eps=min((uB/2-lB/2)*1.e-12,1.e-10); // eps is tricky, as boundaries may be at +-infty;

                // locate inside element; if on boundary
                // derivatives are taken from the side closer to zero, unless specified explicitly at boundary
                bool selectThis=false;
                if(DerivativeSide=="boundaryFromBelow")
                    selectThis=abs(uB-Radius[k])<eps;
                else if (DerivativeSide=="boundaryFromAbove")
                    selectThis=abs(lB-Radius[k])<eps;
                else if (DerivativeSide=="fromInside")
                    selectThis=
                            (lB>=0 and lB< Radius[k]     and Radius[k]<=uB+eps) or
                            (lB< 0 and lB<=Radius[k]+eps and Radius[k]< uB);
                else
                    DEVABORT("illegal value of DerivativeSide="+DerivativeSide
                             +", allowed are boundaryFromBelow,boundaryFromAbove,fromInside(default)");

                if(selectThis){
                    // attach copy of tree that contains surface
                    for(unsigned int l=childSize();l>0;l--)childErase(l-1);
                    elemN.push_back(elem->nSibling());
                    childAdd(new Index(this,From->child(elemN.back())));

                    // insert v/d at matching level
                    for(Index *vd=childBack()->axisIndex(From->axisName());vd!=0;vd=vd->nodeRight(),elemFunc=elemFunc->nodeRight()){
                        // erase function level
                        for(int l=vd->childSize();l>0;l--)vd->childErase(l-1);
                        // insert v/d level
                        // NOTE: we need all possible levels below to be equivalent
                        if(elemFunc->descend())for(int l=0;l<2;l++)vd->childAdd(new Index(vd,elemFunc->descend()));
                        else                   {vd->leafAdd();vd->leafAdd();} // for now, surface indes DOES carry dummies
                        vd->setBasis(BasisGrid::factory(std::vector<double>(2,Radius[k])));
                        vd->setAxisName("ValDer"+From->axisName());
                    }
                    break;
                }
            }
            if(elem==0){
                if(DerivativeSide.substr(0,8)=="boundary")
                    ABORT(Str("surface does not coincide with any element boundary:")+Radius[k]);
                else
                    ABORT(Str("surface outside all elements:")+Radius[k]);
            }
        }
        // basis on surf-level is a grid of element numbers
        std::vector<double> el;
        for(unsigned int n: elemN)el.push_back(double(n));
        setBasis(BasisGrid::factory(el));
        //        }
    }
    sizeCompute();
}
