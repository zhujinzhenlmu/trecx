// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "derivativeBlock.h"

#include "coefficients.h"
#include "useMatrix.h"
#include "operatorTree.h"
#include "operatorFloor.h"
#include "str.h"

using namespace std;


DerivativeBlock::DerivativeBlock(std::vector<unsigned int> BlockSort,
                                 const OperatorTree* OLeaf, Coefficients * ICoef, Coefficients*JCoef, double ONorm, double *XNorm)
    :oLeaf(OLeaf),xNorm(XNorm),blockSort(BlockSort){

    eps=ONorm;
    cInOut.push_back(JCoef);
    cInOut.push_back(ICoef);

    // distribute the send/receive info
    // this should be replaced by ParallelOperator
    vector<complex<double> > buf;
}

double DerivativeBlock::load() const{return oLeaf->floor()->applicationCost(false);}

bool DerivativeBlock::lessEqual(const DerivativeBlock &A, const DerivativeBlock &B){
    for(unsigned int k=0;k<A.blockSort.size();k++)
        if(A.blockSort[k]!=B.blockSort[k])
            return A.blockSort[k]<B.blockSort[k];
    return &A != &B;
}

bool DerivativeBlock::less(const DerivativeBlock &A, const DerivativeBlock &B){
    for(unsigned int k=0;k<A.blockSort.size();k++)
        return A.blockSort[k]<B.blockSort[k];
    return true;
}
