// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "haccInverse.h"

#include <string>

#include "mpiWrapper.h"

#include "coefficients.h"
#include "coefficientsGlobal.h"
#include "mo.h"
#include "operator.h" // rather deeply rooted - major change
#include "operatorZG.h"
#include "discretizationHaCC.h"
#include "index.h"
#include "useMatrix.h"
#include "operatorTree.h"
#include "operatorView.h"
#include "inverseDvr.h"
#include "printOutput.h"
#include "Eigen/SVD"
#include "eigenSolver.h"
#include "diagnose.h"
#include "operatorAbstract.h"

#include "parallel.h"

// there is a name conflict with  EigenSolver - rename
typedef EigenSolver tRecX_EigenSolver;
using namespace std;
#include "eigenNames.h"


double HaccInverse::zeroThreshold=3.e-2;
double HaccInverse::rhoThreshold=1.e-10;

static double tolerance=1.e-10; // tolerance for the tests

static std::unique_ptr<CoefficientsGlobal> gY, gX;

HaccInverse::~HaccInverse(){
    delete qVec;
    delete s0InvU;
    delete zeroC;
    delete uAdj;
    delete dAdjS0;
}

HaccInverse::HaccInverse(const DiscretizationHaCC *Hacc)
    :Inverse(new InverseDVR(Hacc->idx())),hacc(Hacc),zeroC(0),dAdjS0(0)
{

    // get the (non-singular) natural orbitals
    // NOTE: Rho contains dyson coeffs with neutral in last rows/cols - ignore
    MatrixXd  bcorr(hacc->ionBf->Rho.block(0,0,hacc->mos()*hacc->ions(),hacc->mos()*hacc->ions()));
    VectorXd natPop;
    corrNatOrb(bcorr,natOrb,natPop);

    // zero eigenvectors of S
    vector<Coefficients*> vC,vDs0;
    getZeroD(vC,vDs0,natPop.size());
    PrintOutput::DEVmessage("number of zero-vectors: "+tools::str(vC.size()));

    // operators on top level
    uAdj  =new Operator(0,hacc->idx(),0,"uAdj");
    s0InvU=new Operator(hacc->idx(),0,0,"s0InvU");
    if(vC.size()>0){
        zeroC=new Operator(hacc->idx(),0,0,"zeroD");
        dAdjS0=new Operator(0,hacc->idx(),0,"dAdjS0");
    }

    // get C, D^H S0, S0^-1 U, U^H (for the natural orbitals)
    getUD(hacc->idx(),natOrb,vDs0,vC,zeroC,dAdjS0,s0InvU,uAdj);

    if(hacc->anti_sym)
        // get Zinv = [N^H U^H  Q S0^-1 U N]^-1 (and make U continuous), N^H U^H C
        getZandF(natOrb,natPop,zInv,nUadjC);

    // create auxilary vectors and make sure storage is contiguous
    qVec = new Coefficients(uAdj->iIndex);
    qVec->treeOrderStorage();
    if(zeroC!=0){
        dVec = new Coefficients(dAdjS0->iIndex);
        dVec->treeOrderStorage();
        ndVec=new vector<complex<double> >(natOrb.cols()+dVec->idx()->sizeCompute());
    }
    else {
        ndVec= new vector<complex<double> >(natOrb.cols());
    }

    // for the spectral projector, need to use full overlap, supplement zeros with dummy
    _ovrNonSingular=new OvrNonSingular(Hacc->idx()->overlap(),this);

//    if(dAdjS0!=0){
//        // test S1
//        Coefficients c(hacc->s0->iIndex);
//        Coefficients d(c);
//        c.setToRandom();
//        c.makeContinuous();
//        project(c);

//        ovr1(1.,c,0.,d);
//        inv1(1.,d,-1.,c);
//        if(c.norm()>1.e-10){
//            cout<<c.str()<<endl;
//            ABORT("S1inv*S1 failed");
//        }

//        c.setToRandom();
//        c.makeContinuous();
//        projectDual(c);

//        inv1(1.,c,0.,d);
//        ovr1(1.,d,-1.,c);
//        if(c.norm()>1.e-10){
//            cout<<c.str()<<endl;
//            PrintOutput::DEVwarning("S1*S1inv failed");
//        }
//        else PrintOutput::DEVmessage("S1 S1inv OK");
//    }
    analyze();
}


void HaccInverse::apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    //Coefficients x(Vec);
    apply0(A,Vec,B,Y);
    applyCorrection(&Y);
}

void HaccInverse::corrNatOrb(const  MatrixXd &Rho,  MatrixXd &NatOrb,  VectorXd &NatPop){
    Eigen::SelfAdjointEigenSolver< MatrixXd> slv;
    slv.compute(Rho);
    for(int n=0;n<slv.eigenvalues().rows();n++){
        if(abs(slv.eigenvalues()(n))>rhoThreshold){
            NatOrb.conservativeResize(Rho.rows(),NatOrb.cols()+1);
            NatPop.conservativeResize(NatOrb.cols());
            NatOrb.rightCols(1)=slv.eigenvectors().col(n);
            NatPop(NatOrb.cols()-1)=slv.eigenvalues()(n);
        }
    }
}

void HaccInverse::getZandF(MatrixXd & NatOrb, VectorXd &NatPop, MatrixXcd & Zinv, MatrixXcd &NUadjC){

    // get U^H S0^-1 U (need continuity!)
    Coefficients d(s0InvU->iIndex);
    Coefficients e(s0InvU->iIndex);
    Coefficients kMO1(s0InvU->jIndex);
    Coefficients kMO2(s0InvU->jIndex);
    kMO1.treeOrderStorage();
    kMO2.treeOrderStorage();
    d.treeOrderStorage();
    e.treeOrderStorage();

    OperatorView uView(*s0InvU);
    if(s0InvU->jIndex->axisName()!="Ion")ABORT("for now, ion axis must be on top");

    // loop through columns of S0^-1 U
    MatrixXcd mZ(uAdj->iIndex->sizeCompute(),s0InvU->jIndex->sizeCompute());

    for (int j=0;j<mZ.cols();j++){

        uView.getColumn(j,d);
        //d.makeContinuous();
        hacc->s0Inv->apply(1.,d,0.,e);

        //e.makeContinuous();
        uView.insertColumn(j,e);
        uAdj->apply(1.,e,0.,kMO1);
        mZ.col(j) = Eigen::Map< MatrixXcd>(kMO1.data(),kMO1.size(),1);
        kMO1.makeContinuous();

        if(zeroC != 0) {
            Coefficients f(dAdjS0->iIndex);
            Coefficients g(zeroC->iIndex);
            f.treeOrderStorage();
            g.treeOrderStorage();

            dAdjS0->apply(1., e, 0., f);
            //f.makeContinuous();
            zeroC->apply(1., f, 0., g);
            //g.makeContinuous();
            uAdj->apply(1.,g,0.,kMO2);
            kMO2.makeContinuous();
            mZ.col(j) -= Eigen::Map< MatrixXcd>(kMO2.data(),kMO2.size(),1);
        }

    }
    Zinv=NatOrb.adjoint()*mZ*NatOrb;// transform to natural orbitals

    for(int k=0;k<NatPop.size();k++)Zinv(k,k)-=1./NatPop(k);

    Eigen::SelfAdjointEigenSolver<MatrixXcd> slv(Zinv);
    VectorXd ev=slv.eigenvalues();

    // get non-zeros
    for(int k=0;k<ev.size();k++)
        if(abs(ev(k))>1.e-3){
            vNonz.conservativeResize(ev.size(),vNonz.cols()+1);
            vNonz.col(vNonz.cols()-1)=slv.eigenvectors().col(k);
        }

    // get matrix on non-zero subspace
    MatrixXcd mNonz,iNonz;
    mNonz=vNonz.adjoint()*Zinv*vNonz;
    iNonz=mNonz.inverse();
    Zinv=vNonz*iNonz*vNonz.adjoint();

    Eigen::SelfAdjointEigenSolver<MatrixXcd> slv1(Zinv);
    VectorXd ev1=slv1.eigenvalues();

    if(zeroC!=0){
        Coefficients b(zeroC->jIndex);
        Coefficients c(zeroC->iIndex);
        b.treeOrderStorage();
        c.treeOrderStorage();

        MatrixXcd mUadjC(uAdj->iIndex->sizeCompute(),zeroC->jIndex->sizeCompute());
        for (int j=0;j<mUadjC.cols();j++){
            b.setToZero();
            b.data()[j]=1.;
            zeroC->apply(1.,b,0.,c);
            c.makeContinuous();
            uAdj->apply(1.,c,0.,kMO1);
            mUadjC.col(j)= Eigen::Map< MatrixXcd>(kMO1.data(),kMO1.size(),1);
        }
        NUadjC=NatOrb.adjoint()*mUadjC; // transform to natural orbitals
    }
}

void HaccInverse::applyCorrection(std::complex<double> A, const CoefficientsLocal &Vec, std::complex<double> B, CoefficientsLocal &Y) const{
   // applyCorrection(A,*dynamic_cast<const Coefficients*>(&Vec),B,*dynamic_cast<Coefficients*>(&Y));
    Coefficients* result;
    const Coefficients* X;
    if(MPIwrapper::Size() > 1) {
        Parallel::gather(gX.get(), const_cast<CoefficientsLocal*>(&Vec), MPIwrapper::master());
        Parallel::gather(gY.get(), const_cast<CoefficientsLocal*>(&Y), MPIwrapper::master());
        X = gX.get();
        result = gY.get();
    }
    else {
        X=dynamic_cast<const Coefficients*>(&Vec);
        result = dynamic_cast<Coefficients*>(&Y);
    }
    if(MPIwrapper::isMaster())
        applyCorrection(A,*X,B,*result);
    if(MPIwrapper::Size() > 1) {
        Parallel::scatter(gY.get(), &Y, MPIwrapper::master());
    }
}

void HaccInverse::apply0(std::complex<double> A, const CoefficientsLocal &Vec, std::complex<double> B, CoefficientsLocal &Y) const{
    Coefficients* result;
    Coefficients* X;
    if(MPIwrapper::Size() > 1) {
        Parallel::gather(gX.get(), const_cast<CoefficientsLocal*>(&Vec), MPIwrapper::master());
        Parallel::gather(gY.get(), const_cast<CoefficientsLocal*>(&Y), MPIwrapper::master());
        X = gX.get();
        result = gY.get();
    }
    else {
        X = const_cast<Coefficients*>(dynamic_cast<const Coefficients*>(&Vec));
        result = dynamic_cast<Coefficients*>(&Y);
    }
    if(MPIwrapper::isMaster())
        inv1(A,*X,B,*result);
    if(MPIwrapper::Size() > 1) {
        Parallel::scatter(gY.get(), &Y, MPIwrapper::master());
    }
}

void HaccInverse::apply0(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const
{
    *hacc->s0Inv->tempRHS()=Vec;
    inv1(A,*hacc->s0Inv->tempRHS(),B,Y);
}

void HaccInverse::applyCorrection(Coefficients *Avec) const {
    if(!hacc->anti_sym) return;
    // q = (U^H) Avec
    //Avec->makeContinuous(); // uAdj may not be continuous
    uAdj->apply( 1.,*Avec,0.,*qVec);

    // write natural orbitals into qdVec = (q,d) (contiguous storage)
    int qSize=qVec->idx()->sizeStored(),nSize=natOrb.cols(),dSize=0;
    ndVec->assign(ndVec->size(),0.);
    Eigen::Map< VectorXcd>(ndVec->data(),nSize)=natOrb.adjoint()*Map<VectorXcd>(qVec->data(),qSize);
    if(zeroC!=0){
        dAdjS0->apply(1.,*Avec,0.,*dVec);
        dSize=dVec->idx()->sizeStored();
        Eigen::Map< VectorXcd>(ndVec->data()+nSize,dSize)=Map<VectorXcd>(dVec->data(),dSize);
    }

    // Parallel: sum ndVec contributions from all nodes
    /*for(std::size_t n = 0; n < ndVec->size(); ++n) {
        std::complex<double> c;
        MPIwrapper::AllreduceSUM((*ndVec)[n], c);
        (*ndVec)[n] = c;
    }*/

    // q-= F d (natural orbs)
    if(zeroC!=0)Eigen::Map< VectorXcd>(ndVec->data(),nSize)-= nUadjC*Eigen::Map< VectorXcd>(ndVec->data()+nSize,dSize);

    // q = Z^-1 q (convert back to standard MOs)
    Eigen::Map< VectorXcd>(qVec->data(),qSize)=natOrb*zInv* Eigen::Map< VectorXcd>(ndVec->data(),nSize);

    // subtract corrections S^-1 c = (Avec -= S0^-1 U q + C d )
    s0InvU->apply(-1.,*qVec,1.,*Avec);
    /*if(zeroC!=0){
        for(int k=0;k<dSize;k++)dVec->data()[k]=ndVec->data()[nSize+k];
        zeroC->apply(-1.,*dVec,1.,*Avec);
    }*/
    project(*Avec);
}

// recursively construct operators
void HaccInverse::getUD(const Index* Idx, const  MatrixXd &NatOrb,
                        const std::vector<Coefficients *> DualD,const std::vector<Coefficients *> VecsD,
                        Operator *ZeroD, Operator *DAdjS0,
                        Operator *CorrU, Operator *UAdj){

    // top level - initialize
    if(Idx==hacc->idx()){

        // product basis Ion x NaturalOrbital indices
        vector<const BasisAbstract*>bases(1,BasisSet::getDummy(hacc->ions()));
        vector<string> names(1,string("Ion"));
        bases.push_back(BasisSet::getDummy(hacc->mos()));
        names.push_back(string("MolOrb"));

        // U and U^H
        CorrU->jIndex=new Index(bases,names);
        UAdj->iIndex=CorrU->jIndex;

        if(VecsD.size()>0){
            // zero vector index
            bases.assign(1,BasisSet::getDummy(VecsD.size()));
            names.assign(1,string("SingVecs"));

            // D and D^H S0
            ZeroD->jIndex=new Index(bases,names);
            DAdjS0->iIndex=ZeroD->jIndex;
        }
    }

    // OperatorFloor level
    if(Idx->hasFloor()){

        if(not Idx->basisIntegrable()->name().find("pol"))ABORT("only implementd for \"pol..\" type basis");

        // get ion index
        const Index * iIon=Idx;
        while(iIon->axisName()!="Ion")iIon=iIon->parent();

        // put overlap matrix into operator storage
        vector<int> kdx(hacc->getIndices(Idx)); // returns Eta.Phi.Ion.Rn sorted indices
        MatrixXcd mUAdj;
        hacc->MO->matrix_1e(mUAdj,"1",kdx[3],kdx[0],kdx[1]); // is <iMO|kBas> (column-wise)

        // floor of U^H
        UseMatrix aU;
        aU=UseMatrix::UseMap(mUAdj.data(),mUAdj.rows(),mUAdj.cols());
        UAdj->oFloor=new OperatorZG(&aU,UAdj->name);

        // floor of U <kBas|iMO>
        MatrixXcd mU=mUAdj.adjoint();
        UseMatrix pU=UseMatrix::UseMap(mU.data(),mU.rows(),mU.cols());
        CorrU->oFloor=new OperatorZG(&pU,CorrU->name);

        if(VecsD.size()!=0){
            // contiguous storage of vD floors
            vector<complex<double> > allD,duaD;
            for(int k=0;k<VecsD.size();k++){
                allD.insert(allD.end(),VecsD[k]->data(),VecsD[k]->data()+VecsD[k]->size());
                duaD.insert(duaD.end(),DualD[k]->data(),DualD[k]->data()+DualD[k]->size());
            }

            // floor of D
            UseMatrix pD,dD,pDadj;
            pD=UseMatrix::UseMap(allD.data(),ZeroD->iIndex->sizeCompute(),VecsD.size());
            dD=UseMatrix::UseMap(duaD.data(),ZeroD->iIndex->sizeCompute(),VecsD.size());
            pDadj=dD.transpose();

            pD.purge();
            pDadj.purge();
            ZeroD->oFloor=new OperatorZG(&pD,ZeroD->name);
            DAdjS0->oFloor=new OperatorZG(&pDadj,DAdjS0->name);
        }

    }

    // descend further
    else
    {
        Index*uIndex=const_cast<Index*>(CorrU->jIndex);
        for(int n=0;n<Idx->childSize();n++){
            // descending below Ion level - use next level index
            if(Idx->axisName()=="Ion")
                uIndex=const_cast<Index*>(CorrU->jIndex->child(n));

            CorrU->childAdd(new Operator(Idx->child(n),uIndex,0,CorrU->name));
            UAdj->childAdd( new Operator(uIndex,Idx->child(n),0, UAdj->name));

            if(VecsD.size()!=0){
                // D-vectors a next lower level
                vector<Coefficients*> vecsD,dualD;
                for(int k=0;k<VecsD.size();k++){
                    vecsD.push_back(VecsD[k]->child(n));
                    dualD.push_back(DualD[k]->child(n));
                }
                ZeroD->childAdd( new Operator(Idx->child(n),ZeroD->jIndex,0,ZeroD->name));
                DAdjS0->childAdd(new Operator(ZeroD->jIndex,Idx->child(n),0,DAdjS0->name));
                getUD(Idx->child(n),NatOrb,dualD,vecsD,ZeroD->child(n),DAdjS0->child(n),CorrU->child(n),UAdj->child(n));
            }
            else {
                getUD(Idx->child(n),NatOrb,vector<Coefficients*>(),vector<Coefficients*>(),0,0,CorrU->child(n),UAdj->child(n));
            }
        }
    }

}

void HaccInverse::project(Coefficients &Vec) const{
    if(zeroC!=0){
        zeroC->tempRHS()->makeContinuous();
        dAdjS0->apply(1.,Vec,0.,*zeroC->tempRHS());
        zeroC->apply(-1.,*zeroC->tempRHS(),1.,Vec);
    }
    Vec.makeContinuous();
}

void HaccInverse::projectDual(Coefficients &Vec) const{
    if(zeroC==0)return;
    hacc->s0Inv->apply(1.,Vec,0.,*hacc->s0Inv->tempLHS());
    hacc->s0Inv->tempLHS()->makeContinuous();
    project(*hacc->s0Inv->tempLHS());
    hacc->s0->apply(1.,*hacc->s0Inv->tempLHS(),0.,Vec);
    Vec.makeContinuous();
}

void HaccInverse::getZeroD(std::vector<Coefficients*> &VecsD, std::vector<Coefficients*> &DualD, int MaxD) const{

    if(DualD.size()>0)ABORT("enter with zero dual vector");

    int nvec = std::min(100, (int) hacc->idx()->sizeCompute());
    tRecX_EigenSolver slv(-1.,zeroThreshold,nvec,true,true,false,"auto");
    slv.parallel(false);
    slv.compute(hacc->idx()->overlap(),hacc->s0);
    if(slv.eigenvalues().size() == 0) return;
    slv.verify();
    VecsD=slv.rightVectors();
    DualD=slv.dualVectors();

}

void HaccInverse::OvrNonSingular::apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    *tempRHS()=Vec;
    _inv->project(*tempRHS());
    _ovr->apply(A,*tempRHS(),B,Y);
    _inv->projectDual(Y);
    *tempRHS()-=Vec;
    Y.axpy(-A,tempRHS());
}


void HaccInverse::parallelSetup() const{
    if(MPIwrapper::Size()>1) {//ABORT("for now, only single processor");
        gX.reset(new CoefficientsGlobal(hacc->s0Inv->iIndex));
        gY.reset(new CoefficientsGlobal(hacc->s0Inv->iIndex));
    }
}

void HaccInverse::ovr1(std::complex<double> A, Coefficients &X, std::complex<double> B, Coefficients &Y) const{
    Coefficients x(X);
    project(x);
    hacc->s0->apply(A,x,B,Y);
}


void HaccInverse::inv1(std::complex<double> A, Coefficients &X, std::complex<double> B, Coefficients &Y) const{
    hacc->s0Inv->apply(A,X,B,Y);
    project(Y);
}

//move to UseMatrix eventually
string nonZero(const UseMatrix & Mat, double Eps){
    string res;
    for(int i=0;i<Mat.rows();i++){
        Str l;
        for(int j=0;j<Mat.cols();j++)
            if(abs(Mat(i,j))>Eps)l=l+j+Mat(i,j).complex();
        if(l!=""){res+=Str("")+i+":"+l;res+="\n";}
    }
    if(res.length()==0)res=Str("none above")+Eps;
    return res;
}

void HaccInverse::analyze() const {
    return;
    Inv1 aInv(this);
    Ovr1 aOvr(this);
    Inv1Ovr1 aIO1(this);
    UseMatrix res,mInv,mOvr,mPro,qPro;
    aInv.matrixAdd(1.,mInv);
    aOvr.matrixAdd(1.,mOvr);

    aIO1.matrixAdd(1.,res);
    res=mOvr*mInv;

    Project dPro(this);
    dPro.matrixAdd(1.,qPro);

    ProjectDual aPro(this);
    aPro.matrixAdd(1.,mPro);

    res-=mPro;

    res.purge();
    if(not res.isZero()){
        res.print("mInv*mOvr",0);
        cout<<"non_zeros\n"+nonZero(res,1.e-10);
    }
    else
        cout<<"OK mOvr*mInv=mPro"<<endl;

    /*UseMatrix mS1invU,mUAdj,mZinv;
    s0InvU->matrixAdd(1.,mS1invU);
    mS1invU=qPro*mS1invU;
    uAdj->matrixAdd(1.,mUAdj);
    mZinv=mUAdj*mS1invU;
    mZinv.purge().print("Zinv",1);

    mUAdj.print("muAdj",0);
    mS1invU.print("s1invU",0);*/


    mPro-=mPro*mPro;
    cout<<"mpro is zero="<<mPro.isZero(1.e-10)<<endl;

}



