#include "surfaceConstraint.h"

#include "index.h"
//#include "operatorMap.h"
#include "operatorFloor.h"
#include "surfaceFlux.h"
#include "coefficients.h"

using namespace std;

void SurfaceConstraint::update(double Time, const Coefficients* CurrentVec){
    //_flux->timeDerivative(Time);
    bool success = _flux->update(Time,true);
    if(not success) ABORT("Could not update the time derivative of the flux");
}

SurfaceConstraint::SurfaceConstraint(SurfaceFlux *Flux, /*const OperatorMap* Map*/const DiscretizationSurface::Map *RcMap)
    :OperatorTree("SurfaceConstraint",RcMap->jIndex,RcMap->jIndex),_flux(Flux)
{
    name="surfaceConstraint";

    for(unsigned int k=0;k<RcMap->childSize();k++){
        childAdd(new SurfaceConstraint(Flux,dynamic_cast<const DiscretizationSurface::Map*>(RcMap->child(k))));
    }
    if(RcMap->isLeaf())oFloor=new Floor(Flux,RcMap);

}

SurfaceConstraint::Floor::Floor(const SurfaceFlux *Flux,const DiscretizationSurface::Map *Map)
    :OperatorFloor("SurfaceConstraint")
{
    _derF=Flux->coefs->nodeAt(Map->iIndex->index())->data();
    UseMatrix mat;
    Map->matrix(mat);
    _c=Eigen::Map<Eigen::MatrixXcd>(mat.data(),Map->iIndex->sizeStored(),Map->jIndex->sizeStored());
    _b=_c.adjoint()*(_c*_c.adjoint()).inverse();
}

void SurfaceConstraint::Floor::axpy(const std::complex<double> &Alfa, const std::complex<double> *X, unsigned int SizX,
                                    const std::complex<double> &Beta, std::complex<double> *Y, unsigned int SizY) const{
    // compute derivative
    if(Alfa!=1. or Beta!=1.)DEVABORT("use only with Alfa=Beta=1");

    //  a = d/dt f <- d/dt f - C * (-iPb)
    Eigen::VectorXcd derF=Eigen::Map<Eigen::VectorXcd>(const_cast<complex<double>*>(_derF),_c.rows());
    derF-=_c*Eigen::Map<Eigen::VectorXcd>(const_cast<complex<double>*>(X),SizX);

    // y <- y - B * a
    Eigen::Map<Eigen::VectorXcd>(const_cast<complex<double>*>(Y),SizY)=_b*derF;

}
