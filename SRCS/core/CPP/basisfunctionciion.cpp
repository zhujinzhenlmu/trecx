// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "basisfunctionciion.h"
#include "readInput.h"
#include "printOutput.h"
#include "read_columbus_data.h"
#include "quantumChemicalInput.h"
#include "mo.h"
#include "index.h"
#include "ci.h"

using namespace std;
#include "eigenNames.h"


BasisFunctionCIion::BasisFunctionCIion(int order):BasisFunction("CIion",order)
{
    string col_path;
    My4dArray_shared* vee;
    orbitals=new mo(*QuantumChemicalInput::ions,0);
    if(order>QuantumChemicalInput::ions->ciCoef.size())
        ABORT(Str("Only")+QuantumChemicalInput::ions->ciCoef.size()+"ionic states available, asking for"+order
              +", source:"+QuantumChemicalInput::ions->source());

    //HACK - redundancy and misuse of notation
    QuantumChemicalInput::ions->cif.numI=order;
    QuantumChemicalInput::ions->cif.numI=order;

    multIon = QuantumChemicalInput::ions->multiplicity();
    CI = new ci(*QuantumChemicalInput::ions,*orbitals,order,0);
    vee=QuantumChemicalInput::ions->vee;

    /// Add spin multiplets to CI list
    CI->add_multiplets(multIon);


    /// Setup spin and lc_factors
    setup_spin(CI);
    setup_lc_factors();

    /// Setup required reduced density matrices
    CI->ci_setup_all_spindens(vee,rho1,rho2_lc,rho3_vee,lc_factors,spin,&ion_2e);

    /*if(MPIwrapper::isMaster()) {
    std::ofstream strm2("rho1.dat");
    for(std::size_t n = 0; n < rho1[0][0].array().size(); ++n)
        if(std::abs(rho1[0][0].array()(n)) > 1.e-10)
            strm2 << n << "\t" << rho1[0][0].array()(n) << '\n';

    std::ofstream strm("rho2.dat");
    for(std::size_t n = 0; n < rho2_lc[0][0]->size(); ++n)
        if(std::abs(rho2_lc[0][0]->data()[n]) > 1.e-10)
        strm << n << "\t" << rho2_lc[0][0]->data()[n] << '\n';

    std::ofstream strm3("rho3.dat");
    for(std::size_t n = 0; n < rho3_vee[0][0].array().size(); ++n)
        if(std::abs(rho3_vee[0][0].array()(n)) > 1.e-10)
            strm3 << n << "\t" << rho3_vee[0][0].array()(n) << '\n';
    }*/
}

BasisFunctionCIion::~BasisFunctionCIion()
{
    if(CI!=0)       {delete CI; CI=0;}
    if(orbitals!=0) {delete orbitals; orbitals=0;}
}

void BasisFunctionCIion::setup_spin(ci* ion)
{
    int multF;
    multF=QuantumChemicalInput::ions->cif.multF;
    for(int i=0;i<ion->total_states;i++)
        for(unsigned int j=0;j<ion->det.size();j++){
            if(abs(ion->coef[i](j))>1e-14){
                int spin_up  =  ion->det[j].spinup;
                int spin_down = ion->det[j].spac.size()-spin_up;
                if(multIon==2 and multF==1){
                    if(spin_up>spin_down)   spin.push_back(0);
                    else if (spin_up<spin_down) spin.push_back(1);
                    else ABORT("sae_disc::setup_spin:  Impossible condition encountered");
                    //cout<<ion->det[j]<<"   "<<spin[spin.size()-1]<<endl;
                }
                else if(multIon==2 and multF==3){
                    if(spin_up>spin_down)   spin.push_back(0);
                    else if (spin_up<spin_down) spin.push_back(1);
                    else ABORT("sae_disc::setup_spin:  Impossible condition encountered");
                }
                else if(multIon==1 and multF==2){
                    if(spin_up==spin_down)   spin.push_back(0);
                    else ABORT("sae_disc::setup_spin:  Impossible condition encountered");
                }
                else
                    ABORT("sae_disc::setup_spin:  case not yet implemented");

                j = ion->det.size();    // condition to complete the j loop
            }
        }
}

void BasisFunctionCIion::setup_lc_factors()
{

    int multF;
    multF=QuantumChemicalInput::ions->cif.multF;


    if(multIon==2 and multF==1){
        lc_factors.push_back(1.0/sqrt(2.0));
        lc_factors.push_back(-1.0/sqrt(2.0));
    }
    else if(multIon==2 and multF==3){
        //lc_factors.push_back(0.0);
        //lc_factors.push_back(1.0);
        lc_factors.push_back(1.0/sqrt(2.0));
        lc_factors.push_back(1.0/sqrt(2.0));
    }
    else if(multIon==1 and multF==2){
        lc_factors.push_back(1.0);
    }
    else
        ABORT("sae_disc::setup_lc_factors:  case not yet implemented");
}

UseMatrix BasisFunctionCIion::matrix_1e(string def) const
{
    // check if it is storage
    if(onepMat.find(def)!=onepMat.end()) return onepMat[def];

    // Else compute

    UseMatrix result = UseMatrix::Zero(ion_2e.rows(),ion_2e.rows());

    if(def!="EEInt"){

        MatrixXcd mores;
        orbitals->matrix_1e(mores,def);

        if(def=="1") mores *= (1./CI->nElectrons());     // special case for overlap

        int Nmos = orbitals->NumberOfOrbitals();
        for(int I_i=0;I_i<result.rows();I_i++)
            for(int I_j=0;I_j<result.cols();I_j++){

                MatrixXd rho = MatrixXd::Zero(Nmos,Nmos);

                for(int m1=0;m1<multIon;m1++)
                    for(int m2=0;m2<multIon;m2++)
                        if(abs(lc_factors[m1]*lc_factors[m2])>1e-14 and spin[multIon*I_i+m1]==spin[multIon*I_j+m2]){
                            rho += rho1[multIon*I_i+m1][multIon*I_j+m2].block(0,0,Nmos,Nmos)*lc_factors[m1]*lc_factors[m2];
                            rho += rho1[multIon*I_i+m1][multIon*I_j+m2].block(Nmos,Nmos,Nmos,Nmos)*lc_factors[m1]*lc_factors[m2];
                        }

                result(I_i,I_j) = (mores.array()*rho.array()).sum();
            }
    }
    else{

        for(int I_i=0;I_i<result.rows();I_i++)
            for(int I_j=0;I_j<result.cols();I_j++){
                if(CI->nElectrons()>1){
                    if(I_i>=I_j ) result(I_i,I_j) += 0.5*ion_2e(I_i,I_j);
                    else          result(I_i,I_j) += 0.5*ion_2e(I_j,I_i);
                }
            }
    }


    onepMat[def]=result;
    return result;
}

void BasisFunctionCIion::setup_rho2_X_op(MatrixXcd& result, string def, int I_i, int I_j)
{
    Vector2i aaa; aaa << I_i,I_j;
    MatrixXcd* r_temp = rho2_X_op_storage.retrieve(def,aaa);
    if(r_temp!=NULL){
        result = *r_temp;
        return;
    }

    typedef Map<Matrix<double,Dynamic,Dynamic,RowMajor> > MapEigRM;

    int Nmos = orbitals->NumberOfOrbitals();
    result = MatrixXcd::Zero(Nmos,Nmos);
    MatrixXcd op;
    orbitals->matrix_1e(op,def);
    int d3=Nmos;
    int d4=Nmos;
    int shift_add =d3*d4;

    int shift=0;
    for(int d1=0;d1<Nmos;d1++)
        for(int d2=0;d2<Nmos;d2++){

            if(I_i>=I_j){
                if(not(rho2_lc[I_i][I_j]->isZero))
                    result(d1,d2) += (((MapEigRM(rho2_lc[I_i][I_j]->data()+shift,d3,d4)).array()*op.array()).matrix().sum());
            }
            else{
                if(not(rho2_lc[I_j][I_i]->isZero))
                    result(d2,d1) += (((MapEigRM(rho2_lc[I_j][I_i]->data()+shift,d3,d4)).transpose().array()*op.array()).matrix().sum());
            }
            shift+=shift_add;
        }

    std::ofstream ofs{ def + ".txt", std::ios::app };
    ofs << I_i << " " << I_j << "\n\n" << result << "\n\n\n";
    rho2_X_op_storage.add(def,aaa,result);
}

void BasisFunctionCIion::valDer(const std::complex<double> &X, std::vector<std::complex<double> > &Val, std::vector<std::complex<double> > &Der, bool ZeroOutside) const
{
    ABORT("Should not be called");
}

void BasisFunctionCIion::setup_eta3_X_op(MatrixXcd &result, string def, int I_i, int numN)
{
    int aaa = I_i;
    MatrixXcd* r_temp = eta3_X_op_storage.retrieve(def,aaa);
    if(r_temp!=NULL){
        result = *r_temp;
        return;
    }

    MatrixXcd mat;
    mo::main->matrix_1e(mat,def);
    int Nmos = orbitals->NumberOfOrbitals();
    //construct delta x eta3
    result = MatrixXcd::Zero(numN,Nmos);
    for(int m1=0;m1<multIon;m1++){
        int new_I = multIon*I_i+m1;
        int sp = spin[new_I];
        for(int nn=0;nn<numN;nn++){
            int shift = sp*4*Nmos*Nmos*Nmos;
            for(int mm=0;mm<Nmos;mm++){
                result(nn,mm) += ((Map<Matrix<double,Dynamic,Dynamic,RowMajor> >(eta3[nn][new_I].data()+shift,2*Nmos,2*Nmos).block(0,0,Nmos,Nmos)+
                                   Map<Matrix<double,Dynamic,Dynamic,RowMajor> >(eta3[nn][new_I].data()+shift,2*Nmos,2*Nmos).block(Nmos,Nmos,Nmos,Nmos)).array()*mat.array()).sum()*lc_factors[m1];

                shift += 4*Nmos*Nmos;
            }
        }
    }
    eta3_X_op_storage.add(def,aaa,result);
}

UseMatrix BasisFunctionCIion::matrixWithNeutrals(string def, const Index* chan, const Index* neut){

    /// Modify string
    def = def.substr(6,def.size()-8);

    UseMatrix res = UseMatrix::Zero(chan->sizeCompute(),neut->sizeCompute());
    if(def=="FieldFreeHamiltonian"){
        res=matrixWithNeutrals_helper("Laplacian",chan,neut);
        res*=0.5;            // Kinetic = 1/2 Laplacian
        res+=matrixWithNeutrals_helper("Coulomb",chan,neut);
        res+=matrixWithNeutrals_helper("EEInt",chan,neut);
    }
    else if(def=="MixedGaugeDipole"){
        ABORT("Not implemented.");
    }
    else{
        res=res+=matrixWithNeutrals_helper(def,chan,neut);
    }
    return res;
}

UseMatrix BasisFunctionCIion::matrixWithNeutrals_helper(string def, const Index* chan, const Index* neut)
{
    /// Contruct indices
    int numN=neut->sizeCompute();
    int n,l,m,I,numI,mMax;
    for(const Index* s=chan; ; s=s->parent()){
        if(s->parent()==0) break;
        if(s->parent()->axisName()=="Eta") l=s->nSibling()+abs(s->parent()->basisSet()->param(0));
        else if(s->parent()->axisName()=="Phi") {m=s->parent()->basisSet()->parameters()[s->nSibling()]; mMax = (s->parent()->basisSet()->order()-1)/2;}  // mmax check ?
        else if(s->parent()->axisName()=="Ion") {I=s->nSibling(); numI=s->parent()->basisSet()->order();}
        else if(s->parent()->axisName()=="Rn")   n=s->nSibling();
        else if(s->parent()->axisName()=="Hybrid") continue;
        else ABORT("Unknown axis name: "+s->parent()->axisName());
    }

    MatrixXcd res=MatrixXcd::Zero(chan->sizeCompute(),neut->sizeCompute());
    int d = orbitals->coef.rows();

    if(n<mo::main->gauge_boundary){
        if(def!="EEInt"){

            if(def=="FullOverlap" or def=="1"){

                /// Overlap

                MatrixXcd Oi;
                mo::main->matrix_1e(Oi,"1",n,l,m);

                /// Term with standard Dyson orbital
                if(not Rho.block(I*d,numI*d,d,numN).isZero(1e-14) and not Oi.isZero(1e-14))res-= Oi.adjoint()*Rho.block(I*d,numI*d,d,numN);
            }
            else{

                /// Single particle operators

                MatrixXcd opi,de3,Oi;
                mo::main->matrix_1e(opi,def,n,l,m);
                mo::main->matrix_1e(Oi,"1",n,l,m);

                /// Term with standard Dyson orbital
                if(not Rho.block(I*d,numI*d,d,numN).isZero(1e-14) and not opi.isZero(1e-14))res-= opi.adjoint()*Rho.block(I*d,numI*d,d,numN);

                /// Term with eta3
                setup_eta3_X_op(de3,def,I,numN);
                if(not Oi.isZero(1e-14) and not de3.isZero(1e-14))res += Oi.adjoint()*de3.adjoint();
            }
        }

        else{

            /// Two particle operator

            res = mo::main->R_d[I][n][l][m+min(l,mMax)];

            ///eta5 term
            //            if(mo::main->col_data->no_electrons>1){
            if(CI->nElectrons()>1){
                MatrixXcd Oi;
                mo::main->matrix_1e(Oi,"1",n,l,m);
                res += 0.5*Oi.adjoint()*Rho3.block(I*d,numI*d,d,numN);
            }
        }
    }

    UseMatrix mat = UseMatrix::Zero(res.rows(),res.cols());
    Map<MatrixXcd>(mat.data(),mat.rows(),mat.cols()) = res;

    return mat;
}

void BasisFunctionCIion::setup_crossterms(My4dArray_shared* Vee,BasisFunctionCINeutral *CIneut)
{
    if(CIneut==0) return;
    else CIneut->CI->ci_setup_all_spindyson(Vee,eta1,eta3,eta5_X_Vee,CI);
}

int BasisFunctionCIion::nElectrons() const {return CI->nElectrons();}
