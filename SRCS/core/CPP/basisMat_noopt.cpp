// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "basisMat.h"

using namespace std;

void BasisMat::funcDefaults()
{
    if(funcDefaultsSet)return;
    funcDefaultsSet=true;
    basisMatFuncSet(new basisMatRsq());
    basisMatFuncSet(new basisMatHarm2());
    basisMatFuncSet(new basisMatSpherBessel());
    basisMatFuncSet(new basisMatExpI());
    UserFunctions::set();
}

