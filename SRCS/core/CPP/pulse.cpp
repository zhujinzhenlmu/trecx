// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "pulse.h"

#include <memory>
#include "readInput.h"
#include "abort.h"
#include "mpiWrapper.h"
#include "constants.h"
#include "parameters.h"
#include "printOutput.h"
//#include "boost/lexical_cast.hpp"
#include "algebra.h"
#include "multiParam.h"

using namespace std;
using namespace tools;

Pulse Pulse::current=Pulse(); // declare variable for current pulse
bool Pulse::checkNew=false; // for now, check new pulse


void Pulse::setCurrent(ReadInput & Inp, bool Print, string Kind)
{
    std::string File="";
    if(Print)File=Inp.output()+Kind;
    setCurrent(Pulse(Inp,Kind),File);
}

void Pulse::setCurrent(const Pulse & Pulse, string PrintFile){
    current=Pulse;
    Parameters::add("LaserF0[t]",1.,Pulse::F0);
    Parameters::add("LaserFz[t]",1.,Pulse::F0);
    Parameters::add("LaserFx[t]",1.,Pulse::F1);
    Parameters::add("LaserFy[t]",1.,Pulse::F2);

    // alternate, more consistent naming
    Parameters::add("iLaserA0[t]",1.,Pulse::iAz);
    Parameters::add("iLaserAz[t]",1.,Pulse::iAz);
    Parameters::add("iLaserAx[t]",1.,Pulse::iAx);
    Parameters::add("iLaserAy[t]",1.,Pulse::iAy);

    Parameters::add("LaserAxx[t]",1.,Pulse::Axx);
    Parameters::add("LaserAyy[t]",1.,Pulse::Ayy);
    Parameters::add("LaserAzz[t]",1.,Pulse::Azz);
    Parameters::add("LaserAxy[t]",1.,Pulse::Axy);
    Parameters::add("LaserAxz[t]",1.,Pulse::Axz);
    Parameters::add("LaserAyz[t]",1.,Pulse::Ayz);

    Parameters::add("LaserAsq[t]",1.,Pulse::Asqu);
    Parameters::add("LaserAsqHalf[t]",1.,[](double d) { return Pulse::Asqu(d) / 2.; });
    Parameters::add("iLaserAabs[t]",1.,Pulse::iAabs);
    Parameters::add("iLaserDtheta[t]",1.,Pulse::idTheta);

    Parameters::add("LaserFabs[t]",1.,Pulse::Fabs);
    // only printout laser file when on master process, or it will be very slow
    if(PrintFile!="" and MPIwrapper::isMaster())current.print(PrintFile, 0., 0., 2000);
}

/// electric field
std::complex<double> Pulse::F0(double Time){return current.Field(Time,0);}
std::complex<double> Pulse::F1(double Time){return current.Field(Time,1);}
std::complex<double> Pulse::F2(double Time){return current.Field(Time,2);}

/// vector potential (time-integral of electric field
std::complex<double> Pulse::iAz(double Time){return complex<double>(0,current.Apot(Time,0));}
std::complex<double> Pulse::iAx(double Time){return complex<double>(0,current.Apot(Time,1));}
std::complex<double> Pulse::iAy(double Time){return complex<double>(0,current.Apot(Time,2));}

std::complex<double> Pulse::iAabs(double Time){return complex<double>(0,std::sqrt(current.Apot2(Time)));}
std::complex<double> Pulse::idTheta(double Time){return complex<double>(0,current.dTheta(Time));}

std::complex<double> Pulse::Axx(double Time){return complex<double>(std::pow(current.Apot(Time,1),2),0.);}
std::complex<double> Pulse::Ayy(double Time){return complex<double>(std::pow(current.Apot(Time,2),2),0.);}
std::complex<double> Pulse::Azz(double Time){return complex<double>(std::pow(current.Apot(Time,0),2),0.);}

std::complex<double> Pulse::Axy(double Time){return complex<double>(current.Apot(Time,1)*current.Apot(Time,2),0.);}
std::complex<double> Pulse::Axz(double Time){return complex<double>(current.Apot(Time,1)*current.Apot(Time,0),0.);}
std::complex<double> Pulse::Ayz(double Time){return complex<double>(current.Apot(Time,2)*current.Apot(Time,0),0.);}

std::complex<double> Pulse::Asqu(double Time){return complex<double>(current.Apot2(Time),0.);}

std::complex<double> Pulse::Fabs(double Time){
    return std::sqrt( std::pow(current.Field(Time,0),2)
                      +std::pow(current.Field(Time,1),2)
                      +std::pow(current.Field(Time,2),2));
}

Pulse::Pulse( ReadInput &Inp,string PulseName):pulseName(PulseName),t0F(0.),t0A(0)
{
    //    comp.assign(3,vector<PulseSingle>());

    string shape;
    double peakIntensity=0.;
    double fwhm=0.;
    double lambda=0.;
    double phiCeo=0.;
    double t0=0.;
    double polAng;
    double aziAng=0.;
    double ellip=0.,ellipAng=0.;
    double photonEnergy=0.;
    double energyChirp=0.;
    double amplitude=0.;
    double laserCycles=0.;
    print0=DBL_MAX;
    print1=-DBL_MAX;

    Inp.read("Pulse","check",checkNew,"false","check new against old pulse checking code");

    // a series of pulses is read in
    int line=0;
    while (true) {
        line++;
        Inp.read(PulseName,"shape",shape,"BLANK","pulse shapes: gauss,cos8,cos4,cos2,sin2,flatTop,train,BLANK",line);
        if(shape=="BLANK") break;
        if (PulseName=="Pulse") {
            Inp.read(PulseName,"peakAmpl",amplitude,ReadInput::noDefault,"peak amplitude",line);
            Inp.read(PulseName,"circFreq",photonEnergy,"0","circular carrier frequency (omega)",line);
        }
        else if(PulseName=="Laser"){
            Inp.read(PulseName,"lambda(nm)",lambda,"-1","wave length",line);
            photonEnergy=Units::convert(physics::planck_constant*physics::speed_of_light/(lambda*1.e-9),"SI|energy","au");
            Inp.read(PulseName,"ePhoton",photonEnergy,tools::str(photonEnergy),"photon energy",line);
            Inp.exclude("Laser","Laser","ePhoton","lambda(nm)");

            Inp.read(PulseName,"I(W/cm2)",peakIntensity,ReadInput::noDefault,"peak intensity",line);
            Inp.read(PulseName,"lambda@I/2(nm)",energyChirp,tools::str(lambda),"linear chirp - wave length at half intensity after peak",line);
            energyChirp=Units::convert(physics::planck_constant*physics::speed_of_light/((energyChirp)*1.e-9),"SI|energy","au")-photonEnergy;

            amplitude=sqrt(Units::convert(peakIntensity,"W/cm2","au"))/photonEnergy;
        }
        else
            ABORT("no specifics defined for pulse type: "+PulseName);
        if(photonEnergy<0.)ABORT("must specify ePhoton or lambda(nm)");



        if(photonEnergy!=0. and line==1)
            Units::addUnit("OptCyc|time",2*math::pi*Units::convert(1/photonEnergy,"au|time","SI"));

        Inp.read(PulseName,"FWHM",fwhm,ReadInput::noDefault,"intensity FWHM of pulse",line);
        Inp.read(PulseName,"peak",t0,"0","time of peak envelope",line);
        Inp.read(PulseName,"phiCEO",phiCeo,"0","(rad) carrier-envelope offset",line,"","[-6.2832,6.2832]");
        Inp.read(PulseName,"polarAngle",polAng,"0","(deg) polar angle (measured from z-axis)",line);
        Inp.read(PulseName,"azimuthAngle",aziAng,"0","(deg) azimuthal angle (measured from x-axis)",line);
        if((polAng!=0. and abs(polAng)<5.) or (aziAng!=0. and abs(aziAng)<5.))
            PrintOutput::warning(Str("ARE YOU SURE? polar and azimuthAngle are in degrees, found: ")+polAng+aziAng);
        Inp.read(PulseName,"ellip",ellip,"0","ellip%2 gives -1=left circular, 0=linear, +1=right cirular ",line);
        if(checkNew and ellip!=0.){
            checkNew=false;
            PrintOutput::warning("checking cannot be done for elliptic polarization, switched off");
        }
        Inp.read(PulseName,"ellipAng",ellipAng,"0","angle of ellipticity plane arount main polarization axis",line);

        Inp.obsolete(PulseName,"LaserCycles","enter FWHM in OptCyc instead",line);

        energyChirp/=(fwhm/2);

        sing.push_back(std::shared_ptr<PulseSingle>(
                           PulseSingle::factory(shape,amplitude,t0,fwhm,photonEnergy,phiCeo,
                                                ellip,ellipAng,polAng,aziAng,energyChirp)));
        print0=min(print0,sing.back()->print0);
        print1=max(print1,sing.back()->print1);
    }
    // possibly read parameter range
    //parameterRange(Inp);


    Inp.exclude("Pulse","Laser");
    if(sing.size()==0)PrintOutput::warning("no pulse given - dummy zero pulse");

    t0int=tBegin();
    if(t0int<-DBL_MAX/2){
        t0int=print0;
        PrintOutput::DEVwarning("infinite pulse, start integration at beginning of default print range");
    }

    intAsqT0=0.;
    vecA(t0A+1.e-10);
    vecF(t0F+1.e-10);
    intAsq(t0int+1.e-10);
}

double Pulse::tBegin() const {
    double t=DBL_MAX;
    for (int k=0;k<sing.size();k++)t=min(t,sing[k]->tBegin);
}

void Pulse::alignFieldZ(double Time){
    // align polarization such that pulse Field(Time) points in positive Z-direction
    VectorReal newZ(vecF(Time));
    double cosA=newZ[0]/sqrt(newZ.normSqu());
    double sinA=abs(sqrt(1.-cosA*cosA));
    if(newZ[1]<0.)sinA=-sinA;
    for(int k=0;k<sing.size();k++){
        VectorReal oldMain(sing[k]->polarMain),oldPerp(sing[k]->polarPerp);
        if((abs(oldMain[2])+abs(oldPerp[2]))>1.e-10)ABORT("only for polarization in 0-1 plane");
        sing[k]->polarMain =oldMain*cosA;
        sing[k]->polarMain-=oldPerp*sinA;
        sing[k]->polarPerp =oldMain*sinA;
        sing[k]->polarPerp+=oldPerp*cosA;
    }
}


void Pulse::output(string Title,string Operator) const{

    if(Title=="")PrintOutput::title(current.pulseName);
    else         PrintOutput::title(Title);

    PrintOutput::paragraph();
    if(apotMax()==0.){
        PrintOutput::message("no non-zero component");
        return;
    }

    PrintOutput::newRow();
    PrintOutput::rowItem("polAxis");
    PrintOutput::rowItem("Shape");
    if(current.pulseName=="Laser"){
        PrintOutput::rowItem("Intens(W/cm2)");
        PrintOutput::rowItem("lambda(nm)");
    } else {
        PrintOutput::rowItem("peakAmpl");
        PrintOutput::rowItem("photonEnergy");

    }
    PrintOutput::rowItem("FWHM");
    PrintOutput::rowItem("begin");
    PrintOutput::rowItem("end");
    PrintOutput::rowItem("phiCEO");
    vector<string> ax(3,"z");ax[1]="x";ax[2]="y";
    vector<string> ax_alt(ax);ax_alt[0]="0";
    double omega0;
    bool multicolor=false;
    for(unsigned int i=0;i<sing.size();i++){
        if(i==0)omega0=sing[i]->omega;
        multicolor=multicolor or omega0!=sing[i]->omega;
        if (abs(sing[i]->aPeak)<1.e-12)continue;
        PrintOutput::newRow();
        PrintOutput::rowItem(Str("","")+"["+sing[i]->polarMain.purge()+"]");
        PrintOutput::rowItem(sing[i]->shape);
        if(current.pulseName=="Laser"){
            PrintOutput::rowItem(Units::convert((sing[i]->aPeak*sing[i]->omega)*(sing[i]->aPeak*sing[i]->omega),"au|intensity","W/cm2"));
            PrintOutput::rowItem(physics::planck_constant/Units::convert(sing[i]->omega,"au|energy","SI")*physics::speed_of_light*1.e9);
        } else {
            PrintOutput::rowItem(sing[i]->aPeak);
            PrintOutput::rowItem(sing[i]->omega);
        }
        PrintOutput::rowItem((sing[i]->fwhm()));
        PrintOutput::rowItem((sing[i]->tBegin));
        PrintOutput::rowItem((sing[i]->tEnd));
        if(sing[i]->phiCeo!=0)PrintOutput::rowItem((sing[i]->phiCeo));
        else                  PrintOutput::rowItem("");
        if(abs(sing[i]->deltaOmega)>sing[i]->omega*1.e-10)
            PrintOutput::rowItem((sing[i]->omega+sing[i]->deltaOmega*sing[i]->fwhm()/2.));

    }
    // check consistency of operator with pulse
    if(Operator!="NONE"){
        for(int i=0;i<3;i++){
            if(vecAmax(i)<1.e-10)continue;
            if(Operator.find("LaserA"+ax[i])==string::npos
                    and Operator.find("LaserF"+ax[i])==string::npos
                    and Operator.find("LaserA"+ax_alt[i])==string::npos
                    and Operator.find("LaserF"+ax_alt[i])==string::npos
                    and Operator.find("<<MixedGaugeDipole")==string::npos
                    )
                PrintOutput::warning("Operator "+Operator+" does not couple to "
                                     +ax[i]+" component of pulse - spectra may be inconsistent");
        }
    }
    if(multicolor){
        PrintOutput::newLine();
        PrintOutput::warning("multi-color pulse, first pulse input line defines OptCyc, value="
                             +tools::str(Units::convert(1.,"OptCyc|time","au"),7)+" (au)",1);
    }



    if(sing.size()==0)PrintOutput::message("NO PULSE SPECIFIED");
    PrintOutput::paragraph();
    PrintOutput::lineItem("U_ponderomotive (eV)",Units::convert(current.uPonderomotive(),"au","eV"));

}

string Pulse::str(unsigned int Brief) const {

    if(apotMax()==0.)return "noPulse";

    string s;
    if(Brief==0){
        for(unsigned int i=0;i<sing.size();i++){
            if (sing[i]->aPeak==0.)continue;
            if(i>0)s+=".";
            s+=sing[i]->shape+"["+tools::str(i)+"]";
            if(current.pulseName=="Laser"){
                s+=tools::str(int(sing[i]->fwhm()*sing[i]->omega/(2.*math::pi)*(1.+1.e-10)))+"c ";
                s+=tools::str(Units::convert((sing[i]->aPeak*sing[i]->omega)*(sing[i]->aPeak*sing[i]->omega),"au|intensity","W/cm2"),2);
                s+="@"+tools::str(physics::planck_constant/Units::convert(sing[i]->omega,"au|energy","SI")*physics::speed_of_light*1.e9,4);
            } else {
                s+=tools::str(sing[i]->fwhm())+"d ";
                s+=tools::str(sing[i]->aPeak,2);
                s+="@"+tools::str(sing[i]->omega,3);
            }
        }

    }
    else if(Brief>0){
        for(int l=0;l<sing.size();l++)
            if(sing[l]->aPeak>1.e-12)s+="\n"+sing[l]->str();
        if(s.length()>0)s.erase(s.begin(),s.begin()+1);
        else s=" --- empty pulse ----";
    }
    return s;
}

double Pulse::gettBegin()
{

    if(current.sing.size()==0)return -DBL_MAX; // empty pulse lasts forever
    double t = current.sing[0]->tBegin;
    for (int j=0; j<current.sing.size(); ++j)
        t=min(t,current.sing[j]->tBegin);
    return t;
}
double Pulse::gettEnd()
{
    if(current.sing.size()==0)return DBL_MAX; // empty pulse lasts forever
    double t = current.sing[0]->tEnd;
    for (int j=0; j<current.sing.size(); ++j)
        t=max(t,current.sing[j]->tEnd);
    return t;
}

double Pulse::uPonderomotive() const{
    double uP=0.;
    for(unsigned int l=0;l<sing.size();l++){
        if(sing[l]->omega==0.)ABORT("no meaningful ponderomotive potential for frequency=0");
        uP+=0.25*sing[l]->aPeak*sing[l]->aPeak;
    }
    return uP;
}

double Pulse::omegaMax() const {
    double res=0.;
    for(unsigned int l=0;l<sing.size();l++)
        res=max(res,sing[l]->omega);
    return res;
}
double Pulse::apotMax() const {
    double res=0.;
    for(unsigned int l=0;l<sing.size();l++)
        res=max(res,sing[l]->aPeak);
    return res;
}
double Pulse::omegaMin() const {
    double res=DBL_MAX;
    for(unsigned int l=0;l<sing.size();l++)
        res=min(res,sing[l]->omega);
    return res;
}

double Pulse::dTheta(double Time) const{
    double eps=1.e-12;
    double time=Time;

    for(unsigned int k=0;k<2;k++)
    {
        double Fz=Field(Time,0),Fx=Field(Time,1);
        if(abs(Fx)+abs(Fz)<2*eps)return 0.;
        double Az=Apot(Time,0),Ax=Apot(Time,1);
        double Asq=Ax*Ax+Az*Az;
        double d=Az*Fx-Ax*Fz;
        if(Asq>eps*eps){
            if(abs(d)>abs(Az*Fx)*1.e-8)return d/Asq;
        }
        else
            if(abs(Az*Fz)<eps*eps)return 0.;

        // in case of zero vector potential,
        // use nearby value
        if(k==0)time=Time+1e-5;
        if(k==1)time=Time-1.e-5;
    }
    return 0.; //HACK
    ABORT("failed to uniquely compute dTheta: t="+tools::str(Time));
}

double Pulse::Apot2(double Time) const {
    double res=0;
    for(unsigned int i=0;i<3;i++)res+=std::pow(Apot(Time,i),2);
    return res;
}

VectorReal Pulse::vecA(double Time, bool Check) const {
    if(Time!=t0A){
        Pulse*q=const_cast<Pulse*>(this);
        q->t0A=Time;
        // new time-recalculate
        q->vecAt0.assign(3,0.);
        for(int k=0;k<sing.size();k++){
            double env=sing[k]->aPeak*sing[k]->valEnv(Time-sing[k]->t0);
            double car=sing[k]->carrier(Time,false);
            if(sing[k]->omega!=0.)
                q->vecAt0+=sing[k]->polarMain*env*car*sing[k]->compMain;
            else
                q->vecAt0+=sing[k]->polarMain*env*sing[k]->compMain;

            if(sing[k]->polarPerp.size()!=0)
                q->vecAt0+=sing[k]->polarPerp*env*sqrt(1-car*car)*sing[k]->compPerp;

        }
    }
    return vecAt0;
}

double Pulse::vecAmax(int Component) const {
    VectorReal vecAmax;
    vecAmax.assign(3,0.);
    for(int k=0;k<sing.size();k++){
        double env=sing[k]->aPeak;
        vecAmax+=sing[k]->polarMain*env*sing[k]->compMain;

        if(sing[k]->polarPerp.size()!=0)
            vecAmax+=sing[k]->polarPerp*env*sing[k]->compPerp;

    }
    return std::abs(vecAmax[Component]);
}

VectorReal Pulse::vecF(double Time,bool Check) const {
    if(Time!=t0F){
        Pulse*q=const_cast<Pulse*>(this);
        q->t0F=Time;
        q->vecFt0.assign(3,0.);
        for(int k=0;k<sing.size();k++){
            double der=sing[k]->aPeak*sing[k]->derEnv(Time-sing[k]->t0);
            double env=sing[k]->aPeak*sing[k]->valEnv(Time-sing[k]->t0);
            double car=sing[k]->carrier(Time,false),dCar=sing[k]->carrier(Time,true);
            q->vecFt0+=sing[k]->polarMain*sing[k]->compMain*(dCar*env+car*der); // derivative of carrier

            if(sing[k]->polarPerp.size()>0 and abs(sing[k]->compPerp)>1.e-10){
                if(sing[k]->deltaOmega!=0)ABORT("chirp not implemented for elliptic");
                q->vecFt0+=sing[k]->polarPerp*sing[k]->compPerp*(-car*env+dCar*der);
            }
        }
    }
    return vecFt0;
}

double Pulse::intAsq(double Time) const {
    if(Time!=t0int){
        Pulse*q=const_cast<Pulse*>(this);
        q->intAsqT0+=aSq().integral(complex<double>(t0int),complex<double>(Time)).real();
        q->t0int=Time;
    }
    return intAsqT0;
}

void Pulse::print(string File,double T0, double T1, int Points){
    ofstream laser;
    vector<double>intF(3,0.);
    laser.open((File).c_str());
    laser<<"# time,|A|, dTheta/dt, Apot[0], Field[0], Int(Field[0]), Apot[1], Field[1], Int(Field[1]), Apot[2], Field[2], Int(Field[2])"<<endl;
    if(T0>=T1){T0=print0;T1=print1;};
    double dt=(T1-T0)/std::max(Points,1);
    for (double t=T0;t<=T1;t+=dt){
        laser<<setw(8)<<setprecision(10)<<t<<setw(18)<<sqrt(Apot2(t))<<setw(18)<<dTheta(t);
        for (unsigned int i=0;i<3;i++) {
            laser<<setw(13)<<setprecision(10)<<setw(18)<<Apot(t,i)<<" "<<setw(18)<<Field(t,i)<<" "<<setw(18)<<intF[i];
            intF[i]+=aField(i).integral(complex<double>(t),complex<double>(t+dt)).real();
        }
        laser<<endl;
    }
    laser.close();
}
