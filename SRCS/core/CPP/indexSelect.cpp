// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "indexSelect.h"

#include <string>
#include "readInput.h"
#include "printOutput.h"
#include "index.h"
#include "axis.h"

using namespace std;

const IndexSelect* IndexSelect::cur=0;

void IndexSelect::read(ReadInput &Inp, const Index* Root)
{
    cur=0;
    if(Inp.found("Select","L-M")){
        cur=new IndexSelectLminusM(Inp,Root);
    }
    else if(Inp.found("Select"))ABORT("undefned Select, admissible: L-M");
    output();
}

void IndexSelect::read(ReadInput &Inp, const vector<Axis> & Ax)
{
    cur=0;
    if(Inp.found("Select","L-M")){
        cur=new IndexSelectLminusM(Inp,Ax);
    }
    else if(Inp.found("Select"))ABORT("undefned Select, admissible: L-M");
    output();
}

void IndexSelect::output(){
    if(cur==0)return;
    PrintOutput::title("Select sub-index "+cur->str());
}

void IndexSelect::remove(Index *I){
    if(cur==0)return; /// no selection defined
    vector<int> rem;
    for(int k=I->childSize();k>0;k--)
    {
        if(cur->select(I->child(k-1)))
            remove(I->child(k-1));
        else {
            I->childErase(k-1);
            rem.push_back(k-1);
        }

    }
    // adjust Index basis, if needed
    if(rem.size()>0){
        I->setBasis(I->basisSet()->remove(rem));
    }
}



IndexSelectLminusM::IndexSelectLminusM(ReadInput &Inp, const Index* Root){
    Inp.read("Select","L-M",diffLM,ReadInput::noDefault,"keep only l-m <= this value");
    Inp.read("Select","Lmin",Lmin,ReadInput::noDefault,"keep all l<Lmin");

    // determine Phi levels
    const Index *mdx=Root;
    while(not mdx->isLeaf()){
        if(mdx->axisName().find("Phi")==0){
            if(mdx->basisSet()->str().find("expIm")==std::string::npos)
                ABORT("LminusM constraint only with expIm basis, is: "+mdx->basisSet()->str());
            mLevel.push_back(mdx->depth());
            const Index*ldx=mdx->descend();
            while(not ldx->isLeaf()){
                if(ldx->axisName().find("Eta"+mdx->axisName().substr(3))==0){
                    lLevel.push_back(ldx->depth());
                    if(ldx->basisSet()->str().find("assocLegendre")==std::string::npos)
                        ABORT("LminusM constraint only with assocLegendre basis, is: "+ldx->basisSet()->str());
                    break;
                }
            }
            if(ldx->isLeaf())ABORT("no Eta match found for axis "+mdx->axisName());
            ldx=ldx->descend();
        }
        mdx=mdx->descend();
    }
}
IndexSelectLminusM::IndexSelectLminusM(ReadInput &Inp, const vector<Axis> & Ax){
    Inp.read("Select","L-M",diffLM,ReadInput::noDefault,"keep only l-m <= this value");
    Inp.read("Select","Lmin",Lmin,ReadInput::noDefault,"keep all l<Lmin");

    // determine Phi levels
    for (int k=0;k<Ax.size();k++){
        if(Ax[k].name.find("Phi")==0){
            mLevel.push_back(k);
            if(Ax[k].basDef.size()!=1 or Ax[k].basDef[0].funcs.find("expIm")==std::string::npos)
                ABORT("LminusM constraint only with expIm basis, is: "+Ax[k].basDef[0].funcs);
            // get matching l-level
            for(int l=mLevel.back()+1;l<Ax.size();l++){
                if(Ax[l].name.find("Eta"+Ax[k].name.substr(3))==0){
                    if(Ax[l].basDef.size()!=1 or Ax[l].basDef[0].funcs.find("assocLegendre")==std::string::npos)
                        ABORT("LminusM constraint only with assocLegendre basis, is: "+Ax[l].basDef[0].funcs);
                    lLevel.push_back(l);
                    break;
                }
            }
            if(lLevel.size()!=mLevel.size())ABORT("no Eta match found for axis:\n"+Ax[k].str());
        }
    }
}



std::string IndexSelectLminusM::str() const{
    return "all L <= "+tools::str(Lmin)+", above that L-M <= "+tools::str(diffLM);
}

bool IndexSelectLminusM::select(const Index *I) const{


    if(I->depth()<=lLevel.back()){
        for(int k=0;k<I->childSize();k++)
            if(select(I->child(k)))return true;
        return false;
    }

    std::vector<unsigned int> idx(I->index());
    for(unsigned int k=0;k<lLevel.size();k++){
        // function numbers
        unsigned int fEta=idx[lLevel[k]];
        unsigned int fPhi=idx[mLevel[k]];

        // matching indices
        const Index *ldx=I;
        while(ldx->depth()!=lLevel[k])ldx=ldx->parent();
        const Index *mdx=ldx;
        while(mdx->depth()!=mLevel[k])mdx=mdx->parent();

        // l and m quantum numbers
        int mcur=mdx->basisSet()->parameter(fPhi,"m");
        int lcur=ldx->basisSet()->parameter(fEta,"l");

        // NOTE: for now, spectra seem to be calculated with special assumptions about m-basis
        //       must keep at least on m each
        if(lcur-mcur>diffLM and lcur>Lmin and lcur!=-mcur){
            return false;
        }
    }
    return true;
}
