// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "coefficientsLocal.h"

#include "coefficientsGlobal.h"
#include "parallel.h"
#include "parallelContinuity.h"

#include "mpiWrapper.h"

using namespace std;

bool CoefficientsLocal::local(const Coefficients*C){
    return Parallel::owner(C->idx())==MPIwrapper::Rank() or Parallel::owner(C->idx())==Parallel::none;
}

std::complex<double> CoefficientsLocal::_dummyStorage;

void purgeLocal(Coefficients* c, const Coefficients* orig) {
    // remove branches that do not lead to floors
    if(c->idx()->isHybrid()) { // special hybrid treatment: purge neutral and ionic subtrees individually
        // (tree algorithms won't work with non-uniform trees)
        unsigned neutralHeight = orig->child(0)->height();
        unsigned ionicHeight = orig->child(1)->height();
        if(c->childSize() > 1) { // some MPI-nodes may not have the Neutral branch
            c->child(0)->purge(neutralHeight);
            c->child(1)->purge(ionicHeight);
        }
        else c->child(0)->purge(ionicHeight);
    }
    else {
        c->purge(orig->height());
    }
}

CoefficientsLocal::CoefficientsLocal(const Index* I,complex<double>Val)
    :CoefficientsLocal()
{
    Coefficients C(I,Val);
    C.subTree(local,this);
    ::purgeLocal(this, &C);
    complex<double>*NextData(0);
    treeOrderStorage(NextData);
    unsetOrderedData(); // only floors are sorted according to idx()
    newCont.reset(new ParallelContinuity(this));
    _size=setSize(this);
}

int CoefficientsLocal::setSize(const Coefficients *C){
    if(C->isLeaf()){
        if(C->idx()->hasFloor())return C->idx()->size();
        // coefficient has been truncated completly
        return 0;
    }
    int _size=0;
    for(int k=0;k<C->childSize();k++)_size+=setSize(C->child(k));
    return _size;
}

CoefficientsLocal::CoefficientsLocal(const CoefficientsLocal & Other)
{
    Coefficients C(Other.idx());
    C.subTree(local,this);
    _size=Other.size();

    ::purgeLocal(this, &C);
    complex<double>* NextData(0);
    treeOrderStorage(NextData);
    unsetOrderedData(); // only floors are sorted according to idx()
    newCont.reset(new ParallelContinuity(this));
    _size=setSize(this);
    operator=(Other);
}
static int cnt=0;
CoefficientsLocal & CoefficientsLocal::operator=(const CoefficientsLocal & Other){
    if(_size!=Other._size)DEVABORT("sizes do not match");
    Coefficients::operator=(Other);
    return *this;
}


void CoefficientsLocal::makeContinuous(double Scal){
    newCont->apply(this,Scal);
}

/// in contiguous storage (hopefully for the same parallel layout)
CoefficientsLocal* CoefficientsLocal::cwiseProduct(const CoefficientsLocal &Rhs){
    for(int k=0;k<size();k++)anyData()[k]*=Rhs.anyData()[k];
    return this;
}

map<std::string,CoefficientsLocal*> CoefficientsLocal::_views;

CoefficientsLocal* CoefficientsLocal::view(Coefficients* C)
{
    if(_views.count(C->hash())==1){
        return _views[C->hash()];
    }
    CoefficientsLocal* v = new CoefficientsLocal();
    C->subTree(local,v,true); // view on subtree of local nodes
    ::purgeLocal(v, C);
    v->unsetOrderedData(); // only floors are Index-ordered
    v->_size=setSize(v);
    v->newCont.reset(new ParallelContinuity(v));
    _views[C->hash()]=v;
    return v;
}

std::complex<double> * CoefficientsLocal::storageData(){
    return firstLeaf()->floorData();
}

double CoefficientsLocal::norm() const{
    double nrm=const_cast<CoefficientsLocal*>(this)->Coefficients::norm();
    MPIwrapper::AllreduceMAX(&nrm,1);
    return nrm;
}
