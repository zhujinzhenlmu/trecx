#include "jacobian.h"

const Jacobian* Jacobian::factory(std::string Kind,std::complex<double> Eta)
{
    if(Kind=="1" )return new Jacobian1(Eta);
    if(Kind=="Q" )return new JacobianQ(Eta);
    if(Kind=="QQ")return new JacobianQQ(Eta);
}
