// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "operatorVectors.h"

#include "index.h"
#include "coefficients.h"
#include "timer.h"
OperatorVectors::OperatorVectors(std::string Name, const Index *IIndex, const Index *JIndex)
    :OperatorAbstract(Name,IIndex,JIndex){vecs.resize(JIndex->sizeCompute(),Coefficients(IIndex,0.));}

void OperatorVectors::apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
    Y.scale(B);
    for(int k=0;k<vecs.size();k++)
        Y.axpy(A*const_cast<Coefficients*>(&Vec)->data()[k],&vecs[k]);
}

void OperatorVectors::insertColumn(unsigned int J, const Coefficients  &C){
    if(C.idx()!=iIndex)ABORT("cannot insert, index does not match");
    vecs[J]=C;
}
