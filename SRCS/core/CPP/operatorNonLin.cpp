#include "operatorNonLin.h"
#include "operator.h"
#include "operatorSingle.h"
#include "operatorFloorNonLin.h"
#include "operatorMeanEE.h"

OperatorNonLin::OperatorNonLin(const std::string Name, const OperatorDefinition & Definition, const Index *IIndex, const Index *JIndex)
    :OperatorTree(Name,Definition,IIndex,JIndex)
{
    childAdd(new OperatorTree(Name,Definition,IIndex,JIndex));
}
void OperatorNonLin::update(double Time, Coefficients* C){
    OperatorFloorNonLin* fNonLin;
    for(OperatorTree* op=firstLeaf();op!=nullptr;op=op->nextLeaf()){
        if(fNonLin=dynamic_cast<OperatorFloorNonLin*>(op->floor()))fNonLin->updateNonLin(Time,C);
    }
}
