// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "discretizationSurfaceHybrid.h"

#include "discretizationHybrid.h"
#include "operatorTree.h"
#include "basicDisc.h"
#include "printOutput.h"

using namespace std;

DiscretizationSurfaceHybrid::DiscretizationSurfaceHybrid(const DiscretizationHybrid *Parent, const std::vector<double> &Rad, unsigned int NSurf)
    :DiscretizationSurface(Parent->comp[1],Rad,NSurf)
{
    if(idx()->axisName()!="Neut&Chan" and idx()->axisName()!="Subspace&Complement")
        PrintOutput::warning("not tested for hybrid discretization "+idx()->axisName());

    // only adjust name and map
    _mapFromParent.reset(new Map(mapFromParent(),Parent->idx()));
    name="(Hybrid)"+name;
}
