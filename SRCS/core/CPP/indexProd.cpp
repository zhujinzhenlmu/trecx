// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "indexProd.h"

using namespace std;
IndexProd::IndexProd(const Index *A, const Index *B)
{
    nodeCopy(A,false);
    for(int k=0;k<A->childSize();k++)
        childAdd(A->child(k)->deepCopy());

    Index * next=firstLeaf();
    while(next!=0){
        Index * node=next;
        next=next->nextLeaf();

        node->nodeCopy(B,false);
        for(int k=0;k<B->childSize();k++){
           node->childAdd(B->child(k)->deepCopy());
        }
    }
    if(isRoot())sizeCompute();
}
