// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#ifndef BASISPOLAROFF_H
#define BASISPOLAROFF_H

#include "basisNdim.h"

class ReadInput;
class BasisPolarOff : public BasisNdim
{
    std::vector<double> _origin;
    const Index* _idx; // basis in polar coordinates around _origin
    const Index* idxConstruct(double RadMax, int Lmax, int Mmax, int Nrad);
protected:
    // all necessary tranformations
    std::vector<double> toCartesian(const std::vector<double>&CoorNdim) const;
    double absFactor(const std::vector<double>&CoorNdim) const {return CoorNdim[2];}
    double nablaFactor(const std::vector<double>&CoorNdim,int I) const {if(I==2)return 1.;return 0.;}
    Eigen::MatrixXd jacobianToNdim(const std::vector<double>&CoorNdim) const;

public:
    BasisPolarOff(ReadInput & Inp);
    static Index * productIndex(const BasisSet *Radial, int Lmax, int Mmax);

};

#endif // BASISPOLAROFF_H
