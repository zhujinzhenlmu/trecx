// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef DISCRETIZATIONCHANNEL_H
#define DISCRETIZATIONCHANNEL_H

#include <vector>
#include <string>

#include "discretizationDerived.h"
#include "coefficients.h"

class Operator;
class Coefficients;
class Wavefunction;
class DiscretizationFactor;
class DiscretizationSpectral;
class TimePropagator;
class DiscretizationSurface;


#endif // DISCRETIZATIONCHANNEL_H
