// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef EIGEN_SOLVER_ARPACK_H
#define EIGEN_SOLVER_ARPACK_H

#include <vector>
#include <complex>

#include "eigenSolverAbstract.h"
#include "arpack.h"
#include "coefficients.h"

/// \ingroup Linalg
/// \brief Iterative Arnoldi Eigen solver
///
/// Implementation of EigenSolverAbstract using ARPACKs Arndoldi algorithm.
//
class EigenSolverArpack: public EigenSolverAbstract{
    double _tolerance;
    unsigned int _maxIterations;  // maxIterations passed to ARPACK
    std::complex<double> _shift;  // Energy shift lambda: A -> A - lambda Id
    
protected:
    void _compute();

public:
    EigenSolverArpack(OperatorAbstract* Op): EigenSolverAbstract(Op), _tolerance(1.e-9), _maxIterations(5000), _shift(100.) {}

    void tolerance(double Tolerance){ _tolerance=Tolerance; } ///< set convergence tolerance ARPACK

    void maxIterations(unsigned int MaxIterations){ _maxIterations=MaxIterations; } ///< set maximum number of ARPACK iterations


    void shift(std::complex<double> Shift){ _shift=Shift; } ///< shift the problem to avoid obtaining zero's as lowest eigenvectors of sub-space problem

private:
    class ArpackWrapper: public Arpack{
    private:
        const OperatorAbstract* op;
        
        Coefficients x, tmp, y;
        std::vector<std::complex<double>* > pX, pY;

        std::complex<double> _shift;
    public:
        ArpackWrapper(const OperatorAbstract* Op, double Tolerance, unsigned int MaxIterations, std::complex<double> Shift);
        void apply(const std::complex<double>* X, std::complex<double>* Y);  

        std::complex<double> shift() const{ return _shift; }
    };
};

#endif // EIGEN_SOLVER_ARPACK_H
