// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef COEFFICIENTSMULTI_H
#define COEFFICIENTSMULTI_H

#include "coefficients.h"

// a simple implementation of multiple Coefficients
class OperatorAbstract;
/** \ingroup Coefficients */

/// @brief (incomplete) multiple Coefficients for given Index
class CoefficientsMulti
{
    std::vector<Coefficients> c;
public:
    CoefficientsMulti(){}
    CoefficientsMulti(const Index *,unsigned int Cols, std::complex<double> Val=0.);

    CoefficientsMulti & apply(std::complex<double> A,const OperatorAbstract & Op, CoefficientsMulti & C);
    UseMatrix innerProduct(const CoefficientsMulti & Rhs) const;
    CoefficientsMulti & operator*=(const UseMatrix & Mat);

    unsigned int vecs() const {return c.size();}
    const Coefficients & vec(unsigned int k) const {return c[k];}
    Coefficients & vec(unsigned int k) {return c[k];}
    void push_back(const Coefficients & C){c.push_back(C);}
    void clear(){c.clear();}
    Coefficients & back(){return c.back();}
    CoefficientsMulti & operator-=(const CoefficientsMulti & C){for(int k=0;k<c.size();k++)c[k]-=C.c[k];return *this;}
    CoefficientsMulti & operator+=(const CoefficientsMulti & C){for(int k=0;k<c.size();k++)c[k]+=C.c[k];return *this;}
    /// orthonormalize, remove singular vectors below threshold EpsRemove
    CoefficientsMulti & orthonormalize(const OperatorAbstract & Overlap,bool Pseudo=false,double EpsRemove=-1.);
};

#endif // COEFFICIENTSMULTI_H
