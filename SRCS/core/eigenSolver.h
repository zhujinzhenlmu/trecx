// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef EIGENSOLVER_H
#define EIGENSOLVER_H

#include <vector>
#include <string>
#include "tree.h"
#include "eigenSolverAbstract.h"

class OperatorAbstract;
class Index;

#include "operatorTree.h"

#include "arpack.h"
#include "coefficients.h"

/// \ingroup Linalg
/// \brief Eigen solver for OperatorAbstract - detects and exploits block-structure
///
/// optionally computes dual of right-eigenvectors
///
/// the projector onto the subspace of energies is
///
/// P = sum[i] rightEigenvector[i] dualVector[i].transpose()
///
class EigenSolver: public EigenSolverAbstract, public Tree<EigenSolver>
{   
    std::string _method;

    void expandInto(bool Dual, std::vector<Coefficients*> &CVec, std::vector<std::complex<double> > &Val, const std::complex<double>* PVal, UseMatrix &Vec,
                    double Emin, double Emax, bool ExcludeRange);
    std::complex<double> innerProductLeft(unsigned int I, const Coefficients* OvrTimesRight) const;
    void _compute();
    void _computeBlock(const OperatorTree *OpTree, const OperatorAbstract *Ov);


    ///\brief fix for unique phases and normalization (if ambiguous)
    ///
    /// general non-symmetric: sum_i |(R_m)_i|^2=1 for all m
    /// hermitian: coefficient at maximal component >0

    void collect(); // establish eigenvalues and eigenvectors on present level

public:
    /// Schmidt-style orthonormalize right eigenvectors and duals
    ///
    ///  Dual[i].innerProduct(Rvec[j])=0, starting from low indices, i.e. lowest are least modified
    static void orthonormalize(const std::vector<Coefficients*> &Dual,const std::vector<Coefficients*> &Rvec);

    /// defaults to all eigenvalues and right eigenvectors
    EigenSolver(){}

    /// compute eigenvalues in range
    EigenSolver(double Emin /** lowest eigenvalue real part to include */,
                double Emax /** highest eigenvalue real part to include */,
                bool RightVectors=true /** compute right eigenvectors */,
                bool DualVectors=false /** compute dual vectors */,
                bool ExcludeRange=false /** compute eigenvalues OUTSIDE [Emin,Emax] */,
                std::string Method="auto" /** Lapack, Arpack */
            )
        :EigenSolver(Emin,Emax,INT_MAX,RightVectors,DualVectors,ExcludeRange,Method){}

    EigenSolver(double Emin,double Emax,int Nmax,bool RightVectors, bool DualVectors, bool ExcludeRange, std::string Method);


    std::vector<std::complex<double> > eigenvalues() const;

    // Does NOT retain ownership of these
    std::vector<Coefficients *> rightVectors() const; ///< right hand eigenvectors right_j
    std::vector<Coefficients *> dualVectors() const; ///< duals of eigenvectors:  dual_i:=(left_i)^* S,  = dual_i.inner(right_j)=delta_ij

    ///\brief orthognalize eigenvectors (and matching duals) in degenerate subspaces
    void orthonormalizeDegenerate(double Eps,const std::vector<std::complex<double> > & Eval,const std::vector<Coefficients*> & Dual,const std::vector<Coefficients*> & Rvec);

    ///\brief normalize to L_n.dot(overlap*R_m)= delta_mn, choose unique
    void orthonormalize();

private:

    class Arp:public Arpack{

        static std::complex<double> shift; // subtract shift*S from Hamiltonian to move desired eigenvalues to below 0

        // internal data
        const OperatorAbstract* o;
        std::vector<std::complex<double> *> pX,pY;
        Coefficients x,y,tmp;
    public:
        std::complex<double> & ref_shift(){return shift;}
        void apply(const std::complex<double> *X, std::complex<double> *Y);
        Arp(const OperatorAbstract* O);
        /// return eigenvectors
        void eigen(std::vector<std::complex<double> > &Eval, std::vector<Coefficients* > &Rvec,
                   unsigned int Nvec, const std::string &Which, bool Restart);
    };


};

#endif // EIGENSOLVER_H
