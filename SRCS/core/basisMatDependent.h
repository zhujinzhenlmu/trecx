#ifndef BASISMATDEPENDENT_H
#define BASISMATDEPENDENT_H

#include <string>
#include "qtEigenDense.h"
#include "index.h"
#include "basisMatAbstract.h"


class BasisAbstract;
class UseMatrix;
/// 1-d multiplicative operator that depends on higher axis functions
/// (analoguous to BasisMatMulti, may be abstracted, limited performance and functionality at present)
class BasisMatDependent: public BasisMatAbstract
{
    void construct(std::string Op, const Index * IIndex, const Index* JIndex,std::vector<std::complex<double> > &Diag);
    void valsOnQuadgrid(const std::string TopAx, int Inflate, const Index* Idx,
                        std::vector<double> &Grid, std::vector<double> &Weig, std::vector<std::complex<double> > &Vals);
    std::complex<double> operAtQ(std::complex<double> Q, std::string Op, const Index* IIndex, const Index* JIndex);
    void _construct(std::string Op, const Index * IIndex, const Index* JIndex);
public:
    BasisMatDependent(){}
    BasisMatDependent(std::string Op, const Index * IIndex, const Index* JIndex){_construct(Op,IIndex,JIndex);}
    static std::string modify(const std::string Op, const Index *IIndex, const Index *JIndex); // Operator string name of the depedent axis inserted
};

#endif // BASISMATDEPENDENT_H
