// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef __PRIMITIVE_GAUSSIANS__
#define __PRIMITIVE_GAUSSIANS__

#include <complex>
#include <math.h>
#include "extra_functions.h"
#include "read_columbus_data.h"
#include "quantumChemicalInput.h"
#include <math.h>
#include <cmath>

///
/// \brief The primitive_gaussians class - Part of the COLUMBUS interface
///  Integral between gaussians using Gauss-Hermite quadrature
///

class primitive_gaussians{
 
 public:
  // parameters desribe gaussian orbital of form (x-cord)^powx (y-cordy)^powy (z-cordz)^powz exp(-exponent*r^2)
  std::string type;
  Eigen::VectorXd exponents;                     //gaussian exponent
  Eigen::Matrix<int, Eigen::Dynamic, 3> pow;            //powers
  Eigen::Matrix<double, Eigen::Dynamic, 3> coord;       //center coordinates 
  int lmax;
  int mmax;
  void gauss_gauss(Eigen::MatrixXcd &result, std::string name, primitive_gaussians *b);
  std::complex<double > gauss_1(int n,int ii,double ai,double ri,int jj,double aj,double rj);
  std::complex<double > gauss_k(int n,int ii,double ai,double ri,double kj,double bj);

  primitive_gaussians(columbus_data *col_data);
  primitive_gaussians(QuantumChemicalInput  & ChemInp);
  void values(Eigen::MatrixXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz);
  void values(Eigen::VectorXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, int fun_index);
  void derivatives(Eigen::MatrixXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, std::string wrt);
  void derivatives(Eigen::VectorXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, int fun_index, std::string wrt);  
  void matrix_1e(Eigen::MatrixXcd &result, std::string name, primitive_gaussians *b=NULL);
};

#endif
