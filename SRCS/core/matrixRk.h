#ifndef MATRIX_RK_H
#define MATRIX_RK_H

#include "hMatrix.h"

class MatrixRk: public HMatrix{
    unsigned int _rank;

    std::vector<std::complex<double> >* _v_decompress;
    std::vector<std::complex<double> >* _v_compress;
    std::vector<std::complex<double> >* _v_full;

    std::complex<double>* _decompress;
    std::complex<double>* _compress;
    std::complex<double>* _full;

    bool _use_full;

    void create(std::vector<std::complex<double> >& m);

protected:
    unsigned int storageRequirement() const;
    void storage(std::complex<double>* storage);

    std::vector<MatrixRk*> getLowrankMatrices();
public:
    MatrixRk(const HMatrix::HMatrixIndex* iIndex, const HMatrix::HMatrixIndex* jIndex, HMatrix::TruncationStrategy* strategy, const UseMatrix& mat);
    ~MatrixRk();

    void fullMatrix(UseMatrix& mat) const;

    void rankForAccuracy(double rel, double abs, const UseMatrix* mat=0);
    void rank(unsigned int rank);
    int rank() const;

    void axpy(std::complex<double> A, const std::complex<double>* X, std::complex<double> B, std::complex<double>* Y) const;
    long applyCount() const;

    // TEMPORARY CODE
    void structureTikZ(std::ostream* output) const;
};

#endif //MATRIX_RK_H
