#ifndef BASISMATOPERATOR_H
#define BASISMATOPERATOR_H

class BasisAbstract;
class ReadInput;

#include "basisMatMatrix.h"

class BasisMatOperator : public BasisMatMatrix
{
    std::string _operDef;
    std::string _refIndex;
public:
    BasisMatOperator(std::string Kind);
    void setup(const BasisAbstract * IBas, const BasisAbstract * JBas);
};

#endif // BASISMATOPERATOR_H
