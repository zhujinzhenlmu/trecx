// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef SYMMADAPORB_H
#define SYMMADAPORB_H

#include "atomicOrbital.h"

class ReadInput;
class AtomicOrbital;
class UseMatrix;

class SymmAdapOrb : public SingPartOrb
{
public:
    SymmAdapOrb(ReadInput & Inp);
}

#endif // SYMMADAPORB_H
