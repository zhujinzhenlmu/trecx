#ifndef VINAYDATA_H
#define VINAYDATA_H

#include "tools.h"
#include <string>
#include <vector>

class Coefficients;
class Index;
class BasisAbstract;

class VinayData
{
    static std::vector<const Index*> _allIdx;
    Coefficients * _c;
    Index* _idx;
    Index* getIndex(std::vector<std::vector<std::vector<double> > > Grids, std::vector<const BasisAbstract*> Bas, int Level);
    void readVec(std::ifstream & Inp, int Size, std::vector<double> & Vec);
    double _phiCEO;
    double _polarAngle;
    std::string _channel;
    std::string _fullGrid;
    std::string _sampleGrid;
public:
    ~VinayData();
    VinayData(std::string &FileName);
    const Coefficients* ampl() const {return _c;}
    const Index* idx() const {return _idx;}
    double phiCEO() const {return _phiCEO;}
    double polarAngle() const {return _polarAngle;}
    std::string channel(){return _channel;}
    std::string fullGrid(){return _fullGrid;}
    std::string sampleGrid(){return _sampleGrid;}
};

#endif // VINAYDATA_H
