// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORDEFINITION_H
#define OPERATORDEFINITION_H

#include <string>
#include <map>
#include <vector>

class Index;
class UseMatrix;

/** @defgroup OperatorData Definitions
 *  \ingroup Operators
 *  \brief definition strings, syntax, manipulation, and matrix evaluation
 *  @{
*/

/// define and manipulate standard operator strings (Laplacian, derivatives etc) for a range of coordinate systems
class OperatorDefinition : public std::string
{
    friend class OperatorDefinitionNew; // eventually this will be merged
// place constraint string C after the Pos'th "." in after $ConstType
    static std::string addConstraint(std::string Def, std::string ConstType, int Pos, std::string C);
    ///HACK - do not use
    static std::string opPermuted(const std::string &Op, const std::vector<unsigned int> Perm);
    OperatorDefinition axisToCoor(const std::string Hierarchy);///< if top-level is continuity-level, move factor to end
    std::string parentHierarchy(std::string Hierarchy);///< replace certain coordinates by what they were derived from
public:
    OperatorDefinition(std::string Def){assign(Def);} ///< use operator "as is" assuming it will match hierarchy
    OperatorDefinition(){}

    /// sort input string by hierarchy, do not sort if empty hierarchy
    OperatorDefinition(std::string Def,std::string Hierarchy);

    /// modify for use in tSurff
    OperatorDefinition & tsurffDropTerms(const Index* IIndex, const Index* JIndex);

    /// used for tsurff computations, modify base disc operator strings for semibound regions (duplicate of the above?)
    static void dropTerms(std::vector<std::string> &terms, const Index *IIndex, const Index *JIndex);
    static void dropTerms(std::vector<std::string> &terms, std::string Hierarchy);

    /// operator string for Hierarchy given in the form X.Y.Z or Phi1.Eta1.Phi2.Eta2
    /// if duplicate coordinates or pairs specXXX - kXXX, first term will be ingnored,
    /// e.g specA.Z.kA -> Z.kA, or Y.B.Y -> B.Y
    static std::string get(std::string Name, std::string Hierarchy);

    std::string parameter(OperatorDefinition & Remainder) const;
    static void setParameters(const std::string Definition);
    /// return name without << >> and trailing parameters, set parameter value
    static std::string extractParameters(const std::string Name);
    /// resolve brackets (without expanding standard operators)
    static std::string unBracket(std::string Term, std::string Front="",std::string Back="");

    static void truncationRadius(double Rsmooth, double Rtrunc); ///< too specialize for being here

    /// apply constraints to Mult, return remainder with adjusted contstraint, sanity checks on constraints
    void specialConstrain(UseMatrix & Mult,const Index * IIndex, const Index * JIndex) const;
    OperatorDefinition constrain(UseMatrix & Mult,const Index * IIndex, const Index * JIndex) const;
    OperatorDefinition constrainBand(UseMatrix & Mult,const Index * IIndex, const Index * JIndex) const;
    OperatorDefinition constrainBlock(UseMatrix & Mult,const Index * IIndex, const Index * JIndex) const;
    bool isStandard(const Index * IIndex, const Index * JIndex) const; ///< true for non-special operators
    bool isLocal(const Index * IIndex, const Index * JIndex) const; ///< local - no overlap between finite elements
    bool isSeparable() const;
    void syntax() const; ///< basic syntax check (very rudimentary for now)

    static std::string coordinates(const std::string  & Hierarchy);///< remove non-coordinate strings from hierarchy (i.e. remove FE and specXXX...)
    static void setup(); ///< set standard operators for a range of coordinate systems
    static std::map<std::string,std::map<std::string,std::string> >standardOperators;
    static void Test();

    void checkVariable(std::string Message, std::string Name) const;
};

/** @} */ // end group OperatorData

#endif // OPERATORDEFINITION_H
