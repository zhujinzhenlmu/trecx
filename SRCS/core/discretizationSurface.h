// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef DISCRETIZATIONSURFACE_H
#define DISCRETIZATIONSURFACE_H

#include <complex>
#include "discretizationDerived.h"
#include "operator.h"
#include "indexDerived.h"
#include "coefficientsFunction.h"

class OperatorAbstract;

/** \ingroup Discretizations */

/// surface values and derivatives derived from given Discretization
class DiscretizationSurface: public DiscretizationDerived{
public:
    static std::string prefix; ///< prefix surface output files by this
    static std::string prefixAx; ///< surface axis for coordinate CCC will be prefixAx+CCC

//    static void read(ReadInput & Inp,std::map<std::string,std::vector<double>);
    static DiscretizationSurface * factory(const Discretization *Parent, const std::vector<double> & Rad, unsigned int Nsurf);
    static std::vector<std::string>  surfaceFiles(std::string RunDir, const Index * Idx, std::string Region);


    ~DiscretizationSurface(){}
    DiscretizationSurface(const Discretization *Parent, const std::vector<double> & Rad, int NSurf,
                          std::string DerivativeSide="fromInside" /** derivative from side nearer to zero, else boundaryFromBelow, boundaryFromAbove */);
    DiscretizationSurface(const Discretization *Parent, const std::vector<double> &Rad, const std::string SurfaceFile, std::string DerivativeSide="fromInside");

    std::shared_ptr<const OperatorAbstract> sharedFromParent() const {return _mapFromParent;}

    class IndexS: public IndexDerived {
        // one of the FE axes of the original discretization is converted as follows:
        // - the "continuity level" CCC is replaced by one branch for each surface radius and renamed surfCCC
        //   (it would be good to have the Basis contain the radii, but for now this is just numbered)
        // - the matching floor level is converted to a 2-component Basis ValDer and with value and derivative at the surface
        // - below an exact copy of the remaining index tree is attached
    public:
        IndexS(const Index* I, std::vector<double> Radius, unsigned int NSurf,std::string DerivativeSide="fromInside");
    };
    IndexS * wIdx;

    class MapSurface:public OperatorTree {
    public:
        MapSurface(const Index* SurfI, const Index* FromI);
        void update(double Time, const Coefficients* CurrentVec=0){}
    };

    class Map:public Operator {
        bool _valDerLevel;
        std::vector<std::complex<double> > val,der;
        std::vector<unsigned int> elem;
        Index * viewI, *viewSurfI;
        Coefficients* temp,*viewTemp, *tempSurf, *viewTempSurf; // view into the floor of X (if needed)
    public:
        ~Map(){delete viewI,viewSurfI,temp,viewTemp,tempSurf,viewTempSurf;}
        Map(const Index* SurfI, const Index* FromI);
        void apply(std::complex<double> Alfa, const Coefficients &X, std::complex<double> Beta, Coefficients &Y) const ;
        /// dummy update function for surface
        void update(double Time, const Coefficients* CurrentVec=0){}
    };
};

#endif // DISCRETIZATIONSURFACE_H
