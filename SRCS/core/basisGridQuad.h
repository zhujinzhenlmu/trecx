#ifndef BASISGRIDGUAD_H
#define BASISGRIDGUAD_H

#include <vector>

#include "basisSet.h"
#include "basisGrid.h"

/// grid with a quadrature rule
class BasisGridQuad : public BasisGrid
{
    static std::map<std::vector<double>,const BasisGridQuad*>_allBasis;
protected:
    BasisGridQuad(const std::vector<double> Mesh,const std::vector<double> Weights):BasisGrid(Mesh),_weights(Weights){}
    BasisGridQuad(const BasisAbstract * Grid);
    std::vector<double> _weights;
public:
    static const BasisGridQuad* factory(const std::string Definition);
    static const BasisGridQuad* factory(const BasisAbstract * Grid);
    static const BasisGridQuad* factory(const std::vector<double> Mesh, const std::vector<double> Weights);
    const BasisGridQuad *append(const BasisAbstract* Grid) const;

    std::string strDefinition() const; ///< string fully defines basis
    const std::vector<double> & weights() const {return _weights;}
};

#endif // BASISGRIDGUAD_H
