// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef SURFACEFLUX_H
#define SURFACEFLUX_H

#include "tools.h"
#include "wavefunction.h"
#include <memory>

/*!
 * A special wavefunction
 * Handles Surface files
 * (1) Reads surfaces, maintains the buffer
 * (2) Provides the UpdateSurface(time) - which interpolates existing surfaces and
 *        returns surface at the required time
 */

// Forward declarations
class ReadInput;
class NewtonInterpolatorWF;
class DiscretizationSurface;
class CoulXFlux;

class SurfaceFlux: public Wavefunction
{
    int readSize; ///< number surface values read in at once
    std::ifstream _surfaceStream;

    double _timeBegin; //!< time of first wave function on file
    double _eps; //!< for float comparisons
    int _interSize;
    std::vector<Wavefunction*> _inputBuffer;
    NewtonInterpolatorWF* _interPol; //!< Interpolator
protected:
    DiscretizationSurface* _surfaceDisc; //!< surface discretization, (Should not be here in principle!!)
    std::shared_ptr<const Index>_idx; ///< surface index
public:
    SurfaceFlux():_interPol(0),_timeBegin(DBL_MAX){}
    SurfaceFlux(ReadInput &inp, DiscretizationSurface* SurfD, std::string Region, int NSurf);
    SurfaceFlux(const std::string SurfaceFile, int InterSize=4, int ReadSize=500);
    virtual ~SurfaceFlux();

    virtual bool update(double time, bool Derivative=false);     ///< Returns wavefunction at time, returns false if unsuccessful
    virtual double FluxBufferBeginTime() const;
    virtual const DiscretizationSurface * discSurf(){if(not _surfaceDisc)DEVABORT("_surfaceDisc is obsolescent");return _surfaceDisc;}
    const Index* idx() const; ///< Index for flux Coefficients

};

#endif // SURFACEFLUX_H
