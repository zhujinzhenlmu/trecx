#ifndef OPERATORFLOORINVERSE_H
#define OPERATORFLOORINVERSE_H

#include "operatorFloor.h"
#include "useMatrix.h"

class OperatorFloorInverse:public OperatorFloor
{
    // results from LU factorization; requires "friend class OperatorFloorInverse;" in useMatrix.h!
    UseMatrix *lumat; // L and U matrices
    std::vector<int> ipiv; // pivots
    unsigned int subD,superD;
    bool bandOvr; // banded overlap or not

public:
    OperatorFloorInverse(const Index *Idx, unsigned int SubD, unsigned int SuperD, bool BandOvr);
    ~OperatorFloorInverse();
    void axpy(const std::complex<double> & Alfa, const std::complex<double> *X, unsigned int SizX,
                      const std::complex<double> & Beta, std::complex<double> *Y, unsigned int SizY) const;
    void pack(std::vector<int> &Info, std::vector<std::complex<double> >&Buf) const {DEVABORT("Not implemented");}
};

#endif // OPERATORFLOORINVERSE_H
