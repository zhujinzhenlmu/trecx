#include "qtEigenDense.h"
#include "index.h"
#include "basisDvr.h"

class TestIntegration
{
public:
    TestIntegration();
    Eigen::MatrixXd GetBasisCoeff(const Index* idx);
    double IntegrateMonomial(int power,double UpBound, double LowBound);
    double IntegratePolynomial(Eigen::VectorXd C, double UpBound, double LowBound, int powerofX); //int b x^power
    double IntegrateLowerTriangle(Eigen::VectorXd C1, Eigen::VectorXd C2,double UpBound,double LowBound);
    double IntegrateUpperTriangle(Eigen::VectorXd C1, Eigen::VectorXd C2,double UpBound,double LowBound);
    double IntegrateElementEE(const Index *idx, int i, const Index *jdx, int j, std::string type);
};
