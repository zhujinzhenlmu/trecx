// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef PARALLELBUFFER_H
#define PARALLELBUFFER_H

#include "mpiWrapper.h"

class Coefficients;
class ParallelBuffer:public MPIwrapper::Buffer
{
    void add(const std::complex<double>* Data,int Size){
        val.insert(val.end(),Data,Data+Size);
    }
    int extract(int Pos,std::complex<double>* Data, int Size){
        for(int k=0;k<Size;k++)Data[k]=val[Pos+k];
        return Pos+Size;
    }

public:
    ParallelBuffer(){}
    void resize(int Size){val.resize(Size);}

    void add(const std::vector<std::complex<double> > & Data){add(Data.data(),Data.size());}
    int extract(int Pos, std::vector<std::complex<double> >&Data){return extract(Pos,Data.data(),Data.size());}

    void add(const Coefficients* C);
    int extract(int Pos, Coefficients* C);

    void bcast(int From){MPIwrapper::Bcast(val.data(),val.size(),From);}
    void allGather();
};

#endif // PARALLELBUFFER_H
