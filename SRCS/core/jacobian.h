// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#ifndef JACOBIAN_H
#define JACOBIAN_H
#include "abort.h"
#include "useMatrix.h"
#include "str.h"
#include <memory>

class Jacobian{
public:
    virtual ~Jacobian(){}
    virtual void operator()(const UseMatrix & QuadX, UseMatrix & QuadW) const=0;
    virtual std::string kind() const=0;
    static const Jacobian* factory(std::string Kind,std::complex<double> Eta);
};

class Jacobian1:public Jacobian{
public:
    Jacobian1(std::complex<double> Eta){}
    std::string kind() const{return "1";}
    void operator()(const UseMatrix & QuadX, UseMatrix & QuadW) const{}
};

class JacobianQQ:public Jacobian{
public:
    std::string kind() const{return "QQ";}
    JacobianQQ(std::complex<double> Eta){}
    void operator()(const UseMatrix & QuadX, UseMatrix & QuadW) const{QuadW.cwiseProductIn(QuadX);QuadW.cwiseProductIn(QuadX);}
};

class JacobianQ:public Jacobian{
public:
    std::string kind() const{return "Q";}
    JacobianQ(std::complex<double> Eta){}
    void operator()(const UseMatrix & QuadX, UseMatrix & QuadW) const{QuadW.cwiseProductIn(QuadX);}
};

#endif // JACOBIAN_H
