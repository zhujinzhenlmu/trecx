// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef INITIALSTATE_H
#define INITIALSTATE_H

#include <string>

class OperatorTree;
class Discretization;
class DerivativeFlat;
class Wavefunction;
class Plot;
class Coefficients;
class ReadInput;

class InitialState
{
    static void productFunction(std::string InitialKind, Coefficients & C);
    static void indexOrbital(std::string InitialKind, Coefficients & C);
    static void externalOrbital(std::string InitialKind, Coefficients & C);
    static void subblockEigen(std::string InitialKind, const OperatorTree* Op, Coefficients & C);
public:
    static std::string readKind(ReadInput & Inp);

    /// initial state into wf - switch between various methods
    static Wavefunction get(const Discretization *D, std::string initialKind /** choices: ZERO, Hinitial, atBegin */,
                            int initialN /** N'th excited state */,
                            double tBeg /** time if InitialOper is time-dependent */,
                            const OperatorTree &InitialOper /** initial state can be eigenvector of this operator */,
                            DerivativeFlat * derNew /** time-evolution operator (for initialKind="atBegin") */);
    InitialState();
};

#endif // INITIALSTATE_H
