// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef CIION_H
#define CIION_H

#include "basisFunction.h"
#include "mol_sae_shelf.h"
#include "basisfunctioncineutral.h"
#include "multiArrayWrapper.h"

class mo;
class ci;
class My4dArray_shared;
class Index;

/** \ingroup Basissets */

/// \brief CI ionic basis functions
///
///  Additionally, handles cross-terms b/w neutral and ionic channels
/// Spin muliplicity: is handled through spi, lc_factors data members

class BasisFunctionCIion : public BasisFunction
{
  mo *orbitals;
  ci *CI;
  /// Storage
  mutable std::map<std::string,UseMatrix> onepMat;
  mo_store<Eigen::Vector2i> rho2_X_op_storage;
  mutable mo_store<int > eta3_X_op_storage;

  UseMatrix matrixWithNeutrals_helper(string def, const Index *chan, const Index *neut);

public:

  /// Public data members
  int multIon;                   ///< multiplicity of ion
  int nElectrons() const;        ///< number of electrons in ion

  /// Rho matrics: putting together \rho^IJ
  Eigen::MatrixXd Rho;                       /// used by sinv_helperdisc and coefficients_wbc_sinv
  Eigen::MatrixXd Rho3;                      /// used in sae_disc

  /// Helpers for spin linear combinations
  std::vector<double > lc_factors;          /// Linear combination for spin multiplicity
  std::vector<int > spin;                   /// spin of active electron: 0 - down spin, 1 - up spin
  void setup_spin(ci *ion);                 /// Initializes spin list
  void setup_lc_factors();                  /// Initializes lc_factors

  BasisFunctionCIion(int order);
  ~BasisFunctionCIion();
  void valDer(const std::complex<double> &X, std::vector<std::complex<double> > &Val, std::vector<std::complex<double> > &Der, bool ZeroOutside) const;

  /// public data members, reduced density matrices
  std::vector<std::vector<Eigen::MatrixXd > > rho1,rho3_vee;
  std::vector<std::vector<My4dArray_shared* > > rho2_lc;
  Eigen::MatrixXd ion_2e;

  UseMatrix matrix_1e(std::string def) const;  
  void setup_rho2_X_op(Eigen::MatrixXcd &result, string def, int I_i, int I_j);

  /// cross terms with Neutrals
  std::vector<std::vector<Eigen::VectorXd> > eta1,eta5_X_Vee;
  std::vector<std::vector<multiarray3d > > eta3;

  void setup_crossterms(My4dArray_shared *Vee, BasisFunctionCINeutral* CIneut);
  void setup_eta3_X_op(Eigen::MatrixXcd &result, string def, int I_i, int numN);
  UseMatrix matrixWithNeutrals(std::string def, const Index* chan, const Index* neut);
};

#endif // CIION_H
