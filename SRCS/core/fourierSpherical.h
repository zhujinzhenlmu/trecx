#ifndef FOURIERSPHERICAL_H
#define FOURIERSPHERICAL_H

// Transform the coefficients from space distribution to momentum distribution by using fourier transformation with the plane wave
// e^{i\mathbf{k}\cdot\mathbf{x}} = 4\pi\sum_{l=0}^\infty\sum_{m=-l}^l i^lj_l(kr)Y_{lm}(\theta,\phi)Y_{lm}^*(\vartheta,\varphi)
//                                = 4\pi\sum_{l=0}^\infty\sum_{m=-l}^l i^lj_l(kr)Y^*_{lm}(\theta,\phi)Y_{lm}(\vartheta,\varphi)
// and the transformation can be written as
// \mathscr{F}f(\mathbf{k}) = (2\pi)^{-3/2}\int d^3x\hspace{2pt} e^{i\mathbf{k}\cdot\mathbf{x}}f(\mathbf{x})
// = (2\pi)^{-3/2}\int d^3x\left(4\pi\sum_{l,m}i^lj_l(kr)Y^*_{lm}(\theta,\phi)Y_{lm}(\vartheta,\varphi)\right)\left(\sum_{l',m'}g_{l'm'}(r)Y_{l'm'}(\theta,\phi)\right)
// So the result is
// \mathscr{F}f(k,\vartheta,\varphi) = \sqrt{\frac{2}{\pi}}\sum_{l,m}i^lY_{lm}(\vartheta,\varphi) \cdot \int j_l(kr)g_{lm}(r)r^2dr.

#include <complex>

class Wavefunction;
class Discretization;
class ReadInput;
class Coefficients;
class Algebra;
class DiscretizationGrid;
class Plot;
class DiscretizationTsurffSpectra;

using namespace std;

class FourierSpherical {
    unsigned int lmax, mmax;
    vector<vector<vector<vector<complex<double>>>>> besselFuncs;
    vector<double> kGrids;
    vector<vector<double>>rGrids, rWeights;
    vector<unsigned int> phiDepth, etaDepth, rDepth;
    DiscretizationGrid *Dg;
    DiscretizationTsurffSpectra *D;
    Coefficients *coefMom, * coefGrid;
    Plot *plotUp, * plotDn;
public:
    FourierSpherical(ReadInput inp, const Discretization *Din);
    Coefficients tranform(const Coefficients coeff);
    void transCoef(Coefficients *coefR, Coefficients *coefK);
    void transFloor(complex<double> *fData, complex<double> *fDatasMom, vector<unsigned int> ls, vector<unsigned int> intervals);
    void genPlot6D(unsigned int NEta, unsigned int diffRange);
    void plotTransform(string fileName, string side, const Coefficients coefIn);
};

#endif
