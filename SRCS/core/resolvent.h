// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef RESOLVENT_H
#define RESOLVENT_H

#include <complex>

#include "operatorAbstract.h"
#include "tree.h"
#include "Eigen/SparseLU"

class TriFactor;
class OperatorTree;
class Resolvent : public Tree<Resolvent>, public OperatorAbstract
{
    std::complex<double> _z;
    TriFactor * factor; // the actual factorization at the leaf level
//    Eigen::
//    * SparseLU<SparseMatrix<scalar, ColMajor>, COLAMDOrdering<Index> >   solver;
//    * // fill A and b;
//    * // Compute the ordering permutation vector from the structural pattern of A
//    * solver.analyzePattern(A);
//    * // Compute the numerical factorization
//    * solver.factorize(A);
//    * //Use the factors to solve the linear system
//    * x = solver.solve(b);
    Eigen::SparseLU<Eigen::SparseMatrix<std::complex<double> > > solver;
public:
    ~Resolvent();
    Resolvent():factor(0){}
    /// Resolvent of Op at Z, i.e. (Op-Z)^-1
    Resolvent(const OperatorTree * Op, std::complex<double> Z);
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;

    std::complex<double> z() const {return _z;}

    bool verify(const OperatorTree * Op, double Epsilon=1.e-12) const;
};

#endif // RESOLVENT_H
