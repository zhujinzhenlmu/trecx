#include "operatorFloor.h"
#include "basicDisc.h"
#include "readInput.h"
#include "useMatrix.h"
#include "operatorZD.h"

#include "operator.h"
#include "basisSet.h"
#include "gaunt.h"
#include "coefficientsFloor.h"
#include "index.h"
#include "qtAlglib.h"
#include "radialmultipole.h"
#include "basisMat.h"
#include "inverseDvr.h"
#include "basisMat1D.h"
#include "eigenNames.h"
#include "indexNew.h"
#include "basisDvr.h"

class OperatorMeanEEold : public OperatorFloor
{
    std::vector<double> _storedRho; // rho at dvr-points
    std::vector<std::complex<double>> _storedCoeff; // stored coeff

    std::vector<double> _basSq;   // norm of basis values at DVR points
    std::vector<std::complex<double>> _GP; // product of basis norms squared with coefficient norms
    std::vector<double> _dvrPoints;
    std::vector<double> _dvrWeights;
    std::vector<double> _nodeVal;

    int _nBeg;
    int _nSibling;
    int _levelSize;

    //std::vector<std::vector<std::complex<double>>> _upTriangle;
    std::vector<std::complex<double>> _lowTriangle;
    std::vector<std::complex<double>> _upTriangle;

    double _JupBoundary;
    double _JlowBoundary;

    double _LowTriangle;
    double _UpTriangle;

    const BasisDVR * bi;
    const BasisDVR * bj;

    void calculateIup();
    void calculateIdown();
    void calculateJup(int SizX);
    void calculateJdown(int SizX);
    void calculateLowTriangle(int SizX, int i);
    void calculateUpTriangle(int SizX, int i);

public:
    OperatorMeanEEold(const std::string Name, const std::string Def, const Index *IIndex, const Index *JIndex);
    OperatorMeanEEold(const std::vector<int> &Info, const std::vector<std::complex<double> > &Buf);
    OperatorMeanEEold(std::string Pot, const Index* IIndex, const Index* JIndex, std::complex<double> Multiplier);

    void axpy(const std::complex<double> & Alfa, const std::complex<double>* X, unsigned int SizX,
              const std::complex<double> & Beta, std::complex<double>* Y, unsigned int SizY) const;

    void pack(std::vector<int> & Info,std::vector<std::complex<double> > &Buf) const;

    //Switchers
    static bool _iterations;
    static bool _store;
    static bool _noInteraction;
    static std::complex<double> _matrixElement;
    static std::vector<double> Jdown;
    static std::vector<double> Jup;
};

