#ifndef OPERATORFLOORNONLIN_H
#define OPERATORFLOORNONLIN_H

#include "operatorFloor.h"

class OperatorFloorNonLin: public OperatorFloor
{
public:
    OperatorFloorNonLin(std::string Kind):OperatorFloor(Kind){}
    virtual void updateNonLin(double Time, const Coefficients* C)=0;
};

#endif // OPERATORFLOORNONLIN_H
