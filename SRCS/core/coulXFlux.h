#ifndef COULXFLUX_H
#define COULXFLUX_H

#include "surfaceFlux.h"
#include <memory>

class DiscretizationSurface;
class DiscretizationCoulXNew;
class TimePropagator;
class SurfaceConstraint;
class Plot;
class CoulXFlux : public SurfaceFlux
{
    Discretization * _disc;            // discretization for time-propagation under constraint
    DiscretizationSurface * _surfRc;   // surface at Rc for CoulX discretization
    SurfaceFlux * _inputFlux;
    Wavefunction _bWf;
    TimePropagator* _prop;
    double _time;

    //HACK - for debug
    static double _currentTime;
    Coefficients* intFlux;
    void integrateSource(double Time);
    SurfaceConstraint * _constr;

    std::shared_ptr<Plot> _plot;
    double _plotInterval,_previousPlot;
    std::string _plotFile;

public:
    ~CoulXFlux(){delete _inputFlux,_bWf,_disc,_surfaceDisc,_prop;}
    CoulXFlux(ReadInput &Inp, DiscretizationSurface* SurfD, std::string Region, int NSurf);
    bool update(double Time, bool Derivative=false);     ///< returns wavefunction at time, returns false if unsuccessful
    double FluxBufferBeginTime() const {return _inputFlux->FluxBufferBeginTime();}
};

#endif // COULXFLUX_H
