// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef __MY4DARRAY__
#define __MY4DARRAY__

#include "qtEigenDense.h"
#include <iostream>
//#include <boost/multi_array.hpp>
#include "tools.h"
using namespace std;
//#include "eigenNames.h"


class My4dArray{

 public:
   bool isZero;

  //Eigen::VectorXcd A;
  int d1,d2,d3,d4;    //A is of dimension d1 x d2 x d3 x d4
  //dummies to enable compilation - need rewriting
 void resize(int d1, int d2, int d3, int d4){}
 double val_at(int l1, int l2, int l3, int l4){}
 void addto(int l1, int l2, int l3, int l4, double num){}
 std::vector<double>A;
  /*
  boost::multi_array<double ,4> A;

  My4dArray(){ isZero = false; }

  int get_d1() {return d1;}
  int get_d2() {return d2;}
  int get_d3() {return d3;}
  int get_d4() {return d4;}

  void resize(int d1, int d2, int d3, int d4){
    //int dims;
    //dims = d1*d2*d3*d4;
    this->d1 = d1;
    this->d2 = d2;
    this->d3 = d3;
    this->d4 = d4;
    A.resize(boost::extents[d1][d2][d3][d4]);
    
    for(int i=0;i<d1;i++)
      for(int j=0;j<d2;j++)
        for(int k=0;k<d3;k++)
          for(int l=0;l<d4;l++)
            A[i][j][k][l] = 0;

    isZero = false;
  }

  void assign(int l1, int l2, int l3, int l4, double num){

    if(isZero) ABORT("My4dArray::assign - matrix readonly");
    A[l1][l2][l3][l4] = num;

  }

  void addto(int l1, int l2, int l3, int l4, double num){

    if(isZero) ABORT("My4dArray::assign - matrix readonly");
    A[l1][l2][l3][l4] += num;

  }

  double val_at(int l1, int l2, int l3, int l4){
   
    if(isZero) return 0;

    return A[l1][l2][l3][l4];

  }

  void remove_zero(){

     // helper function - usable for matrices that are setup once and not modified again. If entire matrix is zero,
     // it deallocates memory and declares the matrix as readonly

    bool iszero = true;

    for(int i=0;i<d1;i++)
      for(int j=0;j<d2;j++)
        for(int k=0;k<d3;k++)
          for(int l=0;l<d4;l++){
            if(abs(A[i][j][k][l]) > 1e-14){
                iszero = false;
                break;
              }
            }

    if(iszero){
        A.resize(boost::extents[0][0][0][0]);
        isZero = true;
      }
    //cout<<isZero<<endl;
  }

  double dot(My4dArray& b){

    //returns \sum_{a,b,c,d} this(a,b,c,d)*b(a,b,c,d)

    if(this->isZero or b.isZero) return 0;

    if(this->d1!=b.d1 or this->d2!=b.d2 or this->d3!=b.d3 or this->d4!=b.d4){
        std::cout<<"Dimension don't match - cannot perform this operation";
        return 0;
      }

    double res=0;
    int shift_add =d3*d4;
    int shift=0;
    for(int i=0;i<d1;i++)
      for(int j=0;j<d2;j++){
        res+=(Eigen::Map<Eigen::MatrixXd>(A.data()+shift,d3,d4).array()*Eigen::Map<Eigen::MatrixXd>(b.A.data()+shift,d3,d4).array()).matrix().sum();
        shift+=shift_add;
        }
    return res;
  }

  void print(){

    if(this->isZero) return;
    // prints nonzero elements
    for(int i=0;i<d1;i++)
      for(int j=0;j<d2;j++)
        for(int k=0;k<d3;k++)
          for(int l=0;l<d4;l++){
            if(abs(A[i][j][k][l]) > 1e-14)
              cout<<i<<"  "<<j<<"  "<<k<<"  "<<l<<"  "<<A[i][j][k][l]<<endl;
            }

  }

  double * data() {return A.data();}
  */
};

#endif
