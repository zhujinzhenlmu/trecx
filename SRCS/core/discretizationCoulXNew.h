#ifndef DISCRETIZATIONCOULXNEW_H
#define DISCRETIZATIONCOULXNEW_H

#include "discretizationDerived.h"

class DiscretizationCoulXNew:public DiscretizationDerived
{
public:
    DiscretizationCoulXNew(const Discretization *SurfDisc, double RC, double RMax, const std::vector<double> KGrid, bool BandOvr, bool PureBessel=false);
    OperatorAbstract*& getMapTo(std::string To){DEVABORT("cannot map to Parent");}
};

#endif // DISCRETIZATIONCOULXNEW_H
