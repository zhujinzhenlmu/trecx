// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORTENSORPRODUCT_H
#define OPERATORTENSORPRODUCT_H

#include <string>
#include <vector>
#include <complex>

class UseMatrix;

#include "operatorFloor.h"

/// abstract base for all tensor products
class OperatorTensorProduct: public OperatorFloor{
protected:
    std::vector<std::vector<std::complex<double> > * >facDat; ///< pointers for tensor factors
    std::vector<unsigned int> subRows,subCols; // dimension information of tensor factors
protected:
    void axpy(const std::complex<double> & Alfa, const std::complex<double>* X, unsigned int SizX,
              const std::complex<double> & Beta, std::complex<double>* Y, unsigned int SizY);
public:
    OperatorTensorProduct(std::vector<const UseMatrix *> Mat, std::string Kind);
    OperatorTensorProduct(const std::vector<int> &Info, const std::vector<std::complex<double> > &Buf, std::string Kind);
    void pack(std::vector<int> &Info, std::vector<std::complex<double> > &Buf) const;
    void addDat(const std::vector<std::complex<double> > &Buf, unsigned int Siz0,unsigned int Siz1);
};


#endif // OPERATORTENSORPRODUCT_H
