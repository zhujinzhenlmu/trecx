#ifndef BASISMATABSTRACT_H
#define BASISMATABSTRACT_H

#include "qtEigenDense.h"
#include "index.h"

class BasisMatAbstract
{
protected:
    Eigen::MatrixXcd _mat;
    virtual void _construct(std::string Op, const Index * IIndex, const Index* JIndex)=0;
public:
    BasisMatAbstract(){}
    bool isEmpty() const {return _mat.size()==0;}
    const UseMatrix useMat() const;
    const std::vector<const Eigen::MatrixXcd*> mats() const;
public:
};

#endif // BASISMATABSTRACT_H
