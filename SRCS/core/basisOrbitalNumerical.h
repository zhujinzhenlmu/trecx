#ifndef BASISORBITALNUMERICAL_H
#define BASISORBITALNUMERICAL_H

#include "basisOrbital.h"

class Index;
class VectorValuedFunction;

/** \ingroup Basissets */
///@brief Generate BasisOrbital from function
class BasisOrbitalNumerical : public BasisOrbital
{
protected:
    std::string _refName;
    std::string _funcDef;
    std::vector<int> _select;
public:
    static void  setup(); ///< setup CO2 basis (HACK for now)
    BasisOrbitalNumerical(const BasisSetDef &Def);
    unsigned int size() const {return _select.size();}
    unsigned int order() const {return size();}
    const std::vector<int> & select() const {return _select;}

    ///@brief Constructor
    ///
    /// Def is FuncDef:RefIndex,
    /// <br>e.g. Phi.Eta.Rn:1.Q.exp[-1]:main (main reference Index is set up in main_trecx)
    /// <br>MO:complement...MO must have been defined by VectorValuedFunctions::add
    /// <br>             ..."complement" index must have been set previously, e.g. for a hybrid Index
    /// Coordinates must match the Index axes
    BasisOrbitalNumerical(std::string Def, int NumberOfOrbitals, int First=0 /** first orbital to use */);

    BasisOrbitalNumerical(std::string Def, std::vector<int> Select={});
    void generateOrbitals(const Index* Idx=0);
    void print(std::string Title="") const;
    void plot() const; ///< plot, if Plot is defined in ReadInput::main
    std::string str(int Level=0) const;
    bool operator==(const BasisAbstract & Other) const;
    const VectorValuedFunction* orbitalFunctions() const;
};

#endif // BASISORBITALNUMERICAL_H
