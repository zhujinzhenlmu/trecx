#ifndef BASISHF_H
#define BASISHF_H

#include "basisOrbitalNumerical.h"

class BasisHF : public BasisOrbitalNumerical
{
public:
    BasisHF(int NOrbs, const Index* Idx);//:BasisOrbital("HF"){_orb.resize(NOrbs);}
    void generateOrbitals(const Index* Idx=0){} ///< here dummy, construction directly
    void reset(const std::vector<const Coefficients*> &Orbs) const;
};

#endif // BASISHF_H
