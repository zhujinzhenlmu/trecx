#ifndef TSURFF_SOURCE_NEW_H
#define TSURFF_SOURCE_NEW_H

#include <vector>

#include "operatorTree.h"
#include "operatorFloor.h"
#include <memory>

class SurfaceFlux;
class ReadInput;
class DiscretizationSurface;
class Wavefunction;
class DiscretizationTsurffSpectra;
class OperatorAbstract;
class VolkovGrid;
class OperatorTreeVolkov;
class Coefficients;
class CoefficientsPermute;

class TsurffSource{
    SurfaceFlux* flux;

    Coefficients* tempSmoothSpecDisc;
    Coefficients* wfSmoothSpecDisc;
    std::shared_ptr<Coefficients> _currentSourceDamped;
    std::shared_ptr<CoefficientsPermute> _x12Source
    ;
    Coefficients* wfSurface;

    OperatorAbstract* commutator;
    OperatorAbstract* volkovPhase;

    double _currentTime,_turnoffRange,_endIntegration; ///< linearly turn off source across this time interval

public:
    TsurffSource(const DiscretizationTsurffSpectra* SmoothSpecDisc,
                 ReadInput& Inp, std::vector<DiscretizationSurface*> surfD, std::string Region, int SurfNumber);
    TsurffSource(const std::vector<std::string> &AxisPath, const Index* Parent, ReadInput& Inp);

    ///\brief generates Index's of Surface and Source according to AxisPath
    ///
    /// source will be from surface defined by all except last AxisPath
    static void generate(const std::vector<std::string> AxisPath, ReadInput& Inp, const Index* Parent, Index* & Surface, Index* & Source);

    CoefficientsLocal *UpdateSource(double Time);
    CoefficientsLocal *CurrentSource();

    const DiscretizationTsurffSpectra* smoothSpecDisc; //!< spectral discretization
    double SourceBufferBeginTime();
    const Index* idx() const; ///< source Index (i.e. surface converted to spectrum)
    std::vector<double> surfaceRadius(const std::string Axis) const; ///< surface radii (at present only 1)
    static std::string nextRegion(ReadInput & Inp, std::string Region); ///< find next spectral region to compute
    static std::vector<std::string> unboundAxes(ReadInput & Inp);

    double currentTime() const {return _currentTime;}
    std::string str() const; ///< energy range and points, turn-off interval
    void print(std::string SourceFile) const; ///< pretty print details of the source

    static void allReads(ReadInput & Inp);
    static bool readSymmetry12(ReadInput & Inp);
    void readTurnOff(ReadInput & Inp, double &EndProp);

};
#endif
