// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef SPECTRUMCOULOMB_H
#define SPECTRUMCOULOMB_H

#include "vectorReal.h"

#include "qtEigenDense.h"
#include "evaluator.h"


class OperatorDiagonal;
class ReadInput;
class DiscretizationSpectral;

class SpectrumCoulomb
{

    // recursive
   void map(Coefficients *CGrid, Coefficients *Spec, std::vector<std::vector<double> > &TransL);

   /// radial bound states of the Hydrogen atom, orthonormalized
   double hydrogenRadialBound(int L,int Nmax, double Radius, double Charge=1.) const;
   /// radial scattering waves of the Hydrogen atom, delta-normalized
   std::vector<double> hydrogenRadialScatter(int Lmax,double K, double Radius, double Charge=1.) const;
   bool hydrogenTest() const;

   std::vector<double> kFit; // k-grid used for fitting

public:
    SpectrumCoulomb(ReadInput & Inp, ReadInput & RunInp);
    void setup(const Discretization * D);
    bool compute(const tSurffTools::Evaluator & TSurff, Coefficients &Spec);

    static void svd(const Eigen::MatrixXd &A, Eigen::MatrixXd & U, Eigen::VectorXd & Sing, Eigen::MatrixXd &Vtrans);
    static void svd(const Eigen::MatrixXcd &A, Eigen::MatrixXcd & U, Eigen::VectorXd &Sing, Eigen::MatrixXcd &Vadj);
};

#endif // SPECTRUMCOULOMB_H
