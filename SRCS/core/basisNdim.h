// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASISNDIM_H
#define BASISNDIM_H

#include "basisAbstract.h"
#include <map>
#include "useMatrix.h"
#include "complexScaling.h"

class CoordinateTrans;
class OperatorDefinition;
class Index;
class OperatorTree;

/** \ingroup Basissets */

/// Basis function depending on several arguments
class BasisSet;
class BasisNdim : public BasisAbstract
{
    static std::map<const std::string,std::map<const Index*,std::map<const Index*,UseMatrix> > > opers;
    static std::map<std::string,const BasisAbstract*> bases;
    static std::map<std::string,const OperatorTree*> basicOp;

protected:
    std::string _ndimCoor; // coordinates wrt to basis
    std::string _quadCoor; // coordinates for _quadGrid
    std::vector<std::vector<double> > _quadGrid; // quadrature points in terms of the main coordinate system
    std::vector<double> _quadWeig; // quadrature weight wrt to main coordinate systems (and integration measure)
    std::vector<std::vector<std::vector<std::complex<double> > > > _valDer; //  value and derivative of all basis functions at all quadrature points

    std::vector<ComplexScaling> _comSca;

    // essential properties of any Ndim basis
    virtual std::vector<double> toCartesian(const std::vector<double>&CoorNdim) const=0; ///< map point from Ndim to reference coordinates
    virtual Eigen::MatrixXd jacobianToNdim(const std::vector<double>&CoorNdim) const=0; ///< Jacobian d(ref coords)/d(ndim coords) at CoordNdim
    virtual double absFactor(const std::vector<double>&CoorNdim) const=0; ///< extra integration factor at point (see tsurff.pdf for details)
    virtual double nablaFactor(const std::vector<double>&CoorNdim,int I) const=0; ///< partial derivatives of integration factor

    /// quadrature grid and weights for tree-product basis
    void productGridWeig(const Index * Idx, std::vector<std::vector<double> > & Grid, std::vector<double> & Weig, int MinQuad,
                            std::vector<double> GridK=std::vector<double>(), double WeigK=1.);
    void mainQuadValDer(); ///< move quadrature rule, values, and gradient to quadrature coordinates

    std::string mainCoor(ReadInput &Inp);
public:
    static const BasisAbstract* factory(std::string Name); /// get from table
    static void read(ReadInput & Inp); ///< put into table from input
    static void matrix(const std::string &Op, const Index* IIndex, const Index* JIndex, UseMatrix & Mat, std::complex<double> Multiplier);

    std::string quadCoor() const {return _quadCoor;} ///< the coordinates used for the quadrature (grid and weight)
    unsigned int dim() const {return std::count(_quadCoor.begin(),_quadCoor.end(),'.')+1;} ///< spatial dimension
    unsigned int size() const {return _valDer[0].size();} ///< number of basis functions
    std::string str(int Level=0) const;

    const std::vector<std::vector<double> > & quadGrid() const {return _quadGrid;} ///< quadrature points wrt main coordinate system
    const std::vector<double> & quadWeig() const {return _quadWeig;} ///< quadrature weight wrt to main coordinate systems (and integration measure)
    const std::vector<std::vector<std::vector<std::complex<double> > > > &valDer() const {return _valDer;}  ///< values and derivatives of all basis functions at all quadrature points

    const Index* rootIndex(const Index* Idx) const; ///< return index root matching BasisNdim, 0 if no match

    void test();

};


#endif // BASISNDIM_
