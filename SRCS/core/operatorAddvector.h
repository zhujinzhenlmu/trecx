// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORADDVECTOR_H
#define OPERATORADDVECTOR_H

#include "operatorSingle.h"
#include "operator.h"
#include "parameters.h"

/// \ingroup Structures
/// \brief add a vector, i.e. an inhomenuity aka source term
class OperatorAddvector : public OperatorSingle, public Updatable {
public:
    virtual ~OperatorAddvector(){}
    OperatorAddvector(const std::string & Def, const Discretization *IDisc, const Discretization *JDisc,
                      const Index *IFloor, const Index *JFloor,
                      std::complex<double> Multiplier=1.);

    typedef void (*UpdateC)(const double Time, std::vector<std::complex<double> > & C);
    static std::map<std::string,UpdateC> updateTable;
    static void addUpdate(UpdateC UpdateFunction, std::string Name);
    void update(double Time){if(updateFunction!=0)updateFunction(Time,C);}///< update addition vector

    void axpy(std::complex<double> Alfa, CoefficientsFloor & X,std::complex<double> Beta, CoefficientsFloor & Y,bool transpose=false) const; ///< Y+= C;
    void apply(CoefficientsFloor * X, CoefficientsFloor * Y, std::complex<double> Alfa=1.,std::complex<double> Beta=1.) const; ///< Y+= alfa C
    void inverse(){ABORT("cannot invert vector addition");}
    std::string str() const; ///< return string describing the object
    bool isZero(double Eps=0.) const;
protected:
    void setUpdate(std::string Definition); ///< check syntax and search for update function
    UpdateC updateFunction;
    std::vector<std::complex<double> > C;
    void fillC(); ///< move contents of matrices to C
    bool absorb(OperatorSingle *&Other);
};


#endif // OPERATORADDVECTOR_H

