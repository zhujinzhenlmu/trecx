// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef DENSITYOFSTATES_H
#define DENSITYOFSTATES_H

#include <vector>
#include <complex>
#include <string>

class OperatorAbstract;
class Coefficients;
class ReadInput;

class DensityOfStates
{
    double eMin,eMax;
    int nPoints;
    double box;
    std::string outDir;
public:
    DensityOfStates(ReadInput & Inp);
    void output(const OperatorAbstract* Op);
    std::vector<double> compute(const OperatorAbstract* Op);
};

#endif // DENSITYOFSTATES_H
