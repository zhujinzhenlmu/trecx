#ifndef INVERSECOULX_H
#define INVERSECOULX_H

#include "inverse.h"
#include "mpiWrapper.h"

class Index;
class OperatorInverseCoulX;

class InverseCoulX:public Inverse
{
    OperatorInverseCoulX *opInvCoulX;
public:
    InverseCoulX(const Index *Idx, unsigned int SubD, unsigned int SuperD, bool BandOvr);
    void apply0(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;

    // no correction needed
    void applyCorrection(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{}

    // local versions (not needed)
    void parallelSetup() const{if(MPIwrapper::Size()>1) DEVABORT("cannot be run in parallel");}
    void apply0(std::complex<double> A, const CoefficientsLocal &Vec, std::complex<double> B, CoefficientsLocal &Y) const{ABORT("Not implemented!");}
    void applyCorrection(std::complex<double> A, const CoefficientsLocal &Vec, std::complex<double> B, CoefficientsLocal &Y) const{ABORT("Not implemented!");}
};

#endif // INVERSECOULX_H
