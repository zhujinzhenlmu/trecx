// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef INDEXDERIVED_H
#define INDEXDERIVED_H

#include "index.h"

/// \ingroup Index
/// \brief base class for derived Index's
class IndexDerived : public Index
{
public:
    IndexDerived();

    /// selected levels converted to grid
    IndexDerived(const Index * I, std::vector<unsigned int> Level, std::vector<unsigned int> Point=std::vector<unsigned int>(), std::vector<double> Limit=std::vector<double>());
protected:
    std::vector<const Index*> fromIndex; ///< list of indices from which the present is derived (if emtpy, identical Index tree structures assumed)
    std::string strData() const;         ///< Index-specific data
};

#endif // INDEXDERIVED_H
