#ifndef OPERATORINVERSECOULX_H
#define OPERATORINVERSECOULX_H

#include "operatorTree.h"

class Index;

class OperatorInverseCoulX:public OperatorTree
{
public:
    OperatorInverseCoulX(const Index *Idx, unsigned int SubD, unsigned int SuperD, bool BandOvr);
};

#endif // OPERATORINVERSECOULX_H
