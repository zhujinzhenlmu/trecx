// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORTREE_H
#define OPERATORTREE_H

#include <complex>
#include <string>

#include "tree.h"
#include "operatorAbstract.h"
#include "coefficientsLocal.h"

class OperatorDefinition;
class OperatorFloor;
class UseMatrix;
class TransformOperatorAbstract;
class OperatorTucker;
class HMatrix;
class SparseMatrix;

/** \ingroup Structures */
/// new main operator type base on template class Tree
class OperatorTree: public Tree<OperatorTree>, public OperatorAbstract
{
    friend class HMatrix;
    friend class Parallel;
    friend class PermuteOperatorTree;
    friend class OperatorTucker;
    friend class TransformOperatorAbstract; // for setting floors
    friend class OperatorSVD; // for setting floors
    friend class Operator; // for transition only
    friend class Index;
    friend class DerivativeBlock; //HACK for changing parallelization
    friend class ParallelOperator; // that is OK

    static std::vector<std::complex<double> > colStor;
    static UseMatrix* colMat;

protected:
    OperatorFloor * oFloor;
    bool _view;

private:

    void fuse(); // fuse the compatible children (i.e. same iIndex,jIndex, absorbable oFloor
    void fuseBottomUp(); // fuse tree bottom up

public:
    bool absorb(OperatorTree *Other); // absorb Other into present, return false if cannot be absorbed

private:
    OperatorTree(const std::string Name, const std::string Definition, const Index* IIndex, const Index* JIndex, OperatorFloor *OFloor);

    /// construct matrix with continuity conditions imposed (columns/rows contracted), exploit block-structure
    void _matrixContracted(UseMatrix &Mat, const Index* IRoot, const Index* JRoot, std::vector<unsigned int> ICont, std::vector<unsigned int> JCont) const;

    OperatorTree(const std::string Name, const OperatorDefinition & Definition, const Index* IIndex, const Index* JIndex,
                 std::complex<double> Multiplier, std::vector<std::complex<double> *> TFac,bool EntryLevel);

protected:
    void _apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;

public:
    OperatorTree():oFloor(nullptr),_view(false){}
    OperatorTree(std::string Name,const Index* IIndex,const Index* JIndex):OperatorAbstract(Name,IIndex,JIndex),oFloor(nullptr),_view(false){}
    OperatorTree(const std::string Name, const OperatorDefinition & Definition, const Index* IIndex, const Index* JIndex,std::complex<double> Multiplier=1.)
        :OperatorTree(Name,Definition,IIndex,JIndex,Multiplier,std::vector<std::complex<double> *>(),true){}
    OperatorTree(const OperatorAbstract *A, const Index* IIndex=nullptr, const Index* JIndex=nullptr);
    OperatorTree(const Operator *A);
    virtual ~OperatorTree();
    // need copy and asignement...

    /// add Term to present operator and try fuse blocks
    OperatorTree& add(const OperatorTree * Term);

    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
    void applyTranspose(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;

    void postProcess(); ///< post-process special floors, parallel, fuse, purge

    bool constructHybrid(const std::string &Name, const OperatorDefinition &Def, const Index* IIndex, const Index* JIndex, std::complex<double> Mult = 1.);

    void floorInvert(); ///< replaces floor operators with their inverses, ABORTs if not invertible

    bool isZero(double Eps=1.e-12) const;
    double norm() const;
    void purge(double Eps=1.e-12); ///< remove branches with isZero(Eps)==true

    typedef bool(*purgeCriterion)(const OperatorTree* Op);
    void purge(purgeCriterion Crit );

    std::string str(int Digits=-1) const {return Tree::str(Digits);}
    std::string strData(int Digits=-1) const;
    
    virtual long applyCount() const;
    
    std::string def() const { return definition; }

    /*
     * Methods to enable rearranging of optrees
     */
    bool nodeEmpty() const{ return iIndex==nullptr or jIndex==nullptr;}
    bool nodeEquivalent(const OperatorTree* other) const{
        //TODO: Needs to be muted for permuteOperatorTree to work.
        return true;
    }
    void nodeCopy(const OperatorTree* other, bool View);

    bool isBlockDiagonal() const;
    bool isDiagonal() const;
    Coefficients  diagonal(bool OnlyForDiagonalOperator) const;

    /// matrix of block-norms of an operator
    UseMatrix matrixBlocks(unsigned int IDepth=INT_MAX,unsigned int JDepth=INT_MAX) const;

    virtual const OperatorFloor* floor() const;
    virtual OperatorFloor*& floor();

    /// construct matrix w/o continuity conditions imposed, exploit block-structure
    void matrix(UseMatrix &Mat) const;

    /// sub-matrix w/o continuity conditions (returns ref to Mat, defaults to complete matrix)
    Eigen::MatrixXcd  & matrix(Eigen::MatrixXcd & Mat, const Index* ISub=0, const Index* JSub=0) const;

    /// construct matrix with continuity conditions imposed (columns/rows contracted), exploit block-structure
    void matrixContracted(UseMatrix &Mat) const;

    /// create test operators with Dim-dimensional discretization from file (generated if no file is given)
    static OperatorTree* testOp(int Dim, std::string File="");

    /// compare two operators (with tolerance Eps)
    void compare(const OperatorTree * Other, double Eps=0.) const;
		size_t diagnoseSizeOfNode() const;

    void updateNonLin(double Time, const Coefficients *C);
};

#endif // OPERATORTREE_H
