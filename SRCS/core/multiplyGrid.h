// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef MULTIPLYGRID_H
#define MULTIPLYGRID_H

#include <string>
#include <complex>
#include <vector>
#include <cfloat>
#include "tree.h"
#include "operator.h"

class Discretization;
class DiscretizationDerived;
class CoefficientsFunction;
class Index;

/// \ingroup Structures
///@brief Apply multiplication operator by transforming to quadrature grid and back
class MultiplyGrid:public OperatorAbstract
{
public:
    ~MultiplyGrid(){}
    MultiplyGrid();
    MultiplyGrid(const Discretization *D /** original, non-grid discretization */,
                 std::string Func /** string specifying the function (see "function factory" in code) */,
                 std::string Grid /** axis to transform: format as axisName1.axisName2.axisName3 */);

    void apply(std::complex<double> Alfa, const Coefficients & X, std::complex<double> Beta, Coefficients & Y) const;
    double setNorm(){return 1.;}
private:
    DiscretizationDerived* dGrid;
    CoefficientsFunction* function;

};

#endif // MULTIPLYGRID_H
