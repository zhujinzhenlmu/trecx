#ifndef OPERATORVOLKOVPHASE_H
#define OPERATORVOLKOVPHASE_H

#include "operatorAbstract.h"

class DiscretizationDerived;
class CoefficientsFunction;

class OperatorVolkovPhase:public OperatorAbstract
{
    CoefficientsFunction* volkov;
    void update(double Time, const Coefficients* CurrentVec);

public:
    OperatorVolkovPhase(const DiscretizationDerived *GridSpecDisc, const std::string KLevel);
    void apply(const std::complex<double> A, const Coefficients& X, const std::complex<double> B, Coefficients& Y) const;
};

#endif // OPERATORVOLKOVPHASE_H
