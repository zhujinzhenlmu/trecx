// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef MULTIPOLE_H
#define MULTIPOLE_H

#include <vector>
#include <map>

#include "integrate.h"

class UseMatrix;
class BasisSet;

class MultiPole
{
    /// SVD of multipole potentials wrt to basis
    std::map<std::string,std::vector<std::vector<UseMatrix> > >svdVl;
public:
    MultiPole(){}
    MultiPole(const BasisSet & Ibas, const BasisSet &Jbas, unsigned int Lmax, double Epsilon);

private:
    std::string hash(const BasisSet & Ibas, const BasisSet &Jbas) const;


    // recursive integration for optaining accurate multipole integrals
    class Ints:public Integrate::Tools
    {
        friend class Tools;
        unsigned int currentDim;   // transmit current dimension index dim=0,1,2,...)
        std::vector<double> xvals; // complex scaled function arguments

        static std::vector<std::complex<double> > funcVal; ///< number of different integrands
    public:
        /// min(r1,r2)^l/max(r1,r2)^(l+1) up to Lmax
        static std::vector<std::complex<double> > multiPot(const std::vector<double> & R1R2);
        static std::vector<const BasisSet*> basis; ///< left and right basis sets
        static int test; ///< switch for direct evaluation for non-overlapping case

        Ints(){}
        Ints(unsigned int Nintegrands, const BasisSet &IBas, const BasisSet &JBas,
             double AccRel=1.e-12, double AccAbs=1.e-12,
             std::vector<std::vector<unsigned int> > NQuad=std::vector<std::vector<unsigned int> >(0),
             const std::string Kind="GaussLegendre",
             const std::string KindInf="GaussLaguerre");

        std::vector<std::complex<double> >  nDim(const std::vector<std::vector<double> > Vol,
                       const std::tr1::function<std::vector<std::complex<double> >(const std::vector<double>)> Func,
                       const std::vector<double> Params=std::vector<double>(0));

        std::vector<std::complex<double> >  recursive(const std::vector<std::vector<double> > Vol,
                         const std::tr1::function<std::vector<std::complex<double> > (const std::vector<double> &)> Func,
                         const std::vector<double>Params=std::vector<double>(0)
                ) {return Integrate::Recursive < std::vector<std::complex<double> > ,double,Ints>(*this,Vol,Func,Params);}
    };

};

#endif // MULTIPOLE_H
