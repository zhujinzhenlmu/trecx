// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef SPECTRUMTINFINITE_H
#define SPECTRUMTINFINITE_H

#include "evaluator.h"
#include "vectorReal.h"
class OperatorDiagonal;
class ReadInput;
class DiscretizationSpectral;
class OperatorTree;
class SpectrumTinfinite
{
    std::string hamDef;
    VectorReal radii;
    double stretch;
    bool extrapol;

    // recursive
    void mapResolvent(Coefficients * Wf, Coefficients &Spec, OperatorTree * Hamiltonian,
                      Operator *ToSurface, Operator *Commutator, Operator *ToSpectrum);

public:
    SpectrumTinfinite(ReadInput & Inp, ReadInput & RunInp);
    bool compute(const tSurffTools::Evaluator & Eval, std::vector<Coefficients> &SpecSmooth, double &Tend, Coefficients &AmpInfty);
    double radius(int K) const {return radii[K];}


};

#endif // SPECTRUMTINFINITE_H
