#ifndef H_MATRIX_H
#define H_MATRIX_H

#include "index.h"
#include "operatorAbstract.h"
#include "operatorFloor.h"
#include "tree.h"
#include "operatorTree.h"

class ReadInput;
class MatrixRk;

/**
 * H-Matrices [Hackbusch] as OperatorFloors or OperatorAbstracts
 */
class HMatrix: public Tree<HMatrix>{
public:
    /////////////////////////////////////////////////////////////////////////////////////
    class TruncationStrategy{
    public:
        /// Defines the blocking of the index set I
        virtual std::vector<unsigned int> block(std::vector<unsigned int> path, unsigned int childSize);
        
        /// Defines the distribution of the available absolute error over the matrix
        virtual double errorPercentage(const Index* iIndex, const Index* jIndex, const Index* iChildIndex, const Index* jChildIndex);

        /// Defines the blocking of I x I
        /// Bypassed by AdaptiveTruncationStrategy
        virtual bool truncateToRk(const Index* iIndex, const Index* jIndex);

        virtual std::string str() const{ return "P2"; }

        /// Configuration
        static std::string defaultTruncationStrategy;
        static TruncationStrategy* instantiateDefault();
        static void read(ReadInput& Inp);
    };
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\//

    /////////////////////////////////////////////////////////////////////////////////////
    class AdaptiveTruncationStrategy: public TruncationStrategy{
    public:
        std::vector<unsigned int> block(std::vector<unsigned int> path, unsigned int childSize);
        std::string str() const{ return "adaptive"; }

        virtual bool truncateToRk(const UseMatrix& original, HMatrix* hmat, MatrixRk* matRk);

    };
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\//


    /////////////////////////////////////////////////////////////////////////////////////
    class HMatrixIndex: public Index{
    public:
        const Index* _base;

    private:
        void create(std::vector<const Index*> children, std::vector<unsigned int> path, TruncationStrategy* strategy);

        HMatrixIndex(std::vector<const Index*> children, std::vector<unsigned int> path, TruncationStrategy* strategy);
    
    public:
        HMatrixIndex(const Index* base, TruncationStrategy* strategy);
    };
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\//
    
private:
    /////////////////////////////////////////////////////////////////////////////////////
    class HMatrixOperatorAbstract: public OperatorAbstract{
        friend class HMatrix;

        const HMatrix* _base;
    public:
        HMatrixOperatorAbstract(const Index* IIndex, const Index* JIndex, const HMatrix* base);
        void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
    };
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\//

    /////////////////////////////////////////////////////////////////////////////////////
    class HMatrixOperatorFloor: public OperatorFloor{
        friend class HMatrix;

        const HMatrix* _base;
    public:
        HMatrixOperatorFloor(const HMatrix* base);
        
        /// Packing is not supported
        void pack(std::vector<int> &Info, std::vector<std::complex<double> >&Buf) const{ ABORT("Not supported"); }

        void axpy(const std::complex<double>& Alfa, const std::complex<double>* X, unsigned int SizX,
                  const std::complex<double>& Beta, std::complex<double>* Y, unsigned int SizY) const;
    };
    //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\//

    HMatrixOperatorAbstract* _asOperatorAbstract;
    HMatrixOperatorFloor* _asOperatorFloor;

    std::vector<unsigned int> _iPos;
    std::vector<unsigned int> _jPos;

    // Cache optimization, only root stores data here
    std::vector<std::complex<double> >* _storage;

protected:
    void setPositions();
    HMatrix* flattened();

    virtual unsigned int storageRequirement() const;
    virtual void storage(std::complex<double>* storage=0);
public:
    /// Mimic OperatorAbstract
    const HMatrixIndex* iIndex;
    const HMatrixIndex* jIndex;

protected:
    TruncationStrategy* _strategy;
   
    /// Constructor for subclasses
    HMatrix(const HMatrixIndex* iIndex, const HMatrixIndex* jIndex, TruncationStrategy* strategy);

    virtual std::vector<MatrixRk*> getLowrankMatrices();
private:
    /// Recursive constructor
    HMatrix(const HMatrixIndex* iIndex, const HMatrixIndex* jIndex, TruncationStrategy* strategy, const UseMatrix& mat);

public:
    virtual ~HMatrix();


    OperatorFloor* asOperatorFloor() const{ return _asOperatorFloor; }
    OperatorAbstract* asOperatorAbstract() const{ return _asOperatorAbstract; }

    virtual void axpy(std::complex<double> a, const std::complex<double>* x, std::complex<double> b, std::complex<double>* y) const;
    virtual long applyCount() const;
    
    static HMatrix* truncate(const OperatorAbstract* op, TruncationStrategy* strategy=0);
    static void truncateFloorLevel(OperatorTree* tree, TruncationStrategy* strategy=0);

    static void rank(OperatorTree* optree, unsigned int rank);
    static void fixedRankForAccuracy(OperatorTree* optree, double relAtEachFloor);
    static void accuracy(const OperatorTree* optree, double& abs);

    virtual void fullMatrix(UseMatrix& mat) const;

    virtual void rank(unsigned int rank);
    virtual int rank() const;

    /// Give the rel and abs error of the truncation (Frobenius norm)
    virtual void accuracy(double& rel, double& abs, const UseMatrix* mat=0) const;

    /// Give either accuracyRel or accuracyAbs
    virtual void rankForAccuracy(double rel, double abs, const UseMatrix* mat=0);
    virtual void fixedRankForAccuracy(double rel, double abs, const UseMatrix* mat=0);

    /// Configuration
    static double defaultRelativeAccuracy;
    static void read(ReadInput& Inp);

    /*
     * TEMPORARY
     */
    virtual void structureTikZ(std::ostream* output) const;
    void writeStructureTikZ(std::ostream* output) const;
    static void collectDataForFloors(OperatorTree* root, TruncationStrategy* strategy=0);
    static void collectDataForFloors2(OperatorTree* root, std::ofstream* output=0);
    static void collectEpsilonF(OperatorTree* root, std::ofstream* ouput=0);
};


#endif
