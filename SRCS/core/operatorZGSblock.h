// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORZGSBLOCK_H
#define OPERATORZGSBLOCK_H


#include <string>
#include <vector>
#include <complex>

class UseMatrix;
class Index;
class OperatorZG;

#include "operatorFloor.h"
#include "operatorZG.h"

/** \ingroup OperatorFloors */
/// compress sparse matrix into block (assumes many empty rows or columns)
class OperatorZGSblock: public OperatorFloor
{
    OperatorZG* sub;
    std::vector<unsigned int> colSub,rowSub;
    long applyCount() const { return sub->applyCount(); }
protected:
    void axpy(const std::complex<double> & Alfa, const std::complex<double>* X, unsigned int SizX,
              const std::complex<double> & Beta, std::complex<double>* Y, unsigned int SizY) const;

    void axpyTranspose(std::complex<double> Alfa, const std::vector<std::complex<double> > & X,
              std::complex<double> Beta, std::vector<std::complex<double> > & Y) const;
public:
    OperatorZGSblock(const std::vector<int> &Info, const std::vector<std::complex<double> > &Buf);
    OperatorZGSblock(const UseMatrix *Mat, std::string Kind);
    OperatorZGSblock(const OperatorZGSblock&);

    static unsigned int packCode(){return 2;}
    void pack(std::vector<int>& Info, std::vector<std::complex<double> >&Buf) const;
};

#endif // OPERATORZGSBLOCK_H
