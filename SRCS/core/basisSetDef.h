// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASISSETDEF_H
#define BASISSETDEF_H

#include "coordinate.h"
#include "complexScaling.h"
#include <string>


class BasisSet;
class Index;
class BasisSetDef{
public:
    unsigned int order;
    unsigned int minOrder; // use functions starting from minOrder
    double shift,scale;
    std::string funcs;
    bool first,last;
    Coordinate coor;
    std::vector<int> margin;
    ComplexScaling comSca;
    bool exactIntegral; // perform integrations exactly
    bool deriv;
    std::vector<double> par;

    BasisSetDef():minOrder(0),order(0),shift(0.),scale(1.),funcs("useIndex"),first(false),last(false),coor(),comSca(ComplexScaling()),exactIntegral(false),deriv(false){}
    BasisSetDef(unsigned int Order, double Shift, double Scale,std::string Funcs,bool ExactIntegral,
                const bool First, const bool Last, const Coordinate & Coor, std::vector<int> Margin={0,-1},
                ComplexScaling ComSca = ComplexScaling(), bool Deriv = false, std::vector<double> Par=std::vector<double>(0))
        : minOrder(0),order(Order),shift(Shift),scale(Scale),funcs(Funcs),exactIntegral(ExactIntegral),first(First),last(Last),coor(Coor),margin(Margin),
          comSca(ComSca),deriv(Deriv),par(Par){
        if(coor.isPeriodic()){
            first=false;
            last=false;
        }

        // modify parameters: interprete first element as reverted, if it has infinite range
        if(first and not last and BasisFunction::asympZero(funcs) and scale>0){
            shift+=scale;
            scale=-std::abs(scale);
        }
        // no shift or scale for useIndex
        if(funcs=="useIndex"){
            shift=0.;
            scale=1.;
        }
        if(margin[1]==-1)margin[1]=order-1;
    }
    BasisSetDef resolveDependence(const std::vector<unsigned int> &Pos, const std::vector<const Index *> &Path) const;

    std::string size() const;

    std::string name() const {return funcs;}
    /// resolve dependencies of basis definition on other functions
    void resolveDependence(std::vector<const BasisSet *> &Bas, const std::vector<unsigned int> &N);

    double lowBound() const;
    double upBound() const;
    bool operator==(const BasisSetDef &o) const;
    std::string str() const;
};
#endif // BASISSETDEF_H
