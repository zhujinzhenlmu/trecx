// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef __READ_COLUMBUS_DATA__
#define __READ_COLUMBUS_DATA__

// This file contains functions to read infotmation from COLUMBUS.

#include <fstream>
#include <iostream>
#include <vector>
#include "qtEigenDense.h"
#include "my4darray_shared.h"
#include "quantumChemicalData.h"

struct symmetry{
  
  std::string symbol;
  int no_of_irreps;
  Eigen::VectorXi orbitals_per_irrep;

};
class ReadInput;

/// \ingroup ChemStruc
/// \brief read and convert COLUMBUS data
class columbus_data : public QuantumChemicalData
{
  void read_cif(bool ion=true);  // read columbus input file
 public:
  static ReadInput* HACKinput;
  std::string col_path;        ///< original COLUMBUS data
  std::string home;            ///< contains path to the home(main directory) of the columbus calculation (contains copy of Columbus data)
  symmetry sym;                //stores the symmetry of the calculation that helps to set up the wavefunction appropriately
  Eigen::Matrix<int, Eigen::Dynamic, 4> Elec_Rep_Ind; // Electron repulsion indices
  Eigen::VectorXd Elec_Rep_Int;                       // Electron Repulsion Integrals stored in same order as indices
  My4dArray_shared* vee;

  columbus_data(bool ion=true);
  ~columbus_data();
  void read_primitive_gaussian_info(Eigen::VectorXd& exponent, Eigen::Matrix<int, Eigen::Dynamic, 3>& power, Eigen::Matrix<double, Eigen::Dynamic, 3>& coord);
  void read_contraction_coefficients(Eigen::MatrixXd& trans);
  void read_sao_coefficients(Eigen::MatrixXd& trans,int no_ao);
  void read_mo_coefficients(Eigen::MatrixXd& trans,int no_ao);
  void read_matrices();
  void read_determinants(std::vector<std::vector<int > >& det, Eigen::VectorXcd& coef, int state);
  void read_determinants_allstates(std::vector<std::vector<int > >& det, std::vector<Eigen::VectorXcd >& coef);
  int read_multiplitcity();
};
#endif
