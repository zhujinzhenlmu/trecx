// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef MAPGAUGE_H
#define MAPGAUGE_H

#include "operatorAbstract.h"
#include <deque>
#include <memory>
#include "tree.h"

class DiscretizationGrid;
class DiscretizationSurface;

/// \ingroup OperatorData
/// \brief map surface to velocity gauge
class MapGauge:public OperatorAbstract
{
    // use deque to ensure data can be pointed to
    std::vector<std::vector<double> > rgrid; // 0,1,2...unit vector, 3...min(length,GaugeRadius)
    std::deque<std::complex<double> > phase;
    std::deque<double> aDotUnit;

    // tree with pointers to the Phases
    class GaugePhase: public Tree<GaugePhase>{
    public:
        std::complex<double>* pPhase;
        double * pADotUnit;
        GaugePhase(const Index* IdxAng,double Phi,double Eta,
                  std::vector<std::vector<double> > &Unit,
                  std::deque<double> &ADotUnit,
                  std::deque<std::complex<double> > & Phase
                  );
    };
    GaugePhase * gPhas;
    std::shared_ptr<const OperatorAbstract> mapToSurface;
    DiscretizationGrid * gridAngular;
    double setNorm(){return 1.;}
    void applyPhase(Coefficients* Vec, const GaugePhase *Grid) const;
public:
    ~MapGauge();
    MapGauge(const DiscretizationSurface *S);
    void apply(std::complex<double> Alfa, const Coefficients &X, std::complex<double> Beta, Coefficients &Y) const ;
    void update(double Time, const Coefficients* CurrentVec=0);
    void axpy(std::complex<double> Alfa, const Coefficients &X, std::complex<double> Beta, Coefficients &Y, double Time);
};


#endif // MAPGAUGE_H
