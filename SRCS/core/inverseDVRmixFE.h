#ifndef INVERSEDVRMIXFE_H
#define INVERSEDVRMIXFE_H

#include "inverse.h"
#include "tree.h"

class Index;
class OperatorTree;

/// inverse of a DVR basis that is interspersed with isolated FE (full) overlap matrices
class InverseDVRmixFE: public Inverse
{
    OperatorTree* inv0;
public:
    InverseDVRmixFE(Index *Idx);
    void apply0(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
    void applyCorrection(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;

    void applyCorrection(std::complex<double> A, const CoefficientsLocal &Vec, std::complex<double> B, CoefficientsLocal &Y) const;
    void apply0(std::complex<double> A, const CoefficientsLocal &Vec, std::complex<double> B, CoefficientsLocal &Y) const;
    void parallelSetup() const;// {ABORT("no parallel setup implmented");}
};

#endif // INVERSEDVRMIXFE_H
