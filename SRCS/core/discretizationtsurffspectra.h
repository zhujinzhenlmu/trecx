// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#ifndef DISCRETIZATIONTSURFFSPECTRA_H
#define DISCRETIZATIONTSURFFSPECTRA_H

#include "discretizationDerived.h"
#include "index.h"
#include "tree.h"
#include "operatorAbstract.h"
#include "operatorMap.h"

class SurfaceFlux;
class DiscretizationSurface;

/** \ingroup Discretizations */
/// for computing spectra, selected axes converted to momentum grid
class DiscretizationTsurffSpectra: public DiscretizationDerived
{
//    friend class DiscretizationTsurffParallel;

    void _construct(const Index *IParent, const std::vector<double> Momenta);
public:
    class IndexTsurffSpectra : public Index
    {
    protected:
        std::vector<const Index*> fromIndex; // list of indices from which the present is derived (if emtpy, identical Index tree structures assumed)
    public:
        /// Surface index is converted to momentum index
        IndexTsurffSpectra(const Index * parentI, const std::vector<double> momenta);
    };


    static int _defaultRadialPoints;
    static void computeMomenta(double minEnergy, double maxEnergy, std::vector<double>& momenta, bool KGrid);
    static void readFlags(ReadInput &Inp, int &radialPoints, double &MinEnergy, double &maxEnergy, bool &kGrid, bool AllowFlags);
    static void read(ReadInput &Inp, const Discretization *D, int unboundDOF, std::string &Region, int &NSurf);
    static void checkRegion(int &unbound, std::string &Region, int &NSurf, std::vector<std::string> &infCoorNames, ReadInput &inp);
    static void getNextRegion(int &unbound, std::string &Region, int &NSurf, std::vector<std::string> &infCoorNames, ReadInput &inp);
    static void getRegions(int pos, unsigned int ibeg, int unbound, std::vector<std::string> &infCoorNames, std::vector<std::string> &regions, std::vector<std::string> &names);
    static std::vector<std::string> splitRegion(const std::string s);
    static DiscretizationTsurffSpectra * factoryTsurff(const DiscretizationSurface *Parent, ReadInput &Inp, int radialpoints=0);
    /// spectrum discratizaiton with Momenta replacing the surface
    DiscretizationTsurffSpectra(const DiscretizationSurface *Parent, const std::vector<double> Momenta);
    /// discretization for spectra, further input from Inp
    DiscretizationTsurffSpectra(const DiscretizationSurface *Parent, ReadInput &Inp, int radialPoints=_defaultRadialPoints);
    /// discretization spectra from Flux
    DiscretizationTsurffSpectra(const SurfaceFlux *Flux, ReadInput &Inp, int RadialPoints=_defaultRadialPoints);
    DiscretizationTsurffSpectra(){}
};

#endif // DISCRETIZATIONTSURFFSPECTRA_H
