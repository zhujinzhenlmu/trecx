#ifndef BANDEDOVERLAP_H
#define BANDEDOVERLAP_H

#include "operatorTree.h"

class BandedOverlap:public OperatorTree
{
public:
    BandedOverlap(Index *Idx, unsigned int SubD, unsigned int SuperD);
};

#endif // BANDEDOVERLAP_H
