#ifndef INDEXCOULXNEW_H
#define INDEXCOULXNEW_H

#include "indexDerived.h"

class IndexCoulXNew:public IndexDerived
{
public:
    IndexCoulXNew(const Index *Idx, double RC, double RMax, const std::vector<double> KGrid, bool BandOvr, bool PureBessel, bool Bottom=false);
};

#endif // INDEXCOULXNEW_H
