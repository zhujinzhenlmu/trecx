// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef MY4DARRAY_SHARED_H
#define MY4DARRAY_SHARED_H

#define __NO_SHM_4d__

// disable shared memory use
#ifndef __NO_SHM_4d__
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#endif

#define __NO_SHM_4d__

#ifndef __NO_SHM_4d__
typedef boost::interprocess::allocator<double, boost::interprocess::managed_shared_memory::segment_manager> ShmemAllocator;
typedef boost::interprocess::vector<double, ShmemAllocator> MyVector;
#else
typedef double MyVector;
#endif

#include "My4dArray.h"

class My4dArray_shared
{
public:
  static int obj_count;       // helps to give a unique name
  int d;                      // dimension

  static int extra_index;     // extra index in case the file number exists

  My4dArray_shared(int size=0, bool cout_info=true);
  ~My4dArray_shared();

  void construct(int size, bool cout_info=true);

  double * data();
  unsigned int size();

  void assign(int l1, int l2, int l3, int l4, double num);
  int find_pos(int l1, int l2, int l3, int l4);
  void addto(int l1, int l2, int l3, int l4, double num);
  double val_at(int l1, int l2, int l3, int l4);
  double dot(My4dArray_shared* b);
  double dot(My4dArray &b);

  void remove_zero();
  bool isZero;
private:
  MyVector * x;
  std::string shared_memory_name;
#ifndef __NO_SHM_4d__
  boost::interprocess::managed_shared_memory* segment;
#endif
};

#endif // MY4DARRAY_SHARED_H
