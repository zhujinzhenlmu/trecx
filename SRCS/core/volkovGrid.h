#ifndef VOLKOV_GRID_H
#define VOLKOV_GRID_H

#include "operatorAbstract.h"
#include "discretizationDerived.h" //temporary

class Discretization;
class DiscretizationSurface;
class DiscretizationDerived;
class SurfaceFlux;
class CoefficientsFunction;

class VolkovGrid: public OperatorAbstract{
    DiscretizationDerived* gridSpecDisc;
    CoefficientsFunction* volkov;
    Coefficients* cGrid;
    double time;

public:
    VolkovGrid(const Discretization * D, SurfaceFlux *Flux, const DiscretizationDerived* SmoothSpecDisc):VolkovGrid(Flux,SmoothSpecDisc->idx()){}
    VolkovGrid(SurfaceFlux *Flux, const Index *SpecIdx);

    void update(double Time, const Coefficients* CurrentVec);
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const override;
};


#endif
