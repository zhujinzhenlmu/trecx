// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASISSET_H
#define BASISSET_H

#include "useMatrix.h"
#include "basisFunction.h"
//#include "basisMat.h"
#include "coordinate.h"

#include "complexScaling.h"
#include <map>
#include <deque>
#include <string>
#include "basisAbstract.h"
#include "basisIntegrable.h"
#include "basisSetDef.h"

class basisMat;
class ComplexScaling;

class Index;
class Axis;

/** \ingroup Basissets */

/// \brief One-dimensinal basis set \f$ \left\{|i\rangle\right\} \f$ (main class, legacy code)
///
/// a basis set is a set of functions (0,...,Order-1)
/// defined as a linear combination of shifted and scaled BasisFunction
/// NOTE: not all BasisSet objects really qualify as "BasisIntegrable" - do not dynamically cast, use BasisIntegrable::factory instead
/// (this is a HACK to be removed)
class BasisSet: public BasisFunction, public BasisIntegrable {
    friend class IndexFloor;
    friend class Axis;
    friend class BasisMat;
    friend class Grid;
    friend class Gauge;
    friend class InFlux;
    friend class BasisSetDef;
public:

    std::complex<double> jacobian(const std::complex<double> &X) const {
        switch(def.coor.jaco) {
        case Coordinate::J_one: return 1.;
        case Coordinate::J_val: return X;
        case Coordinate::J_squ: return X*X;
        default:
            ABORT("Jacobian not defined for code: "+tools::str(def.coor.jaco)+", basis: "+str());exit(1);
        }
    }

    static void Test(bool Print); ///< basic tests
    static std::map<unsigned int,BasisSet*> dummyBasis;
    static std::deque<BasisSet> gridBasis,genBasis;

    /// add basis set to deque and return pointer to it
    static BasisSet* get(BasisSetDef BasisSetDef);
    /// add basis set to deque and return pointer to it
    static const BasisAbstract *getAbstract(BasisSetDef BasisSetDef);
    /// add dummy basis set to deque and return pointer to it
    static BasisSet* getDummy(unsigned int Order);
    /// add right incomingbasis set to deque and return pointer to it
    static BasisSet* getRightIn(BasisSetDef BasisSetDef);
//    /// add grid  basis set to deque and return pointer to it
//    static BasisSet* getGrid(const Coordinate Coor,const UseMatrix Points, const UseMatrix Weights);

    // the big three...
    ~BasisSet();
    BasisSet();
    BasisSet(const BasisSet & other);
    BasisSet & operator=(const BasisSet &other);


    bool operator==(const BasisAbstract &Other) const;

    BasisSet(const BasisSetDef & BasisSetDef);
    /** \brief Constructor for prolate spheroidal coordinates.
     *
     *  The overlap for each k is given by
     *  \f$ S^{(k)} = s_k * <\xi*J*\xi>_\xi - q_k <J>_\xi \f$
     *  where \f$ s_k \f$ and \f$ q_k \f$ are the diagonal values of
     *  the eta basis operators \f$ <\eta*J*\eta>_\eta \f$ and \f$ <J>_\eta \f$ respectively
     */

    BasisSet(const Coordinate & Coor, const UseMatrix & Points, const UseMatrix & Weights=UseMatrix()); //!< "grid" basis set, if no weights are given, w[i]=average spacing at i
    BasisSet(unsigned int Order); //!< useIndex basis (dummy, indices only)

    UseMatrix val(const UseMatrix & coordinates, bool ZeroOutside=false) const; ///< val(i,k): value f_k(coordinate[i])
    UseMatrix der(const UseMatrix & coordinates, bool ZeroOutside=false) const; ///< derivatives at a coordinate grid

    void valDer(const UseMatrix & X,UseMatrix & Val, UseMatrix & Der, bool ZeroOutside=false) const; ///< values and derivates on X grid
    void valDer(const std::vector<std::complex<double> > &X, std::vector<std::complex<double> > &Val, std::vector<std::complex<double> > &Der, bool ZeroOutside) const;

    /// returns quadrature weight * jacobian as a diagonal matrix
    UseMatrix matrixWeigJacobian(const UseMatrix & x, const UseMatrix & w, bool Eta=false) const;

    unsigned int lowerMargin() const;///< index of lower margin function
    unsigned int upperMargin() const;///< index of upper marging function

    unsigned int defaultNQuad() const; ///< minimal number of points for exact overlap integrals

    /// base and weight for N-point quadrature on basis function
    void quadRule(unsigned int N, UseMatrix & QuadX, UseMatrix & QuadW) const;
    void quadRule(int N, std::vector<double> & QuadX, std::vector<double> & QuadW) const;
    void dvrRule(UseMatrix & QuadX, UseMatrix & QuadW, bool All=false) const;

    unsigned int size() const; ///< number of basis functions on element
    unsigned int order()const; ///< {return std::max(trans.rows(),points.size());} ///< order = number of fundamental functions
    const UseMatrix * transMat() const {return &trans;}
    const Coordinate & coor() const {return def.coor;}  ///< return coordinate of basis set
    bool isIntegrable(const BasisSet& other) const; ///< true if matrix can be computed
    bool first() const {return def.first;} ///< true if first element on coordinate
    bool last()  const {return def.last;}  ///< true if last element on coordinate

    std::vector<double> parameters() const {return _parameters;} ///< return the function set parameter vector
    double parameter(int K, std::string Kind) const; ///< return K'th basis function parameter of Kind (Kind will we crosschecked)
    std::complex<double> eta() const; ///< return complex scaling eta of basis set
    std::complex<double> complexScaled(double X) const {return def.comSca.xScaled(X);}
    bool isAbsorptive() const; ///< true if absorption acts on basis
    void show(const std::string & Text = "") const; ///< print basis parameters
    std::string str(int Level=0) const; ///< print basis parameters
    std::string strDefinition() const; ///< re-implementes AbstractBasis::strDefinition()
    std::string strFunc(const int I) const; ///< string of parameters of I'th function

    std::string parameterString(int K) const; ///< parameter for K'th basis function (if any)
//    BasisSet* remove(const std::vector<int> &RemoveK) const; ///< returen new basis set with subset of functions removed
//    BasisSet* remove(int RemoveK) const {return remove(std::vector<int>(1,RemoveK));} ///< add new basis set with k'th basis function removed, add to table, return pointer

    std::string name() const{return fs->name(false);}
    double upBound() const;        ///< return upper boundary of interval
    double lowBound() const;        ///< return lower boundary of interval
    double scale() const;        ///< scale of basis relative to standard
    double param(int I) const {return parameters()[I];}
    const BasisSetDef & definition() const {return def;}     ///< access to basis set definition
    UseMatrix to(const BasisSet & IBas) const;                 ///< matrix maps this to IBas
    UseMatrix from(const BasisSet & JBas) const;               ///< matrix maps JBas to this

    void plot(std::string Dir) const;
    bool isPeriodic() const {return def.coor.isPeriodic();}
    bool isGrid() const {return fs->name()=="grid";}
    bool isIndex() const {return fs->name()=="useIndex";}
    bool isCIstate() const {return fs->name().find("CI")!=std::string::npos;}
    /// true if domains of two basis sets overlap, ShareBoundary=true counts shard point as overlap
    bool hasOverlap(const BasisSet & other) const;
    bool shareBoundary(const BasisSet & other) const;
    const BasisFunction* PointerToFunctions() const {return fs;}
    const BasisSetDef & getDef() const {return def;}
    bool isDVR() const {return not def.exactIntegral and not isGrid() and not isIndex();}

    UseMatrix Points() const{return points;}
    UseMatrix Weights() const{return weights;}
private:
    void construct(unsigned int Order, std::string Function, const bool First, const bool Last,
                   const Coordinate & Coor, std::vector<int> Margin, bool Deriv, const UseMatrix & funOvr);
    unsigned int iFunction(int K) const; ///< return BasisFunction number; if not single basis function, ABORT


protected:
    BasisSetDef def;        ///< basis set definition (replaces most of the following)
    const BasisFunction * fs; ///< functions from which basis is derived
    std::vector<double> _parameters;
    unsigned int label;       ///< UNIQUE label of basis set (incremented automatically)
    mutable double upB,lowB;
    UseMatrix trans;        ///< transformation: basis_m(x_i) = sum_k fundamental_k (shift+scale*x_i) trans_km
public:
    UseMatrix points,weights;   ///< "grid" type basis has grid points and quadrature weights
    unsigned int fdOrder;       ///< order for finite difference grid
protected:
    bool check(const UseMatrix & Coor,const UseMatrix & funOvr, double eps, int cond); ///< true for correct basis boundary values and orthogonality

    static int nextLabel; ///< counter: incremened for each new basis set

public:
    double physical(int Index) const override;
};

#endif // BASISSET_H
