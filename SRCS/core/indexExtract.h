#ifndef INDEXEXTRACT_H
#define INDEXEXTRACT_H

#include "index.h"
typedef bool (*selectIndex)(const Index * Idx); /// slection criterion

/// \ingroup Index
/// \brief Extract new from Index by various criteria
class IndexExtract : public Index
{
    void _construct(Index* Result, const Index *Idx, selectIndex Select);
public:
    ///\brief all nodes with axisName in Ax (or complement), avoid double-count when extracting from product space, re-impose constraints, if any
    IndexExtract(const Index* Idx, const std::vector<std::string> Ax, bool Complement, const IndexConstraint* Constraint=0);
    ///\brief return new IndexExtract(...) or 0 if emtpy
    static Index* get(const Index* Idx, const std::vector<std::string> Ax, bool Complement, const IndexConstraint* Constraint=0);
};

#endif // INDEXEXTRACT_H
