#ifndef SpectrumPzSimple_H
#define SpectrumPzSimple_H

#include <vector>

#include "tree.h"
#include "polylagrange.h"

class Coefficients;
class CoefficientsPermute;
class Index;
class BasisGrid;
class Plot;
class ReadInput;
/// sigma(kZ1,kZ2)=int dRho1 Rho1 dRho2 Rho2 sigma(Eta,Rn)
class SpectrumPzSimple {
    unsigned int Nk, _version, _thetaNum, _startKIdx;
    double deltaK;
    std::string _specFile;
    // interp[z][k] - interpolate in Eta for given kZ and kR
    std::vector<std::vector<std::vector<double> > > interp;
    std::vector<double> zGrid, kGrid, etaGrid;
    Coefficients *_ampl;
    Discretization *DPlot;
public:
    SpectrumPzSimple(const Coefficients amplitutes, ReadInput inpc, int version);
    void getInterplateWf(double cosTheta, int k, std::vector<double> &interItem);
    void getInterplate();
    void compute();
    void compute6D();
    void compute3D();
    void computeWf3D(std::vector<double> &specKZ);
    void computeWf6D(std::vector<std::vector<double> > &specKZ);
    void computeNormal(std::vector<std::vector<double> > &specKZ, int startKIdx = -1, int endkIdx = -1);
    void cropCoef(int k01Idx, int k02Idx, int k11Idx, int k12Idx);
    static void initialize(ReadInput & inpc);
};

#endif // SpectrumPzSimple_H
