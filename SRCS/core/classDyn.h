// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef CLASSDYN_H
#define CLASSDYN_H

#include <string>

/// solve classical dynamics until some break-off criterion
class ClassDyn
{
public:
    ClassDyn(std::string HamFunc);
};

#endif // CLASSDYN_H
