#ifndef TENSOR_OPERATOR_TREE_WITH_ID
#define TENSOR_OPERATOR_TREE_WITH_ID

#include <vector>
#include <utility>
#include "operatorTree.h"

class Index;
class IndexConstraint;

// Safe, tested choice, but less memory-efficient
// #define _GENERATE_INDEX_OLD_ 

/** \ingroup Operators */
/// \brief Tensor product of operators
/**
 * Given an OperatorTree \f$a: J\to I\f$ (denoted as Base) as a sum of maps between floor levels   \f$ j\to i\f$
 * \f[
 * a = \sum_{i,j} a_{ij},
 * \f]
 * a quotient index \f$K\f$, and indices in the products spaces given by pairs \f$(i,k)\f$ and \f$(j,k)\f$
 * \f[
 * \mathcal{I}=\{(i_0,k_0),(i_1,k_1),\ldots\},\qquad \mathcal{J}=\{(j_0,k_0),(j_1,k_1),\ldots\}
 * \f]
 * forms
 * \f[
 * A: \mathcal{J}\to \mathcal{I},\qquad A: = \sum_{\alpha,\beta} a_{i_\alpha j_\beta}\otimes Id^{k_\alpha}_{k_\beta}.
 * \f]
 *
 * If there are no constraints on the pairs, i.e. \f$\mathcal{I}=I\otimes K, \mathcal{J}=J\otimes K\f$, this reduces to the
 * tensor product \f$A = a\otimes Id\f$.
 *
 * Needs the operator \f$a\f$ (OperatorTree), at least one of the indices \f$\mathcal{I}\f$ or \f$\mathcal{J}\f$, and
 * the index \f$K\f$ (denoted as QuotIndex).
 *
 * Remark: If a covariant tensor (as the Hamiltonian) \f$H_{ij}\f$ is combined with \f$Id^i_j\f$, the result has
 * mixed indices.
 */
class TensorOperatorTreeWithId{
    /*
     * Configuration to be set prior to build
     * and filled in during build
     */
    const OperatorTree* base;
    const Index* iIndex;

    const Index* jIndex;

    const Index* quotIndex;
    std::string hierarchy;
    int floorDepth;

#ifndef _GENERATE_INDEX_OLD_
    class IndexFactory{
        const Index* first;
        const Index* second;
        std::vector<bool> choose_first;
        const IndexConstraint* constraint;

        bool buildRec(Index* at,
                const std::vector<unsigned int>& path_first, const std::vector<unsigned int>& path_second,
                const std::vector<const Index*>& path_idx);
        IndexFactory(){DEVABORT("bad");}
    public:
        IndexFactory(const Index* first, const Index* second, std::vector<bool> choose_first);
        IndexFactory& withConstraint(const IndexConstraint* constraint);
        Index* build();
    };
#endif

    /*
     * Generated convenience data for build and InternalOperatorTree constructor
     */
    std::string quotHierarchy;
    std::vector<bool> iIdentities;
    std::vector<bool> jIdentities;
    bool floorZGtimesId; // false == IdtimesZG

    /*
     * Actual optree - merely a constructor
     */
    class InternalOperatorTree: public OperatorTree{
        static const OperatorTree* find(
                TensorOperatorTreeWithId* Parent,
                std::vector<double> IPath,
                std::vector<double> JPath,
                const OperatorTree* Cur=0);

    public:
        InternalOperatorTree(TensorOperatorTreeWithId* Parent, const Index* IIndex, const Index* JIndex);
    };
    friend class InternalOperatorTree;

    /*
     * Notation: base maps between spaces a and b,
     * the operator we want to create maps between
     * a\otimes I = A and b\otimes I = B.
     *
     * This method returns the permutation of the indices of B,
     * where a, b, Id may not be changed.
     */
    std::vector<unsigned int> findPermutation(
            std::string Hierarchy_a,
            std::string Hierarchy_A,
            std::string Hierarchy_b,
            std::string Hierarchy_B,
            std::string Hierarchy_I);

    Index* generateIndex(
            const Index* Idx_a,
            const Index* Idx_A,
            const Index* Idx_b);
public:

    TensorOperatorTreeWithId(const OperatorTree* Base);

    /// iIndex of the resulting operator
    TensorOperatorTreeWithId& withIIndex(const Index* IIndex);

    /// jIndex of the resulting operator
    TensorOperatorTreeWithId& withJIndex(const Index* JIndex);

    /**
     * Index of the quotient space, where the resulting operator acts as identity
     * Need not be supplied, if this space is zero-dimensional, must be supplied otherwise.
     *
     * Does not take ownership of QuotIndex.
     */
    TensorOperatorTreeWithId& withQuotIndex(const Index* QuotIndex);

    /**
     * If only iIndex or jIndex are given, the other one is auto-generated. This step can (need not)
     * be configured here.
     */
    TensorOperatorTreeWithId& withGeneratedIndex(std::string Hierarchy, int FloorDepth=-1);

    /// Tensor a vector with (1,1,...)
    static Coefficients* tensorCoefficients(const Coefficients& cFactor, const Index* iFull);

    /**
     * Computation
     */
    OperatorTree* build(bool Check=true);
};


#endif
