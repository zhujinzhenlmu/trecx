#ifndef AXISTREE_H
#define AXISTREE_H

#include "tree.h"
#include "axis.h"

class ReadInput;
class AxisTree : public Axis, public Tree<AxisTree>
{
    std::string _subset;
public:
    virtual ~AxisTree(){}
    AxisTree():_subset(""){}
    AxisTree(const Axis Ax):Axis(Ax){}
    AxisTree(const std::vector<Axis> & Axes);
    AxisTree(ReadInput & Inp,  int &Line, std::string Subset="");
    std::string str(int Level=0) const {return Tree::str(Level);}
    std::string strData(int Level=0) const;// {if(_subset!="")return name+"("+_subset+")";return name;}
    std::vector<Axis> toVector() const;
    std::string subset() const {return _subset;}

    AxisTree * factor(std::vector<std::string> Factor) const;
    AxisTree * complement(std::vector<std::string> Factor) const;
    void nodeCopy(const AxisTree *Node, bool View);
    void print() const;
};

#endif // AXISTREE_H
