// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#ifndef __MO__
#define __MO__

#include "singleparticleorbitals.h"
#include "sao.h"
//#include <boost/multi_array.hpp>
#include "mol_sae_shelf.h"
#include "gaunt.h"
#include "basisfunctionciion.h"
#include "basisfunctioncineutral.h"

/** @brief Molecular orbitals class
 *  Heavy class: doing all the mixed integrals and two electron integrals needed by haCC
 *
 */

class mo;
class mol_sae_shelf;
extern mo *MO;
class Axis;
class QuantumChemicalInput;
class BasisMolecularOrbital;

#include "basisSet.h"


class mo : public singleparticleorbitals{    

    /// Helper matrices for 2-e integrals
    vector<vector<vector<Eigen::MatrixXd > > > vlqq;                     //usage vlqq_xch[na][nb][L]
    vector<vector<vector<vector<vector<Eigen::VectorXcd> > > > > R_dir;  //usage R_dir[Ia][Ib][n][L][M]
    vector<vector<multiarray3igenXcd>>  R_xch;   //usage R_xch[na][la][ma][q][L](i,M)

    /// Helpers for 1-particle and 2-particle operator
    vector<Eigen::MatrixXd > poly_val,poly_der;                          //usage poly_val[n]
    void setup_pol_val();

    /// Helpers for nuclear Coulomb potential: polynomial, molecular orbital
    vector<Eigen::MatrixXd > poly_val_nuclear;                           //usage poly_val_nuclear[n]
    void setup_pol_val_nuclear();
    void setup_ylm_0();

    /// Helpers for nuclear Coulomb potential between polynomials
    void setup_cl_helpers();
    void cl_helper_helper(Eigen::MatrixXcd& res, int na, int n, int l1, int l2, int L);

    /// off-centered coulomb integral helper: between polynomials
    vector<vector<vector<Eigen::MatrixXcd > > > cl_helper;                //other elements cl_helper[atom][n][L]        n=0 left dummy to avoid confusion

    /// Storage for single expansions
    vector<Eigen::MatrixXcd > sce,sce_der,sce_nuc;

    /// haCC 2-e integral helper functions
    void setup_vlqq();
    void Vlqq_matrices(Eigen::MatrixXd& res, int order1, int order2, int Langle, int n1, int n2);
    std::vector<Eigen::MatrixXd> Vlqq_matrices_new(int order1, int order2, int LambdaMax, int n1, int n2);
    void setup_R_dir(BasisFunctionCIion *ion);
    void R_O_helper(vector<vector<vector<vector<Eigen::MatrixXcd> > > > &v_d);
    void setup_R_O(const BasisFunctionCIion* ion);
    void setup_R_d(BasisFunctionCIion *ion);

    inline int L_min(int la, int na);
    inline int L_max(int la, int na);
    inline int M_min(int L, int ma, int na);
    inline int M_max(int L, int ma, int na);
    void SVD_UV(const BasisFunctionCIion* ion, int Ia, int Ib, Eigen::MatrixXcd &U, Eigen::MatrixXcd &V);

    mo_store<Eigen::Vector3i> mo_poly;
    mo_store<mo*> mo_X_mo;
    mo_store<Eigen::Matrix<int , 8, 1> > exch_store;
    mo_store<Eigen::Vector2i> svd_U,svd_V;
    mo_store<Eigen::Vector4i> svd_U_tg,svd_V_tg;

    std::vector<const BasisMolecularOrbital*> _moNdim;

public:

    /// Static variable initialized in haCC discretization
    static mo* main;

    static const double rho_eps;
    int gauge_boundary;
    double mat_tol;
    vector<vector<vector<vector<Eigen::MatrixXcd > > > > R_d;           //usage R_d[[Ia][na][la][ma](neut,k) - for <mo p|mo mo> type of 2-e
    vector<vector<vector<vector<vector<Eigen::MatrixXcd> > > > > R_O;   //usage R_O[Ia][Ib][n][la][ma](k3,q) - for <mo p|mo mo> type of 2-e

    sao S;
    Eigen::MatrixXd coef;

private:
    //  columbus_data *col_data;
    QuantumChemicalData *col_data;
public:
    vector<int> mo_l,mo_m,g_o;                                                               //lmax_g(n),mmax_g(n)
    std::vector<Axis>* axis;

    /// Constructor and destructor
    mo(columbus_data *col_data, std::vector<Axis>* ax, bool compute_sce=false);
    mo(QuantumChemicalInput & ChemDat, std::vector<Axis>* ax, bool compute_sce=false);
    ~mo();

    /// MO info check
    void check_box_size(std::string Mess, double BoxSize, double Eps);
    void info_check();
    Axis* AxisForString(string s);

    Axis* radialAxis();
    BasisSetDef radialDef(int N);
    double radialBox();
    double radialSize();

    int numberOfelementBeforecomplexscaling();
    int orderonElement(int n);

    /// Matrix elements between MO's
    void matrix_1e(Eigen::MatrixXcd &result,std::string op);
    void matrix_1e(Eigen::MatrixXcd &result, std::string op, mo* b);
    void matrix_2e(My4dArray &result);
    int NumberOfOrbitals();

    /// Values and derivatives of MO's
    void values(Eigen::MatrixXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz);
    void values(Eigen::VectorXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, int fun_index);
    void derivatives(Eigen::MatrixXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, std::string wrt);
    void derivatives(Eigen::VectorXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, int fun_index, std::string wrt);

    /// Single centre expansion set up
    void find_angular_componenets();
    void setup_sce_expansions();
    void expansion_dilmq_x_ylm(Eigen::MatrixXcd &res, int n, bool der_r=false, bool for_nucl_pot=false);
    void setup_3dquadrature(Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, Eigen::VectorXd &theta_quad, Eigen::VectorXd &theta_weig, Eigen::VectorXd &phi_quad, Eigen::VectorXd &phi_weig, Eigen::VectorXd r_pts, int lmax, int mmax, bool is_nucl_pot=false);

    /// Matrix elements between MO's and polynomials
    void matrix_1e(Eigen::MatrixXcd& result, std::string op, int n, int l1, int m1);
    void coulomb_setofbf(Eigen::MatrixXcd &res, int n, int l1, int m1, int l2, int m2);
    void coulomb_gauss_poly(Eigen::MatrixXcd &res, int n, int l, int m);

    /// Two-electron integrals
    bool matrix_poly_2e_xch_v2(Eigen::MatrixXcd &res, const vector<int> &blockIdx, const vector<int> &blockJdx, const BasisFunctionCIion *ion);
    void matrix_poly_2e_direct(Eigen::MatrixXcd &res, const vector<int> &blockIdx, const vector<int> &blockJdx);

    /// Two electron integrals helpers
    void setup_Rs(BasisFunctionCIion* ion, BasisFunctionCINeutral* Neut);
    vector<Gaunt*> gaunt_xch;

    void quadRuleRn(unsigned int Elem, unsigned int N, Eigen::VectorXd & Pts, Eigen::VectorXd & Wgs); //HACK to replace p_roots
    void quadRuleRnNew(const BasisIntegrable* B, unsigned int N, Eigen::VectorXd & Pts, Eigen::VectorXd & Wgs); //HACK to replace p_roots

    double vEE(int I1, int I2, int J1, int J2) const;
};

#endif
