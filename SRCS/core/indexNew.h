#ifndef INDEXNEW_H
#define INDEXNEW_H

#include "index.h"

class AxisTree;

///@brief temporary for developing new index constructor
class IndexNew : public Index
{
    /// add child, IF the subtree is non-empty
    void addNonEmpty(AxisTree *Ax, const IndexConstraint* Constraint, std::vector<unsigned int>  Pos,
                     std::vector<const Index *> Path, std::vector<int> & Removed);
    void setBasisRemoved(const std::vector<int> & Removed); ///< handle basis truncation
    ///@brief set up overlap and inverse for more complex Index's
    void buildOverlap();
public:
    IndexNew():Index(){}
    static bool doNotUse;
    /// new basic construct from axes
    IndexNew(const AxisTree *Ax, const IndexConstraint* Constraint=0,
          std::vector<unsigned int> Pos=std::vector<unsigned int>(),
          std::vector<const Index*> Path=std::vector<const Index*>());

};

#endif // INDEXNEW_H
