#ifndef OPERATORDEFINITIONNEW_H
#define OPERATORDEFINITIONNEW_H

#include <climits>
#include "operatorDefinition.h"

/// @brief transparent constructor for operator definition
///
/// legal operator strings are of the form
///
/// OpStr = (+/-Term1 +/- Term2 +/-Term3 ... )
///
/// (no blanks are needed, round brackets optional)\n
///
/// TermX has one of the following forms\n
///  -  param<singleFactor>OpStrX
///  -  param<<PredefinedOp>>OpStrX
///  -  OpStrX
///
/// where:
///  +  OpStrX....... a legal operator string (including blank)
///  +  param ....... string that can be interpreted by class Algebra
///  +  singleDef.... string that can be interpreted by class BasisMat1D
///  +  PredefinedOp. name of an operator that is predefined for Coordinates
///  +  Coordinates.. string of coordinate axis names, separated by '.' (operator factors will be properly permuted)
///
/// there is limited support for handling constraints of the type $BAND.0.*.2  etc.
class OperatorDefinitionNew : public OperatorDefinition
{

    /// return vector of terms, single blank term if empty
    static std::vector<std::string> terms(const std::string Def);
    /// extract parameter para from Term, if Param!="" return product with proper sign
    std::string parameter(std::string Term, std::string Param);
    /// return sign of parameter times OtherSign (+ or -)
    std::string sign(std::string Par, const std::string OtherSign);
    /// remainder after first factor was removed
    std::string remainder(std::string Term, std::string First);
    /// return Term without parameter
    std::string noParameter(std::string Term);
    /// tetrun Term without sign
    std::string noSign(std::string Term);
    /// single factor of the form <string>, empty if not first factor
    std::string firstFactor(std::string Term);
    /// single predefined operator <<string>>, empty if not first factor
    std::string firstPredefined(std::string Term);
    /// coordinates as given in input
    static std::string inputCoordinates(std::string Hierarchy);
    /// compose factor and Term, include update of constraint if there is any
    std::string compose(std::string Factor, std::string Term);
    /// permute single term
    std::string permute(std::string Term, std::string InCoor, std::string OutCoor);
    std::string permuteFEM(std::string Hierarchy);

    /// return operator where identity in all terms is dropped, attach coordinates:
    /// coordinates=X.Y.Z, <A><1><1>+<1><1><B> ->  (<A><1>+<1><B>):X.Z
    std::string removeId(const std::string Hierarchy) const;


public:
    OperatorDefinitionNew(const std::string String, std::string Hierarchy);
    /// return string where certain terms are droped (for deriving operators)
    std::string dropTerms(std::string Hierarchy) const;
};

#endif // OPERATORDEFINITIONNEW_H
