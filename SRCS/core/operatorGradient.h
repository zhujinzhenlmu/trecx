// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORGRADIENT_H
#define OPERATORGRADIENT_H

#include "operatorAbstract.h"

class Discretization;
class DiscretizationGrid;
class ReadInput;

/** \ingroup Structures */
/// values and partial derivatives on a grid
class OperatorGradient : public OperatorAbstract
{
    DiscretizationGrid * grid;
    Coefficients * model,*permModel;

public:
    /// return pointer to operator gradient from Parent with grid read from Inp
    static OperatorGradient* read(const Discretization * Parent /** original discretization */,
                                  ReadInput & Inp /** read grid specification from here */);
    OperatorGradient(const Discretization *Parent, std::vector<std::string> &Ax,
                     std::vector<unsigned int> &Points, std::vector<std::vector<double> > &Bounds);
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
};

#endif // OPERATORGRADIENT_H
