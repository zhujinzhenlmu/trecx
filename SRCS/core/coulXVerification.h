#ifndef COULXVERIFICATION_H
#define COULXVERIFICATION_H

#include "basisMat.h"

class CoulXVerification
{
    std::vector<double> etaRange;
    std::vector<double> rRange;
    std::vector<double> kVec;
public:
    CoulXVerification(double RMax, std::vector<double> KVec);
    void calculateOverlap(const Index *Idx, UseMatrix &mat);
    void calculateMatrix(const Index *IdxPrime, const Index *Idx, UseMatrix &mat);
    void calculateRow(const Index *Idx, UseMatrix &Mat);

    static int m;
    static int mPrime;
    static int lAngular;
    static int lAngularPrime;
    static double k;
    static double kPrime;
    static double normalizationConstPrime;
    static int kIndex;
    static int kPrimeIndex;
};

namespace NassocLegendre{
static OrthogonalNassocLegendre legendreLHS(0);
}

#endif // COULXVERIFICATION_H
