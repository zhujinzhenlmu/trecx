#ifndef OPERATORSUBSPACE_H
#define OPERATORSUBSPACE_H

#include <memory>
#include "operatorTree.h"
#include "coefficients.h"

class ProjectSubspace;

/** \ingroup Structures */

///@brief Operator restricted to subspace
///
/// P Op P, where P is class ProjectSubspace
class OperatorSubspace: public OperatorTree{
    std::shared_ptr<const ProjectSubspace> _proj;
    const OperatorAbstract* _op;
    std::unique_ptr<Coefficients> _C,_D;
public:
    OperatorSubspace(const OperatorAbstract* Op); ///< infer projection from Op's Index (for Subspace&Complement hybrids)
    OperatorSubspace(const OperatorAbstract* Op, std::shared_ptr<const ProjectSubspace> Proj);
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
};


#endif // OPERATORSUBSPACE_H
