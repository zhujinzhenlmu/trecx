#ifndef OPERATORBESSELCOULOMB_H
#define OPERATORBESSELCOULOMB_H

#include "operatorFloor.h"

class OperatorAbstract;
class BasisBesselCoulomb;
class OperatorBesselCoulomb:public OperatorFloor
{
    OperatorFloor *floor;
    std::vector<std::complex<double> > bVecI,bVecJ;
    unsigned int mIdxI,mIdxJ;

public:
    OperatorBesselCoulomb(const std::string& TermOper,const BasisBesselCoulomb* IBas,
                          const BasisBesselCoulomb* JBas, std::complex<double> Multiplier);
    void axpy(const std::complex<double> &Alfa, const std::complex<double> *X, unsigned int SizX,
              const std::complex<double> &Beta, std::complex<double> *Y, unsigned int SizY) const;
    void pack(std::vector<int> &Info, std::vector<std::complex<double> > &Buf) const {DEVABORT("Not implemented");}
};

#endif // OPERATORBESSELCOULOMB_H
