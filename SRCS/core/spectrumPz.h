#ifndef SPECTRUMPZ_H
#define SPECTRUMPZ_H

#include <vector>

#include "tree.h"
#include "polylagrange.h"

class Coefficients;
class CoefficientsPermute;
class Index;
class BasisGrid;
class Plot;


/// sigma(kZ)=int dRho Rho sigma(Eta,Rn)
/// <br>
/// sigma(kZ1,kZ2)=int dRho1 Rho1 dRho2 Rho2 sigma(Eta1,Rn1,Eta2,Rn2)
///
/// (see pzSpectra.tex for algorithm)
///
/// (integrations are hard to get accurate, reason unclear)
class SpectrumPz
{
    const Index * _zIndex;
    CoefficientsPermute * _sortedC;

    Plot * toKEta;
    // interp[z][k] - interpolate in Eta for given kZ and kR
    std::vector<std::vector<std::vector<double> > > interp;
    void compute(const Coefficients* SpecEtaR, Coefficients * SpecZ, double Factor) const;
public:
    SpectrumPz(const Index* InIdx);
    Coefficients compute(const Coefficients* SpecKEta) const;
};

#endif // SPECTRUMPZ_H
