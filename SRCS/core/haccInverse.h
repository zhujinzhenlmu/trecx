// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef HACCINVERSE_H
#define HACCINVERSE_H

#include <vector>
#include "qtEigenDense.h"
#include <cfloat>
#include "operatorTree.h"
#include "inverse.h"
#include "index.h"

class DiscretizationHaCC;
class Coefficients;
class Index;


/// anti-symmetrized inverse and linear dependence correction for haCC
/// (see tsurff.pdf for algorithm)
class HaccInverse: public Inverse
{
    static double zeroThreshold;
    static double rhoThreshold;

    const DiscretizationHaCC * hacc;
    const OperatorAbstract * _ovrNonSingular;

    Operator * s0InvU; // S0^-1 U
    Operator * uAdj;   // U^H

    Operator * zeroC;  // C
    Operator * dAdjS0; // D^H S0

    Eigen::MatrixXd natOrb;    // natural orbitals
    Eigen::MatrixXcd zInv;    // Z^-1  (wrt to natural orbitals)
    Eigen::MatrixXcd nUadjC;  // U^H C (wrt to natural orbitals)

    // auxiliary storage for apply
    Coefficients *qVec,*dVec;
    std::vector<std::complex<double> > * ndVec;

    // natural orbitals for the Rho-matrix
    void corrNatOrb(const Eigen::MatrixXd & Rho, Eigen::MatrixXd &NatOrb, Eigen::VectorXd &NatPop);

    // singular vectors of anti-symmetrized overlap
    void getZeroD(std::vector<Coefficients*> &VecsD,std::vector<Coefficients*> &DualD, int MaxD) const;

    // setup U, U^H, D, D^H S0
    void getUD(const Index *Idx, const Eigen::MatrixXd &NatOrb, const std::vector<Coefficients *> DualD, const std::vector<Coefficients *> VecsD,
                Operator * ZeroD, Operator * DAdjS0,
                Operator * CorrU, Operator * UAdj);

    // make S0^-1 U continous and place U^H S0^-1 U into Zinv, U^H C into UadjC
    void getZandF(Eigen::MatrixXd &NatOrb, Eigen::VectorXd &NatPop, Eigen::MatrixXcd &Zinv, Eigen::MatrixXcd &NUadjC);

    // suppress usage of copy-constructor
    HaccInverse(const HaccInverse & Other);

    // near linearly dependent vectors
    void nearZeros(Eigen::MatrixXcd &Z, std::vector<Coefficients*> VecZ);

    // supplement with non-singular map on projected part
    class OvrNonSingular:public OperatorAbstract {
        const OperatorAbstract* _ovr;
        const HaccInverse* _inv;
    public:
        OvrNonSingular(const OperatorAbstract* Ovr,const HaccInverse * Inv):OperatorAbstract("S nonsing",Ovr->iIndex,Ovr->jIndex),_ovr(Ovr),_inv(Inv){}
        void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
    };
    class Project:public OperatorAbstract {
        const HaccInverse* _inv;
    public:
        Project(const HaccInverse* Inv):OperatorAbstract("hacc projector",Inv->iIndex,Inv->jIndex),_inv(Inv){}
        void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
            if(A!=1. or B!=0.)ABORT("only for A=1,B=0");Y=Vec;_inv->project(Y);}
    };
    class ProjectDual:public OperatorAbstract {
        const HaccInverse* _inv;
    public:
        ProjectDual(const HaccInverse* Inv):OperatorAbstract("hacc projector",Inv->iIndex,Inv->jIndex),_inv(Inv){}
        void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
            if(A!=1. or B!=0.)ABORT("only for A=1,B=0");Y=Vec;_inv->projectDual(Y);}
    };
    class _PS0inv:public OperatorAbstract {
        const HaccInverse* _inv;
    public:
       _PS0inv(const HaccInverse* Inv):OperatorAbstract("hacc projector",Inv->iIndex,Inv->jIndex),_inv(Inv){}
        void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
            if(A!=1. or B!=0.)ABORT("only for A=1,B=0");_inv->iIndex->localInvOvr()->apply(1.,Vec,0.,Y);_inv->project(Y);}
    };

    // apply with singular vectors removed
    void ovr1(std::complex<double> A, Coefficients & X, std::complex<double> B, Coefficients & Y) const;
    void inv1(std::complex<double> A, Coefficients & X, std::complex<double> B, Coefficients & Y) const;

  Eigen::MatrixXcd vNonz;

  class Inv1: public OperatorAbstract{
      const HaccInverse * haccI;
  public:
      Inv1(const HaccInverse * HaccI):OperatorAbstract("Inv1",HaccI->iIndex,HaccI->jIndex),haccI(HaccI){}
      void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const
      {haccI->inv1(A,*const_cast<Coefficients*>(&Vec),B,Y);}
  };
  class Ovr1: public OperatorAbstract{
      const HaccInverse * haccI;
  public:
      Ovr1(const HaccInverse * HaccI):OperatorAbstract("Ovr1",HaccI->iIndex,HaccI->jIndex),haccI(HaccI){}
      void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const
      {haccI->ovr1(A,*const_cast<Coefficients*>(&Vec),B,Y);}
  };
  class Inv1Ovr1: public OperatorAbstract{
      const HaccInverse * haccI;
  public:
      Inv1Ovr1(const HaccInverse * HaccI):OperatorAbstract("IO1",HaccI->iIndex,HaccI->jIndex),haccI(HaccI){}
      void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const
      {
          haccI->ovr1(A,*const_cast<Coefficients*>(&Vec),0.,*tempLHS());
          {haccI->inv1(1.,*tempLHS(),B,Y);}
      }
  };
  class Ovr1Inv1: public OperatorAbstract{
      const HaccInverse * haccI;
  public:
      Ovr1Inv1(const HaccInverse * HaccI):OperatorAbstract("IO1",HaccI->iIndex,HaccI->jIndex),haccI(HaccI){}
      void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const
      {
          haccI->inv1(A,*const_cast<Coefficients*>(&Vec),0.,*tempLHS());
          {haccI->ovr1(1.,*tempLHS(),B,Y);}
      }
  };
  void analyze() const;

public:
    ~HaccInverse();
    HaccInverse(const DiscretizationHaCC * Hacc);
    /// inverse overlap (preceded by make continuous)
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;   
    void apply0(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
    void applyCorrection(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const{
        if(A!=1. or B!=0.)ABORT("implemented only for A=1,B=0");
        Y.makeContinuous();
        applyCorrection(&Y);
    }

    void project(Coefficients & Vec) const; ///< remove zero vectors from ket-vector: vec = (1- D D^H S0) vec
    void projectDual(Coefficients & Vec) const; ///< remove zero vectors from ket-vector: vec = (1- D D^H S0) vec

    void applyCorrection(Coefficients * V) const;

    // coefficients local versions
    void applyCorrection(std::complex<double> A, const CoefficientsLocal &Vec, std::complex<double> B, CoefficientsLocal &Y) const;
    void apply0(std::complex<double> A, const CoefficientsLocal &Vec, std::complex<double> B, CoefficientsLocal &Y) const;

    void parallelSetup() const;
    const OperatorAbstract* ovrNonSingular() const{return _ovrNonSingular;}

};

#endif // HACCINVERSE_H
