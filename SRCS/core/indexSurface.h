// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef INDEXSURFACE_H
#define INDEXSURFACE_H

#include "indexDerived.h"

/// \ingroup
/// \brief special index for tSurff surfaces
class IndexSurface : public IndexDerived
{
public:
    IndexSurface(const Index* I, std::vector<double> Radius, unsigned int NSurf);
};

#endif // INDEXSURFACE_H
