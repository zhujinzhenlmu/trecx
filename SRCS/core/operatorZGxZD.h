// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORZGXZD_H
#define OPERATORZGXZD_H

#include <string>
#include <vector>
#include <complex>
#include "qtEigenDense.h"

class UseMatrix;
class Index;

#include "operatorTensorProduct.h"

/** \ingroup OperatorFloors */
/// tensor product of (complex diagon) x (complex general) matrices
class OperatorZGxZD: public OperatorTensorProduct
{
public:
    OperatorZGxZD(std::vector<const UseMatrix *> Mat, std::string Kind);
    void axpy(const std::complex<double> & Alfa, const std::complex<double>* X, unsigned int SizX,
              const std::complex<double> & Beta, std::complex<double>* Y, unsigned int SizY) const;
    OperatorZGxZD(const std::vector<int> &Info, const std::vector<std::complex<double> >&Buf)
        :OperatorTensorProduct(Info,Buf,"ZGxZD"){addDat(Buf,subRows[0]*subCols[0],subRows[1]);}
  Eigen::MatrixXcd matrixFactor(int D) const;
};

#endif // OPERATORZGXZD_H
