// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORLOWRANK_H
#define OPERATORLOWRANK_H

#include "operatorSingle.h"
class CoefficientsFloor;

/** \ingroup Structures */
/// (incomplete)
class OperatorLowRank : public OperatorSingle { //i think this object does not work as intended, and it is used nowhere used
public:
    OperatorLowRank(const std::string & name, const std::string & definition, Discretization * iDisc, Discretization * jDisc,
                    std::vector<int> & iBlock, std::vector<int> & jBlock);
    ~OperatorLowRank();
    virtual void axpy(CoefficientsFloor & X,  CoefficientsFloor & Y, std::complex< double > & factor) const;
    void inverse();
    bool isZero(double Eps=0.) const;
};


#endif // OPERATORLOWRANK_H
