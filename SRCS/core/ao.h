// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef __AO__
#define __AO__

#include "primitive_gaussians.h"


///
/// \brief The ao class: Atomic orbitals class composed of primitive Gaussians
///  Note: Includes specific normalizations (normalize()) as used in the COLUMBUS code.
///

class ao{

 public:

  primitive_gaussians pg;
  Eigen::MatrixXd coef;
 
  ao(columbus_data *col_data);
  ao(QuantumChemicalInput & ChemInp);
  int number_aos();
  void normalize();
  void ao_matrix_1e(Eigen::MatrixXcd &result, std::string op, ao* b=nullptr);
  void values(Eigen::MatrixXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz);
  void values(Eigen::VectorXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, int fun_index);
  void derivatives(Eigen::MatrixXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, std::string wrt);
  void derivatives(Eigen::VectorXd& val, Eigen::Matrix<double, Eigen::Dynamic, 3>& xyz, int fun_index, std::string wrt);
};

#endif
