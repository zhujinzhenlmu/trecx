// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef __EXTRA_FUNCTIONS__
#define __EXTRA_FUNCTIONS__

#include "qtAlglib.h"
#include "qtAlglib.h"
#include "qtEigenDense.h"
#include <vector>
#include <iostream>

//#include "eigenNames.h"


void h_roots(int N , Eigen::VectorXd &x, Eigen::VectorXd &w);
void p_roots(int N , Eigen::VectorXd &x, Eigen::VectorXd &w, double lb=-1.0, double ub=1.0);
void l_roots(int N, Eigen::VectorXd &x, Eigen::VectorXd &w, double x0, double alpha);
void lobatto_quadrature(int order, Eigen::VectorXd & quadraturePoints,Eigen::VectorXd & weights, double lb=-1.0, double ub=1.0);

long factorial (int a);
double binomial(int n,int l);

bool parity_of_permutation(Eigen::VectorXi &o, Eigen::VectorXi p);
void generate_permutations(Eigen::VectorXi &o, std::vector<Eigen::VectorXi > &p, std::vector<bool > &par);

class scaled_legendre{

    double x0;
    double x1;
    int order;
    Eigen::VectorXd norms;

public:

    scaled_legendre(double x0, double x1, int order);
    void values(const Eigen::VectorXd& x, Eigen::MatrixXd& vals);

};

class scaled_laguerreexpon{

    double x0;
    double alpha;
    double decay_factor;
    int order;
    Eigen::VectorXd norms;

public:

    scaled_laguerreexpon(double x0, double alpha, int order);
    void values(const Eigen::VectorXd& x, Eigen::MatrixXd& vals);

};
#ifndef JAKOB

//void output_ionization_rates_to_csv(std::vector<double > field, Eigen::VectorXcd ev, int cif.numI);
#endif

#endif
