// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef __TIMEPROPAGATOR__
#define __TIMEPROPAGATOR__
#include <vector>
#include <string>
#include "linSpaceMap.h"
#include "coefficientsLocal.h"
#include "odeRK4.h"
#include "odeSIA.h"

#include "timePropagatorOutput.h"

//forward class declarations
class OperatorAbstract;
class Wavefunction;
class Plot;
class DerivativeFlat;
class ReadInput;

class TimePropagator {

    // currently not used:
//    class MapTime{
//        double _mapped;
//        MapTime(double Time):_mapped(-Time){}
//        double operator()(){return _mapped;}
//        double external(){return -_mapped;}
//    };

    double h; // actual time step size
    double h_max; // maximal time step size
    double h_fix; // fixed time step (used if !=0)
    double hSum,hSquareSum;
    double desiredPrecision;
    unsigned int successfulTimeSteps;
    unsigned int failedTimeSteps;

    //MapTime mapTime;
    TimePropagatorOutput * out;
    OdeStep<LinSpaceMap<Coefficients>,Coefficients> *ode;
    OdeStep<LinSpaceMap<CoefficientsLocal>,CoefficientsLocal> *odeLocal;
    void propagate_intern(Coefficients *C, double &Time, double TEnd); ///< propagate without printing, but possibly writing

public:
    Operator* halfPlane;
    /// constructor for abstract ODE solver and Output
    TimePropagator(OdeStep<LinSpaceMap<Coefficients>,Coefficients> * Ode, TimePropagatorOutput * Out, double Precision, double FixStep);
    TimePropagator(OdeStep<LinSpaceMap<CoefficientsLocal>,CoefficientsLocal> * Ode, TimePropagatorOutput * Out, double Precision, double FixStep);

    void fixStep(double StepSize){h_fix=StepSize;}

    /// propagate and print out
    void propagate(Wavefunction * Wf, double tEnd, const std::string Mode="StartAndStop");

    /// string describing the time propagator
    std::string str(unsigned int Brief) const;

    /// current time-propation info
    std::string info() const;

    /// standard format for reading TimePropagator parameters
    static void read(ReadInput &Inp, double &tBeg, double &tEnd, double &tPrint, double &tStore,
                     double &accuracy, double &cutE, double & FixStep, double &ApplyThreshold,std::string & Method);
    static void print(double tBeg, double tEnd, double tPrint, double tStore, double accuracy, double cutE, double  FixStep,
                              double ApplyThreshold);
};

#endif
