#ifndef COULXDERNEW_H
#define COULXDERNEW_H

#include "operatorAbstract.h"

class Inverse;
class Coefficients;
class OperatorTree;
class SurfaceConstraint;

class CoulXDerNew:public OperatorAbstract
{
    const OperatorAbstract *_op;
    SurfaceConstraint *_surfConstr;
    const Inverse *_inv;
    Coefficients *tempCoeffs;
    void update(double Time, const Coefficients* CurrentVec=0);
public:
    CoulXDerNew(const OperatorAbstract *Op, SurfaceConstraint *SurfConstr);
    ~CoulXDerNew();
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
};

#endif // COULXDERNEW_H
