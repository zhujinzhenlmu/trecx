// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef INDEXPROD_H
#define INDEXPROD_H

#include "index.h"

/// \ingroup Index
/// \brief tensor product of two indices
class IndexProd : public Index
{
public:
    IndexProd(const Index *A, const Index * B);
};

#endif // INDEXPROD_H
