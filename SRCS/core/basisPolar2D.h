#ifndef BASISPOLAR2D_H
#define BASISPOLAR2D_H

#include "basisNdim.h"

class ReadInput;
class BasisPolar2D  : public BasisNdim
{
    std::vector<double> _origin;
    const Index* _idx; // basis in polar coordinates around _origin
    const Index* idxConstruct(double RadMax, int Mmax, int Nrad);
    std::string selectCoor(const std::string Coor) const; ///< select admissible subset
    Index * productIndex(const BasisSet *Radial, int Mmax);
protected:
    // all necessary transformations
    std::vector<double> toCartesian(const std::vector<double>&CoorNdim) const;
    double absFactor(const std::vector<double>&CoorNdim) const {return 1.;}
    double nablaFactor(const std::vector<double>&CoorNdim,int I) const {return 0.;}
    Eigen::MatrixXd jacobianToNdim(const std::vector<double>&CoorNdim) const;
public:
    BasisPolar2D(ReadInput & Inp);

};

#endif // BASISPOLAR2D_H
