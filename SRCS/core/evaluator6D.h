// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef EVALUATOR_H
#define EVALUATOR_H

#include <vector>
#include "tools.h"
#include "CircularQueue.h"

#include "coefficientsWriter.h"

// Forward declaration(s)
class ReadInput;
class UseMatrix;
class Coefficients;
class Wavefunction;
class Discretization;
class DiscretizationDerived;
class Operator;
class TsurffSource;
class OperatorMapChannelsSurface;
class SpectrumTinfinite;
class Index;

namespace tSurffTools {

/**
 * \ingroup tSurff
 * \brief Framework for the calculation of electron spectra (legacy code)
 *
 * Differs from Evaluator in that it uses TsurffSource and does not rely
 * on grid transformations. However, that fact removes the optimization of summing
 * the spectrum on the grid.
 *
 * Set up discretizations, get Pulse and the surface values from file
 */
class Evaluator6D
{
    friend class SpectrumTinfinite;
public:
    Evaluator6D(ReadInput &inp);
    ~Evaluator6D();

    /*!
     * \brief Compute the electron spectrum.
     * \param timeResolution gives the time resolution in the time integration.
     * \param maxEnergy the maximum energy to calculate the spectrum.
     * \param energyResolution density of the energies for which the spectrum is calculated.
     *
     * Compute the spectrum for given momenta (given by maximum and resolution of energy) and time resolution in time integration.
     * Only stores a limited number of times, which is set in bufferSize
     */
    void computeSpectrum(double Tend);

    /*!
     * \brief plotSpectrum After computation of the spectrum, plot it.
     * \param levels2plot specify grid for levels to plot (as in grid transformation)
     * sums over all but the levels specified for plotting
     */
    void plotSpectrum(ReadInput &Inp, const Coefficients &AmplitudeSmooth, std::string ExtName=""); // To be called AFTER computeSpectrum!

    Coefficients * amplitude() const {return averagedResult;}
private:
    std::vector<std::string> inputFlags;
public: //HACK - friendship does not seem to work
    std::string outputDir; //!< directory containing the outputFile
private:
    bool overwrite;      ///< by default, most recent spectrum overwrites previous
    bool forceCompute;      ///< re-compute, even if ampltude file is present
    std::string plotWhat; ///< what to plot: compute,total,partial,2dim, etc.
    std::string amplNum; ///<  number of amplitude files (if multiple are found)

    double runPropagationTime; ///< original run propagation time
    double integrationTime; ///< how far to integrate
    double timeResolution; ///< in tSURFF integration, step size

    // tsurff Source object
public://HACK - friendship does not seem to work
    std::vector<TsurffSource *> tSource;      ///< Object that handles Surface Flux
private:
    // transformation to standard gauge may be needed
    Coefficients * gaugeTemp;

    void standardGauge(double Time,Coefficients & C);

    const static std::string inputExtension;
public://HACK - friendship does not seem to work
    Discretization* D; //!< basic discretization
private:
    Coefficients* result; ///< contains the sum of the integrands already calculated

    double beginAveraging; //!< Time where to start averaging over the spectrum
    Coefficients* averagedResult; //!< accumulated spectrum for averaging

    OperatorMapChannelsSurface* channelHelper; //!< Channel helper

    void getFlags(ReadInput &Inp, int &radialPoints, double &maxEnergy, bool &kGrid);
    void print(int radialPoints, double maxEnergy, bool kGrid) const;


private:
    /**
     * Store results also in si_spec, for processing with spec.py
     */
    class SiSpec{
        double tStore;
        double tLastAverage;
        double tBeginAverage;
        double tEndAverage;

        CoefficientsWriter spec;
        Coefficients* avg;

    public:
        ~SiSpec();
        SiSpec(const Index* Idx, std::string OutputDir, double TStore=0.);
        void average(Coefficients* C, double Time);

    };
};

/*!
 * \brief computeMomenta calculates momenta according to given maximal energy and energy resolution and stores them in a vector
 * \param[in] maxEnergy maximum energy covered
 * \param[in] energyResolution energy resolution of the momenta
 * \param[out] momenta empty vector, to be filled with momenta
 *
 * Maybe add the possibility to compute equidistant points?
 */
void computeMomenta(double maxEnergy, double energyResolution, std::vector<double> &momenta);

unsigned int sizeOfAxis(const Discretization* disc, std::string name);

} // tSurffTools

#endif // EVALUATOR_H
