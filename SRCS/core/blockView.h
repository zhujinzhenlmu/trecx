// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BLOCKVIEW_H
#define BLOCKVIEW_H

#include <vector>
#include <complex>

#include "operatorAbstract.h"
#include "index.h"
#include "operatorAbstract.h"

#include "operatorTree.h"
#include <Sparse>


/// view operator as a matrix of Operators
class BlockView: public OperatorAbstract
{
    static void multiplicities(const Index * Idx, std::vector<unsigned int> &Glob, int & Dim, std::vector<double>& Norm);
public:
    struct Triplet{
        int i,j;
        Triplet(){}
        Triplet(int I, int J):i(I),j(J){}
        std::vector<const OperatorAbstract*> block;
        void add(const OperatorAbstract *Block);
        int blockRow() const {return i;}
        int blockCol() const {return j;}
    };
    std::vector<Triplet> _trip;
public:
    static const int depthFloor=INT_MAX;
    static const int depthContinuity=INT_MAX-1;

    virtual ~BlockView(){}
    BlockView(){}
    BlockView(const OperatorTree *Op, int IDepth=depthContinuity, int JDepth=depthContinuity);
    BlockView(const OperatorAbstract *Op);

    /*
     * These are needed by ParallelOperator - but deleted by Labelled
     */
    BlockView(const BlockView& Other): _trip(Other._trip){}
    BlockView& operator=(BlockView&& Other){
        _trip = std::move(Other._trip);
        return *this;
    }

    void update(double Time, const Coefficients* CurrentVec){} /// dummy (no time-updates for BlockView)
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;

    int superDiagonals() const; ///< number of superdiagonals as for banded storage
    int subDiagonals() const; ///< number of subdiagonals as for banded storage

    /// returns one BlockView for each multiplicative factor (for now: only if floor blocks)
    void splitByFactors(std::vector<BlockView> & Part, std::vector<std::complex<double> *> &Factor);

    std::vector<std::complex<double> > bandedStorage() const; ///< data in banded storage (LAPACK style)
    void sparseMatrix(Eigen::SparseMatrix<std::complex<double> > & Mat, bool Contract=false) const; ///< matrix in sparse storage

    std::string normsMatrix(int Digits=0);
    int blockRows() const;
    int blockCols() const;
    std::string str() const;

    int triplets() const{return _trip.size();}
    int blocks(int Triplett) const {return _trip[Triplett].block.size();}
    const OperatorAbstract* block(int Triplett, int Block) const {return _trip[Triplett].block[Block];}
};

#endif // BLOCKVIEW_H
