// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATOROVERLAP_H
#define OPERATOROVERLAP_H

#include "operatorAbstract.h"

class OperatorOverlap : public OperatorAbstract
{
public:
    /// \brief if blocked, operator will be distributed over index hierarchy
    OperatorOverlap(const OperatorAbstract * Ovr);
};

#endif // OPERATOROVERLAP_H
