// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef COEFFICIENTSVIEWDEEP_H
#define COEFFICIENTSVIEWDEEP_H

#include <string>
#include <complex>

#include "coefficients.h"

class CoefficientsViewDeep {
    Coefficients _view;
    static void extend(Coefficients* View, std::complex<double>* Data);
    static void view(Coefficients* View, Coefficients* C);
    static void disown(Coefficients*);
public:
    CoefficientsViewDeep(const Index* Idx, int Depth=INT_MAX, bool paraSub=false);
    Coefficients* view(Coefficients* C);
    std::string str() const {return _view.str();}
};

#endif // COEFFICIENTSVIEWDEEP_H
