// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef COEFFICIENTSLOCAL_H
#define COEFFICIENTSLOCAL_H
#include "coefficients.h"

#include <map>
#include <memory>

class Index;
class CoefficientsGlobal;
class ParallelContinuity;

/** \ingroup Coefficients */

/// @brief only data that are local on a given thread are stored
///
/// contiguous  and ordered storage matches the corresponding section in CoefficientsGlobal
class CoefficientsLocal: public Coefficients{
    static std::map<std::string,CoefficientsLocal*> _views;
    static std::complex<double> _dummyStorage;
    long unsigned int _size;
    static int setSize(const Coefficients* C);
public:
    std::shared_ptr<ParallelContinuity> newCont;
public:
    static bool local(const Coefficients*C); ///< true if C->idx() is on local process or not owned by any process
    static CoefficientsLocal* view(Coefficients* C); ///< local view on Coefficients
//    static CoefficientsLocal* view(CoefficientsGlobal* C); ///< local view on Global (keep data in place, i.e. Global remains intact)

    CoefficientsLocal(const CoefficientsLocal & Other);
    CoefficientsLocal():_size(0){}
    CoefficientsLocal(const Index *I, std::complex<double> Val=0.); ///< local Coefficient
    std::complex<double> * storageData();
    long unsigned int size() const {return _size;}
    double norm() const;

    double localNorm() const {return Coefficients::norm();}
    CoefficientsLocal & operator=(const CoefficientsLocal & Other);
    void makeContinuous(double Scal=1.);
    CoefficientsLocal * cwiseProduct(const CoefficientsLocal & Rhs);


};

#endif // COEFFICIENTSLOCAL_H
