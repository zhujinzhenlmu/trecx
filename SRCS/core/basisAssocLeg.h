#ifndef BASISASSOCLEG_H
#define BASISASSOCLEG_H

#include "basisIntegrable.h"

class BasisAssocLeg: public BasisIntegrable
{
    int _mAbs;
    int _size;
    void test();
public:
    BasisAssocLeg(std::string Def);
    std::string name() const{return "assocLegendre";}
    unsigned int size() const {return _size;}
    unsigned int order() const {return _size+_mAbs;}
    double physical(int Index) const {return _mAbs+Index;}
    double lowBound() const{return -1.;}
    double upBound() const{return 1.;}

    std::string str(int Level=0) const {return name()+"{"+tools::str(_mAbs)+"} ["+tools::str(size())+"]";}
    std::string strDefinition() const {return std::string("AssociatedLegendre: ")+tools::str(_mAbs)+","+tools::str(size());}

    void valDer(const std::vector<std::complex<double> > & X,
                std::vector<std::complex<double> > & Val,
                std::vector<std::complex<double> > & Der, bool ZeroOutside=false) const;
    void quadRule(int N, std::vector<double> & QuadX, std::vector<double> & QuadW) const;
    bool operator==(const BasisAbstract& Other) const {return strDefinition()==Other.strDefinition();}
};

#endif // BASISASSOCLEG_H
