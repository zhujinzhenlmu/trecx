// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef DISCRETIZATIONCONSTRAINED_H
#define DISCRETIZATIONCONSTRAINED_H

#include "discretizationDerived.h"


/** \ingroup Discretizations */

/// reads input from file to constrain a given discretization
class DiscretizationConstrained: public DiscretizationDerived{
    std::string constString;
public:
    DiscretizationConstrained(const Discretization *D, ReadInput &Inp);
    std::string constraints() const {return constString;}

    static bool inputs(ReadInput & Inp); ///< parse all inputs
};

#endif // DISCRETIZATIONCONSTRAINED_H
