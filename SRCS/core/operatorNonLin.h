#ifndef OPERATORNONLIN_H
#define OPERATORNONLIN_H


#include "eigenSolverAbstract.h"
#include "eigenSolver.h"

class OperatorNonLin : public OperatorTree
{
protected:
    OperatorFloor * oFloor;
    bool _view;
public:
    OperatorNonLin(const std::string Name, const OperatorDefinition & Definition, const Index *IIndex, const Index *JIndex);
    void update(double Time, Coefficients* C);
};
#endif
