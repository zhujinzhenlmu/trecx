#ifndef OPERATORIDENTITY_H
#define OPERATORIDENTITY_H

#include "operatorTree.h"
#include "wavefunction.h"
#include "coefficients.h"
class OperatorIdentity : public OperatorTree
{
public:
    OperatorIdentity():OperatorTree(){name="Identity";}
    void axpy(Wavefunction & X, Wavefunction & Y, bool transpose = false) const {Y+=X;} ///< add input to output
    void axpy(Coefficients & X, Coefficients & Y) const {Y+=X;}///< add input to output
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const {Y.axpy(A,Vec,B);}
};

#endif // OPERATORIDENTITY_H
