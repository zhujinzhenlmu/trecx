#ifndef PATCHRADIAL_H
#define PATCHRADIAL_H

#include "qtEigenDense.h"
#include <vector>
#include "densityMatrix1.h"
#include "abort.h"

class BasisIntegrable;
class OperatorTree;
class OperatorFloor;

///@brief collects operator leaf's of a given Floor type by the Floors' radial bases
///
/// sort into a hierarchy of LeafM[ma,mb].LeafL[la,lb].LeafC[chanA,chanB]
template<class Floor>
class PatchRadial{
    const BasisIntegrable *_basA,*_basB;

    /// get channel number and M,L quantum numbers for given Idx
    static void getChanML(const Index* Idx,int & Chan, int &M, int &L){
        M=INT_MAX;
        L=INT_MAX;
        Chan=INT_MAX;
        while(Idx->parent()!=0){
            if(Idx->parent()->axisName()=="Eta")L=Idx->parent()->basisAbstract()->physical(Idx->nSibling());
            if(Idx->parent()->axisName()=="Phi")M=Idx->parent()->basisAbstract()->physical(Idx->nSibling());
            if(Idx->parent()->axisName()=="Channel")Chan=Idx->nSibling();
            if(Chan!=INT_MAX and M!=INT_MAX and L!=INT_MAX){
               if(std::abs(M)>L)DEVABORT(Sstr+"BAD: c,m > l"+Chan+M+L);
                return;
            }
            Idx=Idx->parent();
        }
        ABORT("need axes Channel, Eta, and Phi, found hierarchy: "+Idx->root()->hierarchy());
    }

    /// true if Leaf is to be selected
    bool select(const OperatorTree &Leaf, const BasisIntegrable *&BasA, const BasisIntegrable *&BasB){
        const Floor* f=dynamic_cast<const Floor*>(Leaf.floor());
        if(not f or f->ignore())return false;
        if(BasA==0){
            BasA=Leaf.iIndex->basisIntegrable();
            BasB=Leaf.jIndex->basisIntegrable();
            return true;
        }
        if(     BasA->lowBound()==Leaf.iIndex->basisIntegrable()->lowBound() and
                BasB->lowBound()==Leaf.iIndex->basisIntegrable()->lowBound())return true;
        return false;
    }
public:

    /// patch of all Operator leafs that match first undefined
    const BasisIntegrable* aBas() const {return _basA;} ///< left radial basis of patch
    const BasisIntegrable* bBas() const {return _basB;} ///< right radial basis of patch

    struct LeafC{
        /// matrix elements for a given radial patch and la,ma,lb,mb and channel pair CaCb (channel numbering as in class DensityMatrix1)
        LeafC(OperatorTree* Op, int CaCb, std::string Kind):op(Op),cc(CaCb)
        {
            mat=&dynamic_cast<Floor*>(op->floor())->mat();
            if(Kind=="full")         *mat=Eigen::MatrixXcd::Zero(op->iIndex->size(),op->jIndex->size());
            else if(Kind=="diagonal")*mat=Eigen::MatrixXcd::Zero(op->iIndex->size(),1);
            else ABORT("temporary matrix either Kind=\"full\" or -\"diagonal\"");
        }
        int cc;
        OperatorTree* op; ///< pointer to operator leaf
        Eigen::MatrixXcd *mat; ///< temporary storage for accumulating matrix
    };

    struct LeafL{
        /// all channel leafs for given ma,mb,la,lb
        LeafL(int La,int Lb):la(La),lb(Lb){}
        int la,lb;
        std::vector<LeafC> leafs;
    };

    struct LeafM{
        /// all channel leafs for given ma,mb
        LeafM(int Ma,int Mb):ma(Ma),mb(Mb){}
        int laMax() const {int ll=0; for(LeafL l: leafs)ll=std::max(ll,l.la); return ll;}
        int lbMax() const {int ll=0; for(LeafL l: leafs)ll=std::max(ll,l.lb); return ll;}

        int ma,mb;
        std::vector<LeafL> leafs;
    };
    std::vector<LeafM> leafs; // all leafs, collected by ma,mb

    /// next RadialPatch in Op, using first non-finalize()'d  leaf of Op
    PatchRadial(OperatorTree* Op, const DensityMatrix1 & Rho, std::string Kind)
        :_basA(0),_basB(0){
        for(OperatorTree * o=Op->firstLeaf();o!=0;o=o->nextLeaf()){
            if(select(*o,_basA,_basB)){
                // sort patch into list
                int ma,la,mb,lb,chanA,chanB;
                getChanML(o->iIndex,chanA,ma,la);
                getChanML(o->jIndex,chanB,mb,lb);

                int m,l;
                for (m=0;m<leafs.size() and (leafs[m].ma!=ma or leafs[m].mb!=mb);m++);
                if(m==leafs.size())leafs.push_back(LeafM(ma,mb));

                for (l=0;l<leafs[m].leafs.size() and (leafs[m].leafs[l].la!=la or leafs[m].leafs[l].lb!=lb);l++);
                if(l==leafs[m].leafs.size())leafs[m].leafs.push_back(LeafL(la,lb));
                leafs[m].leafs[l].leafs.push_back(LeafC(o,Rho.cacb(chanA,chanB),Kind));
            }
        }
    }
    int laMax(){int lmax=0; for(LeafM l: leafs)lmax=std::max(lmax,l.laMax());return lmax;} ///<largest la in PatchRadial
    int lbMax(){int lmax=0; for(LeafM l: leafs)lmax=std::max(lmax,l.lbMax());return lmax;} ///< larges lb in PatchRadial
    const LeafC & firstC() const {return leafs[0].leafs[0].leafs[0];}
};


#endif // PATCHRADIAL_H
