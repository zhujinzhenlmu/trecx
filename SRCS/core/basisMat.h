// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASISMAT_H
#define BASISMAT_H

#include "useMatrix.h"
#include "basisSet.h"
#include "abort.h"
#include <map>
#include <cmath>

//#include <boost/math/special_functions.hpp>
//#include <boost/lexical_cast.hpp>

#include "integrate.h"
#include "algebra.h"

/** @addtogroup OperatorData Operator definitions
 *  @{
*/

/// @brief  list of user-defined functions
class UserFunctions{
    friend class BasisMat;
    friend class FunctionOneArg;
    static bool haveBeenSet;
    static void set();
public:
    static void list();
};


/// @brief  abstract base class for user defined functions in matrix calculations
class basisMatFunc {
public:
    virtual ~basisMatFunc(){}

    /// @brief function name as used in operator strings
    ///
    /// example name="One", 1d matrix string "<One[1,2]>" for matrix elements of the characteristic function on interval [1,2]
    std::string name;
    virtual std::complex<double> operator() (std::complex<double>) const
    {ABORT("function undefined for single argument: "+name);} ///< function value
    /// function with vector of arguments
    virtual std::complex<double> operator() (std::vector<std::complex<double> > Q) const{
        if(Q.size()!=1)ABORT("function only defined for single argument: "+name);
        return operator()(Q[0]);
    }

    virtual basisMatFunc & set(const std::string Par){return *this;}         ///< may update parameter
    basisMatFunc(std::string Name="UNDEFINED"):name(Name){}

    virtual bool sameParameters(const basisMatFunc &Other) const {ABORT("multiple construction of userFunction \""+name+"\", need class method sameParameters");}

    /// check whether X has required size (recommended to always use for checking input errors)
    void needSize(std::vector<std::complex<double> > X,unsigned int Size) const {
        if(X.size()!=Size)ABORT(name+" requires argument vector size="+tools::str(Size)+", is="+tools::str(int(X.size())));
    }
};

/// \cond DEV
///< @brief model instantiation of basisMatFunc
class basisMatExp:public basisMatFunc {
    std::complex<double>exponent;
public:
    basisMatExp(std::complex<double> Exponent):basisMatFunc("Exp"),exponent(Exponent){}
    std::complex<double> operator()(std::complex<double> x){return exp(exponent*x);}
    basisMatExp & set(std::string Par=""){if(Par!="")exponent=tools::string_to_complex(Par);return *this;}
};


//! \brief exponential function \f$ \frac{d^n}{(dx)^n}\exp[ikr] \f$ with hindsight to 1d tSURFF
class basisMatExpI: public basisMatFunc {
public:
    basisMatExpI(std::string par = ""): basisMatFunc("ExpI"), der(0), surface(1.) {
        if (par!="") {set(par);}
    }

    virtual std::complex<double> operator() (std::complex<double> k) const {
        return pow(std::complex<double>(0., 1)*k, der)*exp(std::complex<double>(0., surface)*k);
    }

    virtual basisMatFunc& set(const std::string Par) {
        std::vector<std::string> parameters(tools::splitString(Par, ','));
        if (parameters.size()!=2) {
            std::cerr << "Failed to assign new parameters: wrong number of parameters!" << std::endl;
            return *this;
        }
        der=tools::string_to_int(parameters[0]);
        surface=tools::string_to_double(parameters[1]);
        return *this;
    }

    double getSurface() const {return surface;}
    int getDer() const {return der;}
private:
    double surface; // naming with 1d tSURFF in mind
    unsigned int der; ///< order of derivative; 0--value, 1--derivative, -1--integral etc.
};

//! \brief spherical Besselfunction: order of derivative (0--value,1--derivative), order, scaling (abscissa)
class basisMatSpherBessel: public basisMatFunc {
public:
    explicit basisMatSpherBessel(std::string par = ""):
        basisMatFunc("spherBessel"), der(0), order(0), offset(0), surface(1.) {
        if (par!="") { set(par); }
    }
    virtual std::complex<double> operator() (std::complex<double> k) const {
        if (der==0) { // value
            // assume real k for sph_bessel; does not work with complex<double>
            DEVABORT("non-boost");//return sqrt(2./M_PI)*pow(std::complex<double>(0,-1.),order+offset)*boost::math::sph_bessel(order+offset, k.real()*surface);
        }
        else if (der==1) { // derivative; use recurrence relation
            DEVABORT("non-boost");
            return 0.;
//            return  sqrt(2./M_PI)*pow(std::complex<double>(0,-1.),order+offset)
//                    *(boost::math::sph_bessel(order+offset, k.real()*surface)/surface*(order+offset)
//                      -boost::math::sph_bessel(order+offset+1, k.real()*surface)*k.real());
        }
    }

    virtual basisMatFunc& set(const std::string Par) {
        std::vector<std::string> parameters(tools::splitString(Par, ','));
        if (parameters.size()!=4) {
            std::cerr << "Failed to assign new parameters: wrong number of parameters!" << std::endl;
            return *this;
        }
        unsigned int tempDer =tools::string_to_int(parameters[0]);
        if (tempDer==0 or tempDer==1) {std::swap(der,tempDer);}
        else {std::cerr << "CANNOT HANDLE DERIVATIVE OF ORDER "+parameters[0] << "! DO NOT CHANGE ORDER!\n";}
        order  =tools::string_to_int(parameters[1]);
        offset =tools::string_to_int(parameters[2])/2 +tools::string_to_int(parameters[2])%2;
        surface=tools::string_to_double(parameters[3]);
        return *this;
    }

    unsigned int getDer() const {return der; }
    unsigned int getOrder() const {return order; }
    double getSurface() const {return surface; }
private:
    unsigned int der, order, offset;
    double surface; // with hindsight of using it for tsurff with Volkov states
};
/// \endcond

class BasisSet;

/// \brief (legacy code, to be replaced) compute and keep matrices \f$ \langle i | \hat{O} | j \rangle \f$ for various operators \f$ \hat{O} \f$
class BasisMat {
public:
    enum kind {
        map,                //!< map rhs basis set onto lhs
        function,           //!< given by a function through basisMatFunc class; this should only be used with the right get() function WHAT IS THE RIGHT GET FUNCTION!
        // note: the following are actually functions and should go into a separate map or enum
        legendre,           //!< \f$ < f| legendre[l]> \f$ requires specification of l
        lobatto,           //!< \f$ < f| lobatto[l]> \f$ requires specification of l
        gauss,               //!< \f$ < f| gauss(widht)> \f$ requires specification width
        delta,               //!< \f$ < f| \delta(q-q_0)> \f$ requires specification of \f$q_0\f$ in the form delta[q0]
        noKind             //!< undefined string
    }; //!< unique labels (can be extended as needed)

    static bool femDVR;
    static std::map<std::string,basisMatFunc*> BasisMatFuncs;
    static void cleanUp();

    /// for now, all operators are local; modify, as non-local operators are added
    static bool isLocal(kind Kind){
        switch(Kind){
        default: ABORT("define kind as local or not");
        case noKind: return true;
        case map: return false;
        case function: return true;
            // note: the following should go into a separate map or enum
        case legendre: return true;
        case lobatto: return true;
        case gauss: return true;
        case delta: return true;
        }
    }

    static kind kindStr(std::string Name) ///< access kind's through strings, e.g. kindStr["dJd"]
    {
        if(Name=="map")            return map;
        // note: the following should go into a separate map or enum
        if(Name=="legendre")       return legendre;
        if(Name=="lobatto")        return lobatto;
        if(Name=="gauss")          return gauss;
        if(Name=="delta")          return delta;
        return noKind;
    }
    static unsigned int extraPoints(kind Kind, const BasisSet & Bas); ///< additional points for accurate quadrature
    static std::vector<std::vector<std::map<std::string,UseMatrix> > > table; //!< table of matrices between BasisSet's
    static std::map<std::string,std::map<std::string,const UseMatrix*> >  tableMats; //!< table of matrices between BasisSet's

    /// @cond DEV
    /// matrix and parameter string
    struct MatPar{
        UseMatrix mat;std::string par;
        MatPar(){}
        MatPar(const UseMatrix & Mat,const std::string Par):mat(Mat),par(Par){}
    };
    /// @endcond

    static struct std::vector<std::map<std::string,UseMatrix> >tableInts; //!< table of integrals with basis functions

    /// pointer to diagonal matrix of integrals  \f$ \langle i | func[param] \rangle \f$
    static void getInts(std::string Name, std::string Pars, const BasisSet & iBas, UseMatrix & mat);

//   // OBSOLETE, superseeded by multi-dimnsional version
//    static void get(const std::vector<std::string>Oper, const BasisSet & IBas, const BasisSet &JBas, UseMatrix & mat, const std::vector<std::string> Par);

    /// \brief pointer to indexed matrix \f$ \langle i | \Pi_\lambda Op(String[\lambda])^{(i1,\ldots;j1,\ldots)} | j \rangle \f$ for non-tensor product operators
    /// \param Oper operator name string
    /// \param iBas left hand basis set(s)
    /// \param jBas right hand basis set(s)
    /// \param mat  matrix
    /// \param Par  parameters
    /// \param iSub left hand subindex
    /// \param jSub right hand subindex
    static void get(const std::vector<std::string> Oper,
                    const std::vector<const BasisSet*> IBas, const std::vector<const BasisSet*> JBas, UseMatrix &mat,
                    const std::vector<std::string> &Par,
                    const std::vector<unsigned int> &ISub=std::vector<unsigned int>(0),
                    const std::vector<unsigned int> &JSub=std::vector<unsigned int>(0)
            );

    ///
    /// \brief getMultip transformation matrices and diagonal for a multiplication operator
    /// \param Oper format <{}><{}>...<functionName{ax0,ax1,...,axN}> (presently only for N=2
    /// \param IBas left hand basis
    /// \param JBas right hand basis
    /// \param mat  mat[0]...values on quadrature grid, mat[1]...work storage, mat[2+n]...transformation to grid on nth axis
    /// \param Par  (optional) parameters to transmit to function
    static void getMultip(const std::string Oper, std::vector<const BasisSet*> IBas, std::vector<const BasisSet*> JBas, std::vector<UseMatrix*> &mat,
                    const std::string &Par);

    /// pointer to matrix  \f$ \langle i | Op(Kind) | j \rangle \f$ (OBSOLESCENT)
    static void get(std::string StrKind, const BasisSet & iBas, const BasisSet &jBas, UseMatrix & mat, std::string Par="");
    static void mapSet(const BasisSet & iBas, const BasisSet &jBas, UseMatrix & mat); ///< map between "BasisSet"s
    static basisMatFunc* getFunction(std::string Definition); ///< check Definition and return pointer, if valid
    static void Test(bool Print);    ///< compute the harmonic oscillator for testing

private:
    static std::string hashIndex(const std::vector<int>&IMult,const std::vector<int>&JMult);
    static std::string hashIndex(const std::vector<unsigned int>&IMult,const std::vector<unsigned int>&JMult);

    ///< @brief general algebraic string (see class Algebra)
    class basisMatAlgebra:public basisMatFunc {
        const Algebra* a;
    public:
        basisMatAlgebra(const Algebra * A):basisMatFunc(A->definition),a(A)
        {if(not a->isAlgebra())ABORT("malformed Algebra definition "+Algebra::failures);}
        std::complex<double> operator()(std::complex<double> Z) const {return a->val(Z);}
        std::complex<double> operator()(std::vector<std::complex<double> > Z) const
        {needSize(Z,1);return operator()(Z[0]);}
    };

    ///< @brief \f$ q^2 \f$ instantiation of basisMatFunc
    class basisMatQ2:public basisMatFunc {
    public:
        basisMatQ2():basisMatFunc("qJq"){}
        std::complex<double> operator()(std::complex<double> Z) const {return Z*Z;}
        std::complex<double> operator()(std::vector<std::complex<double> > Z) const {
            std::complex<double> Q2=Z[0]*Z[0];
            for(unsigned int k=1;k<Z.size();k++)Q2+=Z[k]*Z[k];
            return Q2;
        }
    };
    ///< @brief \f$ \cos(q) \f$ instantiation of basisMatFunc
    class basisMatCos:public basisMatFunc {
    public:
        basisMatCos():basisMatFunc("cos"){}
        std::complex<double> operator()(std::complex<double> Z) const {return cos(Z);}
    };
    ///< @brief \f$ \sin(q) \f$ instantiation of basisMatFunc
    class basisMatSin:public basisMatFunc {
    public:
        basisMatSin():basisMatFunc("sin"){}
        std::complex<double> operator()(std::complex<double> Z) const {return sin(Z);}
    };
    ///< @brief \f$ \cos(q) \f$ instantiation of basisMatFunc
    class basisMatSqrt1mQ2:public basisMatFunc {
    public:
        basisMatSqrt1mQ2():basisMatFunc("sqrt(1mq2)"){}
        std::complex<double> operator()(std::complex<double> Z) const {return sqrt(1.-Z*Z);}
    };
    ///< @brief \f$ \cos(q) \f$ instantiation of basisMatFunc
    class basisMatQDivideSqrt1mQ2:public basisMatFunc {
    public:
        basisMatQDivideSqrt1mQ2():basisMatFunc("Jq/sqrt(1mq2)"){}
        std::complex<double> operator()(std::complex<double> Z) const {return Z/sqrt(1.-Z*Z);}
    };
    ///< @brief \f$ \cos(q) \f$ instantiation of basisMatFunc
    class basisMatQSqrt1mQ2:public basisMatFunc {
    public:
        basisMatQSqrt1mQ2():basisMatFunc("Qsqrt(1mq2)"){}
        std::complex<double> operator()(std::complex<double> Z) const {return Z*sqrt(1.-Z*Z);}
    };
    ///< @brief \f$ \cos(q) \f$ instantiation of basisMatFunc
    class basisMatDivideSqrt1mQ2:public basisMatFunc {
    public:
        basisMatDivideSqrt1mQ2():basisMatFunc("J/sqrt(1mq2)"){}
        std::complex<double> operator()(std::complex<double> Z) const {return 1./sqrt(1.-Z*Z);}
    };
    ///< @brief \f$ \cos(q) \f$ instantiation of basisMatFunc
    class basisMat1mQ2:public basisMatFunc {
    public:
        basisMat1mQ2():basisMatFunc("1mq2|J1mq2"){}
        std::complex<double> operator()(std::complex<double> Z) const {return 1.-Z*Z;}
    };
    class basisMatDivide1mQ2:public basisMatFunc {
    public:
        basisMatDivide1mQ2():basisMatFunc("J/1mq2"){}
        std::complex<double> operator()(std::complex<double> Z) const {return 1./(1.-Z*Z);}
    };
    ///< @brief \f$ q^{-1} \f$ instantiation of basisMatFunc
    class basisMatDivide:public basisMatFunc {
    public:
        basisMatDivide():basisMatFunc("J/q"){}
        std::complex<double> operator()(std::complex<double> Z) const {return 1./Z;}
    };
    ///< @brief \f$ q \f$ instantiation of basisMatFunc
    class basisMatValue:public basisMatFunc {
    public:
        basisMatValue():basisMatFunc("Q|Jq"){}
        std::complex<double> operator()(std::complex<double> Z) const {return Z;}
    };
    ///< @brief \f$ q^{-2} \f$ instantiation of basisMatFunc
    class basisMatDivide2:public basisMatFunc {
    public:
        basisMatDivide2():basisMatFunc("J/q2"){}
        std::complex<double> operator()(std::complex<double> Z) const {return 1./(Z*Z);}
    };
    ///< @brief characteristic function of some interval of basisMatFunc
    class basisMatOne:public basisMatFunc {
        double qmin,qmax;
    public:
        basisMatOne():basisMatFunc("One|J"),qmin(-DBL_MAX),qmax(DBL_MAX){}
        std::complex<double> operator()(std::complex<double> Z) const {if(Z.real()<qmin or Z.real()>qmax)return 0.; return 1.;}
        basisMatFunc & set(const std::string Pars){
            if(Pars.find(',')==std::string::npos){
                qmin=-DBL_MAX;
                qmax= DBL_MAX;
                return *this;
            } else {
                std::vector<std::string> limits(tools::splitString(Pars,','));
                if(limits.size()!=2)ABORT("specify lower and upper limit in One["+Pars+"]");
                qmin=tools::string_to_double(limits[0]);
                qmax=tools::string_to_double(limits[1]);
                return *this;
            }
        } ///< may update parameter
    };

    ///< @brief square of the difference
    class basisMatRsq:public basisMatFunc {
    public:
        basisMatRsq():basisMatFunc("Rsq"){}
        std::complex<double> operator()(std::vector<std::complex<double> >X) const {needSize(X,2); return abs(std::pow(X[0]-X[1],2.));}
    };

    ///< @brief for debugging only (asymmmetric HO)
    class basisMatHarm2:public basisMatFunc {
    public:
        basisMatHarm2():basisMatFunc("Harm2"){}
        std::complex<double> operator()(std::vector<std::complex<double> >X) const {needSize(X,2); return X[0]*X[0]*50.+X[1]*X[1]*0.5;}
    };

    static bool funcDefaultsSet;
    static void funcDefaults();
    static std::string stripDeriv_d(const std::string Oper); ///< strip function of d_ and _d

public:
    /// @cond DEV
    class Ints:public Integrate::Tools
    {
        friend class BasisMat;
        friend class Tools;
        std::vector<const BasisSet*> iBas,jBas;
        bool iDer,jDer; // true: use derivates instead of values
        unsigned int dim;
        std::vector<double> xvals; // complex scaled arguments
    public:
        static std::vector<basisMatFunc*> currentFunc;
        static std::vector<const BasisSet*> currentBas;
        static UseMatrix func(const std::vector<double> &Q);
        Ints(){}
        Ints(const std::vector<std::string> Oper, const std::vector<const BasisSet*> &IBas, const std::vector<const BasisSet*> &JBas,
             double AccRel=1.e-12, double AccAbs=1.e-12,
             std::vector<std::vector<unsigned int> > NQuad=std::vector<std::vector<unsigned int> >(0),
             const std::string Kind="GaussLegendre",
             const std::string KindInf="GaussLaguerre");

        UseMatrix nDim(const std::vector<std::vector<double> > Vol,
                       const std::function<UseMatrix (std::vector<double>)> Func,
                       const std::vector<double> Params=std::vector<double>(0));
        UseMatrix recursive(const std::vector<std::vector<double> > Vol,
                         const std::function<UseMatrix(const std::vector<double> &)> Func,
                         const std::vector<double>Params=std::vector<double>(0)
                ) {return Integrate::Recursive<UseMatrix,double,Ints>(*this,Vol,Func,Params);}
    };
    /// @endcond
};

/// add a function to the BasisMatFuncs table and check consistency
template<class T>
void basisMatFuncSet(T* Func){
    std::vector<std::string> akaNames=tools::splitString(Func->name,'|');
    for(unsigned int k=0;k<akaNames.size();k++){
        if(not tools::hasKey(BasisMat::BasisMatFuncs,akaNames[k]))BasisMat::BasisMatFuncs[akaNames[k]]=Func;
        else{
            basisMatFunc* inTable=BasisMat::BasisMatFuncs[akaNames[k]];
            if(inTable->name!=Func->name)return;
            /// note: dynamic_cast may require compiler option on some compilers
            const T * o=dynamic_cast<const T*>(inTable); // need cast, as Other is basisMatFunc
            if(o==0 or not o->sameParameters(*Func))ABORT("inconsistent multiple basisMatFunc="+inTable->name); // cast failed, i.e. classes differ
        }
    }
}
/** @} */ // end group OperatorData


#endif // BASISMAT_H
