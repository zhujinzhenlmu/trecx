// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef SELECTIONRULE_H
#define SELECTIONRULE_H

#include <string>
#include <map>
#include <vector>

class Axis;

class Discretization;
class SelectionRule
{
    static std::map<std::string,std::string> operList;
public:
    // attach selection rule to index of discretization
    void add(const std::vector<Axis> & Ax, std::string Rule);
};

#endif // SELECTIONRULE_H
