// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASPROD_H
#define BASPROD_H

#include "basisNdim.h"
#include "coordinateTrans.h"

/** \ingroup Basissets */
/// tree product basis evaluated at quadrature grid for reference BasisNdim
///\n
class BasisProd : public BasisNdim
{
    const BasisNdim * ref;
    static std::map<const Index*,BasisProd*> all;

    // tensor product Prod <- Prod (x) Factor[0] (x) ... (x) Factor.back()
    std::vector<std::complex<double> > tensorMultiply(const std::vector<std::complex<double> > & LFac, const std::vector<std::complex<double> > &RFac) const;
    void valDerAll(const Index * Idx, std::vector<double> Point, std::vector<std::vector<std::complex<double> > > &ValDer) const;

    // it becomes obvious that these function pointers should be in BasisNdim and matching actual funcionts are not needed
    // this is all strictly coordinate dependent and not much dependent on basis
    CoordinateMap _fromNdim,_toNdim;/// Ndim may be Phi.Eta.R, Phi.Rho (to be extended)
    JacobianMap _jacToNdim; ///< Ndim may be Phi.Eta.R, Phi.Rho (to be extended)
    IntegrationFactor _intFactor;
    NablaFactor _nabFactor;

protected:
    std::vector<double> toCartesian(const std::vector<double>&CoorNdim) const {return _fromNdim(CoorNdim);}
    double absFactor(const std::vector<double> & CoorNdim) const{return _intFactor(CoorNdim);}
    double nablaFactor(const std::vector<double>&CoorNdim,int I) const{return _nabFactor(CoorNdim,I);}
    Eigen::MatrixXd jacobianToNdim(const std::vector<double>&CoorNdim) const;

public:
    BasisProd(const Index *RootIdx, const BasisNdim *Ref);
    BasisProd(const Index *RootIdx, unsigned int MinQuad=10);
    static const BasisProd* factory(const Index* Root, const Index * Ref); ///< use quadrature grid of reference
};

#endif // BASPROD_H
