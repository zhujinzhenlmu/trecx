// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef PLOTSPECTRAL_H
#define PLOTSPECTRAL_H

#include <vector>
#include <string>
#include "plotCoefficients.h"

class DiscretizationSpectral;
class Coefficients;
class OperatorDiagonal;

/// \ingroup Plot
/// \brief plot solution wrt to the spectral representation of a given operator
class PlotSpectral : public PlotCoefficients
{
    const DiscretizationSpectral * spec;
    Coefficients* tmp;
    void intoCols(OperatorDiagonal * Eigen, Coefficients * C, std::vector<std::vector<double> > & Cols) const;
public:
    ~PlotSpectral();
    PlotSpectral(const DiscretizationSpectral *Spec);
    /// transform to plot and write to file
    void plot(const Coefficients & C, const std::string & File,
              const std::vector<std::string> & Head=std::vector<std::string>(0),std::string Tag="",bool OverWrite=false) const;
    /// brief name of output file type
    std::string briefName() const {return "sp";}
};

#endif // PLOTSPECTRAL_H
