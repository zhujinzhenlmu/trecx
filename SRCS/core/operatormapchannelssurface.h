// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORMAPCHANNELSSURFACE_H
#define OPERATORMAPCHANNELSSURFACE_H

#include "operatorAbstract.h"
#include "odeRK4.h"

class Coefficients;
class Wavefunction;
class Discretization;
class DiscretizationHybrid;
class DiscretizationFactor;
class DiscretizationSpectral;
class DiscretizationSurface;
class DiscretizationGrid;
class TimePropagator;
class RungeKutta4;
class ReadInput;
class Operator;
class OperatorGradient;
class UseMatrix;
class CoefficientsLocal;

/** \ingroup Structures */
/// map to surface values and derivatives for multiple channels
class OperatorMapChannelsSurface : public OperatorAbstract
{
    const Discretization* _mainDisc;
    const DiscretizationHybrid* hyb;
    DiscretizationSpectral* spec;
    DiscretizationFactor *ion, *ionCh, *freeCh;
    DiscretizationSurface* freeChSurf;
    OperatorGradient * grad;
    Wavefunction *ionWf, *freeWf, *iniState;
    double chanEmax; // maximum ionic channel energy

    TimePropagator* prop;
    OdeStep<LinSpaceMap<CoefficientsLocal>,CoefficientsLocal>*odeLoc;
    OperatorAbstract* chanOp;

    // Initialize to states between Emin and Emax
    void initializeIonWfs(ReadInput &In);
    double applyThreshold;
    void setProp() const; // set up propagators before first apply

    static void read(ReadInput & In, std::string &ChanOp, std::string &ChanInt,std::string &IonAxes,std::string &Shift, double &Emin, double &Emax);

public:
    static std::string axis(ReadInput & In);
    OperatorMapChannelsSurface(ReadInput& In, const Discretization *Disc);
    void setup(ReadInput & In);
    ~OperatorMapChannelsSurface();

    double energyShift() const;
    void update(double Time, const Coefficients* CurrentVec){time=Time;}
    void apply(std::complex<double> A, const Coefficients &X, std::complex<double> B, Coefficients &Y) const;
    void currentUntwistingMatrix(UseMatrix& res);
    DiscretizationSurface* pointerToChannelSurface();
    void printInfo() const;
};

#endif // OPERATORMAPCHANNELSSURFACE_H
