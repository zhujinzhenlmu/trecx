#ifndef BASISMATNUMBERS_H
#define BASISMATNUMBERS_H

#include "basisMatMatrix.h"

class BasisMatNumbers : public BasisMatMatrix
{
public:
    BasisMatNumbers(const Eigen::MatrixXcd Mat);
    BasisMatNumbers(ReadInput & Inp, int &Line);
};

#endif // BASISMATNUMBERS_H
