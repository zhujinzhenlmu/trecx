#ifndef SpectrumAngle_H
#define SpectrumAngle_H

#include <vector>

#include "tree.h"
#include "readInput.h"
using namespace std;
class Coefficients;
class CoefficientsPermute;
class Index;
class BasisGrid;
class Plot;
class SpectrumAngle {
    unsigned int Nk, _version, _thetaNum, _startKIdx, _phiNum;
    double _EAverage;
    std::string _specFile, _plotWhat;
    std::vector<std::string> _head;
    // interp[z][k] - interpolate in Eta for given kZ and kR
    std::vector<double> kGrids, etaGrid;
    Coefficients *_ampl;
    Discretization *DPlot;
    Plot *_plotJAD;
    ReadInput Inp;
public:
    SpectrumAngle(const Coefficients amplitutes, ReadInput inpc, string plotWhat, std::vector<string> head, Discretization *D = 0);
    std::vector<std::vector<double>> getJADPlot(unsigned int k1Idx, unsigned int k2Idx, bool toIntegrate = false);
    void plotByData(std::vector<std::vector<double>> result, std::string fileName, int blockRows);
    void JAD();
    void intK3D();
    void intK();
    void antiCorrYield();
    void run();
    static void getKIdx(double E1, double E2, unsigned int &k1Idx, unsigned int &k2Idx, std::vector<double> KGrids);
    static Discretization *getJADDisc(ReadInput dataInp);
    static void initialize(ReadInput &dataInp);
};

#endif // SpectrumAngle_H
