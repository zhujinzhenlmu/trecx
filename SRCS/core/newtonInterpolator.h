// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef NEWTONINTERPOLATOR_H
#define NEWTONINTERPOLATOR_H
#include "toolsHeader.h"

class NewtonInterpolator {
  public:
    NewtonInterpolator(std::vector<std::complex<double> >& coordinates, std::vector<std::complex<double> >& fctValues);
    std::vector<std::complex<double> >& getVals(std::vector<std::complex<double> >& coordinates, std::vector<std::complex<double> >& fctValues) const;
  protected:
    std::vector<std::complex<double> > coeffs;
    std::vector<std::complex<double> > supports;
};

class Wavefunction;
class Discretization;
class Index;

class NewtonInterpolatorWF {
    // dividierte differenzen schema
    // knownWfs->time represent the support points
    // knownWfs->coefs represent the known function values at the support points
    // the interpolation at a given time returns a wavefunction
public:
    NewtonInterpolatorWF(const Index* Idx, int numberOfSupportPoints); // allocates some memory
    NewtonInterpolatorWF(const Discretization* disc, int numberOfSupportPoints); // allocates some memory
    ~NewtonInterpolatorWF();

    void getInterpolatedWF(double time, Wavefunction* result); // computePolynomialCoefficients needs to be called first ! computes the interpolated wavefunction
    void timeDerivative(double time, Wavefunction* result); // computePolynomialCoefficients needs to be called first ! computes the interpolated wavefunction

    void computePolynomialCoefficients(const std::vector<Wavefunction*> & knownWfs); // for given support points compute the expansion coefficients of the unique polynomial in newton basis
    bool inInterval(double Time);

    std::vector<Wavefunction*> polynomialCoefficients; // the expansion coefficients of the polynomial
    std::vector<Wavefunction*> divDiffs; // helper variable for computePolynomialCoefficients
    std::vector<double> times; // the supportpoints
};

#endif // NEWTONINTERPOLATOR_H
