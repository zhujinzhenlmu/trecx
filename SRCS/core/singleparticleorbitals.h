// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef SINGLEPARTICLEORBITALS_H
#define SINGLEPARTICLEORBITALS_H

#include "My4dArray.h"

class singleparticleorbitals {

public:
  singleparticleorbitals();
  virtual void matrix_1e(Eigen::MatrixXcd &result,std::string op) = 0;
  virtual void matrix_2e(My4dArray &result) = 0;
  virtual int NumberOfOrbitals() = 0;
};

#endif // SINGLEPARTICLEORBITALS_H
