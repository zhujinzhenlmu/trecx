#ifndef OPERATOR_ZTIMESID_H
#define OPERATOR_ZTIMESID_H


#include <string>
#include <vector>
#include <complex>

class Index;

#include "operatorFloor.h"

/** \ingroup OperatorFloors */
/// complex diagonal matrix
class OperatorZtimesId: public OperatorFloor
{
public:
    OperatorZtimesId():OperatorFloor(0,0,"ZtimesId"){}
    OperatorZtimesId(std::complex<double> Coeff, int Rows, std::string Kind);
    void axpy(const std::complex<double>& Alfa, const std::complex<double>* X, unsigned int SizX,
              const std::complex<double>& Beta, std::complex<double>* Y, unsigned int SizY) const;

    void axpyTranspose(const std::complex<double>& Alfa, const std::complex<double>* X, unsigned int SizX,
                       const std::complex<double>& Beta, std::complex<double>* Y, unsigned int SizY) const
    {axpy(Alfa,X,SizX,Beta,Y,SizY);}

    void pack(std::vector<int> &Info, std::vector<std::complex<double> >&Buf) const;

    virtual long applyCount() const;
};






#endif
