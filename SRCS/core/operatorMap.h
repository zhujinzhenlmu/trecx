// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply.
//
// See terms of use in the LICENSE file included with the source distribution
//
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#ifndef OPERATORMAP_H
#define OPERATORMAP_H


#define EIGEN_MATRIX_PLUGIN "EigenAddonMatrix.h"
#include "qtEigenDense.h"
#include "operatorAbstract.h"
#include "tree.h"
#include <memory>

class Index;
class BasisAbstract;
class CoefficientsViewDeep;

/** \ingroup Structures */
/// map between hierarchies of equal structure
class OperatorMap : public OperatorAbstract,public Tree<OperatorMap>
{
    // for creating contigous and deep views, as needed
    CoefficientsViewDeep *_deepX,*_deepY;
    Coefficients *_highX,*_highY,*_viewX,*_viewY;
    Index *_iX,*_iY;

    Coefficients * _tmp;
    const Eigen::MatrixXcd * _tensor;
    int _iVec,_jVec;

    class Storage{
        std::vector<std::unique_ptr<Eigen::MatrixXcd>> _matrices;
    public:
        static Storage main;
        const Eigen::MatrixXcd* get(Eigen::MatrixXcd&& from);
    };

    Eigen::MatrixXcd basisMap(const BasisAbstract *IBas, const BasisAbstract *JBas, int JDerivative=0);
    void axpy(std::complex<double> A, const Coefficients *X, Coefficients *Y) const;
    OperatorMap(const Index* IIndex, const Index* JIndex, std::string Derivative, const std::complex<double> Multiplier, bool EntryLevel);
    bool isIdentity(double Eps=0.) const;
public:
    ~OperatorMap();
    OperatorMap(const Index* IIndex, const Index* JIndex, std::string Derivative="")
        :OperatorMap(IIndex,JIndex,Derivative,1.,true){}
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
    std::string strData(int Level) const;
    std::string str(int Level=0) const{return Tree<OperatorMap>::str(Level);}
};

#endif // OPERATORMAP_H
