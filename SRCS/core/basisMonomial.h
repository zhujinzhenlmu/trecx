#ifndef BASISMONOMIAL_H
#define BASISMONOMIAL_H

#include "basisIntegrable.h"
#include <vector>

///@brief monomial basis for tests
class BasisMonomial:public BasisIntegrable{
    int _order;
public:
    BasisMonomial(int Order, double Low, double Up):BasisIntegrable(Low,Up),_order(Order){}
    unsigned int size() const{return _order;}
    unsigned int order() const{return _order;}
    void quadRule(int N, std::vector<double> & QuadX, std::vector<double> & QuadW) const{
        OrthogonalLegendre leg;
        std::vector<double> xx,ww;
        leg.quadrature(N,xx,ww);
        QuadX.clear();
        QuadW.clear();
        for(int k=0;k<N;k++){
            QuadX.push_back(lowBound()+(0.5*(1.+xx[k]))*(upBound()-lowBound()));
            QuadW.push_back(ww[k]*0.5*(upBound()-lowBound()));
        }
    }
    void valDer(const std::vector<std::complex<double> > & X,
                std::vector<std::complex<double> > & Val,
                std::vector<std::complex<double> > & Der, bool ZeroOutside=false) const{
        Val.assign(X.size(),1.);
        Der.assign(X.size(),0.);
        for(int k=1;k<size();k++){
            for(int i=0;i<X.size();i++){
                Val.push_back(Val[X.size()*(k-1)+i]*X[i]);
                Der.push_back(Der[X.size()*(k-1)+i]*X[i]+Val[size()*(k-1)+i]);
            }
        }
    }
};

#endif // BASISMONOMIAL_H
