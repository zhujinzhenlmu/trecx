// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef EIGEN_SOLVER_ABSTRACT_H
#define EIGEN_SOLVER_ABSTRACT_H

#include <vector>
#include <complex>
#include <string>

#include "operatorAbstract.h"
#include "index.h"
#include "abort.h"
#include "str.h"


class Coefficients;
class ReadInput;

/// \ingroup Linalg
/// \brief Abstract base class for eigensolver on OperatorAbstract
///
/// Solves the generalized linear eigenproblem (possibly on a subspace)
/// <br>Op vec = S vec eval
/// <br> S is invertible on a subspace
/// <br> Op is an OperatorAbstract and S is provided through the Operator's Index
///
/// the class holds configuration info, which can be altered by various setters <br>
/// construct / call compute() / retrieve results such as eigenvalues

class EigenSolverAbstract {
protected:


    bool _serial; ///< do not use MPI parallelism

    const OperatorAbstract* _op;
    const OperatorAbstract * _ovr;

    std::vector<std::complex<double> > _eigenvalues;
    std::vector<Coefficients* > _rightVectors;
    std::vector<Coefficients* > _leftVectors;
    std::vector<Coefficients* > _dualVectors;

    bool _computeLeftVectors;
    bool _computeRightVectors;

    unsigned int _numberEigenv;
    std::string _sort;  ///< how to sort: SmallReal, SmallAbs, LargeReal, LargeAbs etc. see ??? for all options

    double _eMin;       ///< provide only eigenvalues with Eval.real()>= _eMin
    double _eMax;       ///< provide only eigenvalues with Eval.real()<= _eMax
    bool _excludeRange; ///< revert the above, i.e. provide only eigenvalues outside the range


    virtual void _compute()=0; ///< solve the raw eigenproblem (w/o sorting, orthonormalization, etc.)

    /// determine a reasonable eigensolver method
    std::string autoMethod(const std::string Method);

public:

    void postSelect();

    static void readControls(ReadInput & Inp);
    static std::string method,defaultMethod;
    static void setMethod(std::string Method); ///< set method (Lapack, Arpack...) for eigensolver
    static void resetMethod(){method=defaultMethod;}///< reset method to defaultMethod

    EigenSolverAbstract(const OperatorAbstract* Op=0);

    void computeLeftVectors(bool Compute){ _computeLeftVectors=Compute; }
    void computeRightVectors(bool Compute){ _computeRightVectors=Compute; }
    void numberEigenv(unsigned int Number){ 
        if(_op != 0 && _op->iIndex->sizeStored()<Number)
            ABORT(Str("Can't compute ")+Number+" eigenvalues in a size="+_op->iIndex->sizeStored()+" matrix");
        _numberEigenv=Number; 
    }

    void selectEigenvalues(
            double EMin /** Eval.real()>= EMin*/,
            double EMax /** Eval.real()<= EMax*/,
            bool Exclude=false /**  revert, provide only eigenvalues outside [Emin,Emax]*/){
        _excludeRange=Exclude;
        _eMin=EMin;
        _eMax=EMax;
    }

    /// Yet to be implemented in a useful way
    static EigenSolverAbstract* factory(OperatorAbstract* _op);

    virtual std::vector<std::complex<double> > eigenvalues() const{ return _eigenvalues; }
    virtual std::vector<Coefficients* > leftVectors() const{ return _leftVectors; } ///< left hand eigenvectors: (left_i)^* A = e_i (left_i)^* S
    virtual std::vector<Coefficients* > _vectors() const { return _rightVectors; } ///< right hand eigenvectors Op * right_j = S * right_j eval_j

//    EigenSolverAbstract& compute(); ///< solve the eigenproblem
    EigenSolverAbstract& compute(const OperatorAbstract * Op, const OperatorAbstract* Ovr=0); ///< solve the eigenproblem
    bool verify(double Epsilon=1.e-10) const; ///< verify the eigenvectors
    void normalize(); ///< fix phase and normalize to Rvec[i].adjoint() Ovr Rvec[i]=1, adjust dual
    void phaseFix();  ///< fix the phase to =1 for largest in magnitude coefficient
    void sort(std::string Kind);
    void select(std::string Kind); ///< select: for now only by lowest eigenvalues

    void parallel(bool Parallel){_serial=not Parallel;} ///< true: use parallelism on block-diagonal operator
    const OperatorAbstract* oper() const {return _op;}
    const OperatorAbstract* ovrl() const {return _ovr;}
};

#endif //EIGEN_SOLVER_ABSTRACT_H
