// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OVERLAPDVR_H
#define OVERLAPDVR_H

#include "coefficients.h"
#include "operatorTree.h"

class CoefficientsLocal;
class Index;
class OverlapDVR : public OperatorTree
{
    Coefficients _diagonal;
    Coefficients* diagonalLocal;
    OverlapDVR(Coefficients* Diagonal);
public:
    // note: here we need the Discretization explicitly as no Operator constructor is not optimal
    OverlapDVR(const Index *Idx);
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
    std::vector<std::complex<double> > diagonal() const;
    std::string strData(int Digits) const {return Sstr+"diagonal"+diagonal().size();}
};

#endif // OVERLAPDVR_H
