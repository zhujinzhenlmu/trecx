// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORVIEW_H
#define OPERATORVIEW_H

#include <complex>

#include <vector>
#include "tree.h"


class Operator;
class Coefficients;
class Index;


/** \ingroup Structures */
/// \brief block-matrix view to an operator
///
/// get and insert subsets of data
class OperatorView : public Tree<OperatorView>
{
    Operator * bl;
    std::complex<double> *data;

    std::string strData() const;
public:
    OperatorView():bl(0),data(0){}
    OperatorView(Operator &Op);

    void insertColumn(int J, const Coefficients & C);
    void getColumn(int J, Coefficients & C);
    void getRow(int I, Coefficients & C);

};

#endif // OPERATORVIEW_H
