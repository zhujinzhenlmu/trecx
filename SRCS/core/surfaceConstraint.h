#ifndef SURFACECONSTRAINT_H
#define SURFACECONSTRAINT_H

#include <vector>
#include <complex>

#include "operatorTree.h"
#include "operatorFloor.h"
#include "discretizationSurface.h"

class SurfaceFlux;

class SurfaceConstraint:public OperatorTree
{
    SurfaceFlux * _flux;
public:
    class Floor: public OperatorFloor{
    public://HACK
        const std::complex<double> * _derF;
        Eigen::MatrixXcd _c,_b;
    public:
        Floor(const SurfaceFlux *Flux, /*const OperatorMap* Map*/const DiscretizationSurface::Map *Map);
        void axpy(const std::complex<double> &Alfa, const std::complex<double> *X, unsigned int SizX, const std::complex<double> &Beta, std::complex<double> *Y, unsigned int SizY) const;
        void pack(std::vector<int> &Info, std::vector<std::complex<double> > &Buf) const {DEVABORT("Not implemented!");}
    };

public:
    SurfaceConstraint(SurfaceFlux * Flux, /*const OperatorMap* Map*/const DiscretizationSurface::Map *RcMap);
    void update(double Time, const Coefficients* CurrentVec=0);

};

#endif // SURFACECONSTRAINT_H
