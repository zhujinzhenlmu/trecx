// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef QUANTUMCHEMICALINPUT_H
#define QUANTUMCHEMICALINPUT_H

#include <fstream>
#include <iostream>
#include "abort.h"
#include "str.h"

#include "quantumChemicalData.h"

class columbus_data;
class mo;
class ReadInput;

/** @defgroup ChemStruc Chemical structure
 *  \brief imports quantum chemistry data into tRecX
 *  @{
*/

/// \ingroup ChemStruc
/// \brief (re-)read and hold all chemistry data for haCC (superseeds columbus_data)
class QuantumChemicalInput : public QuantumChemicalData
{
    void dimensionMatch(string Mess,int A,int B){if(A!=B)ABORT(Str(Mess+" - dimension mismatch:")+A+"vs"+B);}
    std::string _source; ///< present source of data
    std::string originalSource; ///< original source of data

    QuantumChemicalInput(ReadInput & Inp,std::string Dir);
public:
    static QuantumChemicalInput * neutrals;
    static QuantumChemicalInput * ions;
    static void read(ReadInput & Inp); ///< create ions and neutrals

    /// the default constructor - will only be used to generate portable data sets
    QuantumChemicalInput(bool Ion=true);

    std::string source() const {return _source;} ///< standard source
    void write(std::string Dir) const; ///< write complete class into files in Dir
    void read(std::string Dir); ///<  read complete class into files in Dir


public:
//    QuantumChemicalInput():QuantumChemicalData(),det_cutoff(0),det_threshold(0.),sceTolerance(0.){}

    columbus_data * coluData; // temporary, at present being destroyed after use in constructor

    // data provided by columbus_data read functions:
    int _multiplicity;
    int multiplicity(){return _multiplicity;} ///< ???not sure about the meaning
    std::vector<sod> dets(); ///< return detertminants in internal format

    // could be set apart as "primitive data"
    Eigen::VectorXd exponents;                     //gaussian exponent
    Eigen::Matrix<int, Eigen::Dynamic, 3> pow;     //powers
    Eigen::Matrix<double, Eigen::Dynamic, 3> coord;//center coordinates

    // data for MOs
    Eigen::MatrixXd moCoef;
    Eigen::MatrixXd saoCoef;
    Eigen::MatrixXd aoCoef;

    std::vector<Eigen::VectorXcd > ciCoef;
    std::vector<int>columbusDets; // determinants definition in COLUMBUS format

    unsigned int det_cutoff; // maximum number of determinants
    double det_threshold; // CI expansion coefficient threshold all determinants within the cutoff fulfill
//    double sceTolerance;  // tolerance for single-center-expansion of orbitals

    // the following will be generated from above data
    mo * molOrb;

};
/** @} */
#endif // QUANTUMCHEMICALINPUT_H
