// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef __OPERATOR__
#define __OPERATOR__

#include <deque>

class OperatorSingle;
class Discretization;
class DiscretizationHybrid;
class Afield;
//class IndexFloor;
class Coefficients;
class CoefficientsFloor;
class Index;
class OperatorSingle;
class OperatorFloor;
class OperatorTree;
class ReadInput;

#include "useMatrix.h"
#include "wavefunction.h"
#include "coefficients.h"

#include "abort.h"
#include "arpack.h"
#include "operatorAbstract.h"
#include "discretizationGrid.h"

namespace tSurffTools { class VolkovPhase; }

/** \ingroup Structures */
/// old style operator - still carries main load (OBSOLESCENT)
class Operator:public OperatorAbstract, public Labelled<Operator> {
    friend class OperatorTree;
    friend class OperatorView;
    friend class DerivativeFlat;

    friend class OperatorTensorId; //HACK
    friend class HaccInverse; //HACK
    friend class RotateBasis; //HACK
    friend class Parallel; //<HACK for now
    friend class Coefficients;
public:
    /// auxiliary class for tensor product for non-floor indices
    class Tensor{
//        friend class DiscretizationGrid::MapFrom;
        friend class Operator;
        friend class OperatorMap;
        std::complex<double>*factor; // point to factor of tensor operator
        UseMatrix mat;               // matrix: Y.C[a] += sum[k] factor*mat(a,k)*X.C[k]
        // alternate: sparse tensor storage
        Coefficients* Y;             // auxiliary storage
        const Index* I;

        ~Tensor();
        Tensor() : Y(0) {}
        Tensor(const Tensor & other);
        Tensor(std::string Definition, const Discretization * Idisc, const Discretization * Jdisc, const Index *IIndex, const Index * JIndex);
        Tensor(std::string Definition, const Index *IIndex, const Index *JIndex);
        void construct(std::string Definition, const Index *IIndex, const Index *JIndex);

        static bool keep(const Index *IIndex, const Index *JIndex, Tensor *&Tens, std::string Definition, UseMatrix &Mult);
        bool operator==(const Tensor &Other) const ;
    };
public:
    static bool useOperatorFloor;    //!< whether to use OperatorFloor where possible
    static bool fuseOp;    //!< =fuse operator into as few blocks as possible
    static bool useTensor; //!< =false: never use the tensor-product form
    static bool flat;      //!< =false: "flatten" the operator for time-propagation
    static std::string eigenMethod; //!< preferred eigen problem solver

    friend class DiscretizationSpectral;
    friend class ArpackBase;
    friend class tSurffTools::VolkovPhase;
    friend class Index; //HACK

    virtual ~Operator();
    Operator();
    Operator(const Operator &other);
    Operator(OperatorTree & OTree); ///< convert OperatorTree to Operator (destructive)
    Operator(const Index* IIndex, const Index* JIndex, OperatorSingle* Osing, std::string Name);
    Operator(bool OwnsData,std::complex<double>*TimeDepFac,OperatorFloor* OFloor, const Index* IIndex,const Index* JIndex,Tensor* Ttensor)
        :OperatorAbstract("Floor",IIndex,JIndex),ownsData(OwnsData),oFloor(OFloor),tensor(Ttensor),preTensor(0){}

    OperatorSingle * & oRef(unsigned int N){return o[N];}
    unsigned int oSize() const {return o.size();}

    // emulate a few template class Tree functions (until operator itself is converted to tree
    virtual Operator* child(unsigned int N) const {return const_cast<Operator*>(O[N]);} ///< emulates Tree::child
    virtual Operator*& childRef(unsigned int N) {return O[N];} ///< emulates Tree::child
    unsigned int childSize() const {return O.size();}///< emulates Tree::childSize()
    virtual void childAdd(Operator* Child){O.push_back(Child);O.back()->_parent=this;} ///< emulate Tree::childAdd();
    Operator * root() const {if(_parent==0)return const_cast<Operator*>(this);return _parent->root();} ///< emulates Tree::root
    Operator* nodeAt(const std::vector<unsigned int> & Idx) const; ///< emulates Tree::nodeAt

    /// standard operator constructor from Def
    Operator(const std::string & Name /** free format name string */,
             const std::string & Def  /** defintion string, see OperatorData for format */,
             const Discretization *IDisc    /** left side discretization */,
             const Discretization *JDisc    /** right side discretization */,
             const Index *IIndex=0          /** left side index, =0 -> replace width IDisc->Idx */,
             const Index *JIndex=0          /** right side index, =0 -> replace width JDisc->Idx */,
             std::complex<double> Multiplier=1. /** multiply operator by this number */,
             unsigned int Level=0 /** HACK until conversion to tree - indicate entry level */,
             std::complex<double>* TFac=0 /** time-dependent factor */
            );

    /// compose operator from lower level operators and Tensor on present level
    Operator(const std::string Name,const std::string TensorDef, Index* IIndex, Index* JIndex,std::vector<Operator*>Op,std::vector<OperatorSingle*>Os);

    Operator(const Index * I, const Index * J, std::string DerAxis=""); ///< create a map from index system J to I
    Operator(const Operator *Op, Index *Idx, Index *Jdx); ///< sub-block of Op

    void apply(std::complex<double> Alfa, const Coefficients & X, std::complex<double> Beta, Coefficients & Z) const; ///< Z = Alfa*Operator*X+Beta*Z
    void applyAdjoint(std::complex<double> Alfa, const Coefficients & X, std::complex<double> Beta, Coefficients & Z) const; ///< Z = Alfa*Adjoint(Operator)*X+Beta*Z;

    void update(double Time, const Coefficients* CurrentVec=0);
    void axpy(std::complex<double> Alfa, const Coefficients & X, std::complex<double> Beta, Coefficients & Z, double Time)
    {update(Time);apply(Alfa,X,Beta,Z);} ///< Y = Operator*X+Y;
    virtual void axpy(const Wavefunction & X, Wavefunction & Y, bool transpose = false) const; ///< Y = Operator*X+Y;
    virtual void axpy(const Coefficients & X, Coefficients & Y) const {apply(1.,X,1.,Y);} ///< (OBSOLESCENT) Y = Operator*X+Y;

    /// output to File all expectation values of Operator for all Vecs and all non-zero matrix elements of Ops
    void allMatrixElements(std::vector<const Coefficients*>Vecs, std::vector<const OperatorAbstract *> Ops, std::string File) const;

    /// add factor*(operator matrix) into global matrix (include continuity condition), if empty matrix, it will be created
    void matrix(UseMatrix & Mat) const; ///< build overall matrix from operator (no continuity imposed)
private:
    const Index* match(const Index* M, const Index * Parent); //HACK return child of Parent that matches M
public:
    virtual void matrixDirect(UseMatrix & Mat) const; ///< brute force, slow matrix calculation
    virtual void matrix(std::vector<std::vector<std::complex<double> > >& Mat, std::string Constraint="") const ; ///< intelligent, faster matrix calculation
    void show(std::string Kind) const; /// show operator with various levels of detail
    double norm() const; ///< norm of operator (WITHOUT the external factors)
    double nonZeroBlocks() const; ///< number of non-zero entries in the operator
    double nonZeros() const; ///< number of non-zero entries in the operator
//protected:
    double setNorm();
private:
    void opShow(int mpiId = 0) const;  /// prints the operator (matrix) onto screen
    void benchmark(unsigned int numberReps); ///< tests speed of operator application
    unsigned int operationsCount() const;    ///< return the operations count (not very accurate!)

public:
    /// list non-constant parameters in operator (in time-propagation, these have performance penalty)
    void checkVariable(std::string Message="") const;

    /// get eigenvalues
    void eigenValues(Operator & Overlap, std::vector<std::complex<double> > & Eval, unsigned int MaxN=INT_MAX,
                     double Emin=-DBL_MAX, double Emax=DBL_MAX, bool excludeEnergyRange = false);
    /// get eigenvalues and -vectors of operator
    void eigen(const OperatorAbstract &Overlap, std::vector<std::complex<double> > & Eval, std::vector<Coefficients *> &Evec,
               unsigned int MaxN=INT_MAX /**< get at least MaxN solutions (may be more for blocked systems) */,
               double Emin=-DBL_MAX, /**< lowest allowed eigenvlaue */
               double Emax=DBL_MAX, /**< highest allowed eigenvalue */
               bool excludeEnergyRange = false,
               bool Vecs=true,
               const std::string Sort="SmallReal",
               const std::string Constraints=""/** options: ZeroBoundaries=all or ZeroBoundaries=val1,val2,.. or FloorDiagonal */
            ) const;
    void eigenVerify(const OperatorAbstract &Ovr, std::vector<std::complex<double> > &Eval, std::vector<Coefficients*> &Evec,
                     unsigned int print=0, double eps=1.e-12, bool Pseudo=true) const; ///< verify eigenvectors

    bool diagonalConstraint(std::string Constraint) const; ///< true if constraint to diagonal elements on present level

    bool isBlockDiagonal() const; ///< true if blocked at the next lower index level
    bool isIdentity() const; ///< true if trivial 1 operator
    bool isLeaf() const {return O.size()==0;}

    static void readControls(ReadInput & Inp);
    static void setControls(std::string Key,std::string Value);

    /// extract diagonal of the operator into coefficients
    Coefficients diagonal(bool OnlyForDiagonalOperator = false) const;


    // === data =======================================================================
    std::string definition;

    const Index* getIindex() const {return iIndex;}
    const Index* getJindex() const {return jIndex;}

private:
    class Arp:public Arpack{
        friend class Operator;
        static std::complex<double> shift; // subtract shift*S from Hamiltonian to move desired eigenvalues to below 0
        void apply(const std::complex<double> *X, std::complex<double> *Y);
        Arp(const Operator* O);
        /// return eigenvectors
        void eigen(std::vector<std::complex<double> > &Eval, std::vector<Coefficients* > &Rvec,
                   unsigned int Nvec, const std::string &Which, bool Restart);
        // internal data
        const Operator* o;
        std::vector<std::complex<double> *> pX,pY;
        Coefficients x,y,tmp;
    };
    Operator & operator=(Operator & Other);
    const std::complex<double> * variableParameter() const;

protected:
    // === data ====================================
    bool ownsData;                   ///< if true, sub-operators will be deleted in destructor
//    std::complex<double>* timeDepFac;///< pointer to time-dependent factor, product will be multiplied on floor
public:
    std::vector<Operator*> O;        ///< O.size() indicates floor level O[0] gehoert zu index iIdx[0]
    const OperatorTree* hackTree;    ///< keep if copy of tree
protected:
    std::vector<OperatorSingle *> o; ///< floor level operator(s)
    OperatorFloor* oFloor;           ///< new (single) floor level operator
    const Operator* _parent;        ///< to conform with tree.h
public: //HACK
    Tensor * tensor;            ///< tensor factor (applied after operator below had been applied)
    Tensor * preTensor;         ///< tensor factor (applied before operator at lower levels)

    // === functions ====================================
    bool isZero(double Eps=1.e-12); ///< true if operator is zero
    /// add operator to matrix, create new if empty matrix
    void matrixAdd(std::complex<double> factor, UseMatrix & Mat) const;
protected:
    void matrix(UseMatrix & Mat, bool Continuous); ///< ELIMINATE(?), replace by addMatrix(?),  operator as complex matrix, Continuous==true: enforce continuity
    void floorDiagonalMatrix(UseMatrix & Mat, unsigned int I0) const; ///< diagonal floor block matrix of Operator
    Discretization *dataDisc(Discretization *Idisc,Discretization *Jdisc); ///< point to discretization that has the data (Idisc or Jdisc)

    virtual void construct(const Discretization *IDisc, const Discretization *JDisc, std::complex<double> Multiplier=1., std::complex<double> *TFac=0); // delegate constructor - feature only implemented in C++11

    /// if IDisc and JDisc are DiscretizationHybrid's, attach component operators and return true, else do nothing and return false
    bool constructHybrid(const std::string & Name,const std::string & Def,const Discretization *IDisc,const Discretization *JDisc);

    void static fuse(std::vector<Operator *> &Ops); ///< fuse blocks with identical indices
    void static split(std::vector<Operator *> &Ops); ///< fuse blocks with identical indices
    bool absorb(Operator *&Other); ///< absorb other into operator: if success, Other is deleted and true is returned
    void convert(); ///< convert OperatorSingle to OperatorFloor
    void purge(); ///< remove branches with neither OperatorSingle nor OperatorFloor
    void bcast(); ///< broadcast all OperatorFloor's

};



#endif

