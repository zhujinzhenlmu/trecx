// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef EIGENSUBSPACE_H
#define EIGENSUBSPACE_H

#include <complex>
#include <vector>
#include <coefficientsMulti.h>


class Operator;
class Discretization;
class DiscretizationSpectral;
class ReadInput;

class EigenSubspace
{
    double tolerance;

    const Operator* h;
    DiscretizationSpectral * specH0;

    std::vector<std::vector<std::complex<double> > > e0;
    CoefficientsMulti c0;

    std::vector<std::complex<double> > eGuess;  ///< guess for energies including interaction
    std::vector<std::complex<double> > eResolv; ///< energies to be removed from resolvent

    std::complex<double> eCenter() const; ///< average of eResolv
    bool converged(const UseMatrix &Smat);
    CoefficientsMulti & applyResolvent(CoefficientsMulti & C) const;
    /// Sort - sorting of Values by their smallest distance to any of the Centers
    void sortByDistance(const std::vector<std::complex<double> > &Centers, const UseMatrix & OTarget, const std::vector<std::complex<double> > &Values, std::vector<int> & Sort);
    /// prepare precondintioner for given ETarget energies, with E0 resolvent energies
    void setTarget(std::vector<std::complex<double> > E0, std::vector<std::complex<double> > &ETarget,  std::vector<Coefficients *> & Evec);
    /// run to convergence
    std::vector<std::complex<double> > iterate();
public:
    ~EigenSubspace(){}
    EigenSubspace(ReadInput & Inp);
    void setup(const Discretization *D, const Operator *H, const Operator *H0);
    void eigen(const std::vector<std::complex<double> > & E0, std::vector<std::complex<double> > &ETarget, std::vector<Coefficients*> & Evec);
    void write(const std::string & File);
    void print() const;
};




#endif // EIGENSUBSPACE_H
