// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef DISCRETIZATIONSPECTRAL_H
#define DISCRETIZATIONSPECTRAL_H

#include <memory>
#include <string>
#include "discretizationDerived.h"
#include "operatorTree.h"
#include "constrainedView.h"

class EigenSolver;
class OperatorDiagonal;
class ProjectSubspace;

/** \ingroup Discretizations */

/// eigenvectors of operator
class DiscretizationSpectral: public DiscretizationDerived{

    std::string constString;

    int selectNmin(const Index* Idx) const{return 0;}
    int selectNmax(const Index* Idx) const;

    void indexAndMaps(const EigenSolver * Slv, Index * &Idx, OperatorTree *&FromParent, OperatorTree *&ToParent);
    void addSpectralValues(const EigenSolver * Slv, Index* Idx , OperatorDiagonal * SpectralValues);
    OperatorTree * _mapConstructor(bool From, const Discretization *Parent, const Index* SIndex, const Index *EIndex,
                                   const std::vector<Coefficients*> & Evec);

    std::unique_ptr<ProjectSubspace> _project;

protected:
    std::string _selectionCriterion;
public:
    virtual ~DiscretizationSpectral();
    std::vector<std::shared_ptr<Coefficients>> eigenVectors;
    std::vector<std::complex<double>> eigenValues;
    DiscretizationSpectral(const Discretization *D, std::string Criterion);

    /// \brief construct spectral discretization from constrained view on an operator (OBSOLESCENT)
    DiscretizationSpectral(const Discretization *D,
                           const OperatorAbstract* Op,
                           double Emin=-DBL_MAX,
                           double Emax=DBL_MAX,
                           bool excludeEnergyRange = false
            );
    /// \brief spectral discretization for constrained view on an operator
    DiscretizationSpectral(const OperatorAbstract* Op,
                           double Emin=-DBL_MAX,
                           double Emax=DBL_MAX,
                           bool excludeEnergyRange = false
            );

    OperatorDiagonal * spectralValues;
    void check(const OperatorAbstract* Op) const;

    const OperatorAbstract* projector() const {return _project.get();}

};

#endif // DISCRETIZATIONSPECTRAL_H
