// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BASISMOLECULARORBITAL_H
#define BASISMOLECULARORBITAL_H

#include "basisNdim.h"

class mo;
class BasisSet;
class BasisMolecularOrbital : public BasisNdim
{
    const mo * _mo;
public:
    //HACK temporary
    const Index* _moIdx;
    const Index* _polarIdx;

    /// \brief molecular orbital basis in spherical shell
    ///
    /// the quadrature grid will be adjusted for accurate integrals
    /// for the radial basis and angular momenta up to Lmax,Mmax
    BasisMolecularOrbital(mo &Mo, const BasisSet & Radial, int Lmax, int Mmax);

    std::vector<std::vector<std::complex<double> > > valDer(std::vector<double> Point ) const{ABORT("undefined");}

};

#endif // BASISMOLECULARORBITAL_H
