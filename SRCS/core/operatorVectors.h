// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef OPERATORVECTORS_H
#define OPERATORVECTORS_H

#include <vector>
#include <string>

#include "operatorAbstract.h"
#include "coefficients.h"

class Coefficients;
class Index;
/** \ingroup Structures */
/// set of vectors considered as columns of an operator
class OperatorVectors : public OperatorAbstract
{
    std::vector<Coefficients> vecs;
public:
    OperatorVectors(std::string Name, const Index* IIndex, const Index* JIndex);
    void apply(std::complex<double> A, const Coefficients &Vec, std::complex<double> B, Coefficients &Y) const;
    void insertColumn(unsigned int J, const Coefficients & C);
};

#endif // OPERATORVECTORS_H
