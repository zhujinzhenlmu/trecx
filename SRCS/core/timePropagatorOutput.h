// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#ifndef TIMEPROPAGATOROUTPUT_H
#define TIMEPROPAGATOROUTPUT_H

#include <vector>
#include <complex>
#include <string>
#include <iostream>
#include <fstream>
#include "mpiWrapper.h"

/// derive a class from this for your own output
class Wavefunction;
class Coefficients;
class Operator;
class OperatorAbstract;
class DiscretizationDerived;
class Timer;
class PlotCoefficients;
class Discretization;
class OperatorMap;
class ChannelsSubregion;
class CoefficientsViewDeep;
class OperatorTree;
class CoefficientsLocal;
class DiscretizationSurface;
class TimePropagatorOutput{
    friend class TimePropagator;
private:
    std::vector<std::vector<std::complex<double> > > coefsT;
    void coefsFFT();
    std::ofstream* expecStream;
    int expecSample; // calculation of expectation values scales poorly, keep low unless needed otherwise
    unsigned int countPrint;
    unsigned int countWrite;
    std::string dir;
    const PlotCoefficients* wfPlot;
    static int obj_count;

    ChannelsSubregion* _channels;
    void flush() const; ///< flush all output streams
public:
    TimePropagatorOutput(){}
    TimePropagatorOutput(std::vector<OperatorAbstract *> Print, /**< Print expectation values of these operators */
            std::string ExpecFile="",     /**< File where to print to (""->cout) */
            double PrintInterval=0.,      /**< Print at these exact time-intervals (0=1/10 of total time) */
            std::string WriteDir="",      /**< Wave-function save directory (""=do not save) */
            const PlotCoefficients* WfPlot=0,               /**< Plot wave function */
            std::vector<const OperatorAbstract*> Write=std::vector<const OperatorAbstract*>(0), /**< apply these operators to wavefunction and save, filename=operator name */
            double WriteInterval=-1., /**< Minimal wave function save interval (default=PrintInterval, 0=every valid time step) */
            bool WriteAscii=false    /**< write also in ascii (at WriteInterval) */
            );
    void setInterval(double TBeg, double TEnd){tStart=TBeg;tEnd=TEnd;} ///< interval for progress monitoring
    double writeInterval; ///< minimal time to elaps between writes (default: = 0)
    double lastTimeWritten; ///< this time is actually on file
    double nextWriteTime(){return lastTimeWritten+writeInterval;} ///< write if time>= nextWriteTime and increment to nextWriteTime=time+writeInterval
    virtual void write(const Coefficients *C, double Time, bool Force=false); ///< save every converged valid wave function
    void close();

    double printInterval;  ///< print at these intervals (default: 10 times during total)
    virtual void print(const Coefficients *Wf, double Time, double TimeCPU); ///< default printout

public:
    std::vector<OperatorAbstract*> expecOp;
    std::vector<const OperatorAbstract*> writeOp;


    std::vector<Coefficients> temp;
    std::vector<std::ofstream *> streamBin,streamAsc;

    clock_t startingCPU;
    double tStart,tEnd;
    Timer *timer;
    //HACK - transform coefficients befor Fourier transforming
    DiscretizationDerived* discSpec;
    TimePropagatorOutput& withChannelsSubregion(ChannelsSubregion* Channels);
};


#endif // TIMEPROPAGATORTimePropagatorOutput_H
