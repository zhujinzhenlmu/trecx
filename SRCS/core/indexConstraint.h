#ifndef INDEX_CONSTRAINT_H
#define INDEX_CONSTRAINT_H

#include <functional>
#include <vector>
#include <map>
#include <string>
#include <memory>

#include "str.h"
class ReadInput;
class Index;

/**
 * Impose constraints, like l1 >= 5 => l2 < 5, l2 >= 5 => l1 < 5.
 *
 * Meant to replace IndexSelect.
 */
class IndexConstraint{

    struct Constraint{
        const std::vector<std::string> required_physicals;
        Constraint(std::vector<std::string> RequiredPhysicals): required_physicals(RequiredPhysicals) {}
        virtual bool includes(std::map<std::string, double>& Physicals) =0;
        virtual std::string str() const=0;
    };

    struct LmMConstraint: public Constraint{
        const int threshold;
        const int excluded_l;
        const std::string l_axis;
        const std::string m_axis;

        LmMConstraint(std::string LAxis, std::string MAxis, int Threshold, int Excluded_l):
            Constraint({LAxis, MAxis}),
            threshold(Threshold),
            excluded_l(Excluded_l),
            l_axis(LAxis),
            m_axis(MAxis){}

        bool includes(std::map<std::string, double>& Physicals) override{
            return (Physicals[l_axis] + Physicals[m_axis]) < threshold
                    or Physicals[l_axis] < excluded_l;
        }
        virtual std::string str() const {return Str("L-M<","")+threshold+" for L>"+excluded_l+" at "+l_axis+"."+m_axis;}
    };

    struct MZeroConstraint: public Constraint{
        const std::string m1_axis;
        const std::string m2_axis;

        MZeroConstraint(std::string M1Axis, std::string M2Axis):
            Constraint({ M1Axis, M2Axis }),
            m1_axis(M1Axis),
            m2_axis(M2Axis){}

        bool includes(std::map<std::string, double>& Physicals) override{
            return Physicals[m1_axis] + Physicals[m2_axis] == 0.;
        }
        virtual std::string str() const {return Str("M1+M2=0 at ","")+m2_axis+"."+m2_axis;}
    };

    struct LM1Constraint: public Constraint {
        const std::string m_axis;
        const std::string l_axis;
        const int lm1Lmax;
        LM1Constraint(std::string MAxis, std::string LAxis, int Lm1Lmax):
            Constraint({ MAxis, LAxis }), m_axis(MAxis), l_axis(LAxis), lm1Lmax(Lm1Lmax) {}

        bool includes(std::map<std::string, double> &Physicals) override {
            return Physicals[m_axis] == 0 or Physicals[l_axis] < lm1Lmax;
        }
        virtual std::string str() const {return Str("Lmax,for M!=0","")+lm1Lmax;}

    };
    struct LShapeConstraint: public Constraint{
        int threshold;
        int sumL;
        const std::string l1_axis;
        const std::string l2_axis;

        LShapeConstraint(std::string L1Axis, std::string L2Axis, int Threshold, int SumL):
            Constraint({ L1Axis, L2Axis }),
            threshold(Threshold),
            sumL(SumL),
            l1_axis(L1Axis),
            l2_axis(L2Axis){}

        bool includes(std::map<std::string, double>& Physicals) override{
            return (Physicals[l1_axis] < threshold)
                    or (Physicals[l2_axis] < threshold)
                    or (Physicals[l1_axis] + Physicals[l2_axis] < sumL);
        }
        virtual std::string str() const {return Str("l1,l2<","")+threshold+" for l1+l2>"+sumL+" at "+l1_axis+"."+l2_axis;}
    };

    std::vector<std::unique_ptr<Constraint>> _constraints;

public:
    static IndexConstraint main;
    static void readAll(ReadInput &Inp, std::string &Kind, std::string &Axes, int Line=1);
    void read(ReadInput& Inp);
    const std::vector<std::unique_ptr<Constraint>> & constraints() const {return _constraints;}
    bool includes(std::vector<const Index*> Path, std::vector<unsigned int> Pos) const;
    void apply(Index *Idx, std::vector<const Index*> Path, std::vector<unsigned int> Pos) const;
    std::string str() const;
};
#endif
