#ifndef BASISEIGEN_H
#define BASISEIGEN_H

#include "basisOrbital.h"
#include "basisSetDef.h"
class Index;

/** \ingroup Basissets */

///@brief BasisOrbital composed of eigenvalectors of a given operator
///
/// name has the format Eigenbasis[operator:reference], where
///
/// operator... convertible by OperatorDefinitionNew<br>
/// reference.. name of a refererence Index subset that has been set up before
///
/// Eigenvalues are sorted by increasing real part and the _nLow's eigenvector is the first orbital
class BasisEigen : public BasisOrbital
{
    std::string _operDef;
    std::string _refName;
    int _nLow;
    std::vector<std::complex<double> > _eigenValues;
public:
    BasisEigen(std::string OperRef, int Size, int NLow=0);
    BasisEigen(const BasisSetDef & Def);
    void generateOrbitals(const Index* Idx=0);
};

#endif // BASISEIGEN_H
