#ifndef POPULATION_H
#define POPULATION_H
// For dealing with the population projected to eigenstates, SI and DI, can be ingnored when merging
#include <string>
#include <vector>
#include <complex>

using namespace std;
class DiscretizationGrid;
class Coefficients;
class ReadInput;
class DerivativeFlat;
class TimePropagatorOutput;
class Wavefunction;
class Index;
/// corrections to the scattering amplitude
class Population {
    int populationN;
    int MConstrain;
    vector<int> Lshape;
    vector<vector<int> > statesToWrite;
    vector<ofstream *> writeStream;
public:
    Population(ReadInput &Inpc);
    void run(TimePropagatorOutput & out, DerivativeFlat *propDer);
    void read(vector<complex<double>> &Eval, vector<Coefficients *> &Evec, const Index *readIdx);
    void crop(Coefficients *coef);
};

#endif // SPECX_H
