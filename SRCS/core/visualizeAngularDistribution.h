#ifndef VISUALIZE_ANGULAR_DISTRIBUTION
#define VISUALIZE_ANGULAR_DISTRIBUTION

#include <map>
#include <string>

class Index;
class Coefficients;

class VisualizeAngularDistribution{
    std::map<const Index*, double> data;

    std::string x_axis;
    std::string y_axis;

public:
    VisualizeAngularDistribution(const Index* Root);
    VisualizeAngularDistribution(const Coefficients* Coeff);

    VisualizeAngularDistribution& withAxes(std::string XAxis, std::string YAxis){
        x_axis = XAxis;
        y_axis = YAxis;
        return *this;
    }

    void print();

    static void printAllPossible(const Index* Idx);
};


#endif
