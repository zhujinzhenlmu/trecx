#ifndef OPERATORNONLINPSI_H
#define OPERATORNONLINPSI_H

#include "operatorFloor.h"

class OperatorNonlinPsi : public OperatorFloor
{
protected:
    void axpy(const std::complex<double> &Alfa, const std::complex<double> *X, unsigned int SizX,
              const std::complex<double> &Beta, std::complex<double> *Y, unsigned int SizY) const;
public:
    OperatorNonlinPsi(std::string Def, const Index* IIndex, const Index* JIndex);
    void pack(std::vector<int> &Info, std::vector<std::complex<double> > &Buf) const{DEVABORT("not for parallel yet");}
};

#endif // OPERATORNONLINPSI_H
