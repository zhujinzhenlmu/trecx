// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "tools.h"

#include <iostream>
#include <fstream>
#include "histogram.h"

using namespace std;
using namespace tools;
using std::ofstream;
using std::ios_base;

int main(int argc, char* argv[]) {
    MPIwrapper::Init(argc,argv);
    Histogram::test(argc,argv);
}
