// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "tools.h"
//#include "algebra.h"
#include "timer.h"
#include "operator.h"
#include "readInput.h"
#include "printOutput.h"
#include "mpiWrapper.h"
#include "debugInfo.h"

#include "basisNdim.h"
#include "basicDisc.h"
#include "discretizationHybrid.h"
#include "discretizationSpectral.h"
#include "discretizationSpectralProduct.h"
#include "discretizationtsurffspectra.h"
#include "discretizationSurface.h"
#include "discretizationConstrained.h"

#include "pulse.h"
#include "harmonics.h"
#include "plot.h"
#include "plotSpectral.h"
#include "makePlot.h"
#include "scanEigenvalue.h"
#include "eigenSubspace.h"
#include "floquetAnalysis.h"
#include "inFlux.h"
#include "finDiff.h"
#include "operatorDefinition.h"
#include "operatorDefinitionNew.h"
#include "operatorFactor.h"
#include "operatorFloor3d.h"
#include "operatorFloorEE.h"
#include "parallelOperator.h"
#include "parallelLayout.h"

#include "timePropagator.h"
#include "timePropagatorOutput.h"

#include "tsurffSource.h"
#include "derivativeFlatInhomogeneous.h"
#include "derivativeLocal.h"
#include "operatormapchannelssurface.h"
#include "operatorGradient.h"

//#include "read_columbus_data.h"
#include "discretizationHaCC.h"
#include "mo.h"
#include "ci.h"

#include "spectralCut.h"
#include "odeRungeKutta.h"
#include "odeIntegral.h"
#include "tRecXchecks.h"
#include "channelsSubregion.h"

#include "densityOfStates.h"
#include "resolvent.h"

#include "initialState.h"

#include "coefficientsWriter.h"
#include "log.h"

#include "multiplyGrid.h"
#include "volkovGrid.h"
#include "basisMat.h"
#include "basisOrbital.h"

#include "toolsPrint.h"
#include "spectrumPlot.h"



//develop only
#include "surfaceFlux.h"
#include "evaluator.h"
#include "indexConstraint.h"
#include "multipolePotential.h"
#include "timeCritical.h"

using namespace std;
using namespace tools;

static unsigned int currentFile=0;       ///< will be set to the class static fileCount

TIMER(run,)
TIMER(setup,)
TIMER(operator,)
TIMER(matrix,)
TIMER(setupDisc,)
TIMER(setupPLot,)
TIMER(setupSpectral,)
TIMER(input,)
TIMER(setProp,)
TIMER(other,)

// auxiliary sections for main code
#include "tRecX.h"

bool isRerun(ReadInput & Inp) {

}


int main(int argc, char* argv[]) {
    //    Sout+"try this A"+boost::math::sph_bessel(1,0.27557017407092*20)+Sendl;

    MPIwrapper::Init(argc,argv);
    Threads::setup();

    LOG_ON();

    //============================================================================================
    // input and part of setup
    //==============================================================================================
    if(not MPIwrapper::isMaster())PrintOutput::off();

    ReadInput::openMain("",argc,argv);

    Units::setDefault("au");        // general default units
    ReadInput::main.setUnits("au"); // convert input to these units

    string col_path;
    ReadInput::main.read("Columbus","path",col_path,"None","Columbus run path");
    QuantumChemicalInput::read(ReadInput::main);

    ParallelLayout::read(ReadInput::main);

    //***************************************************************
    // for parallel debuging
    // compile with -O0
    // mpirun -np 4 ...
    // ps aux | grep tRecX
    // gdb tRecX procNumber
    // gdb> set DebugWait=0 to start each process
    if(ReadInput::main.flag("DEBUGparallel","block, see main_trecx.cpp how to proceed")){
        unsigned int DebugWait=1;
        while (DebugWait);
        MPIwrapper::Barrier();
    }
    //***************************************************************

    tRecX::readConstantsAndParameters();
    tRecX::readBases(ReadInput::main);

    OperatorDefinition::setup();

    // command line flags
    bool showMatrices=ReadInput::main.flag("showMatrices","show matrices and stop");
    bool showOperators=ReadInput::main.flag("showOperators","show operator structure and stop");
    bool printMatrices=ReadInput::main.flag("printMatrices","print matrices and stop");
    // read possible multi-dimensional bases

    double hpR,hpS,hpEta;
    string halfPlane="";
    ReadInput::main.read("_EXPERT_","halfPlaneR",hpR,"0","smoothing for half-plane suppression");
    ReadInput::main.read("_EXPERT_","halfPlaneS",hpS,"5","smoothing for half-plane suppression");
    ReadInput::main.read("_EXPERT_","halfPlaneEta",hpEta,"0.2","smoothing for half-plane suppression");
    if(hpR>0.){
        halfPlane="<1><1-trunc[0,"+tools::str(hpEta)+"]><1-trunc["+tools::str(hpR-hpS)+","+tools::str(hpR)+"]>";
    }

    OperatorFactor::readMatrix(ReadInput::main);
    ChannelsSubregion::read(ReadInput::main);
    OperatorFloorEE::read(ReadInput::main);
    OperatorFloor3d::read(ReadInput::main);


    // ---------------------------------------------------------------------------------
    string title;
    ReadInput::main.read("Title","",title,"tRecX calculation","title that will be printed to output");

    // Laser Pulse
    Pulse::read(ReadInput::main,true);

    // operators
    tRecX::readExtra(ReadInput::main);

    // Eigenproblem xor initial value problem
    ReadInput::main.exclude("Initial","Eigen");

    string eigenSelect;
    ReadInput::main.read("Eigen","select",eigenSelect,"NONE",
                         "kinds: All, SmallReal[N]...first N, Rectangle[Rmin,Rmax,Imin,Imax]...in complex plane, NONE",
                         1,"eigenSelect");

    string initialKind;
    int initialN;
    ReadInput::main.read("Initial","state",initialN,"0","initial state (number of state in Hamiltonian)");
    initialKind=InitialState::readKind(ReadInput::main);

    string hamDef,intDef,iniDef,paramDef,expecDef,preconDef,specDef;
    ReadInput::main.read("Operator","hamiltonian",hamDef,ReadInput::noDefault,"Hamiltonian definition");
    ReadInput::main.read("Operator","interaction",intDef,"","interaction definition");
    ReadInput::main.read("Operator","initial",iniDef,"atBegin",string("eigenstate of this operator will be used as initial states -- ")
                         +"atBegin..H(tBegin), hamiltonian..H0 or general operator string");
    if(iniDef=="hamiltonian")iniDef=hamDef;
    ReadInput::main.read("Operator","projection",preconDef,hamDef,"spectrally project in time propagation wrt to this operator");
    ReadInput::main.read("Operator","parameterTerm",paramDef,"","use hamiltonian+parameterTerm for parameter studies");
    ReadInput::main.read("Operator","expectationValue",expecDef,"","expectation values (comma-separated list inside quotes),"
                         +string(" default is overlap and energy, \'suppress\' for suppressing all expectation values"));
    ReadInput::main.read("Operator","spectrum",specDef,"Hamiltonian","print spectral decomposition of wf wrt to operator");
    OperatorDefinition::setParameters(intDef); // need gauge radius for checking discretization setup

    // time propagation
    string propMethod;
    double applyThreshold,accuracy,cutE,fixStep,tBeg=0.,tEnd=0.,tPrint,tStore;
    TimePropagator::read(ReadInput::main,tBeg,tEnd,tPrint,tStore,accuracy,cutE,fixStep,applyThreshold,propMethod);
    std::string dum1,dum2;
    IndexConstraint::readAll(ReadInput::main,dum1,dum2);

    std::vector<double> surf;
    bool surfAscii;
    ReadInput::main.read("Surface","points",surf,"","save values and derivatives at surface points (blank-separated list)");
    ReadInput::main.read("Surface","ascii",surfAscii,ReadInput::flagOnly,"produce ascii file with surface values",0,"surfaceAscii");
    int spectrumPoints=0;
    double minEnergy,maxEnergy;
    bool kGrid(false),computeSpectrum;
    ReadInput::main.read("Spectrum","spectrum",computeSpectrum,ReadInput::flagOnly,"compute spectrum",1,"spectrum");
    if(surf.size()==0 and computeSpectrum)ABORT("cannot compute spectrum, need Surface: points defined");

    bool specOverwrite;
    std::string plotWhat,ampFiles;
    SpectrumPlot::read(ReadInput::main,plotWhat,specOverwrite,ampFiles);


    std::string region;
    region=TsurffSource::nextRegion(ReadInput::main,region);
    if(region.find("unbound axes:")!=string::npos)
        PrintOutput::set(ReadInput::main.output()+"outspec");
    else if (region!="")
        ReadInput::main.outSubdir("S_"+region);
    else
        PrintOutput::set(ReadInput::main.output()+PrintOutput::outExtension);

    PrintOutput::title(title);
    PrintOutput::paragraph();

    DiscretizationTsurffSpectra::readFlags(ReadInput::main,spectrumPoints,minEnergy,maxEnergy,kGrid,true);
    TsurffSource::allReads(ReadInput::main);
    do {
        Parallel::clear();
        START(run);
        START(setup);
        LOG_PUSH("execute");
        MPIwrapper::setCommunicator(Threads::all());

        timeCritical::suspend();
        // update spectral region
        region=TsurffSource::nextRegion(ReadInput::main,region);

        if(ReadInput::main.flag("generate-linp","only generate list-of-inputs file") )
            PrintOutput::set("cout");
        else {
            std::string linp=ReadInput::main.outputTopDir()+"linp";
            MPIwrapper::Bcast(linp,MPIwrapper::master());
            if(region!="" and
                    (spectrumPoints>0 or
                     string(argv[1]).find(ReadInput::inputCopy)+ReadInput::inputCopy.length()==string(argv[1]).length())
                    ){

                // plot spectra from existing ampl files
                if(region.find("unbound axes:")!=string::npos){
                    // plotting does not work in parallel - use single thread
                    MPIwrapper::setCommunicator(Threads::single());
                    if(MPIwrapper::isMaster(Threads::all())){
                        PrintOutput::set(ReadInput::main.outputTopDir()+"outspec");
                        SpectrumPlot::allRegions(ReadInput::main.outputTopDir(),ReadInput::main);
                        //SpectrumPlot plt;
                        //                        plt=SpectrumPlot(ReadInput::main.outputTopDir(),ReadInput::main);
                        //                        plt.plot(ReadInput::main);

                        PrintOutput::paragraph();
                        PrintOutput::message("spectra calculated from amplitude file(s), for recomputing amplitudes, specify region as -region=AXIS_NAME");
                    }
                    STOP(run);
                    goto Terminate;
                }

                ReadInput::main.outSubdir("S_"+region);
                PrintOutput::set(ReadInput::main.output()+PrintOutput::outExtension);
            }
        }

        if(ReadInput::main.flag("generate-linp","only generate list-of-inputs file"))PrintOutput::off();

        std::string info="Master host = "+tools::cropString(platformSpecific::current_host());
        if(MPIwrapper::Size(Threads::all())>1)info+=" ("+tools::str(MPIwrapper::Size())+" processes)";
        PrintOutput::message(info);
        PrintOutput::paragraph();

        STARTDEBUG(input);
        LOG_PUSH("setup");
        LOG_PUSH("discretization");
        STARTDEBUG(setupDisc);

        std::shared_ptr<Discretization> mainD(Discretization::factory(ReadInput::main));
        BasisOrbital::addIndex("main",mainD->idx());

        STOPDEBUG(setupDisc);
        LOG_POP();
        LOG_PUSH("other");

        // ---------------------------------------------------------------------------------

        // Construct the discretization for the subregion, find all the sources feeding into this region and check if they exist
        vector<DiscretizationSurface* > source_surfaces;
        vector<TsurffSource* > source_terms(0);
        std::shared_ptr<Discretization> runD(mainD);
        if(region=="")
            mainD->print();
        else{
            // calculation in subregion
            string srcs;
            srcs=DiscretizationSurface::surfaceFiles(ReadInput::main.outputTopDir(),mainD->idx(),region).back();
            source_surfaces.push_back(new DiscretizationSurface(mainD.get(),surf,srcs));

            // spectral discretization (will be forked along radial spectral values)
            runD.reset(new DiscretizationTsurffSpectra(new SurfaceFlux(srcs),ReadInput::main));
            source_terms.push_back(new TsurffSource(dynamic_cast<DiscretizationTsurffSpectra*>(runD.get()),ReadInput::main,source_surfaces,region,0));

            if(source_terms.size()==0)ABORT("no source");
            for(TsurffSource* s: source_terms)s->readTurnOff(ReadInput::main,tEnd);

            runD->print("","DISCRETIZATION AND INCOMING FLUX");
            PrintOutput::newLine();
            source_terms[0]->print(srcs);
        }
        if(surf.size()>0){
            PrintOutput::paragraph();
            PrintOutput::lineItem("Surfaces",Str("","")+surf);
        }

        OperatorFloor3d::print();
        CoefficientsWriter::read(ReadInput::main);
        if(region!="")CoefficientsWriter::instance()->disable();

        // surface discretization for each unbound coordinate axis
        //NOTE: reverted to using same surface on all coordinates
        vector<DiscretizationSurface*> discSurf;
        if(region=="" and TsurffSource::readSymmetry12(ReadInput::main))
            discSurf.push_back(DiscretizationSurface::factory(runD.get(),surf,0));      // Surface disc corresponding to each continuity level
        else
            for(unsigned int c=0;surf.size()>0 and runD->idx()->continuity(c)!=Index::npos;c++)
                discSurf.push_back(DiscretizationSurface::factory(runD.get(),surf,c));      // Surface disc corresponding to each continuity level


        vector<string>dipNames;
        string dipoleDef=Harmonics::dipoleDefinitions(ReadInput::main,mainD->coordinates(),hamDef+"+"+intDef,dipNames);
        PrintOutput::paragraph();
        PrintOutput::newRow();
        PrintOutput::title("OPERATORS");
        PrintOutput::paragraph();
        PrintOutput::lineItem("Hamiltonian",hamDef);
        PrintOutput::newLine();

        std::string subDef=hamDef;
        if(mainD!=runD){
            subDef=OperatorDefinitionNew(hamDef,mainD->idx()->hierarchy()).tsurffDropTerms(runD->idx(),runD->idx());
            if(subDef!="")PrintOutput::lineItem("Region "+region,subDef);
            else          PrintOutput::lineItem("Region "+region,"only spectral amplitudes");
            PrintOutput::newLine();
        }
        if(intDef!=""){
            PrintOutput::lineItem("Interaction",intDef);
            PrintOutput::newLine();
            if(region!=""){
                PrintOutput::lineItem("Region "+region,OperatorDefinitionNew(intDef,mainD->idx()->hierarchy()).tsurffDropTerms(runD->idx(),runD->idx()));
                PrintOutput::newLine();
            }
        }

        if(initialKind=="manyBody")iniDef=hamDef;

        string iniName=iniDef;
        if(iniDef==hamDef)iniName="Hamiltonian";
        else if(iniDef=="atBegin")iniName="H(t=Begin)";
        PrintOutput::lineItem("HamInitial",iniName);
        PrintOutput::newLine();

        PrintOutput::newLine();
        if(paramDef!=""){PrintOutput::lineItem("Static field",paramDef);PrintOutput::newLine();}
        if(expecDef!=""){PrintOutput::lineItem("Expectation Values",expecDef);PrintOutput::newLine();}
        if(preconDef!=""){
            if(preconDef==hamDef)
                PrintOutput::lineItem("Projection"," = Hamiltonian");
            else
                PrintOutput::lineItem("Projection",preconDef);
        }
        if(dipoleDef!=""){PrintOutput::newLine();PrintOutput::lineItem("Dipoles",dipoleDef);PrintOutput::newLine();}
        PrintOutput::paragraph();

        // ranges for operator parameters and initialize parameters
        ScanEigenvalue eigenScan(ReadInput::main);
        if(eigenScan.size()>0)eigenScan.print();

        FloquetAnalysis floqAna(ReadInput::main);

        EigenSubspace eigSub(ReadInput::main);

        Pulse::current.output("",hamDef+intDef);

        //    if(surf.size()>0)VolkovPhase::consistency(hamDef+intDef);

        TimePropagator::print(tBeg,tEnd,tPrint,tStore,accuracy,cutE,fixStep,applyThreshold);
        Str strIni(initialKind,"");
        if(initialN!=0)strIni=strIni+" state["+initialN+"] ";
        strIni=strIni+" for operator "+iniName;
        PrintOutput::lineItem("Initial state",strIni);
        PrintOutput::paragraph();

        string projConstraint;
        ReadInput::main.read("Project","constraints",projConstraint,"","constrain spectral projection");

        // set up possible spectral cuts
        SpectralCut specCut(runD.get(),ReadInput::main,hamDef);

        PrintOutput::title("OUTPUT");
        PrintOutput::lineItem("Directory",ReadInput::main.output());
        PrintOutput::paragraph();
        PrintOutput::paragraph();

        STOPDEBUG(input);
        STARTDEBUG(setupPLot);
        std::shared_ptr<Plot> plot(new Plot(runD.get(),ReadInput::main)); // set up plot, get the definitions from file
        plot->print();

        std::shared_ptr<PlotCoefficients> plotC(plot);
        STOPDEBUG(setupPLot);
        STARTDEBUG(input);

        if(ReadInput::main.flag("showDiscretization","print the discretization and stop")){
            runD->print();
            exit(0);
        }

        // if no input specified, just print doc
        if(ReadInput::main.file()=="trecx.inp"){
            ReadInput::main.writeDoc();
            cout<<"\nUsage: tRecX file[.inp] [-flags]"<<endl;
            exit(0);
        }

        if(ReadInput::main.flag("generate-linp","only generate list-of-inputs file")){
            cout<<"\nList-of-inputs (linp) written to "+ReadInput::main.output()<<endl;
            exit(0);
        }

        // create gradient operator, if specified (=0 else)
        OperatorAbstract *chanMap=0,*grad=0;
        string ChanAxis=OperatorMapChannelsSurface::axis(ReadInput::main);
        if(ChanAxis!="")grad=OperatorGradient::read(runD.get(),ReadInput::main);

        // constraints for initial state calculations (unmainained)
        DiscretizationConstrained::inputs(ReadInput::main);

        tRecX::read();
        tRecX::print();

        DensityOfStates dos(ReadInput::main);

        tRecX::PlotFunctions(ReadInput::main);

        // all inputs should be finished above these lines - please do not remove
        ReadInput::main.finish();
        PrintOutput::paragraph();
        STOPDEBUG(input);
        //==== END OF INPUT ====================================================================

        // can use further cleanup... (get rid of Chandef/ChanAxis)
        std::string Chandef="";
        if(ChanAxis!="")chanMap=new OperatorMapChannelsSurface(ReadInput::main,runD.get());

        LOG_POP();
        LOG_PUSH("Operators");

        START(operator);
        std::shared_ptr<OperatorTree> initialOper,propOper,hamOper;
        vector<OperatorAbstract*>printOps;

        tRecX::SetOperators(mainD->idx()->hierarchy(),hamDef,intDef,iniDef,"",dipNames,expecDef,runD.get(),propOper,hamOper,initialOper,printOps);
        MPIwrapper::Barrier();

        // Shift the full Hamiltonian spectrum by a specified channel energy
        OperatorMapChannelsSurface* mapch = static_cast<OperatorMapChannelsSurface*>(chanMap);
        if(mapch != nullptr && mapch->energyShift() != 0.) {
            std::string ovl;
            if(hamOper->iIndex->isHybrid()) // hybrid basis needs special string
                ovl = "<1>(<<FullOverlap>>)+<1,0>[[haCC1]]+<0,1>[[haCC1]]";
            else ovl = "<<FullOverlap>>";

            // HACK: As for now, it is not very clear what happens with OperatorTrees that got added/fused --> create two shift operators to be sure, don't dare to "delete" the pointers, as this may delete floors
            OperatorTree* shift = new OperatorTree("IonicShift", OperatorDefinition(ovl,hamOper->iIndex->hierarchy()), hamOper->iIndex, hamOper->iIndex, -mapch->energyShift());
            hamOper->add(shift);
            shift = new OperatorTree("IonicShift", OperatorDefinition(ovl,hamOper->iIndex->hierarchy()), hamOper->iIndex, hamOper->iIndex, -mapch->energyShift());
            propOper->add(shift);
        }

        Harmonics::addDipolesToPlot(dipoleDef,*plot,printOps);
        STOP(operator);
        LOG_POP();
        LOG_PUSH("other");

        STARTDEBUG(other);
        tRecX::PrintShow(showMatrices,printMatrices,showOperators,*propOper,*initialOper,printOps);

        // make sure inverse of overlap works correctly
        if(propOper!=0)runD->idx()->testInverseOverlap();
        MPIwrapper::Barrier();

        if(ReadInput::main.found("Operator","spectrum")){
            if(plot->dimension()>0)ABORT("wave function plot and spectral plot mutually excluded");
            if(specDef!="Hamiltonian")ABORT("for now, only for Hamiltonian");
            std::shared_ptr<DiscretizationSpectral>spec(new DiscretizationSpectral(runD.get(),initialOper.get()));
            plotC.reset(new PlotSpectral(spec.get()));
        }

        // output dos (if set up) and stop
        dos.output(propOper.get());

        STOPDEBUG(other);
        LOG_POP();
        LOG_POP();

        if(eigenSelect!="NONE")
        {
            Parameters::updateSpecial();
            tRecX::ComputeEigenvalues(eigenSelect,*plot,printOps,Operator::eigenMethod);
        }

        else if(eigenScan.size()>0){
            // scan eigenvalues for a range of parameters
            Parameters::updateSpecial();
            ABORT("re-formulate for OperatorTree");
            //        Operator op(*propOper);
            //        eigenScan.setEigenSub(D,&op,dynamic_cast<const Operator*>(printOps[1]));
            //        eigenScan.scan();
        }

        else if(ReadInput::main.found("FloquetAnalysis")){
            ABORT("re-formulate for OpeatorTree");
            //        Operator op(*propOper);
            //        floqAna.run(&op,D);
        }


        else if(tBeg<tEnd)
        {
            STARTDEBUG(setProp);
            LOG_PUSH("DerivativeFlat");

            // no eigenvalue - time-propagate
            DerivativeFlat * propDer=tRecX::SetDerivative(runD.get(),propOper.get(),hamOper.get(),initialOper.get(),applyThreshold,cutE,preconDef,projConstraint,source_terms);
            LOG_POP();

            // set up the output
            TimePropagatorOutput out;
            tRecX::SetOutput(out,tPrint,tStore,surfAscii,runD.get(),discSurf,region,plotC.get(),grad,printOps,expecDef,ChanAxis,Chandef,chanMap);
            out.setInterval(tBeg,tEnd);

            // checks and messages before start of time propagation
            std::shared_ptr<ChannelsSubregion>channels;
            if(propOper!=0){
                OperatorDefinition(propOper->def()).checkVariable("non-const parameters may have penalty in time-propagation, avoid!",propOper->name);
                PrintOutput::warningList();

                LOG_PUSH("other");
                // set up channels
                if(runD!=mainD and (runD->idx()->hierarchy()!="specX.Z.kX.Z")){
                    //                    LOG_PUSH("ChannelsSubregion");
                    //                    runD->print();
                    //                    channels.reset(new ChannelsSubregion(runD.get(),hamOper.get(),region, ReadInput::main));
                    //                    LOG_POP();
                    //                    out.withChannelsSubregion(channels.get());
                    PrintOutput::DEVwarning("channels subregion disabled for development");
                }
                LOG_POP();

            }
            // time-propagator (for parallel code, create local derivative operator)
            LOG_PUSH("TimePropagator");

            TimePropagator* prop;
            DerivativeLocal* derLoc=new DerivativeLocal(propDer);
            OdeStep<LinSpaceMap<CoefficientsLocal>,CoefficientsLocal>*odeLoc;
            if(propOper)odeLoc=new OdeRK4<LinSpaceMap<CoefficientsLocal>,CoefficientsLocal>(derLoc);
            else        odeLoc=new OdeIntegral<LinSpaceMap<CoefficientsLocal>,CoefficientsLocal>(derLoc);
            prop=new TimePropagator(odeLoc,&out,accuracy,fixStep);
            LOG_POP();
            STOP(setup);

            // timer info for all setup
            PrintOutput::warningList();
            PrintOutput::timerWrite();
            Parallel::timer();

            if(ReadInput::main.flag("DEBUGsetup","stop after setup")){
                STOPDEBUG(setProp);
                STOP(run);
                goto Terminate;
            }

            // initial wave function
            if(runD!=mainD)initialKind="ZERO";
            else if (iniDef!="atBegin" and initialKind!="manyBody")initialKind="Hinitial";

            // If there is a stored checkpoint, use it. Otherwise proceed normally
            Wavefunction wf;
            LOG_PUSH("InitialState");
            wf=InitialState::get(runD.get(),initialKind,initialN,tBeg,*initialOper,propDer);
            if(dynamic_cast<DerivativeFlat*>(derLoc))dynamic_cast<DerivativeFlat*>(derLoc)->project(*wf.coefs);
            LOG_POP();

            // propagation monitor settings
            string mess=runD->name+" Tol="+prop->str(1)+" "+Pulse::current.str(1);
            if(MPIwrapper::isMaster())out.timer->monitor(wf.time,mess,ReadInput::main.outputTopDir()+"mon");
            STOPDEBUG(setProp);
            LOG_PUSH("propagation");

            if(propOper==0){
                // no actual propagation, only integration of spectra
                //NOTE: we propagate RK4, sample 32 times across shortest cycle in the spectrum
                double maxE=max(3*Pulse::current.omegaMax(),12.*Pulse::current.uPonderomotive());
                prop->fixStep(2*math::pi/(32*maxE));
            }

            // time-propagate
            if(wf.time<min(Pulse::gettEnd(),tEnd))prop->propagate(&wf,min(Pulse::gettEnd(),tEnd),"Start");
            LOG_POP();
            LOG_PUSH("afterwards");
            if(Pulse::gettEnd()<tEnd)prop->propagate(&wf,tEnd,"Stop");
            LOG_POP();

            // amplitude calculation
            if(propOper==0 or region=="Rn1" or region=="Rn2"){
                if(MPIwrapper::isMaster()){
                    ofstream ampl((ReadInput::main.output()+"ampl").c_str(),(ios_base::openmode) ios::beg|ios::binary);
                    const Coefficients * joinedWf=Threads::join(*wf.coefs);
                    if(Threads::isMaster())joinedWf->write(ampl,true,"IndexFull");
                    PrintOutput::message("Photo-electron amplitudes on "+ReadInput::main.output()+"ampl");
                }
            }

            if(region=="Rn1" or region=="Rn2"){
                // single-ionization spectra
                const Coefficients * joinedWf=Threads::join(*wf.coefs);
                if(Threads::isMaster()){
                    std::vector<std::string> ax=region=="Rn1"?std::vector<std::string>(1,"kRn1"):std::vector<std::string>(1,"kRn2");
                    std::vector<std::string> use(1,"g");
                    if(region=="Rn1")ax[0]="kRn1";
                    Plot plt(joinedWf->idx(),ax,use);
                    plt.plot(*joinedWf,ReadInput::main.output()+"spec_single");
                    PrintOutput::message("single ionization spectrum on "+ReadInput::main.output()+"spec_single");
                }
            }

            PrintOutput::end();
            PrintOutput::paragraph();
            PrintOutput::subTitle("TimePropagator "+prop->info());
            delete propDer;
            delete prop;
            MPIwrapper::setCommunicator(Threads::all());

        }
        else
            PrintOutput::message(Str("tBeg =")+tBeg+">= tEnd="+tEnd+"--- no time propagation",0,true);

        if(MPIwrapper::isMaster() and dipNames.size()>0)Harmonics(ReadInput::main,"expec"," ",false,Units::convert(1.,"OptCyc"));

        LOG_POP();

        MPIwrapper::setCommunicator(MPIwrapper::worldCommunicator());
        STOP(run);
        PrintOutput::warningList();
        PrintOutput::timerWrite();
        Parallel::timer();
    }
    while ((computeSpectrum or ReadInput::main.found("Spectrum")) and
           region!=TsurffSource::nextRegion(ReadInput::main,region) and
           surf.size()>0 and
           spectrumPoints!=0);

Terminate:
    LOG_POP();
    LOG_POP();

    MPIwrapper::setCommunicator(MPIwrapper::worldCommunicator());
    PrintOutput::paragraph();
    ReadInput::main.finish();
    PrintOutput::terminationMessages();

Terminate_basic:
    //    Coefficients::cleanUp();
    BasisMat::cleanUp();
    Coordinate::cleanUp();
    PrintOutput::title("done - "+ReadInput::main.output());
    MPIwrapper::Finalize();
    exit(0);
}
