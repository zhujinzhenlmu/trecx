// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "readInput.h"
#include "printOutput.h"
#include "units.h"
#include "mpiWrapper.h"
#include "fieldTailor.h"

#include <dlib/optimization.h>

using namespace std;

int main(int argc, char* argv[]) {
    MPIwrapper::Init(argc,argv);

    //============================================================================================
    // input and part of setup
    //==============================================================================================

    ReadInput::openMain("",argc,argv);
    Units::setDefault("au");        // general default units
    ReadInput::main.setUnits("au"); // convert input to these units

    if(not MPIwrapper::isMaster())PrintOutput::off();

    // read the pulse range
    FieldTailor f(ReadInput::main);
    f.print();

    ReadInput::main.finish();

    // run all trajectories
    f.run();
    f.writeTrajectories();
}
