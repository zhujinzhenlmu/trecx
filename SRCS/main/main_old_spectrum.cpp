// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply.
//
// See terms of use in the LICENSE file included with the source distribution
//
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "tools.h"

#include <vector>
#include "timer.h"

// for minimalEvaluator
#include "readInput.h"
#include "printOutput.h"

// for evaluator
#include "evaluator.h"

#include "coefficients.h"
#include "operatorDefinition.h"
#include "basisMat.h"

#include "mpiWrapper.h"
#include "asciiFile.h"
#include "spectrumTinfinite.h"

#include "composeAmplitudes.h"

#ifndef _SEDNA_
#include "spectrumCoulomb.h"
#endif
#include "plot.h"
#include "oldSpectrum.h"
#include "discretizationDerived.h"
#include "tsurffsource.h"
#include "discretizationSurface.h"
#include "discretizationtsurffspectra.h"
using namespace std;

int main(int argc, char* argv[]) {
    MPIwrapper::Init(argc,argv);
    // for the genergation of grids, not working yet
//    ReadInput::openMain(ReadInput::flagOnly,argc,argv,false);
//    ReadInput dataInp(string(argv[1])+"/inpc",argc,argv,false);
//    dataInp.setUnits("au");
//    cout<< "done creating evaluator"<< endl;
//    Discretization * D = Discretization::factory(dataInp);
//    cout<< "done1"<< endl;
//    DiscretizationSurface * S1 = new DiscretizationSurface(D,vector<double>(1,20.), 1);
//    cout<< "done2"<< endl;

//    Discretization * Dtf1 = DiscretizationTsurffParallel::factoryTsurff(S1,dataInp);
//    delete S1;
//    cout<< "done3"<< endl;

//    DiscretizationSurface * S2 = new DiscretizationSurface(Dtf1,vector<double>(1,20.), 0);
//    delete Dtf1;
//    cout<< "done4"<< endl;

//    Discretization * Dtf2 = DiscretizationTsurffParallel::factoryTsurff(S2,dataInp);
//    delete S2;
//    cout<< "show "<<Dtf2->Idx->hierarchy()<< endl;
//    const Index * dt = Dtf2->Idx->firstLeaf()->parent()->parent();
//    while(dt != 0){
//        while(dt->childSize() > 1)
//            const_cast<Index *>(dt)->childPop();
//        while(dt->child(0)->childSize() > 1)
//            dt->child(0)->childPop();
//        dt = dt->nodeRight();
//    }
//    cout<< "done discretization"<< endl;
//    OldSpectrum * old = new OldSpectrum(argv[1], -1, -1, true);
//    old->construct();

//    vector<string>axis(1,"kRn1"),use(1,"g");
//    vector<unsigned int> points(1,0);
//    vector<vector<double> >bounds(1,vector<double>(2));
//    axis.push_back("kRn2");
//    use.push_back("g");
//    points.push_back(0);
//    bounds.push_back(vector<double>(2));
//    // never sum over specRn
//    axis.push_back("specRn1");
//    use.push_back("p");
//    points.push_back(0);
//    bounds.push_back(vector<double>(2,0.));
//    axis.push_back("specRn2");
//    use.push_back("p");
//    points.push_back(0);
//    bounds.push_back(vector<double>(2,0.));

//    use.push_back("g");
//    axis.push_back("Eta1");
//    points.push_back(64);
//    bounds.push_back(vector<double>(2,-1.));
//    bounds.back()[1]=1.;

//    use.push_back("g");
//    axis.push_back("Eta2");
//    points.push_back(4);
//    bounds.push_back(vector<double>(2,-1.));
//    bounds.back()[1]=1.;
////    Plot plot(Dtf2, dataInp);
//    Plot plot(Dtf2,axis,use,points,bounds);
//    plot.print();
//    string specFile = "specFromOld";
////    const Coefficients test(*(old->ampl));
//    vector<string> head;
//    const Index * testIdx = Dtf2->Idx->firstLeaf();
//    cout<< "The leaf testIdx "<< testIdx->axisName()<< endl;

//    Coefficients test(Dtf2->Idx);
//    *(test.child(0)->child(0)) = *(old->ampl);
//    plot.plot(test,specFile,head,"",true);
    OldSpectrum * old = new OldSpectrum(argv[1], -1, -1, false);
    old->construct();
    old->DI();
    vector<vector<double> > spectrum = old->DI(true);
    old->plotJAD(64);

    return 0;
} // main
