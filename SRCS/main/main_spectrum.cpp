// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply.
//
// See terms of use in the LICENSE file included with the source distribution
//
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "tools.h"
#include "tRecX.h"

#include <vector>
#include "timer.h"

#include "mpiWrapper.h"
#include "readInput.h"
#include "printOutput.h"
#include "asciiFile.h"

#include "evaluator.h"
#include "coefficients.h"

#include "spectrumTinfinite.h"
#include "spectrumLinCom.h"
#include "spectrumPlot.h"

#ifndef _SEDNA_
#include "spectrumCoulomb.h"
#endif

#include "plot.h" // should go elswhere
#include "quantumChemicalInput.h"

using namespace std;

TIMER(all,)
TIMER(setup,)
TIMER(spectrum,)
TIMER(plot,)

std::vector<std::string> commandLineUnflaggedArguments(int argc, char* argv[]){
    std::vector<string> argU;
    for(int k=1;k<argc;k++)
        if(argv[k][0]!='-')argU.push_back(argv[k]);
    return argU;
}


int main(int argc, char* argv[]) {


    MPIwrapper::Init(argc,argv);

    START(all);

    // test for spectral parameter file
    ReadInput::openMain(ReadInput::flagOnly,argc,argv,false);
    Units::setDefault("au");
    ReadInput::main.setUnits("au");
    Units::setDefault("au");

    if(argc<2){
        AsciiFile("Spectrum.doc").copy(cout);
        PrintOutput::paragraph();
        PrintOutput::message("usage: >Spectrum runDir/0123 [flags]");
        PrintOutput::message("(available inputs see above)");
        MPIwrapper::Finalize();
        exit(0);
    }

    if(argv[1][0]=='-')ABORT("first command line argument must be data directory, e.g. myRun/0137, found \""+string(argv[1])+"\"");
    string run=string(argv[1]);


    // read main controls
    bool forceCompute,overwrite;
    SpectrumPlot plt;
    plt=SpectrumPlot(run,ReadInput::main);
    overwrite=true;
    ReadInput::main.read("Spectrum","compute",forceCompute,ReadInput::flagOnly,"force re-computation of ampl file",1,"compute");

    // ensure all possible reads were done (so ReadInput::main.finish() does not complain)
    SpectrumLinCom::found();
    PrintOutput::noScreenFlag();

    vector<string> specFileHeader;
    if(folder::exists(run+"/inpc") and not SpectrumLinCom::found()
            and (forceCompute or not folder::exists(run+"/ampl")))
    {
        // this part is obsolescent - functionality is in tRecX
        ReadInput dataInp(run+"/inpc",argc,argv,false);
        dataInp.setUnits("au");
        double E1=0., E2=0.;
        int thetaNum;
        dataInp.read("JAD","E1",E1,"0.","The selected E1 to plotJAD");
        dataInp.read("JAD","E2",E2,"0.","The selected E2 to plotJAD");
        dataInp.read("JAD","thetaNum",thetaNum,"64","The k2 index to plotJAD");
        PrintOutput::set(dataInp.output()+"anaout");

        string col_path;
        dataInp.read("Columbus","path",col_path,"None","Columbus run path");
        QuantumChemicalInput::read(dataInp);

        START(setup);
        tRecX::readBases(dataInp);
        tSurffTools::Evaluator eval(plt.what(),forceCompute,overwrite,dataInp);
        STOP(setup);

        SpectrumTinfinite specInf(ReadInput::main,dataInp);

        vector<Coefficients> vSpec;
        Coefficients aTinfty;
        double tEnd;
        specInf.compute(eval,vSpec,tEnd,aTinfty);

        for(int k=0;k<vSpec.size();k++)
            eval.plotSpectrum(vSpec[k],Str("R","")+tools::str(int(specInf.radius(k)),5,'0'));

        if(E1 != 0. and E2 != 0.)
            Plot::plotJAD(eval.amplitude(),E1,E2,string(argv[1])+"/JAD_" + tools::str(E1) + "_" + tools::str(E2),thetaNum);

        START(spectrum);
        eval.computeSpectrum(tEnd);
        STOP(spectrum);

        specFileHeader=eval.inputFlags;
    }

    if (SpectrumLinCom::found()){
        if(forceCompute)ABORT("for linear combination, first compute all spectra");
        SpectrumLinCom linCom(commandLineUnflaggedArguments(argc,argv));
        linCom.plotAverage(plt);
    }
    else {
        if(commandLineUnflaggedArguments(argc,argv).size()>1)
            ABORT("for standard compute, specify only a single input directory");

        string ampfile=run;
        if(ampfile.rfind("/ampl")==string::npos
                and ampfile.find("VinayData")==string::npos)ampfile+="/ampl";
        specFileHeader.insert(specFileHeader.begin(),"read from: "+ampfile);
        plt.setInfo(specFileHeader);
        plt.amplFromFile(ampfile);
        string chan;
        if(ampfile.find("/ampl_")!=string::npos)chan="c"+ampfile.substr(ampfile.rfind("/ampl_")+6);
        plt.plot(chan);
    }

    ReadInput::main.finish(); // final crosscheck on flags etc.

    STOP(all);

    PrintOutput::timerWrite();
    PrintOutput::terminationMessages();
    PrintOutput::warningList();
    PrintOutput::title("done - "+string(argv[1]));
} // main
