// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply.
//
// See terms of use in the LICENSE file included with the source distribution
//
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "tools.h"

#include <vector>
#include "timer.h"

// for minimalEvaluator
#include "readInput.h"
#include "printOutput.h"

// for evaluator
#include "evaluator6D.h"

#include "coefficients.h"
#include "operatorDefinition.h"
#include "basisMat.h"

#include "mpiWrapper.h"
#include "asciiFile.h"
#include "plot.h"
using namespace std;

TIMER(all,)
TIMER(setup,)
TIMER(spectrum,)
TIMER(plot,)

int main(int argc, char *argv[]) {


    MPIwrapper::Init(argc, argv);

    START(all);

    // test for spectral parameter file
    ReadInput::openMain(ReadInput::flagOnly, argc, argv, false);
    Units::setDefault("au");
    ReadInput::main.setUnits("au");
    Units::setDefault("au");

    if (argc < 2) {
        AsciiFile("Spectrum.doc").copy(cout);
        PrintOutput::paragraph();
        PrintOutput::message("usage: >Spectrum runDir/0123 [flags]");
        PrintOutput::message("(available inputs see above)");
        MPIwrapper::Finalize();
        exit(0);
    }
    if (argv[1][0] == '-')ABORT("first command line argument must be data directory, e.g. myRun/0137, found \"" + string(argv[1]) + "\"");
    string run = string(argv[1]);
    if (run.rfind("/") + 5 != run.length())
        PrintOutput::warning("run-directories are \"inputFileName/0123\",\nyour input: \"" + run + "\", are you sure? ");
    ReadInput dataInp(string(argv[1]) + "/inpc", argc, argv, false);
    dataInp.setUnits("au");
    double E1, E2;
    int thetaNum;
    dataInp.read("JAD", "E1", E1, "0.", "The selected E1 to plotJAD");
    dataInp.read("JAD", "E2", E2, "0.", "The selected E2 to plotJAD");
    dataInp.read("JAD", "thetaNum", thetaNum, "64", "The k2 index to plotJAD");
    PrintOutput::set(dataInp.output() + "anaout");

    OperatorDefinition::setup();
    START(setup);
    tSurffTools::Evaluator6D eval(dataInp);
    STOP(setup);

    ReadInput::main.finish();

    double tEnd;

    START(spectrum);
    if (MPIwrapper::Size() > 1)
        eval.computeParallel();
    else
        eval.computeSpectrum(tEnd);
    STOP(spectrum);

    START(plot);
    eval.plotSpectrum(dataInp, *eval.amplitude());
    STOP(plot);

    STOP(all);
    if (MPIwrapper::isMaster()) {
        PrintOutput::timerWrite();
        PrintOutput::terminationMessages();
        PrintOutput::warningList();
        PrintOutput::title("done - " + string(argv[1]));
    }
    MPIwrapper::Finalize();
//    MPIwrapper::Finalize();

} // main
