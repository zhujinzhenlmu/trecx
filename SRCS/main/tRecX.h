// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef TRECX_H
#define TRECX_H

#include <vector>
#include <string>
#include <memory>

class Discretization;
class DiscretizationSurface;
class DiscretizationDerived;

class OperatorAbstract;
class Operator;
class OperatorTree;
class DerivativeFlat;
class Wavefunction;

class TimePropagator;
class TimePropagatorOutput;

class PlotCoefficients;

class MultiParam;
class Plot;

class TsurffSource;
class ReadInput;

/// auxiliary routines for tRecX main
namespace tRecX{

void readConstantsAndParameters();
void readBases(ReadInput &Inp);

void cleanUp();

/// various additional reads
void readExtra(ReadInput & Inp);

/// read function definitions from input and write graphs to file
void PlotFunctions(ReadInput & Inp);

/// for tSurff multi-electron spectra: reduce discretization D to sub-region
void GetDiscInSubregion(Discretization *&D, std::vector<DiscretizationSurface *> &srcSurf, int UnboundDOF, int Subregion, std::vector<double> Radii);

/// for diagnostics (mostly debugging)
void PrintShow(bool showMatrices /** create matrix and show structure */,
               bool printMatrices /** create matrix and print values */,
               bool showOperators /** show operator structure */,
               OperatorTree & PropOper /** for time propagation */,
               OperatorTree & InitialOper /** for initial state calculation */,
               std::vector<OperatorAbstract*> PrintOps /** operators for wich expectation values are printed */
               );

/// create output object
/// define if we need the expectationvalues
void SetOutput(TimePropagatorOutput & out,
               double tPrint /** time-interval for printing to output file (strictly)*/,
               double tStore /** time-interval for saving (approximate) */,
               bool surfAscii /** store ascii in addition to binary */,
               Discretization* D /** (obsolescent) */,
               std::vector<DiscretizationSurface *> S /** (obsolescent) */,
               std::string Region,
               const PlotCoefficients *plotC /** (obsolescent) */,
               OperatorAbstract* grad /** if non-zero, print Gradient to file*/,
               std::vector<OperatorAbstract*> PrintOps /** expectation values for these will be saved */,
               std::string expecDef,
               std::string ChanAxis,
               std::string Chandef,
               OperatorAbstract* chanMap);

/// compute and test eigenvalues of PrintOps[1] with overlap PrintOps[0]
void ComputeEigenvalues(std::string Select /** number of eigenvalues */,
                        Plot& plot /** eigenvectors will be plotted on grid*/,
                        std::vector<OperatorAbstract*> PrintOps/** all non-zero matrix elements for these will be saved */,
                        std::string Method="Lapack");

/// derivative for time-propagation
DerivativeFlat *SetDerivative(const Discretization * D /** needed for setting constraint */,
                              OperatorTree *PropOper /** time propagate by this operator */,
                              OperatorTree *HamOper /** interaction-free operator */,
                              OperatorTree *HamInitial /** operator for computing initial state */,
                              double applyThreshold /** if operator-block result remains below, block will not be applied*/,
                              double cutE /** spectral cut */,
                              std::string preconDef /** operator wrt to which spectral cut is applied */,
                              std::string projConstraint /** restriction on spectral constraint */,
                              const std::vector<TsurffSource*> &source_terms /** run with source (for multi-partical spectra) */
                              );

/// set up all desired operators
void SetOperators(std::string MainHierarchy /** operator definitions are for this hierarchy */,
                  std::string hamDef,
                  std::string intDef,
                  std::string iniDef,
                  std::string inFluxDef,
                  std::vector<std::string> dipNames,
                  std::string expecDef,
                  Discretization* D,
                  std::shared_ptr<OperatorTree>&PropOp,
                  std::shared_ptr<OperatorTree> &HamOp,
                  std::shared_ptr<OperatorTree> &InitialOper,
                  std::vector<OperatorAbstract *> &PrintOps
                  );

}
#endif // TRECX_H
