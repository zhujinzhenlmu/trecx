// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply.
//
// See terms of use in the LICENSE file included with the source distribution
//
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "tools.h"

#include <vector>
#include "timer.h"

// for minimalEvaluator
#include "readInput.h"
#include "printOutput.h"

// for evaluator

#include "coefficients.h"
#include "operatorDefinition.h"
#include "basisMat.h"
#include "discretization.h"
#include "operator.h"
#include "mpiWrapper.h"
#include "asciiFile.h"
#include "plot.h"
#include <dirent.h>
#include <sys/stat.h>
#include "fourierSpherical.h"
#include "pulse.h"

using namespace std;

TIMER(all,)
TIMER(setup,)
TIMER(spectrum,)
TIMER(plot,)

int main(int argc, char *argv[]) {

    MPIwrapper::Init(argc, argv);

    START(all);

    STARTDEBUG(input);
    //============================================================================================
    // input and part of setup
    //==============================================================================================
    if (not MPIwrapper::isMaster())PrintOutput::off();

    ReadInput::openMain("", argc, argv);


    Units::setDefault("au");        // general default units
    ReadInput::main.setUnits("au"); // convert input to these units

    //***************************************************************
    // for parallel debuging
    // compile with -O0
    // mpirun -np 4 ...
    // ps aux | grep tRecX
    // gdb tRecX procNumber
    // gdb> set DebugWait=0 to start each process
    if (ReadInput::main.flag("DEBUGparallel", "block, see main_trecx.cpp how to proceed")) {
        unsigned int DebugWait = 1;
        while (DebugWait);
        MPIwrapper::Barrier();
    }
    //***************************************************************

    Algebra::readConstants(ReadInput::main);
    Pulse::read(ReadInput::main, true);

    // ---------------------------------------------------------------------------------
    Discretization *D = Discretization::factory(ReadInput::main);
    if (D->Idx->hierarchy() != "Phi1.Eta1.Phi2.Eta2.Rn1.Rn2.Rn1.Rn2")
        ABORT("This program now only supports the 6D with Phi1.Eta1.Phi2.Eta2.Rn1.Rn2.Rn1.Rn2");
    double R0, R1, R2, R3;
    unsigned int N1, N2, NEta, sidesDiffer, plotMomenta;
    ReadInput::main.read("PlotWf", "R0", R0, "0", "R0");
    ReadInput::main.read("PlotWf", "R1", R1, "5", "R1");
    ReadInput::main.read("PlotWf", "R2", R2, "15", "R2");
    ReadInput::main.read("PlotWf", "R3", R3, "20", "R3");
    ReadInput::main.read("PlotWf", "NEta", NEta, "32", "NEta");
    ReadInput::main.read("PlotWf", "N1", N1, "5", "N1");
    ReadInput::main.read("PlotWf", "N2", N2, "5", "N2");
    ReadInput::main.read("PlotWf", "sidesDiffer", sidesDiffer, "0", "sidesDiffer");
    ReadInput::main.read("PlotWf", "plotMomenta", plotMomenta, "0", "plotMomenta");

    vector<double> boundE2 = {R2, R3}, boundE1 = {R0, R1};
    vector<double> boundUpEta = {0., 1.}, boundDnEta = {-1., 0.};
    vector<vector<double> > UpWfBounds = {boundE2, boundE1, boundUpEta, boundUpEta}, DnWfBounds = {boundE2, boundE1, boundDnEta, boundDnEta};
    if (sidesDiffer != 0) {
        UpWfBounds = {boundE2, boundE1, boundUpEta, boundDnEta}, DnWfBounds = {boundE2, boundE1, boundDnEta, boundUpEta};
    }
    vector<unsigned int> points = {N1, N2, NEta, NEta};
    vector<string> axes = {"Rn1", "Rn2", "Eta1", "Eta2"}, uses = {"g", "g", "", ""};
    Plot *plotUpWf = new Plot(dynamic_cast<const Discretization *>(D), axes, uses, points, UpWfBounds);
    Plot *plotDnWf = new Plot(dynamic_cast<const Discretization *>(D), axes, uses, points, DnWfBounds);
    FourierSpherical *fourierSph = 0;
    if (plotMomenta != 0) {
        fourierSph = new FourierSpherical(ReadInput::main, D);
        fourierSph->genPlot6D(NEta, sidesDiffer);
    }
    string outFolder = ("R0" + tools::str(R0) + "R1" + tools::str(R1) + "R2" + tools::str(R2) + "R3" + tools::str(R3) + "diff"  + tools::str(sidesDiffer)).c_str();
    outFolder = (ReadInput::main.outputDir() + "/" + outFolder + "/").c_str();
    DIR *dir;
    Coefficients C(D->Idx);
    Coefficients CTem(D->Idx);
    string outputUpFile, outputDnFile;

    struct dirent *ent;
    if ((dir = opendir (outFolder.c_str())) != NULL) {}
    else {
        if (mkdir(outFolder.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
            ABORT("the folder" + outFolder + " can not be created");
    }


    if ((dir = opendir (ReadInput::main.outputDir().c_str())) != NULL) {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL) {
            string fileName = string(ent->d_name);
            if (fileName.length() > 10 and fileName.substr(fileName.length() - 4) == ".bin") {
                cout << "Dealing with wf at t=" << fileName.substr(2, fileName.length() - 6) << " Opt. Cycle" << endl;
                std::ifstream stream((ReadInput::main.output() + fileName).c_str(),
                                     (std::ios_base::openmode) std::ios::beg | std::ios::binary);
                C.read(stream, true);
                string outputUpFile = (outFolder + "6dUpWf" + fileName.substr(2, fileName.length() - 6)).c_str();
                string outputDnFile = (outFolder + "6dDnWf" + fileName.substr(2, fileName.length() - 6)).c_str();
                if (fourierSph) {
                    outputUpFile = (outFolder + "6dUpK" + fileName.substr(2, fileName.length() - 6)).c_str();
                    outputDnFile = (outFolder + "6dDnK" + fileName.substr(2, fileName.length() - 6)).c_str();
                    fourierSph->plotTransform(outputUpFile, "Up", C);
                    fourierSph->plotTransform(outputDnFile, "Dn", C);
                } else {
                    outputUpFile = (outFolder + "6dUpWf" + fileName.substr(2, fileName.length() - 6)).c_str();
                    outputDnFile = (outFolder + "6dDnWf" + fileName.substr(2, fileName.length() - 6)).c_str();
                    plotDnWf->plot(C, outputDnFile);
                    plotUpWf->plot(C, outputUpFile);
                }

            }
        }
        closedir (dir);
    }
    MPIwrapper::Finalize();
} // main
