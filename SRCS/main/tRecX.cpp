// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply.
//
// See terms of use in the LICENSE file included with the source distribution
//
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#include "tRecX.h"

#include <memory>

#include "readInput.h"

// for GetDiscSubregion
#include "discretizationSurface.h"
#include "discretizationtsurffspectra.h"

// add for initialState
#include "discretizationConstrained.h"
#include "derivativeFlat.h"

// add for setOutput
#include "timePropagatorOutput.h"
#include "plotCoefficients.h"
#include "operatormapchannelssurface.h"

// add for eigen
#include "multiParam.h"
#include "plot.h"

// add for deriviative
//#include "tsurffsource.h"
#include "tsurffSource.h"
#include "discretizationSpectral.h"
#include "discretizationSpectralProduct.h"
#include "derivativeFlatInhomogeneous.h"

#include "parameters.h"
#include "operatorTree.h"
#include "operatorHF.h"
#include "operatorDefinition.h"
#include "operatorDefinitionNew.h"
#include "constrainedView.h"
#include "operatorSubspace.h"

#include "printOutput.h"
#include "algebra.h"

#include "potSolid.h"
#include "basisOrbital.h"

#include "log.h"

//TEMPORARY
#include "tools.h"
#include "operatorSVD.h"
#include "permuteOperatorTree.h"
#include "eigenSolver.h"
#include "eigenSolverAbstract.h"
#include "eigenSolverNonLin.h"

#include "tRecXchecks.h"
#include "asciiFile.h"

#include "parallelOperator.h"
#include "index.h"
#include "overlapDVR.h"
#include "discretizationHaCC.h"
#include "discretizationHybrid.h"
#include "projectSubspace.h"

#include "matrixTools.h"
#include "basisMO.h"
#include "basisNdim.h"
#include "basisOrbitalNumerical.h"
#include "basisMatMatrix.h"

#include "finDiff.h"

namespace tRecX{

using namespace std;

void cleanUp(){

}

void readBases(ReadInput & Inp){
    BasisMO::read(Inp);
    BasisNdim::read(Inp);
    BasisMatMatrix::read(Inp);
}


void readConstantsAndParameters(){

    Algebra::readConstants(ReadInput::main);
    Operator::readControls(ReadInput::main);
    double rtrunc,rsmooth;
    ReadInput::main.read("Operator","truncate",rtrunc,"infty","truncate potentials of type <<PotName>> (default=do not truncate)");
    ReadInput::main.read("Operator","smooth",rsmooth,tools::str(rtrunc),"start smoothing potentials of type <<PotName>> (default=no smoothing)");
    if(rtrunc<DBL_MAX/2){
        OperatorDefinition::truncationRadius(rsmooth,rtrunc);
        PrintOutput::lineItem("---> Truncation of potentials","["+tools::str(rsmooth)+","+tools::str(rtrunc)+"] <---");
        PrintOutput::newLine();
    }
    // experimental and debugging switches
    ReadInput::main.read("_EXPERT_","FDgrid",FinDiff::FDgrid,"exp","kind of grid spacing: exp,sudden,smooth",1);
    ReadInput::main.read("_EXPERT_","FDmatrix",FinDiff::FDmatrix,"exact","allow inexact stensils");
}

void readExtra(ReadInput &Inp){
    PotSolid::read(Inp);
}

void PlotFunctions(ReadInput & Inp){
    vector<string> algs;
    ReadInput::main.read("PlotFunction","algebra",algs,"","Algebra's to evaluate and plot");
    double qmin,qmax;
    ReadInput::main.read("PlotFunction","lower",qmin,"0","lower end of plot interval");
    ReadInput::main.read("PlotFunction","upper",qmax,"0","upper end of plot interval");
    int npoint;
    ReadInput::main.read("PlotFunction","points",npoint,"201","number of plot points");

    if(algs.size()==0)return;

    vector<vector<double> > cols(algs.size()+1);
    for(int k=0;k<npoint;k++)cols[0].push_back(qmin+((qmax-qmin)*k)/(npoint-1));
    for(int k=0;k<algs.size();k++){
        Algebra a(algs[k]);
        if(not a.isAlgebra())ABORT("malformed algebra string: "+algs[k]+" "+a.failures);
        for(int l=0;l<cols[0].size();l++)cols[k+1].push_back(a.val(cols[0][l]).real());
    }

    AsciiFile f(Inp.outputTopDir()+"funcs");
    f.writeComments(algs);
    f.writeCols(cols);
    PrintOutput::message("function plots on "+f.name());
}

void GetDiscInSubregion(Discretization *&D, vector<DiscretizationSurface *> &srcSurf, int UnboundDOF, int Subregion, std::vector<double> Radii){
    // setting manually. Can be smarter!
    switch(D->continuityLevel.size()){
    case 1:
        if(UnboundDOF==0){}
        else ABORT("Unknown Case");
        break;
    case 2:
        if(UnboundDOF==0){}
        else if(UnboundDOF==1){
            LOG_PUSH("DiscretizationSurface");
            srcSurf.push_back(new DiscretizationSurface(D,Radii,Subregion));
            LOG_POP();
            LOG_PUSH("factoryTsurff");
            srcSurf.back()->print();
            Discretization * A = DiscretizationTsurffSpectra::factoryTsurff(srcSurf.back(),ReadInput::main);
            LOG_POP();
            D = A; // Overwriting the pointer, original D is still accessible as parent
        }
        else ABORT("Case Unknown");
        break;
    default:
        ABORT("Case not implemented");
    }
}

// transform Vecs such that they solve the eigenproblem on the unscaled region
void diagonalizeUnscaled(const OperatorTree & Ham,const OperatorTree & Ovr,
                         const vector<Coefficients*> & In,vector<Coefficients> & Out){
    Eigen::MatrixXcd h(In.size(),In.size());
    Eigen::MatrixXcd o(In.size(),In.size());
    Coefficients hamV(Ham.iIndex),ovrV(Ovr.iIndex),tmp(Ovr.jIndex);
    for(int k=0;k<In.size();k++){
        tmp=*In[k]; // Ham only matches first block of Vecs
        Ham.apply(1.,tmp,0.,hamV);
        Ovr.apply(1.,tmp,0.,ovrV);
        for (int l=0;l<=k;l++){
            h(l,k)=In[l]->innerProductUnscaled(&hamV);
            o(l,k)=In[l]->innerProductUnscaled(&ovrV);
            h(k,l)=conj(h(l,k));
            o(k,l)=conj(o(l,k));
        }
    }

    Eigen::GeneralizedSelfAdjointEigenSolver<Eigen::MatrixXcd> slv;
    slv.compute(h,o);
    Out.clear();
    for(int k=0;k<In.size();k++){
        Out.push_back(Coefficients(Ham.iIndex));
        for(int l=0;l<In.size();l++)
            Out[k].axpy(slv.eigenvectors()(l,k),In[l]);
    }
}


void PrintShow(bool showMatrices, bool printMatrices, bool showOperators,
               OperatorTree & PropOper, OperatorTree & InitialOper,
               vector<OperatorAbstract*> PrintOps)
{
    if(not showMatrices and not printMatrices and not showOperators)return;

    vector<const OperatorAbstract*> showOp;

    cout<<"Index of full operator\n"<<PropOper.iIndex->str()<<endl;
    if(InitialOper.iIndex==PropOper.iIndex)
        cout<<"Constrained index for initial state calculation\n"<<InitialOper.iIndex->str()<<endl;


    // compile list of all operators to show
    showOp.push_back(PropOper.iIndex->overlap());
    if(InitialOper.iIndex!=PropOper.iIndex)showOp.push_back(InitialOper.iIndex->overlap());
    showOp.push_back(&PropOper);
    showOp.push_back(&InitialOper);

    // create matrices
    for(unsigned int k=0;k<showOp.size() and (showMatrices or printMatrices);k++){
        std::vector<complex<double> > cmat;
        if(k>0 and showOp[k]->iIndex->axisSubset()=="Subspace&Complement"){
            OperatorSubspace opSub(showOp[k]);
            OperatorAbstract* opA=dynamic_cast<OperatorAbstract*>(&opSub);
            opA->matrix(cmat);
        }
        else
            showOp[k]->matrix(cmat);
        UseMatrix mat;
        mat=UseMatrix::UseMap(cmat.data(),showOp[k]->iIndex->sizeCompute(),showOp[k]->jIndex->sizeCompute());
        mat.purge();
        if(showMatrices)mat.show(Str(showOp[k]->name)+showOp[k]);
        if(printMatrices)mat.print(Str(showOp[k]->name)+showOp[k]);
        cout<<"matrix type "<<mat.purge().type(0.2)<<endl;

        if(k==0){
            UseMatrix eval;
            mat.eigenValues(eval,UseMatrix::Identity(mat.rows(),mat.cols()));
            eval.transpose().print("eval1");
            for(int k=0;k<eval.size();k++){
                if(eval(k).real()<1.e-15){
                    mat.purge().print();
                    ABORT("non-positive overlap");
                }
            }
        }
    }

    PrintOutput::warning("print/show of PrintOps temporarily out of service");
    if(ReadInput::main.flag("DEBUGexpec","check expectation value matrix")){
        unsigned int k=0;
        for(;k<PrintOps.size();k++){
            UseMatrix mat;
            PrintOps[k]->matrixAdd(1.,mat);
            cout<<"shape "<<mat.strShape()<<endl;
            if(not mat.isHermitian())ABORT("not hermitian: "+PrintOps[k]->name);
            else PrintOutput::message("is hermitian: "+PrintOps[k]->name);
        }
        exit(0);
    }

    if(showOperators){
        ABORT("cannot propagate with input flags -showOperators or -printMatrices");
    }

}

void SetOutput(TimePropagatorOutput & out,double tPrint, double tStore, bool surfAscii,
               Discretization* D, vector<DiscretizationSurface*> DiscSurf, std::string Region, const PlotCoefficients *plotC,
               OperatorAbstract* grad, vector<OperatorAbstract*> PrintOps, std::string expecDef,
               string ChanAxis, string Chandef, OperatorAbstract* chanMap)
{

    // channel surface maps
    vector<const OperatorAbstract*> wOps;
    if(ChanAxis==Chandef)
        for(unsigned int c=0;c<DiscSurf.size();c++)wOps.push_back(DiscSurf[c]->mapFromParent());
    else if(chanMap!=0)
        wOps.push_back(chanMap);

    // if defined, write the gradient operator
    if(grad!=0)wOps.push_back(grad);
    string expecFile;
    if(expecFile != "" and PrintOps.size()!=0)
        expecFile=ReadInput::main.output()+"expec";
    else
        expecFile="";
    if(DiscSurf.size() == 1 and MPIwrapper::Size() > 1)
        out=TimePropagatorOutput(PrintOps,expecFile,tPrint,ReadInput::main.output(),plotC,wOps,tStore,surfAscii);
    else
        out=TimePropagatorOutput(PrintOps,expecFile,tPrint,ReadInput::main.output(),plotC,wOps,tStore);
   }

void ComputeEigenvalues(std::string Select, Plot& plot, std::vector<OperatorAbstract *> PrintOps, std::string Method)
{
    // calculate eigenvalues
    vector<complex<double> > eval;
    vector<Coefficients*> evec;
    EigenSolverAbstract::readControls(ReadInput::main);


    AsciiFile eig(ReadInput::main.output()+"eig",3,", ");

    // eigenvalue problem
    PrintOutput::paragraph();
    PrintOutput::title("EIGENVALUES  ==  "+Select);

    // for single set of parameter, get eigenvalues and vectors
    int nEv=INT_MAX;
    if(Select.find("[")!=string::npos)nEv=tools::string_to_int(tools::stringInBetween(Select,"[","]"));
    if(EigenSolverAbstract::method=="NonLin"){
        EigenSolverNonLin slv_nonlin(-DBL_MAX,DBL_MAX,nEv,true,false,false,"Lapack");
        slv_nonlin.compute(PrintOps[1],PrintOps[1]->iIndex->ovrNonSingular());
        //if(slv_nonlin.eigenvalues().size()<2000 and not tRecX::off("mainEigenvalues"))slv_nonlin.verify();
        slv_nonlin.select(Select);
        eval=slv_nonlin.eigenvalues();
        evec=slv_nonlin.rightVectors();
    } else {
        EigenSolver slv(-DBL_MAX,DBL_MAX,nEv,true,false,false,EigenSolverAbstract::method);
        if(PrintOps[1]->iIndex->axisSubset()=="Subspace&Complement")
        {
            OperatorSubspace op(PrintOps[1]);
            slv.compute(&op,PrintOps[1]->iIndex->ovrNonSingular());
            if(slv.eigenvalues().size()<2000 and not tRecX::off("mainEigenvalues"))slv.verify();
        }
        else {
            slv.compute(PrintOps[1],PrintOps[1]->iIndex->ovrNonSingular());
            if(slv.eigenvalues().size()<2000 and not tRecX::off("mainEigenvalues"))slv.verify();
        }

        slv.select(Select);
        eval=slv.eigenvalues();
        evec=slv.rightVectors();
    }

    // verify eigenvectors
    PrintOutput::newRow();
    PrintOutput::rowItem("");
    PrintOutput::rowItem("real");
    PrintOutput::rowItem("imag");
    for(int n=2;n<PrintOps.size();n++)
        PrintOutput::rowItem(PrintOps[n]->name);
    for (unsigned int k=0;k<eval.size();k++){
        PrintOutput::newRow();
        PrintOutput::rowItem(k);
        PrintOutput::rowItem(eval[k].real(),12);
        PrintOutput::rowItem(eval[k].imag(),12);
        for(int n=2;n<PrintOps.size();n++){
            PrintOutput::rowItem(PrintOps[n]->matrixElement(*evec[k],*evec[k]).real());
        }
    }

    if(eval.size()==0){
        PrintOutput::newRow();
        PrintOutput::rowItem(" -- NONE --");
    }


    // single set of eigenvalues, standard kind of output
    vector<string> com;
    com.push_back("selected Eigenvalues, criterion = "+Select);
    com.push_back("");
    com.push_back("  Real(E)     Imag(E) ");
    vector<vector<double> > cols(2);
    for (unsigned int n=0;n<eval.size();n++){
        cols[0].push_back(real(eval[n]));
        cols[1].push_back(imag(eval[n]));
    }
    eig.writeComments(com);
    eig.writeCols(cols,10);
    PrintOutput::message("All "+tools::str(eval.size())+" eigenvalues on "+ReadInput::main.output()+"eig",0,true);

    // plot a few eigenvectors for checking
    if(plot.dimension()>0)PrintOutput::message("Nth state on file "+ReadInput::main.output()+"stateN");
    int dig=2;
    //dig=(int)log10(min(double(evec.size(),100.)));
    std::cout<< "start plot"<< std::endl;
    for(unsigned int k=0;k<min(int(evec.size()),100);k++)
        plot.plot(*evec[k],ReadInput::main.output()+"state"+tools::str(k,dig,'0'));

}

static ProjectSubspace * projectPrecon(const Index* Idx, OperatorTree* OpProj, double cutE){
    if(OpProj==0)return 0;
    EigenSolver slv(cutE,DBL_MAX,true,true);
    slv.parallel(true);
    slv.compute(OpProj);
    vector<Coefficients*>vecs(slv.rightVectors()),dual(slv.dualVectors());

    const BasisOrbital* b=0;
    if(Idx->axisName()=="Subspace&Complement"){
        b=dynamic_cast<const BasisOrbital*>(Idx->child(0)->basisAbstract());
        // include Subspace orbitals into list for projection
        for(int k=0;k<b->size();k++){
            vecs.insert(vecs.begin(),new Coefficients(Idx,0.));
            dual.insert(vecs.begin(),new Coefficients(Idx,0.));
            *vecs[0]->child(0)=*b->orbital(k);
            Coefficients vConj(*vecs[0]);
            vConj.conjugate();
            Idx->overlap()->apply(1.,vConj,0.,*dual[0]);
        }
        // orthonormalize high-energy projection to lowest orbitals
        EigenSolver::orthonormalize(dual,vecs);
    }

    for(int k=0;b!=0 and k<b->size();k++)delete vecs[k],dual[k];

    return vecs.size()>0 ? new ProjectSubspace(vecs,dual) : 0;
}

DerivativeFlat* SetDerivative(const Discretization * D, OperatorTree* PropOper, OperatorTree* HamOper, OperatorTree * HamInitial, double applyThreshold, double cutE,
                              string preconDef, string projConstraint,
                              const vector<TsurffSource*> & source_terms)
{
    // check for mixed gauge, add zero into projector
    vector<double>zeroBoundary;
    if(cutE<DBL_MAX/2. and Algebra::isSpecialConstant("Rg")){
        double rG=Algebra("Rg").val(0.).real();
        if(rG!=0. and zeroBoundary.size()==tools::locateElement(zeroBoundary,rG)){
            zeroBoundary.push_back(rG);
            PrintOutput::DEVmessage("gauge radius Rg="+tools::str(rG)+" will be exempted from high energy projection");
            projConstraint=Str("ZeroBoundary=")+rG;
        }
    }

    DiscretizationSpectral* proDisc=0;
    ProjectSubspace* projSub=0;
    if(cutE<DBL_MAX/2){

        if(false and D->idx()->hierarchy().find(".k")!=string::npos){
            PrintOutput::DEVwarning("spectral constraint does not work (yet) for regions - removed ("+D->idx()->hierarchy()+")");
        }
        else {


            OperatorTree* opProj;
            if(not OperatorDefinition(preconDef).isSeparable()){
                string def=OperatorDefinitionNew(preconDef,D->idx()->hierarchy());
                if(PropOper==0)
                    opProj=0;
                else if(HamInitial!=0 and HamInitial->def()==def and projConstraint=="")
                    opProj=HamInitial;
                else if(HamOper!=0 and HamOper->def()==def and projConstraint=="")
                    opProj=HamOper;
                else {
                    opProj = new OperatorTree("Precondition",OperatorDefinitionNew(preconDef,D->idx()->hierarchy()),D->idx(),D->idx());
                }
            }

            // here we need some hacking for now:
            // SpectralProduct and projConstraint is not supported by ProjectSubspace
            if(OperatorDefinition(preconDef).isSeparable() or projConstraint!="")
            {
                LOG_PUSH("SpectralProjection");
                if(OperatorDefinition(preconDef).isSeparable()){
                    LOG_PUSH("DiscretizationSpectralProduct");
                    proDisc=new DiscretizationSpectralProduct(D,preconDef,cutE,DBL_MAX,false);
                    LOG_POP();
                }else {
                    // Spectral constraint for surf and spec only if using product projection
                    if(D->idx()->hierarchy().find("surf")==string::npos and D->idx()->hierarchy().find("spec")==string::npos){
                        const OperatorAbstract * opView=ConstrainedView::factory(opProj,projConstraint);
                        proDisc=new DiscretizationSpectral(D,opView,cutE,DBL_MAX,false);
                        if(opProj!=HamInitial and opProj!=HamOper)delete opProj; // a special precondition operator was set up
                    }
                }
                LOG_POP();

                if(proDisc!=0 and proDisc->idx()!=0 and proDisc->idx()->sizeStored()>0){
                    std::string sizes;
                    if(dynamic_cast<DiscretizationSpectralProduct*>(proDisc)){
                        for(auto& factor: dynamic_cast<DiscretizationSpectralProduct*>(proDisc)->factors){
                            sizes+=" "+std::to_string(factor->idx()->sizeStored());
                        }
                    }else{
                        sizes = " "+std::to_string(proDisc->idx()->sizeStored());
                    }

                    PrintOutput::message("high energy projector size ="+(Str("","+")+sizes)+"/"+std::to_string(D->idx()->sizeStored())
                                         +" at cutEnergy="+tools::str(cutE)+", constraints: "+projConstraint);

                } else {
                    delete proDisc;
                    proDisc=0;
                }
            }
            else {
                projSub=projectPrecon(D->idx(),opProj,cutE);
            }
        }
    }
    return projSub==0 ? new DerivativeFlatInhomogeneous(PropOper,applyThreshold,proDisc,source_terms):
                        new DerivativeFlatInhomogeneous(PropOper,applyThreshold,projSub,source_terms);
}

void SetOperators(std::string MainHierarchy,
                  string hamDef, string intDef, string iniDef, string inFluxDef, vector<string> dipNames, string expecDef,
                  Discretization* D,
                  std::shared_ptr<OperatorTree> & PropOp,
                  std::shared_ptr<OperatorTree> & HamOp,
                  std::shared_ptr<OperatorTree> & InitialOper, vector<OperatorAbstract*> & PrintOps)
{
    const Index* dIdx=D->idx(); // shorthand

    if(PrintOps.size()>0)ABORT("need to enter with emtpy list of vectors");

    // check for correctly defined gauge radius
    if(Algebra::isSpecialConstant("Rg")){
        double rG=real(Algebra("Rg").val(0.));
        for(unsigned int k=0;k<D->getAxis().size();k++){
            if(D->getAxis()[k].name=="Rn" and not D->getAxis()[k].isElementBoundary(rG))
                ABORT("gauge radius="+tools::str(rG)+" does not fall on element boundary \n"+D->getAxis()[k].str());
        }
    }

    // Hamiltonian including interaction, as used for time-propagation
    string hprop=hamDef;
    if(intDef!="")hprop+="+"+intDef;
    if(inFluxDef!="")hprop+="+"+inFluxDef;

    std::string hamDefX=OperatorDefinitionNew(hamDef,MainHierarchy).dropTerms(D->idx()->hierarchy());
    if(hamDefX!="")HamOp.reset(new OperatorTree("H0",hamDefX,dIdx,dIdx,true));

    std::string intDefX=OperatorDefinitionNew(intDef,MainHierarchy).dropTerms(D->idx()->hierarchy());
    // this step takes the largest percentage of time of operatorSetup, when using multiple nodes
    if(intDefX=="")
        PropOp=HamOp;
    else {
        PropOp.reset(new OperatorTree("Interaction",intDefX,dIdx,dIdx));
        PropOp->add(HamOp.get());
    }
    // end of time-consuming
    if(iniDef=="atBegin")
        InitialOper=PropOp;
    else if(iniDef==hamDef)
        InitialOper=HamOp;
    else {
        std::string iniDefX=OperatorDefinitionNew(iniDef,MainHierarchy).dropTerms(D->idx()->hierarchy());
        if(iniDef!="")InitialOper.reset(new OperatorTree("HamInitial",iniDefX,dIdx,dIdx));
    }
    if(expecDef!="suppress"){
        if(dIdx->overlap()!=0)PrintOps.push_back(const_cast<OperatorAbstract*>(dIdx->overlap()));
        // interaction-free Hamiltonian
        if(HamOp!=0)PrintOps.push_back(HamOp.get());
        // dipole expectation values (if any)
        for(unsigned int k=0;k<dipNames.size();k++){
            string s=dipNames[k];
            if(s.find(":")!=string::npos){
                PrintOps.push_back(new OperatorTree(s.substr(0,s.find(":")),OperatorDefinitionNew(s.substr(s.find(":")+1),MainHierarchy).dropTerms(D->idx()->hierarchy()),dIdx,dIdx));
            } else {
                PrintOps.push_back(new OperatorTree(dipNames[k],OperatorDefinitionNew("<<"+dipNames[k]+">>",MainHierarchy).dropTerms(D->idx()->hierarchy()),dIdx,dIdx));
            }
        }

        // for initial (main) run: add operators to list of expectation values
        if(MainHierarchy==dIdx->hierarchy()){
            vector<string> expDef,sep;
            tools::splitString(expecDef,",",expDef,sep,"(|<[",")>>]");
            for(unsigned int k=0;k<expDef.size();k++){
                string enam="Exp"+tools::str(k),edef=expDef[k];
                size_t pos=expDef[k].find(":");
                if(pos!=string::npos){
                    enam=expDef[k].substr(0,pos);
                    edef=expDef[k].substr(pos+1);
                }
                if(expDef[k]=="<<HartreeFock>>")
                    PrintOps.push_back(new OperatorHF("CoulombEE",D->idx(),D->idx()));
                else {
                    OperatorDefinitionNew def(edef,MainHierarchy);
                    if(def!="")PrintOps.push_back(new OperatorTree(enam,def.dropTerms(D->idx()->hierarchy()),dIdx,dIdx));
                }
            }
        }
    }
}


} // end namespace tRecX
