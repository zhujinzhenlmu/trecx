// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
// read ionic matrix

// set up ionic propagator

// back-propagate ionic states

// create channels directories

// write dummy inpc files into channel directories

// read surface

// advance ionic channels

// linearly combine and write into channel directories


