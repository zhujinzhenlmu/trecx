// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef SPHERICALHARMONICREAL_H
#define SPHERICALHARMONICREAL_H

#include "abort.h"
#include <complex>
#ifdef _USE_BOOST_
#include "boost/math/special_functions.hpp"
#endif

/// \ingroup Functions
class SphericalHarmonicReal
{
public:
    SphericalHarmonicReal(){}
    double operator()(int L, int M, double Theta, double Phi){
#ifdef _USE_BOOST_
        if(M==0)return std::real(boost::math::spherical_harmonic(L,abs(M), Theta, Phi));
        std::complex<double> plus=boost::math::spherical_harmonic(L, abs(M), Theta, Phi);
        std::complex<double> minu=boost::math::spherical_harmonic(L,-abs(M), Theta, Phi);
#else
        std::complex<double> plus;
        DEVABORT("no-boost");
#endif
        if(M==0)return real(plus);
        if(M>0)return std::real(plus)*sqrt(2.);
        return std::imag(plus)*sqrt(2.);
    }
};

#endif // SPHERICALHARMONICREAL_H
