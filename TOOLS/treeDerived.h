#ifndef TREEDERIVED_H
#define TREEDERIVED_H

#include "tree.h"

template <typename T>
class TreeDerived: public Tree<T>{
public:
    TreeDerived(const T* Parent=0):Tree<T>(Parent){} ///< elementary constructor
    TreeDerived(const TreeDerived<T> &Other, bool View=false); ///< (deep) copy constructor
    TreeDerived<T> & operator=(const TreeDerived<T> & Other); ///< (deep) assignement operator
};
#endif // TREEDERIVED_H
