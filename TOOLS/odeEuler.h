// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef ODEeuler_H
#define ODEeuler_H

#include "odeStep.h"

#include <vector>
#include <complex>
#include "abort.h"
#include "unistd.h"
#include "str.h"
#include <memory>

/// explicit Euler
template<class Der,class V>
class OdeEuler: public OdeStep<Der,V>{
    using OdeStep<Der,V>::derOde; // C++? surprising it does not know where it is derived from
    std::unique_ptr<V> lhs;
public:
    virtual ~OdeEuler(){}

    /// classical euler scheme
    OdeEuler(Der*D):OdeStep<Der,V>("euler",D){lhs.reset(new V(derOde->lhsVector()));}

    /// one step explicit euler
    V &step(V &Vec, double Tstart, double  Tstep){
        derOde->update(Tstart);
        *lhs=Vec;
        derOde->apply(Tstep,*lhs,1.,Vec);
        return Vec;
    }
    /// return model vector argument of ODEstep
    const V & modelVec() const {return*derOde->rhsVector();}

    unsigned int consistencyOrder() const {return 1;}
    unsigned int nApplyStep() const {return 1;}
    double safetyFactor() const {return 0.5;}
};

#endif // ODEeuler_H
