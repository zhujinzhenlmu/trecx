#ifndef ALGEBRAMULTI_H
#define ALGEBRAMULTI_H

#include <memory>
#include <complex>
#include "algebra.h"

/** \ingroup Functions */

///@brief multi-argument algebra
class AlgebraMulti : public Algebra
{
protected:
    std::string _coors; /// set to "*".. for any coordinate, else, e.g., "Eta,Rn", accepts Eta,Rn, but also Eta1,Rn1 etc (numbers must match)
    std::vector<const Algebra*> _arg;
    virtual void checkArgs(const std::vector<std::complex<double> > Q) const;
public:
    static bool isAlgebraMulti(std::string Definition);
    static const AlgebraMulti * factory(const std::string Definition);
    AlgebraMulti(std::string Definition /** func(arg1,arg2,...)[:var1,var2..] argN is single-variable Algebra's of varN, varN defaults to QN */
                 ,std::string const Coors);
    virtual std::complex<double> val(const std::vector<std::complex<double> > Q) const=0;
};

/// trivial example for a multi-variate function
class AlgebraSum: public AlgebraMulti{
public:
    AlgebraSum(std::string Definition):AlgebraMulti(Definition,"*"){}
    std::complex<double> val(const std::vector<std::complex<double> > Q) const {
        std::complex<double> s(0.);
        for(int k=0;k<Q.size();k++)s+=_arg[k]->val(Q[k]);
        return s;
    }
};

/// trivial example for a multi-variate function (asymmetric harmonic oscillator)
class AlgebraExternalHO: public AlgebraMulti{
public:
    AlgebraExternalHO(std::string Definition):AlgebraMulti(Definition,"Eta,Rn"){}
    std::complex<double> val(const std::vector<std::complex<double> > Q) const{
        checkArgs(Q);
        return Q[1]*Q[1]*(-Q[0]*Q[0]+2.);
    }
};

/// fill as needed
class AlgebraExternal: public AlgebraMulti{
    std::shared_ptr<const Algebra> trunc;
    double dist,screen;
public:
    AlgebraExternal(std::string Definition);
    std::complex<double> val(const std::vector<std::complex<double> > Q) const;
};

/// fill as needed
class AlgebraCO2Pot: public AlgebraMulti{
    std::shared_ptr<const Algebra> trunc;
    double bCO,screenC,screenO,alfaCharge;
public:
    AlgebraCO2Pot(std::string Definition);
    std::complex<double> val(const std::vector<std::complex<double> > Q) const;
};

/// fill as needed
class AlgebraCO2Trunc: public AlgebraMulti{
    std::shared_ptr<const Algebra> trunc;
    std::shared_ptr<const Algebra> smooth;
    std::shared_ptr<const AlgebraCO2Pot> co2;
    double smoothR;
public:
    AlgebraCO2Trunc(std::string Definition);
    std::complex<double> val(const std::vector<std::complex<double> > Q) const;
};

#endif // ALGEBRAMULTI_H
