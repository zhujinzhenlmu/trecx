// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license

#ifndef PRINTOUTPUT_H
#define PRINTOUTPUT_H
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include "timer.h"
#include "str.h"


/// \ingroup IO
/// \brief unified formated output for project
///
/// output is by default to screen, can be duplicated to file, and can be suppressed
/// by specifying the "-noScreen" flag
class PrintOutput
{
public:

    static const std::string outExtension;   ///< default extension to use for output files
    static void set(std::string File);       ///< direct output to File (for direct "cout" to cout)
    static bool noScreenFlag();                 ///< check for -noScreen flag
    static void off(){_off=true;}            ///< switch off all output from PrintOutput
    static void header(std::string File=""); ///< write header to File, File=="": do not change current
    static void title(std::string Title);    ///< write a title over group of outputs
    static void subTitle(std::string Sub);   ///< write subtitle over group of outputs
    static void message(std::string Mess, std::ostream *Ostr=0, bool Final=false, int Limit=1); ///< send a message to output
    static void terminationMessages(std::ostream *Ostr=0); ///< reapeat messages listed as "Final=true"
    static void verbatim(std::string Value,std::ostream *Ostr=0); ///< put into output stream, formatting completely in Value
    static void progressStart(std::string Mess); ///< append Mess to line, flush
    static void progressStop(std::string Mess="",std::ostream *Ostr=0); ///< append Mess to line, flush
    static void progress(std::string Mess,std::ostream *Ostr=0); ///< append Mess to line, flush
    static void warning(std::string Mess, int Limit=50, std::ostream *Ostr=0); ///< send a message to output (Limit<0: warn only after |Limit| calls)
    static void warningList(); ///< send a message to output
    static void paragraph();                                    ///< end group of outputs with empty line
    static void newLine();                                      ///< end group, start new line
    static void lineItem(std::string Name,double      Value,std::string Subtitle="",unsigned int Digits=4); ///< add double item to line of output
    static void lineItem(std::string Name,int         Value,std::string Subtitle=""); ///< add int item to line of output
    static void lineItem(std::string Name,std::string Value,std::string Subtitle=""); ///< add string item to line of output
    static void tableColumn(std::string Name,std::vector<double>      Values,std::string Subtitle="");
    static void tableColumn(std::string Name,std::vector<int>         Values,std::string Subtitle="");
    static void tableColumn(std::string Name,std::vector<std::string> Values,std::string Subtitle="");
    static void tableColumn(std::string Name,std::string Subtitle=""); ///< start new column
    static void newRow();                   ///<open a new row in table
    static void rowItem(double Value, unsigned int Width=4); ///< append double item to current row
    static void rowItem(       int Value);  ///< append int item to current row
    static void rowItem(unsigned int Value);///< append unsigned int item to current row
    static void rowItem(std::string Value); ///< append string item to current row
    static void flush(); ///< write current output, without terminating
    static void end(); ///< flush and explicitly terminate any set of outputs (line,table...)
    static std::map<std::string,unsigned int>warningCount;
    static std::map<std::string,unsigned int>messageCount;
    static std::vector<std::string>finalMessage;

    static std::ostream * output(){if(sOut!=0)return sOut; return &std::cout;} ///< return output stream (file or cout)

    static void outputLevel(std::string Level);
    static void timerWrite(); ///< write timer out to output stream(s)
    static std::string level;

    static void DEVmessage(std::string Mess, std::ostream *Ostr=0, bool Final=false);///< message for developers
    static void DEVwarning(std::string Mess, int Limit=50, std::ostream *Ostr=0);///< warning for developers

private:
    /// a simple tee-stream with limited functionality for small amounts of output
    class DualStream {
    public:
        DualStream(std::ostream * Os1, std::ostream * Os2=0);// : os1(Os1), os2(Os2) {if(MPIwrapper::Rank)}
        template<class T>
        DualStream& operator<<(const T& x) {
            if(not _off){
                *os1 << x<<std::flush;
                if(os2!=0)*os2 << x<<std::flush;
            }
            return *this;
        }
    private:
        std::ostream * os1;
        std::ostream * os2;
    };

    static DualStream * out;
    static std::ofstream * sOut;
    static std::ofstream * sMon;
    static std::string fileOut;
    static std::string monMessage;

    static unsigned int lengthLine;
    static unsigned int namesTab;
    static std::string indent;
    static std::string titleSeparator;
    static std::string itemSeparator;
    static std::string nameSeparator;
    static std::string columnSeparator;
    static std::string namesEnd;
    static std::vector<std::vector<std::string> > table;
    static std::vector<unsigned int> tableWidth;
    static std::string namesLine,valuesLine;

    static unsigned int row;
    static unsigned int col;

    static bool _off;

};

#endif // PRINTOUTPUT_H
