// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef ODERK4_H
#define ODERK4_H

#include "odeStep.h"

#include <vector>
#include <complex>
#include "abort.h"
#include "unistd.h"
#include "str.h"


#include "timer.h"
TIMER(rk4,)
TIMER(rk4a,)
TIMER(rk4b,)
TIMER(rk4c,)
TIMER(rk4d,)

template<class Der,class V>
class OdeRK4: public OdeStep<Der,V>{
    using OdeStep<Der,V>::derOde; // C++? surprising it does not know where it is derived from
    std::vector<double> a,b,c;
    V *vec0,*kCur,*aux;
public:
    virtual ~OdeRK4(){
        if(aux!=kCur)delete aux;
        delete kCur;
        delete vec0;
    }

    /// classical RK4 scheme
    OdeRK4(Der*D):OdeStep<Der,V>("RK4",D){
        vec0=new V(D->lhsVector());
        kCur=new V(D->lhsVector());

        if(derOde->applyAlias())aux=kCur;
        else aux=new V(D->lhsVector());

        a.assign(4,1./2.);
        b.assign(4,1./3.);
        c.assign(4,1./2.);
        a[0]=0.;a[3]=1.;
        b[0]=1./6.;b[3]=1./6.;
        c[0]=0.;c[3]=1.;
    }

    /// one step of the classical RK4
    V &step(V &Vec, double Tstart, double  Tstep){
        STARTDEBUG(rk4);
        *vec0=Vec;

        for (unsigned int i=0;i<4;i++){
            STARTDEBUG(rk4a);
            kCur->axpy(1.,*vec0,Tstep*a[i]);
            STOPDEBUG(rk4a);
            STARTDEBUG(rk4d);
            derOde->update(Tstart+Tstep*c[i],kCur);      // set time to t0+h*c[i]
            STOPDEBUG(rk4d);
            STARTDEBUG(rk4b);
            derOde->apply(1.,*kCur,0.,*aux);
            STOPDEBUG(rk4b);
            STARTDEBUG(rk4c);
            std::swap(kCur,aux);
            Vec.axpy(Tstep*b[i],*kCur,1.);  // y[i+1]=y[i]+h*b[i]*f(t0 + h*c[i+1],y0+h*a[i+1,i]*k[i]), y[4]=final
            STOPDEBUG(rk4c);

        }

        OdeStep<Der,V>::nCallsStep=4;
        OdeStep<Der,V>::nCalls+=4;
        STOPDEBUG(rk4);
        return Vec;
    }

    /// return model vector argument of ODEstep
    const V & modelVec() const {return *kCur;}

    unsigned int consistencyOrder() const {return 4;}
    unsigned int nApplyStep() const {return 4;}
    double safetyFactor() const {return 0.5;}
};

#endif // ODERK4_H
