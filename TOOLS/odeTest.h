// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef ODETEST_H
#define ODETEST_H

/// tests for class OdeStep type classes
#include "linSpaceMap.h"
#include "vectorComplex.h"

class VectorComplex;
class TestDer:public VectorComplex,public LinSpaceMap<VectorComplex>{
public:
    TestDer(const VectorComplex & Dat, const VectorComplex &Ht, const VectorComplex & Vec);
    void apply(std::complex<double> A, const VectorComplex &Vec, std::complex<double> B, VectorComplex &Y) const;
    void update(double Time, const std::vector<std::complex<double>> & Parameters=std::vector<std::complex<double>>());
    void update(double Time, const VectorComplex* CurrentVec);
    const VectorComplex & lhsVector() const;
    const VectorComplex & rhsVector() const {return rhsVector();}
private:
    double tCurr;
    VectorComplex dat,vec,ht;
};

class OdeTest{
public:
    static void SIA();
};

#endif // ODETEST_H

