#ifndef TIMECRITICAL_H
#define TIMECRITICAL_H

namespace timeCritical{
void setOn();
void setOff();
void suspend();
void resume();
bool isOn();
}


#endif // TIMECRITICAL_H
