// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef CHEMICALDATA_H
#define CHEMICALDATA_H

///\ingroup Units
///\brief some (very few) chemical data
namespace ChemDat{

const double Ip_eV_C=11.26030;
const double Ip_eV_Methane=12.61;
const double Ip_eV_Xe=12.13; ///< source: Wikipedia

} // end ChemDat

#endif // CHEMICALDATA_H
