// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef READINPUTLIST_H
#define READINPUTLIST_H

#include <string>

#include "readInput.h"

/// \ingroup IO
/// \brief read input from ReadInput-generated "inputList"
class ReadInputList: public ReadInput
{
    std::string file;
public:
    ReadInputList(std::string File);
    std::string readValue(const std::string Category, const std::string Name, const std::string Default, const std::string Docu, unsigned int Line, std::string Flag, std::string Allow);
    void write(const std::string Category, const std::string Name, unsigned int Line, std::string Value);
    void write(const std::string Flag, std::string Value);
};

#endif // READINPUTLIST_H
