// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef EIGENNAMES_H
#define EIGENNAMES_H

/// frequent eigen names (instead of "using namespace Eigen")
///
/// Eigen.3.3.4 contains Eigen::Index, which conflicts with tRecX's Index
using Eigen::Map;

// this may be not too good an idea
using Eigen::Matrix;
using Eigen::RowMajor;
using Eigen::Dynamic;

using Eigen::Vector2i;
using Eigen::Vector3i;
using Eigen::VectorXi;
using Eigen::RowVector3i;
using Eigen::RowVector4i;

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::RowVector3d;

using Eigen::MatrixXcd;
using Eigen::Vector3cd;
using Eigen::VectorXcd;
using Eigen::ArrayXcd;
using Eigen::RowVector3cd;
using Eigen::RowVectorXcd;


#endif // EIGENNAMES_H
