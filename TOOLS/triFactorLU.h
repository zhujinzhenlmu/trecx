// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef LU_H
#define LU_H
#include "triFactor.h"
#include "abort.h"

/// \ingroup Linalg
/// \brief Abstract for LU matrix factorization
class TriFactorLU: public TriFactor
{
    TriFactorLU(const TriFactorLU & other){ABORT("do not use copy constructor");}
    TriFactorLU(const UseMatrix &M, bool ImproveIteratively=false);
public:
    ~TriFactorLU();
    TriFactorLU(){shapeM=0;}
    UseMatrix & solve(const char Trans, UseMatrix &Rhs) const;
    UseMatrix inverse() const;
    std::complex<double> det() const;

    void reFactor(const UseMatrix & M);
};

#endif // LU_H
