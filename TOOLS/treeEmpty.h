#ifndef TREEEMPTY_H
#define TREEEMPTY_H

#include "tree.h"
#include "treeDerived.h"
#include "coefficients.h"

///< Emtpy tree w/o any data (for debug purposes)
class TreeEmpty: public TreeDerived<TreeEmpty>
{
public:
    TreeEmpty(const Coefficients * T)
    {
        for(int k=0;k<T->childSize();k++)
            childAdd(new TreeEmpty(T->child(k)));
    }
};

#endif // TREEEMPTY_H
