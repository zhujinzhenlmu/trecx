#ifndef FILETOOLS_H
#define FILETOOLS_H

#include <vector>
#include<string>

namespace tools
{
std::string findFirstLine(std::string File, const std::vector<std::string> & String); ///< return the first line that contains any of the strings

}




#endif // FILETOOLS_H
