// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef LABELLED_H
#define LABELLED_H

#include <string>
#include <limits.h>
#include "tools.h"

/// labels for class T: label will be created only upon first call to label()
template <typename T>
class Labelled
{
    static long _nextLabel;
    static std::map<const Labelled<T>*,long> _listOfLabels; // memory-light version
public:
    ~Labelled(){_listOfLabels.erase(this);}

    /// compose a hash string from numerical label and possible overflow
    std::string hash() const {return "#" + tools::str(label());}

    /// return label, check for possible overflow
    int label() const {
        // altough long SHOULD be default constructed to =0, we do not rely on it
        auto p=_listOfLabels.find(this);
        if(p==_listOfLabels.end()){
            if(++_nextLabel>=LONG_MAX)DEVABORT("Label overflow: Change implementation.");
            _listOfLabels[this]=_nextLabel;
            p=_listOfLabels.find(this);
        }
        if(p->second > INT_MAX)DEVABORT("Label overflow: Use hash.");
        return int(p->second);
    }
};

template<typename T>
long Labelled<T>::_nextLabel=1;

template<typename T>
std::map<const Labelled<T>*,long> Labelled<T>::_listOfLabels;

#endif // LABELLED_H
