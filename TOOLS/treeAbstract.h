// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef TREE_H
#define TREE_H

#include <vector>
#include <iostream>
#include <algorithm>

#include "tools.h"

// to be done:
// add lots of checks for in debug mode

///@brief a lean Tree class (thanks to Andreas Swoboda for the "Curiously Recurring Template Pattern" design)
///
/// Usage example in class SparseVector
template <typename T>
class TreeAbstract
{
    template <class U> friend class TreeAbstract;
    template <class U> friend class TreeDerived;

protected:
    // use of virtual functions:
    // in the template, call of virtualFunction(...) or this->virtualFuction(...) will call the base version,
    // WARNING: advice against such use, because it rarely really is the intended effect
    virtual std::string strData() const;  ///< string showing data of node
    virtual std::string strData(int Level) const; ///< string showing data of node
    virtual bool aboveChild(typename std::vector<T*>::iterator &Pos) const {return false;}///< default order in child: put "this" always last
    virtual bool matchChild(typename std::vector<T*>::iterator &Pos) const {return true;} ///< default: only identical nodes match


public:

    long diagnoseNodeCount() const; ///< total number of nodes in Tree
    size_t diagnoseSizeOf() const;
    virtual size_t diagnoseSizeOfNode() const;

    TreeAbstract(const T* Parent=0):_parent(Parent),nSib(-1),deleteChild(true){} ///< elementary constructor
    TreeAbstract(const TreeAbstract<T> &Other, bool View=false); ///< (deep) copy constructor
    TreeAbstract<T> & operator=(const TreeAbstract<T> & Other); ///< (deep) assignement operator

    T* deepCopy() const; ///< return a deep copy
    T* view() const; ///< return new tree with nodeCopy(this,view) and view on children

    TreeAbstract<T>(const TreeAbstract<T> * InTree, std::vector<unsigned int> Permute); ///< return a permuted view of a tree

    /// return a tree with levels permuted
    T & permute(std::vector<unsigned int> Perm /** permutation of levels */, T & Out, bool View=false /** =true: copy only permuted nodes */ ) const;
    virtual void purge(unsigned int Height); ///< remove branches that do not have at least one node on Level below present (virtual as some trees need special care)

    T * subTree(bool (*Select)(const T*), T *Out, bool View=false) const; ///< create a copy (or view: View=true) of a tree by criterion Select

    // various relatives
    T * root() const {if(parent()==0)return const_cast<T*>(dynamic_cast<const T*>(this));return parent()->root();} ///< first node in tree

    virtual const T * parent() const {return _parent;}          ///< parent node
    virtual const T *&parentRef() {return _parent;}          ///< reference to parent

    T * child(unsigned int N) const {return _child[N];} ///< return N'th child
    const std::vector<const T*> childVector(int PosBeg, int PosEnd) const; ///< return subrange of vector
    const std::vector<T*> childVector(int PosBeg, int PosEnd); ///< return subrange of vector
    T *& childRef(unsigned int N) {return _child[N];} ///< return reference to N'th child
    unsigned int childSize() const {return _child.size();} ///< number of sub-nodes
    T * childBack() const {return _child.back();} ///< return last child
    void * childPop() {delete _child.back(),_child.pop_back();}
    void childAdd(T* Child){_child.push_back(Child);_child.back()->parentRef()=dynamic_cast<const T*>(this);_child.back()->nSib=_child.size()-1;} ///< append sub-node
    void childView(T* Child){_child.push_back(Child);_child.back()->nSib=-1;} ///< append sub-node
    void childReplace(unsigned int N, T* Child){delete _child[N];_child[N]=Child;_child[N]->parentRef()=dynamic_cast<const T*>(this);_child[N]->nSib=N;} ///< replace child(N) with Child, data of previous child(N) is deleted
    void childErase(unsigned int N){delete _child[N];childEraseNode(N);} ///< delete child, removed child entry
    void childEraseNode(unsigned int N){_child.erase(_child.begin()+N);if(_child.size()>0)setNSib(_child)/* reset numbering*/;} ///< remove node, do not delete subtrees

    T * rightSibling() const; ///< return right sibling, =0 if none
    T * nodeRight(const T *Root=0) const;    ///< node to the right of this in subtree starting from Root (Root==0: complete tree); return 0 if last node on level; error if this not desendent of Root
    T * nodeNext(const T* Root=0) const;    ///< next right, or else, first non next lower level relative to Branch (Root==0: complete Tree); return 0 if last node on level; abort with error if this is not desendent of Branch
    T * descend(unsigned int Descend=1, const T *Branch=0) const; /// leftmost node in Branch, Decscend levels down from present, Branch==0: only in present subtree

    T* nodeAt(const std::vector<unsigned int> & Idx) const {
        if(Idx.size()==0)return const_cast<T*>(dynamic_cast<const T*>(this));
        if(Idx[0]>=_child.size())return 0;
        if(Idx.size()==1)return _child[Idx[0]];
        return _child[Idx[0]]->nodeAt(std::vector<unsigned int>(Idx.begin()+1,Idx.end()));
    }

    int nSibling() const; ///< position of node among children of its parent()
    std::vector<unsigned int> index() const; ///< sibling numbers from top to including present

    unsigned int size(unsigned int Depth) const; ///< number of nodes at Depth
    unsigned int levelSize() const; ///< number of nodes on level
    unsigned int leafSize() const; ///< number of leafs in tree
    unsigned int depth() const; ///< number of links to root
    unsigned int height() const; ///< number of links firstLeaf()
    unsigned int levelRank(const T* Parent=0) const; ///< number of nodes to the left, on present level

    bool isLeaf() const; ///< a Leaf is a tree without children
    bool isSubtree(const T* Root) const; ///< true if this is subTree of Root
    T *firstLeaf() const; ///< leaf at the left edge of tree
    T *nextLeaf(const T * Subtree=0) const; ///< next leaf (to the right) at lower edge of the tree starting from Subtree

    // for rearranging trees, class T must tell which data it considers relevant and when it is to be considered empty
    // functions below all refer to the given node, not to its relations in the tree
    virtual bool nodeEmpty() const {ABORT("for rearranging trees, define bool empty(T*) const in class T");}
    virtual bool nodeEquivalent(const T* Other) const {ABORT("for rearranging trees, define bool equivalent(T*) const in class T");}
    virtual void nodeCopy(const T* Node, bool View) {ABORT("for rearranging trees, define void nodeCopy(T*) in class T");}

    typename std::vector<T*>::iterator attach(T *Pointer,  const T & insertPath=T()); ///< attach Tree POINTER after the end of insertPath

    bool isView() const {return not deleteChild;}
    virtual std::string str(int Level=INT_MAX, int Depth=INT_MAX) const;       ///< default printing

    /// remove all sub-trees
    void clear();
    
    /// All children at a given depth below current node ordered, be sure to give an empty vector!
    void childrenAtDepth(int depth, std::vector<const T*>& children) const;

    std::vector<int> sizeSummary() const;

public:
    // virtual destructor makes sure that before this destructor is called, the destructor of the derived class is called
    // virtual is inheritable, i.e. destruction starts from the last derived class and works its way back
    ~TreeAbstract();
};

///@brief a Tree where each node carries an integer index
template<class T>
class IndexTree:public TreeAbstract<T>{
    template<class U> friend class TreeAbstract;

protected:
    // these should be used by Tree only:
    inline bool matchChild(typename std::vector<T*>::iterator &Pos) const {return idx==(**Pos).idx;}
    inline bool aboveChild(typename std::vector<T*>::iterator &Pos) const {return idx >(**Pos).idx;}

    const int idx; // do not manipulate index after construction
    inline int index() const {return idx;}

    /// advance directly for index matching (no detour through matchChild/aboveChild
    bool indexPos(int Index, unsigned int & Pos) const  {
        while( Pos < this->_child.size() and Index >this->_child[Pos]->idx)Pos++;
        return Pos < this->_child.size() and Index==this->_child[Pos]->idx;
    }

public:
    IndexTree(int Index=0):idx(Index){}
};

//=========================================================================================
//--- codes -------------------------------------------------------------------------------
//=========================================================================================

template <class T>
TreeAbstract<T>::~TreeAbstract(){
    for(unsigned int k=0;k<_child.size();k++)if(deleteChild)delete _child[k];
}

template <class T>
void TreeAbstract<T>::clear(){
    for(unsigned int k=0;k<_child.size();k++)if(deleteChild)delete _child[k];
    _child.clear();
}

template <class T>
std::string TreeAbstract<T>::indent="  ";

template <class T>
TreeAbstract<T>::TreeAbstract(const TreeAbstract<T> &Other, bool View):deleteChild(not View){ ///< (deep or view) copy constructor
    nodeCopy(dynamic_cast<T*>(this),View);
    for (unsigned int i=0; i!=Other._child.size(); ++i){
        if(View) childAdd(Other.child(i));
        else childAdd(Other.child(i)->deepCopy());
    }
}

template <class T>
void TreeAbstract<T>::purge(unsigned int Height){
    if(Height==1)return;
    for(unsigned int k=childSize();k>0;k--){
        if(child(k-1)->descend(Height-1)==0)childErase(k-1);
        else {
            child(k-1)->purge(Height-1);
        }
    }
    for(unsigned int k=childSize();k>0;k--)
        if(child(k-1)->isLeaf())childErase(k-1);
}

template <class T>
TreeAbstract<T> & TreeAbstract<T>::operator=(const TreeAbstract<T> &Other){ ///< (deep) assignment
    if(this==&Other)return *this;
    nSib=-1;
    _parent=0;
    deleteChild=true;
    for (unsigned int i=0; i!=Other._child.size(); ++i) {
        typename std::vector<T*>::iterator pos=_child.begin();
        if(childPos(Other._child[i],pos)){
            // overwrite
            delete *pos;
            *pos=new T(*Other._child[i]);
        } else {
            // insert/append new
            _child.push_back(new T(*Other._child[i]));
        }
    }
}

template <class T>
bool TreeAbstract<T>::childPos(const T* Node,typename std::vector<T*>::iterator & Pos) const{
    while( Pos < _child.end() and Node->aboveChild(Pos)) Pos++;
    return Pos < _child.end() and Node->matchChild(Pos);
}
template <class T>
const std::vector<const T*> TreeAbstract<T>::childVector(int PosBeg, int PosEnd) const {
    const std::vector<const T*> v(_child.begin()+PosBeg,_child.end()+PosEnd);
    return v;
}
template <class T>
const std::vector<T*> TreeAbstract<T>::childVector(int PosBeg, int PosEnd) {
    const std::vector<T*> v(_child.begin()+PosBeg,_child.end()+PosEnd);
    return v;
}
template <class T>
std::string TreeAbstract<T>::strData() const {return "+";}

template <class T>
std::string TreeAbstract<T>::strData(int Level) const {std::cout<<"level "<<Level<<std::endl;return strData();}

template <class T>
bool TreeAbstract<T>::isLeaf() const {return _child.size()==0;}

template<class T>
unsigned int TreeAbstract<T>::depth() const{
    if(parent()==0)return 0;
    return parent()->depth()+1;
}

template<class T>
unsigned int TreeAbstract<T>::height() const{
    if(isLeaf())return 0;
    return _child[0]->height()+1;
}


template<class T>
unsigned int TreeAbstract<T>::levelRank(const T* Parent) const {
    if(parent()==0 or this==Parent)return 0;

    unsigned int s=nSibling();
    unsigned int d=1;
		if(parent() == Parent) return s;
    const T* p=parent();
    for(; p!=0 and p->parent()!=Parent;p=p->parent()){
        for(int k=0;k<p->nSibling();k++){
            s+=p->parent()->child(k)->Tree::size(d);
        }
        d++;
    }
    if(p==0)ABORT("argument is not parent of node");
    return s;
}

template<class T>
T * TreeAbstract<T>::rightSibling() const {
    if(parent()==0)return 0;
    unsigned int n=nSibling();
    if(n==parent()->childSize()-1)return 0;
    return parent()->child(n+1);
}

template<class T>
T* TreeAbstract<T>::firstLeaf() const {
    if(isLeaf())return dynamic_cast<T*>(const_cast<TreeAbstract<T>*>(this));
    else return child(0)->firstLeaf();
}
template<class T>
T * TreeAbstract<T>::nextLeaf(const T* Subtree) const {
    if(not isLeaf())ABORT("nextLeaf can only be called by leaf");

    const T * next=nodeRight(Subtree);
    if(next!=0){
        next=next->firstLeaf();
    }
    else {
        // ascend until new subtree below Root
        next=parent();
        while(next!=Subtree and next->nodeRight()==0)next=next->parent();
        if(next==Subtree)next=0;
        if(next!=0)next=next->firstLeaf();
    }
    return const_cast<T*>(next);
}

template<class T>
unsigned int TreeAbstract<T>::levelSize() const {
    return root()->Tree::size(depth());
}

template<class T>
unsigned int TreeAbstract<T>::size(unsigned int Depth) const {
    if(Depth<2){
        if(Depth==0)return 1;
        return childSize();
    }
    unsigned int s=0;
    for(int k=0;k<childSize();k++)s+=child(k)->Tree::size(Depth-1);
    return s;
}

template<class T>
T * TreeAbstract<T>::nodeNext(const T* Branch) const {
    const T* branch=Branch;
    if(branch==0)branch=root();
    T* next=nodeRight(branch);
    if(next==0)
        next=branch->descend(depth()-branch->depth()+1,branch);
    return next;
}

template<class T>
T * TreeAbstract<T>::descend(unsigned int Descend, const T* Branch) const {

    if(Branch==0)Branch=dynamic_cast<const T*>(this);

    if(Descend==0)return dynamic_cast<T*>(const_cast<TreeAbstract<T>*>(this));
    if(childSize()>0)return child(0)->descend(Descend-1,Branch);
    if(Branch==0)return 0; // no branch specified - left edge only

    T* right=nodeRight(Branch);
    if(right!=0)return right->descend(Descend,Branch);
    return 0;
}

template<class T>
bool TreeAbstract<T>::isSubtree(const T* Root) const {
    if(Root==this)return true;
    const T * s=dynamic_cast<const T*>(this);
    while(s!=0 and s!=Root){
        s=s->parent();
    }
    return s!=0;
}

template<class T>
T * TreeAbstract<T>::nodeRight(const T* Root) const {
    if(Root==0)Root==root();
    if(parent()==0)return 0;
    if(this==Root) return 0; // at root of subtree

    T* right=rightSibling();
    if(right!=0)return right;

    unsigned int lev=0;
    const TreeAbstract<T>* up=parent();
    while(up->parent()!=0 and up->parent()->_child.back()==up){
        if(up==Root)return 0; // cannot move further up
        up=up->parent();
        if(up==0)ABORT("index "+tools::str(this)+" not in subtree: \n"+Root->Tree::str());
        lev++;
    }
    if(up==Root or up->rightSibling()==0)return 0; //
    return up->rightSibling()->descend(lev+1,Root);
}


template <class T>
int TreeAbstract<T>::nSibling() const {
    if(nSib==-1){
        if(parent()==0) const_cast<TreeAbstract<T>*>(this)->nSib=0;
        else            setNSib(parent()->_child);
    }
    return nSib;
}

template <class T>
std::vector<unsigned int> TreeAbstract<T>::index() const {
    if(parent()==0)return std::vector<unsigned int>(0);
    std::vector<unsigned int> i(parent()->index());
    i.push_back(nSibling());
    if(i.back()>1000)std::cout<<"nsib "<<nSib<<" "<<nSibling()<<std::endl;
    return i;
}

// auxiliary routine for inserting or appending an element to child
template <class T>
typename std::vector<T*>::iterator TreeAbstract<T>::insertChild(typename std::vector<T*>::iterator Pos, T* NewChild){
    typename std::vector<T*>::iterator pos;
    if(Pos==_child.end()){
        _child.push_back(NewChild);
        _child.back()->nSib=_child.size()-1;
        pos=_child.end();
        pos--;
    } else {
        pos=_child.insert(Pos,NewChild);
        setNSib(_child);
    }
    (**pos)._parent=this;
    return pos;
}

template <class T>
typename std::vector<T*>::iterator TreeAbstract<T>::attach(T *Pointer, const T &insertPath)
{
    typename std::vector<T*>::iterator pos=_child.begin();
    if(insertPath.isLeaf()){
        if(childPos(Pointer,pos))ABORT("cannot insert, position occupied:"+Pointer->str()+"\nin tree \n"+str());
        pos=insertChild(pos,Pointer);
    } else {
        // create new node and descend
        if(not childPos(insertPath._child[0],pos))pos=insertChild(pos,new T(*insertPath._child[0]));
        (**pos).attach(Pointer,*insertPath._child[0]);
    }
    return pos;
}

template <class T>
std::string TreeAbstract<T>::str(int Level, int Depth) const
{
    std::string s;
    if(parent()==0)s="|";
    if(Level==INT_MAX)s+=strData()+" ("+tools::str(this)+") "+tools::str(parent());
    else              s+=strData(Level);

    if(Depth==0)return s;

    for(unsigned int k=0;k<_child.size();k++){
        s+="\n";
        //        for(unsigned int i=0;i<depth();i++)s+=indent;
        s+=tools::str(index(),"  ")+" ";
        s+="|";
        for(unsigned int i=1;i<indent.length();i++)s+="....";
        s+=_child[k]->Tree<T>::str(Level,Depth-1);
    }
    return s;
}

/// build a copy (or View) of the tree where Select(node) is true
template <class T>
T* TreeAbstract<T>::subTree(bool (*Select)(const T *), T *Out, bool View) const {
    Out->deleteChild=not View;
    Out->nodeCopy(dynamic_cast<const T*>(this),View);
    for(unsigned int k=0;k<childSize();k++){
        if(Select(child(k))){
            Out->childAdd(new T());
            child(k)->subTree(Select,Out->childBack(),View);
        }
    }
    return Out;
}

template <class T>
T& TreeAbstract<T>::permute(std::vector<unsigned int> Permute, T& Out, bool View) const{
    T * l=descend(Permute.size());
    if(l==0)ABORT("tree height smaller than permutation size");

    //    if(not Out.root()==&Out)ABORT("provide emtpy Tree for output");

    // descend and create path to childView
    T* res=&Out;
    while (l!=0){
        T* kLevel=res;
        std::vector<unsigned int> idx(l->index());

        for(unsigned int k=0;k<Permute.size();k++){
            for(unsigned int m=kLevel->childSize();m<idx[Permute[k]]+1;m++)kLevel->childAdd(new T());
            // get node from the parent tree level corresponding to new
            T* rep=nodeAt(std::vector<unsigned int>(idx.begin(),idx.begin()+Permute[k]));

            // if node is empty, copy data from the corresponding parent node
            if(kLevel->nodeEmpty())kLevel->nodeCopy(rep,View);

            // else check data equivalence
            else if(not rep->nodeEquivalent(kLevel))
                ABORT(rep->strData()+" != "+kLevel->strData()+"\nCannot rearrange level - node data not equivalent: ");

            kLevel=kLevel->child(idx[Permute[k]]);
        }
        kLevel->deleteChild=not View; // if View, level does not delete its children (as they belong to other tree)
        if(kLevel->childSize()!=0)ABORT("tree position occupied - cannot place new node");
        kLevel->nodeCopy(l,View);
        for(unsigned int k=0;k<l->childSize();k++){
            if(View)kLevel->childView(l->child(k));
            else    kLevel->childAdd(l->child(k)->deepCopy());
        }
        l=l->nodeRight();
    }
    if(Permute.size()>0)
        res->purge(Permute.size()-1); //< last Level (=Permute.size()-1) must be non-empty
    return Out;
}

template <class T>
T* TreeAbstract<T>::deepCopy() const {
    T* t=new T();
    
    t->nodeCopy(dynamic_cast<const T*>(this),false);
    for(unsigned int k=0;k<childSize();k++){
        t->childAdd((child(k)->deepCopy()));
    }
    return t;
}
template <class T>
T* TreeAbstract<T>::view() const{
    T* t=new T();
    
    t->nodeCopy(dynamic_cast<T*>(this),true);
    deleteChild=false;
    for(unsigned int k=0;k<childSize();k++)t->childAdd(child(k));
    return t;
}

template<class T>
void TreeAbstract<T>::childrenAtDepth(int depth, std::vector<const T*>& children) const{
    if(depth==0){
        children.push_back(dynamic_cast<const T*>(this));
    }else{
        for(unsigned int i=0; i<childSize();i++){
            child(i)->childrenAtDepth(depth-1, children);
        }
    }
}

template<class T>
unsigned int TreeAbstract<T>::leafSize() const{
    if(childSize()==0) return 1;
    int res=0;
    for(int i=0; i<childSize(); i++){
        res +=child(i)->leafSize();
    }
    return res;
}

template<class T>
std::vector<int> TreeAbstract<T>::sizeSummary() const{
    std::vector<int> result(1, 1);
    for(unsigned int i=0; i<childSize(); i++){
        std::vector<int> tmp = child(i)->sizeSummary();
        for(unsigned int j=0; j<tmp.size(); j++){
            if(result.size()<=j+1) result.push_back(0);
            result[j+1] += tmp[j];
        }
    }
    return result;
}

template<class T>
size_t TreeAbstract<T>::diagnoseSizeOf() const{
    size_t siz=diagnoseSizeOfNode();
    for(int k=0;k<childSize();k++)siz+=child(k)->diagnoseSizeOf();
    return siz;
}
template<class T>
size_t TreeAbstract<T>::diagnoseSizeOfNode() const{ return sizeof *this;}

template<class T>
long TreeAbstract<T>::diagnoseNodeCount() const{
    long cnt=1;
    for (auto c: _child)cnt+=c->diagnoseNodeCount();
    return cnt;
}

#endif

