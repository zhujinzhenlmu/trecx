#ifndef EIGENTOOLS_H
#define EIGENTOOLS_H

#include "qtEigenDense.h"
#include <string>


class UseMatrix;

/// \ingroup Linalg
/// \brief Convenience routines covering features missing in Eigen
namespace EigenTools
{
bool isIdentity(const Eigen::MatrixXcd & Mat, double Eps=0.);
bool isZero(const Eigen::MatrixXcd & Mat, double Eps=0.);
std::string str(const Eigen::MatrixXcd & Mat, int Digits=2);
void purge(const Eigen::MatrixXcd &Mat, double Eps=1.e-12);
void saveAscii(std::string FileName,const UseMatrix &Mat);
void saveAscii(std::string FileName,const Eigen::MatrixXcd &Mat);
void readAscii(std::string FileName,Eigen::MatrixXcd & Mat);
bool compareMatrices(std::string FileA, std::string FileB, double Epsilon=1.e-12);
}

#endif // EIGENTOOLS_H
