// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef TRIFACTOR_H
#define TRIFACTOR_H
#include "useMatrix.h"

/// \ingroup Linalg
/// \brief base class for triangular factorizations
class TriFactor
{
    friend class UseMatrix;
public:
    virtual ~TriFactor(){delete storeLU;delete mat;}
    TriFactor():shapeM(0),storeLU(0),mat(0){}
    virtual UseMatrix inverse() const =0;
    /// overwrite Rhs with solution, also return reference to Rhs
    virtual UseMatrix & solve(const char Trans, UseMatrix & Rhs) const =0;

    /// LU factorization allows iterative improvement of given solution
    virtual void improve(const UseMatrix & M, const UseMatrix & Rhs, UseMatrix & Solution) const{}

    virtual std::complex<double> det() const =0;
    virtual void reFactor(const UseMatrix & M)=0; /// (re-)compute the factorization of M
    void reset(); /// reset (when main matrix is modifield
    bool verify(const UseMatrix & M);
protected:
    std::vector<int> pivot;
    UseMatrix::Shape * shapeM; /// pointer to the shape of the original matrix
    UseMatrix * storeLU;
    UseMatrix * mat; /// original matrix
};

#endif // TRIFACTOR_H
