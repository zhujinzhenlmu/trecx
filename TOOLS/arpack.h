// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef ARPACK_H
#define ARPACK_H

#include <string>
#include <map>
#include <complex>
#include <vector>

class Arpack;

/// @brief basic interface to the fortran77 arpack routines
class ArpackFunctions
{
    friend class Arpack;///< only Arpack classes are meant to use these routines

public:
    ArpackFunctions():tolerance(1.e-12),maxIter(1000){whichListSet();}
    ArpackFunctions( double Tolerance,  /**< accuracy control parameter */
                     int MaxIter    /**< maximal number of arnoldi iterations */,
                     bool Seriel
                     ) : tolerance(Tolerance),maxIter(MaxIter),_seriel(Seriel){whichListSet();}
private:
    void eigen(Arpack *, int K,   /**< desired number of eigenvectors */
               std::string Which, /**< wich part of the spectrum */
               bool Restart,      /**< true: iteration starts from first vector in  Rvec */
               bool computeVectors, bool Parallel=true);

    double tolerance;
    unsigned int maxIter;
    bool _seriel;

    static std::map<std::string,std::string>whichList; ///< which part of the spectrum
    void whichListSet();
};

/// @brief abstract base class for arpack eigensolver
class Arpack{
    friend class ArpackFunctions;
public:
    Arpack(double Tolerance,unsigned int MaxIter,bool Seriel=false){A=ArpackFunctions(Tolerance,MaxIter,Seriel);}
    /// apply operator to vectors
    virtual void apply(const std::complex<double> * X, std::complex<double> * Y) =0;

    /// eigenvalues as std::vector
    void eigenValues(
            std::vector<std::complex<double> > & Eval,
            unsigned int Nvec=1, ///< number of eigenvectors, =0: all
            const std::string & Which="SmallReal"
            );

    /// eigenvalues and eigenvectors as std::vector's
    void eigen(
            std::vector<std::complex<double> > & Eval,
            std::vector<std::vector<std::complex<double> > >& Rvec,
            unsigned int Nvec=1, ///< number of eigenvalues
            const std::string & Which="SmallReal",
            bool Restart=false
            );

    /// verify eigenvectors by direct application and get accuracies
    void verify();

protected:
    // internal data
    ArpackFunctions A;                       ///< interface class to F77 Arpack
    void eigen(unsigned int Nvec, std::string Which,bool Restart,bool ComputeVectors){
        A.eigen(this,Nvec,Which,Restart,ComputeVectors);
    }
    unsigned int lvec;                       ///< number of coefficients in vector
    std::vector<std::complex<double> > eval; ///< eigenvalues
    std::vector<std::complex<double> > rvec; ///< vector of right eigenvectors
};

/// @brief Example Arpack class using only standard vectors
class ArpackStandard:private Arpack {
public:
    ArpackStandard(std::vector<std::complex<double> > Mat,double Tolerance,unsigned int MaxIter)
        :Arpack(Tolerance,MaxIter),mat(Mat)
    {A=ArpackFunctions(Tolerance,MaxIter,true);lvec=(unsigned int) sqrt(double(Mat.size())+1);}
    void apply(const std::complex<double> *X, std::complex<double> * Y);

    static void test();    ///< basic test

private:
    std::vector<std::complex<double> >mat; // this is how this particular instance stores the matrix
};

#endif // ARPACK_H
