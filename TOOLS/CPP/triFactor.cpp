// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "triFactor.h"

///< detstroy the tri-diagonal factors (e.g. if parent matrix was changed)
void TriFactor::reset(){
    delete storeLU;
    storeLU=0;
    pivot.clear();
    delete shapeM;
    shapeM=0;
}


bool TriFactor::verify(const UseMatrix &M){
    UseMatrix vec=UseMatrix::Random(M.rows(),1);
    UseMatrix ve0;
    ve0=vec;
    solve('n',vec);
    ve0-=M*vec;
    if(ve0.maxAbsVal()>1.e12){
        ve0.transpose().print("error",0);
    }
    return ve0.maxAbsVal()<1.e12;
}
