#include "eigenTools.h"

#include "useMatrix.h"

#include <stdio.h>      /* fopen, fputs, fclose, stderr */
#include <iostream>
#include <fstream>
#include <sstream>
#include "abort.h"

namespace EigenTools {

bool isIdentity(const Eigen::MatrixXcd & Mat, double Eps){
    return UseMatrix::UseMap(const_cast<Eigen::MatrixXcd*>(&Mat)->data(),Mat.rows(),Mat.cols()).isIdentity(Eps);
}

bool isZero(const Eigen::MatrixXcd & Mat, double Eps){
    return UseMatrix::UseMap(const_cast<Eigen::MatrixXcd*>(&Mat)->data(),Mat.rows(),Mat.cols()).isZero(Eps);
}

std::string str(const Eigen::MatrixXcd &Mat, int Digits){
    return UseMatrix::UseMap(const_cast<Eigen::MatrixXcd*>(&Mat)->data(),Mat.rows(),Mat.cols()).str(" row/col",Digits);
}

void purge(const Eigen::MatrixXcd &Mat, double Eps){
    UseMatrix::UseMap(const_cast<Eigen::MatrixXcd*>(&Mat)->data(),Mat.rows(),Mat.cols()).purge(Eps,Eps*0.01);
}

void saveAscii(std::string FileName, const UseMatrix &Mat){
    saveAscii(FileName,Eigen::Map<Eigen::MatrixXcd>(Mat.data(),Mat.rows(),Mat.cols()));
}
void saveAscii(std::string FileName, const Eigen::MatrixXcd &Mat){
    std::ofstream stream(FileName.c_str(),std::ios::out);
    if(not stream.is_open())ABORT("failed to open write file "+FileName);
    stream<<"Info: cd"<<std::endl;
    stream<<Mat.rows()<<" "<<Mat.cols()<<std::endl;
    stream<<std::setprecision(16);
    for(int j=0;j<Mat.cols();j++)
        for(int i=0;i<Mat.rows();i++)
            stream<<i<<" "<<j<<" "<<Mat(i,j)<<std::endl;
    std::cout<<"saveAscii matrix to "+FileName<<std::endl;
    stream.close();
}

void readAscii(std::string FileName, Eigen::MatrixXcd &Mat){
    if(FileName==""){
        std::cout<<"no FileName given, enter here:"<<std::endl;
        std::cin>>FileName;
    }
    if(FileName=="")ABORT("no file name");
    std::ifstream stream(FileName.c_str(),std::ios::in);
    if(not stream.is_open())ABORT("failed to open read file "+FileName);
    std::string line;
    std::getline(stream,line);
    std::getline(stream,line);
    int rows,cols;
    std::stringstream(line)>>rows>>cols;
    Mat=Eigen::MatrixXcd::Zero(rows,cols);
    int i,j;
    while(std::getline(stream,line)){
        std::stringstream(line)>>i>>j;
        std::stringstream(line)>>i>>j>>Mat(i,j);
    }
}

bool compareMatrices(std::string FileA, std::string FileB, double Epsilon){
    Eigen::MatrixXcd A,B,C;
    readAscii(FileA,A);
    readAscii(FileB,B);
    C=A-B;
    if(A.rows()!=B.rows() or A.cols()!=B.cols()){
        std::cout<<"Fail: Matrix dimensions do not match "<<A.rows()<<"X"<<A.cols()<<" vs. "<<B.rows()<<"X"<<B.cols()<<std::endl;
        return false;
    }
    else if(not C.isZero(Epsilon)){
        for(int j=0;j<A.cols();j++)
            for(int i=0;i<A.rows();i++)
                if(std::abs(C(i,j))>std::max(1.,abs(A(i,j))+abs(B(i,j)))*Epsilon)
                    std::cout<<i<<j<<": A="<<A(i,j)<<" B="<<B(i,j)<<std::endl;
        std::cout<<str(C,0)<<std::endl; // show structure of error
        std::cout<<"FAIL: "<<A.rows()<<"x"<<A.cols()<<" matrices from "<<FileA<<" and "<<FileB<<" disagree"<<std::endl;
        return false;
    }

    std::cout<<"OK: "<<A.rows()<<"x"<<A.cols()<<" matrices from "<<FileA<<" and "<<FileB<<" agree"<<std::endl;
    return true;
}

}
