// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "gaunt.h"
#include "basisSet.h"
#include "operatorData.h"

using namespace std;

Gaunt Gaunt::main = Gaunt();

Gaunt::Gaunt():order(0),lmax(-1){}

void Gaunt::initialize(int lmax)
{
  this->lmax = lmax;
//  order = max(3,2*lmax+1);
  order = max(3,lmax+1);

  UseMatrix quad,weig;
  Passoc.clear();
  for(int m=0;m<=lmax;m++){
      BasisSetDef AssLeg(lmax+1,-1.,2.,"assocLegendre",true,true,true,Coordinate::fromString("Eta"));
      AssLeg.par={double(m)};
      BasisSet* YLM = BasisSet::get(AssLeg);

      if(m==0){
          YLM->quadRule(order,quad,weig);
          quadX = Eigen::VectorXd::Zero(quad.size());
          quadW = Eigen::VectorXd::Zero(quad.size());
          for(int i=0;i<quadX.rows();i++){
              quadX(i) = quad(i).real();
              quadW(i) = weig(i).real();
            }
        }

      UseMatrix val = YLM->val(quad);
      Eigen::MatrixXd valEig = Eigen::MatrixXd::Zero(val.rows(),val.cols());
      for(int i=0;i<val.rows();i++)
        for(int j=0;j<val.cols();j++){
            complex<double > a = val(i,j).complex();
            if(abs(imag(a))>1e-14) ABORT("Complex part in values ?? "+tools::str(imag(a)));
            valEig(i,j) = real(a);
          }
      Passoc.push_back(valEig*sqrt(1.0/(2.0*math::pi)));      // normalization for m component;
    }

  // Run the test
  test();
}

bool Gaunt::coeff_isZero(int lc, int l1, int l2, int mc, int m1, int m2)
{
  return mc!=m1+m2 or abs(l1-l2)>lc or lc>l1+l2 or abs(mc)>lc or abs(m1)>l1 or abs(m2)>l2;
}

double Gaunt::coeff(int lc, int l1, int l2, int mc, int m1, int m2)
{
  
  if(std::max(lc, std::max(l1, l2)) > lmax) initialize(std::max(lc, std::max(l1, l2)));

  // returns the gaunt coefficient
  if (mc!=m1+m2)  return 0.0;
  if (abs(l1-l2)>lc or lc>l1+l2) return 0.0;
  if (abs(mc)>lc or abs(m1)>l1 or abs(m2)>l2) return 0.0;

  int f=1;
  if (mc<0 and (-mc)%2==1) f  = -1;
  if (m1<0 and (-m1)%2==1) f *= -1;
  if (m2<0 and (-m2)%2==1) f *= -1;

  return (Passoc[abs(mc)].col(lc-abs(mc)).array()
          *Passoc[abs(m1)].col(l1-abs(m1)).array()
          *Passoc[abs(m2)].col(l2-abs(m2)).array()
          *quadW.array()
          ).sum()*2.0*math::pi*(double)f;

  //  return (Passoc[abs(mc)][lc-abs(mc)].array()*Passoc[abs(m1)][l1-abs(m1)].array()*Passoc[abs(m2)][l2-abs(m1)].array()*quadw.cast<long double>().array()).sum()*2.0*myPi*(double)f;
}

void Gaunt::test(){

//    if(MPIwrapper::isMaster()==1) cout << "gaunt test ... " << flush;
    if(this->Passoc.size()==0) return;
    double eps = 1e-12;
    int errorcount = 0;
    int shortentest=0;
    for( int m=lmax;m>=0;m--){
        if(shortentest++ > 10) break;
        for(unsigned int l=m; l<=(unsigned int)lmax; l++){
            // if(l>85) break;
            double test = coeff(l,l,0,m,m,0);
            if (abs(abs(test)-sqrt(0.25/math::pi))>eps) {
                cout<<"failed test1 in gaunt.h. m="<<m<<", l="<<l<<" off by " << abs(abs(test)-sqrt(0.25/math::pi))<<" "<<abs(test)<<" "<<sqrt(0.25/math::pi)<<endl;// exit(0);
                errorcount++;
            }
            if (abs(abs(coeff(l,l,0,-m,-m,0))-sqrt(0.25/math::pi))>eps)            {
                cout<<"failed test2 in gaunt.h. m="<<m<<", l="<<l<<" off by " << abs(abs(coeff(l,l,0,-m,-m,0))-sqrt(0.25/math::pi))<<endl; //exit(0);
                errorcount++;
            }
            if (abs(abs(coeff(0,l,l,0,m,-m))-sqrt(0.25/math::pi))>eps)            {
                cout<<"failed test3 in gaunt.h. m="<<m<<", l="<<l<<" off by " << abs(abs(coeff(0,l,l,0,m,-m))-sqrt(0.25/math::pi))<<endl;//exit(0);
                errorcount++;
            }
            if (abs(abs(coeff(0,l,l,0,-m, m))-sqrt(0.25/math::pi))>eps)            {
                cout<<"failed test4 in gaunt.h. m="<<m<<", l="<<l<<" off by " << abs(abs(coeff(0,l,l,0,-m, m))-sqrt(0.25/math::pi))<<endl;//exit(0);
                errorcount++;
            }
        }
    }
    if(errorcount>0){
        cout << endl << "GauntCoeffTable::test: long double should be giving correct results until lmax/mmax > 200." << endl;
        cout << "if shit happened at l/m around 90, this is possibly because the compiler/computer/whatever has a different (smaller: like double) definition of long double than ours ... " << endl;
        ABORT("GauntCoeffTable::test fatal");
    }
    else{
    //    if(MPIwrapper::isMaster()==1) cout << "finished" << endl;
    }

}

double Gaunt::twoYintegrals(int l1, int l2, int m1, int m2, string operat)
{

  ABORT("Need to implement");

//  BasisSet::Def AssLeg(lmax+1,-1.0,2.0,"assocLegendre",true,true,true,Coordinate::fromString("Eta"),vector<int>(0),
//                       ComplexScaling(),false,vector<double>(1,m));
//  BasisSet* YLM = BasisSet::get(AssLeg);

//  UseMatrix mat;
//  OperatorData::get(operat,vector<BasisSet*>(1,YLM),vector<BasisSet*>(1,YLM),vector<UseMatrix>(1,mat));
//  return mat().real();

//  //these are used to compute the overlaps appearing in dipol field hamiltonian
//      Matrix<long double,Dynamic, 1> temp,temp2;
//      double result = 0.0;
//      if(operat == "q") result= ( ValueOfAssLeg(m1,l1,temp).array()*normForSphericHarmonic(m1,l1)*ValueOfAssLeg(m2,l2,temp2).array()*normForSphericHarmonic(m2,l2)*quadw.cast<long double>().array()*quadx.cast<long double>().array()).sum();
//      else if(operat == "q^2") result= ( ValueOfAssLeg(m1,l1,temp).array()*normForSphericHarmonic(m1,l1)*ValueOfAssLeg(m2,l2,temp2).array()*normForSphericHarmonic(m2,l2)*quadw.cast<long double>().array()*quadx.cast<long double>().array()*quadx.cast<long double>().array()).sum();
//      else if(operat == "(1-q^2)")  result= ( ValueOfAssLeg(m1,l1,temp).array()*normForSphericHarmonic(m1,l1)*ValueOfAssLeg(m2,l2,temp2).array()*normForSphericHarmonic(m2,l2)*quadw.cast<long double>().array()*
//                                              (1.0-quadx.cast<long double>().array()*quadx.cast<long double>().array())).sum();
//      else if(operat == "|") result= ( ValueOfAssLeg(m1,l1,temp).array()*normForSphericHarmonic(m1,l1)*ValueOfAssLeg(m2,l2,temp2).array()*normForSphericHarmonic(m2,l2)*quadw.cast<long double>().array()).sum();
//      else if(operat == "(1-q^2)d") result= ( ValueOfAssLeg(m1,l1,temp).array()*normForSphericHarmonic(m1,l1)*DerivOfAssLeg(m2,l2,temp2).array()*
//                                              normForSphericHarmonic(m2,l2)*quadw.cast<long double>().array()*(1.0-quadx.cast<long double>().array()*quadx.cast<long double>().array())).sum();
//      else if(operat == "sqrt(1-q^2)") result= (ValueOfAssLeg(m1,l1,temp).array()*normForSphericHarmonic(m1,l1)*ValueOfAssLeg(m2,l2,temp2).array()*
//                                                normForSphericHarmonic(m2,l2)*quadw.cast<long double>().array()*sqrt(1.0-quadx.cast<long double>().array()*quadx.cast<long double>().array())).sum();
//      else if(operat == "sqrt(1-q^2)qd") result= (ValueOfAssLeg(m1,l1,temp).array()*normForSphericHarmonic(m1,l1)*DerivOfAssLeg(m2,l2,temp2).array()*
//                                                  normForSphericHarmonic(m2,l2)*quadw.cast<long double>().array()*quadx.cast<long double>().array()*sqrt(1.0-quadx.cast<long double>().array()*quadx.cast<long double>().array())).sum();
//      else if(operat == "sqrt(1-q^2)q") result= (ValueOfAssLeg(m1,l1,temp).array()*normForSphericHarmonic(m1,l1)*ValueOfAssLeg(m2,l2,temp2).array()*
//                                                  normForSphericHarmonic(m2,l2)*quadw.cast<long double>().array()*quadx.cast<long double>().array()*sqrt(1.0-quadx.cast<long double>().array()*quadx.cast<long double>().array())).sum();
//      else if(operat == "1/sqrt(1-q^2)") result= (ValueOfAssLeg(m1,l1,temp).array()*normForSphericHarmonic(m1,l1)*ValueOfAssLeg(m2,l2,temp2).array()*
//                                                  normForSphericHarmonic(m2,l2)*quadw.cast<long double>().array()/sqrt(1.0-quadx.cast<long double>().array()*quadx.cast<long double>().array())).sum();
//      else if(operat == "q^2/sqrt(1-q^2)") result=(ValueOfAssLeg(m1,l1,temp).array()*normForSphericHarmonic(m1,l1)*ValueOfAssLeg(m2,l2,temp2).array()*
//                                                    normForSphericHarmonic(m2,l2)*quadw.cast<long double>().array()*quadx.cast<long double>().array()*quadx.cast<long double>().array()/sqrt(1.0-quadx.cast<long double>().array()*quadx.cast<long double>().array())).sum();
//      else{
//          cout << "operator in twoYintegrals in gaunt.h unknown " <<operat<< endl; throw;
//      }
//      if(result != result){
//          cout << normForSphericHarmonic(m1,l1) << " " << normForSphericHarmonic(m2,l2) << endl;
//          cout << "nan in GauntCoeffTable::twoYintegrals " << l1 << " " << l2 << " " << m1 << " " << m2 << " " << operat << endl;
//          throw;
//      }
//      return result;

}
