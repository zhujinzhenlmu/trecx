// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "str.h"
#include "asciiFile.h"
#include "useMatrix.h"

using namespace std;
string Str::CurrentSeparator=" ";
Str::_Print Str::print;
Str::Separator Str::sep;
Str Str::emptyStr(""," ");

Str::Str(std::string String, std::string Sep, int Width):_wid(Width){
    CurrentSeparator=Sep;
    if(_wid!=0 and _wid>String.length())String=string(' ',_wid-String.length())+String;
    assign(String);
}

Str::Str(char Char, std::string Sep, int Width):_wid(Width){
    CurrentSeparator=Sep;
    string s(1,Char);
    if(_wid!=0)s=string(' ',_wid-1)+s;
    assign(s);
}

void Str::operator+(_Print Arg) {
#ifndef _DEVELOP_
    return; // only print in develop mode
#endif
    std::cout<<*this<<" (Str::print)"<<std::endl<<std::flush;
    clear();
    CurrentSeparator=" "; // restore default
}

Str & Str::operator+(Separator Arg) {
    CurrentSeparator=Arg._sep;
    return *this;
}

void Str::operator+(File F){
    ofstream stream;
    stream.open(F._name.c_str(),std::ios_base::out|std::ios_base::app);
    if(not stream.is_open())ABORT("could not open input file '"+F._name+"'");
    stream<<*this<<endl;
    clear();
    CurrentSeparator=" "; // restore default
}

void Str::Test(){
    complex<double>x(1.,1.);
    vector<int> i(3,6);
    Str("a","=")+x+Str::sep("...")+42+7+i+Str::print;
    Sstr+"try"+SEP("----")+"this"+Sendl;
    Sstr+"show a pointer"+SEP(" &")+&i+Sendl;
}
