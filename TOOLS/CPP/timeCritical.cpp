#include "timeCritical.h"
#include "coefficients.h" // temporary
namespace timeCritical{

static bool _on=false;
static bool _onOld=false;

void setOn(){_on=true;_onOld=true;      Coefficients::timeCritical=_on;}
void setOff(){_on=false;_onOld=false;   Coefficients::timeCritical=_on;}
void suspend(){_onOld=_on;_on=false;    Coefficients::timeCritical=_on;}
void resume(){_on=_onOld;               Coefficients::timeCritical=_on;}
bool isOn(){return _on;}
}
