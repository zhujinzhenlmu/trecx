// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "abort.h"
#include "../TOOLS/toolsHeader.h"
#include "mpiWrapper.h"
#include "readInput.h"
#include <map>
#include <iostream>
void abortFile(const std::string file,int line, const std::string mess,int signal) {
    std::cerr <<file.substr(file.rfind("/")+1)<<":"<<line;
    if(mess!="")std::cerr<<"\n"<<mess << "\n";
    std::cerr<< std::endl << std::flush;
#ifdef _DEVELOP_
    signal=1;
#endif
    if(MPIwrapper::Size()>1)
        MPIwrapper::Abort(signal);
    else
        abort();
}
void devAbortFile(const std::string file,int line, const std::string mess) {
    abortFile(file,line,"(Developer) "+mess,1);
}


std::map<std::string,unsigned int> abortCnt;
void abortCountDown(const std::string file,int line,const std::string mess, unsigned int Cnt) {
    if(abortCnt.count(file+mess)==0)abortCnt[file+mess]=Cnt;
    if (abortCnt[file+mess]==0)abortFile(file,line,mess,1);
    abortCnt[file+mess]--;
}


/// \brief activate by flag -DEBUGnew[=true]
bool newCode(){return ReadInput::main.flag("DEBUGnew","use new code during changes");}
/// \brief activate by flag -DEBUGold[=true]
bool oldCode(){return ReadInput::main.flag("DEBUGold","use old code during changes");}
bool oldEva(){return ReadInput::main.flag("DEBUGeva","use old evaluator during changes");}


/** @} */
