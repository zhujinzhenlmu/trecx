// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "tMath.h"
#include "stdio.h"
#include <iostream>

long double tMath::doubleFactorial(unsigned int N){
    long double f=1.;
    if(N%2==0)for(unsigned int k=1;k<N/2+1;  k++)f*=(long double)(2*k);
    else      for(unsigned int k=1;k<(N+1)/2;k++)f*=(long double)(2*k+1);
    return f;
}

