// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "readInputRange.h"

#include <string>
#include "readInput.h"
#include "printOutput.h"
#include "tools.h"
#include "algebra.h"

using namespace std;

ReadInputRange::ReadInputRange(const ReadInput &Inp)
{
    for(int k=0;k<Inp.inputTable.size();k++){
        if(Inp.inputTable[k].value.find("|")!=string::npos){
            _categ.push_back(Inp.inputTable[k].category);
            _names.push_back(Inp.inputTable[k].name);
            _line.push_back(Inp.inputTable[k].line);

            int n;
            string inUnit=Inp.unitSystem,sLow,sUp;
            if(_names.back().find("(")!=string::npos)
                inUnit=tools::stringInBetween(_names.back(),"(",")");
            range(Inp.inputTable[k].value,inUnit,sLow,sUp,n);

            double vlow=Algebra(sLow).val(0.).real();
            double vup=Algebra(sUp).val(0.).real();

            string outUnit=Inp.unitSystem;
            _lowVal.push_back(Units::convert(vlow,inUnit,outUnit));
            _upVal.push_back( Units::convert(vup, inUnit,outUnit));
            step.push_back((_upVal.back()-_lowVal.back())/n);
        }
    }
}

void ReadInputRange::print() const {
        PrintOutput::title("Parameter range");
        PrintOutput::paragraph();
        PrintOutput::newRow();
        PrintOutput::rowItem(" ");
        PrintOutput::rowItem("line");
        PrintOutput::rowItem("from");
        PrintOutput::rowItem("to");
        PrintOutput::rowItem("step");
        for(int k=0;k<_names.size();k++){
            string parUnit("DEFAULT_SYSTEM");
            if(_names[k].find("(")!=string::npos)parUnit=tools::stringInBetween(_names[k],"(",")");
            PrintOutput::newRow();
            PrintOutput::rowItem(_names[k]);
            PrintOutput::rowItem(_line[k]);
            PrintOutput::rowItem(Units::convert(_lowVal[k],"DEFAULT_SYSTEM",parUnit));
            PrintOutput::rowItem(Units::convert(_upVal[k],"DEFAULT_SYSTEM",parUnit));
            PrintOutput::rowItem(Units::convert(step[k],"DEFAULT_SYSTEM",parUnit));
        }
        PrintOutput::paragraph();
}

string ReadInputRange::low(const string Range){
    string units,lo,up;
    int n;
    range(Range,units,lo,up,n);
    if(units!="")lo+=" "+units;
    tools::cropString(lo);
    return lo;
}
void ReadInputRange::range(const string Range,string & Unit, string & Low,string & Up,int & N){
    Low=Range;Up=Range;N=0;
    if(Range.find("|")==string::npos)return;
    vector<string> valUnit=tools::splitString(Range,' ');
    if(valUnit.size()==0)return;
    vector<string> range=tools::splitString(valUnit[0],'|');
    Low=range[0];
    Up=range[1];
    N=1;
    if(range.size()==3)N=tools::string_to_int(range[2]);
    if(valUnit.size()>1)Unit=valUnit[1];
}
