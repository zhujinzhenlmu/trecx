// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "readInputList.h"
#include "mpiWrapper.h"
#include <stdio.h>      /* fopen, fputs, fclose, stderr */
#include <iostream>
#include <fstream>
#include "abort.h"
#include "stringTools.h"
#include "str.h"

using namespace std;

ReadInputList::ReadInputList(std::string File)
    :file(File)
{
    ifstream stream(file.c_str());
    if(not stream.is_open())ABORT("could not open input file '"+file+"'");

    if(file.find("/")!=string::npos)outDir=file.substr(0,file.rfind("/")+1);
    else                            outDir="";

    string line;
    if(MPIwrapper::isMaster())
        while(getline(stream,line))allLines.push_back(line);
    MPIwrapper::Bcast(allLines,MPIwrapper::master());
    stream.close();
}

void ReadInputList::write(const string Category, const string Name, unsigned int Line, string Value){
    ofstream stream;
    stream.open(file.c_str(),std::ios_base::out|std::ios_base::app);
    stream<<Category+":"+Name+"["+tools::str(Line)+"]="+Value;
    stream.close();
}

void ReadInputList::write(const string Flag, string Value){
    ofstream stream;
    stream.open(file.c_str(),std::ios_base::out|std::ios_base::app);
    stream<<Flag+"="+Value;
    stream.close();
}

string ReadInputList::readValue(const string Category, const string Name, const string Default, const string Docu, unsigned int Line, string Flag, string Allow){
    string def=InputItem(Category,Name,Line,Flag,Docu,Default,Allow).listDef();
    for(int k=0;k<allLines.size();k++){
        // machine-generated input is assumed to be legal - all checks are by-passed
        if(allLines[k].find(def)==0)
            return allLines[k].substr(def.length());
    }
    return ReadInput::notFound+"_"+Category+":"+Name;
}
