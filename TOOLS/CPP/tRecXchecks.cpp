// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "tRecXchecks.h"

#include "readInput.h"
#include "printOutput.h"
namespace tRecX{

std::vector<std::string> offList;

void read(){
    ReadInput::main.read("Checks","off",offList,"","do not perform checks as listed, if emty list switch of globally");
    if((offList.size()==0 and ReadInput::main.found("Checks","off")) or
            ReadInput::main.flag("noChecks","globally switch off all checks"))
            offList.insert(offList.begin(),1,"__GLOBAL__");
}
void print(){
    if(offList.size()==0)return;

    if(offList[0]=="__GLOBAL__"){
        PrintOutput::warning("checks switched off globally");
        return;
    }

    for(int k=0;k<offList.size();k++){
        PrintOutput::warning("check \""+offList[k]+"\" disabled");
    }
}

bool off(const std::string mess) {
    if(offList.size()==0)return false;
    if(offList[0]=="__GLOBAL__")return true;
    for(int k=0;k<offList.size();k++)
        if(mess==offList[k])return true;
    else return false;
}

void setOff(std::string Off){
            offList.push_back(Off);
}

}
