// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "printOutput.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include "tools.h"
#include "folder.h"
#include <float.h>

#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>
#include <fstream>
#include <iostream>

#include "readInput.h"
#include "mpiWrapper.h"

//namespace bio = boost::iostreams;
//namespace bio = std;
//using bio::tee_device;
//using bio::stream;

using namespace std;

bool PrintOutput::_off=false;
const string PrintOutput::outExtension="outf";
string PrintOutput::fileOut;
string PrintOutput::monMessage;
std::ofstream * PrintOutput::sOut=0;
std::ofstream * PrintOutput::sMon=0;
PrintOutput::DualStream * PrintOutput::out=new DualStream(&cout);
//std::ostream * PrintOutput::out=&cout;
unsigned int PrintOutput::lengthLine=80;
unsigned int PrintOutput::namesTab=20;
unsigned int PrintOutput::row=0;
unsigned int PrintOutput::col=0;
string PrintOutput::indent="   ";
string PrintOutput::titleSeparator="=";
string PrintOutput::itemSeparator=", ";
string PrintOutput::nameSeparator=",";
string PrintOutput::columnSeparator=" ";
string PrintOutput::namesEnd=":";

map<string,unsigned int> PrintOutput::warningCount;
map<string,unsigned int> PrintOutput::messageCount;

vector<string> PrintOutput::finalMessage;
string PrintOutput::level="full";

vector<unsigned int > PrintOutput::tableWidth=vector<unsigned int>(0);
vector<vector<string> > PrintOutput::table=vector<vector<string> >(0);
string PrintOutput::namesLine="",PrintOutput::valuesLine="";

// file-local variables
static string progressHeader="";
static string progressStatus="";
static vector<string> previousLevels;


PrintOutput::DualStream::DualStream(std::ostream * Os1, std::ostream * Os2) : os1(Os1), os2(Os2)
{}

bool PrintOutput::noScreenFlag() {
    bool noScreen;
    ReadInput::main.read("Print","noScreen",noScreen,ReadInput::flagOnly,"suppress output to screen",0,"noScreen");
    return noScreen;
}

void PrintOutput::set(string File){
    if(not MPIwrapper::isMaster())return;
    if(File.length()==0 or File=="")return;
    paragraph();
    paragraph();
    if(File=="cout" or File==outExtension){
        out=new DualStream(&cout);
        fileOut="NONE";
    } else {
        message("further output on "+File);
        fileOut=File;
        sOut=new ofstream();
        sOut->open(File.c_str());
        if(noScreenFlag())PrintOutput::out= new DualStream(sOut);
        else              PrintOutput::out= new DualStream(sOut,&cout);
    }
}

void PrintOutput::outputLevel(string Level){
    if(Level!="full" and Level!="low" and Level!="off" and Level!="restore")
        ABORT("output level must be one of full,low,off,restore is: "+Level);
    if(Level=="restore"){
        if(previousLevels.size()>0){
            level=previousLevels.back(); // simple solution, later push/pop back
            previousLevels.pop_back();
        }
    }
    else {
        level=Level;
        if(previousLevels.size()==0 or level!=previousLevels.back())
            previousLevels.push_back(level);
    }
}

void PrintOutput::timerWrite(){
    if(_off)return;
    PrintOutput::title("timer information");
    Timer::write(cout);
    if(sOut!=0)Timer::write(*sOut);
}

void PrintOutput::header(string File){
    set(File);
    string headerName="Dummy header";
    *out<<'\n'<<" ";
    for(unsigned int k=1;k<lengthLine;k++)*out<<"*";
    *out<<'\n'<<" *** "<<headerName;
    for(unsigned int k=5+headerName.length();k<lengthLine-3;k++)*out<<" ";
    *out<<"***"<<'\n'<<" ";
    for(unsigned int k=1;k<lengthLine;k++)*out<<"*";
    *out<<'\n';
}

void PrintOutput::title(string Title){
    flush();
    *out<<'\n'<<'\n'<<" ";
    for(unsigned int k=0;k<3;k++)*out<<titleSeparator;
    if(Title.length()>0)*out<<"  "+Title+"  ";
    for(unsigned int k=Title.length()+8;k<lengthLine;k++)*out<<titleSeparator;
    *out<<'\n';
}
void PrintOutput::subTitle(string Sub){
    flush();
    *out<<" "<<Sub<<'\n';
}
void PrintOutput::verbatim(string Mess,ostream *Ostr){
    end();
    if(level!="full")return;
    if(Ostr==0)*out<<Mess;
    else *Ostr<<Mess;
}

void PrintOutput::message(string Mess, ostream *Ostr, bool Final, int Limit){
    unsigned int cnt=messageCount[Mess];
    messageCount[Mess]++;
    if(Limit<0 and cnt>=-Limit)return;
    if(Limit>0 and cnt>= Limit)return;
    end();
    if(level!="full")return;
    string line;
    if(Mess.length()<100)
        line=" *** "+Mess+" ***\n";
    else {
        line=Mess;
        size_t pos=0;
        while(string::npos!=(pos=line.find("\n",pos+10)))line.insert(pos+1,"   * ");
        line=" *** "+line+"\n";
    }

    // if message during progess monitor
    if(progressStatus!=""){
        line="\n"+line;
        progressStatus="";
    }

    if(Ostr==0)*out<<line;
    else *Ostr<<line;
    if(Final)finalMessage.push_back(line);
}
void PrintOutput::DEVmessage(std::string Mess, std::ostream *Ostr, bool Final){
#ifdef _DEVELOP_
   message("(Developer) "+Mess,Ostr,Final);
#endif
}
void PrintOutput::DEVwarning(std::string Mess, int Limit, std::ostream *Ostr){
#ifdef _DEVELOP_
    warning("(Developer) "+Mess,Limit,Ostr);
#endif

}

void PrintOutput::terminationMessages(ostream *Ostr){
    if(Ostr==0)for(int k=0;k<finalMessage.size();k++)*out<<"\n";
    else       for(int k=0;k<finalMessage.size();k++)*Ostr<<"\n";
    if(Ostr==0)for(int k=0;k<finalMessage.size();k++)*out<<finalMessage[k];
    else       for(int k=0;k<finalMessage.size();k++)*Ostr<<finalMessage[k];
}

void PrintOutput::progressStart(string Mess){
    progressHeader=Mess;
}
void PrintOutput::progressStop(string Mess, ostream *Ostr){
    if(progressStatus=="")return;
    if(Ostr==0)*out<<" "+Mess<<"\n";
    else      *Ostr<<" "+Mess<<"\n";
    progressStatus="";
}

void PrintOutput::progress(string Mess,ostream *Ostr){
    if(level=="off")return;
    string line=Mess;

    // first call after non-empty progressStart(...)
    if(progressHeader!="")line=progressHeader+": "+Mess;
    progressHeader="";

    progressStatus+=line;
    if(Ostr==0)*out<<line;
    else *Ostr<<line;
    if(progressStatus.length()>80){
        if(Ostr==0)*out<<"\n";
        else *Ostr<<"\n";
        progressStatus="";
    }
}
void PrintOutput::warning(string Mess, int Limit, ostream *Ostr){
    unsigned int cnt=warningCount[Mess];
    warningCount[Mess]++;
    if(Limit<0 and cnt>=-Limit)return;
    if(Limit>0 and cnt>= Limit)return;
    end();
    // split into multi-line warning
    vector<string> mlin=tools::splitString(Mess,'\n');
    string line;
    for(int n=0;n<mlin.size();n++){
        line=mlin[n];
        if(n==0)line=" !!! WARNING --- "+line;
        else    line="             ... "+line;
        if(n==mlin.size()-1)line+=" --- WARNING !!!";
        line+="\n";
        if(Ostr==0)*out<<line;
        else *Ostr<<line;
    }
    if(cnt+1==Limit)line="                 !!! Limit="+tools::str(Limit)+" reached, no further warnings as above !!!\n\n";
    else            line="\n";
    if(Ostr==0)*out<<line;
    else *Ostr<<line;
}
void PrintOutput::warningList(){
    if(warningCount.size()==0)return;
    vector<string> list;
    list=tools::splitString(tools::listMapKeys(warningCount,"\f"),'\f');
    subTitle("\n !!! --- CURRENT WARNINGS --- !!!");
    for(unsigned int k=0;k<list.size();k++){
        std::replace(list[k].begin(),list[k].end(),'\n',' ');
        if(list[k].length()>80)list[k]=list[k].substr(0,80)+"...";
        lineItem(tools::str(warningCount[list[k]]),list[k]);
        newLine();
    }
//    paragraph();
}

void PrintOutput::paragraph(){end();*out<<'\n';}
void PrintOutput::newLine(){end();*out<<'\n';}

void PrintOutput::lineItem(string Name, double Value, string Subtitle, unsigned int Digits){lineItem(Name,tools::str(Value,Digits,DBL_MAX/2),Subtitle);}
void PrintOutput::lineItem(string Name, int Value, string Subtitle)   {lineItem(Name,tools::str(Value),Subtitle);}
void PrintOutput::lineItem(string Name, string Value, string Subtitle){
    if(Subtitle.length()>0)subTitle(Subtitle);
    if(namesLine.length()!=0)
    {   if(Name.length()>0)namesLine +=nameSeparator;
        valuesLine+=itemSeparator;
    }
    namesLine +=Name;
    valuesLine+=Value;
}

void PrintOutput::tableColumn(string Name, std::vector<int> Values, string Subtitle)
{   vector<string> vec;
    for(unsigned int n=0;n<Values.size();n++)vec.push_back(tools::str(Values[n]));
    tableColumn(Name,vec,Subtitle);}
void PrintOutput::tableColumn(string Name, std::vector<double> Values, string Subtitle)
{   vector<string> vec;
    for(unsigned int n=0;n<Values.size();n++)vec.push_back(tools::str(Values[n],14,DBL_MAX/2));
    tableColumn(Name,vec,Subtitle);}
void PrintOutput::tableColumn(string Name, vector<string> Value, std::string Subtitle){
    if(Subtitle.length()>0)subTitle(Subtitle);
    table[0].push_back(Name);
    table.push_back(Value);
}

void PrintOutput::newRow(){row++;col=1;}
void PrintOutput::rowItem(double Value,unsigned int Width){rowItem(tools::str(Value,Width,DBL_MAX/2));}
void PrintOutput::rowItem(   int Value){rowItem(tools::str(Value));}
void PrintOutput::rowItem(unsigned int Value){rowItem(tools::str(Value));}
void PrintOutput::rowItem(string Value){
    if(row==0 or col==0)ABORT("open newRow befor inserting rowItem");
    for (int j=table.size();j<col;j++)table.push_back(vector<string>(0));
    for (int i=table[col-1].size();i<row;i++)table[col-1].push_back("");
    table[col-1][row-1]=Value;
    col++;
}

void PrintOutput::end(){
    flush();
    tableWidth.clear();
}

void PrintOutput::flush(){
    // values in a single line
    if(namesLine.length()>0){
        *out<<" "<<setw(max(namesTab,(unsigned int)(namesLine.length()+2)))<<namesLine+namesEnd+" ";
        *out<<valuesLine;
        namesLine="";
        valuesLine="";
    }

    // values in table
    if(tableWidth.size()>0 or table.size()>0){
        bool newTable=tableWidth.size()==0;
        unsigned int length=0;
        for (unsigned int n=0;n<table.size();n++){
            if(newTable){
                tableWidth.push_back(table[n][0].length());
                for (unsigned int k=1;k<table[n].size();k++)
                    tableWidth.back()=max(tableWidth.back(),(unsigned int)(table[n][k].length()));
            }
            length=max(length,(unsigned int)(table[n].size()));
        }
        for(unsigned int l=0;l<length;l++){
            for(unsigned int n=0;n<table.size();n++){
                if(n==0)*out<<indent;
                if(table[n].size()>l)*out<<setw(tableWidth[n])<<table[n][l];
                else *out<<setw(tableWidth[n])<<" ";
                if(n<table.size()-1)*out<<columnSeparator;
            }
            *out<<'\n';
            if(newTable and l==0){
                // first line in new table is header
                *out<<indent;
                for(unsigned int n=0;n<table.size();n++){
                    if(n>0)*out<<" ";
                    *out<<"  ";
                    for(unsigned int k=1;k<tableWidth[n]-1;k++)*out<<"-";
                }
                *out<<'\n';
            }
        }
    }
    table.clear();
    col=0;
    row=0;
}


