// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "stringTools.h"
#include "tools.h"
#include "str.h"
#include "potSolid.h"
#include "abort.h"

namespace tools{

using namespace std;

/// base name of file
std::string fileBase(const std::string & File){
    size_t i0=File.rfind("/")+1;
    if(i0==std::string::npos)i0=0;
    return File.substr(i0,File.rfind(".")-i0);
}

/// remove leading blanks and returns from string
std::string lcropString(std::string s){while(s.length()!=0){if(s[           0]!=' '&&s[           0]!='\r'&&s[           0]!='\n')break;s.erase(0,           1);};return s;}
std::string rcropString(std::string s){while(s.length()!=0){if(s[s.length()-1]!=' '&&s[s.length()-1]!='\r'&&s[s.length()-1]!='\n')break;s.erase(s.length()-1,1);};return s;}
std::string cropString(std::string s){return lcropString(rcropString(s));}

string toLower(const string Inp) {
    string out(Inp);
    for (int b=0; b < Inp.length(); b++)out[b]=tolower(Inp[b]);
    return out;
}

std::string unquote(string s){
    // single quotes
    size_t i0=s.find("'"),i1=s.rfind("'");
    if(i0==0 and i1==s.length()-1)return s.substr(1,s.length()-2);
    i0=s.find('"');
    i1=s.rfind('"');
    if(i0==0 and i1==s.length()-1)return s.substr(1,s.length()-2);
    return s;
}

std::string str(std::string S){return S;}

std::string str(bool Bool){
    if(Bool)return "true";
    return "false";
}

/// convert Number to std::string (overloaded)
string str(char Arg, int Len){
    ostringstream oss;
    if(Len>0){oss<<std::setw(Len)<< Arg;return oss.str();}
    else     {oss<<                 Arg;return oss.str();}
}
string str(string Arg, int Len){
    ostringstream oss;
    if(Len>0){oss<<std::setw(Len)<< Arg;return oss.str();}
    else     {oss<<                 Arg;return oss.str();}
}
string str(const int          Number, int Len, char Fill){
    ostringstream oss;
    if(Len>0){oss<<std::setw(Len)<<setfill(Fill)<< Number;return oss.str();}
    else     {oss<<                                Number;return oss.str();}
}
string str(const unsigned int Number, int Len, char Fill){
    ostringstream oss;
    if(Len>0){oss<<std::setw(Len)<<setfill(Fill)<< Number;return oss.str();}
    else     {oss<<                                Number;return oss.str();}
}
std::string str(std::complex<double>* Pointer, int Dum){
    ostringstream oss;oss<<Pointer;return oss.str();
}

string str(const size_t Size){ 
    ostringstream ss;
    ss<<Size;
    return ss.str();
}
string str(const long int Size){ 
    ostringstream ss;
    ss<<Size;
    return ss.str();
}
string str(double Number, int Prec, double Inf){
    if(Prec==0)return string(1,tools::zero(complex<double>(Number)));
    if(Inf>0.){
        string inf="Infty";
        inf=inf.substr(0,Prec);
        if(Number>= Inf)return     inf;
        if(Number<=-Inf)return "-"+inf;
    }
    ostringstream oss; oss<<std::setprecision(Prec)<< Number;return oss.str();}
string str(complex<double> Number, int Prec){if(Prec==0)return string(1,tools::zero(Number));return "("+str(real(Number),Prec)+","+str(imag(Number),Prec)+")";}


/// number of (non-overlapping) occurrences of substring in String
unsigned int subStringCount(const std::string String,const std::string Sub){
    if(Sub.length()==0)return 0;
    unsigned int count=0;
    size_t pos=String.find(Sub,0);
    while(pos!=std::string::npos){
        count++;
        pos=String.find(Sub,pos+Sub.length());
    }
    return count;
}
/// number of (non-overlapping) occurrences of substring in String
unsigned int subStringCount(const std::string String,const std::string Sub, string Left, string Right){
    if(Sub.length()==0)return 0;
    unsigned int count=0;
    size_t pos=findFirstOutsideBrackets(String,Sub,Left,Right,0);
    while(pos!=std::string::npos){
        count++;
        pos=findFirstOutsideBrackets(String,Sub,Left,Right,pos+Sub.length());
    }
    return count;
}

/// convert strings to numbers
int    string_to_int(const string &Text){ return atoi(Text.c_str()); }

double string_to_double(const string &Text,const double Inf){
    if(tools::cropString(Text)=="Infty" )return  Inf;
    if(tools::cropString(Text)=="-Infty")return -Inf;
    char* pEnd;
    return strtod(Text.c_str(),&pEnd); }

complex<double> string_to_complex(const string &Text){
    char* pEnd;
    size_t comma=Text.find(",");
    if(comma==string::npos)return complex<double>(strtod(Text.c_str(),&pEnd),0);
    else return complex<double>(strtod(Text.substr(0,comma).c_str(),&pEnd),strtod(Text.substr(comma+1).c_str(),&pEnd));
}

//// conversion to bool: must be either true or false
bool string_to_bool(const string &Text){
    if (Text != "true"){
        if (Text != "false")ABORT("bool string must be true or false, is: " + Text);
        return false;
    }
    return true;
}


/// [beg,end,pts] ... expands to { beg, beg+(end-beg)/(pts-1),..., end} <br>
/// [beg] or beg or beg==end ... single point
//std::vector<double> rangeToGrid(string Range, int Points){
//    vector<string> parts=tools::splitString(tools::stringInBetween(Range,"[","]"),',');
//    vector<double> def;
//    for(string p: parts)def.push_back(Algebra::constantValue(p));

//    if(def.size()==0)return {0.,0.,0};
//    if(def.size()==1 or def[0]==def[1])return {def[0]};

//    int points=Points;
//    if(def.size()==3)points=max(2,int(def[2]));

//    vector<double>grid;
//    for(int k=0;k<points;k++)grid.push_back(def[0]+k*(def[1]-def[0])/double(points-1));
//    return grid;
//}
/// get equidistant grid from Range string (should replace the stringTools version), attach units like' eV' or '~eV'
std::vector<double> rangeToGrid(std::string Range /** [beg,end,pts] */,
                                int Points        /** default for pts */,
                                std::vector<std::string> Delim /** alternate deliminators for Range */)
{
    if(Delim.size()!=3)DEVABORT("need format Delim={begString,sepChar,endString}");
    if(Delim[0]!="" and Delim[1]!="")Range=tools::stringInBetween(Range,"[","]");
    if(Delim[1].length()!=1)DEVABORT(Sstr+"can only use single-character separator, found: Delim="+Delim);
    vector<string> parts=tools::splitString(Range,Delim[1][0]);
    vector<double> def;
    for(string p: parts)def.push_back(Algebra::constantValue(p));

    if(def.size()==0)return {0.,0.,0};
    if(def.size()==1 or def[0]==def[1])return {def[0]};

    int points=Points;
    if(def.size()==3)points=max(2,int(def[2]));

    vector<double>grid;
    for(int k=0;k<points;k++)grid.push_back(def[0]+k*(def[1]-def[0])/double(points-1));
    return grid;
}


std::vector<std::string> splitString(const std::string s, char delimiter, const string LeftBracket, const string RightBracket) {
    string s0(s);
    // remove white-space, if blank delimiter
    if(delimiter==' ')s0=tools::cropString(s);
    std::vector<std::string> elements,sep;
    splitString(s0,string(1,delimiter),elements,sep,LeftBracket,RightBracket);

    // remove white-space, if blank delimiter
    if(delimiter==' '){
        for(string &e: elements)e=tools::cropString(e);
        if(elements.back()=="")elements.pop_back();
    }

    if(elements.size()==0 and s!="")elements.push_back(s);
    return elements;
}

/// next occurrence of any character of Sub that is not between any pair of the brackets
size_t findOutsideBrackets(bool First, const string S,const string &Sub,const string &Left, const string &Right, size_t Pos){
    if(Left.length()!=Right.length()){cout<<"ERROR: number of left brackets does not match right\n";exit(1);}
    size_t i0;
    if(First)i0=S.find_first_of(Sub,Pos);
    else     i0=S.find_last_of(Sub,Pos);
    if(i0==string::npos)return i0;
    int openBracket=0;
    for (unsigned int k=0;k<Left.length();k++){
        string L=Left.substr(k,1),R=Right.substr(k,1);
        // it may happen that delimiters at one side occur multiple times, count that
        unsigned int Lmult=subStringCount(Left,L),Rmult=subStringCount(Right,R);
        if(L!=R)openBracket+=subStringCount(S.substr(0,i0),L)*Rmult-subStringCount(S.substr(0,i0),R)*Lmult;
        else openBracket+=subStringCount(S.substr(0,i0),L)%2;
    }
    if(openBracket!=0){
        if(i0+1==Pos)return string::npos; // no position outside brackets
        return findOutsideBrackets(First,S,Sub,Left,Right,i0+1);
    }
    return i0;
}
size_t findFirstOutsideBrackets(const string &S,const string Sub,const string &Left, const string &Right, size_t Pos){return findOutsideBrackets(true,S,Sub,Left,Right,Pos);}
size_t findLastOutsideBrackets (const string &S,const string Sub,const string &Left, const string &Right, size_t Pos){return findOutsideBrackets(false,S,Sub,Left,Right,Pos);}

/// split a string into substrings at a set of separators, return actual separators in vector
/// first item can, but does not need to be preceded by a separator
/// e.g. split  s="a + b - ccc " with Separators="+-" into Elem=("a "," b "," ccc "), Sep=(" ","+","-")
/// any separators inside a LeftBracket/RightBracket pair will be ignored
/// this allows splitting expressions <dJd+qJq>+<J> into <dJd+qJq> and <J>
void splitString(const string S, string Separators, vector<string> &Elem, vector<string> & Sep,const string LeftBracket,const string RightBracket) {
    Elem.clear();
    Sep.clear();
    string s;

    // if blank separation is an option, reduce to single blank per interval
    if(Separators.find(" ")!=string::npos){
        unsigned int k0=0,k1=S.find(" ");
        while(k0<S.length()){
            s+=tools::cropString(S.substr(k0,k1-k0))+" ";
            k0=S.find_first_not_of(" ",k1);
            k1=S.find(" ",k0);
            if(k1>S.length())k1=S.length();
        }
        tools::cropString(s);
    } else {
        s=tools::cropString(S);
    }
    size_t i0=0,i1=findFirstOutsideBrackets(s,Separators,LeftBracket,RightBracket);
    Elem.push_back(cropString(s.substr(i0,i1)));
    if(Elem.back()=="")Elem.pop_back(); // nothing before first separator
    else Sep.push_back(" ");
    while(i1!=std::string::npos){
        i0=i1;
        i1=findFirstOutsideBrackets(s,Separators,LeftBracket,RightBracket,i1+1);
        Elem.push_back(s.substr(i0+1,i1-i0-1));
        Sep.push_back(s.substr(i0,1));
    }
    if(Elem.size()==0 and S!="")Elem.push_back(S);
}

/// Left and Right constain subsequent pairs of brackets, e.g. ('| and )'> for (...), '...', |...>
/// if no matching pair is found, the original string is returned
string stringInBetween(string S, string Left, string Right,bool Full){
    if(Full){
        size_t il=S.find(Left),ir=S.rfind(Right);
        if(il<ir and ir!=string::npos)return S.substr(il+Left.length(),ir-il-Left.length());
    } else {
        for(unsigned int k=0;k<min(Left.size(),Right.size());k++){
            size_t il=S.find(Left.substr(k,1)),ir=S.rfind(Right.substr(k,1));
            if(il<ir and ir!=string::npos)return S.substr(il+1,ir-il-1);
        }
    }
    return S;
}

}



