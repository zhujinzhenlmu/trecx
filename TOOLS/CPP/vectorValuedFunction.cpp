#include "vectorValuedFunction.h"
#include "tools.h"

std::map<std::string,std::shared_ptr<const VectorValuedFunction> > VectorValuedFunction::_list;
bool VectorValuedFunction::validArguments(std::vector<double> X, std::string Message) const{

    double eps=1.e-12;
    bool valid=true;
    for(int k=0;k<_lowLim.size();k++)valid=valid and X[k]<_lowLim[k]-eps;
    for(int k=0;k<_upLim.size();k++) valid=valid and X[k]>_upLim[k]+eps;
    if(not valid and Message!="")abortInvalidArguments(tools::str(X)+" "+Message);
    return valid;
}

std::string VectorValuedFunction::info(int Number) const {
    std::string s=name()+" ("+coordinates()+") ["+tools::str(length())+"]";
    if(Number>-1)s=s+" No."+tools::str(Number);
    return s;
}

std::string VectorValuedFunction::list(){
    return tools::listMapKeys(_list,", ");}
