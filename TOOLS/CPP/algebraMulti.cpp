#include "algebraMulti.h"
#ifdef _USE_BOOST_
#include <boost/algorithm/string/replace.hpp>
#endif

using namespace std;

bool AlgebraMulti::isAlgebraMulti(string Definition){
    // too primitive, obviously
    return tools::splitString(tools::stringInBetween(Definition,"(",")"),',').size()>1;
}

const AlgebraMulti* AlgebraMulti::factory(const string Definition){
    if(Definition.find("Sum")==0)     return new AlgebraSum(Definition);
    if(Definition.find("ExternalHO[Eta")==0)return new AlgebraExternalHO(Definition);
    if(Definition.find("External")==0)return new AlgebraExternal(Definition);
    if(Definition.find("CO2Pot")==0)return new AlgebraCO2Pot(Definition);
    if(Definition.find("CO2Trunc")==0)return new AlgebraCO2Trunc(Definition);
    return 0;
}

AlgebraMulti::AlgebraMulti(std::string Definition, const string Coors)
{
    definition=Definition;
    vector<string> defAlgs=tools::splitString(tools::stringInBetween(Definition,"(",")"),',');
    if(Coors!="*"){
        vector<string> coors=tools::splitString(Coors,',');
        for(int k=0;k<coors.size();k++)
            if(coors.size()!=defAlgs.size() or coors[k]!=defAlgs[k])
                ABORT("arguments in "+Definition+" must be "+Coors);
    }
    vector<string> var;
    if(Definition.rfind(":")!=string::npos)var=tools::splitString(Definition.substr(Definition.find(":")),':');
    if(var.size()==0)for(int k=0;k<defAlgs.size();k++)var.push_back("Q"+tools::str(k));
    if(var.size()!=defAlgs.size())
        ABORT("the number of variables does not match number of arguments in "+Definition);

    for(int k=0;k<defAlgs.size();k++){
#ifdef _USE_BOOST_
        boost::replace_all(defAlgs[k],var[k],"Q");
#else
        DEVABORT("no-boost");
#endif
        _arg.push_back(new Algebra(defAlgs[k]));
        if(_arg.back()==0 or not _arg.back()->isAlgebra())
            ABORT("failed to evaluate algebra "+defAlgs[k]+" as part of "+Definition+"\nFailure: "+Algebra::failures);
    }
}

void AlgebraMulti::checkArgs(const std::vector<std::complex<double> > Q) const {
    if(Q.size()!=_arg.size())ABORT(Str("need")+_arg.size()+"arguments for"+definition+"got"+Q.size());
}

AlgebraExternal::AlgebraExternal(std::string Definition):AlgebraMulti(Definition,"Eta,Rn"){
    vector<string> par=tools::splitString(tools::stringInBetween(Definition,"[","]"),',');
    if(par.size()!=2)ABORT("need format External[dist,screen](Eta,Rn), got: "+Definition);
    dist=tools::string_to_double(par[0]);
    screen=tools::string_to_double(par[1]);
    trunc.reset(new Algebra("trunc[10,20]"));

}

std::complex<double> AlgebraExternal::val(const std::vector<std::complex<double> > Q) const{
    checkArgs(Q);
    if(abs(Q[1])<3.)return 0;
    complex<double> z=Q[0]*Q[1];
    complex<double> rhoSq=Q[1]*Q[1]-z*z;

    return -trunc->val(Q[1])*(1./sqrt(pow(z-dist,2)+rhoSq+screen));
}

AlgebraCO2Pot::AlgebraCO2Pot(std::string Definition)
    :AlgebraMulti(Definition,"Eta,Rn"),bCO(2.197)
{
    vector<string> par=tools::splitString(tools::stringInBetween(Definition,"[","]"),',');
    if(par.size()!=4)ABORT("need format CO2Pot[runcR,alfa,screenC,screenO](Eta,Rn), got: "+Definition);

    double truncR=Algebra::getParameter(0,Definition);
    trunc.reset(new Algebra("trunc["+tools::str(truncR-5.,2)+","+tools::str(truncR,2)+"]"));
    alfaCharge=Algebra::getParameter(1,Definition);
    screenC=Algebra::getParameter(2,Definition);
    screenO=Algebra::getParameter(3,Definition);
}

std::complex<double> AlgebraCO2Pot::val(const std::vector<std::complex<double> > Q) const{
    checkArgs(Q);
    complex<double>v;
    v =-alfaCharge*(1.+5.*exp(-Q[1]/screenC))/Q[1];
    complex<double> rOff;
    rOff=sqrt(Q[1]*Q[1]+2.*Q[0]*Q[1]*bCO+bCO*bCO);v+=(0.5*(alfaCharge-1.))*(1.+7.*exp(-rOff/screenO))/rOff;
    rOff=sqrt(Q[1]*Q[1]-2.*Q[0]*Q[1]*bCO+bCO*bCO);v+=(0.5*(alfaCharge-1.))*(1.+7.*exp(-rOff/screenO))/rOff;
    v*=trunc->val(Q[1]);
    return v;
}

AlgebraCO2Trunc::AlgebraCO2Trunc(std::string Definition)
    :AlgebraMulti(Definition,"Eta,Rn")
{
    vector<string> par=tools::splitString(tools::stringInBetween(Definition,"[","]"),',');
    if(par.size()!=5)ABORT("need format CO2Pot[smooth,truncR,alfa,screenC,screenO](Eta,Rn), got: "+Definition);
    string co2Def="CO2Pot["+par[1]+","+par[2]+","+par[3]+","+par[4]+"](Eta,Rn)";
    co2.reset(new AlgebraCO2Pot(co2Def));

    smoothR=Algebra::getParameter(4,Definition);
    smooth.reset(new Algebra("trunc["+tools::str(smoothR-1.,2)+","+tools::str(smoothR,2)+"]"));
}

std::complex<double> AlgebraCO2Trunc::val(const std::vector<std::complex<double> > Q) const{
    checkArgs(Q);
    complex<double> vCO2 = co2->val(Q),smoothVal=smooth->val(Q[1]);
    return -smoothVal/smoothR+(1.-smoothVal)*vCO2;
}
