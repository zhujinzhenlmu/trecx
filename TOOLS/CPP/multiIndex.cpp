// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "multiIndex.h"

MultiIndex::MultiIndex(const std::vector<int> &M):m(M){}

void MultiIndex::first(std::vector<int> &i){i.assign(m.size(),0);}

/// increment multi-index, initial=empty vector, final=return false and empty vector
/// NOTE: rightmost indices run fastest
bool MultiIndex::next(std::vector<int> & i){
    if(i.size()==0){
        if(m.size()==0)return false;
        i.assign(m.size(),0);
        return true;
    }
    for(int d=m.size()-1;d>=0;d--){
        if(i[d]<m[d]-1){
            i[d]++;
            for(unsigned int l=d+1;l<m.size();l++)i[l]=0;
            return true;
        }
    }
    // if it cannot be incremented, reset and return false
    i.clear();
    return false;
}
bool MultiIndex::nextCol(std::vector<int> & i){
    if(i.size()==0){
        if(m.size()==0)return false;
        i.assign(m.size(),0);
        return true;
    }
    for(int d=0;d<m.size();d++){
        if(i[d]<m[d]-1){
            i[d]++;
            for(int l=0;l<d;l++)i[l]=0;
            return true;
        }
    }
    // if it cannot be incremented, reset and return false
    i.clear();
    return false;
}
