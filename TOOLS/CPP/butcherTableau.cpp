// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "butcherTableau.h"

#include "abort.h"

ButcherTableau::ButcherTableau(std::string Name)
    :_name(Name)
{
    if(Name=="RK4"){
        _consistency=4;
        _a.resize(4);
        _a[0]={0.,0.,0.,0.};
        _a[1]={0.5,0.,0.,0.,};
        _a[2]={0.,0.5,0.,0.};
        _a[3]={0.,0.,1.0,0.};
        _b={1./6.,1./3.,1./3.,1./6};
        _c={0.,0.5,0.5,1.};
    }
    else if (Name=="Butcher67")
    {//    case (6) ! Butcher (6,7)
        _a.resize(7);
        _b.resize(7);
        _c.resize(7);
        //       np=7
        _consistency=6;
        _a[0].assign(7,0.);
        //       _a[2]1)=1./3.
        _a[1]={1./3.,0.,0.,0.,0.,0.,0.};
        //       _a[3]2)=2./3.
        _a[2]={0.,2./3.,0.,0.,0.,0.,0.};
        //       _a[4]1)=1./12.
        //       _a[4]2)=1./3.
        //       _a[4]3)=-1./12.
        _a[3]={1./12.,1./3.,-1./12.,0.,0.,0.,0.};
        //       _a[5]1)=-1./16.
        //       _a[5]2)=9./8.
        //       _a[5]3)=-3./16.
        //       _a[5]4)=-3./8.
        _a[4]={-1./16.,9./8.,-3./16.,-3./8.,0.,0.,0. };
        //       _a[6]2)=9./8.
        //       _a[6]3)=-3./8.
        //       _a[6]4)=-3./4.
        //       _a[6]5)=1./2.
        _a[5]={0.,9./8.,-3./8.,-3./4.,1./2.,0.,0.};
        //       _a[7]1)=9./44.
        //       _a[7]2)=-9./11.
        //       _a[7]3)=63./44.
        //       _a[7]4)=18./11.
        //       _a[7]6)=-16./11.
        _a[6]={9./44.,-9./11.,63./44.,18./11.,0.,-16./11.,0.};

        //        c(2)=1.d0/3.d0
        //        c(3)=2.d0/3.d0
        //        c(4)=1.d0/3.d0
        //        c(5)=1.d0/2.d0
        //        c(6)=1.d0/2.d0
        //        c(7)=1.d0
        _c[1]=1./3.;
        _c[2]=2./3.;
        _c[3]=1./3.;
        _c[4]=1./2.;
        _c[5]=1./2.;
        _c[6]=1.;

        //       w(1)=11.d0/120.d0
        //       w(3)=27.d0/40.d0
        //       w(4)=27.d0/40.d0
        //       w(5)=-4.d0/15.d0
        //       w(6)=-4.d0/15.d0
        //       w(7)=11.d0/120.d0
        _b[0]=11./120.;
        _b[2]=27./40.;
        _b[3]=27./40.;
        _b[4]=-4./15.;
        _b[5]=-4./15.;
        _b[6]=11./120.;

    }

    else
        ABORT("no default Butcher tableau with name \""+Name+"\"");
}

