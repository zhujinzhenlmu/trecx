#include "productFunction.h"

#include "algebra.h"
ProductFunction::ProductFunction(std::string Coors, std::vector<std::string> Algs)
    :_coors(Coors)
{

    _algs.resize(Algs.size());
    for(int k=0;k<Algs.size();k++){
        std::vector<std::string> fact=tools::splitString(Algs[k],'.',"<({[",">)}]");
        if(fact.size()!=std::count(_coors.begin(),_coors.end(),'.')+1)
            ABORT("algebra count in"+Algs[k]+"does not match coordinates"+_coors
                  +"\nseparate factors by '.', use brackets (alg1).(alg2).alg3 etc if needed");
        for(int l=0;l<fact.size();l++){
            _algs[k].push_back(std::shared_ptr<Algebra>(new Algebra(fact[l])));
            if(not _algs[0].back()->isAlgebra())
                ABORT("malformed algebra in"+fact[k]+"\n"+Algebra::failures);
        }
    }
}

std::vector<std::complex<double> > ProductFunction::operator()(const std::vector<double> X) const{
    validArguments(X,"(ProductFunction)");
    std::vector<std::complex<double> > res(_algs.size(),1.);
    for(int k=0;k<_algs.size();k++){
        for(int l=0;l<X.size();l++)res[k]*=_algs[k][l]->val(X[l]);
    }
    return res;
}
