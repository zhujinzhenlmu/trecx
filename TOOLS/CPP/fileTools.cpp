#include "fileTools.h"

#include <iostream>
#include <fstream>

namespace tools{

std::string findFirstLine(const std::string File, const std::vector<std::string> & String){
    std::ifstream f(File.c_str());
    std::string line;
    while(getline(f,line)){
        for(std::string s: String)
            if(line.find(s)!=std::string::npos)return line;
    }
    return "";
}
}
