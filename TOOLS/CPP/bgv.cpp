// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "zslapack.h"
#include "zslapack_f77.h"
#include "useMatrix.h"
#include <complex>

using namespace std;

void zslapack::bgv(bool Vectors, int n, int ka, int kb,
                   std::complex<double> *ab, int ldab, std::complex<double> *bb, int ldbb,
                   std::complex<double> *w, std::complex<double> *z, int ldz)
{
    vector<complex<double > >zwork(3*n);
    int info;
    complex<double>dummy;
    complex<double>*vecs=&dummy;
    char uplow='l';
    char jobz='n';
    if(Vectors){
        jobz='v';
        vecs=z;
    }

    zsbgv_(&jobz,&uplow,&n,&ka,&kb,ab,&ldab,bb,&ldbb,w,vecs,&ldz,zwork.data(),&info);

    if(info!=0){
        cout<<"complex symmetric banded eigensolver (zsbgv) failed, info= "<<info<<endl;
        abort();
    }

}
