// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "mpiWrapper.h"
#include "arpack.h"
#include "tools.h"
#include "constants.h"
#include "printOutput.h"

#include <vector>

using namespace std;

// rather brute force definition of the interfaces to the arpack F77 routines (may not be very portable)
extern "C"{

void znaupd_(int*,char*,int*,char*,int*,double*,complex<double>*,int*,complex<double>*,int*,
             int*,int*,complex<double>*,complex<double>*,int*,double*,int*);

void zneupd_(int*,char*,int*,complex<double>*,complex<double>*,int*,complex<double>*,complex<double>*,
             char*,int*,char*,int*,double*,complex<double>*,int*,complex<double>*,int*,
             int*,int*,complex<double>*,complex<double>*,int*,double*,int*);
}

map<string,string> ArpackFunctions::whichList;
void ArpackFunctions::whichListSet(){
    whichList["SmallReal"]="SR";
    whichList["LargeReal"]="LR";
    whichList["SmallImag"]="SI";
    whichList["LargeImag"]="LI";
    whichList["LargeAbs"]="LM";
    whichList["SmallAbs"]="SM";
}

void Arpack::eigenValues(vector<complex<double> > & Eval,unsigned int Nvec,const string & Which)
{
    A.eigen(this,Nvec,Which,false,false);
    Eval=eval;
}

void Arpack::eigen(vector<complex<double> > & Eval,vector<vector<complex<double> > >& Rvec,unsigned int Nvec,const string & Which,bool Restart)
{
    A.eigen(this,Nvec,Which,Restart,true);
    Rvec.clear();
    Eval=eval;
    for(unsigned int k=0;k<eval.size();k++){
        Rvec.push_back(std::vector<std::complex<double> >(0));
        for (unsigned int i=0;i<lvec;i++)Rvec[k].push_back(rvec[i+k*lvec]);
    }
}

void Arpack::verify() {
    vector<complex<double> > zero(lvec);
    double maxErr=0.,minErr=DBL_MAX;
    for(unsigned int k=0;k<eval.size();k++){
        apply(rvec.data()+k*lvec,zero.data());
        double norm=0.;
        for (unsigned int i=0;i<lvec;i++){
            norm=max(norm,abs(zero[i])); // L1 norm
            zero[i]-=eval[k]*rvec[i+k*lvec];
        }
        double err=0.;
        for(unsigned int i=0;i<lvec;i++)err=max(err,abs(zero[k])/norm); // L1 error
        minErr=min(minErr,err);
        maxErr=max(maxErr,err);
    }
    PrintOutput::message("verified "+tools::str(int(eval.size()))+" eigenvectors of length "+tools::str(lvec)
                         +", min/max error ="+tools::str(minErr,3)+"/"+tools::str(maxErr,3));
}

void ArpackFunctions::eigen(Arpack* Op,int Nvec, std::string Which, bool Restart, bool computeVectors, bool Parallel){

    if(Nvec+1>Op->lvec)ABORT("at most N-1 eigenvalues can be calculated ");

    // adjust storage
    Op->eval.resize(Nvec+1); // used for temporary storage by zneupd, will be truncted to size=Nvec
    if(computeVectors)Op->rvec.resize(Nvec*Op->lvec);

    if(whichList[Which]=="")ABORT("illegal Which="+Which+", admissible values: "+tools::listMapKeys(whichList,",",&Which));

    // initialze the communication parameters
    vector<int> iparam(11);
    iparam[0]=1;       // exact shifts
    iparam[2]=maxIter; // maximum number of iterations
    iparam[3]=1;       // must be =1
    iparam[6]=1;       // solve standard problem

    // decide restart, put starting vector if needed
    int info;
    vector<complex<double> >resid(Op->lvec);
    if(Restart){
        if(Op->rvec.size()<Op->lvec)ABORT("must supply initial vector for Restart");
        for(unsigned int i=0;i<Op->lvec;i++)resid[i]=Op->rvec[i];
        info=1;
        PrintOutput::message("Arpack used with restart option, criterion = "+Which,0);
    }
    else
        info=0;

    int vecs=0;
    if(computeVectors)vecs=1;


    // eventually define tools::Char(string) about as follows        char * which = new Char(whichList[Which]);

    // auxiliary variables for znaupd
    int ido=0,lvec=Op->lvec,nvec=Nvec,ncv=min(int(2*Nvec+5),int(Op->lvec-1));
    char bmat='I';
    // convert string to char*
    vector<char> which(whichList[Which].begin(),whichList[Which].end());
    which.push_back('\0');
    vector<complex<double> > v, workd, workl;
    vector<double> rwork;

    if(_seriel or MPIwrapper::isMaster()){
        v.resize(Op->lvec*ncv);
        workl.resize(3*ncv*ncv+5*ncv);
        rwork.resize(ncv);
    }
    workd.resize(3*Op->lvec);

    vector<int> ipntr(14);
    int lworkl=workl.size();

    while (ido!=99) {
        if(_seriel or MPIwrapper::isMaster()){
            znaupd_(&ido,&bmat,&lvec,which.data(),&nvec,&tolerance,resid.data(),&ncv,v.data(),&lvec,
                    iparam.data(),ipntr.data(),workd.data(),workl.data(),&lworkl,rwork.data(),&info);
            if(info!=0){
                if(info!=1)ABORT("znaupd failed, info="+tools::str(info));
                ido=99;// info=1: converged to maxIter
            }
        }

        if(not _seriel)MPIwrapper::Bcast(&ido,1,MPIwrapper::master());
        if(not _seriel)MPIwrapper::Bcast(ipntr.data(),ipntr.size(),MPIwrapper::master());
        switch (ido){
        default: ABORT("undefined value of ido: "+tools::str(ido));
        case 1:
        case -1:
            Op->apply(workd.data()+ipntr[0]-1,workd.data()+ipntr[1]-1);
            break;
        case 99:
            // additional auxiliary variables for zneupd
            char howmny='A';
            vector<int> select_v(ncv);
            complex<double>sigma;
            vector<complex<double> > workev(2*ncv);
            if(_seriel or MPIwrapper::isMaster()){
                zneupd_(&vecs,&howmny,select_v.data(),Op->eval.data(),Op->rvec.data(),&lvec,&sigma,
                        workev.data(),&bmat,&lvec,which.data(),&nvec,&tolerance,resid.data(),
                        &ncv,v.data(),&lvec,iparam.data(),ipntr.data(),workd.data(),
                        workl.data(),&lworkl,rwork.data(),&info);
                if(info!=0)ABORT("zneupd failed, info="+tools::str(info));
            }
            Op->eval.pop_back(); // original size is Nvec+1
            if(not _seriel)MPIwrapper::Bcast(Op->eval.data(),Op->eval.size(),MPIwrapper::master());
        }
    }
}

void ArpackStandard::apply(const complex<double> *X, complex<double> *Y) {
    for(complex<double> *y=Y;y<Y+lvec;y++)*y=0;
    unsigned int m=0;
    for(unsigned int i=0;i<lvec;i++)
        for(complex<double> *y=Y;y<Y+lvec;y++,m++)
            *y+=mat[m]*(*(X+i));
}

void ArpackStandard::test(){
    // generate a random matrix
    vector<complex<double> > mat,eval;
    vector<vector<complex<double> > >  evec;
    const unsigned int N=500;

    /* initialize random seed: */
    srand (1);
    double norm=2.*math::pi/double(RAND_MAX);
    for(unsigned int k=0;k<N*N;k++)
        mat.push_back(complex<double>(rand()*norm,rand()*norm));

    // create the ArpackStandard
    ArpackStandard A(mat,1.e-10,100);
    A.eigen(eval,evec,5,"SmallReal");
    A.verify();
}
