// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
/// this is the Linux version ... and dirty
#include "folder.h"
#include <stdlib.h>     /* system, NULL, EXIT_FAILURE */
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;



#ifdef _WIN32

//#include <boost/algorithm/string/replace.hpp>
#include <boost/filesystem.hpp>
#endif



bool folder::create(const string & Name){
#ifdef _WIN32
	return boost::filesystem::create_directory(Name);
#else
    // Remark: This executes a clone syscall, which may take a while (memory
    // is copied) and is possibly interrupted by SIGPROF if compiled with -pg.
    // This creates an infinite loop. So... don't compile with -pg.
    //
	// success returns 0, but that is logical false in cpp: therefore "not"
	return !system(("mkdir " + Name).c_str());
#endif
}

bool folder::exists(const string & Name){
#ifdef _WIN32
	return boost::filesystem::exists(Name);
#else
    ifstream infile(Name.c_str());
    return infile.good();
#endif
}
