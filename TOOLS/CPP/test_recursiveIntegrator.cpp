// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "test_recursiveIntegrator.h"

#include <boost/test/unit_test.hpp>
#include <cmath>
#include <complex>

#include "recursiveIntegrator.h"
//#include "tools.h"

using namespace std;
using namespace tools;
//using namespace boost::unit_test;

// ==== functions to integrate ============================
static double test_double_func(double x) {
    return sin(x);
}

static complex<double> test_complex_double_func(double x) {
    return exp(complex<double>(0.,1.)*x);
}

static const vector<double>& test_vector_double_func(double x) {
    static vector<double> value(2);
    value[0]=sin(x);
    value[1]=cos(x);
    return value;
}

static complex<double> test_complex_double_polynom_4(double x) {
    return complex<double>(1.+8.*pow(x,2)+pow(x,3), 500.*x+pow(x,4));
}
// ==== tests =============================================
void tools::tests::test_recursiveIntegrator_double()
{
    vector<double> interval(2);
    interval[0]=0.;
    interval[1]= 19.*M_PI;
    RecursiveIntegrator<double> test (&test_double_func);
//    using std::abs;
    BOOST_CHECK_CLOSE(test.integrate(interval), 2., 1.e-6);
}

void tools::tests::test_recursiveIntegrator_complex_double()
{
    vector<double> interval(2);
    interval[1]=20*M_PI;
    RecursiveIntegrator<complex<double> > test(&test_complex_double_func);
    using std::abs;
    BOOST_CHECK(abs(test.integrate(interval)) < 1.e-7);
}

void tools::tests::test_recursiveIntegrator_vector_double()
{
    vector<double> interval(2), exactIntegral(2), integral(2);
    interval[1]= 9.*M_PI;
    exactIntegral[0]=2;
    RecursiveIntegrator<vector<double> > test(&test_vector_double_func);
    integral=test.integrate(interval);
    BOOST_CHECK(abs(integral-exactIntegral) < 1.e-7);
}


void tests::test_recursiveIntegrator_static_integration_complex_double()
{
    vector<double> interval(2);
    interval[0]=-10; interval[1]=-5;
    RecursiveIntegrator<complex<double> > test(&test_complex_double_polynom_4);
    using std::abs;
    BOOST_CHECK(abs(test.integrate(interval)-complex<double>(-65./12.,625.))<1.e-6);
}
