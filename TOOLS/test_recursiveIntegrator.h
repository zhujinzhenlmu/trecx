// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef TEST_RECURSIVEINTEGRATOR_H
#define TEST_RECURSIVEINTEGRATOR_H

namespace tools {

namespace tests {

void test_recursiveIntegrator_double();

void test_recursiveIntegrator_complex_double();

void test_recursiveIntegrator_vector_double();

void test_recursiveIntegrator_static_integration_complex_double();

} //tests

} //tools


#endif // TEST_RECURSIVEINTEGRATOR_H
