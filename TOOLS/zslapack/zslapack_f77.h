// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef ZSLAPACK_F77_H
#define ZSLAPACK_F77_H
#include "stdlib.h"
#include <complex>

/// direct interfaces to the Fortran77 zslapack routines
/// do not use unless you know what you are doing
/// standard interfaces are in "zslapack.h"
extern "C" {

void zsbgv_(const char *vect,const char *uplow,
            const int *n,const int *ka,const int *kb,
            std::complex<double> *A, const int *lda,
            std::complex<double> *B, const int *ldb,
            std::complex<double> *Eval,
            std::complex<double> *Evec, const int *ldv,
            std::complex<double> *zwork,int *info);
}

#endif // ZSLAPACK_F77_H
