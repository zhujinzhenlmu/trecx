// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef ZSLAPACK_H
#define ZSLAPACK_H
#include <complex>
#include "useMatrix.h"

void lapack_zsbgv(UseMatrix &A, UseMatrix &B, UseMatrix & Eval, UseMatrix & Evec,bool Vectors);

class zslapack{
public:
    static void bgv(bool Vectors, int n, int ka, int kb,
                    std::complex<double>* ab, int ldab, std::complex<double>*bb, int ldbb,
                    std::complex<double>* w, std::complex<double>* z, int ldz );
};
#endif // ZSLAPACK_H
