// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef MULTIINDEX_H
#define MULTIINDEX_H
#include "../TOOLS/toolsHeader.h"
#include "abort.h"

///< a general multi-index class
class MultiIndex{
public:
    MultiIndex(const std::vector<int> & M);
    /// increment multi-index, initial=empty vector, final=return false and empty vector
    /// NOTE: row-wise increment, i.e. rightmost indices run fastest
    bool next(std::vector<int> & i);
    bool nextCol(std::vector<int> & i); //!< column-wise increment, i.e. left-most indices run fastest
    void first(std::vector<int>& i); //!< set first multi-index
private:
    std::vector<int> m;
};

#endif // MULTIINDEX_H
