// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef _TRECXCHECKS_H_
#define _TRECXCHECKS_H_

#include <vector>
#include <string>
#include <iostream>


/** @defgroup Checks
 *  @ingroup Tools
  * \brief global control of checks and verifications
  * @{
  */
/// \file

namespace tRecX{

/// \ingroup Checks
/// \fn for globally switching on- and off checks
/// \brief usage: off("someName") returns false, if "someName" is in Checks: off=someName someOther yetSome
bool off(const std::string mess="global");
void read(); // create the info
void print(); // display status
void setOff(std::string Off);

}
/** @} */

#endif
