// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef VECTORREAL_H
#define VECTORREAL_H

#include <vector>

/// \ingroup Linalg
/// \brief adds linear space operations to std::vector<double >
class VectorReal:public std::vector<double>
{
public:
    VectorReal(const vector<double> & B):std::vector<double>(B.begin(),B.end()){}
    VectorReal(unsigned int Size=0):std::vector<double>(Size){}
    VectorReal(const VectorReal & B, int Begin, int End):std::vector<double>(End-Begin){for(int k=0;k<End-Begin;k++)data()[k]=B[Begin+k];}
    VectorReal & operator+=(const VectorReal & Other);
    VectorReal & operator-=(const VectorReal & Other);
    VectorReal operator-(const VectorReal & Other) const;

    VectorReal & operator*=(double A);
    VectorReal operator*(double A) const;
    VectorReal & axpy(double A, const VectorReal & X);
    double dot(const VectorReal & X) const;

    double normSqu() const;
    double maxAbsVal() const;

    VectorReal & purge(double Eps=1.e-12); ///< set near-zeros to =0

    static void Test();
};

#endif // VECTORREAL_H
