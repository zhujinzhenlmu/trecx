#ifndef COORRHOZ_H
#define COORRHOZ_H

#include "coorSystem.h"

/// \ingroup Coordinates
/// \brief Rho.Z, subsystem of cylinder - combines with Eta.R
class CoorRhoZ : public CoorSystem
{
protected:
    std::vector<double> _toRef(const std::vector<double> & RhoZ) const;
    std::vector<double> _fromRef(const std::vector<double> & EtaR) const;
    std::vector<double> _jacRefdCoor(const std::vector<double> & RhoZ) const;
public:
    CoorRhoZ(std::string Name=""):CoorSystem("Rho.Z","Eta.R",Name){}
};

#endif // COORRHOZ_H
