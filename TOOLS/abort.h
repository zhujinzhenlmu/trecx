// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef _ABORT_H_
#define _ABORT_H_
#include <map>
#include <iostream>

#define DEVABORT(message) devAbortFile(__FILE__,__LINE__, message)
#define ABORT(message) abortFile(__FILE__,__LINE__,message)
#define COUNTDOWN(message,cnt) abortCountDown(__FILE__,__LINE__,message,cnt)


/** @defgroup Abort Run time
 *  @ingroup Tools
  * \brief controlled abort, timing
  * @{
  */
/// \file

/// \ingroup Abort
/// \fn static void abortFile(const std::string file,int line, const std::string mess="")
/// \brief universal abort function, best use through macro ABORT(message-string)
void abortFile(const std::string file,int line, const std::string mess="",int signal=0);
void devAbortFile(const std::string file,int line, const std::string mess="");

/// \ingroup Abort
/// \fn
/// \brief countdown abort function, best use through macro COUNTDOWN(message-string, counts)
void abortCountDown(const std::string file,int line,const std::string mess, unsigned int Cnt);


/// \brief activate by flag -DEBUGnew[=true]
bool newCode();
/// \brief activate by flag -DEBUGold[=true]
bool oldCode();
bool oldEva();

#endif

/** @} */
