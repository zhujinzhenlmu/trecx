// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef MAPLINSPACE_H
#define MAPLINSPACE_H

#include <complex>
#include <vector>
#include "abort.h"

/// \ingroup Linalg
/// \brief map from a linear space onto itself (map is not necessarily linear)
///
/// V ...complies with LinSpaceVector
template <class V>
class LinSpaceMap{
    bool _allowAlias;
public:
    virtual ~LinSpaceMap(){}
    LinSpaceMap(bool AllowAlias=false):_allowAlias(AllowAlias){}
    /// Y <- A * Map(X) + B * Y
    virtual void apply(std::complex<double> A, const V & X,std::complex<double> B, V & Y) const=0;
    virtual bool applyAlias(){return _allowAlias;} ///< true if &X==&Y is allowed in apply

    /// example object V (for use, e.g., in copy-constructors)
    virtual const V & lhsVector() const=0;
    virtual const V & rhsVector() const=0;

    /// update the map using a set of parameters
    virtual void update(double Time, const V* CurrentVector=0)=0;
};

#endif // MAPLINSPACE_H
