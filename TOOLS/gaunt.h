// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef GAUNT_H
#define GAUNT_H

#include "qtEigenDense.h"
#include "tools.h"

///@brief OBSOLESCENT (in new code, use Gaunts instead)
class Gaunt{
  int lmax;                                  // lmax of the calculation
  int order;                                 // 2*lmax+1
  std::vector<Eigen::MatrixXd> Passoc;       //Passoc[m](l,x)
  Eigen::VectorXd quadX,quadW;

public:
  static Gaunt main;

  Gaunt();
  void initialize(int lmax);
  double coeff(int lc,int l1,int l2, int mc,int m1,int m2);
  bool coeff_isZero(int lc,int l1,int l2, int mc,int m1,int m2);
  void test();

  double twoYintegrals(int l1,int l2,int m1,int m2, std::string operat);
};

#endif // GAUNT_H
