#ifndef COORETAR_H
#define COORETAR_H

#include "coorSystem.h"

/// \ingroup Coordinates
/// \brief Eta.R  subsystem of polar - combines with Rho.Z
class CoorEtaR: public CoorSystem{
protected:
    std::vector<double> _toRef(const std::vector<double> & Coor) const{ return Coor;}
    std::vector<double> _fromRef(const std::vector<double> & Ref) const{return Ref;}
    std::vector<double> _jacRefdCoor(const std::vector<double> & Coor) const{return {1.,0.,0.,1.};}
public:
    CoorEtaR(std::string Name=""):CoorSystem("Eta.R","Eta.R",Name){}
};

#endif // COORETAR_H
