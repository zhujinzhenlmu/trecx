// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef TREEEXAMPLE_H
#define TREEEXAMPLE_H

#include "tree.h"

/// test class for tree
class TreeExample:public Tree<TreeExample>
{
public:
    int val;
    TreeExample():val(0){}
    TreeExample(unsigned int Depth, unsigned int Width, unsigned int LevelVal=1, const TreeExample* Parent=0);
    void valIndex(); // set value to index

    std::string strData(int Precision=0) const {return tools::str(val);}

    bool nodeEquivalent(const TreeExample *Node) const{return val/100000==Node->val/100000;} //< this is not really a good example, as rearrangement would not be allowed
    bool nodeEmpty() const {return val<100000;}
    void nodeCopy(const TreeExample *Node,bool View){val=Node->val;}

    static void test();
};


#endif // TREEEXAMPLE_H
