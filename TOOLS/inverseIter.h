// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef INVERSEITER_H
#define INVERSEITER_H
#include <vector>
#include "useMatrix.h"
#include "arpack.h"

class InverseIter:public Arpack
{
    UseMatrix aMinusEgM;
    const UseMatrix &mat,&ovr;
    UseMatrix yTemp,xTemp;

    void apply(const std::complex<double> *X, std::complex<double> *Y);

public:
    static void test();

    InverseIter(const UseMatrix &Mat, const UseMatrix & Ovr)
        :Arpack(1.e-12,100),mat(Mat),ovr(Ovr),xTemp(UseMatrix(Mat.cols(),1)),yTemp(UseMatrix(Mat.cols(),1)){lvec=yTemp.size();}

    /// \brief eigen - find several eigenvalues:  Mat Evec[k] = Ovr Evec[v] Eval[k]
    /// \param Eval  - initial guess valeus, Eval.size()  > 0
    /// \param Evec  - columns are eigenvectors, non-empty: guess vectors
    void eigen(UseMatrix & Eval, UseMatrix & Evec);

};

#endif // INVERSEITER_H
