#ifndef MULTIARRAYWRAPPER_H
#define MULTIARRAYWRAPPER_H
#include <vector>
#include "qtEigenDense.h"
#ifdef _USE_BOOST_
#include <boost/multi_array.hpp>
typedef boost::multi_array<double,3> multiarray3d;
typedef boost::multi_array<Eigen::MatrixXcd,4> multiarray4EigenXcd;
typedef boost::multi_array<Eigen::MatrixXcd,3> multiarray3igenXcd;
#else
typedef std::vector<double> multiarray3d;
typedef std::vector<double> multiarray4EigenXcd;
typedef std::vector<double> multiarray3igenXcd;
#endif
#endif // MULTIARRAYWRAPPER_H
