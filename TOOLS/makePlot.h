// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef MAKEPLOT_H
#define MAKEPLOT_H

#include <string>

class ReadInput;
class Algebra;

class MakePlot
{
    std::string _file;
    unsigned int _nPoints;
    double _xMin,_xMax;
public:
    MakePlot(ReadInput & Inp);
    void addPlot(const Algebra & A) const;

    double xMin() const {return _xMin;}
    double xMax() const {return _xMax;}
    unsigned int nPoints() const {return _nPoints;}
    std::string file() const {return _file;}
};

#endif // MAKEPLOT_H
