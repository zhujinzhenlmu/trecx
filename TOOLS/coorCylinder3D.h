#ifndef COORCYLINDER3D_H
#define COORCYLINDER3D_H

#include "coorSystem.h"

/// \ingroup Coordinates
/// \brief cylinder coordinates Phi.Rho.Z
class CoorCylinder3D : public CoorSystem
{
protected:
    std::vector<double> _toRef(const std::vector<double> & PhiRhoZ) const;
    std::vector<double> _fromRef(const std::vector<double> & XYZ) const;
    std::vector<double> _jacRefdCoor(const std::vector<double> & PhiRhoZ) const;
public:
    CoorCylinder3D(std::string Name=""):CoorSystem("Phi.Rho.Z","X.Y.Z",Name){}
};

#endif // COORCYLINDER3D_H
