#ifndef PRODUCTFUNCTION_H
#define PRODUCTFUNCTION_H

#include <memory>

#include "vectorValuedFunction.h"
class Algebra;

///@ingroup Functions
///@brief Vector-valued multi-argument function given by products of single-argument Algebra's
///
/// Format (example for radial polar coordinates Phi.Eta.Rn, H-atom)
/// <br> 1.1.Q*exp(-Q),1.Q.pow[2](Q)*exp(-Q/2),exp(i*Q).sqrt(1-Q*Q).pow[2](Q)*exp(-Q/2)...states m,l,n=(0,0,1),(0,1,2),(1,1,2)
class ProductFunction : public VectorValuedFunction
{
    std::string _coors;
    std::vector<std::vector<std::shared_ptr<Algebra> > >_algs;
public:
    ProductFunction(std::string Coors /** e.g. X.Y.Z, Phi.Eta.Rn */,
                    std::vector<std::string> Algs /** list of dot-separated Algebra strings */);
    std::string coordinates() const {return _coors;}
    std::vector<std::complex<double> > operator()(const std::vector<double> X) const;
    unsigned int length() const{return _algs.size();}
};

#endif // PRODUCTFUNCTION_H
