// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef TMATH_H
#define TMATH_H

class tMath
{
public:
    tMath(){}
    static long double factorial(unsigned int N);
    static long double doubleFactorial(unsigned int N);
};

#endif // MATH_H
