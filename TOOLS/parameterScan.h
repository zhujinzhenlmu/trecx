// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef PARAMETERSCAN_H
#define PARAMETERSCAN_H

#include "readInputRange.h"
#include "asciiFile.h"
/// run through multi-parameter range and store results
class ParameterScan : public MultiParam
{
    typedef void (*ValuesForParameters)(const std::vector<std::string> & Name,const std::vector<double> & Par, std::vector<double > & Result);
    ValuesForParameters valForPar;
    std::string outDir;
public:
    ParameterScan(ReadInput & Inp);
    void setFunction(ValuesForParameters ValForPar){valForPar=ValForPar;}
    void scan();
};

#endif // PARAMETERSCAN_H
