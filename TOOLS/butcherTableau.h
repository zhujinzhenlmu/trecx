// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef BUTCHERTABLEAU_H
#define BUTCHERTABLEAU_H

#include <map>
#include <vector>
#include <string>
///@brief collection of various explicit Runge-Kutta methods

/// from: L. Lapidus and J. Seinfeld,
///  <br>     "Numericel Solution of Ordinary Differential Equations"
///  <br>     Academic Press, New York and London, 1971
class ButcherTableau{
    std::string _name;
    int _consistency;
    static std::map<std::string,ButcherTableau> tab;
    std::vector<std::vector<double> > _a;
    std::vector<double> _b,_c;
public:

    ButcherTableau():_consistency(0){}
    ButcherTableau(std::string Name);

    std::string name() const {return _name;}
    int consistency() const {return _consistency;}
    std::vector<std::vector<double> > a() const {return _a;}
    std::vector<double> b() const {return _b;}
    std::vector<double> c() const {return _c;}

};


#endif // BUTCHERTABLEAU_H
