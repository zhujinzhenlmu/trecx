#include <string>
#include <map>
#include <vector>

#include "mpiWrapper.h"
#include "readInput.h"

size_t getPeakRSS();
size_t getCurrentRSS();
std::map<std::string, double> getMemInfo();

