// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#include "readInput.h"
//#include "algebra.h"

#include <iostream>
#include <fstream>
#include "vectorReal.h"
#include "orthopol.h"
#include "orthogonalDerived.h"
#include "tools.h"
#include "odeTest.h"
#include "vectorComplex.h"
#include "treeExample.h"
#include "algebra.h"
#include "str.h"
#include "odeRungeKutta.h"

using namespace std;
using namespace tools;
using std::ofstream;
using std::ios_base;

int main(int argc, const char* argv[]) {

//     OrthogonalPolynomial::Test(true);
//    Algebra::Test();
//    Integrate::test();
//    Units::test();
//    ReadInput::Test(argc,argv);
//    TreeExample::test();
//    Str::Test();
//    OdeRungeKutta<TestExp,complex<double> >::test();

    OrthogonalPolynomial::Test(true);
    OrthogonalDerived::verify();



}
