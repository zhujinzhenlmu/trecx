// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef ORTHOGONALDERIVED_H
#define ORTHOGONALDERIVED_H

#include <memory>
#include "orthopol.h"

class Algebra;
class OrthogonalDerived: public OrthogonalPolynomial{
    std::vector<std::vector<double> >recCoe;
    std::vector<double> _normsq;
    std::shared_ptr<OrthogonalPolynomial>_base;
    std::shared_ptr<Algebra> _aWeig,_aDer;

public:
    OrthogonalDerived(unsigned int MaxDegree, const OrthogonalPolynomial *Base, std::string Weig, std::string Der);
    inline double normsq(int I) const{return _normsq[I];}
    inline long double lowerBoundary() const {return _base->lowerBoundary();}
    inline long double upperBoundary() const {return _base->upperBoundary();}
    double weight(double X) const;
    double derWeight(double X) const;

    static void verify(); // test functionality
    typedef double (*positiveFunction)(double);
private:
    void construct(std::vector<std::vector<double> > Ovr);
    inline long double a(int i) const {return recCoe[0][i-1];}
    inline long double b(int i) const {return recCoe[1][i-1];}
    inline long double c(int i) const {return recCoe[2][i-1];}

    std::vector<std::vector<double> > overlap(unsigned int Degree);

};


#endif // ORTHOGONALDERIVED_H
