#ifndef POLYLAGRANGE_H
#define POLYLAGRANGE_H

#include <vector>

/// Lagrange polynomials
template<class Arg>
class PolyLagrange
{
    std::vector<Arg> _valInit;
    std::vector<Arg> _mesh;
public:
    PolyLagrange(std::vector<Arg> Mesh)
        :_mesh(Mesh)
    {
        _valInit.assign(Mesh.size(),1.);
        for (int i=0;i<_valInit.size();i++)
            for(int j=0;j<_mesh.size();j++)
                if(i!=j)_valInit[i]=_valInit[i]*(1./(_mesh[i]-_mesh[j]));
    }

    /// values of all polynomials at Q
    std::vector<Arg> values(Arg Q) const
    {
        std::vector<Arg> v(_valInit);
        for (int i=0;i<_valInit.size();i++)
            for(int j=0;j<_mesh.size();j++)
                if(i!=j)v[i]*=(Q-_mesh[j]);
        return v;
    }

};

#endif // POLYLAGRANGE_H
