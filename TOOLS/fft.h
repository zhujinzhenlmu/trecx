// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef FFT_H
#define FFT_H

#include <vector>
#include <complex>
#ifdef _USE_FFTW_
#include <fftw3.h>
#endif

class Fft
{
#ifdef _USE_FFTW_
    fftw_complex *in, *out;
    fftw_plan plan;
public:
    ~Fft();
    Fft(unsigned int Size, bool Forward);

    /// normalized DFT (normalizations 1/sqrt(N) included)
    std::vector<std::complex<double> > &transform(std::vector<std::complex<double> > & Vec);

    static void Test(unsigned int Size);
#endif
};

#endif // FFT_H
