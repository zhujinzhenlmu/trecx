// The tRecX package is free for personal use.
// Any commercial use of the code or parts of it is excluded.
// Restrictions for academic use apply. 
// 
// See terms of use in the LICENSE file included with the source distribution
// 
// Copyright (c) 2015,2016,2017,2018 by Armin Scrinzi
// End of license
 
#ifndef FOLDER_H
#define FOLDER_H

#include <string>

/// \ingroup IO
/// \brief check files and folders
namespace folder
{
bool create(const std::string & name);
bool exists(const std::string & name);
}

#endif // FOLDER_H
